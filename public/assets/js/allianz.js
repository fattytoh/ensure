function copyDriver(obj_id, obj_val){
    $("#driver_relation_1").val("I").trigger("change");
    switch(obj_id) {
        case "order_cus_lname":
            $("#driver_lname_1").val(obj_val);
            break;
        case "order_cus_fname":
            $("#driver_fname_1").val(obj_val);
            break;
        case "order_cus_national":
            $("#driver_national_1").val(obj_val).trigger("change");
            break;
        case "order_cus_nirc":
            $("#driver_nirc_1").val(obj_val);
            break;
        case "order_cus_nirc_type":
            $("#driver_nirc_type_1").val(obj_val).trigger("change");
            break;
        case "order_cus_dob":
            $("#driver_dob_1").val(obj_val);
            var new_format = changeDateFormat(obj_val);
            $("#driver_dob_1").next('input').val(new_format);
            break;
        case "order_cus_gender":
            $("#driver_gender_1").val(obj_val).trigger("change");
            break;
        case "order_cus_marital":
            $("#driver_marital_1").val(obj_val).trigger("change");
            break;
        case "order_cus_occ":
            $("#driver_occupation_1").val(obj_val);
            break;
        case "order_cus_exp":
            $("#driver_exp_1").val(obj_val);
            break;
    }
}

function calTSACompProfit(){
    var input_amt = numberRebuild($("#order_premium_amt").val());
    var claim_amt = numberRebuild($("#order_accident_amt").val());
    var ncd_amt = numberRebuild($("#order_ncd_dis_amt").val());
    var in_exp_amt = numberRebuild($("#order_in_exp_amt").val());
    var del_amt = numberRebuild($("#order_delivery_amt").val());
    var add_amt = numberRebuild($("#order_addtional_amt").val());
    var dis_amt = numberRebuild($("#order_discount").val());
    var subtotal = 0;
    var grand_total = 0;

    subtotal = parseFloat(input_amt) + parseFloat(claim_amt) - parseFloat(ncd_amt) + parseFloat(in_exp_amt) + parseFloat(del_amt) + parseFloat(add_amt)

    $("#order_subtotal_amount").val(RoundNum(subtotal,2));
  
    var gst_amount = 0;
    var gst_percent = numberRebuild($("#order_gst_percent").val());

    gst_amount = (parseFloat(subtotal) - parseFloat(dis_amt) ) * (gst_percent / 100);
    $("#order_gst_amount").val(RoundNum(gst_amount,2));

    grand_total = parseFloat(subtotal) - parseFloat(dis_amt) + parseFloat(gst_amount);
    $("#order_grosspremium_amount").val(RoundNum(grand_total,2));
}

function changeDateFormat(inputDate){
    var dt = new Date(inputDate);
    if(!isNaN(dt)){
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        return day +'/'+ month + '/' + year;
    }
    else{
        return inputDate;
    }
}