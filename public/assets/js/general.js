function calculateGeneralTotal(){                
    var input_amt = numberRebuild($("#order_premium_amt").val());
    var dir_charge_amt = numberRebuild($("#order_direct_discount_amt").val());
    var gst_charge_per = numberRebuild($("#order_gst_percent").val());
    var gst_charge_amt = numberRebuild($("#order_premium_amt").val());
    var addtional_amt = numberRebuild($("#order_addtional_amt").val());
    var bank_interest_amt = numberRebuild($("#order_bank_interest").val());
    var grand_total = 0;
    
    $("#order_direct_discount_amt").val(changeNumberFormat(RoundNum(dir_charge_amt, 2)));
    var sub_total = parseFloat(input_amt)  + parseFloat(addtional_amt);
    $("#order_subtotal_amount").val(changeNumberFormat(RoundNum(sub_total, 2)));
    
    gst_charge_amt = parseFloat(sub_total) * (parseFloat(gst_charge_per) / 100);
    
    $("#order_gst_amount").val(changeNumberFormat(RoundNum(gst_charge_amt, 2)));
    grand_total = parseFloat(sub_total) + parseFloat(RoundNum(gst_charge_amt,2))  -parseFloat(dir_charge_amt) + parseFloat(bank_interest_amt);
    $("#order_grosspremium_amount").val(changeNumberFormat(RoundNum(grand_total, 2)));
    
    if($("#order_doc_type").val() == 'DN'){ 
        if(grand_total >= 0){
            $("#cus_dis_total").removeClass( "text-red" ).addClass( "text-green" );
        }
        else{
            $("#cus_dis_total").removeClass( "text-green" ).addClass( "text-red" );
        }
        $("#cus_dis_total").html(changeNumberFormat(RoundNum(grand_total, 2)));
    } else{ 
        if(grand_total <= 0){
            $("#cus_dis_total").removeClass( "text-red" ).addClass( "text-green" );
            $("#cus_dis_total").html("$" + changeNumberFormat(RoundNum(Math.abs(grand_total), 2)));
        }
        else{
            $("#cus_dis_total").removeClass( "text-green" ).addClass( "text-red" );
            $("#cus_dis_total").html("($" + changeNumberFormat(RoundNum(grand_total, 2)) + ")");
        }
    } 

    calculateGeneralTSACompCharge();
}

function calculateGeneralTSACompCharge(){  
    var main_income = 0;
    var subtotal_amount = numberRebuild($("#order_subtotal_amount").val());
    var outpaitient = numberRebuild($("#order_addtional_amt").val());
    var direct_discount = numberRebuild($("#order_direct_discount_amt").val());
    var bank_interest = numberRebuild($("#order_bank_interest").val());
    var comp_comm_gestper = numberRebuild($("#order_payable_gstpercent").val());

    var cover_plan_no_gst = numberRebuild($("#order_cover_plan_no_gst").val());
    main_income = parseFloat(subtotal_amount) + parseFloat(cover_plan_no_gst);  
    var govGST = parseFloat(subtotal_amount) * numberRebuild($("#order_gst_percent").val()) / 100;

    var tsa_fee_per = numberRebuild($("#order_receivable_dealercommpercent").val());
    var tsa_gst_per = numberRebuild($("#order_receivable_gstpercent").val());
    var direct_dis = numberRebuild($("#order_direct_discount_amt").val());
    var pay_fee_per = numberRebuild($("#order_payable_ourcommpercent").val());
    $(".span_premium").html(changeNumberFormat(RoundNum(main_income,2)));
    $(".span_bank_interest").html(changeNumberFormat(RoundNum(bank_interest,2)));
    $("#order_receivable_gstamount").val(RoundNum(govGST,2));
    $("#order_payable_gst").val(RoundNum(govGST,2));

    var tsa_fee_amt = 0;
    if(tsa_fee_per > 0){
        var basic_tsa_amt = (parseFloat(main_income) - parseFloat(outpaitient)) * (tsa_fee_per / 100);
        var out_tsa_amt = parseFloat(outpaitient) * (5 / 100);
        tsa_fee_amt = parseFloat(RoundNum(basic_tsa_amt,2)) + parseFloat(RoundNum(out_tsa_amt,2));
    }
    $("#order_receivable_dealercomm").val(RoundNum(tsa_fee_amt,2));
    if($("#order_receivable_dealergst").val() == 'Y'){
        var tsaGST = RoundNum(tsa_fee_amt,2) * (tsa_gst_per / 100);
        $("#order_receivable_dealergstamount").val(RoundNum(tsaGST,2));
    }
    else{
        var tsaGST = 0;
        $("#order_receivable_dealergstamount").val(RoundNum(0,2));
    }

    var pay_fee_amt = 0;
    if(pay_fee_per > 0){
        var basic_comp_amt = (parseFloat(main_income) - parseFloat(outpaitient)) * (pay_fee_per / 100);
        var out_comp_amt = parseFloat(outpaitient) * (15 / 100);
        pay_fee_amt = basic_comp_amt + out_comp_amt;
    }
    $("#order_payable_ourcomm").val(RoundNum(pay_fee_amt,2));

    var nett_per = parseFloat(RoundNum(pay_fee_per,2)) - parseFloat(RoundNum(tsa_fee_per,2));
    var nett_amt = parseFloat(RoundNum(pay_fee_amt,2)) - parseFloat(RoundNum(tsa_fee_amt,2)) - parseFloat(RoundNum(direct_dis,2));
    
    $("#order_payable_nettcommpercent").val(RoundNum(nett_per,2));
    $("#discount_give").html(RoundNum(direct_dis,2));
    $("#order_payable_nettcomm").val(RoundNum(nett_amt,2));
    
    if($("#order_payable_premiumpayable").val() == 'Y'){
        var pay_gst = RoundNum((pay_fee_amt * comp_comm_gestper / 100 ), 2);
        $("#order_payable_gstcomm").val(RoundNum(pay_gst,2));
    }
    else{
        var pay_gst = 0;
        $("#order_payable_gstcomm").val(RoundNum(pay_gst,2));
    }
    
    if($("#order_record_billto").val() == "To Customer"){
        var final_total = parseFloat(RoundNum(govGST,2)) + parseFloat(RoundNum(main_income,2)) - parseFloat(RoundNum(direct_discount,2)) + parseFloat(bank_interest);
        $("#order_receivable_premiumreceivable").val(changeNumberFormat(RoundNum(final_total,2))); 
    }
    else{
        var final_left = 0;
        final_left = parseFloat(RoundNum(main_income,2)) - parseFloat(RoundNum(tsa_fee_amt,2)) - parseFloat(RoundNum(tsaGST,2)) + parseFloat(RoundNum(govGST,2)) + parseFloat(bank_interest);
        $("#order_receivable_premiumreceivable").val(changeNumberFormat(RoundNum(final_left,2))); 
    }
    
    var pay_final = parseFloat(RoundNum(main_income,2)) - parseFloat(RoundNum(pay_fee_amt,2)) - parseFloat(RoundNum(pay_gst,2)) + parseFloat(RoundNum(govGST,2)) + parseFloat(bank_interest);
    
    $("#order_payable_premium").val(changeNumberFormat(RoundNum(pay_final,2)));
}

function changeDateFormat(inputDate){
    var dt = new Date(inputDate);
    if(!isNaN(dt)){
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        return day +'/'+ month + '/' + year;
    }
    else{
        return inputDate;
    }
}

function planChange(){
    var fee = 0;
    var waiver = 0;
    var input_tag;
    var input_tag1;
    $("input[name='order_optional_coverage_plan1']:checked").each(function(){
        input_tag = $(this).val();
    });
    $("input[name='order_optional_coverage_plan']:checked").each(function(){
        input_tag1 = $(this).val();
    });
    if(input_tag){
        if(input_tag == '300MDC'){
            var input_amt = 85.98;
        }
        else{
            var input_amt = 150;
        }
    }
    else{
        var input_amt = 0;
    }
    
    if(input_tag1){
        if(input_tag1 == '2K'){
            $("#order_cover_plan_no_gst").val(changeNumberFormat(RoundNum(33, 2)));
        }
        else{
            $("#order_cover_plan_no_gst").val(changeNumberFormat(RoundNum(67, 2)));
        }
    }
    else{
        $("#order_cover_plan_no_gst").val(changeNumberFormat(RoundNum(0, 2)));
    }
    
    if( $("#order_waiver_indemnity").val() == 'Yes'){
        waiver = 50;
    }
    
    switch($("#order_coverage").val()){
        case 'Classic Plan':
            if($("#order_period").val() == 14){
                fee = 165;
            }
            else{
                fee = 230;
            }
            break;
        case 'Deluxe Plan':
            if($("#order_period").val() == 14){
                fee = 170;
            }
            else{
                fee = 260;
            }
            break;
        case 'Exclusive Plan':
            if($("#order_period").val() == 14){
                fee = 236;
            }
            else{
                fee = 335;
            }
            break;
        default:
            break;
    }
    
    var total = parseFloat(fee) + parseFloat(waiver);
    $("#order_addtional_amt").val(changeNumberFormat(RoundNum(input_amt, 2)));
    $("#order_premium_amt").val(changeNumberFormat(RoundNum(total, 2)));
    calculateMaidTotal(); 
}