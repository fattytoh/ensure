function validateNRIC(str) {
    if (str.length != 9) 
        return false;

    str = str.toUpperCase();

    var i, 
        icArray = [];
    for(i = 0; i < 9; i++) {
        icArray[i] = str.charAt(i);
    }

    icArray[1] = parseInt(icArray[1], 10) * 2;
    icArray[2] = parseInt(icArray[2], 10) * 7;
    icArray[3] = parseInt(icArray[3], 10) * 6;
    icArray[4] = parseInt(icArray[4], 10) * 5;
    icArray[5] = parseInt(icArray[5], 10) * 4;
    icArray[6] = parseInt(icArray[6], 10) * 3;
    icArray[7] = parseInt(icArray[7], 10) * 2;

    var weight = 0;
    for(i = 1; i < 8; i++) {
        weight += icArray[i];
    }

    var offset = (icArray[0] == "T" || icArray[0] == "G") ? 4:0;
    var temp = (offset + weight) % 11;

    var st = ["J","Z","I","H","G","F","E","D","C","B","A"];
    var fg = ["X","W","U","T","R","Q","P","N","M","L","K"];

    var theAlpha;
    if (icArray[0] == "S" || icArray[0] == "T") { theAlpha = st[temp]; }
    else if (icArray[0] == "F" || icArray[0] == "G") { theAlpha = fg[temp]; }

    return (icArray[8] === theAlpha);
}

function validatePlate(str) {
    if(str.length > 8) {
        return false;
    } else {
        var original = str.slice(0,-1);
        var copy = original;
        var plateAry = original.split('');
        var numCounter = 0;
        var alpCounter = 0;
        var weightAry = new Array('5','4','3','2');
        var weightCounter = 0;
        var checksumAry = new Array('A','Z','Y','X','U','T','S','R','P','M','L','K','J','H','G','E','D','C','B');
        for(var n=0; n<plateAry.length; n++){
            if (isNaN(plateAry[n])) {
                    alpCounter++;
            }
            else {
                    numCounter++;
            }						      
        }
        if (alpCounter == 2) {
            var copy = "0" + original;
        }
        else if (alpCounter == 1) {
            var copy = "00" + original;
        }
        if (numCounter == 1) {
            weightCounter = weightCounter + 3;
        }
        else if (numCounter == 2) {
            weightCounter = weightCounter + 2;
        }
        else if (numCounter == 3) {
            weightCounter++;
        }
        var plateAry = copy.split('');
        var second = letterToNumbers(plateAry[1]);
        var third = letterToNumbers(plateAry[2]);
        var second = second * 9;
        var third = third * 4;
        var sum = second + third;
        for(var n=3; n<plateAry.length; n++){
            var sum = sum + (plateAry[n]*weightAry[weightCounter]);
            weightCounter++;								      
        }
        var divide = sum % 19;
        var original = original + "" + checksumAry[divide];
        
        if (original == str){
            return true;
        }
        else {
            return false;
        }
    }
}

function jsonToOption(jsonStr){
    var html = '';
    var opt_data = JSON.parse( jsonStr );
    $.each(opt_data, function(index, value){
        html += '<option value="'+ value.value +'">' + value.display_name + '</option>';
    });
    return html;
}

function changeDateFormat(inputDate){
    var dt = new Date(inputDate);
    if(!isNaN(dt)){
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        return day +'/'+ month + '/' + year;
    }
    else{
        return inputDate;
    }
}

function changeNumberFormat(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function RoundNum(num, length) { 
    var number = parseFloat(Math.round(num * Math.pow(10, length)) / Math.pow(10, length)).toFixed(2);
    return number;
}

function letterToNumbers(string) {
    if(string){
        string = string.toUpperCase();
    
        var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', sum = 0, i;
        for (i = 0; i < string.length; i++) {
            sum += Math.pow(letters.length, i) * (letters.indexOf(string.substr(((i + 1) * -1), 1)) + 1);
        }
        return sum;
    }
    else{
        return string;
    }
}