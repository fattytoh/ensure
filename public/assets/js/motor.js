function calcutationMotorGrandTotal(){                
    var input_amt = numberRebuild($("#order_premium_amt").val());
    var comm_charge_per = numberRebuild($("#order_commercial_use_per").val());
    var comm_charge_amt = numberRebuild($("#order_commercial_use_amt").val());
    var acc_charge_per = numberRebuild($("#order_accident_per").val());
    var acc_charge_amt = numberRebuild($("#order_accident_amt").val());
    var vehicle_load_per = numberRebuild($("#order_vehicle_load_per").val());
    var vehicle_load_amt = numberRebuild($("#order_vehicle_load_amt").val());
    var fml_charge_per = numberRebuild($("#order_female_dis_per").val());
    var fml_charge_amt = numberRebuild($("#order_female_dis_amt").val());
    var ine_charge_amt = numberRebuild($("#order_in_exp_amt").val());
    var ncd_charge_per = numberRebuild($("#order_ncd_dis_per").val());
    var ncd_charge_amt = numberRebuild($("#order_ncd_dis_amt").val());
    var ofd_charge_per = numberRebuild($("#order_ofd_dis_per").val());
    var ofd_charge_amt = numberRebuild($("#order_ofd_dis_amt").val());
    var trd_charge_amt = numberRebuild($("#order_3rd_rider_amt").val());
    var fth_charge_amt = numberRebuild($("#order_4th_rider_amt").val());
    var bank_interest_amt = numberRebuild($("#order_bank_interest").val());
    var sub_total = 0;
    var gst_charge_per = numberRebuild($("#order_gst_percent").val());
    var gst_charge_amt = 0;
    var grand_total = 0;
    var direct_dis = numberRebuild($("#order_direct_discount_amt").val());
    var add_charge = numberRebuild($("#order_addtional_amt").val());

    
    comm_charge_amt = input_amt * comm_charge_per / 100;
    $("#order_commercial_use_amt").val(numberRebuild(comm_charge_amt));

    acc_charge_amt = (parseFloat(input_amt) + parseFloat(comm_charge_amt)) * acc_charge_per / 100;
    $("#order_accident_amt").val(numberRebuild(acc_charge_amt));

    vehicle_load_amt = (parseFloat(input_amt) + parseFloat(comm_charge_amt)) * vehicle_load_per / 100;
    $("#order_vehicle_load_amt").val(numberRebuild(vehicle_load_amt));

    
    fml_charge_amt = input_amt * fml_charge_per / 100;
    $("#order_female_dis_amt").val(numberRebuild(fml_charge_amt));

    ncd_charge_amt = (parseFloat(input_amt) + parseFloat(comm_charge_amt) + parseFloat(vehicle_load_amt) + parseFloat(acc_charge_amt) + parseFloat(ine_charge_amt)) * ncd_charge_per / 100;
    $("#order_ncd_dis_amt").val(numberRebuild(ncd_charge_amt));

    ofd_charge_amt = (parseFloat(input_amt) + parseFloat(comm_charge_amt) + parseFloat(vehicle_load_amt) + parseFloat(acc_charge_amt) + parseFloat(ine_charge_amt) - parseFloat(ncd_charge_amt)) * ofd_charge_per / 100;
    $("#order_ofd_dis_amt").val(numberRebuild(ofd_charge_amt));
    
                   
    sub_total = parseFloat(input_amt) + parseFloat(acc_charge_amt) + parseFloat(vehicle_load_amt)  + parseFloat(comm_charge_amt) + parseFloat(trd_charge_amt) + parseFloat(fth_charge_amt) + parseFloat(ine_charge_amt) + parseFloat(add_charge) - parseFloat(ncd_charge_amt) - parseFloat(ofd_charge_amt) - parseFloat(fml_charge_amt);
    
    $("#order_subtotal_amount").val(changeNumberFormat(RoundNum(sub_total, 2)));

    gst_charge_amt = sub_total * (gst_charge_per / 100);
    $("#order_gst_amount").val(changeNumberFormat(RoundNum(gst_charge_amt, 2)));
    
    grand_total = parseFloat(sub_total) + parseFloat(RoundNum(gst_charge_amt,2)) - parseFloat(direct_dis) + parseFloat(bank_interest_amt);
    $("#order_grosspremium_amount").val(changeNumberFormat(RoundNum(grand_total, 2)));
    if($("#order_doc_type").val() == 'DN'){
        if(grand_total >= 0){
            $("#cus_dis_total").removeClass( "text-red" ).addClass( "text-green" );
        }
        else{
            $("#cus_dis_total").removeClass( "text-green" ).addClass( "text-red" );
        }
        $("#cus_dis_total").html(changeNumberFormat(RoundNum(grand_total, 2)));
    } else{
        if(grand_total <= 0){
            $("#cus_dis_total").removeClass( "text-red" ).addClass( "text-green" );
            $("#cus_dis_total").html("$" + changeNumberFormat(RoundNum(Math.abs(grand_total), 2)));
        }
        else{
            $("#cus_dis_total").removeClass( "text-green" ).addClass( "text-red" );
            $("#cus_dis_total").html("($" + changeNumberFormat(RoundNum(grand_total, 2)) + ")");
        }
    } 
    calculateMotorTSACompCharge();
}

function calculateMotorTSACompCharge(){
                
    var main_income = 0;
    var subtotal_amount = numberRebuild($("#order_subtotal_amount").val());
    var outpaitient = numberRebuild($("#order_addtional_amt").val());
    var govGSTper  = numberRebuild($("#order_gst_percent").val());
    var direct_discount = numberRebuild($("#order_direct_discount_amt").val());
    var bank_interest = numberRebuild($("#order_bank_interest").val());
                    
    main_income = subtotal_amount;
    var govGST = main_income * govGSTper / 100;
    
    var tsa_fee_per = numberRebuild($("#order_receivable_dealercommpercent").val());
    var direct_dis = numberRebuild($("#order_direct_discount_amt").val());
    var pay_fee_per = numberRebuild($("#order_payable_ourcommpercent").val());
    var tsa_gst_per = numberRebuild($("#order_receivable_gstpercent").val());
    var comp_gst_per = numberRebuild($("#order_payable_gstpercent").val());
    $(".span_premium").html(changeNumberFormat(RoundNum(main_income,2)));
    $(".span_bank_interest").html(changeNumberFormat(RoundNum(bank_interest,2)));
    $("#order_receivable_gstamount").val(RoundNum((main_income * tsa_gst_per / 100),2));
    $("#order_payable_gst").val(RoundNum((main_income * comp_gst_per / 100),2));
    
    var tsa_fee_amt = main_income * (tsa_fee_per / 100);
    $("#order_receivable_dealercomm").val(RoundNum(tsa_fee_amt,2));
    
    if($("#order_receivable_dealergst").val() == 'Y'){
        var tsaGST = RoundNum(govGST,2) * (tsa_gst_per / 100);
        $("#order_receivable_dealergstamount").val(RoundNum(tsaGST,2));
    }
    else{
        var tsaGST = 0;
        $("#order_receivable_dealergstamount").val(RoundNum(0,2));
    }

    var pay_fee_amt = main_income * (pay_fee_per / 100);
    $("#order_payable_ourcomm").val(RoundNum(pay_fee_amt,2));
   
    
    var nett_per = parseFloat(RoundNum(pay_fee_per,2)) - parseFloat(RoundNum(tsa_fee_per,2));
    var nett_amt = parseFloat(RoundNum(pay_fee_amt,2)) - parseFloat(RoundNum(tsa_fee_amt,2)) - parseFloat(RoundNum(direct_dis,2));
   
    $("#order_payable_nettcommpercent").val(RoundNum(nett_per,2));
    $("#discount_give").html(RoundNum(direct_dis,2));
    $("#order_payable_nettcomm").val(RoundNum(nett_amt,2));
    
    if($("#order_payable_premiumpayable").val() == 'Y'){
        var pay_gst = RoundNum((pay_fee_amt * comp_gst_per / 100), 2);
        $("#order_payable_gstcomm").val(RoundNum(pay_gst,2));
    }
    else{
        var pay_gst = 0;
        $("#order_payable_gstcomm").val(RoundNum(pay_gst,2));
    }
    
    if($("#order_record_billto").val() == "To Customer"){
        var final_total = parseFloat(RoundNum(govGST,2)) + parseFloat(RoundNum(main_income,2)) - parseFloat(RoundNum(direct_discount,2)) + parseFloat(bank_interest);
        $("#order_receivable_premiumreceivable").val(changeNumberFormat(RoundNum(final_total,2))); 
    }
    else{
        var final_left = 0;
        final_left = parseFloat(RoundNum(main_income,2)) - parseFloat(RoundNum(tsa_fee_amt,2)) - parseFloat(RoundNum(tsaGST,2)) + parseFloat(RoundNum(govGST,2)) + parseFloat(bank_interest);
        $("#order_receivable_premiumreceivable").val(changeNumberFormat(RoundNum(final_left,2))); 
    }
    
    var pay_final = parseFloat(RoundNum(main_income,2)) - parseFloat(RoundNum(pay_fee_amt,2)) - parseFloat(RoundNum(pay_gst,2)) + parseFloat(RoundNum(govGST,2)) + parseFloat(bank_interest);
    
    $("#order_payable_premium").val(changeNumberFormat(RoundNum(pay_final,2)));
   
}

function changeDateFormat(inputDate){
    var dt = new Date(inputDate);
    if(!isNaN(dt)){
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        return day +'/'+ month + '/' + year;
    }
    else{
        return inputDate;
    }
}