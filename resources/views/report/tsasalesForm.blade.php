@include('includes.header')
<div class="card">
    <div class="card-body">
        <h1 class="mb-5">Export TSA Sales Report</h1>
        <form id="report_form" action="{{ route('TSASalesReport') }}" target="_blank" method="get" enctype="multipart/form-data">
        @csrf
        <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="insurance_type">Inusrance Type</label>
                        <select name="insurance_type" id="insurance_type" class="select form-select select-hidden-accessible">
                            <option value="ALL">ALL Insurance</option>
                            <option value="MI">Motorcycle Insurance</option>
                            <option value="CMI">Commercial Motor Insurance</option>
                            <option value="PMI">Private Motor Insurance</option>
                            <option value="PCMI">Commercial And Private Motor Insurance</option>
                            <option value="GMM">Maid Insurance</option>
                            <option value="GI">General Insurance</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_insurco">Insurer</label>
                        <select name="order_insurco" id="order_insurco" class="select form-select select-hidden-accessible">
                            @foreach($insuranceco_list as $li)
                                <option value="{{ $li['insuranceco_id'] }}" >{{ $li['insuranceco_code'] ." - ".$li['insuranceco_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="period_from">Period From</label>
                        <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="period_from" name="period_from" readonly="readonly" value="{{ date('Y-m-d') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="period_to">Period To</label>
                        <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="period_to" name="period_to" readonly="readonly" value="{{ date('Y-m-d', strtotime('+1 month', strtotime( date('Y-m-d')))) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_dealer">TSA</label>
                        <select name="order_dealer" id="order_dealer" class="select2 form-select select2-hidden-accessible">
                        </select>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <input type="hidden" id="excel_export" name="excel_export" value="0">
                <!-- <button type="submit" class="btn btn-success excel_btn mr-2">Export To Excel</button> -->
                <button type="submit" class="btn btn-success pdf_btn">Export To PDF</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".excel_btn").click(function(e){
            e.preventDefault();
            $('#excel_export').val('1');
            $("#report_form").submit();
        });

        $(".pdf_btn").click(function(e){
            e.preventDefault();
            $('#excel_export').val('0');
            $("#report_form").submit();
        });

        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        $("#order_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $("#period_from").change(function(){
            var dt1 = $(this).val();
            var finalDate = dt1;
            var dt = new Date(finalDate);
            if(!isNaN(dt)){
                var day = dt.getDate();
                var month = dt.getMonth() + 2;
                var year = dt.getFullYear();

                if(month > 12){
                    month = 1;
                    year = year + parseInt(1);
                }
                
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var disdate = year + "-" + month + "-" + day;
                var new_format = changeFormDateFormat(disdate);
                $("#period_to").val(disdate);
                $("#period_to").next('input').val(new_format);
            }
        });
    })
</script>
@include('includes.footer')