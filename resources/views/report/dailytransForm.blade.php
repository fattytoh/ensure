@include('includes.header')
<div class="card">
    <div class="card-body">
        <h1 class="mb-5">Export Daily Transaction Report</h1>
        <form id="report_form" action="{{ route('DailyTransReport') }}" target="_blank" method="get" enctype="multipart/form-data">
        @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="insurance_type">Inusrance Type</label>
                        <select name="insurance_type" id="insurance_type" class="select form-select select-hidden-accessible">
                            <option value="receipt">Receipt</option>
                            <option value="payment">Payment</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="period_from">Period From</label>
                        <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="period_from" name="period_from" readonly="readonly" value="{{ date('Y-m-d') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="period_to">Period To</label>
                        <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="period_to" name="period_to" readonly="readonly" value="{{ date('Y-m-d', strtotime('+1 month', strtotime( date('Y-m-d')))) }}">
                    </div>
                </div>
            </div>
            <div class="text-right">
                <input type="hidden" id="excel_export" name="excel_export" value="0">
                <button type="submit" class="btn btn-success excel_btn mr-2">Export To Excel</button>
                <button type="submit" class="btn btn-success pdf_btn">Export To PDF</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".excel_btn").click(function(e){
            e.preventDefault();
            $('#excel_export').val('1');
            $("#report_form").submit();
        });

        $(".pdf_btn").click(function(e){
            e.preventDefault();
            $('#excel_export').val('0');
            $("#report_form").submit();
        });

        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        $("#period_from").change(function(){
            var dt1 = $(this).val();
            var finalDate = dt1;
            var dt = new Date(finalDate);
            if(!isNaN(dt)){
                var day = dt.getDate();
                var month = dt.getMonth() + 2;
                var year = dt.getFullYear();

                if(month > 12){
                    month = 1;
                    year = year + parseInt(1);
                }
                
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var disdate = year + "-" + month + "-" + day;
                var new_format = changeFormDateFormat(disdate);
                $("#period_to").val(disdate);
                $("#period_to").next('input').val(new_format);
            }
        });
    })
</script>
@include('includes.footer')