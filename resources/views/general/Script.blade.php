<script type="text/javascript" src="{{ asset('assets/js/general.js') }}"></script>
<script>
    $(document).ready(function(){
        @if(isset($order_id) && $order_id > 0)
            $(".span_premium").html(changeNumberFormat(RoundNum($("#order_grosspremium_amount").val(),2)));
            $(".span_bank_interest").html(changeNumberFormat(RoundNum($("#order_bank_interest").val(),2)));
            $("#cus_dis_total").html(changeNumberFormat(RoundNum($("#order_grosspremium_amount").val(), 2)));
        @endif

        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });
      
        $("#order_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $("#order_customer").select2({
            placeholder: "Search for a customer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('Cusselect') }}",
                dataType: 'json',
            }
        });
        
        $(".remove_worker_btn").click(function(){
            var boxid = $(this).attr('rel');
            $("#"+ boxid).remove();
        });

        $(".sum_cal").click(function(){
            calculateGeneralTotal();
        });
    })

    function addWorker(){
        var current_worker = $("#total_worker").val();
        var next_worker = parseInt(current_worker) + parseInt(1);
        $("#total_worker").val(next_worker);
        var html = '<div id="worker_box_' + next_worker + '" class="row"> <div class="col-1"> <div class="form-check mb-3"> <input type="hidden" class="form-control" name="worker_list[' + next_worker + '][worker_id]" value="0"> <input type="checkbox" class="form-check-input" id="worker_display_' + next_worker + '" name="worker_list[' + next_worker + '][worker_display]" value="1" CHECKED> <label class="form-check-label">' + next_worker + '</label> </div> </div> <div class="col-11"> <div class="row"> <div class="col-4"> <div class="form-group mb-3"> <input type="text" class="form-control" id="worker_name_' + next_worker + '" name="worker_list[' + next_worker + '][worker_name]" value="" placeholder="Worker Name"> </div> </div> <div class="col-4"> <div class="form-group mb-3"> <input type="text" class="form-control" id="worker_permit_' + next_worker + '" name="worker_list[' + next_worker + '][worker_permit]" value="" placeholder="Work Permit / FIN Number"> </div> </div> <div class="col-4"> <div class="form-group mb-3"> <input type="text" class="form-control flatpickr-input active" id="worker_dob_' + next_worker + '" name="worker_list[' + next_worker + '][worker_dob]" value="" placeholder="Date of Birth" readonly="readonly"> </div> </div> <div class="col-4"> <div class="form-group mb-3"> <input type="text" class="form-control" id="worker_occupation_' + next_worker + '" name="worker_list[' + next_worker + '][worker_occupation]" value="" placeholder="Occupation"> </div> </div> <div class="col-4"> <div class="form-group mb-3"> <input type="number" class="form-control" id="worker_wages_' + next_worker + '" name="worker_list[' + next_worker + '][worker_wages]" value="" step="0.01" placeholder="Estimate Annual Wages"> </div> </div> <div class="col-4"> <div class="form-group mb-3"> <select class="form-control select" id="worker_status_' + next_worker + '" name="worker_list[' + next_worker + '][worker_status]"> <option vaule="Added">Added</option> <option vaule="Removed">Removed</option> </select> </div> </div> <div class="col-4"> <div class="form-group mb-3"> <input type="text" class="form-control flatpickr-input active" id="worker_effdate_' + next_worker + '" name="worker_list[' + next_worker + '][worker_effdate]" value="" placeholder="Effective Date" readonly="readonly"> </div> </div> <div class="col-4 mb-3"> <button type="button" class="remove_worker_btn btn btn-danger" rel="worker_box_' + next_worker + '">Delete</button> </div> </div> </div> </div>';
        $("#worker-rows").append(html);

        $(".remove_worker_btn").click(function(){
            var boxid = $(this).attr('rel');
            $("#"+ boxid).remove();
        });

        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });
    }
</script>