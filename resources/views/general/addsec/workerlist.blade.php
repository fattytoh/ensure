<h4 class="form-group-title mb-5">Insurance Worker Involved</h4>
<input type="hidden" id="total_worker" name="total_worker" value="0">
<div class="text-right mb-3">
    <button type="button" onclick="addWorker()" class="btn btn-primary">Add Worker</button>
</div>
<div class="d-flex flex-wrap worklist-title-box">
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workername_display" name="order_workername_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workername_display">
            Name
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workpermit_display" name="order_workpermit_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workpermit_display">
            Work Permit / FIN Number
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workerdob_display" name="order_workerdob_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workerdob_display">
            Date of Birth
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workeroccup_display" name="order_workeroccup_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workeroccup_display">
            Occupation
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workerwages_display" name="order_workerwages_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workerwages_display">
            Estimate Annual Wages
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workerstatus_display" name="order_workerstatus_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workerstatus_display">
            Status
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workereffdate_display" name="order_workereffdate_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workereffdate_display">
            Effective Date
        </label>
    </div>
</div>
<div id="worker-rows" class="worker-section">
    
</div>