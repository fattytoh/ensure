<h4 class="form-group-title mb-5">Customer Fees Calculation</h4>
<div class="text-right">
    <button type="button" class="sum_cal btn btn-primary">Auto Calculate</button>
</div>
<div class="form-group mb-3">
    <label for="order_premium_amt">Basic Premium</label>
    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_premium_amt" name="order_premium_amt" value="{{ old('order_premium_amt')}}">
            @if ($errors->has('order_premium_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_premium_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_addtional_amt">Additional Charge</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right grey-read" id="order_addtional_amt" name="order_addtional_amt" value="{{ old('order_addtional_amt')}}" >
            @if ($errors->has('order_addtional_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_addtional_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_subtotal_amount">Sub Total</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_subtotal_amount" name="order_subtotal_amount" value="{{ old('order_subtotal_amount')}}" readonly="readonly">
            @if ($errors->has('order_subtotal_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_subtotal_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_gst_percent">GST</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_gst_percent" name="order_gst_percent" value="{{ old('order_gst_percent')?:$gov_gst }}">
            @if ($errors->has('order_gst_percent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_gst_percent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_gst_amount">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_gst_amount" name="order_gst_amount" value="{{ old('order_gst_amount')}}">
            @if ($errors->has('order_gst_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_gst_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_direct_discount_percentage">Direct Discount</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_direct_discount_percentage" name="order_direct_discount_percentage" value="{{ old('order_direct_discount_percentage') }}" rel_opp="order_direct_discount_amt" rel_type="per">
            @if ($errors->has('order_direct_discount_percentage'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_direct_discount_percentage') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_direct_discount_amt">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_direct_discount_amt" name="order_direct_discount_amt" value="{{ old('order_direct_discount_amt')}}" rel_opp="order_direct_discount_percentage" rel_type="amt">
            @if ($errors->has('order_direct_discount_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_direct_discount_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_bank_interest">Bank Interest</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_bank_interest" name="order_bank_interest" value="{{ old('order_bank_interest')}}">
            @if ($errors->has('order_bank_interest'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_bank_interest') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_grosspremium_amount">Grand Total</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_grosspremium_amount" name="order_grosspremium_amount" value="{{ old('order_grosspremium_amount')}}">
            @if ($errors->has('order_grosspremium_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_grosspremium_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3 text-right">
    <h3>Total : <span id="cus_dis_total" class="text-green">0.00</span></h3>
</div>
<div class="text-right mb-3">
    <button type="button" class="sum_cal btn btn-primary">Auto Calculate</button>
</div>