<h4 class="form-group-title mb-5">Insurance Basic info</h4>
<div class="row mb-3">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_record_billto">Bill To</label>
            <select name="order_record_billto" id="order_record_billto" class="select form-select select-hidden-accessible">
                <option value="TO TSA" {{ (old('order_record_billto') == 'TO TSA') ? "selected" : (($order_record_billto == 'TO TSA') ? "selected" : "") }}>To TSA</option>
                <option value="TO Customer" {{ (old('order_record_billto') == 'TO Customer') ? "selected" : (($order_record_billto == 'TO Customer') ? "selected" : "") }}>To Customer</option>
                <option value="TO Salesman" {{ (old('order_record_billto') == 'TO Salesman') ? "selected" : (($order_record_billto == 'TO Salesman') ? "selected" : "") }}>To Salesman</option>
            </select>
            @if ($errors->has('order_record_billto'))
            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_record_billto') }}</span>
            @endif
        </div>
    </div>
</div>
@if($order_ref > 0)
<div class="row mb-3">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_doc_type">Endorsement Type</label>
            <select name="order_doc_type" id="order_doc_type" class="select form-select select-hidden-accessible" {{ $disabled }}>
                <option value="DN" {{ ($order_doc_type == 'DN') ? "selected" : "" }}>Debit Notes</option>
                <option value="CN" {{ ($order_doc_type == 'CN') ? "selected" : "" }}>Credit Notes</option>
            </select>
            @if ($errors->has('order_doc_type'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_doc_type') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_endorsement_type">Endorsement Purpose</label>
            <input type="text" class="form-control" id="order_endorsement_type" name="order_endorsement_type" value="{{ old('order_endorsement_type')?:$order_endorsement_type }}" >
        </div>
    </div>
</div>
@else
<input type = "hidden" value = "{{ $order_doc_type }}" name = "order_doc_type" id="order_doc_type"/>
@endif
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_type">Type</label>
            <select name="order_type" id="order_type" class="select form-select select-hidden-accessible">
                <option value="New Case" {{ (old('order_type') == 'New Case') ? "selected" : (($order_type == 'New Case') ? "selected" : "") }}>New Case</option>
                <option value="Renewal" {{ (old('order_type') == 'Renewal') ? "selected" : (($order_type == 'Renewal') ? "selected" : "") }}>Renewal</option>
                <option value="Extend" {{ (old('order_type') == 'Extend') ? "selected" : (($order_type == 'Extend') ? "selected" : "") }}>Extend</option>
                <option value="Endorsement" {{ (old('order_type') == 'Endorsement') ? "selected" : (($order_type == 'Endorsement') ? "selected" : "") }}>Endorsement</option> 
            </select>
            @if ($errors->has('order_type'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_type') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_no">Debit / Credit Notes Number</label>
            <input type="text" class="form-control" id="order_no" name="order_no" value="{{ $order_no }}" readonly>
            @if ($errors->has('order_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_no') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_customer">Customer</label>
            <select name="order_customer" id="order_customer" class="select2 form-select select2-hidden-accessible">
            @if($order_customer  != 0)
                <option value="{{ $order_customer }}" selected>{{ $customer_name.' - '.$customer_ic }}</option>
            @endif
            </select>
            @if ($errors->has('order_customer'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_customer') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_dealer">TSA</label>
            <select name="order_dealer" id="order_dealer" class="select2 form-select select2-hidden-accessible">
            @if($order_dealer != 0)
                <option value="{{ $order_dealer }}" selected>{{ $dealer_code.' - '.$dealer_name }}</option>
            @endif
            </select>
            @if ($errors->has('order_dealer'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_dealer') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_insurco">Insurance Company</label>
            <select name="order_insurco" id="order_insurco" class="select form-select select-hidden-accessible">
                @foreach($insuranceco_list as $li)
                    <option value="{{ $li['insuranceco_id'] }}" {{ (old('order_insurco') == $li['insuranceco_id']) ? "selected" : (($order_insurco == $li['insuranceco_id']) ? "selected" : "") }} >{{ $li['insuranceco_code'] ." - ".$li['insuranceco_name']}}</option>
                @endforeach
            </select>
            @if ($errors->has('order_insurco'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_insurco') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_date">Issued Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_date" name="order_date" readonly="readonly" value="{{ old('order_date')?:$order_date }}">
            @if ($errors->has('order_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_date') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_policyno">Policy Number</label>
            <input type="text" class="form-control" id="order_policyno" name="order_policyno" value="{{ old('order_policyno')?:$order_policyno }}" >
            @if ($errors->has('order_policyno'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_policyno') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_coverage">Coverage Plan</label>
            <select name="order_coverage" id="order_coverage" class="select form-select select-hidden-accessible">
                @foreach($insuranceplan_list as $li)
                    <option value="{{ $li['insuranceclass_id'] }}" {{ (old('order_coverage') == $li['insuranceclass_id']) ? "selected" : (($order_coverage == $li['insuranceclass_id']) ? "selected" : "") }} >{{ $li['insuranceclass_code']}}</option>
                @endforeach
            </select>
            @if ($errors->has('order_coverage'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_coverage') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_from">Coverage Date From</label>
            <input type="text" class="form-control cover_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_from" name="order_period_from" value="{{ old('order_period_from')?:$order_period_from }}" readonly="readonly">
            @if ($errors->has('order_period_from'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_from') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_to">Coverage Date To</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_to" name="order_period_to" value="{{ old('order_period_to')?:$order_period_to }}" readonly="readonly">
            @if ($errors->has('order_period_to'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_to') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cancel_date">Cancel Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_cancel_date" name="order_cancel_date" value="{{ old('order_cancel_date')?:$order_cancel_date }}" readonly="readonly">
            @if ($errors->has('order_cancel_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cancel_date') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ins_doc_no">Ins Document Number</label>
            <input type="text" class="form-control" id="order_ins_doc_no" name="order_ins_doc_no" value="{{ old('order_ins_doc_no')?:$order_ins_doc_no }}" >
            @if ($errors->has('order_ins_doc_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ins_doc_no') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group mb-3">
            <label for="order_description">Description</label>
            <textarea  class="form-control" id="order_description" name="order_description" rows="10">{{ old('order_description')?:$order_description }}</textarea>
            @if ($errors->has('order_description'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_description') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group mb-3">
            <label for="order_notes_remark">Notes</label>
            <textarea  class="form-control" id="order_notes_remark" name="order_notes_remark" rows="10">{{ old('order_notes_remark')?:$order_notes_remark }}</textarea>
            @if ($errors->has('order_notes_remark'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_notes_remark') }}</span>
            @endif
        </div>
    </div>
</div>
