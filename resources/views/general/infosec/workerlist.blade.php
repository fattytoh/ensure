<h4 class="form-group-title mb-5">Insurance Worker Involved</h4>
<input type="hidden" id="total_worker" name="total_worker" value="{{ $total_worker }}">
<div class="text-right mb-3">
    <button type="button" onclick="addWorker()" class="btn btn-primary">Add Worker</button>
</div>
<div class="d-flex flex-wrap worklist-title-box">
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workername_display" name="order_workername_display" value="1" {{ ($order_workername_display == 1)?"CHECKED":"" }}> 
        <label class="form-check-label" for="order_workername_display">
            Name
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workpermit_display" name="order_workpermit_display" value="1" {{ ($order_workpermit_display == 1)?"CHECKED":"" }}> 
        <label class="form-check-label" for="order_workpermit_display">
            Work Permit / FIN Number
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workerdob_display" name="order_workerdob_display" value="1" {{ ($order_workerdob_display == 1)?"CHECKED":"" }}> 
        <label class="form-check-label" for="order_workerdob_display">
            Date of Birth
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workeroccup_display" name="order_workeroccup_display" value="1" {{ ($order_workeroccup_display == 1)?"CHECKED":"" }}> 
        <label class="form-check-label" for="order_workeroccup_display">
            Occupation
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workerwages_display" name="order_workerwages_display" value="1" {{ ($order_workerwages_display == 1)?"CHECKED":"" }}> 
        <label class="form-check-label" for="order_workerwages_display">
            Estimate Annual Wages
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workerstatus_display" name="order_workerstatus_display" value="1" {{ ($order_workerstatus_display == 1)?"CHECKED":"" }}> 
        <label class="form-check-label" for="order_workerstatus_display">
            Status
        </label>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="order_workereffdate_display" name="order_workereffdate_display" value="1" CHECKED> 
        <label class="form-check-label" for="order_workereffdate_display">
            Effective Date
        </label>
    </div>
</div>
<div id="worker-rows" class="worker-section">
    @foreach($worker_list as $wky => $wli )
    <div id="worker_box_{{ ($wky + 1) }}" class="row"> 
        <div class="col-1"> 
            <div class="form-check mb-3"> 
                <input type="hidden" class="form-control" name="worker_list[{{ ($wky + 1) }}][worker_id]" value="0"> 
                <input type="checkbox" class="form-check-input" id="worker_display_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_display]" value="1" {{ ($wli['worker_display'] == 1) ?"CHECKED":"" }}> 
                <label class="form-check-label">{{ ($wky + 1) }}</label> 
            </div> 
        </div> 
        <div class="col-11"> 
            <div class="row"> 
                <div class="col-4"> 
                    <div class="form-group mb-3"> 
                        <input type="text" class="form-control" id="worker_name_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_name]" value="{{ $wli['worker_name'] }}" placeholder="Worker Name"> 
                    </div> 
                </div> 
                <div class="col-4"> 
                    <div class="form-group mb-3"> 
                        <input type="text" class="form-control" id="worker_permit_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_permit]" value="{{ $wli['worker_permit'] }}" placeholder="Work Permit / FIN Number">
                    </div> 
                </div> 
                <div class="col-4"> 
                    <div class="form-group mb-3"> 
                        <input type="text" class="form-control flatpickr-input active" id="worker_dob_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_dob]" value="{{ $wli['worker_dob'] }}" placeholder="Date of Birth" readonly="readonly"> 
                    </div> 
                </div> 
                <div class="col-4"> 
                    <div class="form-group mb-3"> 
                        <input type="text" class="form-control" id="worker_occupation_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_occupation]" value="{{ $wli['worker_occupation'] }}" placeholder="Occupation"> 
                    </div> 
                </div> 
                <div class="col-4"> 
                    <div class="form-group mb-3"> 
                        <input type="number" class="form-control" id="worker_wages_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_wages]" value="{{ $wli['worker_wages'] }}" step="0.01" placeholder="Estimate Annual Wages"> 
                    </div> 
                </div> 
                <div class="col-4"> 
                    <div class="form-group mb-3"> 
                        <select class="form-control select" id="worker_status_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_status]"> 
                            <option vaule="Added" {{ ($wli['worker_status'] == "Added") ?"CHECKED":"" }}>Added</option> 
                            <option vaule="Removed" {{ ($wli['worker_status'] == "Removed") ?"CHECKED":"" }}>Removed</option> 
                        </select> 
                    </div> 
                </div> 
                <div class="col-4"> 
                    <div class="form-group mb-3"> 
                        <input type="text" class="form-control flatpickr-input active" id="worker_effdate_{{ ($wky + 1) }}" name="worker_list[{{ ($wky + 1) }}][worker_effdate]" value="{{ $wli['worker_effdate'] }}" placeholder="Effective Date" readonly="readonly"> 
                    </div> 
                </div> 
                <div class="col-4 mb-3"> 
                    <button type="button" class="remove_worker_btn btn btn-danger" rel="worker_box_{{ ($wky + 1) }}">Delete</button> 
                </div> 
            </div> 
        </div>
    </div>
    @endforeach
</div>