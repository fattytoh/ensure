@include('includes.header')
<div class="card">
    <div class="card-body">
         @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            There is an error occur, Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Update General Settings</h1>
        <form id="MotorForm" action="{{ route('optionUpdate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="form-group mb-3">
                <label for="motor_gst">Motor Insurance GST Setting</label>
                <input type="number" step="0.01" class="form-control" id="motor_gst" name="motor_gst" value="{{ old('motor_gst')?:$motor_gst }}" >
                @if ($errors->has('motor_gst'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('motor_gst') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="maid_gst">Maid Insurance GST Setting</label>
                <input type="number" step="0.01" class="form-control" id="maid_gst" name="maid_gst" value="{{ old('maid_gst')?:$maid_gst }}" >
                @if ($errors->has('maid_gst'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('maid_gst') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="general_gst">General Insurance GST Setting</label>
                <input type="number" step="0.01" class="form-control" id="general_gst" name="general_gst" value="{{ old('general_gst')?:$general_gst }}" >
                @if ($errors->has('general_gst'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('general_gst') }}</span>
                @endif
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
</div>
@include('includes.footer')