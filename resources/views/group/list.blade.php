@include('includes.header')
<div class="card">
<div class="card-body">    
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <div class="text-right mb-3">
        <a href="{{ route('UserGroupAddnew') }}" class="btn btn-primary">Add Group</a>
    </div> 
    <h1>Group Management</h1>
    <table id="maintable" class="table petlist-table" >
        <thead>
            <tr>
                <th>No</th>
                <th>Group Code</th>
                <th>Group Type</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($list_data as $key => $li)
            <tr>
                <td>{{ ($key+1) }}</td>
                <td>{{ $li['group_code'] }}</td>
                <td>{{ $li['group_type'] }}</td>
                <td>{{ ($li['group_status'] == 1)?'ACTIVE':'IN-ACTIVE' }}</td>
                <td>
                    <a href="{{ route('UserGroupInfo', ['id' => $li['group_id'] ]) }}" class="btn btn-warning mr-2 mb-2">Edit</a>
                    <a href="{{ route('UserGroupDel', ['id' => $li['group_id'] ]) }}" class="btn btn-danger del_btn" rel="{{ $li['group_code'] }}">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>
@include('includes.footer')