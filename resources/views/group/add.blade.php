@include('includes.header')
<div class="card">
    <div class="card-body">
        <h1>Add New Group</h1>
        <form id="UsrGrpForm" action="{{ route('UserGroupCreate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="form-group mb-3">
                <label for="group_code">Group Code</label>
                <input type="text" class="form-control" id="group_code" name="group_code" value="{{ old('group_code') }}" >
                @if ($errors->has('group_code'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('group_code') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="group_type">Group Type</label>
                <select name="group_type" id="group_type" class="select form-select select-hidden-accessible">
                    <option value="EMPL">Employee</option>
                    <option value="TSA">TSA</option>
                </select>
                @if ($errors->has('group_type'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('group_type') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="group_desc">Group Description</label>
                <textarea class="form-control" id="group_desc" name="group_desc">{{ old('group_desc')}}</textarea>
                @if ($errors->has('group_desc'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('group_desc') }}</span>
                @endif
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-success">Add Group</button>
            </div>
            <hr>
            <h4>Privilege List</h4>
            <hr>
            @foreach($privilege_list as $pri_ky => $pri_data)
                <h5 class="text-center">{{ $pri_data['name'] }}</h5>
                <div class="form-check mb-3">
                    <input class="form-check-input all_check" type="checkbox" value="{{ $pri_ky }}" id="{{ $pri_ky }}_all">
                    <label class="form-check-label" for="{{ $pri_ky }}_all">
                        Check / Cncheck All {{ $pri_data['name'] }}
                    </label>
                </div>
                <hr>
                @foreach($pri_data['list'] as $sub_pri_ky => $sub_pri_data)
                <div class="px-3">
                    <div class="form-check mb-3">
                        <input class="form-check-input {{ $pri_ky }}_sub" type="checkbox" value="{{ $sub_pri_ky }}" id="{{ $sub_pri_ky }}" name="privilege_box[]">
                        <label class="form-check-label" for="{{ $sub_pri_ky }}">
                        {{ $sub_pri_data }}
                        </label>
                    </div>
                </div>
                @endforeach
                <hr>
            @endforeach
            <div class="text-right">
                <button type="submit" class="btn btn-success">Add Group</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".all_check").change(function(){
            var cls = $(this).val();
            if ($(this).prop("checked")) {
                $('.'+cls+"_sub").prop("checked", true);

            } else {
                $('.'+cls+"_sub").prop("checked", false);
            }
        })

        $("#chek_pri_all").change(function(){
            if ($(this).prop("checked")) {
                $('.form-check-input').prop("checked", true);

            } else {
                $('.form-check-input').prop("checked", false);
            }
        })
    })
</script>
@include('includes.footer')