@include('includes.header')
@inject('provider', 'App\Http\Controllers\AllianzController')
<div class="card">
<div class="card-body">
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <div class="text-right mb-3">
        @PrivilegeCheck('PaymentAddNew')
        <a href="{{ route('PaymentAddNew') }}" class="btn btn-primary">Add Customer Payment</a>
        @endPrivilegeCheck
    </div> 
    <h1>Payment Management</h1>
    <table id="paymentTable" class="table petlist-table" >
        <thead>
            <tr>
                <th>No</th>
                <th>Payment No</th>
                <th>Payment Date</th>
                <th>Customer</th>
                <th>Bank</th>
                <th>Amount</th>
                <th>Insert By</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
<script>
    $(document).ready(function(){
        var pay_dt_table = $('#paymentTable');
        pay_dt = pay_dt_table.DataTable({
            ajax: {
                url: "{{ route('PaymentListData') }}",
                type: "POST",
                'data': {
                }
            },
            processing: true,
            serverSide: true,
            columnDefs: [
                { orderable: false, targets: 0 },
                { orderable: false, targets: -1 }
            ]
        });
    })
</script>
@include('includes.footer')