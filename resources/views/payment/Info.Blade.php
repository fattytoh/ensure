@include('includes.header')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            There is an error occur, Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Update Customer Payment</h1>
        <form id="PaymentForm" action="{{ route('PaymentUpdate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="payment_no">Payment No</label>
                        <input type="text" class="form-control" id="payment_no" name="payment_no" value="{{ $payment_no }}" readonly>
                        @if ($errors->has('payment_no'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_no') }}</span>
                        @endif
                    </div>
                    <div id="payment_customer_box" class="form-group mb-3">
                        <label for="payment_customer">Customer</label>
                        <select name="payment_customer" id="payment_customer" class="select2 form-select select2-hidden-accessible" {{ $disabled }}>
                            @if(old('payment_customer'))
                                {!! app('App\Http\Controllers\SelectController')->getCustomerOption(old('payment_customer')) !!}
                            @elseif($payment_customer  != 0)
                                <option value="{{ $payment_customer }}" selected>{{ $customer_name.' - '.$customer_ic }}</option>
                            @endif
                        </select>
                        @if ($errors->has('payment_customer'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_customer') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="payment_method">Payment Method</label>
                        <select name="payment_method" id="payment_method" class="select form-select select-hidden-accessible" {{ $disabled }}>
                            <option value="cheque" {{ (old('payment_method') == 'cheque') ? "selected" : (($payment_method == 'cheque') ? "selected" : "") }}>CHEQUE</option>
                            <option value="paynow" {{ (old('payment_method') == 'paynow') ? "selected" : (($payment_method == 'paynow') ? "selected" : "") }}>PAYNOW</option>
                            <option value="cash" {{ (old('payment_method') == 'cash') ? "selected" : (($payment_method == 'cash') ? "selected" : "") }}>CASH</option>
                            <option value="creditcard" {{ (old('payment_method') == 'creditcard') ? "selected" : (($payment_method == 'creditcard') ? "selected" : "") }}>CREDIT CARD</option>
                            <option value="nets" {{ (old('payment_method') == 'nets') ? "selected" : (($payment_method == 'nets') ? "selected" : "") }}>NETS</option>
                            <option value="contra" {{ (old('payment_method') == 'contra') ? "selected" : (($payment_method == 'contra') ? "selected" : "") }}>CONTRA</option>
                            <option value="ibanking" {{ (old('payment_method') == 'ibanking') ? "selected" : (($payment_method == 'ibanking') ? "selected" : "") }}>IBANKING</option>
                            <option value="chequereturn" {{ (old('payment_method') == 'chequereturn') ? "selected" : (($payment_method == 'chequereturn') ? "selected" : "") }}>CHEQUE RETURN</option>
                            <option value="ccdecline" {{ (old('payment_method') == 'ccdecline') ? "selected" : (($payment_method == 'ccdecline') ? "selected" : "") }}>CC DECLINE</option>
                            <option value="direct" {{ (old('payment_method') == 'direct') ? "selected" : (($payment_method == 'direct') ? "selected" : "") }}>Direct To Insurance Company</option>
                        </select>
                        @if ($errors->has('payment_method'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_method') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="payment_remarks">Remarks</label>
                        <textarea  class="form-control" id="payment_remarks" name="payment_remarks" rows="3" {{ $disabled }}>{{ old('payment_remarks')?:$payment_remarks }}</textarea>
                        @if ($errors->has('payment_remarks'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_remarks') }}</span>
                        @endif
                    </div>
                </div> 
                <div class="col-md-6"> 
                    <div class="form-group mb-3">
                        <label for="payment_date">Payment Date</label>
                        <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="payment_date" name="payment_date" readonly="readonly" value="{{ old('payment_date')?:$payment_date }}" {{ $disabled }}>
                        @if ($errors->has('payment_date'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_date') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="payment_bank">Bank Name </label>
                        <input type="text" class="form-control" id="payment_bank" name="payment_bank" value="{{ old('payment_bank')?:$payment_bank }}" {{ $disabled }}>
                        @if ($errors->has('payment_bank'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_bank') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="payment_cheque">Cheque No </label>
                        <input type="text" class="form-control" id="payment_cheque" name="payment_cheque" value="{{ old('payment_cheque')?:$payment_cheque }}" {{ $disabled }}>
                        @if ($errors->has('payment_cheque'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_cheque') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="payment_paid">Payment Amount </label>
                        <input type="number" step="0.01" class="form-control text-right" id="payment_paid" name="payment_paid" value="{{ old('payment_paid')?:$payment_paid }}" {{ $disabled }}>
                        @if ($errors->has('payment_paid'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('payment_paid') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="payment_cheque">File Upload</label>
                        <input type="file" class="form-control" id="payment_files" name="attachment[]" multiple {{ $disabled }}>
                        @if ($errors->has('attachment'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('attachment') }}</span>
                        @endif
                    </div>
                </div>
            </div> 
            <div class="text-right">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            @include('payment.infosec.fileList')  
            @include('payment.infosec.orderList')  
            <div class="text-right mt-3">
                <input type="hidden" name="payment_to" value="Customer">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
</div>
@include('payment.Script')  
@include('includes.footer')