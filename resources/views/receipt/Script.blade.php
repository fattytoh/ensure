<script>
    $(document).ready(function(){
        @if ($errors->any())
            calTotal()
        @endif

        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        orderTable();

        $("#receipt_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $("#receipt_customer").select2({
            placeholder: "Search for a customer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('Cusselect') }}",
                dataType: 'json',
            }
        });

        @if(old('receipt_type') == 'TSA' || (isset($receipt_type) &&  $receipt_type == 'TSA'))
            $("#receipt_dealer_box").removeClass("d-none");
            $("#receipt_customer_box").addClass("d-none");
            $("#receipt_insuranco_box").addClass("d-none");
        @elseif(old('receipt_type') == 'Customer' || (isset($receipt_type) &&  $receipt_type == 'Customer'))
            $("#receipt_dealer_box").addClass("d-none");
            $("#receipt_customer_box").removeClass("d-none");
            $("#receipt_insuranco_box").addClass("d-none");
        @endif

        $("#receipt_type").change(function(){
            $(".info_order_wrapper").remove();
            if($(this).val() == "TSA"){
                $("#receipt_dealer_box").removeClass("d-none");
                $("#receipt_customer_box").addClass("d-none");
                $("#receipt_insuranco_box").addClass("d-none");
                $("#receipt_customer").val('').trigger('change');
                $("#receipt_insuranco").val('').trigger('change');
            }
            else{
                $("#receipt_dealer_box").addClass("d-none");
                $("#receipt_customer_box").removeClass("d-none");
                $("#receipt_insuranco_box").addClass("d-none");
                $("#receipt_dealer").val('').trigger('change');
                $("#receipt_insuranco").val('').trigger('change');
            }
           
            orderTable();
        });

        $("#receipt_dealer").change(function(){
            orderTable();
        });

        $("#receipt_customer").change(function(){
            orderTable();
        });

        $("#receipt_insuranco").change(function(){
            orderTable();
        });

        $("#filterOrderBtn").click(function(){
            orderTable();
        });

        $(".del_ord_btn").click(function (){
            var ord_id = $(this).attr('rel');
            delOrderBtn(ord_id);
        });

        $(".offset_amt").keyup(function(){
            calTotal();
        });

        $("#addOrderBtn").click(function(){
            
            var input_post = new Array();
            $("input[name='order_case[]']:checked").each(function(i) {
                input_post.push($(this).val());
            });
            var old_post = new Array();
            $(".order_id_input").each(function(i) {
                old_post.push($(this).val());
            });

            $.ajax({
                type: "POST",
                url: "{{ route('ReceiptAddOrder') }}",
                dataType: 'json',
                data:  {
                    receipt_type : $("#receipt_type").val(),
                    order_case : input_post,
                    old_post : old_post,
                },
                success: function(res) {
                    if(res.html){
                        $("#order_list_wrapper").append(res.html);
                    }
                    $(".del_ord_btn").click(function (){
                        var ord_id = $(this).attr('rel');
                        delOrderBtn(ord_id);
                    });

                    $(".offset_amt").keyup(function(){
                        calTotal();
                    });

                    calTotal();
                }
            })
        });
    });

    function delOrderBtn(order){
        $("#order_box_" + order).remove();
        calTotal();
    }

    function calTotal(){
        var total = 0;
        $(".offset_amt").each(function(i) {
            total = parseFloat(total) +  parseFloat($(this).val());
        });

        $("#total_offset").val(total);
        $("#receipt_paid").val(total);
    }

    function orderTable(){
        $('#OrderTable').DataTable().clear().destroy();
        $("#OrderTable").DataTable({
            ajax: {
                url: "{{ route('ReceiptOrderListData') }}",
                type: "POST",
                'data': {
                    receipt_type : $("#receipt_type").val(),
                    receipt_dealer : $("#receipt_dealer").val(),
                    receipt_customer : $("#receipt_customer").val(),
                    fil_start_date : $("#fil_start_date").val(),
                    fil_end_date : $("#fil_end_date").val(),
                    fil_order_type : $("#fil_order_type").val(),
                    fil_policyno : $("#fil_policyno").val(),
                }
            },
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
        });
    }
</script>