@include('includes.header')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            There is an error occur, Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Add Receipt</h1>
        <form id="ReceiptForm" action="{{ route('ReceiptCreate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="receipt_no">Receipt No</label>
                        <input type="text" class="form-control" id="receipt_no" name="receipt_no" value="-- System Generate --" readonly>
                        @if ($errors->has('receipt_no'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_no') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_type">Receipt To</label>
                        <select name="receipt_type" id="receipt_type" class="select form-select select-hidden-accessible">
                            <option value="TSA" {{ (old('receipt_type') == 'TSA') ? "selected" : "" }}>To TSA</option>
                            <option value="Customer" {{ (old('receipt_type') == 'Customer') ? "selected" : "" }}>To Customer</option>
                        </select>
                        @if ($errors->has('receipt_type'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_type') }}</span>
                        @endif
                    </div>
                    <div id="receipt_dealer_box" class="form-group mb-3">
                        <label for="receipt_dealer">TSA</label>
                        <select name="receipt_dealer" id="receipt_dealer" class="select2 form-select select2-hidden-accessible">
                            @if(old('receipt_dealer'))
                                {!! app('App\Http\Controllers\SelectController')->getTSAOption(old('receipt_dealer')) !!}
                            @endif
                        </select>
                        @if ($errors->has('receipt_dealer'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_dealer') }}</span>
                        @endif
                    </div>
                    <div id="receipt_customer_box" class="form-group mb-3 d-none">
                        <label for="receipt_customer">Customer</label>
                        <select name="receipt_customer" id="receipt_customer" class="select2 form-select select2-hidden-accessible">
                            @if(old('receipt_customer'))
                                {!! app('App\Http\Controllers\SelectController')->getCustomerOption(old('receipt_customer')) !!}
                            @endif
                        </select>
                        @if ($errors->has('receipt_customer'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_customer') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_method">Payment Method</label>
                        <select name="receipt_method" id="receipt_method" class="select form-select select-hidden-accessible">
                            <option value="cheque" {{ (old('receipt_method') == 'cheque') ? "selected" : "" }}>CHEQUE</option>
                            <option value="paynow" {{ (old('receipt_method') == 'paynow') ? "selected" : "" }}>PAYNOW</option>
                            <option value="cash" {{ (old('receipt_method') == 'cash') ? "selected" : "" }}>CASH</option>
                            <option value="creditcard" {{ (old('receipt_method') == 'creditcard') ? "selected" : "" }}>CREDIT CARD</option>
                            <option value="nets" {{ (old('receipt_method') == 'nets') ? "selected" : "" }}>NETS</option>
                            <option value="contra" {{ (old('receipt_method') == 'contra') ? "selected" : "" }}>CONTRA</option>
                            <option value="ibanking" {{ (old('receipt_method') == 'ibanking') ? "selected" : "" }}>IBANKING</option>
                            <option value="chequereturn" {{ (old('receipt_method') == 'chequereturn') ? "selected" : "" }}>CHEQUE RETURN</option>
                            <option value="ccdecline" {{ (old('receipt_method') == 'ccdecline') ? "selected" : "" }}>CC DECLINE</option>
                            <option value="direct" {{ (old('receipt_method') == 'direct') ? "selected" : "" }}>Direct To Insurance Company</option>
                        </select>
                        @if ($errors->has('receipt_method'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_method') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_remarks">Remarks</label>
                        <textarea  class="form-control" id="receipt_remarks" name="receipt_remarks" rows="3">{{ old('receipt_remarks') }}</textarea>
                        @if ($errors->has('receipt_remarks'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_remarks') }}</span>
                        @endif
                    </div>
                </div> 
                <div class="col-md-6"> 
                    <div class="form-group mb-3">
                        <label for="receipt_date">Receipt Date</label>
                        <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="receipt_date" name="receipt_date" readonly="readonly" value="{{ old('receipt_date') }}">
                        @if ($errors->has('receipt_date'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_date') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_bank">Bank Name </label>
                        <input type="text" class="form-control" id="receipt_bank" name="receipt_bank" value="{{ old('receipt_bank')}}" >
                        @if ($errors->has('receipt_bank'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_bank') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_cheque">Cheque No </label>
                        <input type="text" class="form-control" id="receipt_cheque" name="receipt_cheque" value="{{ old('receipt_cheque')}}" >
                        @if ($errors->has('receipt_cheque'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_cheque') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_received">Received Amount </label>
                        <input type="number" step="0.01" class="form-control text-right" id="receipt_received" name="receipt_received" value="{{ old('receipt_received')}}" >
                        @if ($errors->has('receipt_received'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_received') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_paid">Receipt Amount </label>
                        <input type="number" step="0.01" class="form-control text-right" id="receipt_paid" name="receipt_paid" value="{{ old('receipt_paid')}}" >
                        @if ($errors->has('receipt_paid'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_paid') }}</span>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <label for="receipt_cheque">File Upload</label>
                        <input type="file" class="form-control" id="receipt_files" name="receipt_files[]" multiple>
                        @if ($errors->has('receipt_files'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('receipt_files') }}</span>
                        @endif
                    </div>
                </div>
            </div> 
            <div class="text-right">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            @include('receipt.addsec.orderList')  
            <div class="text-right mt-3">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
</div>
@include('receipt.Script')  
@include('includes.footer')