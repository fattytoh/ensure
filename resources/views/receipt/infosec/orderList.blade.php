<h4 class="form-group-title mb-5 mt-3">Order Listing</h4>
<h4 class="mb-3">Filter</h4>
<div class="row">
    <div class="col-md-4">
        <div class="form-group mb-3">
            <label for="fil_start_date">Start Date</label>
            <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="fil_start_date" name="fil_start_date" readonly="readonly" value="">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group mb-3">
            <label for="fil_end_date">End Date</label>
            <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="fil_end_date" name="fil_end_date" readonly="readonly" value="">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group mb-3">
            <label for="fil_policyno">Policy No</label>
            <input type="text" class="form-control" id="fil_policyno" name="fil_policyno" value="" >
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group mb-3"> 
            <label for="fil_order_type">Insurance Type</label>
            <select name="fil_order_type" id="fil_order_type" class="select form-select select-hidden-accessible">
                <option value="ALL" >All</option>
                <option value="CY" >Motorcycle Insurance</option>
                <option value="MP" >Private Motor Insurance</option>
                <option value="MT" >Commercial Motorcycle Insurance</option>
                <option value="GI" >Maid Insurance</option>
                <option value="GM" >General Insurance</option>
            </select>
        </div>
    </div>
</div>
<div class="text-right">
    <button type="button" id="filterOrderBtn" class="btn btn-primary">Filter</button>
</div>
<table id="OrderTable" class="table petlist-table">
    <thead>
        <tr>
            <th>No</th>
            <th>Coverage Plan</th>
            <th>Related Info</th>
            <th>Insured Name</th>
            <th>Debit Note</th>
            <th>Policy No</th>
            <th>Amount List</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div class="text-right my-3">
    <button type="button" id="addOrderBtn" class="btn btn-primary">Add Order To Receipt</button>
</div>
<h4 class="form-group-title mb-5">Add Case List</h4>
<div id="order_list_wrapper" class="order_wrapper">
    @if(old('receipt_list'))
        @foreach (old('receipt_list') as $row)
            {!! app('App\Http\Controllers\ReceiptController')->getOldCaseList($row['order_id'], $row['balance'], $row['offset_amount'], $row['order_amount'], $row['recpline_id'], old('receipt_type')) !!}
        @endforeach
    @else
        @foreach($receipt_list as $pli)
            <div id="order_box_{{ $pli['order_id'] }}" class="row order_wrapper info_order_wrapper">
                <div class="col-md-3">
                    Coverage : {{ $pli['coverage'] }}<br>
                    Related Info : {{ $pli['related']?:'-' }}<br>
                    Issue Name : {{ $pli['customer_name'] }}
                </div>
                <div class="col-md-3">
                    Order No : {!! $pli['order_no'] !!}<br>
                    Policy Number : {{ $pli['policy'] }}
                </div>
                <div class="col-md-3">
                    {!! $pli['amount_list'] !!}
                    @if(!$pli['is_view'])
                    <div class="mt-3">
                        <button type="button" class="del_ord_btn btn btn-danger" rel="{{ $pli['order_id'] }}">Delete</button>
                    </div>
                    @endif
                </div>
                <div class="col-md-3">
                    <input type="hidden" name="receipt_list[{{ $pli['order_id'] }}][recpline_id]" value="{{ $pli['recpline_id'] }}">
                    <input type="hidden" name="receipt_list[{{ $pli['order_id'] }}][order_id]" class="order_id_input" value="{{ $pli['order_id'] }}">
                    <input type="hidden" name="receipt_list[{{ $pli['order_id'] }}][balance]" value="{{ $pli['balance'] }}">
                    <input type="hidden" name="receipt_list[{{ $pli['order_id'] }}][order_amount]" value="{{ $pli['total'] }}">
                    <input type="number" step="0.01" class="form-control offset_amt text-right" name="receipt_list[{{ $pli['order_id'] }}][offset_amount]" value="{{ $pli['offset_amt'] }}" {{ $disabled }}>
                </div>
            </div>
        @endforeach
    @endif
</div>
<div class="row">
    <div class="col-md-9 text-right">
        Total :
    </div>
    <div class="col-md-3">
        <input type="text" disabled class="form-control text-right" id="total_offset" value="{{ $receipt_paid }}">
    </div>
</div>