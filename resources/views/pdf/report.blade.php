<!DOCTYPE html>
<html>

<head>
    <title>{{ isset($title)?$title:'Report PDF'}}</title>
    <style>
        @page{
            margin-top: 120px;
            margin-bottom: 60px;
            font-size:8px;
            font-family:'Helvetica';
        }
        header{
            position: fixed;
            padding-top:10px;
            left: 0px;
            right: 0px;
            height: 120px;
            margin-top: -120px;
        }
        footer{
            position: fixed;
            left: 0px;
            right: 0px;
            height: 50px;
            bottom: 0px;
            margin-bottom: -60px;
        }

        footer strong{
            font-size: 8px;
            font-style: italic;
        }
       
        .title-box{
            text-align:center;
            padding-bottom: 10px;
            border-top: 2px solid black;
            border-bottom: 2px solid black;
        }
        .title-box h5{
            margin-bottom:0px;
        }

        .my-10{
            padding-top:10px;
            padding-bottom:10px;
        }

        .data-table{
            width: 100%;
            margin-bottom: 10px;
            border-spacing: 0px;
            table-layout:fixed;
        }

        .data-table td{
            padding: 5px;
        }

        .data-table thead td{
            font-weight: 900;
            font-family:'Helvetica';
        } 

        .data-table tbody tr.orange td{
            background-color:#DCDCC8;
        }

        .tsa-info{
            font-size: 9px;
            margin-bottom: 10px;
        }

        .tsa-info strong{
            font-size: 12px;
        }

        .subtotal-tr td{
            border-top: 2px solid #000;
        }
    </style>
</head>
<body>
    
    <header>
        <table style='width:100%;padding-bottom:10px;border-bottom:1px solid #000;'>
            <tr>
                <td style='text-align:left;width:70%;font-size:12px;'>
                    TSA REFERRAL FEES REPORT<br>
                    Period : {{ $filter_date }}<br>
                    Filter By : {{ $filter_type }}
                </td>
                <td style='text-align:right;width:30%;font-size:12px;'>
                    @if (env('APP_ID')=='2')
                    <img src="{{public_path('assets/images/ensure-logo.png')}}" style="width:120px;max-width:100%;">
                    @else
                    <img src="{{public_path('assets/images/people-logo.png')}}" style="width:120px;max-width:100%;">
                    @endif
                </td>
            </tr>
        </table>
    </header>
    <footer style="text-align:right;font-size:6px;padding-top:10px;border-top:1px solid #000;">
        @if (env('APP_ID')=='2')
        <strong>ENSURE PTE LTD</strong><br>
        38 TOH GUAN ROAD EAST, #01-57 ENTERPRISE HUB , SINGAPORE 608581 FAX: +65 6896 6321 TEL: +65 6515 5988 EMAIL: enquiry@ens.com.sg
        @else
        <strong>PEOPLES INSURANCE AGENCY PTE LTD</strong><br>
        38 TOH GUAN ROAD EAST, #03-57 ENTERPRISE HUB , SINGAPORE 608581 FAX: +65 6896 6321 TEL: +65 6515 5778 EMAIL: enquiry@peoples.com.sg
        @endif
    </footer>
    <main>
        @php
            $grand_total = [
                'order_gross_premium' => 0,
                'order_receivable_premiumreceivable' => 0,
                'order_receivable_dealercomm' => 0,
                'order_receivable_dealergstamount' => 0,
                'total_tsa_comm' => 0,
            ];
        @endphp
        @foreach($listing_data as $list_dt)
        <div class="tsa-info">
            <strong>{{ $list_dt['dealer_info']['dealer_name'] }}</strong><br>
            @if(isset($list_dt['dealer_info']['dealer_blk_no']))
                {{ strtoupper($list_dt['dealer_info']['dealer_blk_no']." ".$list_dt['dealer_info']['dealer_street']) }}<br>
            @endif
            @if(isset($list_dt['dealer_info']['dealer_unit_no']))
                {{ strtoupper( $list_dt['dealer_info']['dealer_unit_no']) }}<br>
            @endif
            @if(isset($list_dt['dealer_info']['dealer_postalcode']))
                {{  strtoupper("SINGAPORE ".$list_dt['dealer_info']['dealer_postalcode']) }}
            @endif
        </div>
        <table class="data-table">
            <thead>
                <tr>
                    @foreach($header as $hkey => $hli)
                    @if(isset($hli['position']) && $hli['position'] == 'R')
                    <td style="text-align:right;width:{{ $hli['width'] }}px;" >{{ $hkey }}</td>
                    @else
                    <td style="width:{{ $hli['width'] }}px;"  >{{ $hkey }}</td>
                    @endif
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @php
                    $count = 0;
                    $sub_total = [
                        'order_gross_premium' => 0,
                        'order_receivable_premiumreceivable' => 0,
                        'order_receivable_dealercomm' => 0,
                        'order_receivable_dealergstamount' => 0,
                        'total_tsa_comm' => 0,
                    ];
                @endphp
                @foreach($list_dt['order_listing'] as $od_ky => $od_dt)
                @if($count % 2 == 0)
                <tr>
                @else
                <tr class="orange">
                @endif
                    @foreach($header as $hkey => $hli)
                        @php
                        if(isset($hli['param']) && isset($sub_total[$hli['param']])){
                            if($od_dt['order_doc_type'] == 'CN'){
                                $sub_total[$hli['param']] -= (isset($od_dt[$hli['param']])?$od_dt[$hli['param']]:0);
                                $grand_total[$hli['param']] -= (isset($od_dt[$hli['param']])?$od_dt[$hli['param']]:0);
                            }
                            else{
                                $sub_total[$hli['param']] += (isset($od_dt[$hli['param']])?$od_dt[$hli['param']]:0);
                                $grand_total[$hli['param']] += (isset($od_dt[$hli['param']])?$od_dt[$hli['param']]:0);
                            }
                        }
                        @endphp
                        @if(isset($hli['param']))
                            @if(isset($hli['position']) && $hli['position'] == 'R')
                                @if(isset($hli['number']) && $hli['number'] === true)
                                <td  style="text-align:right;width:{{ $hli['width'] }}px;" > {!! app('App\Http\Controllers\ReportPDFController')->numberFormat($od_dt[$hli['param']], $od_dt['order_doc_type']) !!}</td>
                                @else
                                <td  style="text-align:right;width:{{ $hli['width'] }}px;" >{{ isset($od_dt[$hli['param']])?$od_dt[$hli['param']]:'-' }}</td>
                                @endif
                            @else
                                @if(isset($hli['number']) && $hli['number'] === true)
                                <td style="width:{{ $hli['width'] }}px;">{!! app('App\Http\Controllers\ReportPDFController')->numberFormat($od_dt[$hli['param']], $od_dt['order_doc_type']) !!}</td>
                                @else
                                <td style="width:{{ $hli['width'] }}px;" >{{ isset($od_dt[$hli['param']])?$od_dt[$hli['param']]:'-' }}</td>
                                @endif
                            @endif
                        @else
                        <td style="width:{{ $hli['width'] }}px;" >{{ $count + 1 }}</td>
                        @endif
                    @endforeach
                </tr>
                @php($count ++)
                @endforeach
                <tr class="subtotal-tr">
                @foreach($header as $hkey => $hli)
                    @if(!isset($hli['param']))
                    <td style="width:{{ $hli['width'] }}px;"><strong> Sub Total :</strong></td>
                    @else
                        @if(isset($sub_total[$hli['param']]))
                            <td style="text-align:right;width:{{ $hli['width'] }}px;" ><strong>{!! app('App\Http\Controllers\ReportPDFController')->numberTotalFormat($sub_total[$hli['param']]) !!}</strong></td>
                        @else
                            <td style="width:{{ $hli['width'] }}px;"></td>
                        @endif
                    @endif
                @endforeach
                </tr>
            </tbody>
        </table>
        @endforeach
        <table class="data-table">
            <tbody>
                <tr class="subtotal-tr">
                @foreach($header as $hkeys => $hlis)
                    @if(!isset($hlis['param']))
                    <td style="width:{{ $hlis['width'] }}px;"><strong> Grand Total :</strong></td>
                    @else
                        @if(isset($grand_total[$hlis['param']]))
                            <td style="text-align:right;width:{{ $hlis['width'] }}px;" ><strong>{!! app('App\Http\Controllers\ReportPDFController')->numberTotalFormat($grand_total[$hlis['param']]) !!}</strong></td>
                        @else
                            <td style="width:{{ $hlis['width'] }}px;"></td>
                        @endif
                    @endif
                @endforeach
                </tr>
            </tbody>
        </table>
    </main>
</body>
<html>