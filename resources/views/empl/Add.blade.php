@include('includes.header')
<div class="card">
<div class="card-body">
    <h1>Add New Employee</h1>
    <form id="EmplForm" action="{{ route('EmplCreate') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group mb-3">
            <label for="empl_code">Code</label>
            <span class="form-control" id="empl_code" name="empl_code" >-- System Generate --</span>
        </div>
        <div class="form-group mb-3">
            <label for="empl_name">Name</label>
            <input type="text" class="form-control" id="empl_name" name="empl_name" value="{{ old('empl_name') }}" >
            @if ($errors->has('empl_name'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_name') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_outlet">Default Company</label>
            <select id="empl_outlet" name="empl_outlet" class="select form-select select-hidden-accessible">
                @foreach($cprofile_list as $cpli)
                <option value="{{ $cpli['cprofile_id'] }}" {{ (old('empl_outlet') == $cpli['cprofile_id']) ? "selected" : "" }}>{{ $cpli['cprofile_name'] }}</option>
                @endforeach
            </select>
            @if ($errors->has('empl_outlet'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_outlet') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_group">Group</label>
            <select id="empl_group" name="empl_group" class="select form-select select-hidden-accessible">
                @foreach($group_list as $gli)
                <option value="{{ $gli['group_id'] }}" {{ (old('empl_group') == $gli['group_id']) ? "selected" : "" }}>{{ $gli['group_code'] }}</option>
                @endforeach
            </select>
            @if ($errors->has('empl_group'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_group') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_department">Department</label>
            <select id="empl_department" name="empl_department" class="select form-select select-hidden-accessible">
                @foreach($department_list as $dli)
                <option value="{{ $dli['department_id'] }}" {{ (old('empl_department') == $dli['department_id']) ? "selected" : "" }}>{{ $dli['department_code'] }}</option>
                @endforeach
            </select>
            @if ($errors->has('empl_department'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_department') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_status">Status</label>
            <select id="empl_status" name="empl_status" class="select form-select select-hidden-accessible">
                <option value="1">ACTIVE</option>
                <option value="0" {{ ( old('empl_status') && old('empl_status') == 0  ) ? "selected" : "" }}>IN-ACTIVE</option>
            </select>
            @if ($errors->has('empl_status'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_status') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_email">Email</label>
            <input type="text" class="form-control" id="empl_email" name="empl_email" value="{{ old('empl_email') }}" >
            @if ($errors->has('empl_email'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_email') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_login_email">Login Email</label>
            <input type="text" class="form-control" id="empl_login_email" name="empl_login_email" value="{{ old('empl_login_email') }}" >
            @if ($errors->has('empl_login_email'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_login_email') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" >
            @if ($errors->has('password'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('password') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="cfm_password">Confirm Password</label>
            <input type="password" class="form-control" id="cfm_password" name="cfm_password" placeholder="Confirm Password" value="" >
            @if ($errors->has('cfm_password'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('cfm_password') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_mobile">Mobile</label>
            <input type="text" class="form-control" id="empl_mobile" name="empl_mobile" value="{{ old('empl_mobile') }}" >
            @if ($errors->has('empl_mobile'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_mobile') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_tel">Tel</label>
            <input type="text" class="form-control" id="empl_tel" name="empl_tel" value="{{ old('empl_tel') }}" >
            @if ($errors->has('empl_tel'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_tel') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_birthday">DOB</label>
            <input type="text" class="form-control flatpickr-input active" placeholder="YYYY-MM-DD" id="empl_birthday" name="empl_birthday" value="{{ old('empl_birthday') }}" readonly="readonly">
            @if ($errors->has('empl_birthday'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_birthday') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_joindate">Join Date</label>
            <input type="text" class="form-control flatpickr-input active" placeholder="YYYY-MM-DD" id="empl_joindate" name="empl_joindate" value="{{ old('empl_joindate') }}" readonly="readonly">
            @if ($errors->has('empl_joindate'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_joindate') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_address">Address</label>
            <textarea class="form-control" rows="5" id="empl_address" name="empl_address">{{ old('empl_address')}}</textarea>
            @if ($errors->has('empl_address'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_address') }}</span>
            @endif
        </div>
        <div class="form-group mb-3">
            <label for="empl_remark">Remark</label>
            <textarea class="form-control" rows="5" id="empl_remark" name="empl_remark">{{ old('empl_remark')}}</textarea>
            @if ($errors->has('empl_remark'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('empl_remark') }}</span>
            @endif
        </div>
        <div class="text-right">
            <a href="{{ route('EmplList') }}"  class="btn btn-info mr-2"> Back </a>
            <button type="submit" class="btn btn-success">Add Employee</button>
        </div>
    </form>
</div>    
</div>
@include('includes.footer')