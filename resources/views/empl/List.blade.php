@include('includes.header')
<div class="card">
<div class="card-body">    
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <div class="text-right mb-3">
        <a href="{{ route('EmplAddNew') }}" class="btn btn-primary">Add Employee</a>
    </div> 
    <h1>Empolyee Management</h1>
    <table id="maintable" class="table petlist-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Group</th>
                <th>Department</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($list_data as $key => $li)
                <tr>
                    <td>{{ ($key+1) }}</td>
                    <td>{{ $li['empl_name'] }}</td>
                    <td>{{ $li['empl_email'] }}</td>
                    <td>{{ $li['empl_mobile'] }}</td>
                    <td>{{ $li['group_code'] }}</td>
                    <td>{{ $li['department_code'] }}</td>
                    <td>{{ ($li['empl_status'] == 1)?'ACTIVE':'IN-ACTIVE'}}</td>
                    <td>
                        <a href="{{ route('EmplView', ['id' => $li['empl_id'] ]) }}" class="btn btn-warning mr-2">Edit</a>
                        <a href="{{ route('EmplDelete', ['id' => $li['empl_id'] ]) }}" class="btn btn-danger del_btn" rel="{{ $li['empl_name'] }}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@include('includes.footer')