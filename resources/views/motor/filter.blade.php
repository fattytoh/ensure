<div class="row">
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="order_vehicles_no">Vehicle Number</label>
            <input type="text" class="form-control" id="order_vehicles_no" name="order_vehicles_no" value="" >
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="order_policyno">Policy Number</label>
            <input type="text" class="form-control"  id="order_policyno" name="order_policyno" value="" >
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="order_period_from">From</label>
            <input type="text" class="form-control  flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_from" name="order_period_from" value="" readonly="readonly">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="order_period_to">To</label>
            <input type="text" class="form-control  flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_to" name="order_period_to" value="" readonly="readonly">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="order_cvnote">Cover Note No</label>
            <input type="text" class="form-control"  id="order_cvnote" name="order_cvnote" value="" >
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="dealer_name">TSA</label>
            <input type="text" class="form-control"  id="dealer_name" name="dealer_name" value="" >
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="order_cancel_date">Cancel Date</label>
            <input type="text" class="form-control  flatpickr-input active" placeholder="DD/MM/YYYY" id="order_cancel_date" name="order_cancel_date" value="" readonly="readonly">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="order_type">Order Type</label>
            <input type="text" class="form-control"  id="order_type" name="order_type" value="" >
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="partner_nirc">I/C No</label>
            <input type="text" class="form-control"  id="partner_nirc" name="partner_nirc" value="" >
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label for="partner_name">Insured Name</label>
            <input type="text" class="form-control"  id="partner_name" name="partner_name" value="" >
        </div>
    </div>
</div>
<div class="text-right mb-3">
    <button class="filter_btn btn btn-primary">Filter</button>
</div>