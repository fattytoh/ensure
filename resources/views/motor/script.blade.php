<script type="text/javascript" src="{{ asset('assets/js/motor.js') }}"></script>
<script>
    $(document).ready(function(){
        @if ($errors->has('order_other_financecode'))
            $("#other_financecode_box").slideDown();
        @endif

        @if(isset($order_id) && $order_id > 0)
            $(".span_premium").html(changeNumberFormat(RoundNum($("#order_grosspremium_amount").val(),2)));
            $(".span_bank_interest").html(changeNumberFormat(RoundNum($("#order_bank_interest").val(),2)));
            $("#cus_dis_total").html(changeNumberFormat(RoundNum($("#order_grosspremium_amount").val(), 2)));
        @endif

        $(".cover_picker").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d"
        });

        $(".basic_picker").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        $(".dob_picker").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            minDate: "{{ $min_age }}",
            maxDate: "{{ $max_age }}",
        });

        $("#order_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $("#order_customer").select2({
            placeholder: "Search for a customer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('Cusselect') }}",
                dataType: 'json',
            }
        });

        $("#order_financecode").select2({
            placeholder: "Search for a Hires Company",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('HiresCompselect') }}",
                dataType: 'json',
            }
        });

        $("#order_financecode").change(function(){
            if($(this).val() == '115'){
                $("#other_financecode_box").slideDown();
            }
            else{
                $("#order_other_financecode").val('');
                $("#other_financecode_box").slideUp();
            }
        });

        $(".driver_btn_expand").click(function(){
            var box = $(this).attr('rel');
            if($("#" + box).is(':hidden')){
                $(this).children(".fa").removeClass("fa-plus");
                $(this).children(".fa").addClass("fa-minus");
                $("#" + box).slideDown();
            }
            else{
                $(this).children(".fa").removeClass("fa-minus");
                $(this).children(".fa").addClass("fa-plus");
                $("#" + box).slideUp();
            }
        });

        @if($errors->has('order_driver1name') || $errors->has('order_driver1occupation') || $errors->has('order_driver1pass_date') || $errors->has('order_driver1relationship') || $errors->has('order_driver1pass_ic') || $errors->has('order_driver1dob') || $errors->has('order_driver1_remark'))
            if($("#driver_main_box").is(':hidden')){
                $("#driver_main_btn").children(".fa").removeClass("fa-plus");
                $("#driver_main_btn").children(".fa").addClass("fa-minus");
                $("#driver_main_box").slideDown();
            }

            if($("#driver1_box").is(':hidden')){
                $("#driver_1_btn").children(".fa").removeClass("fa-plus");
                $("#driver_1_btn").children(".fa").addClass("fa-minus");
                $("#driver1_box").slideDown();
            }
        @endif

        @if($errors->has('order_driver2name') || $errors->has('order_driver2occupation') || $errors->has('order_driver2pass_date') || $errors->has('order_driver2relationship') || $errors->has('order_driver2pass_ic') || $errors->has('order_driver2dob') || $errors->has('order_driver2_remark'))
            if($("#driver_main_box").is(':hidden')){
                $("#driver_main_btn").children(".fa").removeClass("fa-plus");
                $("#driver_main_btn").children(".fa").addClass("fa-minus");
                $("#driver_main_box").slideDown();
            }
            
            if($("#driver2_box").is(':hidden')){
                $("#driver_2_btn").children(".fa").removeClass("fa-plus");
                $("#driver_2_btn").children(".fa").addClass("fa-minus");
                $("#driver2_box").slideDown();
            }
        @endif

        @if($errors->has('order_driver3name') || $errors->has('order_driver3occupation') || $errors->has('order_driver3pass_date') || $errors->has('order_driver3relationship') || $errors->has('order_driver3pass_ic') || $errors->has('order_driver3dob') || $errors->has('order_driver3_remark'))
            if($("#driver_main_box").is(':hidden')){
                $("#driver_main_btn").children(".fa").removeClass("fa-plus");
                $("#driver_main_btn").children(".fa").addClass("fa-minus");
                $("#driver_main_box").slideDown();
            }
            
            if($("#driver3_box").is(':hidden')){
                $("#driver_3_btn").children(".fa").removeClass("fa-plus");
                $("#driver_3_btn").children(".fa").addClass("fa-minus");
                $("#driver3_box").slideDown();
            }
        @endif

        @if($errors->has('order_driver4name') || $errors->has('order_driver4occupation') || $errors->has('order_driver4pass_date') || $errors->has('order_driver4relationship') || $errors->has('order_driver4pass_ic') || $errors->has('order_driver4dob') || $errors->has('order_driver4_remark'))
            if($("#driver_main_box").is(':hidden')){
                $("#driver_main_btn").children(".fa").removeClass("fa-plus");
                $("#driver_main_btn").children(".fa").addClass("fa-minus");
                $("#driver_main_box").slideDown();
            }
            
            if($("#driver4_box").is(':hidden')){
                $("#driver_4_btn").children(".fa").removeClass("fa-plus");
                $("#driver_4_btn").children(".fa").addClass("fa-minus");
                $("#driver4_box").slideDown();
            }
        @endif

        $("#order_3rd_rider_amt,#order_4th_rider_amt,#order_in_exp_amt,#order_commercial_use_per,#order_commercial_use_amt,#order_accident_per,#order_accident_amt,#order_female_dis_per,#order_female_dis_amt,#order_ncd_dis_per,#order_ncd_dis_amt,#order_ofd_dis_per,#order_ofd_dis_amt,#order_vehicle_load_per,#order_vehicle_load_amt").each(function() {
            var chk_val = $(this).val().replace(/,/g , '');
            if(chk_val > 0 ){
                $(this).removeAttr("READONLY");
            }
        });

        $(".opt_charge").change(function(){
            var amt_field = $(this).attr("rel_amt");
            var per_field = $(this).attr("rel_per");
            if($(this).val() == 'yes'){
                if(amt_field){
                    $("#" + amt_field).removeAttr("READONLY");
                }
                if(per_field){
                    $("#" + per_field).removeAttr("READONLY");
                }
            }
            else{
                if(amt_field){
                    $("#" + amt_field).val("0");
                    $("#" + amt_field).prop('readonly', true);;
                }
                if(per_field){
                    $("#" + per_field).val("0");
                    $("#" + per_field).prop('readonly', true);;
                }
            }
        })
                
        // $("#order_premium_amt, #order_direct_discount_amt, #order_bank_interest").keyup(function (){
        //     calcutationMotorGrandTotal(); 
        // });
        
        $('#order_proformsendout_btn').on("click", function(e) {
            $('#order_proformsendout_date').val("{{ date('Y-m-d') }}");
            $('#order_proformsendout_date').next().val("{{ date('d/m/Y') }}");
            $('#order_proformsendout_by').val("{{ auth()->user()->getName() }}");
        });

        $('#order_policysendout_btn').on("click", function(e) {
            $('#order_policysendout_date').val("{{ date('Y-m-d') }}");
            $('#order_policysendout_date').next().val("{{ date('d/m/Y') }}");
            $('#order_policysendout_by').val("{{ auth()->user()->getName() }}");
        });

        $('#order_dncheckedsendout_btn').on("click", function(e) {
            $('#order_dncheckedsendout_date').val("{{ date('Y-m-d') }}");
            $('#order_dncheckedsendout_date').next().val("{{ date('d/m/Y') }}");
            $('#order_dncheckedsendout_by').val("{{ auth()->user()->getName() }}");
        });
        
        $(".sum_cal").click(function(){
            calcutationMotorGrandTotal();
        });

        $("#order_period_from").change(function(){
            var dt1 = $(this).val();
            var finalDate = dt1;
            var dt = new Date(finalDate);
            if(!isNaN(dt)){
                dt.setFullYear(dt.getFullYear() + parseInt(1));
                dt.setDate(dt.getDate() - parseInt(1));
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var disdate = year + "-" + month + "-" + day;
                var new_format = changeDateFormat(disdate);
                $("#order_period_to").val(disdate);
                $("#order_period_to").next('input').val(new_format);
            }
        });
    })
</script>