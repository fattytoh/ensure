<h4 class="form-group-title mb-5">Customer Fees Calculation</h4>
<div class="text-right">
    <button type="button" class="sum_cal btn btn-primary">Auto Calculate</button>
</div>
<div class="form-group mb-3">
    <label for="order_premium_amt">Basic Premium</label>
    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_premium_amt" name="order_premium_amt" value="{{ old('order_premium_amt')?:$order_premium_amt}}" {{ $disabled }} >
            @if ($errors->has('order_premium_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_premium_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_commercial_use_per">Commercial Charges </label>
    <select id="order_commercial_option" name="order_commercial_option" rel_per="order_commercial_use_per" rel_amt="order_commercial_use_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_commercial_use_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_commercial_use_per" name="order_commercial_use_per" value="{{ old('order_commercial_use_per')?:$order_commercial_use_per }}" {{ $disabled }} >
            @if ($errors->has('order_commercial_use_per'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_commercial_use_per') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_commercial_use_amt">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_commercial_use_amt" name="order_commercial_use_amt" value="{{ old('order_commercial_use_amt')?:$order_commercial_use_amt}}" {{ $disabled }}>
            @if ($errors->has('order_commercial_use_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_commercial_use_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_accident_per">Accident Claim</label>
    <select id="order_accident_option" name="order_accident_option" rel_per="order_accident_per" rel_amt="order_accident_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_accident_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_accident_per" name="order_accident_per" value="{{ old('order_accident_per')?:$order_accident_per}}" {{ $disabled }}>
            @if ($errors->has('order_accident_per'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_accident_per') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_accident_amt">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_accident_amt" name="order_accident_amt" value="{{ old('order_accident_amt')?:$order_accident_amt}}" {{ $disabled }}>
            @if ($errors->has('order_accident_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_accident_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_in_exp_option">In-experience Driver</label>
    <select id="order_in_exp_option" name="order_in_exp_option" rel_amt="order_in_exp_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_in_exp_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_in_exp_amt" name="order_in_exp_amt" value="{{ old('order_in_exp_amt')?:$order_in_exp_amt}}" {{ $disabled }} >
            @if ($errors->has('order_in_exp_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_in_exp_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_vehicles_load_option">Vehicles Load</label>
    <select id="order_vehicles_load_option" name="order_vehicles_load_option" rel_per="order_vehicle_load_per" rel_amt="order_vehicle_load_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_vehicle_load_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_vehicle_load_per" name="order_vehicle_load_per" value="{{ old('order_vehicle_load_per')?:$order_vehicle_load_per}}" {{ $disabled }}>
            @if ($errors->has('order_vehicle_load_per'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicle_load_per') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_vehicle_load_amt">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_vehicle_load_amt" name="order_vehicle_load_amt" value="{{ old('order_vehicle_load_amt')?:$order_vehicle_load_amt}}" {{ $disabled }} >
            @if ($errors->has('order_vehicle_load_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicle_load_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_ncd_dis_option">NCD Discount</label>
    <select id="order_ncd_dis_option" name="order_ncd_dis_option" rel_per="order_ncd_dis_per" rel_amt="order_ncd_dis_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_ncd_dis_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_ncd_dis_per" name="order_ncd_dis_per" value="{{ old('order_ncd_dis_per')?:$order_ncd_dis_per}}" {{ $disabled }}>
            @if ($errors->has('order_ncd_dis_per'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_dis_per') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_ncd_dis_amt">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_ncd_dis_amt" name="order_ncd_dis_amt" value="{{ old('order_ncd_dis_amt')?:$order_ncd_dis_amt}}" {{ $disabled }}>
            @if ($errors->has('order_ncd_dis_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_dis_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_ofd_dis_option">OFD Discount</label>
    <select id="order_ofd_dis_option" name="order_ofd_dis_option" rel_per="order_ofd_dis_per" rel_amt="order_ofd_dis_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_ofd_dis_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_ofd_dis_per" name="order_ofd_dis_per" value="{{ old('order_ofd_dis_per')?:$order_ofd_dis_per}}" {{ $disabled }}>
            @if ($errors->has('order_ofd_dis_per'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ofd_dis_per') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_ofd_dis_amt">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_ofd_dis_amt" name="order_ofd_dis_amt" value="{{ old('order_ofd_dis_amt')?:$order_ofd_dis_amt}}" {{ $disabled }}>
            @if ($errors->has('order_ofd_dis_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ofd_dis_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_3rd_rider_option">3rd Rider Charges</label>
    <select id="order_3rd_rider_option" name="order_3rd_rider_option" rel_amt="order_3rd_rider_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_3rd_rider_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_3rd_rider_amt" name="order_3rd_rider_amt" value="{{ old('order_3rd_rider_amt')?:$order_3rd_rider_amt}}" {{ $disabled }}>
            @if ($errors->has('order_3rd_rider_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_3rd_rider_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_4th_rider_option">4th Rider Charges</label>
    <select id="order_4th_rider_option" name="order_4th_rider_option" rel_amt="order_4th_rider_amt" {{ $disabled }}>
        <option value="no">No</option>
        <option value="yes" {{ ($order_4th_rider_amt > 0) ? "selected" : "" }} >Yes</option>
    </select>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_4th_rider_amt" name="order_4th_rider_amt" value="{{ old('order_4th_rider_amt')?:$order_4th_rider_amt}}" {{ $disabled }}>
            @if ($errors->has('order_4th_rider_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_4th_rider_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_addtional_amt">Additonal Charges</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_addtional_amt" name="order_addtional_amt" value="{{ old('order_addtional_amt')?:$order_addtional_amt}}" {{ $disabled }}>
            @if ($errors->has('order_addtional_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_addtional_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_subtotal_amount">Sub Total</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_subtotal_amount" name="order_subtotal_amount" value="{{ old('order_subtotal_amount')?:$order_subtotal_amount }}" readonly="readonly" {{ $disabled }}>
            @if ($errors->has('order_subtotal_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_subtotal_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_gst_percent">GST</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_gst_percent" name="order_gst_percent" value="{{ old('order_gst_percent')?:$order_gst_percent }}" {{ $disabled }}>
            @if ($errors->has('order_gst_percent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_gst_percent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_gst_amount">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_gst_amount" name="order_gst_amount" value="{{ old('order_gst_amount')?:$order_gst_amount }}" {{ $disabled }}>
            @if ($errors->has('order_gst_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_gst_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_direct_discount_amt">Direct Discount</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_direct_discount_amt" name="order_direct_discount_amt" value="{{ old('order_direct_discount_amt')?:$order_direct_discount_amt}}" {{ $disabled }}>
            @if ($errors->has('order_direct_discount_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_direct_discount_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_bank_interest">Bank Interest</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_bank_interest" name="order_bank_interest" value="{{ old('order_bank_interest')?:$order_bank_interest }}" {{ $disabled }}>
            @if ($errors->has('order_bank_interest'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_bank_interest') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_grosspremium_amount">Grand Total</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_grosspremium_amount" name="order_grosspremium_amount" value="{{ old('order_grosspremium_amount')?:$order_grosspremium_amount}}" {{ $disabled }}>
            @if ($errors->has('order_grosspremium_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_grosspremium_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3 text-right">
    <h3>Total : <span id="cus_dis_total" class="text-green">0.00</span></h3>
</div>
<div class="text-right mb-3">
    <button type="button" class="sum_cal btn btn-primary">Auto Calculate</button>
</div>
<div class="form-group mb-3">
    <label for="order_extra_name">Extra Service</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control" id="order_extra_name" name="order_extra_name" placeholder="Extra Service Name" value="{{ old('order_extra_name')?:$order_extra_name }}" {{ $disabled }}>
            @if ($errors->has('order_extra_name'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_extra_name') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_extra_amount">=</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_extra_amount" name="order_extra_amount" value="{{ old('order_extra_amount')?:$order_extra_amount }}" {{ $disabled }}>
            @if ($errors->has('order_extra_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_extra_amount') }}</span>
            @endif
        </div>
    </div>
</div>