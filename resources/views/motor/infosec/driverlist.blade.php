<div class="box box-solid collapsed-box my-5" >
    <div class="box-header">
        <h3 class="box-title">Driver Details</h3>
        <div class="box-tools" style="position:unset;display: inline;" >
            <button class="btn btn-default driver_btn_expand btn-sm" type="button" rel="driver_main_box" id="driver_main_btn" ><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div style="display: none;" class="box-body" id="driver_main_box">
        @for ($i = 1; $i < 5; $i++ )
        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <h3 class="box-title">Driver {{ $i }}</h3>
                <div class="box-tools" style="position:unset;display: inline;">
                    <button class="btn btn-default driver_btn_expand btn-sm" type="button" rel="driver{{ $i }}_box" id="driver_{{ $i }}_btn" ><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div style="display: none;" class="box-body pt-5" id="driver{{ $i }}_box">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="order_driver{{ $i }}name">Driver Name {{ $i }}</label>
                            <input type="text" class="form-control" id="order_driver{{ $i }}name" name="order_driver{{ $i }}name" value="{{ old('order_driver'.$i.'name')?:${'order_driver'.$i.'name'} }}" {{ $disabled }}>
                            @if ($errors->has('order_driver'.$i.'name'))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_driver'.$i.'name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="order_driver{{ $i }}occupation">Occupation</label>
                            <input type="text" class="form-control" id="order_driver{{ $i }}occupation" name="order_driver{{ $i }}occupation" value="{{ old('order_driver'.$i.'occupation')?:${'order_driver'.$i.'occupation'} }}" {{ $disabled }}>
                            @if ($errors->has('order_driver'.$i.'occupation'))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_driver'.$i.'occupation') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="order_driver{{ $i }}pass_date">Driving Pass Dt</label>
                            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_driver{{ $i }}pass_date" name="order_driver{{ $i }}pass_date" value="{{ old('order_driver'.$i.'pass_date')?:${'order_driver'.$i.'pass_date'} }}" readonly="readonly" {{ $disabled }}>
                            @if ($errors->has('order_driver'.$i.'pass_date'))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_driver'.$i.'pass_date') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="order_driver{{ $i }}relationship">Relationship</label>
                            <input type="text" class="form-control" id="order_driver{{ $i }}relationship" name="order_driver{{ $i }}relationship" value="{{ old('order_driver'.$i.'relationship')?:${'order_driver'.$i.'relationship'} }}" {{ $disabled }}>
                            @if ($errors->has('order_driver'.$i.'relationship'))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_driver'.$i.'relationship') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="order_driver{{ $i }}pass_ic">I/C Number</label>
                            <input type="text" class="form-control" id="order_driver{{ $i }}pass_ic" name="order_driver{{ $i }}pass_ic" value="{{ old('order_driver'.$i.'pass_ic')?:${'order_driver'.$i.'pass_ic'} }}" {{ $disabled }} >
                            @if ($errors->has('order_driver'.$i.'pass_ic'))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_driver'.$i.'pass_ic') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="order_driver{{ $i }}dob">DOB</label>
                            <input type="text" class="form-control dob_picker flatpickr-input active" placeholder="YYYY-MM-DD" id="order_driver{{ $i }}dob" name="order_driver{{ $i }}dob" value="{{ old('order_driver'.$i.'dob')?:${'order_driver'.$i.'dob'} }}" readonly="readonly" {{ $disabled }}>
                            @if ($errors->has('order_driver'.$i.'dob'))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_driver'.$i.'dob') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group mb-3">
                    <label for="order_driver{{ $i }}_remark">Remark</label>
                    <textarea class="form-control" id="order_driver{{ $i }}_remark" rows="10" name="order_driver{{ $i }}_remark" {{ $disabled }}>{{ old('order_driver'.$i.'_remark')?:${'order_driver'.$i.'_remark'} }}</textarea>
                    @if ($errors->has('order_driver'.$i.'_remark'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_driver'.$i.'_remark') }}</span>
                    @endif
                </div>
            </div>
        </div>
        @endfor
    </div>
</div>