@include('includes.header')
@inject('provider', 'App\Http\Controllers\AllianzController')
<div class="card">
<div class="card-body">
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <div class="text-right mb-3">
        @PrivilegeCheck('MotorAddNew')
        <a href="{{ route('MotorAddNew') }}" class="btn btn-primary">Add Motor Insurance</a>
        @endPrivilegeCheck
    </div> 
    <h1>Motor Insurance Management</h1>
    @include('motor.filter')  
    <table id="motortable" class="table petlist-table" >
        <thead>
            <tr>
                <th>No</th>
                <th>Order No</th>
                <th>Vehicle No</th>
                <th>Customer</th>
                <th>Order Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
<script>
    $(document).ready(function(){
        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });
        orderTable();
        $(".filter_btn").click(function(){
            orderTable();
        })
    })

    function orderTable(){
        $('#motortable').DataTable().clear().destroy();
        $("#motortable").DataTable({
            ajax: {
                url: "{{ route('motorListData') }}",
                type: "POST",
                'data': {
                    order_vehicles_no : $("#order_vehicles_no").val(),
                    order_policyno : $("#order_policyno").val(),
                    order_period_from : $("#order_period_from").val(),
                    order_period_to : $("#order_period_to").val(),
                    order_cvnote : $("#order_cvnote").val(),
                    dealer_name : $("#dealer_name").val(),
                    order_cancel_date : $("#order_cancel_date").val(),
                    order_type : $("#order_type").val(),
                    partner_nirc : $("#partner_nirc").val(),
                    partner_name : $("#partner_name").val(),
                }
            },
            processing: true,
            serverSide: true,
            columnDefs: [
                { orderable: false, targets: 0 },
                { orderable: false, targets: -1 }
            ]
        });
    }
</script>
@include('includes.footer')