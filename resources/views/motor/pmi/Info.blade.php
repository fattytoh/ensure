@include('includes.header')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            There is an error occur, Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Update Private Motor Insurance</h1>
        <form id="MotorForm" action="{{ route('PrivateMotorUpdate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="d-flex flex-wrap justify-content-end btn-list">
                @PrivilegeCheck('CustomerAddnew')
                <a href="{{ route('CustomerAddnew') }}" class="btn btn-secondary" target="_blank">Add Customer</a>
                @endPrivilegeCheck
                <a href="{{ route('PrivateMotorEndorsement', ['id' => $order_id ]) }}" class="btn btn-warning" target="_blank">Create Endorsement</a>
                <a href="{{ route('PrivateMotorRenewal', ['id' => $order_id ]) }}" class="btn btn-info" target="_blank">Renewal</a>
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            <div class="row">
                <div class="col-md-6">
                    @include('motor.infosec.basicinfo')    
                    @include('motor.infosec.vecinfo')    
                    @include('motor.infosec.driverinfo')   
                    @include('motor.infosec.NCDinfo')   
                </div> 
                <div class="col-md-6">
                    @include('motor.infosec.cuscal')  
                    @include('motor.infosec.tsacal')  
                    @include('motor.infosec.compcal')  
                    <h4 class="form-group-title mb-5">File Attachment</h4>
                    <div class="input-group mb-3">
                        <input type="file" class="form-control" id="order_files_attach" name="order_files_attach[]" multiple>
                        <label class="input-group-text" for="order_files_attach">File Upload</label>
                        @if ($errors->has('order_files_attach'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_files_attach') }}</span>
                        @endif
                    </div>
                </div>
            </div> 
                    
           
            @include('motor.infosec.driverlist') 
            <input type = "hidden" value = "{{ $order_id }}" name = "order_id"/>
            <input type = "hidden" value = "{{ $order_ref }}" name = "order_ref"/>
            <input type = "hidden" value = "{{ $order_renewal_ref }}" name = "order_renewal_ref"/>
            <div class="text-right">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
        @include('motor.infosec.filelist') 
    </div>
</div>
@include('motor.script')
@include('includes.footer')