@include('includes.header')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            There is an error occur, Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Add Commercial Motor Insurance</h1>
        <form id="MotorForm" action="{{ route('CommercialMotorCreate') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="d-flex flex-wrap justify-content-end btn-list">
            @PrivilegeCheck('CustomerAddnew')
            <a href="{{ route('CustomerAddnew') }}" class="btn btn-secondary" target="_blank">Add Customer</a>
            @endPrivilegeCheck
            <button type="submit" class="btn btn-success">Save</button>
        </div>
            <div class="row">
                <div class="col-md-6">
                    @include('motor.addsec.basicinfo')    
                    @include('motor.addsec.vecinfo')    
                    @include('motor.addsec.driverinfo')   
                    @include('motor.addsec.NCDinfo')   
                </div> 
                <div class="col-md-6">
                    @include('motor.addsec.cuscal')  
                    @include('motor.addsec.tsacal')  
                    @include('motor.addsec.compcal')  
                    <h4 class="form-group-title mb-5">File Attachment</h4>
                    <div class="input-group mb-3">
                        <input type="file" class="form-control" id="order_files_attach" name="order_files_attach[]" multiple>
                        <label class="input-group-text" for="order_files_attach">File Upload</label>
                        @if ($errors->has('order_files_attach'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_files_attach') }}</span>
                        @endif
                    </div>
                </div>
            </div> 
                    
           
            @include('motor.addsec.driverlist') 
            <input type = "hidden" value = "DN" name = "order_doc_type" id="order_doc_type"/>
            <div class="text-right">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
</div>
@include('motor.script')
@include('includes.footer')