<h4 class="form-group-title mb-5">NCD Transfer</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ncd_previousinsurer">Previous Insurer</label>
            <input type="text" class="form-control" id="order_ncd_previousinsurer" name="order_ncd_previousinsurer" value="{{ old('order_ncd_previousinsurer')}}" >
            @if ($errors->has('order_ncd_previousinsurer'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_previousinsurer') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ncd_entitlement">NCD% Entitlement</label>
            <input type="text" class="form-control" id="order_ncd_entitlement" name="order_ncd_entitlement" value="{{ old('order_ncd_entitlement')}}" >
            @if ($errors->has('order_ncd_entitlement'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_entitlement') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ncd_cancellationdate">Cancellation Date / Expiry Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_ncd_cancellationdate" name="order_ncd_cancellationdate" value="{{ old('order_ncd_cancellationdate')}}" readonly="readonly">
            @if ($errors->has('order_ncd_cancellationdate'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_cancellationdate') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ncd_vehiclenumber">Vehicle Number</label>
            <input type="text" class="form-control" id="order_ncd_vehiclenumber" name="order_ncd_vehiclenumber" value="{{ old('order_ncd_vehiclenumber')}}" >
            @if ($errors->has('order_ncd_vehiclenumber'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_vehiclenumber') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_ncd_remark">NCD Remark</label>
    <textarea class="form-control" id="order_ncd_remark" name="order_ncd_remark">{{ old('order_ncd_remark')}}</textarea>
    @if ($errors->has('order_ncd_remark'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_remark') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_notes_remark">Notes</label>
    <textarea class="form-control" rows="10" id="order_notes_remark" name="order_notes_remark">{{ old('order_notes_remark')}}</textarea>
    @if ($errors->has('order_notes_remark'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_notes_remark') }}</span>
    @endif
</div>