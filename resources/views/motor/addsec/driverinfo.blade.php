<h4 class="form-group-title mb-5">Driver Info</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_nameddriver_amount">Excess (Comprehensive)</label>
            <input type="text" class="form-control" id="order_nameddriver_amount" name="order_nameddriver_amount" value="{{ old('order_nameddriver_amount')}}" >
            @if ($errors->has('order_nameddriver_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_nameddriver_amount') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_nameddriver" class="d-md-inline d-none">&nbsp;</label>
            <input type="text" class="form-control" id="order_nameddriver" name="order_nameddriver" value="{{ old('order_nameddriver')}}" >
            @if ($errors->has('order_nameddriver'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_nameddriver') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_tpft_amount">Excess (TPFT)</label>
            <input type="text" class="form-control" id="order_tpft_amount" name="order_tpft_amount" value="{{ old('order_tpft_amount')}}" >
            @if ($errors->has('order_tpft_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_tpft_amount') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_tpft_driver" class="d-md-inline d-none">&nbsp;</label>
            <input type="text" class="form-control" id="order_tpft_driver" name="order_tpft_driver" value="{{ old('order_tpft_driver')}}" >
            @if ($errors->has('order_tpft_driver'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_tpft_driver') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_out_amount">Excess (Outside of Singapore)</label>
            <input type="text" class="form-control" id="order_out_amount" name="order_out_amount" value="{{ old('order_out_amount')}}" >
            @if ($errors->has('order_out_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_out_amount') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_out_name" class="d-md-inline d-none">&nbsp;</label>
            <input type="text" class="form-control" id="order_out_name" name="order_out_name" value="{{ old('order_out_name')}}" >
            @if ($errors->has('order_out_name'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_out_name') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_extrabenefit">Remarks</label>
    <input type="text" class="form-control" id="order_extrabenefit" name="order_extrabenefit" value="{{ old('order_extrabenefit')}}" >
    @if ($errors->has('order_extrabenefit'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_extrabenefit') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <button type="button" class="btn btn-info mb-3" id="order_proformsendout_btn">Proposal Form Send Out</button> ON
            <input type="hidden" id="order_proformsendout_date" name="order_proformsendout_date" value="{{ old('order_proformsendout_date') }}">
            <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" value="{{ old('order_proformsendout_date') }}" readonly="readonly">
            @if ($errors->has('order_proformsendout_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_proformsendout_date') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_proformsendout_by" class="float-label-margin">By</label>
            <input type="text" class="form-control" id="order_proformsendout_by" name="order_proformsendout_by" value="{{ old('order_proformsendout_by')}}" readonly="readonly">
            @if ($errors->has('order_proformsendout_by'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_proformsendout_by') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <button type="button" class="btn btn-info mb-3" id="order_policysendout_btn">Policy / CI Send Out</button> ON
            <input type="hidden" id="order_policysendout_date" name="order_policysendout_date" value="{{ old('order_policysendout_date') }}" >
            <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" value="{{ old('order_policysendout_date') }}" readonly="readonly">
            @if ($errors->has('order_policysendout_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_policysendout_date') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_policysendout_by" class="float-label-margin">By</label>
            <input type="text" class="form-control" id="order_policysendout_by" name="order_policysendout_by" value="{{ old('order_policysendout_by')}}" readonly="readonly">
            @if ($errors->has('order_policysendout_by'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_policysendout_by') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <button type="button" class="btn btn-info mb-3" id="order_dncheckedsendout_btn">DN Checked</button> ON
            <input type="hidden" id="order_dncheckedsendout_date" name="order_dncheckedsendout_date" value="{{ old('order_dncheckedsendout_date') }}" >
            <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY"  readonly="readonly">
            @if ($errors->has('order_dncheckedsendout_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_dncheckedsendout_date') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_dncheckedsendout_by" class="float-label-margin">By</label>
            <input type="text" class="form-control" id="order_dncheckedsendout_by" name="order_dncheckedsendout_by" value="{{ old('order_dncheckedsendout_by')}}" readonly="readonly">
            @if ($errors->has('order_dncheckedsendout_by'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_dncheckedsendout_by') }}</span>
            @endif
        </div>
    </div>
</div>