<h4 class="form-group-title mb-5">Insurance Basic info</h4>
<div class="row mb-3">
    <div class="col-md-12">
        <label for="order_record_billto">Bill To</label>
        <select name="order_record_billto" id="order_record_billto" class="select form-select select-hidden-accessible">
            <option value="TO TSA" {{ (old('order_record_billto') == 'TO TSA') ? "selected" : "" }}>To TSA</option>
            <option value="TO Customer" {{ (old('order_record_billto') == 'TO Customer') ? "selected" : "" }}>To Customer</option>
            <option value="TO Salesman" {{ (old('order_record_billto') == 'TO Salesman') ? "selected" : "" }}>To Salesman</option>
        </select>
        @if ($errors->has('order_record_billto'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_record_billto') }}</span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_type">Type</label>
            <select name="order_type" id="order_type" class="select form-select select-hidden-accessible">
                <option value="New Case" {{ (old('order_type') == 'New Case') ? "selected" : "" }}>New Case</option>
                <option value="Renewal" {{ (old('order_type') == 'Renewal') ? "selected" : "" }}>Renewal</option>
                <option value="Extend" {{ (old('order_type') == 'Extend') ? "selected" : "" }}>Extend</option>
                <option value="Endorsement" {{ (old('order_type') == 'Endorsement') ? "selected" : "" }}>Endorsement</option> 
            </select>
            @if ($errors->has('order_type'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_type') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_from">Coverage Date From</label>
            <input type="text" class="form-control cover_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_from" name="order_period_from" value="{{ old('order_period_from') }}" readonly="readonly">
            @if ($errors->has('order_period_from'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_from') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_to">Coverage Date To</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_to" name="order_period_to" value="{{ old('order_period_to') }}" readonly="readonly">
            @if ($errors->has('order_period_to'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_to') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_dealer">TSA</label>
            <select name="order_dealer" id="order_dealer" class="select2 form-select select2-hidden-accessible">
            </select>
            @if ($errors->has('order_dealer'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_dealer') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_no">Debit / Credit Notes Number</label>
            <input type="text" class="form-control" id="order_no" name="order_no" value="-- System Generate --" readonly>
            @if ($errors->has('order_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_no') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_customer">Customer</label>
            <select name="order_customer" id="order_customer" class="select2 form-select select2-hidden-accessible">
            </select>
            @if ($errors->has('order_customer'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_customer') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_date">Entry Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_date" name="order_date" readonly="readonly" value="{{ old('order_date') }}">
            @if ($errors->has('order_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_date') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_insurco">Insurance Company</label>
            <select name="order_insurco" id="order_insurco" class="select form-select select-hidden-accessible">
                @foreach($insuranceco_list as $li)
                    <option value="{{ $li['insuranceco_id'] }}" {{ (old('order_insurco') == $li['insuranceco_id']) ? "selected" : "" }} >{{ $li['insuranceco_code'] ." - ".$li['insuranceco_name']}}</option>
                @endforeach
            </select>
            @if ($errors->has('order_insurco'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_insurco') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_coverage">Coverage</label>
            <select name="order_coverage" id="order_coverage" class="select form-select select-hidden-accessible">
                <option value="">Select One</option>
                <option value="C - Comprehensive" {{ (old('order_coverage') == 'C - Comprehensive') ? "selected" : "" }}>C - Comprehensive</option>
                <option value="F - Third Party Fire and Theft" {{ (old('order_coverage') == 'F - Third Party Fire and Theft') ? "selected" : "" }}>F - Third Party Fire & Theft</option>
                <option value="T - Third Party" {{ (old('order_coverage') == 'T - Third Party') ? "selected" : "" }}>T - Third Party</option>
            </select>
            @if ($errors->has('order_coverage'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_coverage') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_no">Vehicles Number</label>
            <input type="text" class="form-control" id="order_vehicles_no" name="order_vehicles_no" value="{{ old('order_vehicles_no')}}" >
            @if ($errors->has('order_vehicles_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_no') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cvnote">CV Note.</label>
            <input type="text" class="form-control" id="order_cvnote" name="order_cvnote" value="{{ old('order_cvnote')}}" >
            @if ($errors->has('order_cvnote'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cvnote') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_policyno">Policy Number</label>
            <input type="text" class="form-control" id="order_policyno" name="order_policyno" value="{{ old('order_policyno')}}" >
            @if ($errors->has('order_policyno'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_policyno') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cancel_date">Cancel Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_cancel_date" name="order_cancel_date" value="{{ old('order_cancel_date') }}" readonly="readonly">
            @if ($errors->has('order_cancel_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cancel_date') }}</span>
            @endif
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_financecode">Financial Code</label>
            <select name="order_financecode" id="order_financecode" class="select2 form-select select2-hidden-accessible">
            </select>
            @if ($errors->has('order_financecode'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_financecode') }}</span>
            @endif
        </div>
    </div>
    <div id="other_financecode_box" class="col-md-6" style="display:none;">
        <div class="form-group mb-3">
            <label for="order_other_financecode">Other Company Name</label>
            <input type="text" class="form-control" id="order_other_financecode" name="order_other_financecode" value="{{ old('order_other_financecode') }}" >
            @if ($errors->has('order_other_financecode'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_other_financecode') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ins_doc_no">Ins Document Number</label>
            <input type="text" class="form-control" id="order_ins_doc_no" name="order_ins_doc_no" value="{{ old('order_ins_doc_no')}}" >
            @if ($errors->has('order_ins_doc_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ins_doc_no') }}</span>
            @endif
        </div>
    </div>
</div>