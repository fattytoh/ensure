<h4 class="form-group-title mb-5">Vehicles Info</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_brand">Vehicle Brand</label>
            <input type="text" class="form-control" id="order_vehicles_brand" name="order_vehicles_brand" value="{{ old('order_vehicles_brand')}}" >
            @if ($errors->has('order_vehicles_brand'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_brand') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_model">Vehicle Model</label>
            <input type="text" class="form-control" id="order_vehicles_model" name="order_vehicles_model" value="{{ old('order_vehicles_model')}}" >
            @if ($errors->has('order_vehicles_model'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_model') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_engine">Vehicle Engine</label>
            <input type="text" class="form-control" id="order_vehicles_engine" name="order_vehicles_engine" value="{{ old('order_vehicles_engine')}}" >
            @if ($errors->has('order_vehicles_engine'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_engine') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_body_type">Vehicle Body Type</label>
            <input type="text" class="form-control" id="order_vehicles_body_type" name="order_vehicles_body_type" value="{{ old('order_vehicles_body_type')?:'MOTORCYCLE' }}" >
            @if ($errors->has('order_vehicles_body_type'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_body_type') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_chasis">Vehicle Chasis</label>
            <input type="text" class="form-control" id="order_vehicles_chasis" name="order_vehicles_chasis" value="{{ old('order_vehicles_chasis')}}" >
            @if ($errors->has('order_vehicles_chasis'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_chasis') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_origdt">OrigDt</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_vehicles_origdt" name="order_vehicles_origdt" value="{{ old('order_vehicles_origdt') }}" readonly="readonly">
            @if ($errors->has('order_vehicles_origdt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_origdt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_capacity">Capacity</label>
            <input type="text" class="form-control" id="order_vehicles_capacity" name="order_vehicles_capacity" value="{{ old('order_vehicles_capacity')}}" >
            @if ($errors->has('order_vehicles_capacity'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_capacity') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_seatno">Seat No</label>
            <input type="text" class="form-control" id="order_vehicles_seatno" name="order_vehicles_seatno" value="{{ old('order_vehicles_seatno')}}" >
            @if ($errors->has('order_vehicles_seatno'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_seatno') }}</span>
            @endif
        </div>
    </div>
</div>