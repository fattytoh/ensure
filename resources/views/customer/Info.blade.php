@include('includes.header')
<div class="card">
<div class="card-body">
    <form id="CustomerForm" action="{{ route('CustomerUpdate') }}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="row">
            <div class="col-md-6 col-12">
                <h4 class="form-group-title mb-5">Customer General Info</h4>
                <div class="form-group mb-3">
                    <label for="partner_name">Name</label>
                    <input type="text" class="form-control" id="partner_name" name="partner_name" value="{{ old('partner_name')?old('partner_name'):$partner_name }}">
                    @if ($errors->has('partner_name'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_name') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_nirc" >I/C No </label>
                    <input type="text" class="form-control" id="partner_nirc" name="partner_nirc" value="{{ old('partner_nirc')?old('partner_nirc'):$partner_nirc }}">
                    @if ($errors->has('partner_nirc'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_nirc') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_type" >Customer Type</label>
                    <select class="form-control select form-select select-hidden-accessible" id="partner_type" name="partner_type">
                        <option value="">SELECT ONE</option>
                        <option value="singaporean or pr" {{ ($partner_type == "singaporean or pr") ? "selected" : "" }}>SINGAPOREAN / PR</option>
                        <option value="foreign person" {{ ($partner_type == "foreign person") ? "selected" : "" }}>FOREIGN PERSON</option>
                        <option value="company" {{ ($partner_type == "company") ? "selected" : "" }}>COMPANY</option>
                    </select>
                    @if ($errors->has('partner_type'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_type') }}</span>
                    @endif
                </div>

                <div class="form-group mb-3">
                    <label for="partner_blk_no" >Block Number</label>
                    <input type="text" class="form-control" id="partner_blk_no" name="partner_blk_no" value="{{ old('partner_blk_no')?old('partner_blk_no'):$partner_blk_no }}">
                    @if ($errors->has('partner_type'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_blk_no') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_street" >Street Name</label>
                    <input type="text" class="form-control" id="partner_street" name="partner_street" value="{{ old('partner_blk_no')?old('partner_street'):$partner_street }}">
                    @if ($errors->has('partnepartner_streetr_type'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_street') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_unit_no" >Unit No</label>
                    <input type="text" class="form-control" id="partner_unit_no" name="partner_unit_no" value="{{ old('partner_unit_no')?old('partner_unit_no'):$partner_unit_no }}">
                    @if ($errors->has('partnepartner_streetr_type'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_street') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_outlet" >Country </label>
                    <select name="partner_outlet" id="partner_outlet" class="select2 select2-basic form-select select2-hidden-accessible">
                        @foreach($country_list as $cli)
                            <option value="{{ $cli['country_id'] }}" {{ ($partner_outlet == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('partner_outlet'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_outlet') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_postal_code" >Postal Code</label>
                    <input type="text" class="form-control" id="partner_postal_code" name="partner_postal_code" value="{{ old('partner_postal_code')?old('partner_postal_code'):$partner_postal_code }}">
                    @if ($errors->has('partner_postal_code'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_postal_code') }}</span>
                    @endif
                </div>
                <span id="copy_to_billing_address" class="btn btn-primary my-3">Copy Address To Billing Address</span>
                <div class="form-group mb-3">
                    <label for="partner_bil_blk_no" >Billing Block Number</label>
                    <input type="text" class="form-control" id="partner_bil_blk_no" name="partner_bil_blk_no" value="{{ old('partner_bil_blk_no')?old('partner_bil_blk_no'):$partner_bil_blk_no }}">
                    @if ($errors->has('partner_bil_blk_no'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_bil_blk_no') }}</span>
                    @endif
                </div>
                
                <div class="form-group mb-3">
                    <label for="partner_bil_street" >Billing Street Name</label>
                    <input type="text" class="form-control" id="partner_bil_street" name="partner_bil_street" value="{{ old('partner_bil_street')?old('partner_bil_street'):$partner_bil_street }}">
                    @if ($errors->has('partner_bil_street'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_bil_street') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_bil_unit_no" >Billing Unit No</label>
                    <input type="text" class="form-control" id="partner_bil_unit_no" name="partner_bil_unit_no" value="{{ old('partner_bil_unit_no')?old('partner_bil_unit_no'):$partner_bil_unit_no }}">
                    @if ($errors->has('partner_bil_unit_no'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_bil_unit_no') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_bil_country" >Billing Country </label>
                    <select name="partner_bil_country" id="partner_bil_country" class="select2 select2-basic form-select select2-hidden-accessible">
                        @foreach($country_list as $cli)
                            <option value="{{ $cli['country_id'] }}" {{ ($partner_bil_country == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('partner_bil_country'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_bil_country') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_bil_postalcode" >Billing Postal Code</label>
                    <input type="text" class="form-control" id="partner_bil_postalcode" name="partner_bil_postalcode" value="{{ old('partner_bil_postalcode')?old('partner_bil_postalcode'):$partner_bil_postalcode }}">
                    @if ($errors->has('partner_bil_postalcode'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_bil_postalcode') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_co" >Customer Company</label>
                    <input type="text" class="form-control" id="partner_co" name="partner_co" value="{{ old('partner_co')?old('partner_co'):$partner_co }}">
                    @if ($errors->has('partner_co'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_co') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_status" >Status</label>
                    <select class="form-control select form-select select-hidden-accessible" id="partner_status" name="partner_status">
                        <option value="1" {{ ($partner_status == "1") ? "selected" : "" }}>Active</option>
                        <option value="0" {{ ($partner_status == "0") ? "selected" : "" }}>In-active</option>
                    </select>
                </div>
            </div><!-- /.general -->
            <div class="col-md-6 col-12"> 
                <h4 class="form-group-title mb-5">Update Histroy</h4>
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="form-group mb-3">
                            <label class="control-label">Create By</label>
                            <span class="form-control" disabled=""></span>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="form-group mb-3">
                            <label class="control-label">ON</label>
                            <span class="form-control" disabled=""></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="form-group mb-3">
                            <label class="control-label">Last Update By</label>
                            <span class="form-control" disabled=""></span>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="form-group mb-3">
                            <label class="control-label">ON</label>
                            <span class="form-control" disabled=""></span>
                        </div>
                    </div>
                </div>
                <h4 class="form-group-title mb-5">Customer Contact Info</h4>
                <div class="form-group mb-3">
                    <label for="partner_tel" >Tel</label>
                    <input type="text" class="form-control" id="partner_tel" name="partner_tel" value="{{ old('partner_tel')?old('partner_tel'):$partner_tel }}">
                    @if ($errors->has('partner_tel'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_tel') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_tel2" >Tel 2</label>
                    <input type="text" class="form-control" id="partner_tel2" name="partner_tel2" value="{{ old('partner_tel2')?old('partner_tel2'):$partner_tel2 }}">
                    @if ($errors->has('partner_tel2'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_tel2') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_house_no" >Mobile</label>
                    <input type="text" class="form-control" id="partner_house_no" name="partner_house_no" value="{{ old('partner_house_no')?old('partner_house_no'):$partner_house_no }}">
                    @if ($errors->has('partner_house_no'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_house_no') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_fax" >Fax</label>
                    <input type="text" class="form-control" id="partner_fax" name="partner_fax" value="{{ old('partner_fax')?old('partner_fax'):$partner_fax }}">
                    @if ($errors->has('partner_fax'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_fax') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_dob" >DOB / Cert Date</label>
                    <input type="text" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="partner_dob" name="partner_dob" value="{{ old('partner_dob')?old('partner_dob'):$partner_dob }}">
                    @if ($errors->has('partner_dob'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_dob') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_email" >Email</label>
                    <input type="text" class="form-control" id="partner_email" name="partner_email" value="{{ old('partner_email')?old('partner_email'):$partner_email }}">
                    @if ($errors->has('partner_email'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_email') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_marital" >Marital Status</label>
                    <select class="form-control select form-select select-hidden-accessible" id="partner_marital" name="partner_marital">
                        <option value="single" {{ ($partner_marital == "single") ? "selected" : "" }}>Single</option>;
                        <option value="married" {{ ($partner_marital == "married") ? "selected" : "" }}>Married</option>
                        <option value="divorced" {{ ($partner_marital == "divorced") ? "selected" : "" }}>Divorced</option>
                        <option value="Other" {{ ($partner_marital == "Other") ? "selected" : "" }}>Other</option>
                    </select>
                    @if ($errors->has('partner_marital'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_marital') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_income" >Income</label>
                    <input type="text" class="form-control" id="partner_income" name="partner_income" value="{{ old('partner_income')?old('partner_income'):$partner_income }}">
                    @if ($errors->has('partner_income'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_income') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_drivingpass" >Driving Pass Date</label>
                    <input type="text" class="form-control" id="partner_drivingpass flatpickr-input active" placeholder="DD/MM/YYYY" name="partner_drivingpass" value="{{ old('partner_drivingpass')?old('partner_drivingpass'):$partner_drivingpass }}">
                    @if ($errors->has('partner_drivingpass'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_drivingpass') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_drivingpassexpiry" >Driving Pass Expiry Date</label>
                    <input type="text" class="form-control" id="partner_drivingpassexpiry flatpickr-input active" placeholder="DD/MM/YYYY" name="partner_drivingpassexpiry" value="{{ old('partner_drivingpassexpiry')?old('partner_drivingpassexpiry'):$partner_drivingpassexpiry }}">
                    @if ($errors->has('partner_drivingpassexpiry'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_drivingpassexpiry') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_gender" >Gendar</label>
                    <select class="form-control select form-select select-hidden-accessible" id="partner_gender" name="partner_gender">
                        <option value="male" {{ ($partner_gender == "male") ? "selected" : "" }}>Male</option>
                        <option value="female" {{ ($partner_gender == "female") ? "selected" : "" }}>Female</option>
                    </select>
                    @if ($errors->has('partner_gender'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_gender') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_occupation" >Occupation</label>
                    <input type="text" class="form-control" id="partner_occupation" name="partner_occupation" value="{{ old('partner_occupation')?old('partner_occupation'):$partner_occupation }}">
                    @if ($errors->has('partner_occupation'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_occupation') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_country" >Nationality </label>
                    <select name="partner_country" id="partner_country" class="select2 select2-basic form-select select2-hidden-accessible">
                        @foreach($country_list as $cli)
                            <option value="{{ $cli['country_id'] }}" {{ ($partner_country == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('partner_country'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_country') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-12">
                <h4 class="form-group-title mb-5">Customer Account Info</h4>
                <div class="form-group mb-3">
                    <label for="partner_employername" >Employer's Name</label>
                    <input type="text" class="form-control" id="partner_employername" name="partner_employername" value="{{ old('partner_employername')?old('partner_employername'):$partner_employername }}">
                    @if ($errors->has('partner_employername'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_employername') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_employeraddress" >Employer's Address</label>
                    <textarea class="form-control" rows="3" id="partner_employeraddress" name="partner_employeraddress">{{ old('partner_employeraddress')?old('partner_employeraddress'):$partner_employeraddress }}</textarea>
                    @if ($errors->has('partner_employeraddress'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_employeraddress') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_remark" >Remark</label>
                    <textarea class="form-control" rows="3" id="partner_remark" name="partner_remark">{{ old('partner_remark')?old('partner_remark'):$partner_remark }}</textarea>
                    @if ($errors->has('partner_remark'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_remark') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_recommended" >Recommended By</label>
                    <input type="text" class="form-control" id="partner_recommended" name="partner_recommended" value="{{ old('partner_recommended')?old('partner_recommended'):$partner_recommended }}">
                    @if ($errors->has('partner_recommended'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_recommended') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_primarybusinesstype" >Primary Business Type</label>
                    <input type="text" class="form-control" id="partner_primarybusinesstype" name="partner_primarybusinesstype" value="{{ old('partner_primarybusinesstype')?old('partner_primarybusinesstype'):$partner_primarybusinesstype }}">
                    @if ($errors->has('partner_primarybusinesstype'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_primarybusinesstype') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_secondbusinesstype" >Secondary Business Type</label>
                    <input type="text" class="form-control" id="partner_secondbusinesstype" name="partner_secondbusinesstype" value="{{ old('partner_secondbusinesstype')?old('partner_secondbusinesstype'):$partner_secondbusinesstype }}">
                    @if ($errors->has('partner_secondbusinesstype'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_secondbusinesstype') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_note" >Note</label>
                    <textarea class="form-control" rows="3" id="partner_note" name="partner_note">{{ old('partner_note')?old('partner_note'):$partner_note }}</textarea>
                    @if ($errors->has('partner_note'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_note') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="partner_file" >Attachment</label>
                    <input class="form-control" id="partner_file" name="partner_file" type="file">
                    @if ($errors->has('partner_file'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('partner_file') }}</span>
                    @endif
                    @if($attachment)
                        <a href="{{ asset('uploads/customer/'.$attachment) }}" target="_blank" >{{$attachment}}</a>
                    @endif
                </div>
                
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Save</button>
        </div>
    </form>
</div>
</div>
<script>
    $(document).ready(function(){
        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        $("#copy_to_billing_address").click(function(){
            $("#partner_bil_country").val($("#partner_outlet").val()).trigger('change');
            $("#partner_bil_street").val($("#partner_street").val());
            $("#partner_bil_blk_no").val($("#partner_blk_no").val());
            $("#partner_bil_unit_no").val($("#partner_unit_no").val());
            $("#partner_bil_postalcode").val($("#partner_postal_code").val());
        });
    })
</script>
@include('includes.footer')