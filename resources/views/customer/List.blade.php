@include('includes.header')
<div class="card">
<div class="card-body">
    <h1>Customer Management</h1>
    <div class="text-right mb-3">
        @PrivilegeCheck('CustomerAddnew')
        <a href="{{ route('CustomerAddnew') }}" class="btn btn-primary">Add Customer</a>
        @endPrivilegeCheck
    </div> 
    <table id="ajaxtable" class="table petlist-table" link="{{ route('cusListData') }}" >
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>NIRC</th>
                <th>Tel</th>
                <th>Mobile</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
           
        </tbody>
    </table>
</div>
</div>
@include('includes.footer')