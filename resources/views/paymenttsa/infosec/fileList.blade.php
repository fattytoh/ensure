<h4 class="form-group-title mb-5 mt-3">File List</h4>
<table id="FileTable" class="table petlist-table mb-5">
    <thead>
        <tr>
            <th>No</th>
            <th>File Name</th>
            <th>Upload By</th>
            <th>Upload Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($file_list as $file_ky => $file_li)
        <tr>
            <td>{{ ($file_ky + 1) }}</td>
            <td><a href="{{ $file_li['upload_path'].'/'.$file_li['files_name'] }}" target="_blank">{{ $file_li['files_ori_name'] }}</a></td>
            <td>{{ $file_li['insertBy'] }}</td>
            <td>{{ $file_li['created_at'] }}</td>
            <td>
                @if(!$disabled)
                    @PrivilegeCheck('PaymentFileDelete')
                        <a href="{{ route('PaymentFileDelete', ['id' => $file_li['files_id'] ]) }}" class="btn btn-danger del_btn" rel="{{ $file_li['files_ori_name'] }}"> Delete </a>
                    @endPrivilegeCheck
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>