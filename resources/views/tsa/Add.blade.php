@include('includes.header')
<div class="card">
<div class="card-body">
    <h1>Add New TSA</h1>
    <form id="TSAForm" action="{{ route('TSACreate') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <h4 class="form-group-title mb-5">Basic info</h4>
                <div class="form-group mb-3">
                    <label for="dealer_code">Code</label>
                    <input type="text" class="form-control" id="dealer_code" name="dealer_code" value="{{ old('dealer_code') }}" >
                    @if ($errors->has('dealer_code'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_code') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_name">Name</label>
                    <input type="text" class="form-control" id="dealer_name" name="dealer_name"  value="{{ old('dealer_name') }}" >
                    @if ($errors->has('dealer_name'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_name') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_category">Category</label>
                    <select id="dealer_category" name="dealer_category" class="select form-select select-hidden-accessible">
                        <option value="">--</option>
                        <option value="TSA" {{ (old('dealer_category') == "TSA") ? "selected" : "" }}>Trade Specife Agent(TSA)</option>
                        <option value="salesman" {{ (old('dealer_category') == "salesman") ? "selected" : "" }}>Sales Man</option>
                    </select>
                    @if ($errors->has('dealer_category'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_category') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_gst_reg">GST Registraion Company</label>
                    <select id="dealer_gst_reg" name="dealer_gst_reg" class="select form-select select-hidden-accessible">
                        <option value="0">NO</option>
                        <option value="1" {{ (old('dealer_gst_reg') == 1) ? "selected" : "" }}>YES</option>
                    </select>
                    @if ($errors->has('dealer_gst_reg'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_gst_reg') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_blk_no">Block Number</label>
                    <input type="text" class="form-control" id="dealer_blk_no" name="dealer_blk_no"  value="{{ old('dealer_blk_no') }}" >
                    @if ($errors->has('dealer_blk_no'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_blk_no') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_street">Street Name</label>
                    <input type="text" class="form-control" id="dealer_street" name="dealer_street"  value="{{ old('dealer_street') }}" >
                    @if ($errors->has('dealer_street'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_street') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_unit_no">Unit Number</label>
                    <input type="text" class="form-control" id="dealer_unit_no" name="dealer_unit_no"  value="{{ old('dealer_unit_no') }}" >
                    @if ($errors->has('dealer_unit_no'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_unit_no') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_country">Country</label>
                    <select name="dealer_country" id="dealer_country" class="select2 select2-basic form-select select2-hidden-accessible">
                        @foreach($country_list as $cli)
                            <option value="{{ $cli['country_id'] }}" {{ (old('dealer_country') == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('dealer_country'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_country') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_postalcode">Postal Code</label>
                    <input type="text" class="form-control" id="dealer_postalcode" name="dealer_postalcode"  value="{{ old('dealer_postalcode') }}" >
                    @if ($errors->has('dealer_postalcode'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_postalcode') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_contact_name">Contact Name.</label>
                    <input type="text" class="form-control" id="dealer_contact_name" name="dealer_contact_name"  value="{{ old('dealer_contact_name') }}" >
                    @if ($errors->has('dealer_contact_name'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_contact_name') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_payableto">Payable To</label>
                    <input type="text" class="form-control" id="dealer_payableto" name="dealer_payableto"  value="{{ old('dealer_payableto') }}" >
                    @if ($errors->has('dealer_payableto'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_payableto') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_status">Status</label>
                    <select id="dealer_status" name="dealer_status" class="select form-select select-hidden-accessible">
                        <option value="1">ACTIVE</option>
                        <option value="0" {{ ( old('dealer_status') && old('dealer_status') == 0) ? "selected" : "" }}>IN-ACTIVE</option>
                    </select>
                    @if ($errors->has('dealer_status'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_status') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_file">Attachment</label>
                    <div class="input-group mb-3">
                        <input type="file" class="form-control" id="dealer_file" name="dealer_file">
                        <label class="input-group-text" for="dealer_file">File Upload</label>
                    </div>
                    @if ($errors->has('dealer_file'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_file') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="form-group-title mb-5">Other Info</h4>
                <div class="form-group mb-3">
                    <label for="dealer_telo">Tel(Office)</label>
                    <input type="text" class="form-control" id="dealer_telo" name="dealer_telo"  value="{{ old('dealer_telo') }}" >
                    @if ($errors->has('dealer_telo'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_telo') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_telm">Tel(Mobile)</label>
                    <input type="text" class="form-control" id="dealer_telm" name="dealer_telm"  value="{{ old('dealer_telm') }}" >
                    @if ($errors->has('dealer_telm'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_telm') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_telf">Tel(Fax)</label>
                    <input type="text" class="form-control" id="dealer_telf" name="dealer_telf"  value="{{ old('dealer_telf') }}" >
                    @if ($errors->has('dealer_telf'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_telm') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_email">Email</label>
                    <input type="text" class="form-control" id="dealer_email" name="dealer_email"  value="{{ old('dealer_email') }}" >
                    @if ($errors->has('dealer_email'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_email') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="dealer_remark">Remark</label>
                    <textarea class="form-control" rows="5" id="dealer_remark" name="dealer_remark">{{ old('dealer_remark')}}</textarea>
                    @if ($errors->has('dealer_remark'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('dealer_remark') }}</span>
                    @endif
                </div>
                <h4 class="form-group-title mb-5">Login Info</h4>
                <div class="form-group mb-3">
                    <label for="group_id">Dealer Group</label>
                    <select id="group_id" name="group_id" class="select form-select select-hidden-accessible">
                        <option value="0">No Group</option>
                        @foreach($group_list as $grp_li)
                            <option value="{{ $grp_li['group_id'] }}" {{ ( old('group_id') && old('group_id') == $grp_li['group_id']) ? "selected" : "" }}>{{ $grp_li['group_code'] }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('group_id'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('group_id') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="cprofile">Dealer Company Profile</label>
                    <select id="cprofile" name="cprofile" class="select form-select select-hidden-accessible">
                        @foreach($cprofile_list as $grp_li)
                            <option value="{{ $grp_li['cprofile_id'] }}" {{ ( old('cprofile') && old('cprofile') == $grp_li['cprofile_id']) ? "selected" : "" }}>{{ $grp_li['cprofile_name'] }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('cprofile'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('cprofile') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="login_email">Login Email</label>
                    <input type="email" class="form-control" id="login_email" name="login_email" value="{{ old('login_email') }}" >
                    @if ($errors->has('login_email'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('login_email') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" >
                    @if ($errors->has('password'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="cfm_password">Confirm Password</label>
                    <input type="password" class="form-control" id="cfm_password" name="cfm_password" placeholder="Confirm Password" value="" >
                    @if ($errors->has('cfm_password'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('cfm_password') }}</span>
                    @endif
                </div>
                <div class="form-group mb-3">
                    <label for="login_status">Login Status</label>
                    <select id="login_status" name="login_status" class="select form-select select-hidden-accessible">
                        <option value="1" {{ (old('login_status') == "1") ? "selected" : "" }}>Active</option>
                        <option value="0" {{ (old('login_status') == "0") ? "selected" : "" }}>No Active</option>
                    </select>
                    @if ($errors->has('login_status'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('login_status') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="text-right">
            <a href="{{ route('TSAList') }}"  class="btn btn-info mr-2"> Back </a>
            <button type="submit" class="btn btn-success">Add TSA</button>
        </div>
    </form>
</div>    
</div>
@include('includes.footer')