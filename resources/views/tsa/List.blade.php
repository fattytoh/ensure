@include('includes.header')
<div class="card">
<div class="card-body">
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <div class="text-right mb-3">
        <a href="{{ route('TSAAddNew') }}" class="btn btn-primary">Add TSA</a>
    </div> 
    <h1>TSA Management</h1>
    <table id="maintable" class="table petlist-table">
        <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Category</th>
                <th>Contact Name</th>
                <th>Contact No.</th>
                <th>Status</th>
                <th>Create By</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($list_data as $key => $li)
                <tr>
                    <td>{{ $li['dealer_code'] }}</td>
                    <td>{{ $li['dealer_name'] }}</td>
                    <td>{{ $li['dealer_category'] }}</td>
                    <td>{{ $li['dealer_contact_name'] }}</td>
                    <td>{{ $li['dealer_contact'] }}</td>
                    <td>{{ ($li['dealer_status'] == 1)?'ACTIVE':'IN-ACTIVE'}}</td>
                    <td>{{ $li['empl_name'] }}</td>
                    <td>
                        <a href="{{ route('TSAView', ['id' => $li['dealer_id'] ]) }}" class="btn btn-warning mr-2">Edit</a>
                        <a href="{{ route('TSADelete', ['id' => $li['dealer_id'] ]) }}" class="btn btn-danger del_btn" rel="{{ $li['dealer_name'] }}" >Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@include('includes.footer')