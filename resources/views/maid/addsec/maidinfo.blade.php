<h4 class="form-group-title mb-5">Maid Details</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_maid_passno">Passport No</label>
            <input type="text" class="form-control" id="order_maid_passno" name="order_maid_passno" value="{{ old('order_maid_passno')}}" >
            @if ($errors->has('order_maid_passno'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_maid_passno') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_maid_country">Nationality</label>
            <select name="order_maid_country" id="order_maid_country" class="select2 select2-basic form-select select2-hidden-accessible copy_field">
                @foreach($country_list as $cli)
                    <option value="{{ $cli['country_id'] }}" {{ (old('order_maid_country') == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                @endforeach
            </select>
            @if ($errors->has('order_maid_country'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_maid_country') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_maid_name">Maid Name</label>
            <input type="text" class="form-control" id="order_maid_name" name="order_maid_name" value="{{ old('order_maid_name')}}" >
            @if ($errors->has('order_maid_name'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_maid_name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_maid_permit">Work Permit No</label>
            <input type="text" class="form-control" id="order_maid_permit" name="order_maid_permit" value="{{ old('order_maid_permit')}}" >
            @if ($errors->has('order_maid_permit'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_maid_permit') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_maid_dob">Maid DOB</label>
            <input type="text" class="form-control cover_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_maid_dob" name="order_maid_dob" value="{{ old('order_maid_dob') }}" readonly="readonly">
            @if ($errors->has('order_maid_dob'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_maid_dob') }}</span>
            @endif
        </div>
    </div>
</div>
<h4 class="form-group-title mb-5">Address Details</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_postcode">Postal Code</label>
            <input type="text" class="form-control" id="order_cli_postcode" name="order_cli_postcode" value="{{ old('order_cli_postcode')}}" >
            @if ($errors->has('order_cli_postcode'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_postcode') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_blkno">Block No</label>
            <input type="text" class="form-control" id="order_cli_blkno" name="order_cli_blkno" value="{{ old('order_cli_blkno')}}" >
            @if ($errors->has('order_cli_blkno'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_blkno') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_street">Street Name</label>
            <input type="text" class="form-control" id="order_cli_street" name="order_cli_street" value="{{ old('order_cli_street')}}" >
            @if ($errors->has('order_cli_street'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_street') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_country">Country</label>
            <select name="order_cli_country" id="order_cli_country" class="select2 select2-basic form-select select2-hidden-accessible copy_field">
                @foreach($country_list as $cli)
                    <option value="{{ $cli['country_id'] }}" {{ (old('order_cli_country') == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                @endforeach
            </select>
            @if ($errors->has('order_cli_country'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_country') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_cli_address">Address</label>
    <textarea  class="form-control" id="order_cli_address" name="order_cli_address" rows="5">{{ old('order_cli_address') }}</textarea>
    @if ($errors->has('order_cli_address'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_address') }}</span>
    @endif
</div>
<h4 class="form-group-title mb-5">Contact Details</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_telresidence">Tel (R)</label>
            <input type="text" class="form-control" id="order_cli_telresidence" name="order_cli_telresidence" value="{{ old('order_cli_telresidence')}}" >
            @if ($errors->has('order_cli_telresidence'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_telresidence') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_teloffice">Tel (O)</label>
            <input type="text" class="form-control" id="order_cli_teloffice" name="order_cli_teloffice" value="{{ old('order_cli_teloffice')}}" >
            @if ($errors->has('order_cli_teloffice'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_teloffice') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_telmobile">Tel (M)</label>
            <input type="text" class="form-control" id="order_cli_telmobile" name="order_cli_telmobile" value="{{ old('order_cli_telmobile')}}" >
            @if ($errors->has('order_cli_telmobile'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_telmobile') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cli_email">Email</label>
            <input type="email" class="form-control" id="order_cli_email" name="order_cli_email" value="{{ old('order_cli_email')}}" >
            @if ($errors->has('order_cli_email'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cli_email') }}</span>
            @endif
        </div>
    </div>
</div>