<h4 class="form-group-title mb-5">Insurance Basic info</h4>
<div class="row mb-3">
    <div class="col-md-6">
        <label for="order_record_billto">Bill To</label>
        <select name="order_record_billto" id="order_record_billto" class="select form-select select-hidden-accessible">
        <option value="TO TSA" {{ (old('order_record_billto') == 'TO TSA') ? "selected" : (($order_record_billto == 'TO TSA') ? "selected" : "" ) }}>To TSA</option>
            <option value="TO Customer" {{ (old('order_record_billto') == 'TO Customer') ? "selected" : (($order_record_billto == 'TO Customer') ? "selected" : "") }}>To Customer</option>
            <option value="TO Salesman" {{ (old('order_record_billto') == 'TO Salesman') ? "selected" : (($order_record_billto == 'TO Salesman') ? "selected" : "") }}>To Salesman</option>
        </select>
        @if ($errors->has('order_record_billto'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_record_billto') }}</span>
        @endif
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_method"> Order From</label>
            <select name="order_method" id="order_method" class="select form-select select-hidden-accessible">
                <option value="Employment Agency" {{ ($order_method == 'Employment Agency') ? "selected" : "" }}>Employment Agency</option>
                <option value="Online Order" {{ ($order_method == 'Online Order') ? "selected" : "" }}>Online Order</option>
                <option value="Walk In Customer" {{ ($order_method == 'Walk In Customer') ? "selected" : "" }}>Walk In Customer</option>
            </select>
            @if ($errors->has('order_method'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_method') }}</span>
            @endif
        </div>
    </div>
</div>
@if($order_ref > 0)
<div class="row mb-3">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_doc_type">Endorsement Type</label>
            <select name="order_doc_type" id="order_doc_type" class="select form-select select-hidden-accessible" {{ $disabled }}>
                <option value="DN" {{ ($order_doc_type == 'DN') ? "selected" : "" }}>Debit Notes</option>
                <option value="CN" {{ ($order_doc_type == 'CN') ? "selected" : "" }}>Credit Notes</option>
            </select>
            @if ($errors->has('order_doc_type'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_doc_type') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_endorsement_type">Endorsement Purpose</label>
            <input type="text" class="form-control" id="order_endorsement_type" name="order_endorsement_type" value="{{ old('order_endorsement_type')?:$order_endorsement_type }}" >
        </div>
    </div>
</div>
@else
<input type = "hidden" value = "{{ $order_doc_type }}" name = "order_doc_type" id="order_doc_type"/>
@endif
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_no">Debit / Credit Notes Number</label>
            <input type="text" class="form-control" id="order_no" name="order_no" value="{{ $order_no }}" readonly>
            @if ($errors->has('order_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_no') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_date">Insurance Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_date" name="order_date" readonly="readonly" value="{{ old('order_date')?:$order_date }}">
            @if ($errors->has('order_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_date') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_customer">Customer</label>
            <select name="order_customer" id="order_customer" class="select2 form-select select2-hidden-accessible">
            @if(old('order_customer'))
                {!! app('App\Http\Controllers\SelectController')->getCustomerOption(old('order_customer')) !!}
            @elseif($order_customer  != 0)
                <option value="{{ $order_customer }}" selected>{{ $customer_name.' - '.$customer_ic }}</option>
            @endif
            </select>
            @if ($errors->has('order_customer'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_customer') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_empl_sbtran_ref">SB Transmission Ref No </label>
            <input type="text" class="form-control" id="order_empl_sbtran_ref" name="order_empl_sbtran_ref" value="{{ old('order_empl_sbtran_ref')?:$order_empl_sbtran_ref}}" >
            @if ($errors->has('order_empl_sbtran_ref'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_empl_sbtran_ref') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_dealer">TSA</label>
            <select name="order_dealer" id="order_dealer" class="select2 form-select select2-hidden-accessible">
            @if(old('order_dealer'))
                {!! app('App\Http\Controllers\SelectController')->getTSAOption(old('order_dealer')) !!}
            @elseif($order_dealer != 0)
                <option value="{{ $order_dealer }}" selected>{{ $dealer_code.' - '.$dealer_name }}</option>
            @endif
            </select>
            @if ($errors->has('order_dealer'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_dealer') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_insurco">Insurance Company</label>
            <select name="order_insurco" id="order_insurco" class="select form-select select-hidden-accessible">
                @foreach($insuranceco_list as $li)
                    <option value="{{ $li['insuranceco_id'] }}" {{ ($order_insurco == $li['insuranceco_id']) ? "selected" : "" }} >{{ $li['insuranceco_code'] ." - ".$li['insuranceco_name']}}</option>
                @endforeach
            </select>
            @if ($errors->has('order_insurco'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_insurco') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_policyno">Policy Number</label>
            <input type="text" class="form-control" id="order_policyno" name="order_policyno" value="{{ old('order_policyno')?:$order_policyno }}" >
            @if ($errors->has('order_policyno'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_policyno') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_main_proposal mb-3">Proposer</label>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="order_main_proposal" name="order_main_proposal" value="Y" {{ ($order_main_proposal == 'Y') ? "checked" : ""  }}> 
                <label class="form-check-label" for="order_main_proposal">
                    Same as Main Insured
                </label>
            </div>
            @if ($errors->has('order_main_proposal'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_main_proposal') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_from">Coverage Date From</label>
            <input type="text" class="form-control cover_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_from" name="order_period_from" value="{{ old('order_period_from')?:$order_period_from }}" readonly="readonly">
            @if ($errors->has('order_period_from'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_from') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_to">Coverage Date To</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_to" name="order_period_to" value="{{ old('order_period_to')?:$order_period_to }}" readonly="readonly">
            @if ($errors->has('order_period_to'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_to') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period">Period</label>
            <select name="order_period" id="order_period" class="select form-select select-hidden-accessible">
                <option value="14" {{ ($order_period == '14') ? "selected" : "" }}>14 Months</option>
                <option value="26" {{ ($order_period == '26') ? "selected" : "" }}>26 Months</option>
            </select>
            @if ($errors->has('order_period'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_discharge_date">Discharge Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_discharge_date" name="order_discharge_date" value="{{ old('order_discharge_date')?:$order_discharge_date }}" readonly="readonly">
            @if ($errors->has('order_discharge_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_discharge_date') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_coverage">Coverage</label>
            <select name="order_coverage" id="order_coverage" class="select form-select select-hidden-accessible">
                <option value="">Select One</option>
                <option value="Classic Plan" {{ ($order_coverage == 'Classic Plan') ? "selected" : "" }}>Classic Plan</option>
                <option value="Deluxe Plan" {{ ($order_coverage == 'Deluxe Plan') ? "selected" : "" }}>Deluxe Plan</option>
                <option value="Exclusive Plan" {{ ($order_coverage == 'Exclusive Plan') ? "selected" : "" }}>Exclusive Plan</option>
            </select>
            @if ($errors->has('order_coverage'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_coverage') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <label for="order_waiver_indemnity">Exclude From One month rule</label>
        <select name="order_waiver_indemnity" id="order_waiver_indemnity" class="select form-select select-hidden-accessible">
            <option value="Y" {{ ($order_waiver_indemnity == 'Y') ? "selected" : "" }}>Yes</option>
            <option value="N" {{ ($order_waiver_indemnity == 'N') ? "selected" : "" }}>No</option>
        </select>
        @if ($errors->has('order_waiver_indemnity'))
            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_waiver_indemnity') }}</span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group mb-3">
            <label for="order_optional_coverage_plan mb-3">Optional Coverage Plan</label>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="order_optional_coverage_plan" name="order_optional_coverage_plan" value="2K" {{ (old('order_optional_coverage_plan') == '2K') ? "checked" : (($order_optional_coverage_plan == '2K')? "checked":"")  }}> 
                        <label class="form-check-label" for="order_optional_coverage_plan">
                            2K Philippines Bond (26 Months)
                        </label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="order_optional_coverage_plan" name="order_optional_coverage_plan" value="7K" {{ (old('order_optional_coverage_plan') == '7K') ? "checked" : (($order_optional_coverage_plan == '7K')? "checked":"")  }}> 
                        <label class="form-check-label" for="order_optional_coverage_plan">
                            7K Philippines Bond (26 Months)
                        </label>
                    </div>
                    @if ($errors->has('order_optional_coverage_plan'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_optional_coverage_plan') }}</span>
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="order_optional_coverage_plan1" name="order_optional_coverage_plan1" value="300MDC" {{ (old('order_optional_coverage_plan1') == '300MDC') ? "checked" : (($order_optional_coverage_plan1 == '300MDC')? "checked":"")  }}> 
                        <label class="form-check-label" for="order_optional_coverage_plan1">
                            Outpatient Medical Expenses $300
                        </label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="order_optional_coverage_plan1" name="order_optional_coverage_plan1" value="500MDC" {{ (old('order_optional_coverage_plan1') == '500MDC') ? "checked" : (($order_optional_coverage_plan1 == '500MDC')? "checked":"")  }}> 
                        <label class="form-check-label" for="order_optional_coverage_plan1">
                            Outpatient Medical Expenses $500
                        </label>
                    </div>
                    @if ($errors->has('order_optional_coverage_plan'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_optional_coverage_plan') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cancel_date">Termination Date</label>
            <input type="text" class="form-control basic_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_cancel_date" name="order_cancel_date" value="{{ old('order_cancel_date')?:$order_cancel_date }}" readonly="readonly">
            @if ($errors->has('order_cancel_date'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cancel_date') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cancel_reason">Cancel Reason</label>
            <textarea  class="form-control" id="order_cancel_reason" name="order_cancel_reason" rows="3">{{ old('order_cancel_reason')?:$order_cancel_reason }}</textarea>
            @if ($errors->has('order_cancel_reason'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cancel_reason') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ins_doc_no">Ins Document Number</label>
            <input type="text" class="form-control" id="order_ins_doc_no" name="order_ins_doc_no" value="{{ old('order_ins_doc_no')?:$order_ins_doc_no }}" >
            @if ($errors->has('order_ins_doc_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ins_doc_no') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_notes_remark">Remarks</label>
            <textarea  class="form-control" id="order_notes_remark" name="order_notes_remark" rows="3">{{ old('order_notes_remark')?:$order_notes_remark }}</textarea>
            @if ($errors->has('order_notes_remark'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_notes_remark') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 mb-3">
        <label for="order_exclude_option">Exclude From One month rule</label>
        <select name="order_exclude_option" id="order_exclude_option" class="select form-select select-hidden-accessible">
            <option value="N" {{ (old('order_exclude_option') == 'N') ? "selected" : (($order_exclude_option == 'N') ? "selected" : "") }}>No</option>
            <option value="Y" {{ (old('order_exclude_option') == 'Y') ? "selected" : (($order_exclude_option == 'Y') ? "selected" : "") }}>Yes</option>
        </select>
        @if ($errors->has('order_exclude_option'))
            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_exclude_option') }}</span>
        @endif
    </div>
</div>
