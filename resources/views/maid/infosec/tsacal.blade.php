<h4 class="form-group-title mb-5">TSA Fees Calculation</h4>
<div class="form-group mb-3">
    <label for="span_premium">Basic Premium</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <span class="form-control text-right span_premium" id="span_premium" name="span_premium"></span>
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_receivable_dealercommpercent">TSA Referral Fee</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_receivable_dealercommpercent" name="order_receivable_dealercommpercent" value="{{ old('order_receivable_dealercommpercent')?:$order_receivable_dealercommpercent }}" {{ $disabled }}>
            @if ($errors->has('order_receivable_dealercommpercent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_receivable_dealercommpercent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_receivable_dealercomm">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_receivable_dealercomm" name="order_receivable_dealercomm" value="{{ old('order_receivable_dealercomm')?:$order_receivable_dealercomm }}" {{ $disabled }}>
            @if ($errors->has('order_receivable_dealercomm'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_receivable_dealercomm') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_receivable_dealergst">Referral Fee GST</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <select class="form-control" id="order_receivable_dealergst" name="order_receivable_dealergst" {{ $disabled }}>
                <option value="N">N</option>
                <option value="Y" {{ ($order_receivable_dealergst == 'Y') ? "selected" : "" }}>Y</option>
            </select>
            @if ($errors->has('order_receivable_dealergst'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_receivable_dealergst') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_receivable_dealergstamount"></label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_receivable_dealergstamount" name="order_receivable_dealergstamount" value="{{ old('order_receivable_dealergstamount')?:$order_receivable_dealergstamount }}" {{ $disabled }}>
            @if ($errors->has('order_receivable_dealergstamount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_receivable_dealergstamount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_receivable_gstpercent">GST</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_receivable_gstpercent" name="order_receivable_gstpercent" value="{{ old('order_receivable_gstpercent')?:$order_receivable_gstpercent }}" {{ $disabled }}>
            @if ($errors->has('order_receivable_gstpercent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_receivable_gstpercent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_receivable_gstamount">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_receivable_gstamount" name="order_receivable_gstamount" value="{{ old('order_receivable_gstamount')?:$order_receivable_gstamount }}" {{ $disabled }}>
            @if ($errors->has('order_receivable_gstamount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_receivable_gstamount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="span_bank_interest">Bank Interest Charge</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <span class="form-control text-right span_bank_interest" id="span_bank_interest" name="span_bank_interest"></span>
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_receivable_premiumreceivable">TSA Charge Amount</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_receivable_premiumreceivable" name="order_receivable_premiumreceivable" value="{{ old('order_receivable_premiumreceivable')?:$order_receivable_premiumreceivable }}" {{ $disabled }}>
            @if ($errors->has('order_receivable_premiumreceivable'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_receivable_premiumreceivable') }}</span>
            @endif
        </div>
    </div>
</div>