<h4 class="form-group-title mb-5 mt-3">File List</h4>
<table id="FileTable" class="table petlist-table mb-5">
    <thead>
        <tr>
            <th>No</th>
            <th>File Name</th>
            <th>Upload By</th>
            <th>Upload Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($file_list as $file_ky => $file_li)
        <tr>
            <td>{{ ($file_ky + 1) }}</td>
            <td><a href="{{ $file_li['upload_path'].'/'.$file_li['files_name'] }}" target="_blank">{{ $file_li['files_ori_name'] }}</a></td>
            <td>{{ $file_li['insertBy'] }}</td>
            <td>{{ $file_li['created_at'] }}</td>
            <td>
                @if(!$disabled)
                    @PrivilegeCheck('MaidFileDelete')
                        <a href="{{ route('MaidFileDelete', ['id' => $file_li['files_id'] ]) }}" class="btn btn-danger del_btn" rel="{{ $file_li['files_ori_name'] }}"> Delete </a>
                    @endPrivilegeCheck
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<h4 class="form-group-title mb-5 mt-3">Record Section</h4>
<table id="RecordTable" class="table petlist-table mb-5">
    <thead>
        <tr>
            <th>No</th>
            <th>Document No</th>
            <th>Plan Type</th>
            <th>Policy Number</th>
            <th>Amount</th>
            <th>Employer Name</th>
            <th>Maid Name</th>
            <th>From</th>
            <th>To</th>
        </tr>
    </thead>
    <tbody>
        @php($total_price = 0)
        @foreach($record_list as $record_ky => $record_li)
            @if($record_li['order_doc_type'] == 'DN')
                @php($total_price = $total_price + $record_li['order_grosspremium_amount'])
            @else
                @php($total_price = $total_price - $record_li['order_grosspremium_amount'])
            @endif
        <tr>
            <td>{{ ($record_ky + 1) }}</td>
            <td>{!! $record_li['order_no'] !!}</td>
            <td>{{ $record_li['order_coverage'] }}</td>
            <td>{{ $record_li['order_policyno'] }}</td>
            <td>
                @if($record_li['order_doc_type'] == 'DN')
                    {{ $record_li['order_grosspremium_amount'] }}
                @else
                    ( {{ $record_li['order_grosspremium_amount'] }} )
                @endif
            </td>
            <td>{{ $record_li['partner_name'] }}</td>
            <td>{{ $record_li['order_maid_name'] }}</td>
            <td>{{ $record_li['order_period_from'] }}</td>
            <td>{{ $record_li['order_period_to'] }}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="4" style="text-align:right" rowspan="1">Total Balance:</th>
            <th rowspan="1" colspan="5">
                @if($total_price >= 0)
                    <span class="text-green">${{ number_format($total_price, 2, ".", ",")  }}</span>
                @else
                    <span class="text-red">${{ number_format($total_price, 2, ".", ",") }}</span>
                @endif
                
            </th>
        </tr>
    </tfoot>
</table>
<h4 class="form-group-title mb-5 mt-3">Receipt Section</h4>
<table id="ReceiptTable" class="table petlist-table mb-5">
    <thead>
        <tr>
            <th>No</th>
            <th>Receipt No</th>
            <th>Receipt Date</th>
            <th>Receipt Bank</th>
            <th>Receipt Cheque</th>
            <th>Amount Received</th>
            <th>Create By</th>
            <th>Attachment</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @php($total_receipt = 0)
        @if($receipt_list)
        @foreach($receipt_list as $receipt_ky => $receipt_li)
        @php($total_receipt = $total_receipt + $receipt_li['recpline_offset_amount'] )
        <tr>
            <td>{{ ($receipt_ky + 1) }}</td>
            <td>
                @if($receipt_li['is_edit'])
                    <a href="{{ route('ReceiptView', ['id' => $receipt_li['receipt_id'] ]) }}" target="_blank">{{ $receipt_li['receipt_no'] }}</a>
                @elseif($receipt_li['is_view'])
                    <a href="{{ route('ReceiptInfo', ['id' => $receipt_li['receipt_id'] ]) }}" target="_blank">{{ $receipt_li['receipt_no'] }}</a>
                @else
                    {{ $receipt_li['receipt_no'] }}
                @endif
            </td>
            <td>{{ $receipt_li['receipt_date'] }}</td>
            <td>{{ $receipt_li['receipt_bank'] }}</td>
            <td>{{ $receipt_li['receipt_cheque'] }}</td>
            <td>{{ $receipt_li['recpline_offset_amount'] }}</td>
            <td>{{ $receipt_li['insertBy'] }}</td>
            <td>
                @foreach($receipt_li['file_list'] as $rec_file_li)
                    <a href="{{ $rec_file_li['upload_path'].'/'.$rec_file_li['files_name'] }}" target="_blank" class="w-100">{{ $rec_file_li['files_ori_name'] }}</a>
                @endforeach
            </td>
            <td>
                @PrivilegeCheck('ReceiptDelete')
                    <a href="{{ route('ReceiptDelete', ['id' => $receipt_li['receipt_id'] ]) }}" class="btn btn-warning del_btn" rel="{{ $receipt_li['receipt_no'] }}">Delete Receipt</a>
                @endPrivilegeCheck
                @PrivilegeCheck('AllReceiptFileDelete')
                    <a href="{{ route('AllReceiptFileDelete', ['id' => $receipt_li['receipt_id'] ]) }}" class="btn btn-danger del_btn" rel="All {{ $receipt_li['receipt_no'] }} Attachment">Delete Attachment</a>
                @endPrivilegeCheck
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5" style="text-align:right" rowspan="1">Total:</th>
            <th rowspan="1" colspan="4">
                @if($total_receipt >= 0)
                    <span class="text-green">${{ number_format($total_receipt, 2, ".", ",")  }}</span>
                @else
                    <span class="text-red">${{ number_format($total_receipt, 2, ".", ",") }}</span>
                @endif
                
            </th>
        </tr>
    </tfoot>
</table>
<h4 class="form-group-title mb-5 mt-3">Payment Section</h4>
<table id="PaymentTable" class="table petlist-table mb-5">
    <thead>
        <tr>
            <th>No</th>
            <th>Payment No</th>
            <th>Payment Date</th>
            <th>Payment Bank</th>
            <th>Payment Cheque</th>
            <th>Payable</th>
            <th>Amount</th>
            <th>Create By</th>
            <th>Attachment</th>
        </tr>
    </thead>
    <tbody>
        @php($total_payment = 0)
        @if($payment_list)
        @foreach($payment_list as $payment__ky => $payment_li)
        @php($total_payment = $total_payment + $payment_li['payment_paid'] )
        <tr>
            <td>{{ ($receipt_ky + 1) }}</td>
            <td>
                @if($receipt_li['is_edit'])
                    <a href="{{ route('PaymentInfo', ['id' => $payment_li['payment_id'] ]) }}" target="_blank">{{ $payment_li['payment_no'] }}</a>
                @elseif($receipt_li['is_view'])
                    <a href="{{ route('PaymentView', ['id' => $payment_li['payment_id'] ]) }}" target="_blank">{{ $payment_li['payment_no'] }}</a>
                @else
                    {{ $receipt_li['payment_no'] }}
                @endif
            </td>
            <td>{{ $payment_li['payment_date'] }}</td>
            <td>{{ $payment_li['payment_bank'] }}</td>
            <td>{{ $payment_li['payment_cheque'] }}</td>
            <td>{{ $payment_li['order_amount'] }}</td>
            <td>{{ $payment_li['payment_paid'] }}</td>
            <td>{{ $payment_li['insertBy'] }}</td>
            <td>
                @foreach($payment_li['file_list'] as $pay_file_li)
                    <a href="{{ $pay_file_li['upload_path'].'/'.$pay_file_li['files_name'] }}" target="_blank" class="w-100">{{ $pay_file_li['files_ori_name'] }}</a>
                @endforeach
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr>
            <th colspan="6" style="text-align:right" rowspan="1">Total:</th>
            <th rowspan="1" colspan="3">
                @if($total_receipt >= 0)
                    <span class="text-green">${{ number_format($total_receipt, 2, ".", ",")  }}</span>
                @else
                    <span class="text-red">${{ number_format($total_receipt, 2, ".", ",") }}</span>
                @endif
                
            </th>
        </tr>
    </tfoot>
</table>