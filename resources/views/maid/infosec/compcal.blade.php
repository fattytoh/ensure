<h4 class="form-group-title mb-5">Company Payout Calculation</h4>
<div class="form-group mb-3">
    <label for="span_premium">Basic Premium</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <span class="form-control text-right span_premium" id="span_premium" name="span_premium">{{ $order_premium_amt }}</span>
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_payable_ourcommpercent">Our Comm</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_payable_ourcommpercent" name="order_payable_ourcommpercent" value="{{ old('order_payable_ourcommpercent')?:$order_payable_ourcommpercent }}" {{ $disabled }}>
            @if ($errors->has('order_payable_ourcommpercent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_ourcommpercent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_payable_ourcomm">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_payable_ourcomm" name="order_payable_ourcomm" value="{{ old('order_payable_ourcomm')?:$order_payable_ourcomm }}" {{ $disabled }}>
            @if ($errors->has('order_payable_ourcomm'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_ourcomm') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_direct_discount_amt">Discount Given</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <span class="form-control text-right" id="discount_give" >{{ $order_direct_discount_amt }}</span>
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_payable_nettcommpercent">Nett Comm</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_payable_nettcommpercent" name="order_payable_nettcommpercent" value="{{ old('order_payable_nettcommpercent')?:$order_payable_nettcommpercent }}" {{ $disabled }}>
            @if ($errors->has('order_payable_nettcommpercent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_nettcommpercent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_payable_ourcomm">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_payable_nettcomm" name="order_payable_nettcomm" value="{{ old('order_payable_nettcomm')?:$order_payable_nettcomm }}" {{ $disabled }}>
            @if ($errors->has('order_payable_nettcomm'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_nettcomm') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_payable_premiumpayable">GST of Comm</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <select class="form-control" id="order_payable_premiumpayable" name="order_payable_premiumpayable" {{ $disabled }}>
                <option value="N">N</option>
                <option value="Y" {{ ($order_payable_premiumpayable == 'Y') ? "selected" : "" }}>Y</option>
            </select>
            @if ($errors->has('order_payable_premiumpayable'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_premiumpayable') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_payable_gstcomm"></label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_payable_gstcomm" name="order_payable_gstcomm" value="{{ old('order_payable_gstcomm')?:$order_payable_gstcomm }}" {{ $disabled }}>
            @if ($errors->has('order_payable_gstcomm'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_gstcomm') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_payable_gstpercent">GST</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_payable_gstpercent" name="order_payable_gstpercent" value="{{ old('order_payable_gstpercent')?:$order_payable_gstpercent }}" {{ $disabled }}>
            @if ($errors->has('order_payable_gstpercent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_gstpercent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_payable_gst">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_payable_gst" name="order_payable_gst" value="{{ old('order_payable_gst')?:$order_payable_gst }}" {{ $disabled }}>
            @if ($errors->has('order_payable_gst'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_gst') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="span_bank_interest">Bank Interest Charge</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <span class="form-control text-right span_bank_interest" id="span_bank_interest" name="span_bank_interest"></span>
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_payable_premium">Premium Payable</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_payable_premium" name="order_payable_premium" value="{{ old('order_payable_premium')?:$order_payable_premium}}" {{ $disabled }}>
            @if ($errors->has('order_payable_premium'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_payable_premium') }}</span>
            @endif
        </div>
    </div>
</div>