<script type="text/javascript" src="{{ asset('assets/js/maid.js') }}"></script>
<script>
    $(document).ready(function(){        
        @if(isset($order_id) && $order_id > 0)
            $(".span_premium").html(changeNumberFormat(RoundNum($("#order_grosspremium_amount").val(),2)));
            $(".span_bank_interest").html(changeNumberFormat(RoundNum($("#order_bank_interest").val(),2)));
            $("#cus_dis_total").html(changeNumberFormat(RoundNum($("#order_grosspremium_amount").val(), 2)));
        @endif

        $('input[name="order_optional_coverage_plan"]').on('change', function() {
            $('input[name="order_optional_coverage_plan"]').not(this).prop('checked', false);
            planChange();
        });

        $('input[name="order_optional_coverage_plan1"]').on('change', function() {
            $('input[name="order_optional_coverage_plan1"]').not(this).prop('checked', false);
            planChange();
        });

        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        $("#order_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $("#order_customer").select2({
            placeholder: "Search for a customer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('Cusselect') }}",
                dataType: 'json',
            }
        });
        
        $("#order_premium_amt, #order_cover_plan_no_gst, #order_addtional_amt").keyup(function(){
            if(!$("#order_direct_discount_amt").val() && $("#order_direct_discount_percentage").val() > 0){
                var total = parseFloat(numberRebuild($("#order_premium_amt").val())) + parseFloat(numberRebuild($("#order_cover_plan_no_gst").val())) + parseFloat(numberRebuild($("#order_addtional_amt").val()));
                var amt = (numberRebuild($("#order_direct_discount_percentage").val()) / 100 * total);
                $("#order_direct_discount_amt").val(RoundNum(amt,2));
            }
            calculateMaidTotal();     
        });

        $(".sum_cal").click(function(){
            calculateMaidTotal();
        });


        $("#order_record_billto").change(function(){
            calculateMaidTotal();     
        });

        $("#order_bank_interest").keyup(function(){
            calculateMaidTotal();     
        });

        $("#order_coverage, #order_waiver_indemnity").change(function() {
            planChange();
        });

        $("#order_period_from").change(function(){
            var dt1 = $(this).val();
            var res = dt1.split("-");
            var finalDate = res.join('-');
            var dt = new Date(finalDate);
            var period = $("#order_period").val();
            if(!isNaN(dt)){
                dt.setMonth(dt.getMonth() + parseInt(period));
                dt.setDate(dt.getDate() - parseInt(1));
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var disdate = year + "-" + month + "-" + day;
                var disdate1 =  day + "/" + month + "/" + year;
                
                $("#order_period_to").val(disdate);
                $("#order_period_to").next().val(disdate1);
            }
        });
                
        $("#order_period").change(function(){
            if($("#order_period_from").val()){
                var dt1 = $("#order_period_from").val();
                var res = dt1.split("-");
                var finalDate = res.join('-');
                var dt = new Date(finalDate);
                var period = $(this).val();
                if(!isNaN(dt)){
                    dt.setMonth(dt.getMonth() + parseInt(period));
                    dt.setDate(dt.getDate() - parseInt(1));
                    var day = dt.getDate();
                    var month = dt.getMonth() + 1;
                    var year = dt.getFullYear();
                    if (day < 10) {
                        day = "0" + day;
                    }
                    if (month < 10) {
                        month = "0" + month;
                    }
                    var disdate = year + "-" + month + "-" + day;
                    var disdate1 =  day + "/" + month + "/" + year;
                    
                    $("#order_period_to").val(disdate);
                    $("#order_period_to").next().val(disdate1);
                }
            }
            planChange();
        });

        $("#order_cli_postcode").change(function(){
            $.ajax({
                url: "https://developers.onemap.sg/commonapi/search?returnGeom=N&getAddrDetails=Y&pageNum=1&searchVal="+$(this).val(), 
                dataType: "json",
                success: function(result){
                    $.each(result.results, function( index, value ) {
                        if(index == '0'){
                            $("#order_cli_blkno").val(value.BLK_NO);
                            $("#order_cli_street").val(value.ROAD_NAME);
                            $("#order_cli_address").html(value.ADDRESS);
                        }
                    });                           
                },
                error: function(result){
                    console.log("error");
                }
                
            }); 
        });

        $("#order_direct_discount_amt, #order_direct_discount_percentage").keyup(function(){
            var after_dis = parseFloat(numberRebuild($("#order_subtotal_amount").val())) + parseFloat(numberRebuild($("#order_cover_plan_no_gst").val())); 

            if($(this).attr('rel_type') == 'amt'){
                var per =  ($(this).val() / after_dis) * 100;
                $("#"+ $(this).attr('rel_opp')).val(RoundNum(per,2));
            }
            else{
                var amt = after_dis * ($(this).val() /100 );
                $("#"+ $(this).attr('rel_opp')).val(RoundNum(amt,2));
            }
            calculateMaidTotal();
        });
                
        $("#order_gst_percent, #order_gst_amount").keyup(function(){
            var after_dis = numberRebuild($("#order_premium_amt").val());

            if($(this).attr('rel_type') == 'amt'){
                var per =  ($(this).val() / after_dis) * 100;
                $("#"+ $(this).attr('rel_opp')).val(RoundNum(per,2));
            }
            else{
                var amt = after_dis * ($(this).val() /100 );
                $("#"+ $(this).attr('rel_opp')).val(RoundNum(amt,2));
            }
            calculateMaidTotal();
        });

        $("#order_direct_discount_amt, #order_direct_discount_percentage").keyup(function(){
            var after_dis = parseFloat(numberRebuild($("#order_subtotal_amount").val())); 

            if($(this).attr('rel_type') == 'amt'){
                var per =  ($(this).val() / after_dis) * 100;
                $("#"+ $(this).attr('rel_opp')).val(RoundNum(per,2));
            }
            else{
                var amt = after_dis * ($(this).val() /100 );
                $("#"+ $(this).attr('rel_opp')).val(RoundNum(amt,2));
            }
            calculateMaidTotal();
        });
    })
</script>