@include('includes.header')
@inject('provider', 'App\Http\Controllers\AllianzController')
<div class="card">
<div class="card-body">
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <div class="text-right mb-3">
        @PrivilegeCheck('MaidAddNew')
        <a href="{{ route('MaidAddNew') }}" class="btn btn-primary">Add Maid Insurance</a>
        @endPrivilegeCheck
    </div> 
    <h1>Maid Insurance Management</h1>
    @include('maid.filter')  
    <table id="maidtable" class="table petlist-table" >
        <thead>
            <tr>
                <th>No</th>
                <th>Order No</th>
                <th>Maid Name</th>
                <th>Customer</th>
                <th>Order Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
<script>
    $(document).ready(function(){
        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        orderTable();
        
        $(".filter_btn").click(function(){
            orderTable();
        })
    })

    function orderTable(){
        $('#maidtable').DataTable().clear().destroy();
        $("#maidtable").DataTable({
            ajax: {
                url: "{{ route('MaidListData') }}",
                type: "POST",
                'data': {
                    order_policyno : $("#order_policyno").val(),
                    partner_nirc : $("#partner_nirc").val(),
                    order_maid_name : $("#order_maid_name").val(),
                    order_period_from : $("#order_period_from").val(),
                    dealer_name : $("#dealer_name").val(),
                    partner_name : $("#partner_name").val(),
                    order_period_to : $("#order_period_to").val(),
                }
            },
            processing: true,
            serverSide: true,
            columnDefs: [
                { orderable: false, targets: 0 },
                { orderable: false, targets: -1 }
            ]
        });
    }
</script>
@include('includes.footer')