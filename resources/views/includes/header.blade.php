<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light-style layout-menu-fixed">
    <head>
        <meta charset="utf-8">
        <meta name="_token" content="{{ csrf_token() }}">
        <base href="{{url('/')}}/"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="{{ asset('assets/images/logo.png') }}">
        <title><?php echo env('APP_NAME'); ?></title>
        <!-- <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet"> -->
        <link href="{{ asset('assets/css/base.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/theme-default.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/apex-charts.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/datatables.bootstrap5.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/button.datatables.bootstrap5.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/perfect-scrollbar.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/typeahead.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fontawesome/css/fontawesome.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fonts/boxicons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fonts/flag-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fonts/fontawesome.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fontawesome/css/brands.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fontawesome/css/solid.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/select2/select2.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/flatpickr/dist/flatpickr.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
        <script type="text/javascript" src="{{ asset('assets/js/jquery-3.7.0.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/template-customizer.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/helpers.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/config.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugin/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugin/axios/axios.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugin/select2/select2.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugin/flatpickr/dist/flatpickr.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugin/jsencrypt/bin/jsencrypt.min.js') }}"></script>
    </head>
    <body>
        <div class="block-layer"></div>
        <div class="layout-wrapper layout-content-navbar">
            <div class="layout-container">
                @include('includes.nav')
                <div class="layout-page">
                    @include('includes.headernav')
                    <div class="content-wrapper">
                    <!-- Content -->
                    <div class="container-xxl flex-grow-1 container-p-y">