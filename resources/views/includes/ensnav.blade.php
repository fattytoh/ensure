<li class="menu-item">
    <a href="{{ route('dashboard') }}" class="menu-link">
        <i class="fa-solid fa-house menu-icon"></i>
        <div>Dashboard</div>
    </a>
</li>
@PrivilegeGrpCheck(['EmplList', 'TSAList', 'CustomerList', 'UserGroupList'])
<li class="menu-item">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
        <i class="fa-solid fa-users menu-icon"></i>
        <div>Users</div>
    </a>
    <ul class="menu-sub">
        @PrivilegeCheck('EmplList')
        <li class="menu-item">
            <a href="{{ route('EmplList') }}" class="menu-link">
                <div>Employee</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('UserGroupList')
        <li class="menu-item">
            <a href="{{ route('UserGroupList') }}" class="menu-link">
                <div>User Group</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('TSAList')
        <li class="menu-item">
            <a href="{{ route('TSAList') }}" class="menu-link">
                <div>TSA</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('CustomerList')
        <li class="menu-item">
            <a href="{{ route('CustomerList') }}" class="menu-link">
                <div>Customer</div>
            </a>
        </li>
        @endPrivilegeCheck
    </ul>
</li>
@endPrivilegeGrpCheck
@PrivilegeGrpCheck(['GeneralList', 'MotorList', 'PrivateMotorList', 'CommercialMotorList', 'MaidList'])
<li class="menu-item">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
        <i class="fa-solid fa-hospital-user menu-icon"></i>
        <div>Insurance</div>
    </a>
    <ul class="menu-sub">
        @PrivilegeCheck('MotorList')
        <li class="menu-item">
            <a href="{{ route('MotorList') }}" class="menu-link">
                <div>Motor Insurance</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('PrivateMotorList')
        <li class="menu-item">
            <a href="{{ route('PrivateMotorList') }}" class="menu-link">
                <div>Private Motor Insurance</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('CommercialMotorList')
        <li class="menu-item">
            <a href="{{ route('CommercialMotorList') }}" class="menu-link">
                <div>Commercial Motor Insurance</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('MaidList')
        <li class="menu-item">
            <a href="{{ route('MaidList') }}" class="menu-link">
                <div>Maid Insurance</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('GeneralList')
        <li class="menu-item">
            <a href="{{ route('GeneralList') }}" class="menu-link">
                <div>General Insurance</div>
            </a>
        </li>
        @endPrivilegeCheck
    </ul>
</li>
@endPrivilegeGrpCheck
@PrivilegeCheck('optionIndex')
<li class="menu-item">
    <a href="{{ route('optionIndex') }}" class="menu-link">
        <i class="fa-solid fa-gear menu-icon"></i>
        <div>General Setting</div>
    </a>
</li>
@endPrivilegeCheck
@PrivilegeGrpCheck(['PaymentList', 'ReceiptList'])
<li class="menu-item">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
        <i class="fa-solid fa-file-invoice-dollar menu-icon"></i>
        <div>CashBook</div>
    </a>
    <ul class="menu-sub">
        @PrivilegeCheck('PaymentList')
        <li class="menu-item">
            <a href="{{ route('PaymentList') }}" class="menu-link">
                <div>Payment</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeCheck('ReceiptList')
        <li class="menu-item">
            <a href="{{ route('ReceiptList') }}" class="menu-link">
                <div>Receipt</div>
            </a>
        </li>
        @endPrivilegeCheck
    </ul>
</li>
@endPrivilegeGrpCheck