                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/apexcharts.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/hammer.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/menu.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/datatables-bootstrap5.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/popper.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/perfect-scrollbar.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/typeahead.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/admin.js') }}"></script>
        <script>
             $(document).ready(function(){
                $(".del_btn").click(function(event){
                    event.preventDefault();
                    var name = $(this).attr('rel');
                    Swal.fire({
                        title: "Do you want to delete " + name +"?",
                        showCancelButton: true,
                        confirmButtonText: "Delete",
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            window.location = $(this).attr('href');
                        } 
                    });
                });
                
                $(".flatpickr-basic-input").flatpickr({
                    dateFormat: "Y-m-d",
                });
                @if (session()->has('sweet_error_msg'))
                    Swal.fire({
                        icon: 'error',
                        title: "{{ session()->get('sweet_error_title') }}",
                        text: "{{ session()->get('sweet_error_msg') }}"
                    })
                @endif
                @if (session()->has('sweet_success_msg'))
                    Swal.fire({
                        icon: 'success',
                        title: "{{ session()->get('sweet_success_title') }}",
                        text: "{{ session()->get('sweet_success_msg') }}"
                    })
                @endif
            })

            function changeFormDateFormat(inputDate){
                var dt = new Date(inputDate);
                if(!isNaN(dt)){
                    var day = dt.getDate();
                    var month = dt.getMonth() + 1;
                    var year = dt.getFullYear();
                    if (day < 10) {
                        day = "0" + day;
                    }
                    if (month < 10) {
                        month = "0" + month;
                    }
                    return day +'/'+ month + '/' + year;
                }
                else{
                    return inputDate;
                }
            }
        </script>
    </body>
</html>