<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <base href="{{url('/')}}/"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="icon" type="image/x-icon" href="{{ asset('assets/images/logo.png') }}"> -->
        <title><?php echo env('APP_NAME'); ?></title>
        <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/base.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/theme-default.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fontawesome/css/fontawesome.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fontawesome/css/brands.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugin/fontawesome/css/solid.css') }}" rel="stylesheet">
        <script type="text/javascript" src="{{ asset('assets/js/jquery-3.7.0.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugin/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugin/axios/axios.min.js') }}"></script>
    </head>
    <body>
        <div class="container-xxl">