<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme" data-bg-class="bg-menu-theme" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    <div class="app-brand demo">
        <a href="{{ route('dashboard') }}" class="app-brand-link">
            <span class="app-brand-logo demo">
                @if (env('APP_ID')=='3')
                    <i class="fa-solid fa-p menu-icon"></i>
                @elseif(env('APP_ID')=='2')
                    <i class="fa-solid fa-e menu-icon"></i>
                @endif
            </span>
            <span class="app-brand-text demo menu-text fw-bold ms-2"><?php echo env('APP_NAME'); ?></span>
        </a>
        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>
    <div class="menu-inner-shadow"></div>
    <ul class="menu-inner py-1 ps ps--active-y">
        @if(auth()->user()->cprofile == 1)
        <li class="menu-item">
            <a href="{{ route('SwitchSite') }}" target="_blank" class="menu-link">
                <i class="fa-solid fa-repeat menu-icon"></i>
                <div>Switch Site</div>
            </a>
        </li>
        @endif
        <li class="menu-item">
            <a href="{{ route('dashboard') }}" class="menu-link">
                <i class="fa-solid fa-house menu-icon"></i>
                <div>Dashboard</div>
            </a>
        </li>
        @PrivilegeGrpCheck(['EmplList', 'TSAList', 'CustomerList', 'UserGroupList'])
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="fa-solid fa-users menu-icon"></i>
                <div>Users</div>
            </a>
            <ul class="menu-sub">
                @PrivilegeCheck('EmplList')
                <li class="menu-item">
                    <a href="{{ route('EmplList') }}" class="menu-link">
                        <div>Employee</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('UserGroupList')
                <li class="menu-item">
                    <a href="{{ route('UserGroupList') }}" class="menu-link">
                        <div>User Group</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('TSAList')
                <li class="menu-item">
                    <a href="{{ route('TSAList') }}" class="menu-link">
                        <div>TSA</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('CustomerList')
                <li class="menu-item">
                    <a href="{{ route('CustomerList') }}" class="menu-link">
                        <div>Customer</div>
                    </a>
                </li>
                @endPrivilegeCheck
            </ul>
        </li>
        @endPrivilegeGrpCheck
        @PrivilegeGrpCheck(['AllianzList', 'SompoList', 'MotorList', 'PrivateMotorList', 'CommercialMotorList', 'MaidList', 'GeneralList'])
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="fa-solid fa-hospital-user menu-icon"></i>
                <div>Insurance</div>
            </a>
            <ul class="menu-sub">
                @PrivilegeCheck('MotorList')
                <li class="menu-item">
                    <a href="{{ route('MotorList') }}" class="menu-link">
                        <div>Motor Insurance</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('PrivateMotorList')
                <li class="menu-item">
                    <a href="{{ route('PrivateMotorList') }}" class="menu-link">
                        <div>Private Motor Insurance</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('CommercialMotorList')
                <li class="menu-item">
                    <a href="{{ route('CommercialMotorList') }}" class="menu-link">
                        <div>Commercial Motor Insurance</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @if (env('APP_ID')=='3')
                    @PrivilegeCheck('AllianzList')
                    <li class="menu-item">
                        <a href="{{ route('AllianzList') }}" class="menu-link">
                            <div>Allianz Insurance</div>
                        </a>
                    </li>
                    @endPrivilegeCheck
                @endif
                @PrivilegeCheck('MaidList')
                <li class="menu-item">
                    <a href="{{ route('MaidList') }}" class="menu-link">
                        <div>Maid Insurance</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('MaidList')
                <li class="menu-item">
                    <a href="{{ route('GeneralList') }}" class="menu-link">
                        <div>General Insurance</div>
                    </a>
                </li>
                @endPrivilegeCheck
            </ul>
        </li>
        @endPrivilegeGrpCheck
        @PrivilegeCheck('optionIndex')
        <li class="menu-item">
            <a href="{{ route('optionIndex') }}" class="menu-link">
                <i class="fa-solid fa-gear menu-icon"></i>
                <div>General Setting</div>
            </a>
        </li>
        @endPrivilegeCheck
        @PrivilegeGrpCheck(['PaymentList','PaymentTsaList', 'PaymentInsList', 'ReceiptList'])
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="fa-solid fa-file-invoice-dollar menu-icon"></i>
                <div>CashBook</div>
            </a>
            <ul class="menu-sub">
                @PrivilegeCheck('PaymentList')
                <li class="menu-item">
                    <a href="{{ route('PaymentList') }}" class="menu-link">
                        <div>Payment for Customer</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('PaymentTsaList')
                <li class="menu-item">
                    <a href="{{ route('PaymentTsaList') }}" class="menu-link">
                        <div>Payment For TSA</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('PaymentInsList')
                <li class="menu-item">
                    <a href="{{ route('PaymentInsList') }}" class="menu-link">
                        <div>Payment For Insurer</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('ReceiptList')
                <li class="menu-item">
                    <a href="{{ route('ReceiptList') }}" class="menu-link">
                        <div>Receipt</div>
                    </a>
                </li>
                @endPrivilegeCheck
            </ul>
        </li>
        @endPrivilegeGrpCheck
        @PrivilegeGrpCheck(['PremiumStateForm', 'DebitNotesForm', 'CreditNotesForm', 'DailyTransForm', 'CompSalesForm', 'CusSalesForm', 'TSASalesForm'])
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="fa-solid fa-hospital-user menu-icon"></i>
                <div>Account</div>
            </a>
            <ul class="menu-sub">
                @PrivilegeCheck('PremiumStateForm')
                <li class="menu-item">
                    <a href="{{ route('PremiumStateForm') }}" class="menu-link">
                        <div>Premium Statement Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('DebitNotesForm')
                <li class="menu-item">
                    <a href="{{ route('DebitNotesForm') }}" class="menu-link">
                        <div>Debit Note Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('CreditNotesForm')
                <li class="menu-item">
                    <a href="{{ route('CreditNotesForm') }}" class="menu-link">
                        <div>Credit Note Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('DailyTransForm')
                <li class="menu-item">
                    <a href="{{ route('DailyTransForm') }}" class="menu-link">
                        <div>Daily Transaction Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('CompSalesForm')
                <li class="menu-item">
                    <a href="{{ route('CompSalesForm') }}" class="menu-link">
                        <div>Company Sales Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('CusSalesForm')
                <li class="menu-item">
                    <a href="{{ route('CusSalesForm') }}" class="menu-link">
                        <div>Customer Sales Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('TSASalesForm')
                <li class="menu-item">
                    <a href="{{ route('TSASalesForm') }}" class="menu-link">
                        <div>TSA Sales Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
            </ul>
        </li>
        @endPrivilegeGrpCheck
        @PrivilegeGrpCheck(['TsaFeesForm', 'OutStandingForm', 'AccStatementForm'])
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="fa-solid fa-hospital-user menu-icon"></i>
                <div>Report</div>
            </a>
            <ul class="menu-sub">
                @PrivilegeCheck('TsaFeesForm')
                <li class="menu-item">
                    <a href="{{ route('TsaFeesForm') }}" class="menu-link">
                        <div>TSA Fees Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('OutStandingForm')
                <li class="menu-item">
                    <a href="{{ route('OutStandingForm') }}" class="menu-link">
                        <div>Outstanding Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
                @PrivilegeCheck('AccStatementForm')
                <li class="menu-item">
                    <a href="{{ route('AccStatementForm') }}" class="menu-link">
                        <div>Account Statement Report</div>
                    </a>
                </li>
                @endPrivilegeCheck
            </ul>
        </li>
        @endPrivilegeGrpCheck
    </ul>
</aside>