<script>
    $(document).ready(function(){
        @if ($errors->any())
            calTotal()
        @endif
        
        $(".flatpickr-input").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        orderTable();

        $("#payment_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $("#payment_customer").select2({
            placeholder: "Search for a customer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('Cusselect') }}",
                dataType: 'json',
            }
        });

        $("#payment_dealer").change(function(){
            orderTable();
        });

        $("#payment_customer").change(function(){
            orderTable();
        });

        $("#payment_insuranco").change(function(){
            orderTable();
        });

        $("#filterOrderBtn").click(function(){
            orderTable();
        });

        $(".del_ord_btn").click(function (){
            var ord_id = $(this).attr('rel');
            delOrderBtn(ord_id);
        });

        $(".offset_amt").keyup(function(){
            calTotal();
        });

        $("#addOrderBtn").click(function(){
            var input_post = new Array();
            $("input[name='order_case[]']:checked").each(function(i) {
                input_post.push($(this).val());
            });
            var old_post = new Array();
            $(".order_id_input").each(function(i) {
                old_post.push($(this).val());
            });

            $.ajax({
                type: "POST",
                url: "{{ route('PaymentInsAddOrder') }}",
                dataType: 'json',
                data:  {
                    payment_to : $("#payment_to").val(),
                    order_case : input_post,
                    old_post : old_post,
                },
                success: function(res) {
                    if(res.html){
                        $("#order_list_wrapper").append(res.html);
                    }
                    $(".del_ord_btn").click(function (){
                        var ord_id = $(this).attr('rel');
                        delOrderBtn(ord_id);
                    });

                    $(".offset_amt").keyup(function(){
                        calTotal();
                    });

                    calTotal();
                }
            })
        });
    });

    function delOrderBtn(order){
        $("#order_box_" + order).remove();
        calTotal();
    }

    function calTotal(){
        var total = 0;
        $(".offset_amt").each(function(i) {
            total = parseFloat(total) +  parseFloat($(this).val());
        });

        $("#total_offset").val(total);
        $("#payment_paid").val(total);
    }

    function orderTable(){
        $('#OrderTable').DataTable().clear().destroy();
        $("#OrderTable").DataTable({
            ajax: {
                url: "{{ route('PaymentOrderInsListData') }}",
                type: "POST",
                'data': {
                    payment_to : $("#payment_to").val(),
                    payment_dealer : $("#payment_dealer").val(),
                    payment_insuranco : $("#payment_insuranco").val(),
                    payment_customer : $("#payment_customer").val(),
                    fil_start_date : $("#fil_start_date").val(),
                    fil_end_date : $("#fil_end_date").val(),
                    fil_order_type : $("#fil_order_type").val(),
                    fil_policyno : $("#fil_policyno").val(),
                }
            },
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
        });
    }
</script>