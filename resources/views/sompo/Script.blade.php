<script type="text/javascript" src="{{ asset('assets/plugin/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugin/jquery-validation/dist/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/sompo.js') }}"></script>
<script>
    $(document).on("change","#order_coverage", function(){
        var driage = [];
        var cover = $("#order_coverage").val();
        if($("#sompo_driver_dob_1").val()){
            driage.push($("#sompo_driver_dob_1").val());
        }
        if($("#sompo_driver_dob_2").val()){
            driage.push($("#sompo_driver_dob_2").val());
        }
        if($("#sompo_driver_dob_3").val()){
            driage.push($("#sompo_driver_dob_3").val());
        }
        if($("#sompo_driver_dob_4").val()){
            driage.push($("#sompo_driver_dob_4").val());
        }
                        
        $.ajax({
            type: 'POST',
            url: "{{ route('Sompovplanperiod') }}",
            cache: false,
            dataType: 'json',
            data: { order_coverage: cover, dri_dob: driage},
            success: function(data) {
                if(data.status == 0){
                    alert(data.msg);
                    $("#order_coverage").val('').trigger("change");
                }
            },
            error:function(err) {
                console.log("ERRROR");
            }
        });
    });
                
    $(document).ready(function (){
        if(!$('#driver-block').is(":visible")){
            $('#dri_col').click();
        }

        $(".api_btn").click(function(){
            $("#api_sent").val(1);
            $("#SompoForm").submit();
        });

        $(".resend_btn").click(function(){
            $("#resend").val(1);
            $("#api_sent").val(1);
            $("#SompoForm").submit();
        });

        $(".dob_picker").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            minDate: "{{ $min_age }}",
            maxDate: "{{ $max_age }}",
        });

        $(".sompo_picker").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            minDate: "{{ date('Y-m-d') }}",
        });

        $("#order_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $(".basic-sel2").select2();
        
        $("input[type=text], textarea").bind("keyup change",function(){
            var inpclass = ['order_period_from', 'order_period_to','order_cus_dob', 'sompo_driver_dob_1', 'sompo_driver_dob_2', 'sompo_driver_dob_3', 'sompo_driver_dob_4'];
            if($(this).val()){
                var inpdata = $(this).val();
                if(jQuery.inArray( $(this).attr("id"), inpclass ) === -1){
                    $(this).val(inpdata.toUpperCase());
                }
            }
        });
                
       
                
        if($("#order_cus_email").val()){
            encyptdata($("#order_cus_email").val(), "order_cus_enc_email");
        }
        
        if($("#order_premium_amt").val() > 0){
            $(".span_premium").html(changeNumberFormat(RoundNum($("#order_premium_amt").val(),2)));
        };
        
        validateForm();
        
        $(".print1, .print").click(function(){
            var r = confirm("Are You Sure To print This?");
            if(r === true){
                window.open($(this).attr("link"));
            }
            else{
                return false;
            }
            
        });

        $("#claim_option").change(function(){
            if($(this).val() == "Y"){
                $("#claim_option").hide();
                $("#order_cus_claim").show();
                $("#claim_amount").show();
            }
        });
                
        $("#order_cus_claim").bind("change", function(){
            if($(this).val() >= 2){
                $(this).val(1);
                alert("Online quotation not available due to claims experience, Please Contact Ensure.");
            }
        });

        $("#order_cus_claim_amount").bind("keyup change", function(){
            if($(this).val() > 10000){
                $(this).val(10000);
                alert("Online quotation not available due to claims amount, Please Contact Ensure.");
            }
        });
                
        $("#order_vehicles_reg_year, #order_coverage").bind("keyup change", function(){
            var currdate = new Date();
            var fullyear = currdate.getFullYear();
            var carage = parseFloat(fullyear) - parseFloat($("#order_vehicles_reg_year").val());
            var coverage = $("#order_coverage").val();
            var popalert = false;
            var popmsg = "";
            if(coverage == 'MTMC01CP' && carage > 5 ){
                popalert = true;
                popmsg = "Vehicle age cannot more than 5 years for selected plan, Please contact Ensure.";
            }
            else if(coverage == 'MTMC01TF' && carage > 15){
                popalert = true;
                popmsg = "Vehicle age cannot more than 15 years for selected plan, Please contact Ensure.";
            }
            else if(coverage == 'MTMC01TP' && carage > 30){
                popalert = true;
                popmsg = "Vehicle age cannot more than 30 years for selected plan, Please contact Ensure.";
            }
            if(popalert === true){
                $("#order_vehicles_reg_year").val('');
                $("#order_coverage").val('').trigger('change');
                alert(popmsg);
            }
        });
        
        $("#order_financecode").change(function(){
                           
            if($(this).val() == 'HPN999'){
                $("#other_financecode_box").show();
            }
            else{
                $("#order_financecode_type_input").val('');
                $("#other_financecode_box").hide();
            }
        });

        $("#order_ncd_previousinsurer").change(function(){
                            
            if($(this).val() == 'other'){
                $("#order_input_ncd_previousinsurer_div").show();
            }
            else{
                $("#order_input_ncd_previousinsurer").val('');
                $("#order_input_ncd_previousinsurer_div").hide();
            }
        });
                
        $("#order_sompo_payment_method").change(function(){
            if($(this).val() == 'CRI'){
                $("#pay_period").show();
                $("#order_sompo_payment_installment").val('6 months');
            }
            else{
                $("#order_sompo_payment_installment").val('');
                $("#pay_period").hide();
            }
        });
                                 
        $(".enc_data_type").bind("keyup change", function(){
            var out_id = $(this).attr("enc_rel");
            encyptdata($(this).val(), out_id);
        });
                
        $(".copy_field").bind("keyup change", function(){
            if($("#order_cus_driving").val() == "Y"){
                var out_id = $(this).attr("id");
                var out_val = $(this).val();
                copyDriver(out_id, out_val);
            }
        });
                
        $("#order_cus_driving").change(function(){
            if($(this).val() == "Y"){
                $(".copy_field").each(function(){
                    var out_id = $(this).attr("id");
                    var out_val = $(this).val();
                    copyDriver(out_id, out_val);
                });
            }
            else{
                $(".copy_field").each(function(){
                    var out_id = $(this).attr("id");
                    var out_val = "";
                    copyDriver(out_id, out_val);
                });
            }
        });
                
        $(".dri_valid").bind("keyup change", function(){
            var out_id = $(this).attr("rel");
            if(out_id){
                encyptdata($(this).val(), out_id);
            }
        });
                
        $(".dri_nirc_valid").bind("keyup change", function(){
            var out_id = $(this).attr("rel");
            if(out_id){
                encyptdata($(this).val(), out_id);
            }
        });
                
        $(".occup_code").change(function(){
            if($(this).val() == "OCC9999"){
                $("#sompo_driver_other_occupation_" + $(this).attr("seq")).show();
            }
            else{
                $("#sompo_driver_other_occupation_" + $(this).attr("seq")).hide();
                $("#sompo_driver_other_occupation_" + $(this).attr("seq")).val('');
            }
        });
                
        $(".occup_code").each(function(){
            if($(this).val() == "OCC9999"){
                $("#sompo_driver_other_occupation_" + $(this).attr("seq")).show();
            }
        });
                
        if($("#order_cus_occ_code").val() == "OCC9999"){
            $("#order_cus_occ").show(); 
        }
                
        $("#order_cus_occ_code").change(function(){
            if($(this).val() == "OCC9999"){
                $("#order_cus_occ").show();
            }
            else{
                $("#order_cus_occ").val('');
                $("#order_cus_occ").hide();
            }
        });
                
        @if($coverage_list)
            $("#order_coverage").change(function(){
                getPremiunAmt();
            });
            getPremiunAmt();
        @endif
                
        $("#order_period_from").change(function(){
            var dt1 = $(this).val();
            var finalDate = dt1;
            var dt = new Date(finalDate);
            if(!isNaN(dt)){
                dt.setFullYear(dt.getFullYear() + parseInt(1));
                dt.setDate(dt.getDate() - parseInt(1));
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var disdate = year + "-" + month + "-" + day;
                var new_format = changeDateFormat(disdate);
                $("#order_period_to").val(disdate);
                $("#order_period_to").next('input').val(new_format);
            }
        });
                
        $("#order_cus_postal_code").bind('keyup change', function(){
            if($(this).val()){
                if($(this).val().length == 6){
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('SompoGetAddress') }}",
                        cache: false,
                        dataType: 'json',
                        data:  {
                            order_cus_postal_code : $(this).val()
                        },
                        success: function(data) {
                            if(data.status == 1){
                                $("#order_cus_blkno").val(data.block_number);
                                $("#order_cus_street").val(data.street_name);
                                $("#order_cus_building").val(data.building_name);
                                $("#order_cus_postal_code").val(data.postal_code);
                                var full_add = data.block_number + " " + data.street_name + " " +  data.building_name + " SINGAPORE " + data.postal_code;
                                $("#order_cus_address").val(full_add);
                            }
                            else{
                                $("#order_cus_postal_code").val("");
                            }
                        }
                    });
                }
            }
        });
        
        $("#order_cus_unitno").bind('keyup change', function(){
            var full_add = $("#order_cus_blkno").val() + " " + $("#order_cus_street").val() + " " +  $(this).val() +  " " +  $("#order_cus_building").val() + " SINGAPORE " + $("#order_cus_postal_code").val();
            $("#order_cus_address").val(full_add);
        }); 
                
        $("#order_cus_nirc").bind('change',function(){
            $.ajax({
                type: 'POST',
                url: "{{ route('SompoGetCusInfoByIC') }}",
                dataType: 'json',
                data: {
                    order_cus_nirc : $(this).val(),
                },
                success: function(data) {
                    if(data.status == 1){
                        var r = confirm("The Customer Already Exist. Did You Want To Auto Fill In?");
                        if(r === true){
                            getCustInfo(data.partner_id);
                            if(!$('#driver-block').is(":visible")){
                                $('#dri_col').click();
                            }
                            
                        }
                    }
                }
            });
        });  
    });

function getCustInfo(cus_id = ""){
    
    $.ajax({
        type: "POST",
        url: "{{ route('SompoGetCusInfo') }}",
        dataType: 'json',
        data:  {
            order_customer : cus_id
        },
        success: function(data) {
                $("#order_cus_name").val('');
                $("#order_cus_enc_name").val('');
                $("#order_cus_nirc").val('');
                $("#order_cus_enc_nirc").val('');
                $("#order_cus_nirc_type").val('');
                $("#order_cus_email").val('');
                $("#order_cus_enc_email").val('');
                $("#order_cus_contact").val('');
                $("#order_cus_enc_contact").val('');
                $("#order_cus_dob").val('');
                $("#order_cus_dob").next().val('');
                $("#order_cus_occ").val('');
                $("#order_cus_occ").hide();
                $("#order_cus_occ_code").val('').trigger("change");
                $("#order_cus_national").val('').trigger("change");
                $("#order_cus_unitno").val('');
                $("#order_cus_postal_code").val('');
                $("#order_cus_blkno").val('');
                $("#order_cus_street").val('');
            if(data.partner_name){
                $("#order_cus_name").val(data.partner_name);
                encyptdata(data.partner_name, "order_cus_enc_name");
            }
            if(data.partner_nirc){
                $("#order_cus_nirc").val(data.partner_nirc);
                encyptdata(data.partner_nirc, "order_cus_enc_nirc");
            }
            
            if(data.partner_email){
                $("#order_cus_email").val(data.partner_email);
                encyptdata(data.partner_email, "order_cus_enc_email");
            }
            
            if(data.partner_tel){
                $("#order_cus_contact").val(data.partner_tel);
                encyptdata(data.partner_tel, "order_cus_enc_contact");
            }
            if(data.partner_marital){
                $("#order_cus_marital").val(data.partner_marital).trigger("change");
            }
            if(data.partner_country){
                $("#order_cus_national").val(data.partner_country).trigger("change");
            }
            
            if(data.partner_gender){
                $("#order_cus_gender").val(data.partner_gender).trigger("change");
            }
            if(data.partner_dob){
                $("#order_cus_dob").val(data.partner_dob);
                $("#order_cus_dob").next().val(data.partner_dob);
            }
            if(data.partner_occ_code){
                $("#order_cus_occ_code").val(data.partner_occ_code).trigger("change");
            }
            if(data.partner_occupation && data.partner_occ_code == "OCC9999"){
                $("#order_cus_occ").val(data.partner_occupation);
                $("#order_cus_occ").show();
            }
            if(data.partner_nric_type){
                $("#order_cus_nirc_type").val(data.partner_nric_type);
            }
            
            var full_add = data.partner_blk_no + " " + data.partner_street + " " + data.partner_unit_no + " SINGAPORE " + data.partner_postal_code;
            
            if(data.partner_postal_code){
                $("#order_cus_postal_code").val(data.partner_postal_code);
                $("#order_cus_address").val(full_add);
            }
            if(data.partner_unit_no){
                $("#order_cus_unitno").val(data.partner_unit_no);
            }
            if(data.partner_street){
                $("#order_cus_street").val(data.partner_street);
            }
            if(data.partner_blk_no){
                $("#order_cus_blkno").val(data.partner_blk_no);
            }
            
            if($("#order_cus_driving").val() == "Y"){
                $(".copy_field").each(function(){
                    var out_id = $(this).attr("id");
                    var out_val = $(this).val();
                    copyDriver(out_id, out_val);
                });
            }
        }
    });
}

function encyptdata(input_data, out_put){
    $.get("{{ asset('assets/pem/E9FD51E6.pem') }}", function(pub_key){
        var encryption = new JSEncrypt();
        encryption.setPublicKey(pub_key); 
        var res = encryption.encrypt(input_data.toUpperCase());
        $("#"+out_put).val(res);
    });
}

function deldriverlist(element){
    var seq = $(element).attr('seq');
    var i = $("#last_driver_seq").val();
    i--;
    if(i <= 4){
        $("#add_drvier_btn").show();
    }
    $("#last_driver_seq").val(i);
    $("#driver_form_"+seq).remove();
    $(".driver_form_class").each(function(){
        var other_seq = $(this).attr('seq');
        if(other_seq > seq){
            var new_seq = parseInt(other_seq) - 1;
            $("#driver_title_" + other_seq).html("Rider " + new_seq);
            $("#driver_remove_" + other_seq).attr("seq",new_seq);
            $("#driver_remove_" + other_seq).attr("id","driver_remove_" + new_seq);
            $("#driver_form_" + other_seq).attr("seq",new_seq);
            $("#driver_form_" + other_seq).attr("id","driver_form_" + new_seq);
            $("#driver_title_" + other_seq).attr("id","driver_title_" + new_seq);
            $("#sompo_driver_enc_name_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_enc_name_" + other_seq).attr("name","sompo_driver[" + new_seq +"][enc_name]");
            $("#sompo_driver_enc_name_" + other_seq).attr("id","sompo_driver_enc_name_" + new_seq);
            $("#sompo_driver_name_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_name_" + other_seq).attr("rel","sompo_driver_enc_name_" + new_seq );
            $("#sompo_driver_name_" + other_seq).attr("name","sompo_driver[" + new_seq +"][name]");
            $("#sompo_driver_name_" + other_seq).attr("id","sompo_driver_name_" + new_seq);
            $("#sompo_driver_nirc_type_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_nirc_type_" + other_seq).attr("name","sompo_driver[" + new_seq +"][nirc_type]");
            $("#sompo_driver_nirc_type_" + other_seq).attr("id","sompo_driver_nirc_type_" + new_seq);
            $("#sompo_driver_enc_nirc_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_enc_nirc_" + other_seq).attr("name","sompo_driver[" + new_seq +"][enc_nirc]");
            $("#sompo_driver_enc_nirc_" + other_seq).attr("id","sompo_driver_enc_nirc_" + new_seq);
            $("#sompo_driver_nirc_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_nirc_" + other_seq).attr("rel","sompo_driver_enc_nirc_" + new_seq);
            $("#sompo_driver_nirc_" + other_seq).attr("name","sompo_driver[" + new_seq +"][nirc]");
            $("#sompo_driver_nirc_" + other_seq).attr("id","sompo_driver_nirc_" + new_seq);
            $("#sompo_driver_exp_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_exp_" + other_seq).attr("name","sompo_driver[" + new_seq +"][exp]");
            $("#sompo_driver_exp_" + other_seq).attr("id","sompo_driver_exp_" + new_seq);
            $("#sompo_driver_gender_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_gender_" + other_seq).attr("name","sompo_driver[" + new_seq +"][gender]");
            $("#sompo_driver_gender_" + other_seq).attr("id","sompo_driver_gender_" + new_seq);
            $("#sompo_driver_relation_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_relation_" + other_seq).attr("name","sompo_driver[" + new_seq +"][relation]");
            $("#sompo_driver_relation_" + other_seq).attr("id","sompo_driver_relation_" + new_seq);
            $("#sompo_driver_dob_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_dob_" + other_seq).attr("name","sompo_driver[" + new_seq +"][dob]");
            $("#sompo_driver_dob_" + other_seq).attr("id","sompo_driver_dob_" + new_seq);
            $("#sompo_driver_marital_status_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_marital_status_" + other_seq).attr("name","sompo_driver[" + new_seq +"][marital_status]");
            $("#sompo_driver_marital_status_" + other_seq).attr("id","sompo_driver_marital_status_" + new_seq);
            $("#sompo_driver_national_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_national_" + other_seq).attr("name","sompo_driver_national[" + new_seq +"]");
            $("#sompo_driver_national_" + other_seq).attr("id","sompo_driver_national_" + new_seq);
            $("#sompo_driver_occupation_code_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_occupation_code_" + other_seq).attr("name","sompo_driver[" + new_seq +"][occupation_code]");
            $("#sompo_driver_occupation_code_" + other_seq).attr("id","sompo_driver_occupation_code_" + new_seq);
            $("#sompo_driver_other_occupation_" + other_seq).attr("seq",new_seq);
            $("#sompo_driver_other_occupation_" + other_seq).attr("name","sompo_driver[" + new_seq +"][other_occupation]");
            $("#sompo_driver_other_occupation_" + other_seq).attr("id","sompo_driver_other_occupation_" + new_seq);                        
            $("#sompo_driver_seqno_" + other_seq).val(new_seq);                        
            $("#sompo_driver_seqno_" + other_seq).attr("name","sompo_driver[" + new_seq + "][seqno]");                        
            $("#sompo_driver_seqno_" + other_seq).attr("id","sompo_driver_seqno_" + new_seq);                        
        }
    });
}
            
function adddriverlist(){
    var i = $("#last_driver_seq").val();
    var indentyCtrl = jsonToOption('@json($identity_list)');
    var maritalCtrl = jsonToOption('@json($maritial_list)');
    var relCtrl = jsonToOption('@json($relationship_list)');
    var occCtrl = jsonToOption('@json($occupation_list)');
    var nalCtrl = jsonToOption('@json($country_list)');
    var readonly ="";
    var html = "";
    html += '<div class="driver_form_class" id="driver_form_' + i + '" seq="' + i + '">';
    html += '<div class="form-group">';
    html += '<div class="row">';
    html += '<div class="col-md-1">';
    html += '<label class="control-label" id="driver_title_' + i + '">Rider '+ i +'</label>';
    html += '<input type="hidden" id="sompo_driver_seqno_' + i + '" name="sompo_driver[' + i + '][seqno]" value="' + i + '">';
    html += '</div>';
    if(i > 1 ){
        html += '<div class="col-md-11">';
        html += '<button class="btn btn-danger" id="driver_remove_' + i + '" type="button" seq="' + i + '" onclick="deldriverlist(this)">Remove This Driver</button>';
        html += '</div>';
    }
    html += '</div>';
    html += '</div>';
    html += '<hr>';
    html += '<div class="row">';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_enc_name_' + i + '">Name</label>';
    html += '<input type="hidden" value="" id="sompo_driver_enc_name_' + i + '" name="sompo_driver[' + i + '][enc_name]" seq="' + i + '">';
    html += '<input type="text" class="form-control dri_valid" rel="sompo_driver_enc_name_' + i + '" id="sompo_driver_name_' + i + '" name="sompo_driver[' + i + '][name]" seq="' + i + '" value="">';
    html += '</div>';
    html += '</div>';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_nirc_type_' + i + '">NRIC Type</label>';
    html += '<select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[' + i + '][nirc_type]" id="sompo_driver_nirc_type_' + i + '" seq="' + i + '">';
    html += indentyCtrl;
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '';
    html += '<div class="row">';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_enc_nirc_' + i + '">NRIC</label>';
    html += '<input type="hidden" value="" id="sompo_driver_enc_nirc_' + i + '" name="sompo_driver[' + i + '][enc_nirc]" seq="' + i + '">';
    html += '<input type="text" class="form-control dri_valid" rel="sompo_driver_enc_nirc_' + i + '" id="sompo_driver_nirc_' + i + '" name="sompo_driver[' + i + '][nirc]" seq="' + i + '" value="">';
    html += '</div>';
    html += '</div>';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_national_' + i + '">Nationality</label>';
    html += '<select class="select2 form-select select-hidden-accessible basic_sel2 dri_valid" name="sompo_driver[' + i + '][national]" id="sompo_driver_national_' + i + '" seq="' + i + '">';
    html += nalCtrl;
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div class="row">';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_gender_' + i + '">Gender</label>';
    html += '<select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[' + i + '][gender]" id="sompo_driver_gender_' + i + '" seq="' + i + '">';
    html += '<OPTION value="M">MALE</OPTION>';
    html += '<OPTION value="F">FEMALE</OPTION>';
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_relation_' + i + '">Relationship</label>';
    html += '<select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[' + i + '][relation]" id="sompo_driver_relation_' + i + '" seq="' + i + '">';
    html += relCtrl;
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div class="row">';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_dob_' + i + '">Rider DOB</label>';
    html += '<input type="text" class="form-control dob_picker dri_dob_valid flatpickr-input active" placeholder="DD-MM-YYYY" id="sompo_driver_dob_' + i + '" name="sompo_driver[' + i + '][dob]" seq="' + i + '" value="" readonly="readonly">';
    html += '</div>';
    html += '</div>';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_marital_status_' + i + '">Rider Marital Status</label>';
    html += '<select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[' + i + '][marital_status]" id="sompo_driver_marital_status_' + i + '" seq="' + i + '">';
    html += maritalCtrl;
    html += '</select>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div class="row">';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_exp_' + i + '">Rider Experience</label>';
    html += '<input type="text" class="form-control dri_valid_exp" id="sompo_driver_exp_' + i + '" name="sompo_driver[' + i + '][exp]" value="" seq="' + i + '">';
    html += '</div>';
    html += '</div>';
    html += '<div class="col-md-6">';
    html += '<div class="form-group mb-3">';
    html += '<label for="sompo_driver_occupation_code_' + i + '">Rider Occupation</label>';
    html += '<select class="select2 form-select select-hidden-accessible occup_code dri_valid basic_sel2" name="sompo_driver[' + i + '][occupation_code]" id="sompo_driver_occupation_code_' + i + '" seq="' + i + '">';
    html += occCtrl;
    html += '</select>';
    html += '<input type="text" class="form-control" id="sompo_driver_other_occupation_' + i + '" name="sompo_driver[' + i + '][other_occupation]" seq="' + i + '" placeholder="Please Insert Other Occupation" style="display:none;margin-top:2%;">';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    i ++;
    if(i > 4){
        $("#add_drvier_btn").hide();
    }
    $("#last_driver_seq").val(i);
    $("#last_driver").before(html);
    $(".basic-sel2").select2();
    validateForm();
    
    $("input[type=text], textarea").bind("keyup change",function(){
        var inpclass = ['order_period_from', 'order_period_to','order_cus_dob', 'sompo_driver_dob_1', 'sompo_driver_dob_2', 'sompo_driver_dob_3', 'sompo_driver_dob_4'];
        if($(this).val()){
            var inpdata = $(this).val();
            if(jQuery.inArray( $(this).attr("id"), inpclass ) === -1){
                $(this).val(inpdata.toUpperCase());
            }
        }
    });
    
    $(".dob_picker").flatpickr({
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        minDate: "{{ $min_age }}",
        maxDate: "{{ $max_age }}",
    });
    
    $(".dri_valid").bind("keyup change", function(){
        var out_id = $(this).attr("rel");
        if(out_id){
            encyptdata($(this).val(), out_id);
        }
    });
    
    $(".dri_nirc_valid").bind("keyup change", function(){
        var out_id = $(this).attr("rel");
        if(out_id){
            encyptdata($(this).val(), out_id);
        }
    });
    
    
    
    $(".occup_code").change(function(){
        if($(this).val() == "OCC9999"){
            $("#sompo_driver_other_occupation_" + $(this).attr("seq")).show();
        }
        else{
            $("#sompo_driver_other_occupation_" + $(this).attr("seq")).val('');
            $("#sompo_driver_other_occupation_" + $(this).attr("seq")).hide();
        }
    });
}

function getPremiunAmt(){
    var planlist = JSON.parse('{{ isset($order_sompo_plan_list)?$order_sompo_plan_list:"[]"}}');
    var selplan = $("#order_coverage").val();
    $.each(planlist,function(index, value){
        $.each(value.PLANXRISKLIST,function(index1, value1){
            if(value1.RISK_CLASS == selplan){
                $("#order_premium_amt").val(changeNumberFormat(RoundNum(value.PREMIUMBEFORETAX, 2)));
                $("#order_gst_amount").val(changeNumberFormat(RoundNum(value.PLANTAXAMOUNT, 2)));
                $("#order_grosspremium_amount").val(changeNumberFormat(RoundNum(value.PLANTOTALPREMIUM, 2)));
                $(".span_premium").html(changeNumberFormat(RoundNum(value.PREMIUMBEFORETAX, 2)));
               
                if($.isArray(value1.RiskPlanXExcessList)){
                    $.each(value1.RiskPlanXExcessList,function(index2, value2){
                        $("#order_policy_excess").val(changeNumberFormat(RoundNum(value2.Excess_LC_VAL, 2)));
                    });
                }
                else{
                        $("#order_policy_excess").val(changeNumberFormat(RoundNum(0, 2)));
                }
            }
        });
        
    });
}

function copyDriver(obj_id, obj_val){
    $("#sompo_driver_relation_1").val("I").trigger("change");
    switch(obj_id) {
        case "order_cus_name":
            $("#sompo_driver_name_1").val(obj_val);
            $("#sompo_driver_enc_name_1").val(encyptdata(obj_val, 'sompo_driver_enc_name_1'));
            break;
        case "order_cus_national":
            $("#sompo_driver_national_1").val(obj_val).trigger("change");
            break;
        case "order_cus_nirc":
            $("#sompo_driver_nirc_1").val(obj_val);
            $("#sompo_driver_enc_nirc_1").val(encyptdata(obj_val, 'sompo_driver_enc_nirc_1'));
            break;
        case "order_cus_nirc_type":
            $("#sompo_driver_nirc_type_1").val(obj_val).trigger("change");
            break;
        case "order_cus_dob":
            $("#sompo_driver_dob_1").val(obj_val);
            break;
        case "order_cus_gender":
            if(obj_val == "male"){
                obj_val = "M";
            }
            else{
                obj_val = "F";
            }
            $("#sompo_driver_gender_1").val(obj_val).trigger("change");
            break;
        case "order_cus_marital":
            if(obj_val == "single"){
                obj_val = "S";
            }
            else{
                obj_val = "M";
            }
            $("#sompo_driver_marital_status_1").val(obj_val).trigger("change");
            break;
        case "order_cus_occ_code":
            $("#sompo_driver_occupation_code_1").val(obj_val).trigger("change");
            break;
        case "order_cus_occ":
            $("#sompo_driver_other_occupation_1").val(obj_val);
            break;
        case "order_cus_exp":
            $("#sompo_driver_exp_1").val(obj_val);
            break;
    }
}

function validateForm(){
    $("#SompoForm").validate({
        rules:
        {
            order_cus_name:{
                required:true
            },
            order_cus_nirc:{
                required:true,
                checkNRIC:true
            },
            order_cus_nirc_type:{
                required:true,
                checkNRICType:true
            },
            order_cus_unitno:{
                required:true
            },
            order_cus_occ_code:{
                required:true
            },
            order_cus_email:{
                required:true,
                email: true
            },
            order_tsa_email:{
                required:true,
                email: true
            },
            order_cus_national:{
                required:true
            },
            order_cus_contact:{
                required:true,
                digits:true,
                minlength: 8
            },
            order_cus_gender:{
                required:true,
            },
            order_cus_dob:{
                required:true,
                remote: {
                    url: "{{ route('SompoAgeValidate') }}",
                    type: "post",
                    data: 
                    {
                        order_cus_dob: function()
                        {
                            return $("#order_cus_dob").val();
                        }
                    },
                    dataFilter: function(data) {
                        var output = JSON.parse(data);
                        if(output.status == 1){
                            return true;
                        }
                        else{
                            return  "\"" + output.msg + "\"";
                        }
                    }
                }  
            },
            order_cus_marital:{
                required:true
            },
            order_cus_postal_code:{
                required:true,
                digits:true,
                minlength:6,
                maxlength:6,
            },
            order_cus_address:{
                required:true,
            },
            order_period_from:{
                required:true,
                remote: {
                    url: "{{ route('SompoPeriodFromValidate') }}",
                    type: "post",
                    data: 
                    {
                        order_period_from: function()
                        {
                            return $("#order_period_from").val();
                        }
                    },
                    dataFilter: function(data) {
                        var output = JSON.parse(data);
                        if(output.status == 1){
                            return true;
                        }
                        else{
                            return  "\"" + output.msg + "\"";;
                        }
                    }
                }
            },
            order_period_to:{
                required:true,
                remote: {
                    url: "{{ route('SompoPeriodToValidate') }}",
                    type: "post",
                    data: 
                    {
                        order_period_from: function()
                        {
                            return $("#order_period_from").val();
                        },
                        order_period_to: function()
                        {
                            return $("#order_period_to").val();
                        }
                    },
                    dataFilter: function(data) {
                        var output = JSON.parse(data);
                        if(output.status == 1){
                            return true;
                        }
                        else{
                            return  "\"" + output.msg + "\"";
                        }
                    }
                }
            },
            order_coverage:{
                required:true,
            },
            order_sompo_app_lisence:{
                required:true
            },
            order_sompo_terms:{
                required:true
            },
            order_cus_claim_amount:{
                number:true
            },
            order_cus_claim:{
                digits:true
            },
            order_sompo_cus_lisence:{
                required:true
            },
            order_vehicle_type:{
                required:true,
            },
            order_vehicles_no:{
                required:true,
                checkPlateNumber:true,
            },
            order_vehicles_reg_year:{
                required:true,
                digits:true,
            },
            order_vehicles_pro_year:{
                required:true,
                digits:true,
            },
            order_vehicles_engine:{
                required:true
            },
            order_vehicles_chasis:{
                required:true
            },
            order_vehicles_capacity:{
                required:true,
                digits:true,
                min:0,
                max:1500,
            },
            order_ncd_vehiclenumber:{
                required: function(){
                    return ($("#order_ncd_entitlement").val() != 0 && $("#order_ncd_entitlement").val() != '');
                },
                checkPlateNumber:true,
            },
            order_sompo_payment_method:{
                required:true
            },
        },
        messages:
        {
            order_cus_name:{
                required:"Please Insert Customer Name"
            },
            order_cus_nirc:{
                required:"Please Insert Customer Identity Number"
            },
            order_cus_occ_code:{
                required:"Please Select An Occupation"
            },
            order_cus_nirc_type:{
                required:"Please Choose An Identity Type"
            },
            order_cus_unitno:{
                required:"Please Insert Unit Number"
            },
            order_cus_email:{
                required:"Please Insert Customer Email",
                email: "Please Insert Valid Email"
            },
            order_tsa_email:{
                required:"Please Insert TSA Email",
                email: "Please Insert Valid Email"
            },
            order_cus_national:{
                required:"Please Choose An Nationality"
            },
            order_cus_contact:{
                required:"Please Insert Customer Contact",
                digits:"Contact Must Be Number"
            },
            order_cus_gender:{
                required:"Please Select Customer Gender",
            },
            order_cus_dob:{
                required:"Please Insert Customer Birthday"
            },
            order_cus_marital:{
                required:"Please Select Customer Matarial Status"
            },
            order_cus_postal_code:{
                required:"Please Insert Customer Postal code",
                digits:"Postal code Must Be Number",
                minlength:"Postal code Must be 6 Digits",
                maxlength:"Postal code Must be 6 Digits",
            },
            order_cus_address:{
                required:"Please Insert Customer Address",
            },
            order_period_from:{
                required:"Please Select a date"
            },
            order_period_to:{
                required:"Please Select a date"
            },
            order_coverage:{
                required:"Please Select a Coverage Plan",
            },
            order_vehicle_type:{
                required:"Please Select A vehicles",
            },
            order_vehicles_no:{
                required:"Please Insert vehicles number",
            },
            order_vehicles_reg_year:{
                required:"Please Insert Register Year",
                digits:"Register Year Must Be Number",
            },
            order_vehicles_pro_year:{
                required:"Please Insert Production Year",
                digits:"Production Year Must Be Number",
            },
            order_vehicles_engine:{
                required:"Please Insert Engine Number",
            },
            order_vehicles_chasis:{
                required:"Please Insert Chasis Number",
            },
            order_vehicles_capacity:{
                required:"Please Insert Vehicles Capacity",
                digits:"Vehicles Capacity Must Be number"
            },
            order_sompo_payment_method:{
                required:"Please selected a payment method"
            },
            order_cus_claim_amount:{
                number:"Please Enter Number Only"
            },
            order_cus_claim:{
                digits:"Please Enter Number Only"
            }, 
            order_ncd_vehiclenumber:{
                required: "Please Insert Vehicles Number",
            },
        }
    });

    $.validator.addClassRules({
        dri_valid: {
            required: true,  
        },
        dri_nirc_valid: {
            required: true,  
            checkNRIC: true,  
        },
        dri_dob_valid: {
            required: true,  
            remote: {
                url: "{{ route('SompoAgeValidate') }}",
                type: "post",
                dataFilter: function(data) {
                    var output = JSON.parse(data);
                    if(output.status == 1){
                        return true;
                    }
                    else{
                        return  "\"" + output.msg + "\"";
                    }
                }
            }  
        },
        dri_valid_exp: {
            required: true,  
            digits:true,
        },
        dri_valid_claim: {
            number:true,
        },
        dri_valid_point: {
            digits:true,
        },
    });
    
    $.validator.addMethod("checkNRIC", function(value, element){
        return validateNRIC(value);
    },"Invalid Indentity Format");
    
    $.validator.addMethod("checkNRICType", function(value, element){
        var nric = $("#order_cus_nirc").val();
        if(nric){
            var prefix = nric.charAt(0);
            if(prefix.toUpperCase() == "S" && (value.toUpperCase() == "N" || value.toUpperCase() == "B")){
                return true;
            }
            else if(prefix.toUpperCase() == "T" && (value.toUpperCase() == "N" || value.toUpperCase() == "B")){
                return true;
            }
            else if(prefix.toUpperCase() == "F" && (value.toUpperCase() == "E" || value.toUpperCase() == "W")){
                return true;
            }
            else if(prefix.toUpperCase() == "G" && (value.toUpperCase() == "E" || value.toUpperCase() == "W")){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    },"Invalid Indentity Type");
    
    $.validator.addMethod("checkPlateNumber", function(value, element){
        if(value){
            return validatePlate(value);
        }
        else{
            return true;
        }
    },"Invalid Vehicle Registration Format");
    
}
</script>