@include('includes.header')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Update Sompo Insurance</h1>
        <form id="SompoForm" action="{{ route('SompoUpdate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <h4 class="form-group-title mb-5">Insurance Status info</h4>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="timeline-horizontal">
                        <li class="li-horizontal complete-horizontal" >
                        <div class="timestamp-horizontal">
                        </div> 
                        @if($order_sompo_status == 11)
                            <div class="status-horizontal-fail">
                        @elseif($order_sompo_status > 11 || $order_sompo_status == 10)
                            <div class="status-horizontal-complete">
                        @else
                            <div class="status-horizontal">
                        @endif
                            <h4> Sent Qutation </h4>
                        </div>
                        </li>
                        <li class="li-horizontal complete-horizontal">
                        <div class="timestamp-horizontal">
                        </div>
                        @if($order_sompo_status == 21)
                            <div class="status-horizontal-fail">
                        @elseif($order_sompo_status > 21 || $order_sompo_status == 20)
                            <div class="status-horizontal-complete">
                        @else
                            <div class="status-horizontal">
                        @endif
                            <h4> Sent Proposal </h4>
                        </div>
                        </li>
                        <li class="li-horizontal complete-horizontal">
                        <div class="timestamp-horizontal">
                        </div>
                        @if($order_sompo_status == 31)
                            <div class="status-horizontal-fail">
                        @elseif($order_sompo_status == 30)
                            <div class="status-horizontal-complete">
                        @else
                            <div class="status-horizontal">
                        @endif
                            <h4> Create Policy </h4>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>
            @include('sompo.section.InfoSec1')
            <!-- Vehicle and Package Section -->
            @include('sompo.section.InfoSec2')
            <!-- end of Vehicle and Package Section -->
            <!-- Calculation Section -->
            @include('sompo.section.InfoSec3')
            <!-- end of Calculation Section -->
            <div class="text-right">
                <input type="hidden" name="api_sent" id="api_sent" value="0">
                <input type="hidden" name="resend" id="resend" value="0">
                <input type="hidden" name="order_sompo_id" value="{{ $order_sompo_id }}">
                <input type="hidden" value="C" name="order_sompo_payment_method">
                <input type="hidden" value="{{ $order_sompo_status }}" name="order_sompo_status">
                <a href="{{ route('SompoList') }}"  class="btn btn-info mr-2"> Back </a>
                @if($order_sompo_status == 11 || $order_sompo_status == 1)
                <button type="button" class="api_btn btn btn-warning mr-2">Get Quotation</button>
                @elseif($order_sompo_status == 10 || $order_sompo_status == 21)
                <button type="button" class="resend_btn btn btn-warning mr-2">Get Quotation</button>
                <button type="button" class="api_btn btn btn-warning mr-2">Get Proposal</button>
                @elseif($order_sompo_status == 20 || $order_sompo_status == 31)
                <button type="button" class="resend_btn btn btn-warning mr-2">Get Proposal</button>
                <button type="button" class="api_btn btn btn-warning mr-2">Create Policy</button>
                @endif
                @PrivilegeCheck('SompoSave')
                <button type="submit" class="btn btn-success mr-2">Save Data (For Internal Stuff Only)</button>
                @endPrivilegeCheck
            </div>
        </form>
    </div>
</div>
@include('sompo.Script')
@include('includes.footer')