@include('includes.header')
@inject('provider', 'App\Http\Controllers\AllianzController')
<div class="card">
<div class="card-body">
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <div class="text-right pt-4">
        <a href="{{ route('SompoAddNew') }}" class="btn btn-primary">Add Insurance</a>
    </div> 
    <h1>Sompo Insurance List</h1>
    <table id="ajaxtable" class="table petlist-table" link="{{ route('sompoListData') }}">
        <thead>
            <tr>
                <th>No</th>
                <th>Vehicle Number</th>
                <th>Proposal Number</th>
                <th>Policy Number</th>
                <th>Insure Name</th>
                <th>NRIC</th>
                <th>From</th>
                <th>To</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
<script>
    $(document).ready(function(){

        $(".export_date").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });
    })
</script>
@include('includes.footer')