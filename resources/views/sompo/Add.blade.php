@include('includes.header')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Add Sompo Insurance</h1>
        <form id="SompoForm" action="{{ route('SompoCreate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <h4 class="form-group-title mb-5">Insurance Status info</h4>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="timeline-horizontal">
                        <li class="li-horizontal complete-horizontal" >
                        <div class="timestamp-horizontal">
                        </div>
                        <div class="status-horizontal">
                            <h4> Sent Qutation </h4>
                        </div>
                        </li>
                        <li class="li-horizontal complete-horizontal">
                        <div class="timestamp-horizontal">
                        </div>
                        <div class="status-horizontal">
                            <h4> Sent Proposal </h4>
                        </div>
                        </li>
                        <li class="li-horizontal complete-horizontal">
                        <div class="timestamp-horizontal">
                        </div>
                        <div class="status-horizontal">
                            <h4> Create Policy </h4>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>
            @include('sompo.section.AddSec1')
            <!-- Vehicle and Package Section -->
            @include('sompo.section.AddSec2')
            <!-- end of Vehicle and Package Section -->
            <!-- Calculation Section -->
            @include('sompo.section.AddSec3')
            <!-- end of Calculation Section -->
            <div class="text-right">
                <input type="hidden" name="api_sent" id="api_sent" value="0">
                <input type="hidden" value="C" name="order_sompo_payment_method">
                <input type="hidden" value="1" name="order_sompo_status">
                <a href="{{ route('SompoList') }}"  class="btn btn-info mr-2"> Back </a>
                <button type="button" class="api_btn btn btn-warning mr-2">Get Quotation</button>
                <button type="submit" class="btn btn-success mr-2">Save Data (For Internal Stuff Only)</button>
            </div>
        </form>
    </div>
</div>
@include('sompo.Script')
@include('includes.footer')