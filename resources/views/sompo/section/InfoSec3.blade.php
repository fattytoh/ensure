<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title mb-3">Riders Details</h3>
        <div class="box-tools" style="position:unset;display: inline;">
            <button class="btn btn-default btn-sm" id="dri_col" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div style="display: block;" class="box-body" id="driver-block">
    @if($driver_list)
        @foreach ($driver_list as $row)
            <div class="driver_form_class" id="driver_form_{{ $row['sompo_driver_seqno'] }}" seq="{{ $row['sompo_driver_seqno'] }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-1">
                            <label class="control-label" id="driver_title_{{ $row['sompo_driver_seqno'] }}">Rider {{ $row['sompo_driver_seqno'] }}</label>
                            <input type="hidden" id="sompo_driver_sompo_driver_seqno_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][seqno]" value="{{ $row['sompo_driver_seqno'] }}">
                        </div>
                        @if($row['sompo_driver_seqno'] > 1)
                        <div class="col-md-11 ">
                            <button class="btn btn-danger" id="driver_remove_{{ $row['sompo_driver_seqno'] }}" type="button" seq="{{ $row['sompo_driver_seqno'] }}" onclick="deldriverlist(this)">Remove This Driver</button>
                        </div>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_enc_name_{{ $row['sompo_driver_seqno'] }}">Name</label>
                            <input type="hidden" value="{{ $row['sompo_driver_enc_name'] }}" id="sompo_driver_enc_name_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][enc_name]" seq="{{ $row['sompo_driver_seqno'] }}">
                            <input type="text" class="form-control dri_valid" rel="sompo_driver_enc_name_{{ $row['sompo_driver_seqno'] }}" id="sompo_driver_name_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][name]"  seq="{{ $row['sompo_driver_seqno'] }}" value="{{ $row['sompo_driver_name'] }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_nirc_type_{{ $row['sompo_driver_seqno'] }}">NRIC Type</label>
                            <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][nirc_type]" id="sompo_driver_nirc_type_{{ $row['sompo_driver_seqno'] }}"  seq="{{ $row['sompo_driver_seqno'] }}">
                            @foreach($identity_list as $oli)
                                <option value="{{ $oli['value'] }}" {{ ($row['sompo_driver_nirc_type'] == $oli['value']) ? "selected" : "" }}>{{ $oli['display_name'] }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_enc_nirc_{{ $row['sompo_driver_seqno'] }}">NRIC</label>
                            <input type="hidden" value="{{ $row['sompo_driver_enc_nirc'] }}" id="sompo_driver_enc_nirc_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][enc_nirc]"  seq="{{ $row['sompo_driver_seqno'] }}">
                            <input type="text" class="form-control dri_valid" rel="sompo_driver_enc_nirc_{{ $row['sompo_driver_seqno'] }}" id="sompo_driver_nirc_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][nirc]"  seq="{{ $row['sompo_driver_seqno'] }}" value="{{ $row['sompo_driver_nirc'] }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_national_{{ $row['sompo_driver_seqno'] }}">Nationality</label>
                            <select class="select2 form-select select-hidden-accessible basic_sel2 dri_valid" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][national]" id="sompo_driver_national_{{ $row['sompo_driver_seqno'] }}"  seq="{{ $row['sompo_driver_seqno'] }}">
                            @foreach($country_list as $oli)
                                <option value="{{ $oli['value'] }}" {{ ($row['sompo_driver_national'] == $oli['value']) ? "selected" : "" }} >{{ $oli['display_name'] }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_gender_{{ $row['sompo_driver_seqno'] }}">Gender</label>
                            <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][gender]" id="sompo_driver_gender_{{ $row['sompo_driver_seqno'] }}"  seq="{{ $row['sompo_driver_seqno'] }}">
                                <OPTION value="M" {{ ($row['sompo_driver_gender'] == 'M') ? "selected" : "" }}>MALE</OPTION>
                                <OPTION value="F" {{ ($row['sompo_driver_gender'] == 'F') ? "selected" : "" }}>FEMALE</OPTION>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_relation_{{ $row['sompo_driver_seqno'] }}">Relationship</label>
                            <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][relation]" id="sompo_driver_relation_{{ $row['sompo_driver_seqno'] }}"  seq="{{ $row['sompo_driver_seqno'] }}">
                            @foreach($relationship_list as $oli)
                                <option value="{{ $oli['value'] }}" {{ ($row['sompo_driver_relation'] == $oli['value']) ? "selected" : "" }}>{{ $oli['display_name'] }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_dob_{{ $row['sompo_driver_seqno'] }}">Rider DOB</label>
                            <input type="text" class="form-control dob_picker dri_dob_valid flatpickr-input active" placeholder="DD-MM-YYYY" id="sompo_driver_dob_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][dob]"  seq="{{ $row['sompo_driver_seqno'] }}" value="{{ $row['sompo_driver_dob'] }}" readonly="readonly">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_marital_status_{{ $row['sompo_driver_seqno'] }}">Rider Marital Status</label>
                            <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][marital_status]" id="sompo_driver_marital_status_{{ $row['sompo_driver_seqno'] }}"  seq="{{ $row['sompo_driver_seqno'] }}">
                            @foreach($maritial_list as $oli)
                                <option value="{{ $oli['value'] }}" {{ ($row['sompo_driver_marital_status'] == $oli['value']) ? "selected" : "" }}>{{ $oli['display_name'] }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_exp_{{ $row['sompo_driver_seqno'] }}">Rider Experience</label>
                            <input type="text" class="form-control dri_valid_exp" id="sompo_driver_exp_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][exp]" value="{{ $row['sompo_driver_exp'] }}"  seq="{{ $row['sompo_driver_seqno'] }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="sompo_driver_occupation_code_{{ $row['sompo_driver_seqno'] }}">Rider Occupation</label>
                            <select class="select2 form-select select-hidden-accessible occup_code dri_valid basic_sel2" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][occupation_code]" id="sompo_driver_occupation_code_{{ $row['sompo_driver_seqno'] }}"  seq="{{ $row['sompo_driver_seqno'] }}">
                            @foreach($occupation_list as $oli)
                                <option value="{{ $oli['value'] }}" {{ ($row['sompo_driver_occupation_code'] == $oli['value']) ? "selected" : "" }} >{{ $oli['display_name'] }}</option>
                            @endforeach
                            </select>
                            @if($row['sompo_driver_other_occupation'])
                            <input type="text" value="{{ $row['sompo_driver_other_occupation'] }}" class="form-control" id="sompo_driver_other_occupation_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][other_occupation]"  seq="{{ $row['sompo_driver_seqno'] }}" placeholder="Please Insert Other Occupation" style="margin-top:2%;">
                            @else
                            <input type="text" value="{{ $row['sompo_driver_other_occupation'] }}" class="form-control" id="sompo_driver_other_occupation_{{ $row['sompo_driver_seqno'] }}" name="sompo_driver[{{ $row['sompo_driver_seqno'] }}][other_occupation]"  seq="{{ $row['sompo_driver_seqno'] }}" placeholder="Please Insert Other Occupation" style="display:none;margin-top:2%;">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="driver_form_class" id="driver_form_1" seq="1">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label" id="driver_title_1">Rider 1</label>
                        <input type="hidden" id="sompo_driver_seqno_1" name="sompo_driver[1][seqno]" value="1">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_enc_name_1">Name</label>
                        <input type="hidden" value="" id="sompo_driver_enc_name_1" name="sompo_driver[1][enc_name]" seq="1">
                        <input type="text" class="form-control dri_valid" rel="sompo_driver_enc_name_1" id="sompo_driver_name_1" name="sompo_driver[1][name]" seq="1" value="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_nirc_type_1">NRIC Type</label>
                        <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[1][nirc_type]" id="sompo_driver_nirc_type_1" seq="1">
                        @foreach($identity_list as $oli)
                            <option value="{{ $oli['value'] }}" >{{ $oli['display_name'] }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_enc_nirc_1">NRIC</label>
                        <input type="hidden" value="" id="sompo_driver_enc_nirc_1" name="sompo_driver[1][enc_nirc]" seq="1">
                        <input type="text" class="form-control dri_valid" rel="sompo_driver_enc_nirc_1" id="sompo_driver_nirc_1" name="sompo_driver[1][nirc]" seq="1" value="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_national_1">Nationality</label>
                        <select class="select2 form-select select-hidden-accessible basic_sel2 dri_valid" name="sompo_driver[1][national]" id="sompo_driver_national_1" seq="1">
                        @foreach($country_list as $oli)
                            <option value="{{ $oli['value'] }}" >{{ $oli['display_name'] }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_gender_1">Gender</label>
                        <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[1][gender]" id="sompo_driver_gender_1" seq="1">
                            <OPTION value="M">MALE</OPTION>
                            <OPTION value="F">FEMALE</OPTION>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_relation_1">Relationship</label>
                        <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[1][relation]" id="sompo_driver_relation_1" seq="1">
                        @foreach($relationship_list as $oli)
                            <option value="{{ $oli['value'] }}" >{{ $oli['display_name'] }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_dob_1">Rider DOB</label>
                        <input type="text" class="form-control dob_picker flatpickr-input active" placeholder="DD-MM-YYYY" id="sompo_driver_dob_1" name="sompo_driver[1][dob]" seq="1" value="" readonly="readonly">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_marital_status_1">Rider Marital Status</label>
                        <select class="select form-select select-hidden-accessible dri_valid" name="sompo_driver[1][marital_status]" id="sompo_driver_marital_status_1" seq="1">
                        @foreach($maritial_list as $oli)
                            <option value="{{ $oli['value'] }}" >{{ $oli['display_name'] }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_exp_1">Rider Experience</label>
                        <input type="text" class="form-control dri_valid_exp" id="sompo_driver_exp_1" name="sompo_driver[1][exp]" value="" seq="1">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="sompo_driver_occupation_code_1">Rider Occupation</label>
                        <select class="select2 form-select select-hidden-accessible occup_code dri_valid basic_sel2" name="sompo_driver[1][occupation_code]" id="sompo_driver_occupation_code_1" seq="1">
                        @foreach($occupation_list as $oli)
                            <option value="{{ $oli['value'] }}" >{{ $oli['display_name'] }}</option>
                        @endforeach
                        </select>
                        <input type="text" class="form-control" id="sompo_driver_other_occupation_1" name="sompo_driver[1][other_occupation]" seq="1" placeholder="Please Insert Other Occupation" style="display:none;margin-top:2%;">
                    </div>
                </div>
            </div>
        </div>
    @endif
        <div id="last_driver"></div>
    </div>
    <div class="box-footer" style="display: block;">
        <div class="box-tools" id="add_drvier_btn">
            <input type="hidden" id="last_driver_seq" value="{{ (count($driver_list) > 0)?count($driver_list):2 }}">
            <button type="button" class="btn btn-warning" onclick="adddriverlist()">Add New Driver</button>
        </div>
    </div>
</div>