<h4 class="form-group-title mb-5">Insurance Basic info</h4>
<div class="form-group mb-3">
    <label for="order_doc_no">Proposal Number</label>
    <span class="form-control" id="order_doc_no" name="order_doc_no" >-- System Generate --</span>
</div>
<div class="form-group mb-3">
    <label for="order_policyno">Policy Number</label>
    <span class="form-control" id="order_doc_no" name="order_doc_no" >-- System Generate --</span>
</div>
<div class="form-group mb-3">
    <label for="order_record_billto">Bill To</label>
    <select name="order_record_billto" id="order_record_billto" class="select form-select select-hidden-accessible">
        <option value="TO TSA" {{ (old('order_record_billto') == 'TO TSA') ? "selected" : "" }} >To TSA</option>
        <option value="TO Customer" {{ (old('order_record_billto') == 'TO Customer') ? "selected" : "" }} >To Customer</option>
        <option value="TO Salesman" {{ (old('order_record_billto') == 'TO Salesman') ? "selected" : "" }} >To Salesman</option>
    </select>
    @if ($errors->has('order_record_billto'))
    <span class="text-danger mt-2 pl-2">{{ $errors->first('order_record_billto') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_dealer">TSA</label>
    <select id="order_dealer" name="order_dealer" class="select2 form-select select-hidden-accessible">
    @if(old('order_dealer'))
        {!! app('App\Http\Controllers\SelectController')->getTSAOption(old('order_dealer')) !!}
    @endif
    </select>
    @if ($errors->has('order_dealer'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_dealer') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_cus_driving">Policy Holder Driving Y/N</label>
    <select id="order_cus_driving" name="order_cus_driving" class="select form-select select-hidden-accessible">
        <option value="Y" {{ (old('order_cus_driving') == 'Y') ? "selected" : "" }} >Yes</option>
        <option value="N" {{ (old('order_cus_driving') == 'N') ? "selected" : "" }} >No</option>
    </select>
    @if ($errors->has('order_cus_driving'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_driving') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_cus_name">Name</label>
    <input type="hidden" value="{{ old('order_cus_enc_name') }}" name="order_cus_enc_name" id="order_cus_enc_name">
    <input type="text" class="form-control enc_data_type copy_field"  enc_rel="order_cus_enc_name"  id="order_cus_name" name="order_cus_name" value="{{ old('order_cus_name') }}" >
    @if ($errors->has('order_cus_name'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_name') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_cus_national">Nationality</label>
    <select id="order_cus_national" name="order_cus_national" class="basic-sel2 select2 form-select select-hidden-accessible">
        @foreach($country_list as $cli)
            <option value="{{ $cli['value'] }}" {{ (old('order_cus_national') == $cli['value']) ? "selected" : "" }} >{{ $cli['display_name'] }}</option>
        @endforeach
    </select>
    @if ($errors->has('order_cus_national'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_national') }}</span>
    @endif
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_nirc">Identity Number</label>
            <input type="hidden" value="{{ old('order_cus_enc_nirc') }}" name="order_cus_enc_nirc" id="order_cus_enc_nirc">
            <input type="text" class="form-control enc_data_type copy_field"  enc_rel="order_cus_enc_nirc"  id="order_cus_nirc" name="order_cus_nirc" value="{{ old('order_cus_nirc') }}" >
            @if ($errors->has('order_cus_nirc'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_nirc') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_nirc_type">Identity type</label>
            <select class="select form-select select-hidden-accessible copy_field" name="order_cus_nirc_type" id="order_cus_nirc_type">
                <option value="">Select One</option>
                <option value="B" {{ (old('order_cus_nirc_type') == 'B') ? "selected" : "" }}>NRIC (Singapore PR)</option>
                <option value="E" {{ (old('order_cus_nirc_type') == 'E') ? "selected" : "" }}>FIN (Employment Pass)</option>
                <option value="N" {{ (old('order_cus_nirc_type') == 'N') ? "selected" : "" }}>NRIC (Singaporean)</option>
                <option value="W" {{ (old('order_cus_nirc_type') == 'W') ? "selected" : "" }}>FIN (Work Permit / S Pass)</option>                    
            </select>
            @if ($errors->has('order_cus_nirc_type'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_nirc_type') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_dob">Date Of Birth(DD-MM-YYYY)</label>
            <input type="text" class="form-control dob_picker copy_field flatpickr-input active" placeholder="DD-MM-YYYY" id="order_cus_dob" name="order_cus_dob" value="{{ old('order_cus_dob') }}" readonly="readonly">
            @if ($errors->has('order_cus_dob'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_dob') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_gender">Gender</label>
            <select class="form-control copy_field" name="order_cus_gender" id="order_cus_gender">
                <option value="male" {{ (old('order_cus_gender') == 'male') ? "selected" : "" }}>Male</option>
                <option value="female" {{ (old('order_cus_gender') == 'female') ? "selected" : "" }}>Female</option>                    
            </select>
            @if ($errors->has('order_cus_gender'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_gender') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_cus_exp">Driving Exprience In singapore</label>
    <input type="text" class="form-control copy_field" id="order_cus_exp" name="order_cus_exp" value="{{ old('order_cus_exp') }}" >
    @if ($errors->has('order_cus_exp'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_exp') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_marital">Marital Status</label>
            <select class="form-control copy_field" name="order_cus_marital" id="order_cus_marital">
                <option value="married" {{ (old('order_cus_marital') == 'married') ? "selected" : "" }}>Married</option>
                <option value="single" {{ (old('order_cus_marital') == 'single') ? "selected" : "" }}>Single</option>                    
            </select>
            @if ($errors->has('order_cus_marital'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_marital') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_occ_code">Occupation</label>
            <select class="form-control copy_field" name="order_cus_occ_code" id="order_cus_occ_code">    
                @foreach($occupation_list as $oli)
                    <option value="{{ $oli['value'] }}" {{ (old('order_cus_occ_code') == $oli['value']) ? "selected" : "" }} >{{ $oli['display_name'] }}</option>
                @endforeach             
            </select>
            <input type="text" class="form-control sompo_other_occupation copy_field" name="order_cus_occ" id="order_cus_occ" placeholder="Enster Other Occupation" value="{{ old('order_cus_occ') }}" style="display:none">
            @if ($errors->has('order_cus_gender'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_gender') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_unitno">Unit Number</label>
            <input type="hidden" name="order_cus_blkno" id="order_cus_blkno" value="{{ old('order_cus_blkno') }}">
            <input type="hidden" name="order_cus_street" id="order_cus_street" value="{{ old('order_cus_street') }}">
            <input type="hidden" name="order_cus_building" id="order_cus_building" value="{{ old('order_cus_building') }}">
            <input type="text" class="form-control" id="order_cus_unitno" name="order_cus_unitno" value="{{ old('order_cus_unitno') }}" >
            @if ($errors->has('order_cus_unitno'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_unitno') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_postal_code">Postal Code</label>
            <input type="text" class="form-control" id="order_cus_postal_code" name="order_cus_postal_code" value="{{ old('order_cus_postal_code') }}" >
            @if ($errors->has('order_cus_postal_code'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_postal_code') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_cus_address">Address</label>
    <textarea class="form-control" rows="5" id="order_cus_address" name="order_cus_address">{{ old('order_cus_address')}}</textarea>
    @if ($errors->has('order_cus_address'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_address') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_contact">Mobile Number</label>
            <input type="hidden" value="{{ old('order_cus_enc_contact') }}" name="order_cus_enc_contact" id="order_cus_enc_contact">
            <input type="text" class="form-control enc_data_type" enc_rel="order_cus_enc_contact" id="order_cus_contact" name="order_cus_contact" value="{{ old('order_cus_contact') }}" >
            @if ($errors->has('order_cus_contact'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_contact') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_email">Customer Email</label>
            <input type="hidden" value="{{ old('order_cus_enc_email') }}" name="order_cus_enc_email" id="order_cus_enc_email">
            <input type="text" class="form-control enc_data_type" enc_rel="order_cus_enc_email" id="order_cus_email" name="order_cus_email" value="{{ old('order_cus_email') }}" >
            @if ($errors->has('order_cus_email'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_email') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_tsa_email">TSA Email</label>
    <input type="hidden" value="{{ old('order_tsa_enc_email') }}" name="order_tsa_enc_email" id="order_tsa_enc_email">
    <input type="text" class="form-control enc_data_type" enc_rel="order_tsa_enc_email" id="order_tsa_email" name="order_tsa_email" value="{{ old('order_tsa_email') }}" >
    @if ($errors->has('order_tsa_email'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_tsa_email') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_cus_claim">Claim In Last Three Years</label>
            <select class="form-control" name="claim_option" id="claim_option">
                <option value="N" {{ (old('claim_option') == 'N') ? "selected" : "" }}>No</option>
                <option value="Y" {{ (old('claim_option') == 'Y') ? "selected" : "" }}>Yes</option>                    
            </select>
            <input type="text" class="form-control" id="order_cus_claim" name="order_cus_claim" value="{{ old('order_cus_claim') }}" style="display:none">
            @if ($errors->has('order_cus_claim'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_claim') }}</span>
            @endif
        </div>
    </div>
    @if(old('order_cus_claim_amount')):
    <div id="claim_amount" class="col-md-6">
    @else
    <div id="claim_amount" class="col-md-6" style="display:none">
    @endif
        <div class="form-group mb-3">
            <label for="order_cus_claim_amount">Total Amount Of Claims</label>
            <input type="text" class="form-control" id="order_cus_claim_amount" name="order_cus_claim_amount" value="{{ old('order_cus_claim_amount') }}" >
            @if ($errors->has('order_cus_claim_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_claim_amount') }}</span>
            @endif
        </div>
    </div>
</div>