<h4 class="form-group-title mb-5">Vehicles info</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_reg_year">Year of Registration</label>
            <input type="text" class="form-control" id="order_vehicles_reg_year" name="order_vehicles_reg_year" value="{{ old('order_vehicles_reg_year')?:$order_vehicles_reg_year }}" >
            @if ($errors->has('order_vehicles_reg_year'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_reg_year') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_pro_year">Year of Manufacture</label>
            <input type="text" class="form-control" id="order_vehicles_pro_year" name="order_vehicles_pro_year" value="{{ old('order_vehicles_pro_year')?:$order_vehicles_pro_year }}" >
            @if ($errors->has('order_vehicles_pro_year'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_pro_year') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_vehicle_type">Vehicles Make</label>
    <select class="form-control" name="order_vehicle_type" id="order_vehicle_type">    
        @foreach($vehicles_list as $oli)
            <option value="{{ $oli['value'] }}" {{ (old('order_vehicle_type') == $oli['value']) ? "selected" : (($order_vehicle_type == $oli['value']) ? "selected" : "") }} >{{ $oli['display_name'] }}</option>
        @endforeach             
    </select>
    @if ($errors->has('order_vehicle_type'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicle_type') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_model">Vehicle Model</label>
            <input type="text" class="form-control" id="order_vehicles_model" name="order_vehicles_model" value="{{ old('order_vehicles_model')?:$order_vehicles_model }}" >
            @if ($errors->has('order_vehicles_model'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_model') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_capacity">Engine Capacity(0-1500)</label>
            <input type="text" class="form-control" id="order_vehicles_capacity" name="order_vehicles_capacity" value="{{ old('order_vehicles_capacity')?:$order_vehicles_capacity }}" >
            @if ($errors->has('order_vehicles_capacity'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_capacity') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_new_reg">New Registration</label>
            <select class="form-control" name="order_new_reg" id="order_new_reg">
                <option value="N" {{ (old('order_new_reg') == 'N') ? "selected" : (($order_new_reg == 'N') ? "selected" : "") }}>No</option>
                <option value="Y" {{ (old('order_new_reg') == 'Y') ? "selected" : (($order_new_reg == 'Y') ? "selected" : "") }}>Yes</option>                    
            </select>
            @if ($errors->has('order_new_reg'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_new_reg') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_no">Vehicle Registration No</label>
            <input type="text" class="form-control" id="order_vehicles_no" name="order_vehicles_no" value="{{ old('order_vehicles_no')?:$order_vehicles_no }}" >
            @if ($errors->has('order_vehicles_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_no') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_engine">Engine No</label>
            <input type="text" class="form-control" id="order_vehicles_engine" name="order_vehicles_engine" value="{{ old('order_vehicles_engine')?:$order_vehicles_engine }}" >
            @if ($errors->has('order_vehicles_engine'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_engine') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_chasis">Chassis No</label>
            <input type="text" class="form-control" id="order_vehicles_chasis" name="order_vehicles_chasis" value="{{ old('order_vehicles_chasis')?:$order_vehicles_chasis }}" >
            @if ($errors->has('order_vehicles_chasis'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_chasis') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_sompo_offroad">Cross Country or Off Road Bike</label>
            <select class="form-control" name="order_sompo_offroad" id="order_sompo_offroad">
                <option value="N" {{ (old('order_sompo_offroad') == 'N') ? "selected" : (($order_sompo_offroad == 'N') ? "selected" : "")}}>No</option>
                <option value="Y" {{ (old('order_sompo_offroad') == 'Y') ? "selected" : (($order_sompo_offroad == 'Y') ? "selected" : "") }}>Yes</option>                    
            </select>
            @if ($errors->has('order_sompo_offroad'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_sompo_offroad') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ofd_dis_per">Offence Free Discount</label>
            <select class="form-control" name="order_ofd_dis_per" id="order_ofd_dis_per">
                <option value="0" {{ (old('order_ofd_dis_per') == '0') ? "selected" : (($order_ofd_dis_per == '0') ? "selected" : "") }}>No</option>
                <option value="5" {{ (old('order_ofd_dis_per') == '5') ? "selected" : (($order_ofd_dis_per == '5') ? "selected" : "") }}>Yes</option>                    
            </select>
            @if ($errors->has('order_ofd_dis_per'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ofd_dis_per') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_sompo_reward">Use for Hire and Reward</label>
            <select class="form-control" name="order_sompo_reward" id="order_sompo_reward">
                <option value="N" {{ (old('order_sompo_reward') == 'N') ? "selected" : (($order_sompo_reward == 'N') ? "selected" : "") }}>No</option>
                <option value="Y" {{ (old('order_sompo_reward') == 'Y') ? "selected" : (($order_sompo_reward == 'Y') ? "selected" : "") }}>Yes</option>                    
            </select>
            @if ($errors->has('order_sompo_reward'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_sompo_reward') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ncd_entitlement">NCD</label>
            <select class="form-control" name="order_ncd_entitlement" id="order_ncd_entitlement">
                @foreach($ncd_list as $oli)
                    <option value="{{ $oli['value'] }}" {{ (old('order_ncd_entitlement') == $oli['value']) ? "selected" : (($order_ncd_entitlement == $oli['value']) ? "selected" : "") }} >{{ $oli['display_name'] }}</option>
                @endforeach                       
            </select>
            @if ($errors->has('order_ncd_entitlement'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_entitlement') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_financecode">Hire Purchase Company Name</label>
            <select class="basic-sel2 select2 form-select select-hidden-accessible" name="order_financecode" id="order_financecode">
                @foreach($financecode_list as $oli)
                    <option value="{{ $oli['value'] }}" {{ (old('order_financecode') == $oli['value']) ? "selected" : (($order_financecode == $oli['value']) ? "selected" : "") }} >{{ $oli['display_name'] }}</option>
                @endforeach                       
            </select>
            @if ($errors->has('order_financecode'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_financecode') }}</span>
            @endif
        </div>
    </div>
    @if(old('order_financecode_type_input') || $order_financecode_type_input ):
    <div id="other_financecode_box" class="col-md-6">
    @else
    <div id="other_financecode_box" class="col-md-6" style="display:none;">
    @endif
        <div class="form-group mb-3">
            <label for="order_financecode_type_input">Other Company Name</label>
            <input type="text" class="form-control" id="order_financecode_type_input" name="order_financecode_type_input" value="{{ old('order_financecode_type_input')?:$order_financecode_type_input }}" >
            @if ($errors->has('order_financecode_type_input'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_financecode_type_input') }}</span>
            @endif
        </div>
    </div>
</div>
<h4 class="form-group-title mb-5">Insurance History</h4>
<div class="form-group mb-3">
    <label for="order_ncd_previousinsurer">Previous Insurer</label>
    <select class="form-control" name="order_ncd_previousinsurer" id="order_ncd_previousinsurer">
        @foreach($pre_financecode_list as $oli)
            <option value="{{ $oli['value'] }}" {{ (old('order_ncd_previousinsurer') == $oli['value']) ? "selected" :  (($order_ncd_previousinsurer == $oli['value']) ? "selected" : "") }} >{{ $oli['display_name'] }}</option>
        @endforeach                       
    </select>
    <div id="order_input_ncd_previousinsurer_div" class="form-group mt-3" style="{{ (!old('order_input_ncd_previousinsurer' || !$order_input_ncd_previousinsurer )) ?"display:none":"" }}">
        <input type="text" class="form-control" id="order_input_ncd_previousinsurer" name="order_input_ncd_previousinsurer" value="{{ old('order_input_ncd_previousinsurer')?:$order_input_ncd_previousinsurer }}" placeholder="Insert Previous Insurer">
    </div>
    @if ($errors->has('order_ncd_previousinsurer'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_previousinsurer') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_pre_policyno">Previous Policy Number</label>
    <input type="text" class="form-control" id="order_pre_policyno" name="order_pre_policyno" value="{{ old('order_pre_policyno')?:$order_pre_policyno }}" >
    @if ($errors->has('order_pre_policyno'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_pre_policyno') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_ncd_vehiclenumber">Vehicle Number</label>
    <input type="text" class="form-control" id="order_ncd_vehiclenumber" name="order_ncd_vehiclenumber" value="{{ old('order_ncd_vehiclenumber')?:$order_ncd_vehiclenumber }}" >
    @if ($errors->has('order_ncd_vehiclenumber'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_vehiclenumber') }}</span>
    @endif
</div>
<h4 class="form-group-title mb-5">Insurance History</h4>
<div class="form-group mb-3">
    <label for="order_coverage">Cover Type</label>
    <select class="form-control" name="order_coverage" id="order_coverage">
        @foreach($coverage_list as $oli)
            <option value="{{ $oli['value'] }}" {{ (old('order_coverage') == $oli['value']) ? "selected" : (($order_coverage == $oli['value']) ? "selected" : "") }} >{{ $oli['display_name'] }}</option>
        @endforeach                       
    </select>
    @if ($errors->has('order_coverage'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_coverage') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_from">Coverage Period From</label>
            <input type="text" class="form-control sompo_picker flatpickr-input active" placeholder="DD-MM-YYYY" id="order_period_from" name="order_period_from" value="{{ old('order_period_from')?:$order_period_from }}" readonly="readonly">
            @if ($errors->has('order_period_from'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_from') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_to">Coverage Period To</label>
            <input type="text" class="form-control sompo_picker flatpickr-input active" placeholder="DD-MM-YYYY" id="order_period_to" name="order_period_to" value="{{ old('order_period_to')?:$order_period_to }}" readonly="readonly">
            @if ($errors->has('order_period_to'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_to') }}</span>
            @endif
        </div>
    </div>
</div>
<h4 class="form-group-title mb-5">Discount Details</h4>
<div class="form-group mb-3">
    <label for="order_sompo_discount">Authority Discount</label>
    <input type="text" class="form-control" id="order_sompo_discount" name="order_sompo_discount" value="{{ old('order_sompo_discount')?:$order_sompo_discount }}" >
    @if ($errors->has('order_sompo_discount'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_sompo_discount') }}</span>
    @endif
</div>
<h4 class="form-group-title mb-5">Premium and Excess</h4>
<div class="form-group mb-3">
    <label for="order_grosspremium_amount">Premium Including GST</label>
    <input type="text" class="form-control" id="order_grosspremium_amount" name="order_grosspremium_amount" value="{{ old('order_grosspremium_amount')?:$order_grosspremium_amount }}"  readonly="readonly">
    @if ($errors->has('order_grosspremium_amount'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_grosspremium_amount') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_policy_excess">Policy Excess</label>
    <input type="text" class="form-control" id="order_policy_excess" name="order_policy_excess" value="{{ old('order_policy_excess')?:$order_policy_excess }}"  readonly="readonly">
    @if ($errors->has('order_policy_excess'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_policy_excess') }}</span>
    @endif
</div>
<h4 class="form-group-title mb-5">Declaration</h4>
<div class="form-group mb-3">
    <label for="order_sompo_terms">I agree to the above Declaration</label>
    <select class="form-control" name="order_sompo_terms" id="order_sompo_terms">
        <option value="Y" {{ (old('order_sompo_terms') == 'Y') ? "selected" : (($order_sompo_terms == 'Y') ? "selected" : "") }}>Yes</option>                    
        <option value="N" {{ (old('order_sompo_terms') == 'N') ? "selected" : (($order_sompo_terms == 'N') ? "selected" : "") }}>No</option>
    </select>
    @if ($errors->has('order_sompo_terms'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_sompo_terms') }}</span>
    @endif
</div>