<!DOCTYPE html>
<html>

<head>
    <title>Allianz Certificate of Insurance</title>
    <style>
        @page{
            margin-top: 50px;
            margin-bottom: 50px;
            font-size:12px;
            
        }
        header{
            position: fixed;
            padding-top:10px;
            left: 0px;
            right: 0px;
            height: 50px;
            margin-top: -50px;
        }
        footer{
            position: fixed;
            left: 0px;
            right: 0px;
            height: 50px;
            bottom: 0px;
            margin-bottom: -50px;
            color:#003780;
        }
       
        .title-box{
            text-align:center;
            padding-bottom: 10px;
            border-top: 2px solid black;
            border-bottom: 2px solid black;
        }
        .title-box h5{
            margin-bottom:0px;
        }

        .my-10{
            padding-top:10px;
            padding-bottom:10px;
        }
    </style>
</head>
<body>
    
    <header>
        <table style='width:100%;padding-bottom:10px;'>
            <tr>
                <td style='text-align:left;width:70%;font-size:12px;color:#003780'>
                    Allianz Insurance Singapore Pte. Ltd.
                </td>
                <td style='text-align:right;width:30%;font-size:12px;'>
                    <img src="{{public_path('assets/images/allianz-logo.png')}}" style="width:120px;max-width:100%;">
                </td>
            </tr>
        </table>
    </header>
    <footer>
        <strong>Allianz Insurance Singapore Pte. Ltd.</strong> | UEN 201903913C<br>
        79 Robinson Road #09-01 | Singapore 068897 | Tel: +65 6714 3369 | Website: www.allianz.sg
    </footer>
    <main>
        <div class="title-box">
            <h5>CERTIFICATE OF INSURANCE</h5>
        </div>
        <div class="my-10">
            ROAD TRANSPORT ACT 1987 (MALAYSIA)<br>
            MOTOR VEHICLES (THIRD-PARTY RISKS) RULES 1959 (FEDERATION OF MALAYSIA)<br>
            MOTOR VEHICLES (THIRD-PARTY RISKS AND COMPENSATION) ACT (CAP.189 OF THE REVISED EDITION) (REPUBLIC OF SINGAPORE)<br>
            MOTOR VEHICLES (THIRD-PARTY RISKS AND COMPENSATION) RULES 1996 (REPUBLIC OF SINGAPORE)<br>
            MOTOR VEHICLES (THIRD-PARTY RISKS AND COMPENSATION) RULES,1960<br>
            OR ANY AMENDMENT, ACT OR ACTS PASSED IN SUBSTITUTION THEREOF
        </div>
        <table style='width:100%;padding-bottom:10px;'>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Certificate Number
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ $order_policyno?:'-' }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Date of Issue
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ date('d/m/Y', strtotime($order_date))?:'-' }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Coverage
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ $trans[$order_coverage]?:'-' }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Policyholder 
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ $order_cus_lname." ".$order_cus_fname }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Finance Company 
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ $order_financecode?:'-' }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Period of Insurance
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ ($order_period_from?date('d/m/Y', strtotime($order_period_from)):'-').' To '.($order_period_to?date('d/m/Y', strtotime($order_period_to)):'-') }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Registration Number
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ $order_vehicles_no?:'-' }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Chassis Number of Vehicle
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ $order_vehicles_chasis?:'-' }}
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:30%;'>
                    Rider Name
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ ($driver_lname_1?:'')." ".($driver_fname_1?:'') }}
                </td>
            </tr>
            @if($driver_lname_1)
            <tr>
                <td style='text-align:left;width:30%;'>
                </td>
                <td style='text-align:left;width:1%;'>
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ ($driver_lname_2?:'')." ".($driver_fname_2?:'') }}
                </td>
            </tr>
            @endif
            @if($driver_lname_3)
            <tr>
                <td style='text-align:left;width:30%;'>
                </td>
                <td style='text-align:left;width:1%;'>
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ ($driver_lname_3?:'')." ".($driver_fname_3?:'') }}
                </td>
            </tr>
            @endif
            @if($driver_lname_4)
            <tr>
                <td style='text-align:left;width:30%;'>
                </td>
                <td style='text-align:left;width:1%;'>
                </td>
                <td style='text-align:left;width:69%;'>
                    {{ ($driver_lname_4?:'-')." ".($driver_fname_4?:'-') }}
                </td>
            </tr>
            @endif
        </table>
        <hr>
        <div class="my-10">
        <strong>Persons or Classes of Persons Entitled to Drive*:</strong><br>
        (a)	The Policyholder.<br>
        (b)	Name Driver(s) only<br>
        <small><i>*   Provided that the person driving is permitted in accordance with the licensing or other laws or regulation to drive the Motor Vehicle or has been permitted and is not disqualified by order of Court of Law or by reason of any enactment or regulations in that behalf from driving the Motor Vehicle. And provided further that the Motor Vehicle is registered under the Road Traffic Act (Cap 276) (Republic of Singapore) and such registration has not been cancelled at the time of accident loss or damage.</i></small><br><br>

        <strong>Limitation as to Use^:</strong><br>
        (a)	Use in connection with the Policyholder`s business.<br>
        (b)	Use for food / parcel / other delivery services<br>
        (c)	Use for social, domestic and pleasure purposes<br>
        <small><i>^   Limitation rendered inoperative by Section 8 of Motor Vehicles (Third-Party Risks and Compensation) Act (Chapter 189) and Section 95 of the Road Transport Act, 1987 (Malaysia), are not to be included under these headings.</i></small><br><br>
        <strong>Policy does not cover:</strong><br>
        (a)	Use for racing, pace-making, reliability trials or speed-testing.<br>
        (b)	Use whilst drawing a trailer except the towing (other than for reward) of any one disabled mechanically propelled vehicle.<br>
        (c)	Use for hire or reward.<br>
        (d)	Use for any purpose in connection with the Motor Trade.<br><br>

        I/We hereby certify that the Policy to which this Certificate relates is issued in accordance with the provisions of the Motor Vehicles (Third Party Risks and Compensation) Act (Chapter 189) and Part IV of the Road Transport Act,1987 (Malaysia).
        </div>
        <table style='width:100%;padding-bottom:0px;align-items:top;'>
            <tr>
                <td style='text-align:center;width:30%;'>
                    <div style="display:inline-block;">
                        <div style="width:150px;height:50px;position:relative;border-bottom:1px solid #000;">
                            <p style="position:absolute;bottom:-5px;width:100%;text-align:center;left:0px;">
                                {{ date('Y-m-d', strtotime($created_at))?:'-' }}
                            </p>
                        </div>
                        <div style="width:150px;padding:5px 0px;text-align:center;font-weight:600;">
                            Issue Date
                        </div>
                    </div>
                </td>
                <td style='text-align:left;width:30%;'>
                </td>
                <td style='text-align:center;width:40%;'>
                    <div style="display:inline-block;">
                        <div style="width:250px;height:80px;position:relative;border-bottom:1px solid #000;">
                            <img src="{{public_path('assets/images/allianz-sign.png')}}" style="width:87px;max-width:100%;position:absolute;bottom:-10px;left:80px;">
                        </div>
                        <div style="width:250px;padding:5px 0px;text-align:center;font-weight:600;">
                            Hicham Raissi<br>
                            Chief Executive Officer<br>
                            Allianz Insurance Singapore Pte. Ltd.
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <table style='width:100%;padding-bottom:10px;'>
            <tr>
                <td style='text-align:left;width:18%;'>
                    Intermediary Code
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:41%;'>
                    0000547
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:18%;'>
                    Excess
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:41%;'>
                    Own Damage
                </td>
                <td style='text-align:left;width:10%;'>
                </td>
                <td style='text-align:left;width:10%;'>
                    S$
                </td>
                <td style='text-align:right;width:10%;'>
                    @if($order_coverage == 'COMP')
                    {{ $order_excess_amount?:'0.00' }}
                    @else
                        Not Covered
                    @endif
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:18%;'>
                    
                </td>
                <td style='text-align:left;width:1%;'>
                    
                </td>
                <td style='text-align:left;width:41%;'>
                    Fire & Theft
                </td>
                <td style='text-align:left;width:10%;'>
                </td>
                <td style='text-align:left;width:10%;'>
                    S$
                </td>
                <td style='text-align:right;width:10%;'>
                    @if($order_coverage == 'TPO')
                        Not Covered
                    @else
                    {{ $order_excess_amount?:'0.00' }}   
                    @endif
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:18%;'>
                    Addtional Excess
                </td>
                <td style='text-align:left;width:1%;'>
                    :
                </td>
                <td style='text-align:left;width:41%;'>
                    Vehicle stolen outside Singapore
                </td>
                <td style='text-align:left;width:10%;'>
                </td>
                <td style='text-align:left;width:10%;'>
                    S$
                </td>
                <td style='text-align:right;width:10%;'>
                    @if($order_coverage == 'TPO')
                        Not Covered
                    @else
                        1000.00   
                    @endif
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:18%;'>
                    
                </td>
                <td style='text-align:left;width:1%;'>
                    
                </td>
                <td style='text-align:left;width:41%;'>
                    -
                </td>
                <td style='text-align:left;width:10%;'>
                </td>
                <td style='text-align:left;width:10%;'>
                    S$
                </td>
                <td style='text-align:right;width:10%;'>
                    0.00
                </td>
            </tr>
            <tr>
                <td style='text-align:left;width:18%;'>
                </td>
                <td style='text-align:left;width:1%;'>
                    
                </td>
                <td style='text-align:left;width:41%;'>
                    -
                </td>
                <td style='text-align:left;width:10%;'>
                </td>
                <td style='text-align:left;width:10%;'>
                    S$
                </td>
                <td style='text-align:right;width:10%;'>
                    0.00
                </td>
            </tr>
        </table>
    </main>
</body>

</html>
