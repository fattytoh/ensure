@include('includes.header')
@inject('provider', 'App\Http\Controllers\AllianzController')
<div class="card">
<div class="card-body">
    @if (session()->has('success_msg'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('success_msg') }}
    </div>
    @endif
    @if (session()->has('error_msg'))
    <div class="alert alert-danger" role="alert">
        {{ session()->get('error_msg') }}
    </div>
    @endif
    <form id="AllianzExportForm" class="mb-3" action="{{ route('AllianzExportData') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-4">
                <div class="form-group mb-3">
                    <label for="start_date">Export From</label>
                    <input type="text" class="form-control export_date flatpickr-input active" placeholder="DD/MM/YYYY" id="start_date" name="start_date" value="" readonly="readonly">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group mb-3">
                    <label for="end_date">Export To</label>
                    <input type="text" class="form-control export_date flatpickr-input active" placeholder="DD/MM/YYYY" id="end_date" name="end_date" value="" readonly="readonly">
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-right pt-4">
                    <button type="submit" class="btn btn-info">Export</button>
                    <a href="{{ route('AllianzAddNew') }}" class="btn btn-primary">Add Insurance</a>
                </div> 
            </div>
        </div>
    </form>
   
    <h1>Allianz Insurance List</h1>
    <table id="ajaxtable" class="table petlist-table" link="{{ route('allianzListData') }}">
        <thead>
            <tr>
                <th>No</th>
                <th>Vehicle Number</th>
                <th>Proposal Number</th>
                <th>Policy Number</th>
                <th>Insure Name</th>
                <th>NRIC</th>
                <th>From</th>
                <th>To</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
<script>
    $(document).ready(function(){

        $(".export_date").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });
    })
</script>
@include('includes.footer')