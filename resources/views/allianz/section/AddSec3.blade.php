<h4 class="form-group-title mb-5">Basic Calculation</h4>
<div class="form-group mb-3">
    <label for="order_premium_amt">Basic Premium</label>
    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_premium_amt" name="order_premium_amt" value="{{ old('order_premium_amt')}}">
            @if ($errors->has('order_premium_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_premium_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_accident_amt">Claim loading Charge</label>
    <div class="row">
        <div class="col-md-12">
        <input type="text" class="form-control text-right" id="order_accident_amt" name="order_accident_amt" value="{{ old('order_accident_amt')}}">
            @if ($errors->has('order_accident_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_accident_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_ncd_dis_amt">NCD Discount</label>
    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_ncd_dis_amt" name="order_ncd_dis_amt" value="{{ old('order_ncd_dis_amt')}}">
            @if ($errors->has('order_ncd_dis_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_dis_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_in_exp_amt">In Experience Charge</label>
    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_in_exp_amt" name="order_in_exp_amt" value="{{ old('order_in_exp_amt')}}">
            @if ($errors->has('order_in_exp_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_in_exp_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_delivery_amt">Delivery Charge</label>
    <div class="row">
        <div class="col-md-5">
            <select name="order_delivery_opt" id="order_delivery_opt" class="select form-select select-hidden-accessible">
                <option value="y" {{ (old('order_delivery_amt')?:50 > '0') ? "selected" : "" }}>Yes</option>
                <option value="n" {{ (old('order_delivery_amt')?:50 <= '0') ? "selected" : "" }}>No</option>
            </select>
            @if ($errors->has('order_delivery_opt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_delivery_opt') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_delivery_amt" name="order_delivery_amt" value="{{ old('order_delivery_amt')?:50.00}}">
            @if ($errors->has('order_delivery_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_delivery_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_addtional_amt">Additional driver Charge</label>
    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_addtional_amt" name="order_addtional_amt" value="{{ old('order_addtional_amt')}}">
            @if ($errors->has('order_addtional_amt'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_addtional_amt') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_subtotal_amount">Sub Total</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_subtotal_amount" name="order_subtotal_amount" value="{{ old('order_subtotal_amount')}}" readonly="readonly">
            @if ($errors->has('order_subtotal_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_subtotal_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_discount">Discount</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_discount" name="order_discount" value="{{ old('order_discount')?:0.00 }}" >
            @if ($errors->has('order_discount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_discount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_gst_percent">GST</label>
    <div class="row mt-2">
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_gst_percent" name="order_gst_percent" value="{{ old('order_gst_percent')?:'9.00' }}">
            @if ($errors->has('order_gst_percent'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_gst_percent') }}</span>
            @endif
        </div>
        <div class="col-md-2 text-center pt-md-2">
            <label for="order_gst_amount">%</label>
        </div>
        <div class="col-md-5">
            <input type="text" class="form-control text-right" id="order_gst_amount" name="order_gst_amount" value="{{ old('order_gst_amount')?:'0.00' }}">
            @if ($errors->has('order_gst_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_gst_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_grosspremium_amount">Grand Total</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_grosspremium_amount" name="order_grosspremium_amount" value="{{ old('order_grosspremium_amount')}}">
            @if ($errors->has('order_grosspremium_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_grosspremium_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label for="order_excess_amount">Policy Excess</label>
    <div class="row mt-2">
        <div class="col-md-12">
            <input type="text" class="form-control text-right" id="order_excess_amount" name="order_excess_amount" value="{{ old('order_excess_amount')}}">
            @if ($errors->has('order_excess_amount'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_excess_amount') }}</span>
            @endif
        </div>
    </div>
</div>