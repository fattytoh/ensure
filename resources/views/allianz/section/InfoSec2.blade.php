<!-- Package Section -->
<h4 class="form-group-title mb-5">Cover Type And Policy Period</h4>
<div class="form-group mb-3">
    <label for="order_coverage">Cover Type</label>
    <select name="order_coverage" id="order_coverage" class="select form-select select-hidden-accessible" {{ $disabled }} >     
        <option value="COMP" {{ $order_coverage == 'COMP' ? "selected" : "" }}>Comprehensive</option>
        <option value="TPFT" {{ $order_coverage == 'TPFT' ? "selected" : "" }}>Third Party, Fire & Theft</option>
        <option value="TPO" {{ $order_coverage == 'TPO' ? "selected" : "" }}>Third Party</option>
    </select>
    @if ($errors->has('order_coverage'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_coverage') }}</span>
    @endif
</div>
<div class="form-group mb-3">
    <label for="order_period">Cover Period</label>
    <select name="order_period" id="order_period" class="select form-select select-hidden-accessible" {{ $disabled }} >     
        <option value="1" {{ $order_period == '1' ? "selected" : "" }}>1 Years</option>
        <option value="2" {{ $order_period == '2' ? "selected" : "" }}>2 Years</option>
    </select>
    @if ($errors->has('order_coverage'))
        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_coverage') }}</span>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_from">Coverage Period From</label>
            <input type="text" class="form-control alianz_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_from" name="order_period_from" value="{{ old('order_period_from')?:$order_period_from }}" readonly="readonly" {{ $disabled }} >
            @if ($errors->has('order_period_from'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_from') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_period_to">Coverage Period To</label>
            <input type="hidden" class="form-control flatpickr-input active" placeholder="DD/MM/YYYY" id="order_period_to" name="order_period_to" value="{{ old('order_period_to')?:$order_period_to }}" readonly="readonly" {{ $disabled }} >
            <input class="form-control flatpickr-input input active" placeholder="DD/MM/YYYY" tabindex="0" type="text" readonly="readonly" value="{{ old('order_period_to')?date('d/m/Y',strtotime(old('order_period_to'))):date('d/m/Y',strtotime($order_period_to)) }}" {{ $disabled }}>
            @if ($errors->has('order_period_to'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_period_to') }}</span>
            @endif
        </div>
    </div>
</div>
<!-- end of Package Section -->
<!-- Vehicle Section -->
<h4 class="form-group-title mb-5">Vehicle Details</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_reg_year">Year of Registration</label>
            <select name="order_vehicles_reg_year" id="order_vehicles_reg_year" class="select2 form-select select-hidden-accessible" {{ $disabled }}>
                <?php $current_year = date('Y');?>
                @while($current_year >= $min_year)
                <option value="{{ $current_year }}" {{ ( $order_vehicles_reg_year == $current_year) ? "selected" : "" }}>{{ $current_year }}</option>
                <?php $current_year--;?>
                @endwhile
            </select>
            @if ($errors->has('order_vehicles_reg_year'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_reg_year') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_pro_year">Year of Manufacture</label>
            <select name="order_vehicles_pro_year" id="order_vehicles_pro_year" class="select2 form-select select-hidden-accessible" {{ $disabled }}>
                <?php $current_year = date('Y');?>
                @while($current_year >= $min_year)
                <option value="{{ $current_year }}" {{ ( $order_vehicles_pro_year == $current_year) ? "selected" : "" }}>{{ $current_year }}</option>
                <?php $current_year--;?>
                @endwhile
            </select>
            @if ($errors->has('order_vehicles_pro_year'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_pro_year') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_new_reg">New Registration</label>
            <select name="order_new_reg" id="order_new_reg" class="select form-select select-hidden-accessible" {{ $disabled }} >
                <option value="N" {{ ($order_new_reg == 'N') ? "selected" : "" }}>No</option>
                <option value="Y" {{ ($order_new_reg == 'Y') ? "selected" : "" }}>Yes</option>
            </select>
            @if ($errors->has('order_new_reg'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_new_reg') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_no">Vehicle Registration No</label>
            <input type="text" class="form-control" id="order_vehicles_no" name="order_vehicles_no" value="{{ old('order_vehicles_no')?:$order_vehicles_no }}" {{ $disabled }} >
            @if ($errors->has('order_vehicles_no'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_no') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_type">Vehicle Make</label>
            <select id="order_vehicles_type" name="order_vehicles_type" class="select2 form-select select-hidden-accessible" {{ $disabled }}>
                @if($order_vehicles_type != 0)
                    <option value="{{ $order_vehicles_type }}" selected>{{ $vehicles_name }}</option>
                @endif
            </select>
            @if ($errors->has('order_vehicles_type'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_type') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_model">Vehicle Model</label>
            <input type="text" class="form-control" id="order_vehicles_model" name="order_vehicles_model" value="{{ old('order_vehicles_model')?:$order_vehicles_model }}" {{ $disabled }} >
            @if ($errors->has('order_vehicles_model'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_model') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_capacity">Engine Capacity(0-1500)</label>
            <input type="number" min="0" inputmode="numeric" pattern="[0-9]*" class="form-control val_check" id="order_vehicles_capacity" name="order_vehicles_capacity" value="{{ old('order_vehicles_capacity')?:$order_vehicles_capacity }}" {{ $disabled }} >
            @if ($errors->has('order_vehicles_capacity'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_capacity') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_engine">Engine No</label>
            <input type="text" class="form-control" id="order_vehicles_engine" name="order_vehicles_engine" value="{{ old('order_vehicles_engine')?:$order_vehicles_engine }}" {{ $disabled }} >
            @if ($errors->has('order_vehicles_engine'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_engine') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_vehicles_chasis">Chassis No</label>
            <input type="text" class="form-control" id="order_vehicles_chasis" name="order_vehicles_chasis" value="{{ old('order_vehicles_chasis')?:$order_vehicles_chasis }}" {{ $disabled }} >
            @if ($errors->has('order_vehicles_chasis'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_vehicles_chasis') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_ncd_entitlement">NCD</label>
            <select name="order_ncd_entitlement" id="order_ncd_entitlement" class="select form-select select-hidden-accessible" {{ $disabled }} >
                <option value="0" {{ $order_ncd_entitlement == '0' ? "selected" : "" }}>0%</option>
                <option value="10" {{ $order_ncd_entitlement == '10' ? "selected" : "" }}>10%</option>
                <option value="15" {{ $order_ncd_entitlement == '15' ? "selected" : "" }}>15%</option>
                <option value="20" {{ $order_ncd_entitlement == '20' ? "selected" : "" }}>20%</option>
            </select>
            @if ($errors->has('order_ncd_entitlement'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_ncd_entitlement') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label for="order_financecode">Hire Purchase Company Name</label>
            <select id="order_financecode" name="order_financecode" class="select2 form-select select-hidden-accessible" {{ $disabled }}>
                @if($order_financecode != 0)
                    <option value="{{ $order_financecode }}" selected>{{ $financecode_name }}</option>
                @endif
            </select>
            @if ($errors->has('order_financecode'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_financecode') }}</span>
            @endif
        </div>
    </div>
    <div id="other_financecode_box" class="col-md-6" style="display:none;">
        <div class="form-group mb-3">
            <label for="order_other_financecode">Other Company Name</label>
            <input type="text" class="form-control" id="order_other_financecode" name="order_other_financecode" value="{{ old('order_other_financecode') }}" >
            @if ($errors->has('order_other_financecode'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_other_financecode') }}</span>
            @endif
        </div>
    </div>
</div>
<!-- end of Vehicle Section -->
<!-- Rider Section -->
<h4 class="form-group-title mb-5">Rider Infomation</h4>
<div class="box box-solid collapsed-box my-5" >
    <div class="box-header">
        <h3 class="box-title">Rider Details</h3>
        <div class="box-tools" style="position:unset;display: inline;" >
            <button class="btn btn-default driver_btn_expand btn-sm" type="button" rel="driver_main_box"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" id="driver_main_box">
        @for ($i = 1; $i < 5; $i++ )
        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <h3 class="box-title">Rider {{ $i }}</h3>
                <div class="box-tools" style="position:unset;display: inline;">
                @if($i == 1)
                    <button class="btn btn-default driver_btn_expand btn-sm" type="button" rel="driver{{ $i }}_box"><i class="fa fa-minus"></i></button>
                @else
                    <button class="btn btn-default driver_btn_expand btn-sm" type="button" rel="driver{{ $i }}_box"><i class="fa fa-plus"></i></button>
                @endif
                    
                </div>
            </div>
            @if($i == 1)
                <div class="box-body pt-5" id="driver{{ $i }}_box">
            @else
                <div style="display: none;" class="box-body pt-5" id="driver{{ $i }}_box">
            @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="driver_lname_{{ $i }}">Last Name</label>
                            <input type="text" class="form-control" id="driver_lname_{{ $i }}" name="driver_lname_{{ $i }}" value="{{ old('driver_lname_'.$i)?:${'driver_lname_'.$i} }}" {{ $disabled }} >
                            @if ($errors->has('driver_lname_'.$i))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('driver_lname_'.$i) }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="driver_fname_{{ $i }}">First Name</label>
                            <input type="text" class="form-control" id="driver_fname_{{ $i }}" name="driver_fname_{{ $i }}" value="{{ old('driver_fname_'.$i)?:${'driver_fname_'.$i} }}" {{ $disabled }} >
                            @if ($errors->has('driver_fname_'.$i))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('driver_fname_'.$i) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="driver_nirc_{{ $i }}">NRIC</label>
                            <input type="text" class="form-control" id="driver_nirc_{{ $i }}" name="driver_nirc_{{ $i }}" value="{{ old('driver_nirc_'.$i)?:${'driver_nirc_'.$i} }}" {{ $disabled }} >
                            @if ($errors->has('driver_nirc_'.$i))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('driver_nirc_'.$i) }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="driver_national_{{ $i }}">Nationality</label>
                            <select name="driver_national_{{ $i }}" id="driver_national_{{ $i }}" class="select2 select2-basic form-select select2-hidden-accessible" {{ $disabled }} >
                                @foreach($country_list as $cli)
                                    <option value="{{ $cli['country_id'] }}" {{ (${'driver_national_'.$i} == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('driver_national_'.$i))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('driver_national_'.$i) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="driver_gender_{{ $i }}">Gender</label>
                            <select name="driver_gender_{{ $i }}" id="driver_gender_{{ $i }}" class="select form-select select-hidden-accessible" {{ $disabled }} >
                                <option value="male" {{ (${'driver_gender_'.$i} == 'male') ? "selected" : "" }}>Male</option>
                                <option value="female" {{ (${'driver_gender_'.$i} == 'female') ? "selected" : "" }}>Female</option>
                            </select>
                            @if ($errors->has('driver_gender_'.$i))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('driver_gender_'.$i) }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="driver_dob_{{ $i }}">Rider DOB</label>
                            <input type="text" class="form-control dob_picker flatpickr-input active" placeholder="DD/MM/YYYY" id="driver_dob_{{ $i }}" name="driver_dob_{{ $i }}" value="{{ old('driver_dob_'.$i)?:${'driver_dob_'.$i} }}" readonly="readonly" {{ $disabled }} >
                            @if ($errors->has('driver_dob_'.$i))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('driver_dob_'.$i) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="driver_exp_{{ $i }}">Rider Experience</label>
                            <input type="number" min="0" inputmode="numeric" pattern="[0-9]*" class="form-control" id="driver_exp_{{ $i }}" name="driver_exp_{{ $i }}" value="{{ old('driver_exp_'.$i)?:${'driver_exp_'.$i} }}" {{ $disabled }} >
                            @if ($errors->has('driver_exp_'.$i))
                                <span class="text-danger mt-2 pl-2">{{ $errors->first('driver_exp_'.$i) }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endfor
    </div>
</div>
<!-- end of Rider Section -->