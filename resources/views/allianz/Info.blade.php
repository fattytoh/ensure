@include('includes.header')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            Please Check Agian
        </div>
        @endif
        <h1 class="mb-5">Allianz Insurance Info</h1>
        <form id="AllianzForm" action="{{ route('AllianzUpdate') }}" method="post" enctype="multipart/form-data">
        @csrf
            <h4 class="form-group-title mb-5">Insurance Basic info</h4>
            <div class="form-group mb-3">
                <label for="order_doc_no">Proposal Number</label>
                <span class="form-control" id="order_doc_no" name="order_doc_no" >{{ $order_no }}</span>
            </div>
            <div class="form-group mb-3">
                <label for="order_policyno">Policy Number</label>
                <input type="text" class="form-control" id="order_policyno" name="order_policyno" value="{{ old('order_policyno')?:$order_policyno }}" {{ $disabled }} >
                @if ($errors->has('order_policyno'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('order_policyno') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="order_date">Policy Issue Date</label>
                <input type="text" class="form-control flatpickr-basic-input flatpickr-input active" placeholder="DD/MM/YYYY" id="order_date" name="order_date" value="{{ old('order_date')?:$order_date }}" readonly="readonly" {{ $disabled }}>
                @if ($errors->has('order_date'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('order_date') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="order_record_billto">Bill To</label>
                <select name="order_record_billto" id="order_record_billto" class="select form-select select-hidden-accessible" {{ $disabled }}>
                    <option value="TO TSA" {{ ($order_record_billto == 'TO TSA') ? "selected" : "" }} >To TSA</option>
                    <option value="TO Customer" {{ ($order_record_billto == 'TO Customer') ? "selected" : "" }} >To Customer</option>
                    <option value="TO Salesman" {{ ($order_record_billto == 'TO Salesman') ? "selected" : "" }} >To Salesman</option>
                </select>
                @if ($errors->has('order_record_billto'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_record_billto') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="order_dealer">TSA</label>
                <select id="order_dealer" name="order_dealer" class="select2 form-select select-hidden-accessible" {{ $disabled }}>
                    @if($dealer_id != 0)
                        <option value="{{ $dealer_id }}" selected>{{ $dealer_code.' - '.$dealer_name }}</option>
                    @endif
                </select>
                @if ($errors->has('order_dealer'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('order_dealer') }}</span>
                @endif
            </div>
            <div class="form-group mb-3">
                <label for="order_cus_driving">Policy Holder Driving Y/N</label>
                <select name="order_cus_driving" id="order_cus_driving" class="select form-select select-hidden-accessible" {{ $disabled }}>
                    <option value="Y" {{ ($order_cus_driving == 'Y') ? "selected" : "" }}>Yes</option>
                    <option value="N" {{ ($order_cus_driving == 'N') ? "selected" : "" }}>No</option>
                </select>
                @if ($errors->has('order_cus_driving'))
                <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_driving') }}</span>
                @endif
            </div>
            <!-- Customer Section -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_lname">Last Name</label>
                        <input type="text" class="form-control copy_field" id="order_cus_lname" name="order_cus_lname" value="{{ old('order_cus_lname')?:$order_cus_lname }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_lname'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_lname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_fname">First Name</label>
                        <input type="text" class="form-control copy_field" id="order_cus_fname" name="order_cus_fname" value="{{ old('order_cus_fname')?:$order_cus_fname }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_fname'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_fname') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="order_cus_national">Nationality</label>
                <select name="order_cus_national" id="order_cus_national" class="select2 select2-basic form-select select2-hidden-accessible copy_field" {{ $disabled }}>
                    @foreach($country_list as $cli)
                        <option value="{{ $cli['country_id'] }}" {{ ($order_cus_national == $cli['country_id']) ? "selected" : "" }} >{{ $cli['country_code'] }}</option>
                    @endforeach
                </select>
                @if ($errors->has('order_cus_national'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_national') }}</span>
                @endif
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_nirc">Identity Number</label>
                        <input type="text" class="form-control copy_field" id="order_cus_nirc" name="order_cus_nirc" value="{{ old('order_cus_nirc')?:$order_cus_nirc }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_nirc'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_nirc') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_dob">Date Of Birth</label>
                        <input type="text" class="form-control flatpickr-input dob_picker active copy_field" placeholder="DD/MM/YYYY" id="order_cus_dob" name="order_cus_dob" value="{{ old('order_cus_dob')?:$order_cus_dob }}" readonly="readonly" {{ $disabled }}>
                        @if ($errors->has('order_cus_dob'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_dob') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_gender">Gender</label>
                        <select name="order_cus_gender" id="order_cus_gender" class="select form-select select-hidden-accessible copy_field" {{ $disabled }}>
                            <option value="male" {{ ( $order_cus_gender == 'male') ? "selected" : "" }}>Male</option>
                            <option value="female" {{ ( $order_cus_gender == 'female') ? "selected" : "" }}>Female</option>
                        </select>
                        @if ($errors->has('order_cus_gender'))
                        <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_gender') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_exp">Driving Exprience In singapore</label>
                        <input type="number" min="0" inputmode="numeric" pattern="[0-9]*" class="form-control copy_field" id="order_cus_exp" name="order_cus_exp" value="{{ old('order_cus_exp')?:$order_cus_exp }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_exp'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_exp') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <!-- end of Customer Section -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_unitno">Unit Number</label>
                        <input type="text" class="form-control" id="order_cus_unitno" name="order_cus_unitno" value="{{ old('order_cus_unitno')?:$order_cus_unitno }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_unitno'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_unitno') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_build_name">Buliding Name</label>
                        <input type="text" class="form-control" id="order_cus_build_name" name="order_cus_build_name" value="{{ old('order_cus_build_name')?:$order_cus_build_name }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_build_name'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_build_name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_street_name">Street Name</label>
                        <input type="text" class="form-control" id="order_cus_street_name" name="order_cus_street_name" value="{{ old('order_cus_street_name')?:$order_cus_street_name }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_street_name'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_street_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_postal_code">Post Code</label>
                        <input type="number" min="0" inputmode="numeric" pattern="[0-9]*" class="form-control" id="order_cus_postal_code" name="order_cus_postal_code" value="{{ old('order_cus_postal_code')?:$order_cus_postal_code }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_postal_code'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_postal_code') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="order_cus_address">Address</label>
                <textarea class="form-control" rows="5" id="order_cus_address" name="order_cus_address" {{ $disabled }}>{{ old('order_cus_address')?:$order_cus_address }}</textarea>
                @if ($errors->has('order_cus_address'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_address') }}</span>
                @endif
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_contact">Mobile</label>
                        <input type="number" min="0" inputmode="numeric" pattern="[0-9]*" class="form-control val_check" id="order_cus_contact" name="order_cus_contact" value="{{ old('order_cus_contact')?:$order_cus_contact }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_contact'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_contact') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_email">Email</label>
                        <input type="text" class="form-control" id="order_cus_email" name="order_cus_email" value="{{ old('order_cus_email')?:$order_cus_email }}" {{ $disabled }}>
                        @if ($errors->has('order_cus_email'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_email') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_tsa_email">TSA Email</label>
                        <input type="text" class="form-control" id="order_tsa_email" name="order_tsa_email" value="{{ old('order_tsa_email')?:$order_tsa_email }}" {{ $disabled }}>
                        @if ($errors->has('order_tsa_email'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_tsa_email') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_claim">Claim In Last Three Years</label>
                        <select name="order_cus_claim" id="order_cus_claim" class="select form-select select-hidden-accessible" {{ $disabled }}>
                            <option value="0" {{ ($order_cus_claim == '0') ? "selected" : "" }}>0</option>
                            <option value="1" {{ ($order_cus_claim == '1') ? "selected" : "" }}>1</option>
                        </select>
                        @if ($errors->has('order_cus_claim'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_claim') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mb-3">
                        <label for="order_cus_claim_amount">Total Amount Of Claims</label>
                        <input type="number" min="0" inputmode="numeric" pattern="[0-9]*" class="form-control val_check" id="order_cus_claim_amount" name="order_cus_claim_amount" value="{{ old('order_cus_claim_amount')?:$order_cus_claim_amount }}" {{ $disabled }} step="0.01" >
                        @if ($errors->has('order_cus_claim_amount'))
                            <span class="text-danger mt-2 pl-2">{{ $errors->first('order_cus_claim_amount') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="order_promo_code">Promo Code</label>
                <input type="text" class="form-control" id="order_promo_code" name="order_promo_code" value="{{ old('order_promo_code')?:$order_promo_code }}" {{ $disabled }}>
                @if ($errors->has('order_promo_code'))
                    <span class="text-danger mt-2 pl-2">{{ $errors->first('order_promo_code') }}</span>
                @endif
            </div>
            <!-- Vehicle and Package Section -->
            @include('allianz.section.InfoSec2')
            <!-- end of Vehicle and Package Section -->
            <!-- Calculation Section -->
            @include('allianz.section.InfoSec3')
            <!-- end of Calculation Section -->
            <input type="hidden" name="allianz_id" value="{{ $id }}">
            <input type="hidden" name="order_status" id="order_status" value="{{ $order_status }}">
            <div class="text-right">
                <a href="{{ route('AllianzList') }}"  class="btn btn-info"> Back </a>
                @if($order_status == 2)
                <a href="{{ route('AllianzPrint', ['id' => $id] ) }}" target="_blank" class="btn btn-primary ml-2"> Print CI </a>
                @endif
                @if($order_status == 1)
                <button type="button" class="allianz_cal btn btn-warning ml-2">Get Quotation</button>
                <button type="submit" class="btn btn-success ml-2">Save</button>
                <button type="button" id="create_btn" class="btn btn-primary ml-2">Create Policy</button>
                @endif
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="{{ asset('assets/js/allianz.js') }}"></script>
<script>
    $(document).ready(function(){
        @if ($errors->has('order_other_financecode'))
            $("#other_financecode_box").slideDown();
        @endif

        @if ($errors->any())
            $("#driver2_box").slideDown();
            $("#driver3_box").slideDown();
            $("#driver4_box").slideDown();
        @endif

        $("#order_cus_claim").change(function(){
            if($(this).val() > 0){
                $("#order_cus_claim_amount").prop("readonly", false)
            }
            else{
                $("#order_cus_claim_amount").val(0);
                $("#order_cus_claim_amount").prop("readonly", true)
            }
        })

        $(".dob_picker").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            minDate: "{{ $min_age }}",
            maxDate: "{{ $max_age }}",
        });

        $(".alianz_picker").flatpickr({
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
        });

        $(".driver_btn_expand").click(function(){
            var box = $(this).attr('rel');
            if($("#" + box).is(':hidden')){
                $(this).children(".fa").removeClass("fa-plus");
                $(this).children(".fa").addClass("fa-minus");
                $("#" + box).slideDown();
            }
            else{
                $(this).children(".fa").removeClass("fa-minus");
                $(this).children(".fa").addClass("fa-plus");
                $("#" + box).slideUp();
            }
        });

        $("#order_dealer").select2({
            placeholder: "Search for a dealer",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('TSAselect') }}",
                dataType: 'json',
            }
        });

        $("#order_vehicles_reg_year").select2({
            placeholder: "Search a Year",
            minimumInputLength: 3,
        });
        
        $("#order_vehicles_pro_year").select2({
            placeholder: "Search a Year",
            minimumInputLength: 3,
        });

        $("#order_vehicles_type").select2({
            placeholder: "Search for a vehicles make",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('Vehicleselect') }}",
                dataType: 'json',
            }
        });

        $("#order_financecode").select2({
            placeholder: "Search for a Hires Company",
            minimumInputLength: 3,
            ajax:{
                url: "{{ route('HiresCompselect') }}",
                dataType: 'json',
            }
        });

        $("input[type=text], textarea").bind("keyup change", function(){
            var str = $(this).val()
            $(this).val(str.toUpperCase());
        });

        $("#order_period_from").change(function(){
            var dt1 = $(this).val();
            var finalDate = dt1;
            var dt = new Date(finalDate);
            if(!isNaN(dt)){
                dt.setFullYear(dt.getFullYear() + parseInt($("#order_period").val()));
                dt.setDate(dt.getDate() - parseInt(1));
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var disdate = year + "-" + month + "-" + day;
                var new_format = changeDateFormat(disdate);
                console.log(new_format);
                $("#order_period_to").val(disdate);
                $("#order_period_to").next('input').val(new_format);
            }
        });

        $("#order_period").change(function(){
            var dt1 = $("#order_period_from").val();
            var finalDate = dt1;
            var dt = new Date(finalDate);
            if(!isNaN(dt)){
                dt.setFullYear(dt.getFullYear() + parseInt($(this).val()));
                dt.setDate(dt.getDate() - parseInt(1));
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var disdate = year + "-" + month + "-" + day;
                var new_format = changeDateFormat(disdate);
                $("#order_period_to").val(disdate);
                $("#order_period_to").next('input').val(new_format);
            }
        });

        $("#order_delivery_opt").change(function(){
            if($(this).val() == 'n'){
                $("#order_delivery_amt").val('0');
            }
        });

        $(".copy_field").bind("keyup change", function(){
            if($("#order_cus_driving").val() == "Y"){
                var out_id = $(this).attr("id");
                var out_val = $(this).val();
                copyDriver(out_id, out_val);
            }
        });

        $("#order_cus_claim_amount").bind("keyup change", function(){
            $("#span_claim").html($(this).val());
        });

        $(".allianz_cal").click(function(){
            getPremiumBase();
        });

        $("#create_btn").click(function(){
            $("#order_status").val(2);
            $("#AllianzForm").submit();
        });

        $("#order_cus_postal_code").keyup(function(){
            if($('#order_cus_postal_code').val().length >= 6){
                $.get("https://www.onemap.gov.sg/api/common/elastic/search?returnGeom=Y&getAddrDetails=Y&pageNum=1&searchVal=" + $(this).val(), function (response) {
                    $("#order_cus_unitno").val(response.results[0].BLK_NO);
                    $("#order_cus_street_name").val(response.results[0].ROAD_NAME);
                    $("#order_cus_build_name").val(response.results[0].BUILDING);
                    $("#order_cus_address").val(response.results[0].ADDRESS);
                });
            }   
        })

        $(".ic_check").bind("keyup change",function (){
            var field = $(this).attr('id');
            if($(this).val().length > 5){
                ajaxValidation(field, $(this).val());
            }
        })
        
        $("#order_vehicles_no").bind("keyup change",function (){
            if($(this).val().length > 3){
                ajaxValidation("order_vehicles_no", $(this).val());
            }
        });
         
        $(".val_check").bind("keyup change",function (){
            var field = $(this).attr('id');
            ajaxValidation(field, $(this).val());
        });

        $("#order_vehicles_reg_year").change(function (){
            RegYearValidation();
        });

        $("#order_financecode").change(function(){
            if($(this).val() == '115'){
                $("#other_financecode_box").slideDown();
            }
            else{
                $("#order_other_financecode").val('');
                $("#other_financecode_box").slideUp();
            }
        });

        $("#order_new_reg").change(function(){
            if($(this).val() == "N"){
                $("#order_vehicles_no").prop("readonly", false)
            }
            else{
                $("#order_vehicles_no").val('');
                $("#order_vehicles_no").prop("readonly", true)
            }
        })
    });

    function getPremiumBase(){
        var input = {
            'driver_nirc_1' : $('#driver_nirc_1').val(),
            'driver_dob_1' : $('#driver_dob_1').val(),
            'driver_exp_1' : $('#driver_exp_1').val(),
            'driver_nirc_2' : $('#driver_nirc_2').val(),
            'driver_dob_2' : $('#driver_dob_2').val(),
            'driver_exp_2' : $('#driver_exp_2').val(),
            'driver_nirc_3' : $('#driver_nirc_3').val(),
            'driver_dob_3' : $('#driver_dob_3').val(),
            'driver_exp_3' : $('#driver_exp_3').val(),
            'driver_nirc_4' : $('#driver_nirc_4').val(),
            'driver_dob_4' : $('#driver_dob_4').val(),
            'driver_exp_4' : $('#driver_exp_4').val(),
            'order_delivery_opt' : $('#order_delivery_opt').val(),
            'order_vehicles_reg_year' : $('#order_vehicles_reg_year').val(),
            'order_vehicles_capacity' : $('#order_vehicles_capacity').val(),
            'order_coverage' : $('#order_coverage').val(),
            'order_period_from' : $('#order_period_from').val(),
            'order_period_to' : $('#order_period_to').val(),
            'order_cus_claim_amount' : $('#order_cus_claim_amount').val(),
            'order_ncd_entitlement' : $('#order_ncd_entitlement').val(),
        };
        $.ajax({
            type: "POST",
            url: "{{ route('AllianzCal') }}",
            dataType: 'json',
            data: input,
            success: function(res) {
                $("#order_premium_amt").val(RoundNum(res.order_premium_amt,2));
                $("#order_accident_amt").val(res.order_accident_amt);
                $("#order_ncd_dis_amt").val(res.order_ncd_dis_amt);
                $("#order_in_exp_amt").val(res.order_in_exp_amt);
                $("#order_addtional_amt").val(res.order_addtional_amt);
                $("#order_delivery_amt").val(res.order_delivery_amt);
                $("#order_excess_amount").val(res.order_excess_amount);
                $("#order_discount").val(res.order_discount);
                calTSACompProfit();
            }
        })
    }

    function ajaxValidation(field, value){
        $.ajax({
            type: "POST",
            url: "{{ route('AllianzValidate') }}",
            dataType: 'json',
            data: {'field' : field, 'value' : value},
            success: function(res) {
                if($("#" + field).next(".text-danger").length > 0){
                    var error_span = $("#" + field).next(".text-danger");
                }

                if(res.status == 'failed'){
                    if(error_span){
                        error_span.html(res.msg);
                    }
                    else{
                        var html = '<span class="text-danger mt-2 pl-2">'+res.msg+'</span>';
                        if($("#" + field).next("input").length > 0){
                            $("#" + field).next("input").after(html);
                        }
                        else{
                            $("#" + field).after(html);
                        }
                    }
                }
                else{
                    if(error_span){
                        error_span.remove();
                    }
                }
            }
        })
    }

    function RegYearValidation(){
        $.ajax({
            type: "POST",
            url: "{{ route('AllianzValidate') }}",
            dataType: 'json',
            data: {'order_vehicles_reg_year' : $("#order_vehicles_reg_year").val(), 'order_coverage' : $("#order_coverage").val()},
            success: function(res) {
                if($("#order_vehicles_reg_year").next("span").length > 0){
                    var error_span = $$("#order_vehicles_reg_year").next("span");
                }

                if(res.status == 'failed'){
                    if(error_span){
                        error_span.html(res.msg);
                    }
                    else{
                        var html = '<span class="text-danger mt-2 pl-2">'+res.msg+'</span>'
                        $$("#order_vehicles_reg_year").after(html);
                    }
                }
                else{
                    if(error_span){
                        error_span.remove();
                    }
                }
            }
        })
    }
</script>
@include('includes.footer')