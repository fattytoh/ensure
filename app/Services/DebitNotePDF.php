<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;

class DebitNotePDF extends BasePDF {
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        $this->SetFont('helvetica','B',12);
        $this->Cell(100,10, "DEBIT NOTE LISTING - (".strtoupper(date('d-M-Y',strtotime($this->period_from)))." - ".strtoupper(date('d-M-Y',strtotime($this->period_to))).")",0,0,'L');
        $this->SetFont('helvetica','',9);
        $this->Ln(10);
        $this->Cell(100,3, "Filter By : ".$this->insurance_type_mapping[$this->filter_by] ,0,0,'L');
        $this->Ln(5);
        $title_height = $this->GetY();
        if(file_exists($this->cprofile_logo_url)){
            $this->SetY(2);
            $tmy = $iniy = $this->GetY();
            $new_height = 0;
            $new_width = 20;
            list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
            $width_scalling_per = (($new_width / $img_width) * 100);
            $new_height = ($img_height/100) * $width_scalling_per;
            $this->Image($this->cprofile_logo_url, 260, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $logo_height = $this->GetY() + $new_height;
        }
        
        if($logo_height > $title_height){
            $this->line(10,$logo_height + 5,285,$logo_height + 5);
            $this->header_title_height = $logo_height+10 ;
        }
        else{
            $this->line(10,$title_height + 5,285,$title_height + 5);
            $this->header_title_height = $title_height+10 ;
        }
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $number_addtional = 0;
        $this->AddPage("L");
        $this->SetFont($this->font_name,'BU',6);
        $this->SetAutoPageBreak(true, 10);
        $this->SetY($this->header_title_height);
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        $maxlength = 270;
        $ini_height = 3;
        $land_height = 170;
        $total_width = 0;

        if(in_array($this->filter_by,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "No" => array('width'=>13.5),
                "Reg No" => array('width'=>32.4, 'param'=>'order_vehicles_no'),
                "Cover Note" => array('width'=>32.4, 'param'=>'order_cvnote'),
                "Policy#" => array('width'=>32.4, 'param'=>'order_policyno'),
                "Commence Date" =>array('width'=>21.6, 'param'=>'order_date'),
                "Insured Name" => array('width'=>64.8, 'param'=>'partner_name'),
                "Premium Receivable" => array('width'=>32.4, 'param'=>'order_receivable_premiumreceivable', 'number' => true, 'position' => 'R'),
                "Debit Note #" => array('width'=>32.4, 'param'=>'order_no','position' => 'R'),
            );
        }
        else{
            $header = array(
                "No" => array('width'=>13.5),
                "Coverage Plan" => array('width'=>35.1, 'param'=>'order_coverage'),
                "Policy#" => array('width'=>35.1, 'param'=>'order_policyno'),
                "Commence Date" =>array('width'=>21.6, 'param'=>'order_date'),
                "Insured Name" => array('width'=>89.1, 'param'=>'partner_name'),
                "Premium Receivable" => array('width'=>35.1, 'param'=>'order_receivable_premiumreceivable', 'number' => true ,'position' => 'R'),
                "Debit Note #" => array('width'=>35.1, 'param'=>'order_no','position' => 'R'),
            );
        }
        
        foreach($header as $ky => $li){
            $width = $li['width'];
            
            if($ky == 'Premium Receivable'){
                $pre_width = $width;
            }
            
            if(in_array($ky, array('No',"Coverage Plan","Reg No","Cover Note","Policy#","Commence Date","Insured Name"))){
                $total_width = $total_width + $width;
            }
        }
        
        $tmx = 15;
        $tmy = $this->generateColumnHeader($this->GetY(), $tmx, $header);
        $this->SetFont($this->font_name,'',8);
        $total = 0;
        
        if($this->dealer_id > 0 && $this->cus_id > 0){
            $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
            $this->SetFont($this->font_name,'BUI',8);
            $dealer_data = $this->getDealerInfoByID($this->dealer_id);
            $partner_data = $this->getPartnerInfoByID($this->cus_id);
            $this->MultiCell($maxlength,5,"TSA :".$dealer_data['dealer_name']." & Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,20,$tmy);
            $this->SetFont($this->font_name,'',8);
        }
        else{
            if($this->dealer_id > 0){
                $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                $this->SetFont($this->font_name,'BUI',8);
                $dealer_data = $this->getDealerInfoByID($this->dealer_id);
                $this->MultiCell($maxlength,5,"TSA :".htmlspecialchars_decode($dealer_data['dealer_name']),0,'L',0,1,20,$tmy);
                $this->SetFont($this->font_name,'',8);
            }
            elseif($this->cus_id > 0){
                $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                $this->SetFont($this->font_name,'BUI',8);
                $partner_data = $this->getPartnerInfoByID($this->cus_id);
                $this->MultiCell($maxlength,5,"Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,20,$tmy);
                $this->SetFont($this->font_name,'',8);
            }
        }
        
        foreach($this->data_listing as $li_ky => $li_data){
            $tmx = 15;
            if($this->group_by ){
                if($this->group_by == 'customer' && !$this->cus_id ){
                    $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',8);
                        $tmy = $this->generateColumnHeader($this->GetY(), $tmx, $header);
                        $this->SetFont($this->font_name,'',8);
                    }
                    $this->SetFont($this->font_name,'BUI',8);
                    $partner_data = $this->getPartnerInfoByID($li_ky);
                    $this->MultiCell($maxlength,5,"Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,$tmx + 5,$tmy);
                    $this->SetFont($this->font_name,'',8);
                }
                elseif($this->group_by == 'dealer' && !$this->dealer_id){
                    $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',8);
                        $tmy = $this->generateColumnHeader($this->GetY(), $tmx, $header);
                        $this->SetFont($this->font_name,'',8);
                    }
                    $this->SetFont($this->font_name,'BUI',8);
                    $dealer_data = $this->getDealerInfoByID($li_ky);
                    $this->MultiCell($maxlength,5,"TSA :".htmlspecialchars_decode($dealer_data['dealer_name']),0,'L',0,1,$tmx + 5,$tmy);
                    $this->SetFont($this->font_name,'',8);
                }
                $tmy = $this->GetY();
               
                $sub_total = 0;
                foreach($li_data as $item_key => $item_dis){
                    $ini_height =3;
                    $tmx = 15;
                    foreach($header as $ky => $li){
                        $width = $li['width'];
                        $ini_height = $this->checkMulticellHeight($item_dis[$li['param']], $width, $ini_height);
                    }
                    $tmy = $this->checkYaxisoverflow($tmy, $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',8);
                        $tmy = $this->generateColumnHeader($this->GetY(), $tmx, $header);
                        $this->SetFont($this->font_name,'',8);
                    }
                    if(($item_key + 1) % 2 == 0){
                        $this->Rect($tmx, $tmy, $maxlength, $ini_height,'DF',$list_border_style,$list_react_color_style);
                    }
                    foreach($header as $ky => $li){
                        $width = $li['width'];
                        if(isset($li['param'])){
                            if(isset($li['number'])){
                                if($item_dis[$li['param']] > 0){
                                    $this->MultiCell($width,$ini_height,$this->num_format($item_dis[$li['param']]),0,'R',0,1,$tmx,$tmy);
                                }
                                elseif($item_dis[$li['param']] == 0){
                                    $this->MultiCell($width,$ini_height,'',0,'R',0,1,$tmx,$tmy);
                                }
                                else{
                                    $this->MultiCell($width,$ini_height,"(".$this->num_format(abs($item_dis[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                                }
                                $total = $total + $item_dis[$li['param']];
                                $sub_total += $item_dis[$li['param']];
                            }
                            elseif($li['param'] == 'order_no'){
                                $this->MultiCell($width,$ini_height,$item_dis[$li['param']],0,'R',0,1,$tmx,$tmy);
                            }
                            else{
                                $this->MultiCell($width,$ini_height,$item_dis[$li['param']],0,'L',0,1,$tmx,$tmy);
                            }
                        }
                        else{
                            $this->MultiCell($width,$ini_height,$item_key + 1,0,'L',0,1,$tmx,$tmy);
                        }
                        $tmx = $tmx + $width;
                    }
                   $tmy = $this->GetY();
                    
                }
                $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',8);
                    $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
                    $this->SetFont($this->font_name,'',8);
                }
                $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->line(15,$tmy,$maxlength,$tmy);
                $tmy = $tmy + 5;
                $tmy = $this->checkYaxisoverflow($tmy, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',8);
                    $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
                    $this->SetFont($this->font_name,'',8);
                }
                $sub_width = $total_width+15;
                $this->MultiCell(20,3,"SUB Total :",0,'L',0,1,15,$tmy);
                $this->MultiCell($pre_width,3,$this->num_format($sub_total),0,'R',0,1,$sub_width,$tmy);
                $tmy = $this->GetY();
            }
            else{
                $tmy = $this->GetY();
                $ini_height =3;
                foreach($header as $ky => $li){
                    $width = $li['width'];
                    $ini_height = $this->checkMulticellHeight($li_data[$li['param']], $width, $ini_height);
                }
                $tmy = $this->checkYaxisoverflow($tmy, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',8);
                    $tmy = $this->generateColumnHeader($this->GetY(), $tmx, $header);
                    $this->SetFont($this->font_name,'',8);
                }
                if(($li_ky + 1) % 2 == 0){
                    $this->Rect($tmx, $tmy, $maxlength, $ini_height,'DF',$list_border_style,$list_react_color_style);
                }
                foreach($header as $ky => $li){
                    $width = $li['width'];
                    if(isset($li['param'])){
                        if(isset($li['number'])){
                            if($li_data[$li['param']] > 0){
                                $this->MultiCell($width,$ini_height,$this->num_format($li_data[$li['param']]),0,'R',0,1,$tmx,$tmy);
                            }
                            elseif($li_data[$li['param']] == 0){
                                $this->MultiCell($width,$ini_height,'',0,'R',0,1,$tmx,$tmy);
                            }
                            else{
                                $this->MultiCell($width,$ini_height,"(".$this->num_format(abs($li_data[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                            }
                            $total = $total + $li_data[$li['param']];
                        }
                        elseif($li['param'] == 'order_no'){
                            $this->MultiCell($width,$ini_height,$li_data[$li['param']],0,'R',0,1,$tmx,$tmy);
                        }
                        else{
                            $this->MultiCell($width,$ini_height,$li_data[$li['param']],0,'L',0,1,$tmx,$tmy);
                        }
                    }
                    else{
                        $this->MultiCell($width,$ini_height,$li_ky + 1,0,'L',0,1,$tmx,$tmy);
                    }
                    $tmx = $tmx + $width;
                }
            }
        } 
        
        $tmy = $tmy + 10;
        $this->SetFont($this->font_name,'B',8);
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(15,$tmy,$maxlength,$tmy);
        $tmy = $tmy + 5;
        $this->MultiCell(20,3,"Total :",0,'L',0,1,15,$tmy);
        $this->MultiCell($pre_width,3,$this->num_format($total),0,'R',0,1,$total_width+15,$tmy);
        $this->SetFont($this->font_name,'',8);
    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }
}