<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;

class SalesReportPDF extends BasePDF {
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        $this->SetFont('helvetica','B',12);
        $this->Cell(100,10, "SALES REPORT",0,0,'L');
        
        $this->SetFont('helvetica','',9);
        $this->Ln(10);
        $this->Cell(100,3, "Period : ".strtoupper(date('d-M-Y',strtotime($this->period_from)))." - ".strtoupper(date('d-M-Y',strtotime($this->period_to))) ,0,0,'L');
        $this->Ln(3);
        $this->Cell(100,3, "Filter By : ".$this->insurance_type_mapping[$this->insurance_type] ,0,0,'L');
        $this->Ln(3);
        if($this->order_dealer > 0){
            $this->SetFont($this->font_name,'',9);
            $dealer_data = $this->getDealerInfoByID($this->order_dealer);
            $this->MultiCell(270,5,"TSA :".htmlspecialchars_decode($dealer_data['dealer_name']),0,'L',0,0,10,$this->GetY());
            $this->SetFont($this->font_name,'',6);
        }
        elseif($this->order_customer > 0){
            $this->SetFont($this->font_name,'',9);
            $partner_data = $this->getPartnerInfoByID($this->order_customer);
            $this->MultiCell(270,5,"Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,0,10,$this->GetY());
            $this->SetFont($this->font_name,'',6);
        }
       
        $title_height = $this->GetY();
        if(file_exists($this->cprofile_logo_url)){
            $this->SetY(2);
            $tmy = $iniy = $this->GetY();
            $new_height = 0;
            $new_width = 20;
            list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
            $width_scalling_per = (($new_width / $img_width) * 100);
            $new_height = ($img_height/100) * $width_scalling_per;
            $this->Image($this->cprofile_logo_url, 260, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $logo_height = $this->GetY() + $new_height;
        }
        
        if($logo_height > $title_height){
            $this->line(10,$logo_height + 5,285,$logo_height + 5);
            $this->header_title_height = $logo_height+10 ;
        }
        else{
            $this->line(10,$title_height + 5,285,$title_height + 5);
            $this->header_title_height = $title_height+10 ;
        }
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $this->AddPage("L");
        $this->land_height = 170;
        $this->SetFont($this->font_name,'BU',6);
        $this->SetAutoPageBreak(true, 10);
        $this->SetY($this->header_title_height);
        
        $maxlength = 275;
        $ini_height = 3;

        $header = array(
            "No" => array('width'=>8),
            "Issued Date" => array('width'=>13, 'param'=>'order_date'),
            "Document Number" =>array('width'=>15, 'param'=>'order_no'),
            "Insured Name" => array('width'=>25, 'param'=>'partner_name'),
            "Name Of TSA" => array('width'=>25, 'param'=>'dealer_name'),
            "Related Info" => array('width'=>25, 'param'=>'related_info'),
            "Policy Number" =>array('width'=>22, 'param'=>'order_policyno'),
            "Premium Charges" =>array('width'=>16, 'param'=>'order_grosspremium_amount', 'number' => true, 'position' => 'R'),
            "TSA Comm" => array('width'=>16, 'param'=>'total_tsa_comm', 'number' => true, 'position' => 'R'),
            "TSA Premium charge" => array('width'=>16, 'param'=>'order_receivable_premiumreceivable', 'number' => true, 'position' => 'R'),
            "Nett premium" => array('width'=>16, 'param'=>'order_subtotal_amount', 'number' => true, 'position' => 'R'),
            "Premium GST" => array('width'=>16, 'param'=>'order_gst_amount', 'number' => true, 'position' => 'R'),
            "Commission to company" => array('width'=>16, 'param'=>'order_payable_ourcomm', 'number' => true, 'position' => 'R'),
            "GST on commission" => array('width'=>16, 'param'=>'order_payable_gstcomm', 'number' => true, 'position' => 'R'),
            "Profit/ Loss" => array('width'=>16, 'param'=>'order_payable_nettcomm', 'number' => true, 'position' => 'R'),
        );     
                
        $tmx = 15;
        $tmy = $this->generateColumnHeader($this->GetY(), $tmx, $header);
        $this->total = array(
            'order_grosspremium_amount'=> 0,
            'total_tsa_comm'=> 0,
            'order_receivable_premiumreceivable'=> 0,
            'order_subtotal_amount'=> 0,
            'order_gst_amount'=> 0,
            'order_payable_ourcomm'=> 0,
            'order_payable_gstcomm'=> 0,
            'order_payable_nettcomm'=> 0,
        );
        $this->SetFont($this->font_name,'',6);
        
        if($this->insurance_type == 'ALL' ){
            foreach($this->insurance_type_mapping as $mky => $mli){
                if(!isset($this->data_listing[$mky]) || count($this->data_listing[$mky]) <= 0){
                    continue;
                }
                else{
                    $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height);
                    $this->SetFont($this->font_name,'BUI',6);
                    $this->MultiCell(275,3,$mli,0,'L',0,1,15,$tmy);
                    $this->SetFont($this->font_name,'',6);
                    $this->PdfContent($header, $this->data_listing[$mky]);
                }
            }
            
        }
        else{
            $this->PdfContent($header, $this->data_listing);
        }
        
        
        $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height);

        $this->SetFont($this->font_name,'B',6);
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(15,$tmy,275,$tmy);

        $tmy = $this->checkYaxisoverflow($this->GetY() + 10, $this->land_height);

        $this->MultiCell(20,3,"Total :",0,'L',0,1,15,$tmy);
        $ini_height = 5;
        $tmx = 15;
        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $this->total)){
                $width = $li['width'];
                $ini_height = $this->checkMulticellHeight($this->total[$li['param']], $width, $ini_height);
            }
        }

        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $this->total)){
                if($this->total[$li['param']] > 0){
                    $this->MultiCell($li['width'],$ini_height,$this->num_format($this->total[$li['param']]),0,'R',0,1,$tmx,$tmy);
                }
                elseif($this->total[$li['param']] == 0){
                    $this->MultiCell($li['width'],$ini_height,'',0,'R',0,1,$tmx,$tmy);
                }
                else{
                    $this->MultiCell($li['width'],$ini_height,"(".abs($this->num_format($this->total[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                }
            }
            $tmx += $li['width'];
        }
    }

    public function PdfContent($header , $input_data){
        $tmy = $this->GetY();
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        
        $sub_total = array(
            'order_grosspremium_amount'=> 0,
            'total_tsa_comm'=> 0,
            'order_receivable_premiumreceivable'=> 0,
            'order_subtotal_amount'=> 0,
            'order_gst_amount'=> 0,
            'order_payable_ourcomm'=> 0,
            'order_payable_gstcomm'=> 0,
            'order_payable_nettcomm'=> 0,
        );

        $total = 0;

        foreach($input_data as $li_ky => $li_data){
            $tmx = 15;
            $ini_height =3;
            foreach($header as $ky => $li){
                $width = $li['width'];
                if(isset($li['param']) && isset($li_data[$li['param']])){
                    $ini_height = $this->checkMulticellHeight($li_data[$li['param']], $width, $ini_height);
                }
            }
            $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
            if($this->isAddPage){
                $this->SetFont($this->font_name,'BU',6);
                $tmy = $this->generateColumnHeader($this->GetY(), $tmx, $header);
                $this->SetFont($this->font_name,'',6);
            }
            if(($li_ky + 1) % 2 == 0){
                $this->Rect($tmx, $tmy, 260, $ini_height,'DF',$list_border_style,$list_react_color_style);
            }
            foreach($header as $ky => $li){

                $width = $li['width'];
                if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                    $sub_total[$li['param']] += $li_data[$li['param']];
                    $this->total[$li['param']] += $li_data[$li['param']];
                }

                if(isset($li['param'])){
                    if(isset($li['number'])){
                        if(isset($li['param']) && $li_data[$li['param']] > 0){
                            $this->MultiCell($width,$ini_height,$this->num_format($li_data[$li['param']]),0,'R',0,1,$tmx,$tmy);
                        }
                        elseif(isset($li['param']) && $li_data[$li['param']] == 0){
                            if($li['param'] == 'order_gst_amount'){
                                $this->MultiCell($width,$ini_height,'0.00',0,'R',0,1,$tmx,$tmy);
                            }else{
                                $this->MultiCell($width,$ini_height,'',0,'R',0,1,$tmx,$tmy);
                            }
                            
                        }
                        else{
                            $this->MultiCell($width,$ini_height,"(".abs($this->num_format($li_data[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                        }
                        $total = $total + $li_data[$li['param']];
                    }
                    elseif(isset($li['param']) && $li['param'] == 'order_no'){
                        $this->MultiCell($width,$ini_height,$li_data[$li['param']],0,'R',0,1,$tmx,$tmy);
                    }
                    else{
                        $this->MultiCell($width,$ini_height,$li_data[$li['param']],0,'L',0,1,$tmx,$tmy);
                    }
                }
                else{
                    $this->MultiCell($width,$ini_height,$li_ky + 1,0,'L',0,1,$tmx,$tmy);
                }
                $tmx = $tmx + $width;
            }
        }

        $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height);

        $this->SetFont($this->font_name,'B',6);
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(15,$tmy,275,$tmy);

        $tmy = $this->checkYaxisoverflow($this->GetY() + 10 , $this->land_height);

        $this->MultiCell(20,3,"Sub Total :",0,'L',0,1,15,$tmy);
        $ini_height = 3;
        $tmx = 15;
        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                $width = $li['width'];
                $ini_height = $this->checkMulticellHeight($sub_total[$li['param']], $width, $ini_height);
            }
        }

        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                if(isset($sub_total[$li['param']]) && $sub_total[$li['param']] > 0){
                    $this->MultiCell($li['width'],$ini_height,$this->num_format($sub_total[$li['param']]),0,'R',0,1,$tmx,$tmy);
                }
                elseif(isset($sub_total[$li['param']]) && $sub_total[$li['param']] == 0){
                    $this->MultiCell($li['width'],$ini_height,'',0,'R',0,1,$tmx,$tmy);
                }
                else{
                    $this->MultiCell($li['width'],$ini_height,"(".abs($this->num_format($sub_total[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                }
            }
            $tmx += $li['width'];
        }

    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }
}