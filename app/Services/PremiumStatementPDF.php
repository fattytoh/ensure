<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;

class PremiumStatementPDF extends BasePDF {
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        $this->SetFont('helvetica','B',12);
        $this->Cell(100,10, "Premium Statement For The Month Of ".strtoupper(date('m/Y',strtotime($this->period_from))),0,0,'L');
        $this->SetFont('helvetica','',9);
        $this->Ln(5);
        $this->Cell(100,10, "Filter By : ".$this->insurance_type_mapping[$this->filter_by] ,0,0,'L');
        
        $title_height = $this->GetY();
        if(file_exists($this->cprofile_logo_url)){
            $this->SetY(2);
            $tmy = $iniy = $this->GetY();
            $new_height = 0;
            $new_width = 20;
            list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
            $width_scalling_per = (($new_width / $img_width) * 100);
            $new_height = ($img_height/100) * $width_scalling_per;
            $this->Image($this->cprofile_logo_url, 260, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $logo_height = $this->GetY() + $new_height;
        }
        if($logo_height > $title_height){
            $this->line(10,$logo_height + 5,285,$logo_height + 5);
            $this->header_title_height = $logo_height+10 ;
        }
        else{
            $this->line(10,$title_height + 5,285,$title_height + 5);
            $this->header_title_height = $title_height+10 ;
        }
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $this->AddPage("L");
        $this->SetFont($this->font_name,'BU',6);
        $this->SetAutoPageBreak(true, 10);
        $this->SetY($this->header_title_height);
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        $maxlength = 270;
        $line_len = 280;
        $ini_height = 3;
        $land_height = 170;
        if(in_array($this->filter_by,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "No" => array('width'=>8),
                "Date" => array('width'=>25, 'param'=>'order_date'),
                "Ins Type" => array('width'=>13.5, 'param'=>'order_prefix_type'),
                "Insured Name" => array('width'=>35, 'param'=>'partner_name'),
                "Veh. No CoverType" => array('width'=>35, 'param'=>'order_vehicles_no'),
                "Gr. Premium" => array('width'=>24.84, 'param'=>'order_gross_premium', 'number' => true ,'position'=> 'R'),
                "NCD %" => array('width'=> 13.5, 'param'=>'order_ncd_dis_per', 'number' => true,'position'=> 'R'),
                "Our Comm %" => array('width'=>24.84, 'param'=>'order_payable_ourcommpercent', 'number' => true,'position'=> 'R'),
                "GST" => array('width'=>24.84, 'param'=>'order_receivable_gstamount', 'number' => true,'position'=> 'R'),
                "Direct Disc" => array('width'=>24.84, 'param'=>'order_direct_discount_amt', 'number' => true,'position'=> 'R'),
                "Premium Receivable" => array('width'=>24.84, 'param'=>'order_receivable_premiumreceivable','number' => true,'position'=> 'R'),
            );
        }
        else{
            $header = array(
                "No" => array('width'=>8),
                "Date" => array('width'=>25, 'param'=>'order_date'),
                "Ins Type" => array('width'=>13.5, 'param'=>'order_prefix_type'),
                "Insured Name" => array('width'=>35, 'param'=>'partner_name'),
                "Coverage" => array('width'=>35, 'param'=>'order_coverage'),
                "Gr. Premium" => array('width'=>24.84, 'param'=>'order_gross_premium', 'number' => true,'position'=> 'R'),
                "Our Comm %" => array('width'=>24.84, 'param'=>'order_payable_ourcommpercent', 'number' => true,'position'=> 'R'),
                "GST" => array('width'=>24.84, 'param'=>'order_receivable_gstamount', 'number' => true,'position'=> 'R'),
                "Direct Disc" => array('width'=>24.84, 'param'=>'order_direct_discount_amt','number' => true,'position'=> 'R'),
                "Premium Receivable" => array('width'=>24.84, 'param'=>'order_receivable_premiumreceivable', 'number' => true,'position'=> 'R'),
                "Document No." => array('width'=>24.84, 'param'=>'order_no','position'=> 'L'),
            );
        }
        
        foreach($header as $ky => $li){
            $width = $li['width'];

            if($ky == 'Premium Receivable'){
                $pre_width = $width;
            }
        }
        
        $tmx = 10;
        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        $tmy = $this->generateColumnHeader($tmy, 10, $header);
        $this->SetFont($this->font_name,'',6);
        $tmy = $tmy + $ini_height;

        $total = array(
            'order_suminsured_amt' => 0,
            'order_gross_premium' => 0,
            'order_receivable_gstamount' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_premiumreceivable' => 0,
        );
        if(!$this->group_by){
            if($this->dealer_id > 0){
                $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $this->SetFont($this->font_name,'BUI',6);
                $dealer_data = $this->getDealerInfoByID($this->dealer_id);
                $this->MultiCell($maxlength,5,"TSA :".htmlspecialchars_decode($dealer_data['dealer_name']),0,'L',0,1,20,$tmy);
                $this->SetFont($this->font_name,'',6);
            }
            
            if($this->cus_id > 0){
                $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $this->SetFont($this->font_name,'BUI',6);
                $partner_data = $this->getPartnerInfoByID($this->cus_id);
                $this->MultiCell($maxlength,5,"Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,20,$tmy);
                $this->SetFont($this->font_name,'',6);
            }
            
            if($this->insurer_id > 0){
                $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $this->SetFont($this->font_name,'BUI',6);
                $ins_data = $this->getInsuranceComapnyInfoByID($this->insurer_id);
                $this->MultiCell($maxlength,5,"Insurance Comapny :".htmlspecialchars_decode($ins_data['insuranceco_name']),0,'L',0,1,20,$tmy);
                $this->SetFont($this->font_name,'',6);
            }
        }
        
        foreach($this->data_listing as $li_ky => $li_data){
            $tmx = 10;
            if($this->group_by ){
                if($this->group_by == 'customer' && !$this->cus_id ){
                    $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',6);
                        $tmy = $this->generateColumnHeader($tmy, 10, $header);
                        $this->SetFont($this->font_name,'',6);
                    }
                    $this->SetFont($this->font_name,'BUI',6);
                    $partner_data = $this->getPartnerInfoByID($li_ky);
                    $this->MultiCell($maxlength,5,"Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,$tmx + 5,$tmy);
                    $this->SetFont($this->font_name,'',6);
                }
                elseif($this->group_by == 'dealer' && !$this->dealer_id){
                    $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',6);
                        $tmy = $this->generateColumnHeader($tmy, 10, $header);
                        $this->SetFont($this->font_name,'',6);
                    }
                    $this->SetFont($this->font_name,'BUI',6);
                    $dealer_data = $this->getDealerInfoByID($li_ky);
                    $this->MultiCell($maxlength,5,"TSA :".$dealer_data['dealer_name'],0,'L',0,1,$tmx + 5,$tmy);
                    $this->SetFont($this->font_name,'',6);
                }
                elseif($this->group_by == 'insuranceco' && !$this->insurer_id){
                    $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',6);
                        $tmy = $this->generateColumnHeader($tmy, 10, $header);
                        $this->SetFont($this->font_name,'',6);
                    }
                    $this->SetFont($this->font_name,'BUI',6);
                    $ins_data = $this->getInsuranceComapnyInfoByID($li_ky);
                    $this->MultiCell($maxlength,5,"Insurance Company :".$ins_data['insuranceco_name'],0,'L',0,1,$tmx + 5,$tmy);
                    $this->SetFont($this->font_name,'',6);
                }
                
                $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $sub_total = array(
                    'order_suminsured_amt' => 0,
                    'order_gross_premium' => 0,
                    'order_receivable_gstamount' => 0,
                    'order_direct_discount_amt' => 0,
                    'order_receivable_premiumreceivable' => 0,
                );
                
                foreach($li_data as $item_key => $item_dis){
                    $ini_height =3;
                    $tmx = 10;
                    foreach($header as $ky => $li){
                        if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                            $sub_total[$li['param']] += $item_dis[$li['param']];
                            $total[$li['param']] += $item_dis[$li['param']];
                        }

                        if(isset($li['param']) && $li['param'] == 'order_vehicles_no'){
                            $width = $li['width'];
                            $ini_height = $this->checkMulticellHeight($item_dis[$li['param']]."(".$item_dis['order_coverage'].")", $width, $ini_height);
                        }
                        else{
                            $width = $li['width'];
                            $ini_height = $this->checkMulticellHeight($item_dis[$li['param']], $width, $ini_height);
                        }
                    }
                    $tmy = $this->checkYaxisoverflow($tmy, $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',6);
                        $tmy = $this->generateColumnHeader($tmy, 10, $header);
                        $this->SetFont($this->font_name,'',6);
                    }
                    if(($item_key + 1) % 2 == 0){
                        $this->Rect($tmx, $tmy, $line_len, $ini_height,'DF',$list_border_style,$list_react_color_style);
                    }
                   
                    foreach($header as $ky => $li){
                        $width = $li['width'];
                        if(isset($li['param'])){
                            if($li['number']){
                                if($item_dis[$li['param']] > 0){
                                    $this->MultiCell($width,$ini_height,$this->num_format($item_dis[$li['param']]),0,'R',0,1,$tmx,$tmy);
                                }
                                elseif($item_dis[$li['param']] == 0){
                                    $this->MultiCell($width,$ini_height,'',0,'R',0,1,$tmx,$tmy);
                                }
                                else{
                                    $this->MultiCell($width,$ini_height,"(".$this->num_format(abs($item_dis[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                                }
                            }
                            elseif($li['param'] == 'order_vehicles_no'){
                                $this->MultiCell($width,$ini_height,$item_dis[$li['param']]."(".$item_dis['order_coverage'].")",0,'L',0,1,$tmx,$tmy);
                            }
                            else{
                                $this->MultiCell($width,$ini_height,$item_dis[$li['param']],0,'L',0,1,$tmx,$tmy);
                            }
                        }
                        else{
                            $this->MultiCell($width,$ini_height,$item_key + 1,0,'L',0,1,$tmx,$tmy);
                        }
                        $tmx = $tmx + $width;
                    }
                    
                    $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
                    if($this->isAddPage){
                        $this->SetFont($this->font_name,'BU',6);
                        $tmy = $this->generateColumnHeader($tmy, 10, $header);
                        $this->SetFont($this->font_name,'',6);
                    }
                    
                }
                $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->line(10,$tmy,$line_len+10,$tmy);
                $tmy = $tmy + 5;
                $tmy = $this->checkYaxisoverflow($tmy, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }

                $this->MultiCell(20,3,"SUB Total :",0,'L',0,1,10,$tmy);
                $tmx = 10;
                foreach($header as $ky => $li){
                    if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                        $this->MultiCell($li['width'],3,$this->num_format($sub_total[$li['param']]),0,'R',0,1,$tmx,$tmy);
                    }
                    $tmx = $tmx + $li['width'];
                }
                $tmy = $this->GetY();
            }
            else{
                $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $ini_height =3;
                foreach($header as $ky => $li){
                    if(isset($li['param']) && $li['param'] == 'order_vehicles_no'){
                        $width = $li['width'];
                        $ini_height = $this->checkMulticellHeight($li_data[$li['param']]."(".$li_data['order_coverage'].")", $width, $ini_height);
                    }else{
                        $width = $li['width'];
                        $ini_height = $this->checkMulticellHeight($li_data[$li['param']], $width, $ini_height);
                    }
                }
                $tmy = $this->checkYaxisoverflow($tmy, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                if(($li_ky + 1) % 2 == 0){
                    $this->Rect($tmx, $tmy, $line_len, $ini_height,'DF',$list_border_style,$list_react_color_style);
                }

                foreach($header as $ky => $li){
                    $width = $li['width'];
                    if(isset($li['param']) && array_key_exists($li['param'], $total)){
                        $total[$li['param']] += $li_data[$li['param']];
                    }
                    if(isset($li['param'])){
                        if($li['number']){
                            if($li_data[$li['param']] > 0){
                                $this->MultiCell($width,$ini_height,$this->num_format($li_data[$li['param']]),0,'R',0,1,$tmx,$tmy);
                            }
                            elseif($li_data[$li['param']] == 0){
                                $this->MultiCell($width,$ini_height,'',0,'R',0,1,$tmx,$tmy);
                            }
                            else{
                                $this->MultiCell($width,$ini_height,"(".$this->num_format(abs($li_data[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                            }
                        }
                        elseif($li['param'] == 'order_vehicles_no'){
                            $this->MultiCell($width,$ini_height,$li_data[$li['param']]."(".$li_data['order_coverage'].")",0,'L',0,1,$tmx,$tmy);
                        }
                        else{
                            $this->MultiCell($width,$ini_height,$li_data[$li['param']],0,'L',0,1,$tmx,$tmy);
                        }
                    }
                    else{
                        $this->MultiCell($width,$ini_height,$li_ky + 1,0,'L',0,1,$tmx,$tmy);
                    }
                    $tmx = $tmx + $width;
                }
                $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',6);
                }
            }
        } 
        
        $tmy = $tmy + 10;
        $this->SetFont($this->font_name,'B',6);
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(10,$tmy,$line_len+10,$tmy);
        $tmy = $tmy + 5;
        $this->MultiCell(20,3,"Total :",0,'L',0,1,10,$tmy);
        $tmx = 10;
        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $total)){
                $this->MultiCell($li['width'],3,$this->num_format($total[$li['param']]),0,'R',0,1,$tmx,$tmy);

            }
            $tmx = $tmx + $li['width'];
        }
        $this->SetFont($this->font_name,'',6);
    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }
}