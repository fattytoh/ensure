<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;

class OutstandingPDF extends BasePDF {
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        $this->SetFont('helvetica','B',12);
        $this->Cell(100,10, "OUTSTANDING PAYMENT DUE BY INSURED LISTING",0,0,'L');
        $this->SetFont('helvetica','',9);
        $this->Ln(10);
        $this->Cell(100,3, "Period : ".strtoupper(date('d-M-Y',strtotime($this->period_from)))." - ".strtoupper(date('d-M-Y',strtotime($this->period_to))) ,0,0,'L');
        $this->Ln(3);
        $this->Cell(100,3, "Filter By : ".$this->insurance_type_mapping[$this->filter_by] ,0,0,'L');
        $this->Ln(5);
        $title_height = $this->GetY();
        if(file_exists($this->cprofile_logo_url)){
            $this->SetY(2);
            $tmy = $iniy = $this->GetY();
            $new_height = 0;
            $new_width = 20;
            list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
            $width_scalling_per = (($new_width / $img_width) * 100);
            $new_height = ($img_height/100) * $width_scalling_per;
            $this->Image($this->cprofile_logo_url, 260, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $logo_height = $this->GetY() + $new_height;
        }
        
        if($logo_height > $title_height){
            $this->line(10,$logo_height + 5,285,$logo_height + 5);
            $this->header_title_height = $logo_height+10 ;
        }
        else{
            $this->line(10,$title_height + 5,285,$title_height + 5);
            $this->header_title_height = $title_height+10 ;
        }
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $number_addtional = 0;
        $this->AddPage("L");
        $this->SetFont($this->font_name,'BU',6);
        $this->SetAutoPageBreak(true, 10);
        $this->SetY($this->header_title_height);
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        $maxlength = 270;
        $ini_height = 3;
        $land_height = 170;

        if(in_array($this->filter_by,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "No" => array('width'=>13.5),
                "Entry Date" =>array('width'=>21.6, 'param'=>'order_date'),
                "DN No" => array('width'=>25, 'param'=>'order_no'),
                "Policy Number" => array('width'=>23.6, 'param'=>'order_policyno'),
                "Vehicle Number" => array('width'=>21.6, 'param'=>'order_vehicles_no'),
                "Hirer Name" => array('width'=>35.1, 'param'=>'partner_name'),
                "Premium" => array('width'=>21.6, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Receivable Premium" => array('width'=>21.6, 'param'=>'order_receivable_premiumreceivable', 'number' => true, 'position'=>'R'),
                "Receivable Payment" => array('width'=>21.6, 'param'=>'paid_total', 'number' => true, 'position'=>'R'),
                "Payment O/S" => array('width'=>21.6, 'param'=>'order_outstanding', 'number' => true, 'position'=>'R'),
                "O/S day" => array('width'=>13.5, 'param'=>'order_outstanding_day','right' => true, 'position'=>'R'),
                "Sales Type" => array('width'=>13.5, 'param'=>'order_display_type','right' => true, 'position'=>'R'),
            );
        }
        else{
            $header = array(
                "No" => array('width'=>13.5),
                "Entry Date" =>array('width'=>21.6, 'param'=>'order_date'),
                "DN No" => array('width'=>25, 'param'=>'order_no'),
                "Policy Number" => array('width'=>23.6, 'param'=>'order_policyno'),
                "Coverage Plan" => array('width'=>21.6, 'param'=>'order_coverage'),
                "Hirer Name" => array('width'=>35.1, 'param'=>'partner_name'),
                "Premium" => array('width'=>21.6, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Receivable Premium" => array('width'=>21.6, 'param'=>'order_receivable_premiumreceivable', 'number' => true, 'position'=>'R'),
                "Receivable Payment" => array('width'=>21.6, 'param'=>'paid_total', 'number' => true, 'position'=>'R'),
                "Payment O/S" => array('width'=>21.6, 'param'=>'order_outstanding', 'number' => true, 'position'=>'R'),
                "O/S day" => array('width'=>13.5, 'param'=>'order_outstanding_day','right' => true, 'position'=>'R'),
                "Sales Type" => array('width'=>13.5, 'param'=>'order_display_type','right' => true, 'position'=>'R'),
            );
        }
        
        $this->total_display= array(
            'premium' => array('sum'=>0 ,'width'=> 21.6),
            'rec_premium' => array('sum'=>0 ,'width'=> 21.6),
            'rec_pay' => array('sum'=>0 ,'width'=> 21.6),
            'out' => array('sum'=>0 ,'width'=> 21.6),
            'outday' => array('sum'=>0 ,'width'=> 13.5),
        );
        $total_width = 0; 
        foreach($header as $ky => $li){
            $width = $li['width'];                        
            if(in_array($ky, array('No',"Entry Date","DN No","Policy Number","Coverage Plan","Hirer Name","Vehicle Number"))){
                $total_width = $total_width + $width;
            }
        }
        
        $tmx = 15;
        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        $this->SetFont($this->font_name,'',6);
        $tmy = $tmy + $ini_height;
        $total = 0;
        if($this->dealer_id > 0 && $this->cus_id > 0){
            $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
            if($this->isAddPage){
                $this->SetFont($this->font_name,'BU',6);
                $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
                $this->SetFont($this->font_name,'',6);
            }
            $this->SetFont($this->font_name,'BUI',6);
            $dealer_data = $this->getDealerInfoByID($this->dealer_id);
            $partner_data = $this->getPartnerInfoByID($this->cus_id);
            $this->MultiCell($maxlength,5,"TSA :".$dealer_data['dealer_name']." & Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,20,$tmy);
            $this->SetFont($this->font_name,'',6);
        }
        else{
            if($this->dealer_id > 0){
                $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $this->SetFont($this->font_name,'BUI',6);
                $dealer_data = $this->getDealerInfoByID($this->dealer_id);
                $this->MultiCell($maxlength,5,"TSA :".htmlspecialchars_decode($dealer_data['dealer_name']),0,'L',0,1,20,$tmy);
                $this->SetFont($this->font_name,'',6);
            }
            elseif($this->cus_id > 0){
                $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',6);
                    $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
                    $this->SetFont($this->font_name,'',6);
                }
                $this->SetFont($this->font_name,'BUI',6);
                $partner_data = $this->getPartnerInfoByID($this->cus_id);
                $this->MultiCell($maxlength,5,"Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,20,$tmy);
                $this->SetFont($this->font_name,'',6);
            }
        }

        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',6);
            $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
            $this->SetFont($this->font_name,'',6);
        }
        
        // foreach($this->data_listing as $li_ky => $li_data){
        //     $tmx = 15;
        //     $sub_total['premium'] =0; 
        //     $sub_total['rec_premium'] =0;
        //     $sub_total['rec_pay'] =0;
        //     $sub_total['out'] =0;
        //     $sub_total['outday'] =0;
        //     if($this->group_by){
        //         if($this->group_by == 'customer' && !$this->cus_id ){
        //             $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
        //             if($this->isAddPage){
        //                 $this->SetFont($this->font_name,'BU',6);
        //                 $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        //                 $this->SetFont($this->font_name,'',6);
        //             }
        //             $this->SetFont($this->font_name,'BUI',6);
        //             $partner_data = $this->getPartnerInfoByID($li_ky);
        //             $this->MultiCell($maxlength,5,"Customer :".htmlspecialchars_decode($partner_data['partner_name']),0,'L',0,1,$tmx + 5,$tmy);
        //             $this->SetFont($this->font_name,'',6);
        //         }
        //         elseif($this->group_by == 'dealer' && !$this->dealer_id){
        //             $tmy = $this->checkYaxisoverflow($tmy+5, $land_height);
        //             if($this->isAddPage){
        //                 $this->SetFont($this->font_name,'BU',6);
        //                 $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        //                 $this->SetFont($this->font_name,'',6);
        //             }
        //             $this->SetFont($this->font_name,'BUI',6);
        //             $dealer_data = $this->getDealerInfoByID($li_ky);
        //             $this->MultiCell($maxlength,5,"TSA :".$dealer_data['dealer_name'],0,'L',0,1,$tmx + 5,$tmy);
        //             $this->SetFont($this->font_name,'',6);
        //         }
        //         $tmy = $this->GetY()+3;
        //         foreach($li_data as $item_key => $item_dis){
        //             $sub_total['premium'] += $item_dis['order_gross_premium']; 
        //             $sub_total['rec_premium'] += $item_dis['order_receivable_premiumreceivable']; 
        //             $sub_total['rec_pay'] += $item_dis['paid_total']; 
        //             $sub_total['out'] += $item_dis['order_outstanding']; 
        //             $sub_total['outday'] += $item_dis['order_outstanding_day']; 
        //             $this->total_display['premium']['sum'] += $item_dis['order_gross_premium']; 
        //             $this->total_display['rec_premium']['sum'] += $item_dis['order_receivable_premiumreceivable']; 
        //             $this->total_display['rec_pay']['sum'] += $item_dis['paid_total']; 
        //             $this->total_display['out']['sum'] += $item_dis['order_outstanding']; 
        //             $this->total_display['outday']['sum'] += $item_dis['order_outstanding_day']; 
        //             $tmx = 15;
        //             $ini_height =3;
        //             foreach($header as $ky => $li){
        //                 $width = $li['width'];
        //                 $ini_height = $this->checkMulticellHeight($item_dis[$li['param']], $width, $ini_height);
        //             }
                    
        //             $tmy = $this->checkYaxisoverflow($tmy, $land_height);
        //             if($this->isAddPage){
        //                 $this->SetFont($this->font_name,'BU',6);
        //                 $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        //                 $this->SetFont($this->font_name,'',6);
        //             }
        //             if(($item_key + 1) % 2 != 0){
        //                 $this->Rect($tmx, $tmy, $maxlength-$tmx, $ini_height,'DF',$list_border_style,$list_react_color_style);
        //             }
        //             foreach($header as $ky => $li){
        //                 $width = $li['width'];
        //                 if(isset($li['param'])){
        //                     if(isset($li['number'])){
        //                         if($item_dis[$li['param']] == 0){
        //                             $this->MultiCell($width,$ini_height,"",0,'R',0,1,$tmx,$tmy);
        //                         }
        //                         else{
        //                             $this->MultiCell($width,$ini_height,"$".$this->num_format($item_dis[$li['param']]),0,'R',0,1,$tmx,$tmy);
        //                         }
        //                         $total = $total + $item_dis[$li['param']];
        //                     }
        //                     elseif(isset($li['right'])){
        //                         $this->MultiCell($width,$ini_height,$item_dis[$li['param']],0,'R',0,1,$tmx,$tmy);
        //                     }
        //                     else{
        //                         $this->MultiCell($width,$ini_height,$item_dis[$li['param']],0,'L',0,1,$tmx,$tmy);
        //                     }
        //                 }
        //                 else{
        //                     $this->MultiCell($width,$ini_height,$item_key+1,0,'L',0,1,$tmx,$tmy);
        //                 }
        //                 $tmx = $tmx + $width;
        //             }
        //             $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        //             if($this->isAddPage){
        //                 $this->SetFont($this->font_name,'BU',6);
        //                 $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        //                 $this->SetFont($this->font_name,'',6);
        //             }
        //         }
                
        //         $tmy = $this->checkYaxisoverflow($this->GetY() + 3 , $land_height);
        //         if($this->isAddPage){
        //             $this->SetFont($this->font_name,'BU',6);
        //             $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        //             $this->SetFont($this->font_name,'',6);
        //         }
        //         $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        //         $this->line(15,$tmy,$maxlength,$tmy);
        //         $tmy = $this->checkYaxisoverflow($this->GetY() + 3 , $land_height);
        //         if($this->isAddPage){
        //             $this->SetFont($this->font_name,'BU',6);
        //             $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        //             $this->SetFont($this->font_name,'',6);
        //         }
        //         $st_widith = $total_width + 15;
        //         $this->MultiCell(20,3,"Sub Total :",0,'L',0,1,15,$tmy);
        //         foreach($this->total_display as $total_ky => $total_li){
        //             $width = $total_li['width'];
        //             if($total_ky == 'outday'){
        //                 $this->MultiCell($width,3,'',0,'R',0,1,$st_widith,$tmy);
        //             }
        //             else{
        //                 if($sub_total[$total_ky] == 0){
        //                     $this->MultiCell($width,3,"",0,'R',0,1,$st_widith,$tmy);
        //                 }
        //                 else{
        //                     $this->MultiCell($width,3,"$".$this->num_format($sub_total[$total_ky]),0,'R',0,1,$st_widith,$tmy);
        //                 }
        //             }
        //             $st_widith += $width;
        //         }
                
        //     }
        //     else{
        //         $ini_height =3;
        //         foreach($header as $ky => $li){
        //             $width = $li['width'];
        //             $ini_height = $this->checkMulticellHeight($li_data[$li['param']], $width, $ini_height);
        //         }
        //         $tmy = $this->checkYaxisoverflow($tmy, $land_height);
        //         if($this->isAddPage){
        //             $this->SetFont($this->font_name,'BU',6);
        //             $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
        //             $this->SetFont($this->font_name,'',6);
        //         }
        //         if(($li_ky + 1) % 2 != 0){
        //             $this->Rect($tmx, $tmy, $maxlength-$tmx, $ini_height,'DF',$list_border_style,$list_react_color_style);
        //         }
        //         $sub_total['premium'] += $li_data['order_gross_premium']; 
        //         $sub_total['rec_premium'] += $li_data['order_receivable_premiumreceivable']; 
        //         $sub_total['rec_pay'] += $li_data['paid_total']; 
        //         $sub_total['out'] += $li_data['order_outstanding']; 
        //         $sub_total['outday'] += $li_data['order_outstanding_day']; 
        //         $this->total_display['premium']['sum'] += $sub_total['premium'];
        //         $this->total_display['rec_premium']['sum'] += $sub_total['rec_premium'];
        //         $this->total_display['rec_pay']['sum'] += $sub_total['rec_pay'];
        //         $this->total_display['out']['sum'] += $sub_total['out'];
        //         $this->total_display['outday']['sum'] += $sub_total['outday'];
        //         foreach($header as $ky => $li){
        //             $width = $li['width'];
        //             if(isset($li['param'])){
        //                 if(isset($li['number'])){
        //                     if($li_data[$li['param']] == 0){
        //                          $this->MultiCell($width,$ini_height,"",0,'R',0,1,$tmx,$tmy);
        //                     }
        //                     else{
        //                         $this->MultiCell($width,$ini_height,"$".$this->num_format($li_data[$li['param']]),0,'R',0,1,$tmx,$tmy);
        //                     }
        //                     $total = $total + $li_data[$li['param']];
        //                 }
        //                 elseif(isset($li['right'])){
        //                     $this->MultiCell($width,$ini_height,$li_data[$li['param']],0,'R',0,1,$tmx,$tmy);
        //                 }
        //                 else{
        //                     $this->MultiCell($width,$ini_height,$li_data[$li['param']],0,'L',0,1,$tmx,$tmy);
        //                 }
        //             }
        //             else{
        //                 $this->MultiCell($width,$ini_height,$li_ky + 1,0,'L',0,1,$tmx,$tmy);
        //             }
        //             $tmx = $tmx + $width;
        //         }
        //         $tmy = $this->GetY();
        //     }
        // } 
        
        $tmy += 10;
        $tmy = $this->checkYaxisoverflow($tmy, $land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',6);
            $tmy = $this->generateColumnHeader($this->GetY(), 15, $header);
            $this->SetFont($this->font_name,'',6);
        }
        $this->SetFont($this->font_name,'B',6);
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(15,$tmy,$maxlength,$tmy);
        $tmy = $tmy + 5;
        $this->MultiCell(20,3,"Total :",0,'L',0,1,15,$tmy);
        $total_width += 15;
        foreach($this->total_display as $total_ky => $total_li){
            $width = $total_li['width'];
            if(in_array($total_ky, array('outday'))){
                $this->MultiCell($width,3,"",0,'R',0,1,$total_width,$tmy);
            }
            else{
                if($total_li['sum'] == 0){
                    $this->MultiCell($width,3,"",0,'R',0,1,$total_width,$tmy);
                }
                else{
                    $this->MultiCell($width,3,"$".$this->num_format($total_li['sum']),0,'R',0,1,$total_width,$tmy);
                }
            }
            $total_width += $width;
        }
        $this->SetFont($this->font_name,'',8);
    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }
}