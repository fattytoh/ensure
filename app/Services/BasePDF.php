<?php

namespace App\Services;
use App\Models\Customer;
use App\Models\TSA;
use App\Models\CProfile;
use App\Models\Insuranceco;

use TCPDF;

class BasePDF extends TCPDF {
    public $insurance_type_mapping = [
        "MI" => "MotorCycle Insurance",
        "PMI" => "Private Motor Insurance",
        "CMI" => "Commercial Motor Insurance",
        "GMM" => "Domestic Maid Insurance",
        "GI" => "General Insurance",
        "ALL" => "ALL Insurance",
    ];

    public $maid_coverage_mapping = [
        'Exclusive Plan' => 'E',
        'Deluxe Plan' => 'D',
        'Classic Plan' => 'C',
        '300MDC' => 'O3',
        '500MDC' => 'O5',
        '2K' => 'PHB2',
        '7K' => 'PHB7',
    ];

    public $cprofile_logo_url = '';
    public $cprofile_reg_no = '';
    public $cprofile_name = '';
    public $comp_name = '';
    public $footer_str = '';
    public $font_name = 'Arial';
    public $header_title_height = 0;
    public $land_height = 270;

    public function getFooterCompInfo(){
        if(env('APP_ID')=='2'){
            $this->comp_name = 'ENSURE PTE LTD';
            $this->footer_str = '8 TOH GUAN ROAD EAST, #01-57 ENTERPRISE HUB , SINGAPORE 608581 FAX: +65 6896 6321 TEL: +65 6515 5988 EMAIL: enquiry@ens.com.sg';
        }
        else{
            $this->comp_name = 'PEOPLES INSURANCE AGENCY PTE LTD';
            $this->footer_str = '38 TOH GUAN ROAD EAST, #03-57 ENTERPRISE HUB , SINGAPORE 608581 FAX: +65 6896 6321 TEL: +65 6515 5778 EMAIL: enquiry@peoples.com.sg';
        }
    }

    public function getLogoUrl()
    {
        $this->getCprofile();
        if(env('APP_ID')=='2'){
            $this->cprofile_logo_url =public_path('assets/images/ensure-logo.png');
        }
        else{
            $this->cprofile_logo_url =public_path('assets/images/people-logo.png');
        }
    }

    public function getCprofile(){
        $cprofile = CProfile::find(env('APP_ID'));
        $this->cprofile_name = $cprofile->cprofile_name;
        $this->cprofile_reg_no = $cprofile->cprofile_reg_no;
        $this->cprofile_full_address = $cprofile->cprofile_full_address;
        $this->cprofile_email = $cprofile->cprofile_email;
        $this->cprofile_fax = $cprofile->cprofile_fax;
        $this->cprofile_tel = $cprofile->cprofile_tel;
    }
    
    public function checkYaxisoverflow($tmy, $default_height = 270){
        $this->isAddPage = false;
        if($tmy < $this->header_title_height || $tmy > $default_height){
            $this->AddPage();
            $this->setY($this->header_title_height);
            $tmy = $this->header_title_height;
            $this->isAddPage = true;
        }
        return $tmy;
    }
    
    public function checkMulticellHeight($text,$width ,$height){
        $comp_hei = $this->getStringHeight($width,$text);
        if(round($comp_hei) >= $height){
            $height = round($comp_hei);
        }
        return $height;
    }

    public function generateColumnHeader($tmy, $tmx, $header){
        $ini_height = 3;
        foreach($header as $ky => $li){
            $ini_height = $this->checkMulticellHeight($ky,$li['width'], $ini_height);
        }

        foreach($header as $ky => $li){
            $width = $li['width'];
            if(isset($li['position']) && $li['position']){
                $postion = $li['position'];
            }
            else{
                $postion = 'L';
            }

            $this->MultiCell($width,$ini_height,$ky,0,$postion,0,1,$tmx,$tmy);
            
            $tmx = $tmx + $width;
        }
        
        return $ini_height + $tmy;
    }

    public function numberFormat($number, $type = "DN"){
        if($type == 'CN'){
            $number = "($".number_format($number, 2, ".", ",").")";
        }
        else{
            $number = "$".number_format($number, 2, ".", ",");
        }
        return $number;
    }

    public function num_format($sIn){
        return number_format($sIn, 2, ".", ",");
    }

    public function numberTotalFormat($number){
        if($number < 0 ){
            $number = "($".number_format(abs($number), 2, ".", ",").")";
        }
        else{
            $number = number_format($number, 2, ".", ",");
        }
        return $number;
    }

    public function getDealerInfoByID($tsa_id){
        $dealer_info = TSA::select('dealer_name', 'dealer_blk_no', 'dealer_street', 'dealer_unit_no', 'dealer_postalcode')
        ->where('dealer_id', $tsa_id)
        ->first()->toArray();
        return $dealer_info;
    }
    
    public function getPartnerInfoByID($cus_id){
        $cus_info = Customer::select('partner_name', 'partner_blk_no', 'partner_street', 'partner_unit_no', 'partner_postalcode')
        ->where('partner_id', $cus_id)
        ->first()->toArray();
        return $cus_info;
    }

    public function getInsuranceComapnyInfoByID($ins_id){
        $ins_info = Insuranceco::select('insuranceco_name', 'insuranceco_address')
        ->where('insuranceco_id', $ins_id)
        ->first()->toArray();
        return $ins_info;
    }
}