<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;
use Carbon\Carbon;
use App\Models\Receipt;
use App\Models\ReceiptDetail;
use App\Models\Payment;
use App\Models\PaymentDetail;

class MaidAccountStatement extends BasePDF {
    public $title_font_size = '10';
    public $content_font_size = '8';
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        if(in_array($this->filter_by, array('MI','CMI','PMI', 'GI', 'PCMI'))){
            if(file_exists($this->cprofile_logo_url)){
                $this->SetY(5);
                $tmy = $iniy = $this->GetY();
                $new_height = 0;
                $new_width = 20;
                list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                $width_scalling_per = (($new_width / $img_width) * 100);
                $new_height = ($img_height/100) * $width_scalling_per;
                $this->Image($this->cprofile_logo_url, 180, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                $this->SetY($tmy + $new_height + 5);
            }
            else{
                $this->SetY(5);
            }
            $this->SetFont('helvetica','',8);
            $this->Cell(100,5, "Date : ".strtoupper(date('d-m-Y',strtotime(Carbon::now()))),0,0,'L');

            $this->Ln(8);
            $address_ori_height = $this->GetY();
            if($this->cus_id > 0){
                $target_data = $this->getPartnerInfoByID($this->cus_id);
                $this->SetFont('helvetica','B',9);
                $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['partner_name'])),0,0,'L');
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
                if($target_data['partner_blk_no'] || $target_data['partner_street']){
                    $this->Cell(50,3, strtoupper($target_data['partner_blk_no']." ".$target_data['partner_street']),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['partner_unit_no']){
                    $this->Cell(50,3, strtoupper($target_data['partner_unit_no']),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['partner_postal_code']){
                    $this->Cell(50,5, strtoupper("SINGAPORE ".$target_data['partner_postal_code']),0,0,'L');
                    $this->Ln(3);
                }
            }
            elseif($this->dealer_id > 0){
                $target_data = $this->getPartnerInfoByID($this->dealer_id);
                $this->SetFont('helvetica','B',9);
                $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['dealer_name'])),0,0,'L');
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
                if($target_data['dealer_blk_no'] || $target_data['dealer_street']){
                    $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_blk_no']." ".$target_data['dealer_street'])),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['dealer_unit_no']){
                    $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_unit_no'])),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['dealer_postalcode']){
                    $this->Cell(50,3, strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['dealer_postalcode'])),0,0,'L');
                    $this->Ln(3);
                }
            }
            else{
                $this->SetFont('helvetica','',8);
            }
            $tmy = $address_ori_height;
            $this->SetFont('helvetica','B',8);
            $this->MultiCell(60,3,strtoupper($this->soa_no),0,'L',0,1,140,$tmy + 1);
            $this->MultiCell(60,3,strtoupper($this->cprofile_name),0,'L',0,1,140,$this->GetY());
            $this->MultiCell(60,3,strtoupper("GST Reg No.".$this->cprofile_reg_no),0,'L',0,1,140,$this->GetY());
            $this->Ln(5);
            $this->SetFont('helvetica','B',8);
            $this->Cell(150,3, strtoupper("RE : Statement Of Account - Full Listing ".date('d-M-Y', strtotime($this->period_from))." To ".date('d-M-Y', strtotime($this->period_to))),0,0,'L');
            $this->SetFont('helvetica','',8);
            $this->Ln(5);
            $this->header_title_height = $this->GetY();
        }
        elseif(in_array($this->filter_by, array('GMM'))){
            if(file_exists($this->cprofile_logo_url)){
                $this->SetY(5);
                $tmy = $iniy = $this->GetY();
                $new_height = 0;
                if(env('APP_ID') == 3){
                    $new_width = 50;
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = (($new_width / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 10, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                }
                else{
                    $new_width = 20;
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = (($new_width / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 180, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                }
                $this->SetY($tmy + $new_height + 5);
            }
            else{
                $this->SetY(5);
            }

            if(env('APP_ID') == 3){
                $this->SetFont('helvetica','B',9);
                $this->MultiCell(65,5,strtoupper($this->cprofile_name ),0,'L',0,1,35,20);
                $this->SetFont('helvetica','',7);
                $this->MultiCell(65,3,strtoupper("Company Reg No.".$this->cprofile_reg_no),0,'L',0,1,33,25);
                $this->MultiCell(100,3,strtoupper($this->cprofile_full_address),0,'L',0,1,30,28);
                $this->MultiCell(150,3,strtoupper("TEL:".$this->cprofile_tel." FAX:".$this->cprofile_fax." EMAIL:".$this->cprofile_email),0,'L',0,1,25,31);
                $this->Ln(8);
                                    
                $this->header_title_height = $this->GetY();
                $this->SetMargins(15,  $this->header_title_height, 15);
                
                if(file_exists($this->cprofile_logo_url)){
                    $this->SetAlpha(0.15);
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = ((160 / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 25, 95, 160, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                    $this->SetAlpha(1);
                }
            }
            else{
                if(file_exists($this->cprofile_logo_url)){
                    $this->SetY(5);
                    $tmy = $iniy = $this->GetY();
                    $new_height = 0;
                    $new_width = 20;
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = (($new_width / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 180, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                    $this->SetY($tmy + $new_height + 5);
                }
                else{
                    $this->SetY(5);
                }
                $this->SetFont('helvetica','',8);
                $this->Cell(100,5, "Date : ".strtoupper(date('d-m-Y',strtotime(system_date))),0,0,'L');

                $this->Ln(8);
                if($this->cus_id > 0){
                    $target_data = $this->getPartnerInfoByID($this->cus_id);
                    $this->SetFont('helvetica','B',9);
                    $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['partner_name'])),0,0,'L');
                    $this->SetFont('helvetica','',8);
                    $this->Ln(6);
                    if($target_data['partner_blk_no'] || $target_data['partner_street']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['partner_blk_no']." ".$target_data['partner_street'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['partner_unit_no']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['partner_unit_no'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['partner_postal_code']){
                        $this->Cell(50,5, strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['partner_postal_code'])),0,0,'L');
                        $this->Ln(3);
                    }
                }
                elseif($this->dealer_id > 0){
                    $target_data = $this->getPartnerInfoByID($this->dealer_id);
                    $this->SetFont('helvetica','B',9);
                    $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['dealer_name'])),0,0,'L');
                    $this->SetFont('helvetica','',8);
                    $this->Ln(6);
                    if($target_data['dealer_blk_no'] || $target_data['dealer_street']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_blk_no']." ".$target_data['dealer_street'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['dealer_unit_no']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_unit_no'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['dealer_postalcode']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['dealer_postalcode'])),0,0,'L');
                        $this->Ln(3);
                    }
                }
                else{
                    $this->SetFont('helvetica','',8);
                }
                $this->Ln(5);
                $this->SetFont('helvetica','B',8);
                $this->Cell(150,3, strtoupper(htmlspecialchars_decode("RE : Statement Of Account - Full Listing ".date('d-M-Y', strtotime($this->period_from))." To ".date('d-M-Y', strtotime($this->period_to)))),0,0,'L');
                $this->SetFont('helvetica','',8);
                $this->Ln(5);
                $this->header_title_height = $this->GetY();
                $this->SetMargins(15,  $this->header_title_height, 15);
            }
            
            if(file_exists($this->cprofile_logo_url)){
                $this->SetAlpha(0.15);
                list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                $width_scalling_per = ((160 / $img_width) * 100);
                $new_height = ($img_height/100) * $width_scalling_per;
                $this->Image($this->cprofile_logo_url, 25, 95, 160, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                $this->SetAlpha(1);
            }
        }
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $this->AddPage();
        $this->SetFont($this->font_name,'BU',$this->title_font_size);
        $this->SetAutoPageBreak(true, 10);
        $this->SetY($this->header_title_height);
        $this->SetAddress();
        $this->SetFont('helvetica','B',$this->title_font_size);
        $this->Cell(150,5, strtoupper("RE : Statement Of Account - Full Listing ".date('d-M-Y', strtotime($this->period_from))." To ".date('d-M-Y', strtotime($this->period_to))),0,0,'L');
        $this->SetFont('helvetica','BU',$this->content_font_size);
        $this->Ln(5);
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        $maxlength = 190;
        $ini_height = 3;
        $this->land_height = 270;

        if($this->cus_id > 0){
            $total_item = array(
                'Policy Premium' => 'order_gross_premium',
                'Payment Paid' => 'paid_total',
                'Policy Refund To Customer' => 'total_refund',
                'Total Amount Due'=> 'order_cus_pay',
            );
        }
        else{
            $total_item = array(
                'Policy Premium' => 'order_gross_premium',
                'Policy Referral Fees' => 'order_receivable_dealercomm',
                'Payment Paid' => 'paid_total',
                'Policy Refund To TSA' => 'tsa_refund',
                'Referral Fees Claw Back' => 'comm_cawl_back',
                'Total Amount Due' => 'order_tsa_pay',
            ); 
        }

        $grand_total = array(
            'order_gross_premium' => 0,
            'paid_total' => 0,
            'order_receivable_dealercomm' => 0,
            'total_refund' => 0,
            'comm_cawl_back' => 0,
            'order_tsa_pay' => 0,
            'order_cus_pay' => 0,
            'tsa_refund' => 0,
            'cus_refund' => 0,
        );

        $dn_header = array(
            'No' => array('width' => 10),
            'Acceptance Date' => array('width' => 18, "param" => 'order_date'),
            'Commence Date' => array('width' => 18 , "param" => 'order_period_from'),
            'Policy No' => array('width' => 20 , "param" => 'order_policyno'),
            'Name of applicant / Insured Name' => array('width' => 25 , "param" => 'partner_name'),
            'Maid Name / Insured Person Name' => array('width' => 25 , "param" => 'order_maid_name'),
            'Plan' => array('width' => 20 , "param" => 'order_coverage'),
            'Premium After GST' => array('width' => 20 , "param" => 'order_gross_premium', "number" => true),
            'TSA Referral Fee' => array('width' => 20 , "param" => 'order_receivable_dealercomm', "number" => true),
            'Due Payment' => array('width' => 20, "param" => 'order_tsa_pay', "number" => true),
        );
        
        $dn_subtotal = array(
            'order_gross_premium' => 0,
            'order_receivable_dealercomm' => 0,
            'order_tsa_pay' => 0,
            'order_cus_pay' => 0,
        );

        $addtional = 0;
        $dn_width =196;
        $dn_height =3;

        if($this->cus_id > 0){
            $dn_width =176;
            $dn_header['Due Payment']['param'] = 'order_cus_pay';
            unset($dn_header['TSA Referral Fee']);
            unset($dn_subtotal['order_tsa_pay']);
            unset($dn_subtotal['order_receivable_dealercomm']);
        }

        $tmx = 10;
        foreach($dn_header as $ky => $li){
            $dn_height = $this->checkMulticellHeight($ky, $li['width'], $dn_height);
        }
        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->Rect(10, $this->GetY(), $dn_width, $dn_height,'DF',$list_border_style,$list_react_color_style);
        $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));

        foreach($dn_header as $ky => $li){
            $this->MultiCell($li['width'],$dn_height,$ky,"B",'L',0,1,$tmx,$tmy,true,0,true);
            $tmx = $tmx + $li['width'];
        }

        $count = 1;
        $this->SetFont('helvetica','',$this->content_font_size);

        foreach($this->data_listing['DN'] as $li_ky => $dn_row){
            $tmy = $this->checkYaxisoverflow($this->GetY() + 1, $this->land_height);
            if($this->isAddPage){
                $tmy = $this->SetAddress();
                $this->SetFont($this->font_name,'BU',$this->content_font_size);
                $this->Rect(10, $this->GetY(), $dn_width, $dn_height,'DF',$list_border_style,$list_react_color_style);
                $tmy = $this->generateColumnHeader($tmy, 10, $dn_header) + 3;
                $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
                $this->SetFont($this->font_name,'',$this->content_font_size);
            }

            $tmx = 10;
            if($dn_row['next_line']){
                $ini_height =7;
            }
            else{
                $ini_height =3;
            }

            foreach($dn_header as $ky => $li){
                if(isset($li['param'])){
                    $ini_height = $this->checkMulticellHeight($dn_row[$li['param']], $li['width'], $ini_height);
                }
            }

            $ini_height += 2;

            foreach($dn_subtotal as $sub_key  => $sub_item){
                $dn_subtotal[$sub_key] = $dn_subtotal[$sub_key] + $dn_row[$sub_key];
                $grand_total[$sub_key] = $grand_total[$sub_key] + $dn_row[$sub_key];
            }

            foreach($dn_header as $hky => $hli){
                if(isset($hli['number']) && $hli['number']){
                    if(isset($hli['param']) && in_array($hli['param'], array("order_receivable_dealercomm"))){
                        if($dn_row[$hli['param']] < 0){
                            $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format(abs($dn_row[$hli['param']])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                        else{
                            $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($dn_row[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                    }
                    else{
                        $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format($dn_row[$hli['param']]),"B",'R',0,1,$tmx,$tmy,true,0,true);
                    }
                }
                elseif(isset($hli['param'])){
                    $this->MultiCell($hli['width'],$ini_height,$dn_row[$hli['param']],"B",'L',0,1,$tmx,$tmy,true,0,true);
                }
                else{
                    $this->MultiCell($hli['width'],$ini_height,$count,"B",'L',0,1,$tmx,$tmy,true,0,true);
                }
                $tmx = $tmx + $hli['width'];
            }

            $count++;
            $receipt_data = $this->getReceiptData($dn_row['order_id']);

            foreach($receipt_data as $rec_li){
                $tmx = 10;
                $this->Ln(2);
                $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height) +2;
                if($this->isAddPage){
                    $tmy = $this->SetAddress();
                    $this->SetFont($this->font_name,'BU',$this->content_font_size);
                    $this->Rect(10, $this->GetY(), $dn_width, $dn_height,'DF',$list_border_style,$list_react_color_style);
                    $tmy = $this->generateColumnHeader($tmy, 10, $dn_header) +2;
                    $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
                    $this->SetFont($this->font_name,'',$this->content_font_size);
                }
                $grand_total['paid_total'] += $rec_li['paid_total'];
                $rec_string = "Receipt:".date("d-m-Y",strtotime($rec_li['receipt_date'])). " ". $rec_li['receipt_cheque'];
                $ini_height = $this->checkMulticellHeight($rec_string, 140, 3) + 2;
                foreach($dn_header as $hky => $hli){
                    if($hli['param'] == 'order_date'){
                        $this->MultiCell($hli['width'],$ini_height,'',"B",'L',0,1,$tmx,$tmy,true,0,true);
                        $this->MultiCell(140,$ini_height,$rec_string,"",'L',0,1,$tmx,$tmy,true,0,true);
                    }
                    elseif($hli['param'] == 'order_tsa_pay' || $hli['param'] == 'order_cus_pay'){
                        if($rec_li['paid_total'] > 0){
                            $this->MultiCell($hli['width'],$ini_height,"(".$this->num_format(abs($rec_li['paid_total'])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                        else{
                            $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format(abs($rec_li['paid_total'])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                    }
                    else{
                        $this->MultiCell($hli['width'],$ini_height,'',"B",'L',0,1,$tmx,$tmy,true,0,true);
                    }
                    $tmx = $tmx + $hli['width'];
                }
            }
        }

        $tmx = 10;
        $tmy = $this->checkYaxisoverflow($this->GetY() + 2, $this->land_height +7);
        if($this->isAddPage){
            $tmy = $this->SetAddress();
            $this->SetFont($this->font_name,'BU',$this->content_font_size);
            $this->Rect(10, $this->GetY(), $dn_width, $dn_height,'DF',$list_border_style,$list_react_color_style);
            $tmy = $this->generateColumnHeader($tmy, 10, $dn_header) + 3;
            $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
            $this->SetFont($this->font_name,'',$this->content_font_size);
        }
        $this->SetFont($this->font_name,'B',$this->content_font_size);
        foreach($dn_header as $hky => $hli){
            if(isset($hli['param']) && $hli['param'] == 'order_coverage'){
                $this->MultiCell($hli['width'],5,"Sub Total",0,'L',0,1,$tmx,$tmy,true,0,true);
            }
            elseif(isset($hli['param']) && array_key_exists($hli['param'], $dn_subtotal)){
                if(in_array($hli['param'], array("order_receivable_dealercomm"))){
                    if($child_li[$hli['param']] < 0){
                        $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format(abs($dn_subtotal[$hli['param']])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                    }
                    else{
                        $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($dn_subtotal[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                    }
                }
                else{
                    $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format($dn_subtotal[$hli['param']]),"B",'R',0,1,$tmx,$tmy,true,0,true);
                }
            }
            $tmx += $hli['width'];
        }
        $this->SetFont($this->font_name,'',$this->content_font_size);

        //credit note
        if($this->dealer_id > 0){
            $cn_header = array(
                'No' => array('width' => 10),
                'Discharge Date' => array('width' => 17, "param" => 'order_discharge_date'),
                'Commence Date' => array('width' => 18 , "param" => 'order_period_from'),
                'Policy No' => array('width' => 20 , "param" => 'order_policyno'),
                'Name of applicant / Insured Name' => array('width' => 25 , "param" => 'partner_name'),
                'Maid Name / Insured Person Name' => array('width' => 25 , "param" => 'order_maid_name'),
                'Cancelation Rate' => array('width' => 18 , "param" => 'order_cancel_rate'),
                'Refund' => array('width' => 20 , "param" => 'total_refund', "number" => true),
                'Referral Fee crawl back' => array('width' => 20 , "param" => 'comm_cawl_back', "number" => true),
                'Refund after crawl back' => array('width' => 20, "param" => 'tsa_refund', "number" => true),
            );
            $this->Ln(5);
            $addtional = 0;
            $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height - 20);
            $this->SetFont('helvetica','B', $this->title_font_size);
            $this->MultiCell(150,5,strtoupper("CANCELLATION : Refund to TSA"),"",'L',0,1,10,$tmy);
            $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);

            $cn_height =12;
            $cn_width =193;

            $this->Rect(10, $tmy, $cn_width, $cn_height,'DF',$list_border_style,$list_react_color_style);
            $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
            $this->SetFont('helvetica','BU',$this->content_font_size);
            $tmx = 10;
            foreach($cn_header as $ky => $li){
                $this->MultiCell($li['width'],$cn_height,$ky,"B",'L',0,1,$tmx,$tmy,true,0,true);
                $tmx = $tmx + $li['width'];
            }

            $this->SetFont('helvetica','',$this->content_font_size);
            
            $cn_subtotal = array(
                'total_refund' => 0,
                'comm_cawl_back' => 0,
                'tsa_refund' => 0,
            );
            $count = 1;

            foreach($this->data_listing['CN']['TSA'] as $cn_row){
                $ini_height = 3;
                $this->Ln(2);
                $tmx = 10;
                $tmy = $this->checkYaxisoverflow($this->GetY() + 2, $this->land_height);

                if($this->isAddPage){
                    $tmy = $this->SetAddress();
                    $this->SetFont($this->font_name,'BU',$this->content_font_size);
                    $this->Rect(10, $this->GetY(), $cn_width, $cn_height,'DF',$list_border_style,$list_react_color_style);
                    $tmy = $this->generateColumnHeader($tmy, 10, $cn_header) + 3;
                    $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
                    $this->SetFont($this->font_name,'',$this->content_font_size);
                }

                $tmx = 10;
                foreach($cn_header as $hky => $hli){
                    if(isset($hli['param']) && $hli['param']){
                        $ini_height = $this->checkMulticellHeight($cn_row[$hli['param']], $hli['width'], $ini_height);
                    }
                }
                $ini_height += 2;
                foreach($cn_header as $hky => $hli){
                    if(isset($hli['number'])){
                        if(in_array($hli['param'], array("comm_cawl_back"))){
                            if($cn_row[$hli['param']] > 0){
                                $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($cn_row[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                            }
                            else{
                                $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format(abs($cn_row[$hli['param']])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                            }
                        }
                        else{
                            $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($cn_row[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                    }
                    elseif(isset($hli['param'])){
                        $this->MultiCell($hli['width'],$ini_height,$cn_row[$hli['param']],"B",'L',0,1,$tmx,$tmy,true,0,true);
                    }
                    else{
                        $this->MultiCell($hli['width'],$ini_height,$count,"B",'L',0,1,$tmx,$tmy,true,0,true);
                        $count++;
                    }
                    $tmx = $tmx + $hli['width'];
                }

                $this->Ln(2);
            }

            if($this->isAddPage){
                $tmy = $this->SetAddress();
                $this->SetFont($this->font_name,'BU',$this->content_font_size);
                $this->Rect(10, $this->GetY(), $cn_width, $cn_height,'DF',$list_border_style,$list_react_color_style);
                $tmy = $this->generateColumnHeader($tmy, 10, $cn_header) + 3;
                $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
                $this->SetFont($this->font_name,'',$this->content_font_size);
            }
            $tmx = 10;
            $this->SetFont($this->font_name,'B',$this->content_font_size);
            foreach($cn_header as $hky => $hli){
                if(isset($hli['param']) && $hli['param'] == 'order_cancel_rate'){
                    $this->MultiCell($hli['width'],5,"Sub Total",0,'L',0,1,$tmx,$tmy,true,0,true);
                }
                elseif(isset($hli['param']) && array_key_exists($hli['param'], $cn_subtotal)){
                    if(in_array($hli['param'], array("comm_cawl_back"))){
                        if($cn_subtotal[$hli['param']] > 0){
                            $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($cn_subtotal[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                        else{
                            $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format(abs($cn_subtotal[$hli['param']])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                    }
                    else{
                        $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($cn_subtotal[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                    }
                }
                $tmx += $hli['width'];
            }
            $this->Ln(2);

            // commission crawl back tsa when cancel rate 100%
            $th_header = array(
                'No' => array('width' => 10),
                'Discharge Date' => array('width' => 17, "param" => 'order_discharge_date'),
                'Commence Date' => array('width' => 18 , "param" => 'order_period_from'),
                'Policy No' => array('width' => 20 , "param" => 'order_policyno'),
                'Name of applicant / Insured Name' => array('width' => 35 , "param" => 'partner_name'),
                'Maid Name / Insured Person Name' => array('width' => 35 , "param" => 'order_maid_name'),
                'Cancelation Rate' => array('width' => 18 , "param" => 'order_cancel_rate'),
                'Referral Fee crawl back' => array('width' => 20 , "param" => 'comm_cawl_back', "number" => true),
            );

            $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height - 20);
            $this->SetFont('helvetica','B',$this->title_font_size);
            $this->MultiCell(150,5,strtoupper("CANCELLATION : Referral Fee crawl back From TSA Listing"),"",'L',0,1,10,$tmy);
            $tmx = 10;
            $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
            
            $third_width = 173;
            $third_height = 8;
            $this->Rect(10, $tmy, $third_width, $third_height,'DF',$list_border_style,$list_react_color_style);
            $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
            $this->SetFont('helvetica','BU',$this->content_font_size);
            foreach($th_header as $ky => $li){
                $this->MultiCell($li['width'] ,8,$ky,"B",'L',0,1,$tmx,$tmy,true,0,true);
                $tmx = $tmx + $li['width'];
            }
            $this->SetFont('helvetica','',$this->content_font_size);
            $th_subtotal = array(
                'comm_cawl_back' => 0,
            );
            $count = 1;

            foreach($this->data_listing['CN']['CUS'] as $th_row){
                if(isset($th_row['order_cancel_rate']) && $th_row['order_cancel_rate'] == 100){
                    foreach($th_subtotal as $sub_key  => $sub_item){
                        $th_subtotal[$sub_key] = $th_subtotal[$sub_key] + $th_row[$sub_key];
                        $grand_total[$sub_key] = $grand_total[$sub_key] + $th_row[$sub_key];
                    }
    
                    $ini_height = 3;
                    $this->Ln(2);
                    $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
    
                    if($this->isAddPage){
                        $tmy = $this->SetAddress();
                        $this->SetFont($this->font_name,'BU',$this->content_font_size);
                        $this->Rect(10, $this->GetY(), $third_width, $third_height,'DF',$list_border_style,$list_react_color_style);
                        $tmy = $this->generateColumnHeader($tmy, 10, $th_header) + 3;
                        $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
                        $this->SetFont($this->font_name,'',$this->content_font_size);
                    }
    
                    $tmx = 10;
                    foreach($th_header as $hky => $hli){
                        if(isset($hli['param'])){
                            $ini_height = $this->checkMulticellHeight($th_row[$hli['param']], $hli['width'], $ini_height);
                        }
                    }
                    $ini_height += 2;

                    foreach($th_header as $hky => $hli){
                        if(isset($hli['number'])){
                            if(isset($hli['param']) && in_array($hli['param'], array("comm_cawl_back"))){
                                if($th_row[$hli['param']] > 0){
                                    $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($th_row[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                                }
                                else{
                                    $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format(abs($th_row[$hli['param']])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                                }
                            }
                            else{
                                $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($th_row[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                            }
                        }
                        elseif(isset($hli['param'])){
                            $this->MultiCell($hli['width'],$ini_height,$th_row[$hli['param']],"B",'L',0,1,$tmx,$tmy,true,0,true);
                        }
                        else{
                            $this->MultiCell($hli['width'],$ini_height,$count,"B",'L',0,1,$tmx,$tmy,true,0,true);
                            $count++;
                        }
                        $tmx = $tmx + $hli['width'];
                    }
                }
            }

            $tmy = $this->checkYaxisoverflow($this->GetY() + 2, $this->land_height + 7);

            if($this->isAddPage){
                $tmy = $this->SetAddress();
                $this->SetFont($this->font_name,'BU',$this->content_font_size);
                $this->Rect(10, $this->GetY(), $third_width, $third_height,'DF',$list_border_style,$list_react_color_style);
                $tmy = $this->generateColumnHeader($tmy, 10, $th_header) + 3;
                $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
                $this->SetFont($this->font_name,'',$this->content_font_size);
            }
            $this->SetFont($this->font_name,'B',$this->content_font_size);
            $tmx = 10;
            foreach($th_header as $hky => $hli){
                if(isset($hli['param']) && $hli['param'] == 'order_cancel_rate'){
                    $this->MultiCell($hli['width'],5,"Sub Total",0,'L',0,1,$tmx,$tmy,true,0,true);
                }
                elseif(isset($hli['param']) && array_key_exists($hli['param'], $th_subtotal)){
                    if(in_array($hli['param'], array("comm_cawl_back"))){
                        if($th_subtotal[$hli['param']] > 0){
                           $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($th_subtotal[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                        else{
                            $this->MultiCell($hli['width'],$ini_height,"$".$this->num_format(abs($th_subtotal[$hli['param']])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                    }
                    else{
                        $this->MultiCell($hli['width'],$ini_height,"($".$this->num_format(abs($th_subtotal[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                    }
                }
                $tmx += $hli['width'];
            }
            $this->Ln(2);
        }

        // Refund to customer
        $fr_header = array(
            'No' => array('width' => 10),
            'Discharge Date' => array('width' => 17, "param" => 'order_discharge_date'),
            'Commence Date' => array('width' => 18 , "param" => 'order_period_from'),
            'Policy No' => array('width' => 20 , "param" => 'order_policyno'),
            'Name of applicant / Insured Name' => array('width' => 35 , "param" => 'partner_name'),
            'Maid Name / Insured Person Name' => array('width' => 35 , "param" => 'order_maid_name'),
            'Cancelation Rate' => array('width' => 18 , "param" => 'order_cancel_rate'),
            'Refund' => array('width' => 20 , "param" => 'total_refund', "number" => true),
        );
        
        $fr_subtotal = array(
            'total_refund' => 0,
            'cus_refund' => 0,
        );

        $fr_height = 8;
        $fr_width = 173;

        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height - 20);
        $this->SetFont('helvetica','B',$this->title_font_size);
        $this->MultiCell(150,5,strtoupper("CANCELLATION : Refundable To Employer Directly Listing"),"",'L',0,1,10,$tmy);
        $this->SetFont('helvetica','BU',$this->content_font_size);
        $this->Rect(10, $this->GetY(), $fr_width, $fr_height,'DF',$list_border_style,$list_react_color_style);
        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $tmy = $this->generateColumnHeader($tmy, 10, $fr_header) + 3;
        $this->SetY($tmy);
        $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
        $this->SetFont('helvetica','',$this->content_font_size);
        $count = 1;

        $ini_height = 3;
        foreach($this->data_listing['CN']['CUS'] as $fr_row){
            $tmy = $this->checkYaxisoverflow($this->GetY() + 2, $this->land_height);
            if($this->isAddPage){
                $tmy = $this->SetAddress();
                $this->SetFont($this->font_name,'BU',$this->content_font_size);
                $this->Rect(10, $this->GetY(), $fr_width, $fr_height,'DF',$list_border_style,$list_react_color_style);
                $tmy = $this->generateColumnHeader($tmy, 10, $fr_header) + 3;
                $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
                $this->SetFont($this->font_name,'',$this->content_font_size);
            }
            $tmx = 10;

            foreach($fr_header as $hky => $hli){
                if(isset($hli['param']) && array_key_exists($hli['param'], $fr_subtotal)){
                    $fr_subtotal[$hli['param']] +=  abs($fr_row[$hli['param']]) * -1;
                    $grand_total[$hli['param']] +=  abs($fr_row[$hli['param']]) * -1;
                }
                if(isset($hli['param'])){
                    $ini_height = $this->checkMulticellHeight($fr_row[$hli['param']], $hli['width'], $ini_height);
                }
            }

            $ini_height += 2;

            foreach($fr_header as $hky => $hli){
                if(isset($hli['number'])){
                    if(isset($hli['param']) && in_array($hli['param'], array("comm_cawl_back"))){
                        if($fr_row[$hli['param']] < 0){
                            $this->MultiCell($hli['width'],$ini_height,"($".num_format(abs($fr_row[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                        else{
                            $this->MultiCell($hli['width'],$ini_height,"$".num_format(abs($fr_row[$hli['param']])),"B",'R',0,1,$tmx,$tmy,true,0,true);
                        }
                    }
                    else{
                        $this->MultiCell($hli['width'],$ini_height,"($".num_format(abs($fr_row[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
                    }
                }
                elseif(isset($hli['param'])){
                    $this->MultiCell($hli['width'],$ini_height,$fr_row[$hli['param']],"B",'L',0,1,$tmx,$tmy,true,0,true);
                }
                else{
                    $this->MultiCell($hli['width'],$ini_height,$count,"B",'L',0,1,$tmx,$tmy,true,0,true);
                    $count++;
                }
                $tmx = $tmx + $hli['width'];
            }
        }

        //sub total 
        $this->Ln(2);
        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height +5 );
        if($this->isAddPage){
            $tmy = $this->SetAddress();
            $this->SetFont($this->font_name,'BU',$this->content_font_size);
            $this->Rect(10, $this->GetY(), $fr_width, $fr_height,'DF',$list_border_style,$list_react_color_style);
            $tmy = $this->generateColumnHeader($tmy, 10, $fr_header) + 3;
            $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
            $this->SetFont($this->font_name,'',$this->content_font_size);
        }
        $tmx = 10;
        $this->SetFont($this->font_name,'B',$this->content_font_size);
        foreach($fr_header as $hky => $hli){
            if(isset($hli['param']) && $hli['param'] == 'order_cancel_rate'){
                $this->MultiCell($hli['width'],5,"Sub Total",0,'L',0,1,$tmx,$tmy,true,0,true);
            }
            elseif(isset($hli['param']) && array_key_exists($hli['param'], $fr_subtotal)){
                $this->MultiCell($hli['width'],$ini_height,"($".num_format(abs($fr_subtotal[$hli['param']])).")","B",'R',0,1,$tmx,$tmy,true,0,true);
            }
            $tmx += $hli['width'];
        }
        $this->Ln(2);
        //end of listing

        $this->SetFont('helvetica','B',$this->content_font_size);
        $tmy = $this->checkYaxisoverflow($this->GetY(), 210);
       
        if($this->isAddPage & false){
            $tmy = $this->SetAddress();
            $this->SetFont($this->font_name,'B',$this->content_font_size);
        }

        // total end
        $this->SetFont($this->font_name,'',$this->content_font_size);
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

        $grand_total['tsa_refund'] += $grand_total['comm_cawl_back']; 
        
        foreach($total_item as $total_ky => $total_li){
            $this->Cell(40,5, $total_ky,0,0,'L');
            $this->Cell(5,5, ":$",0,0,'L');
            if($total_li == 'order_cus_pay' || $total_li == 'order_tsa_pay'){
                if($total_li == 'order_cus_pay' ){
                    $total =  $grand_total['order_gross_premium'] - $grand_total['paid_total'] + $grand_total['total_refund'];
                }
                else{
                    $total =  $grand_total['order_gross_premium'] - $grand_total['paid_total'] + $grand_total['tsa_refund'] - $grand_total['comm_cawl_back'] - $grand_total['order_receivable_dealercomm'];
                }
                if($total >= 0){
                    $this->Cell(30,5, num_format($total),'TB',0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($total)).")",'TB',0,'R');
                }
            }
            elseif($total_li == 'comm_cawl_back'){
                if($grand_total[$total_li] <= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'tsa_refund'){
                if($grand_total[$total_li] >= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'order_receivable_dealercomm'){
                if($grand_total[$total_li] <= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'paid_total'){
                if($grand_total[$total_li] <= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'order_gross_premium'){
                if($grand_total[$total_li] >= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'total_refund'){
                if($grand_total[$total_li] >= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            
            $this->Ln(5);
        }

        $this->Ln(8);
        $tmx = 10;
        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->SetFont($this->font_name,'',$this->content_font_size);
        $this->MultiCell(110,5,"Please Issue Cheque Make Payable To \"<b>".$this->cprofile_name."</b>\"",0,'L',0,1,$tmx,$tmy,true,0,true);
        $this->Ln(5);
        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        if($_SESSION['empl_outlet'] == 5){
            $this->MultiCell(110,5,"(<b>DBS</b>) Current Bank Account No. :<b>072-007287-1</b>",0,'L',0,1,$tmx,$tmy,true,0,true);
            $this->Ln(5);
            $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        }
        $this->MultiCell(110,5,"* This is a Computer generated letter, which requires no signature",0,'L',0,1,$tmx,$tmy,true,0,true);

        ///cover page
        $this->AddPage();
        $this->SetAddress();
        $this->SetFont('helvetica','B',$this->title_font_size);
        $this->Cell(150,3, strtoupper("RE : Statement Of Account - Full Listing ".date('d-M-Y', strtotime($this->period_from))." To ".date('d-M-Y', strtotime($this->period_to))),0,0,'L');
        $this->SetFont('helvetica','',$this->content_font_size);
        $this->Ln(5);
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);

        $this->SetFont($this->font_name,'B',10);
        $this->Rect(10, $this->GetY(), 190, 5,'DF',$list_border_style,$list_react_color_style);
        $this->Cell(150,5, "Description",0,0,'L');
        $this->Cell(30,5, "| Balance",0,0,'L');
        $this->Ln(8);

        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        foreach($total_item as $tl_ky => $total_li){
            $this->Cell(150,5, $tl_ky,0,0,'L');
            $this->Cell(5,5, "$",0,0,'L');
            if($total_li == 'order_cus_pay' || $total_li == 'order_tsa_pay'){
                if($total_li == 'order_cus_pay' ){
                    $total =  $grand_total['order_gross_premium'] - $grand_total['paid_total'] + $grand_total['total_refund'];
                }
                else{
                    $total =  $grand_total['order_gross_premium'] - $grand_total['paid_total'] + $grand_total['tsa_refund'] - $grand_total['comm_cawl_back'] - $grand_total['order_receivable_dealercomm'];
                }
                if($total >= 0){
                    $this->Cell(30,5, num_format($total),'TB',0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($total)).")",'TB',0,'R');
                }
            }
            elseif($total_li == 'comm_cawl_back'){
                if($grand_total[$total_li] <= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'tsa_refund'){
                if($grand_total[$total_li] >= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'order_receivable_dealercomm'){
                if($grand_total[$total_li] <= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'paid_total'){
                if($grand_total[$total_li] <= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'order_gross_premium'){
                if($grand_total[$total_li] >= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            elseif($total_li == 'total_refund'){
                if($grand_total[$total_li] >= 0){
                    $this->Cell(30,5, num_format(abs($grand_total[$total_li])),0,0,'R');
                }
                else{
                    $this->Cell(30,5, "(".num_format(abs($grand_total[$total_li])).")",0,0,'R');
                }
            }
            $this->Ln(8);
        }
        $this->SetY(-90);
        $this->SetFont($this->font_name,'',$this->content_font_size);
        $this->MultiCell(110,5,"Please Issue Cheque Make Payable To \"<b>".$this->cprofile_name."</b>\"",0,'L',0,1,$tmx,$this->GetY(),true,0,true);
        $this->Ln(5);
        if($_SESSION['empl_outlet'] == 5){
            $this->MultiCell(110,5,"(<b>DBS</b>) Current Bank Account No. :<b>072-007287-1</b>",0,'L',0,1,$tmx,$this->GetY(),true,0,true);
            $this->Ln(5);
        }
        $this->MultiCell(110,5,"* This is a Computer generated letter, which requires no signature",0,'L',0,1,$tmx,$this->GetY(),true,0,true);
        
        $pageNo = $this->PageNo(); 
        // First page of the document
        $toPage = 1;
        $this->movePage($pageNo, $toPage);
    }

    public function getReceiptData($order_id){
        $receipt_query = ReceiptDetail::select('receipt_detail.recpline_order_amount', 'receipt_detail.recpline_offset_amount AS paid_total', 'receipt.receipt_no', 'receipt.receipt_date', 'receipt.receipt_cheque')
        ->join('receipt', 'receipt.receipt_id', '=', 'receipt_detail.recpline_receipt_id')
        ->where('receipt_detail.recpline_order_id', $order_id)
        ->where('receipt.receipt_status', 1)
        ->where('receipt_detail.recpline_status', 1)
        ->get()->toArray();

        $output = [];

        foreach($receipt_query as $row){
            $output[] = $row;
        }

        return $output;
    }

    public function getPaymentData($order_id, $bill_to = ''){
        $payment_query = Payment::select('payment.payment_cheque', 'payment.payment_method')
        ->join('payment', 'payment.payment_id', '=', 'payment_detail.paymentline_payment_id')
        ->where('payment_detail.paymentline_order_id', $order_id)
        ->where('payment.payment_status', 1)
        ->where('payment_detail.paymentline_status', 1);

        if($this->dealer_id > 0 && $bill_to == 'To Customer'){
            $payment_query->where('payment.payment_type', '!=', 'customer');
        }

        $pay_data = $payment_query->first()->toArray();
        return $pay_data;
    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }

    public function SetAddress(){
        if(in_array($this->filter_by, array('GMM'))){
            $tmy = $this->GetY();
            if($this->cus_id > 0){
                $target_data = $this->getPartnerInfoByID($this->cus_id);
                $this->SetFont('helvetica','B',9);
                $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['partner_name'])),0,'L',0,1,10,$this->GetY());
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
    
                if($target_data['partner_blk_no'] || $target_data['partner_street']){
                    $this->MultiCell(70,0,strtoupper($target_data['partner_blk_no']." ".$target_data['partner_street']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['partner_unit_no']){
                    $this->MultiCell(70,0,strtoupper($target_data['partner_unit_no']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['partner_postal_code']){
                    $this->MultiCell(70,0,strtoupper("SINGAPORE ".$target_data['partner_postal_code']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
            }
            elseif($this->dealer_id > 0){
                $target_data = $this->getDealerInfoByID($this->dealer_id);
                $this->SetFont('helvetica','B',9);
                $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['dealer_name'])),0,'L',0,1,10,$this->GetY());
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
                if($target_data['dealer_blk_no'] || $target_data['dealer_street']){
                    $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['dealer_blk_no']." ".$target_data['dealer_street'])),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['dealer_unit_no']){
                    $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['dealer_unit_no'])),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['dealer_postalcode']){
                    $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['dealer_postalcode'])),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['dealer_payableto']){
                    $this->MultiCell(70,0,htmlspecialchars_decode("Attn. :".$target_data['dealer_payableto']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
            }
            $target_height = $this->GetY();
            $this->SetFont('helvetica','B',8);
            
            $this->MultiCell(60,3,strtoupper("Date :".date('d-M-Y')),0,'L',0,1,140,$tmy + 1);
            $this->MultiCell(60,3,strtoupper($this->soa_no),0,'L',0,1,140,$this->GetY());
            $this->MultiCell(60,3,strtoupper($this->cprofile_name),0,'L',0,1,140,$this->GetY());
            $this->MultiCell(60,3,strtoupper("GST Reg No.".$this->cprofile_reg_no),0,'L',0,1,140,$this->GetY());
            $this->SetFont('helvetica','',8);
            if($target_height > $this->GetY()){
                $this->SetY($target_height);
            }
            $this->Ln(5);
            
        }
    
        return $this->GetY();
    
    }
}