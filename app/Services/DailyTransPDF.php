<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;

class DailyTransPDF extends BasePDF {
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        $this->SetFont('helvetica','B',12);
        $this->Cell(100,10, "TRANSACTION LISTING - (".strtoupper(date('d-M-Y',strtotime($this->period_from)))." - ".strtoupper(date('d-M-Y',strtotime($this->period_to))).")",0,0,'L');
        $this->Ln(5);
        $this->Cell(100,10, "Filter By : ".strtoupper($this->filter_by) ,0,0,'L');
        $title_height = $this->GetY();
        if(file_exists($this->cprofile_logo_url)){
            $this->SetY(2);
            $tmy = $iniy = $this->GetY();
            $new_height = 0;
            $new_width = 20;
            list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
            $width_scalling_per = (($new_width / $img_width) * 100);
            $new_height = ($img_height/100) * $width_scalling_per;
            $this->Image($this->cprofile_logo_url, 260, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $logo_height = $this->GetY() + $new_height;
        }
        
        if($logo_height > $title_height){
            $this->line(10,$logo_height + 5,285,$logo_height + 5);
            $this->header_title_height = $logo_height+10 ;
            $this->SetMargins(15,  $this->header_title_height, 15);
        }
        else{
            $this->line(10,$title_height + 5,285,$title_height + 5);
            $this->header_title_height = $title_height+10 ;
            $this->SetMargins(15,  $this->header_title_height, 15);
        }
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $this->AddPage("L");
        $this->SetFont($this->font_name,'BU',8);
        $this->SetAutoPageBreak(true, 10);
        $land_height = '170';
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        if($this->filter_by == 'receipt'){
            $header = array(
                "No" => array('length'=>10),
                "Document No" => array('length'=>25, 'param'=>'order_no'),
                "Receipt Date" => array('length'=>25, 'param'=>'receipt_date'),
                "Receipt No" => array('length'=>30, 'param'=>'receipt_no'),
                "Received Method" => array('length'=>30, 'param'=>'receipt_method'),
                "Receive From" => array('length'=>60, 'param'=>'receive_from'),
                "Receive Amount" => array('length'=>30, 'param'=>'recpline_offset_amount', 'number' => true),
                "Receive Remark" => array('length'=>60, 'param'=>'receipt_remarks'),
            );
        }
        else{
            $header = array(
                "No" => array('length'=>10),
                "Document No" => array('length'=>25, 'param'=>'order_no'),
                "Payment Date" => array('length'=>25, 'param'=>'payment_date'),
                "Payment No" => array('length'=>25, 'param'=>'payment_no'),
                // "Payment Purpose" => array('length'=>30, 'param'=>'paymentline_purpose_id'),
                "Payment Method" => array('length'=>25, 'param'=>'payment_method'),
                "Payment To" => array('length'=>50, 'param'=>'payment_to'),
                "Payment Amount" => array('length'=>30, 'param'=>'paid_amt', 'number' => true),
                "Payment Remark" => array('length'=>50, 'param'=>'payment_remarks'),
            );
        }
        
        $tmx = 15;
        $ini_height = 3;
        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        foreach($header as $ky => $li){
            $ini_height = $this->checkMulticellHeight($ky, $li['length'], $ini_height);
        }
        
        foreach($header as $ky => $li){
            $this->MultiCell($li['length'],$ini_height,$ky,0,'L',0,1,$tmx,$tmy);
            $tmx = $tmx + $li['length'];
        }
        $this->SetFont($this->font_name,'',8);
        $this->Ln(5);
        $total = array(
            'paid_amt' => 0,
            'recpline_offset_amount' => 0,
        );
        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        foreach($this->data_listing as $data_ky => $data_list){
            $tmx = 15;
            $ini_height = 3;
            foreach($total as $cal_ky => $cal_li){
                $total[$cal_ky] += $data_list[$cal_ky];
            }
            
            foreach($header as $ky => $li){
                if(isset($li['param'])){
                $ini_height = $this->checkMulticellHeight($data_list[$li['param']], $li['length'], $ini_height);
                }
            }
            if(($data_ky + 1) % 2 == 0){
                $this->Rect($tmx, $tmy, 270, $ini_height,'DF',$list_border_style,$list_react_color_style);
            }
            foreach($header as $ky => $li){
                if(isset($li['number'])){
                    $this->MultiCell($li['length'],$ini_height,"$".$this->num_format($data_list[$li['param']]),0,'R',0,1,$tmx,$tmy);
                }
                elseif(isset($li['param'])){
                    $this->MultiCell($li['length'],$ini_height,strtoupper($data_list[$li['param']]),0,'L',0,1,$tmx,$tmy);
                }
                else{
                    $this->MultiCell($li['length'],$ini_height,$data_ky+1,0,'L',0,1,$tmx,$tmy);
                }
                $tmx = $tmx + $li['length'];
            }
            $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        }
        
        $tmx = 15;
        $tmy = $this->checkYaxisoverflow($this->GetY()+5, 170);
        $this->SetFont($this->font_name,'B',8);
        $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(15,$tmy,270 +$tmx,$tmy);
        $tmy = $tmy + 3;
        $tmy = $this->checkYaxisoverflow($tmy, 170);
        $this->MultiCell(20,3,"Total :",0,'L',0,1,$tmx,$tmy);
        foreach($header as $ky => $li){
            if(isset($li['param'],) && in_array($li['param'], array_keys($total))){
                $this->MultiCell($li['length'],3,"$".$this->num_format($total[$li['param']]),0,'R',0,1,$tmx,$tmy);
                $tmx += $li['length'];
            }
            else{
                $tmx += $li['length'];
                continue;
            }
        }
        $this->SetFont($this->font_name,'',8);
    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }
}