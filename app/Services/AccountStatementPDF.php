<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;
use Carbon\Carbon;
use App\Models\Receipt;
use App\Models\ReceiptDetail;
use App\Models\Payment;
use App\Models\PaymentDetail;

class AccountStatementPDF extends BasePDF {
    public $title_font_size = '10';
    public $content_font_size = '8';
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        if(in_array($this->filter_by, array('MI','CMI','PMI', 'GI', 'PCMI'))){
            if(file_exists($this->cprofile_logo_url)){
                $this->SetY(5);
                $tmy = $iniy = $this->GetY();
                $new_height = 0;
                $new_width = 20;
                list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                $width_scalling_per = (($new_width / $img_width) * 100);
                $new_height = ($img_height/100) * $width_scalling_per;
                $this->Image($this->cprofile_logo_url, 180, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                $this->SetY($tmy + $new_height + 5);
            }
            else{
                $this->SetY(5);
            }
            $this->SetFont('helvetica','',8);
            $this->Cell(100,5, "Date : ".strtoupper(date('d-m-Y',strtotime(Carbon::now()))),0,0,'L');

            $this->Ln(8);
            $address_ori_height = $this->GetY();
            if($this->cus_id > 0){
                $target_data = $this->getPartnerInfoByID($this->cus_id);
                $this->SetFont('helvetica','B',9);
                $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['partner_name'])),0,0,'L');
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
                if($target_data['partner_blk_no'] || $target_data['partner_street']){
                    $this->Cell(50,3, strtoupper($target_data['partner_blk_no']." ".$target_data['partner_street']),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['partner_unit_no']){
                    $this->Cell(50,3, strtoupper($target_data['partner_unit_no']),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['partner_postal_code']){
                    $this->Cell(50,5, strtoupper("SINGAPORE ".$target_data['partner_postal_code']),0,0,'L');
                    $this->Ln(3);
                }
            }
            elseif($this->dealer_id > 0){
                $target_data = $this->getPartnerInfoByID($this->dealer_id);
                $this->SetFont('helvetica','B',9);
                $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['dealer_name'])),0,0,'L');
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
                if($target_data['dealer_blk_no'] || $target_data['dealer_street']){
                    $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_blk_no']." ".$target_data['dealer_street'])),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['dealer_unit_no']){
                    $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_unit_no'])),0,0,'L');
                    $this->Ln(3);
                }
                if($target_data['dealer_postalcode']){
                    $this->Cell(50,3, strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['dealer_postalcode'])),0,0,'L');
                    $this->Ln(3);
                }
            }
            else{
                $this->SetFont('helvetica','',8);
            }
            $tmy = $address_ori_height;
            $this->SetFont('helvetica','B',8);
            $this->MultiCell(60,3,strtoupper($this->soa_no),0,'L',0,1,140,$tmy + 1);
            $this->MultiCell(60,3,strtoupper($this->cprofile_name),0,'L',0,1,140,$this->GetY());
            $this->MultiCell(60,3,strtoupper("GST Reg No.".$this->cprofile_reg_no),0,'L',0,1,140,$this->GetY());
            $this->Ln(5);
            $this->SetFont('helvetica','B',8);
            $this->Cell(150,3, strtoupper("RE : Statement Of Account - Full Listing ".date('d-M-Y', strtotime($this->period_from))." To ".date('d-M-Y', strtotime($this->period_to))),0,0,'L');
            $this->SetFont('helvetica','',8);
            $this->Ln(5);
            $this->header_title_height = $this->GetY();
        }
        elseif(in_array($this->filter_by, array('GMM'))){
            if(file_exists($this->cprofile_logo_url)){
                $this->SetY(5);
                $tmy = $iniy = $this->GetY();
                $new_height = 0;
                if(env('APP_ID') == 3){
                    $new_width = 50;
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = (($new_width / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 10, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                }
                else{
                    $new_width = 20;
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = (($new_width / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 180, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                }
                $this->SetY($tmy + $new_height + 5);
            }
            else{
                $this->SetY(5);
            }

            if(env('APP_ID') == 3){
                $this->SetFont('helvetica','B',9);
                $this->MultiCell(65,5,strtoupper($this->cprofile_name ),0,'L',0,1,35,20);
                $this->SetFont('helvetica','',7);
                $this->MultiCell(65,3,strtoupper("Company Reg No.".$this->cprofile_reg_no),0,'L',0,1,33,25);
                $this->MultiCell(100,3,strtoupper($this->cprofile_full_address),0,'L',0,1,30,28);
                $this->MultiCell(150,3,strtoupper("TEL:".$this->cprofile_tel." FAX:".$this->cprofile_fax." EMAIL:".$this->cprofile_email),0,'L',0,1,25,31);
                $this->Ln(8);
                                    
                $this->header_title_height = $this->GetY();
                $this->SetMargins(15,  $this->header_title_height, 15);
                
                if(file_exists($this->cprofile_logo_url)){
                    $this->SetAlpha(0.15);
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = ((160 / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 25, 95, 160, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                    $this->SetAlpha(1);
                }
            }
            else{
                if(file_exists($this->cprofile_logo_url)){
                    $this->SetY(5);
                    $tmy = $iniy = $this->GetY();
                    $new_height = 0;
                    $new_width = 20;
                    list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                    $width_scalling_per = (($new_width / $img_width) * 100);
                    $new_height = ($img_height/100) * $width_scalling_per;
                    $this->Image($this->cprofile_logo_url, 180, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                    $this->SetY($tmy + $new_height + 5);
                }
                else{
                    $this->SetY(5);
                }
                $this->SetFont('helvetica','',8);
                $this->Cell(100,5, "Date : ".strtoupper(date('d-m-Y',strtotime(system_date))),0,0,'L');

                $this->Ln(8);
                if($this->cus_id > 0){
                    $target_data = $this->getPartnerInfoByID($this->cus_id);
                    $this->SetFont('helvetica','B',9);
                    $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['partner_name'])),0,0,'L');
                    $this->SetFont('helvetica','',8);
                    $this->Ln(6);
                    if($target_data['partner_blk_no'] || $target_data['partner_street']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['partner_blk_no']." ".$target_data['partner_street'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['partner_unit_no']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['partner_unit_no'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['partner_postal_code']){
                        $this->Cell(50,5, strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['partner_postal_code'])),0,0,'L');
                        $this->Ln(3);
                    }
                }
                elseif($this->dealer_id > 0){
                    $target_data = $this->getPartnerInfoByID($this->dealer_id);
                    $this->SetFont('helvetica','B',9);
                    $this->Cell(50,5, strtoupper(htmlspecialchars_decode($target_data['dealer_name'])),0,0,'L');
                    $this->SetFont('helvetica','',8);
                    $this->Ln(6);
                    if($target_data['dealer_blk_no'] || $target_data['dealer_street']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_blk_no']." ".$target_data['dealer_street'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['dealer_unit_no']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode($target_data['dealer_unit_no'])),0,0,'L');
                        $this->Ln(3);
                    }
                    if($target_data['dealer_postalcode']){
                        $this->Cell(50,3, strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['dealer_postalcode'])),0,0,'L');
                        $this->Ln(3);
                    }
                }
                else{
                    $this->SetFont('helvetica','',8);
                }
                $this->Ln(5);
                $this->SetFont('helvetica','B',8);
                $this->Cell(150,3, strtoupper(htmlspecialchars_decode("RE : Statement Of Account - Full Listing ".date('d-M-Y', strtotime($this->period_from))." To ".date('d-M-Y', strtotime($this->period_to)))),0,0,'L');
                $this->SetFont('helvetica','',8);
                $this->Ln(5);
                $this->header_title_height = $this->GetY();
                $this->SetMargins(15,  $this->header_title_height, 15);
            }
            
            if(file_exists($this->cprofile_logo_url)){
                $this->SetAlpha(0.15);
                list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
                $width_scalling_per = ((160 / $img_width) * 100);
                $new_height = ($img_height/100) * $width_scalling_per;
                $this->Image($this->cprofile_logo_url, 25, 95, 160, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
                $this->SetAlpha(1);
            }
        }
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $number_addtional = 0;
        $this->SetFont($this->font_name,'BU',6);
        $this->SetAutoPageBreak(true, 10);
        $this->SetY($this->header_title_height);
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        $maxlength = 270;
        $ini_height = 3;
        $total_width = 0;

        if(in_array($this->filter_by,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "No" => array('width'=>10),
                "Date" => array('width'=>17, 'param'=>'order_date'),
                "DN No" => array('width'=>25, 'param'=>'order_no'),
                "Insured Name" => array('width'=>35, 'param'=>'partner_name'),
                "Vehicle No" =>array('width'=>17, 'param'=>'order_vehicles_no'),
                "Premium" => array('width'=>16, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Direct Disc" => array('width'=> 16, 'param'=>'order_direct_discount_amt', 'number' => true, 'position'=>'R'),
                "Ref Fee" => array('width'=>16, 'param'=>'order_receivable_dealercomm', 'number' => true, 'position'=>'R'),
                "Ref Fee GST" => array('width'=>16, 'param'=>'order_receivable_dealergstamount', 'number' => true, 'position'=>'R'),
                "Due Pay" => array('width'=>16, 'param'=>'order_tsa_pay', 'number' => true, 'position'=>'R'),
                "Type" => array('width'=>9, 'param'=>'order_display_type'),
            );

            $cn_header = array(
                "No" => array('width'=>10),
                "Date" => array('width'=>17, 'param'=>'order_date'),
                "DN No" => array('width'=>25, 'param'=>'order_no'),
                "Insured Name" => array('width'=>35, 'param'=>'partner_name'),
                "Vehicle No" =>array('width'=>17, 'param'=>'order_vehicles_no'),
                "Refund Premium" => array('width'=>16, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Crawl back Disc" => array('width'=> 16, 'param'=>'order_direct_discount_amt', 'number' => true, 'position'=>'R'),
                "Crawl back Ref Fee" => array('width'=>16, 'param'=>'order_receivable_dealercomm', 'number' => true, 'position'=>'R'),
                "Crawl back Ref Fee GST" => array('width'=>16, 'param'=>'order_receivable_dealergstamount', 'number' => true, 'position'=>'R'),
                "Total refund" => array('width'=>16, 'param'=>'order_tsa_pay', 'number' => true, 'position'=>'R'),
                "Type" => array('width'=>9, 'param'=>'order_display_type'),
            );
        }
        else{
            $header = array(
                "No" => array('width'=>10),
                "Date" => array('width'=>17, 'param'=>'order_date'),
                "DN No" => array('width'=>24, 'param'=>'order_no'),
                "Insured Name" => array('width'=>28.5, 'param'=>'partner_name'),
                "Coverage Plan" =>array('width'=>25, 'param'=>'order_coverage'),
                "Premium" => array('width'=>15, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Direct Disc" => array('width'=> 13.3, 'param'=>'order_direct_discount_amt', 'number' => true, 'position'=>'R'),
                "Ref Fee" => array('width'=>15.2, 'param'=>'order_receivable_dealercomm', 'number' => true, 'position'=>'R'),
                "Ref Fee GST" => array('width'=>15.2, 'param'=>'order_receivable_dealergstamount', 'number' => true, 'position'=>'R'),
                "Due Pay" => array('width'=>20, 'param'=>'order_tsa_pay', 'number' => true, 'position'=>'R'),
                "Type" => array('width'=>10, 'param'=>'order_display_type'),
            );
    
            $cn_header = array(
                "No" => array('width'=>10),
                "Date" => array('width'=>17, 'param'=>'order_date'),
                "DN No" => array('width'=>24, 'param'=>'order_no'),
                "Insured Name" => array('width'=>28.5, 'param'=>'partner_name'),
                "Coverage Plan" =>array('width'=>25, 'param'=>'order_coverage'),
                "Refund Premium" => array('width'=>15, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Crawl back Disc" => array('width'=> 13.3, 'param'=>'order_direct_discount_amt', 'number' => true, 'position'=>'R'),
                "Crawl back Ref Fee" => array('width'=>15.2, 'param'=>'order_receivable_dealercomm', 'number' => true, 'position'=>'R'),
                "Crawl back Ref Fee GST" => array('width'=>15.2, 'param'=>'order_receivable_dealergstamount', 'number' => true, 'position'=>'R'),
                "Total Refund" => array('width'=>20, 'param'=>'order_tsa_pay', 'number' => true, 'position'=>'R'),
                "Type" => array('width'=>10, 'param'=>'order_display_type'),
            );
        }

        if($this->cus_id > 0){
            unset($header['Ref Fee'], $header['Ref Fee GST']);
            unset($cn_header['Crawl back Ref Fee'], $cn_header['Crawl back Ref Fee GST']);
            $header['Due Pay']['param'] = "order_cus_pay"; 
            $cn_header['Total refund']['param'] = "order_cus_pay"; 
        }
        else{
            unset($header['Direct Disc']);
            unset($cn_header['Crawl back Disc']);
        }

        $total_cal = array(
            'order_gross_premium' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'order_cus_pay' => 0,
            'order_tsa_pay' => 0,
        );

        $receipt_total = 0;
        $payment_total = 0;

        $tmx = 10;
        $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height);
        $tmy = $this->generateColumnHeader($tmy, 10, $header);
        $this->SetFont($this->font_name,'',$this->content_font_size);

        $receipt_sub_total = 0;
        $count_block = 1;
        $count = 1;

        $sub_total = array(
            'order_gross_premium' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'order_cus_pay' => 0,
            'order_tsa_pay' => 0,
        );

        $tmx = 10;

        // Debit Notes Part       
        foreach($header as $ky => $li){
            if($this->cus_id > 0){
                if(in_array($li['param'], array('partner_name','order_gross_premium','order_direct_discount_amt','order_receivable_premiumreceivable',))){
                    $header[$ky]['width'] = $li['width'] + 7.6;
                }
            }
            else{
                if(in_array($li['param'], array('order_gross_premium','order_receivable_dealercomm','order_receivable_dealergstamount','order_tsa_pay'))){
                    $header[$ky]['width'] = $li['width'] + 3.8;
                }
            }
            
            $ini_height = $this->checkMulticellHeight($ky,  $header[$ky]['width'] , $ini_height);
            
        }

        $tmx = 10;
        $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height);
        $tmy = $this->generateColumnHeader($tmy, 10, $header);
        $this->SetFont($this->font_name,'',$this->content_font_size);
        

        foreach($this->data_listing['DN'] as $li_ky => $dn_row){
            $tmy = $this->GetY() ;
            $tmx = 10;
            $ini_height =3;
            foreach($header as $ky => $li){
                $ini_height = $this->checkMulticellHeight($dn_row[$li['param']], $li['width'], $ini_height);
            }
            
            foreach($sub_total as $sub_key  => $sub_item){
                if(isset($dn_row[$sub_key])){
                    $sub_total[$sub_key] += $dn_row[$sub_key];
                    $total_cal[$sub_key] += $dn_row[$sub_key];
                }
            }

            $tmy = $this->checkYaxisoverflow($tmy, $this->land_height);
            if($this->isAddPage){
                $this->SetFont($this->font_name,'BU',$this->content_font_size);
                $tmy = $this->generateColumnHeader($tmy, 10, $header);
                $this->SetFont($this->font_name,'',$this->content_font_size);
            }

            if(($count_block) % 2 == 0){
                $this->Rect($tmx, $tmy, $maxlength-$tmx, $ini_height,'DF',$list_border_style,$list_react_color_style);
            }

            foreach($header as $ky => $li){
                if(isset($li['number'])){
                    if(isset($li['param']) && $li['param'] == 'order_direct_discount_amt'){
                        if($dn_row[$li['param']] >= 0){
                            $this->MultiCell($li['width'],$ini_height,"($".$this->num_format($dn_row[$li['param']]).")",0,'R',0,1,$tmx,$tmy);
                        }
                        else{
                            $this->MultiCell($li['width'],$ini_height,"$".$this->num_format(abs($dn_row[$li['param']])),0,'R',0,1,$tmx,$tmy);
                        }
                    }
                    else{
                        $this->MultiCell($li['width'],$ini_height,"$".$this->num_format($dn_row[$li['param']]),0,'R',0,1,$tmx,$tmy);
                    }
                }
                elseif(isset($li['param'])){
                    $this->MultiCell($li['width'],$ini_height,str_replace('&amp;', '&', $dn_row[$li['param']]),0,'L',0,1,$tmx,$tmy);
                }
                else{
                    $this->MultiCell($li['width'],$ini_height,$count,0,'L',0,1,$tmx,$tmy);
                }
                $tmx = $tmx+$li['width'];
            }
            $count_block++;
            $count ++;
            $receipt_data = $this->getReceiptData($dn_row['order_id']);
            foreach($receipt_data as $rec_ky => $rec_li){
                $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',$this->content_font_size);
                    $tmy = $this->generateColumnHeader($tmy, 10, $header);
                    $this->SetFont($this->font_name,'',$this->content_font_size);
                }
                $ini_height = 3;
                $tmx = 10;
                $rec_string = "Receipt:".date("d-m-Y",strtotime($rec_li['receipt_date'])). " ". $rec_li['receipt_cheque'];
                $ini_height = $this->checkMulticellHeight($rec_string, $header['Insured Name']['width'], $ini_height);
                if(($count_block) % 2 == 0){
                    $this->Rect($tmx, $tmy, $maxlength-$tmx, $ini_height,'DF',$list_border_style,$list_react_color_style);
                }
                $receipt_sub_total += $rec_li['paid_total'];
                $receipt_total += $rec_li['paid_total'];
                foreach($header as $ky => $li){
                    if(isset($li['param']) && $li['param'] == 'partner_name'){
                        $this->MultiCell($li['width'],$ini_height,removeAmpcharacter($rec_string),0,'L',0,1,$tmx,$tmy);
                        $tmx = $tmx+$li['width'];
                    }
                    elseif(isset($li['param']) && ($li['param'] == 'order_gross_premium' || $li['param'] == 'order_tsa_pay' || $li['param'] == 'order_cus_pay')){
                        $this->MultiCell($li['width'],$ini_height,"($".$this->num_format($rec_li['paid_total']).")",0,'R',0,1,$tmx,$tmy);
                        $tmx = $tmx+$li['width'];
                    }
                    else{
                        $tmx = $tmx+$li['width'];
                        continue;
                    }
                }
                $count_block++;
            }
        }

        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',$this->content_font_size);
            $tmy = $this->generateColumnHeader($tmy, 10, $header);
            $this->SetFont($this->font_name,'',$this->content_font_size);
        }
        $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(10,$tmy,$maxlength,$tmy);
        $tmy = $tmy + 5;
        $tmy = $this->checkYaxisoverflow($tmy, $this->land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',$this->content_font_size);
            $tmy = $this->generateColumnHeader($tmy, 10, $header);
            $this->SetFont($this->font_name,'',$this->content_font_size);
        }
        $tmx = 10;
        $this->MultiCell(20,3,"SUB Total :",0,'L',0,1,$tmx,$tmy);
        $sub_total["order_gross_premium"] -= $receipt_sub_total; 
        $sub_total["order_cus_pay"] -= $receipt_sub_total; 
        $sub_total["order_tsa_pay"] -= $receipt_sub_total; 
        foreach($header as $ky => $li){
            if(isset($li['param']) && in_array($li['param'], array_keys($sub_total))){
                $this->MultiCell($li['width'],3,"$".$this->num_format($sub_total[$li['param']]),0,'R',0,1,$tmx,$tmy);
                $tmx += $li['width'];
            }
            else{
                $tmx += $li['width'];
                continue;
            }
        }

        // Credit Notes Part
        $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height);
        $this->SetFont($this->font_name,'BU',$this->title_font_size);
        $this->MultiCell(100,5,"Crawl Back Referral Fee / Refundable",0,'L',0,1,15,$tmy);
        $this->SetFont($this->font_name,'BU',$this->content_font_size);
        $tmy = $this->checkYaxisoverflow($this->GetY() + 5, $this->land_height);
        $tmy = $this->generateColumnHeader($tmy, 10, $cn_header);
        $this->SetFont($this->font_name,'',$this->content_font_size);
        
        $count_block = 1;
        $receipt_sub_total = 0;
        $count = 1;

        $sub_total = array(
            'order_gross_premium' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'order_cus_pay' => 0,
            'order_tsa_pay' => 0,
        );

        foreach($this->data_listing['CN'] as $li_ky => $cn_row){
            foreach($sub_total as $sub_key  => $sub_item){
                if($sub_key == 'order_tsa_pay' && $cn_row['order_record_billto'] == "To Customer"){
                    $sub_total[$sub_key] += abs($cn_row[$sub_key]);
                    $total_cal[$sub_key] += abs($cn_row[$sub_key]);
                }
                else{
                    $sub_total[$sub_key] -= abs($cn_row[$sub_key]);
                    $total_cal[$sub_key] -= abs($cn_row[$sub_key]);        
                }
            }

            $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
            $tmx = 10;
            $ini_height =3;
            foreach($cn_header as $ky => $li){
                $ini_height = $this->checkMulticellHeight($cn_row[$li['param']], $li['width'], $ini_height);
            }
            if($this->isAddPage){
                $this->SetFont($this->font_name,'BU',$this->content_font_size);
                $tmy = $this->generateColumnHeader($tmy, 10, $cn_header);
                $this->SetFont($this->font_name,'',$this->content_font_size);
            }

            if(($count_block) % 2 == 0){
                $this->Rect($tmx, $tmy, $maxlength-$tmx, $ini_height,'DF',$list_border_style,$list_react_color_style);
            }

            foreach($cn_header as $ky => $li){
                if(isset($li['number'])){
                    if(isset($li['param']) && $cn_row[$li['param']] != 0){
                        if($li['param'] == 'order_tsa_pay' && $cn_row['order_record_billto'] == "To Customer"){
                            $this->MultiCell($li['width'],$ini_height,"$".$this->num_format(abs($cn_row[$li['param']])),0,'R',0,1,$tmx,$tmy);
                        }
                        else{
                            $this->MultiCell($li['width'],$ini_height,"($".$this->num_format(abs($cn_row[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                        }
                    }
                    else{
                        $this->MultiCell($li['width'],$ini_height,"",0,'R',0,1,$tmx,$tmy);
                    }
                }
                elseif(isset($li['param'])){   
                    $this->MultiCell($li['width'],$ini_height,removeAmpcharacter($cn_row[$li['param']]),0,'L',0,1,$tmx,$tmy);
                }
                else{
                    $this->MultiCell($li['width'],$ini_height,$count,0,'L',0,1,$tmx,$tmy);
                }
                $tmx = $tmx+$li['width'];
            }

            $sub_title = "";
            $pay_row = $this->getPaymentData($cn_row['order_id'], $cn_row['order_record_billto']);
            if(isset($pay_row['payment_method']) && $pay_row['payment_method']){
                $sub_title = "Payment : " . $pay_row['payment_method'] . " " . $pay_row['payment_cheque'];
                $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);
                $tmx = 10;
                if($this->isAddPage){
                    $this->SetFont($this->font_name,'BU',$this->content_font_size);
                    $tmy = $this->generateColumnHeader($tmy, 10, $cn_header);
                    $this->SetFont($this->font_name,'',$this->content_font_size);
                }
                foreach($cn_header as $ky => $li){
                    if(isset($li['param']) && $li['param'] == 'partner_name'){
                        $this->MultiCell($li['width'],$ini_height,$sub_title,0,'L',0,1,$tmx,$tmy);
                    }
                    $tmx = $tmx+$li['width'];
                }
            }

            $count_block++;
            $count++;
        }

        $tmy = $this->checkYaxisoverflow($this->GetY()+3, $this->land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',6);
            $tmy = $this->generateColumnHeader($tmy, 10, $cn_header);
            $this->SetFont($this->font_name,'',6);
        }
        $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(10,$tmy,$maxlength,$tmy);
        $tmy = $tmy + 5;
        $tmy = $this->checkYaxisoverflow($tmy, $this->land_height);

        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',$this->content_font_size);
            $tmy = $this->generateColumnHeader($tmy, 10, $cn_header);
            $this->SetFont($this->font_name,'',$this->content_font_size);
        }

        $tmx = 10;
        $this->MultiCell(20,3,"Sub Total :",0,'L',0,1,$tmx,$tmy);

        foreach($cn_header as $ky => $li){
            if(isset($li['param']) && in_array($li['param'], array_keys($sub_total))){
                if($sub_total[$li['param']] > 0 && $li['param'] == "order_tsa_pay"){
                    $this->MultiCell($li['width'],3,"$".$this->num_format(abs($sub_total[$li['param']])),0,'R',0,1,$tmx,$tmy);
                }
                else{
                    $this->MultiCell($li['width'],3,"($".$this->num_format(abs($sub_total[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                }
                $tmx += $li['width'];
            }
            else{
                $tmx += $li['width'];
                continue;
            }
        }

        $tmy = $this->checkYaxisoverflow($this->GetY(), $this->land_height);

        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',$this->content_font_size);
            $tmy = $this->generateColumnHeader($tmy, 10, $cn_header);
            $this->SetFont($this->font_name,'',$this->content_font_size);
        }
        
        $this->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->line(10,$tmy,$maxlength,$tmy);
        $tmy = $tmy + 5;
        $tmy = $this->checkYaxisoverflow($tmy, $this->land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',$this->content_font_size);
            $tmy = $this->generateColumnHeader($tmy, 10, $cn_header);
            $this->SetFont($this->font_name,'',$this->content_font_size);
        }
        $tmx = 10;
        $this->SetFont($this->font_name,'B',$this->content_font_size);
        $this->MultiCell(20,3,"Total :",0,'L',0,1,$tmx,$tmy);
        $total_cal["order_gross_premium"] = $total_cal["order_gross_premium"] - $receipt_total + $payment_total; 
        $total_cal["order_cus_pay"] = $total_cal["order_cus_pay"] - $receipt_total + $payment_total; 
        $total_cal["order_tsa_pay"] = $total_cal["order_tsa_pay"] - $receipt_total + $payment_total; 
        
        foreach($header as $ky => $li){
            if(in_array($li['param'], array_keys($total_cal))){
                if($total_cal[$li['param']] > 0){
                    $this->MultiCell($li['width'],3,"$".$this->num_format($total_cal[$li['param']]),0,'R',0,1,$tmx,$tmy);
                }
                else{
                    $this->MultiCell($li['width'],3,"($".$this->num_format(abs($total_cal[$li['param']])).")",0,'R',0,1,$tmx,$tmy);
                }
                $tmx += $li['width'];
            }
            else{
                $tmx += $li['width'];
                continue;
            }
        }
        
        $this->SetFont($this->font_name,'',9);
        $this->SetY($this->GetY()+10);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height - 55);
        $this->Cell(30,5, "Premium",0,0,'L');
        $this->Cell(3,5, ":",0,0,'L');
    
        if($total_cal['order_gross_premium'] > 0){
            $this->Cell(25,5, "$".$this->num_format($total_cal['order_gross_premium']),0,0,'R');
        }else{
            $this->Cell(25,5, "($".$this->num_format(abs($total_cal['order_gross_premium'])).")",0,0,'R');
        }
    
        if($this->cus_id > 0){
            $this->Ln(5);
            $this->checkYaxisoverflow($this->GetY(), $this->land_height);
            $this->Cell(30,5, "Disc Amt",0,0,'L');
            $this->Cell(3,5, ":",0,0,'L');
            if($total_cal['order_direct_discount_amt'] > 0){
                $this->Cell(25,5, "$".$this->num_format($total_cal['order_direct_discount_amt']),0,0,'R');
            }else{
                $this->Cell(25,5, "($".$this->num_format(abs($total_cal['order_direct_discount_amt'])).")",0,0,'R');
            }
        }
    
        if($this->dealer_id > 0){
            $this->Ln(5);
            $this->checkYaxisoverflow($this->GetY(), $this->land_height);
            $this->Cell(30,5, "Referral Fee",0,0,'L');
            $this->Cell(3,5, ":",0,0,'L');
            if($total_cal['order_receivable_dealercomm'] > 0){
                $this->Cell(25,5, "$".$this->num_format($total_cal['order_receivable_dealercomm']),0,0,'R');
            }else{
                $this->Cell(25,5, "($".$this->num_format(abs($total_cal['order_receivable_dealercomm'])).")",0,0,'R');
            }
            $this->Ln(5);
            $this->checkYaxisoverflow($this->GetY(), $this->land_height);
            $this->Cell(30,5, "GST Of Referral Fee",0,0,'L');
            $this->Cell(3,5, ":",0,0,'L');
            if($total_cal['order_receivable_dealergstamount'] > 0){
                $this->Cell(25,5, "$".$this->num_format($total_cal['order_receivable_dealergstamount']),0,0,'R');
            }else{
                $this->Cell(25,5, "($".$this->num_format(abs($total_cal['order_receivable_dealergstamount'])).")",0,0,'R');
            }
        }
    
        $this->Ln(5);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->Cell(30,3, "",0,0,'L');
        $this->Cell(3,3, "",0,0,'L');
        $this->Cell(25,3, str_repeat("-", 20) ,0,0,'R');
        $this->Ln(5);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->SetFont($this->font_name,'B',9);
        $this->Cell(30,3, "Due Payment",0,0,'L');
        $this->Cell(3,3, ":",0,0,'L');
    
        if($this->cus_id > 0){
            if($total_cal['order_cus_pay'] > 0){
                $this->Cell(25,3, "$".$this->num_format($total_cal['order_cus_pay']),0,0,'R');
            }else{
                $this->Cell(25,3, "($".$this->num_format(abs($total_cal['order_cus_pay'])).")",0,0,'R');
            }
        }
        else{
            if($total_cal['order_tsa_pay'] > 0){
                $this->Cell(25,3, "$".$this->num_format($total_cal['order_tsa_pay']),0,0,'R');
            }else{
                $this->Cell(25,3, "($".$this->num_format(abs($total_cal['order_tsa_pay'])).")",0,0,'R');
            }
        }
    
        $this->SetFont($this->font_name,'',9);
        
        $this->Ln(5);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->Cell(30,3, "",0,0,'L');
        $this->Cell(3,3, "",0,0,'L');
        $this->Cell(25,3, str_repeat("=", 12) ,0,0,'R');
        $this->Ln(5);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->Cell(30,5, "Please issue cheque make payable to \"".$this->cprofile_name.'" ( '.$this->cprofile_reg_no.")",0,0,'L');
        $this->Ln(8);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->Cell(30,5, "Thank You.",0,0,'L');
        $this->Ln(8);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->Cell(30,5, $this->cprofile_name,0,0,'L');
        
        $this->Ln(5);
        $this->checkYaxisoverflow($this->GetY(), $this->land_height);
        $this->Cell(30,5, "Co.Reg No.:".$this->cprofile_reg_no,0,0,'L');
        $this->SetY(-15);
        $this->Cell(100,0,'* This is a Computer generated letter, which requires no signature',0,0,'L');
    }

    public function getReceiptData($order_id){
        $receipt_query = ReceiptDetail::select('receipt_detail.recpline_order_amount', 'receipt_detail.recpline_offset_amount AS paid_total', 'receipt.receipt_no', 'receipt.receipt_date', 'receipt.receipt_cheque')
        ->join('receipt', 'receipt.receipt_id', '=', 'receipt_detail.recpline_receipt_id')
        ->where('receipt_detail.recpline_order_id', $order_id)
        ->where('receipt.receipt_status', 1)
        ->where('receipt_detail.recpline_status', 1)
        ->get()->toArray();

        $output = [];

        foreach($receipt_query as $row){
            $output[] = $row;
        }

        return $output;
    }

    public function getPaymentData($order_id, $bill_to = ''){
        $payment_query = Payment::select('payment.payment_cheque', 'payment.payment_method')
        ->join('payment', 'payment.payment_id', '=', 'payment_detail.paymentline_payment_id')
        ->where('payment_detail.paymentline_order_id', $order_id)
        ->where('payment.payment_status', 1)
        ->where('payment_detail.paymentline_status', 1);

        if($this->dealer_id > 0 && $bill_to == 'To Customer'){
            $payment_query->where('payment.payment_type', '!=', 'customer');
        }

        $pay_data = $payment_query->first()->toArray();
        return $pay_data;
    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }

    public function SetAddress(){
        if(in_array($this->filter_by, array('GMM'))){
            $tmy = $this->GetY();
            if($this->cus_id > 0){
                $target_data = $this->getPartnerInfoByID($this->cus_id);
                $this->SetFont('helvetica','B',9);
                $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['partner_name'])),0,'L',0,1,10,$this->GetY());
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
    
                if($target_data['partner_blk_no'] || $target_data['partner_street']){
                    $this->MultiCell(70,0,strtoupper($target_data['partner_blk_no']." ".$target_data['partner_street']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['partner_unit_no']){
                    $this->MultiCell(70,0,strtoupper($target_data['partner_unit_no']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['partner_postal_code']){
                    $this->MultiCell(70,0,strtoupper("SINGAPORE ".$target_data['partner_postal_code']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
            }
            elseif($this->dealer_id > 0){
                $target_data = $this->getDealerInfoByID($this->dealer_id);
                $this->SetFont('helvetica','B',9);
                $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['dealer_name'])),0,'L',0,1,10,$this->GetY());
                $this->SetFont('helvetica','',8);
                $this->Ln(6);
                if($target_data['dealer_blk_no'] || $target_data['dealer_street']){
                    $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['dealer_blk_no']." ".$target_data['dealer_street'])),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['dealer_unit_no']){
                    $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode($target_data['dealer_unit_no'])),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['dealer_postalcode']){
                    $this->MultiCell(70,0,strtoupper(htmlspecialchars_decode("SINGAPORE ".$target_data['dealer_postalcode'])),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
                if($target_data['dealer_payableto']){
                    $this->MultiCell(70,0,htmlspecialchars_decode("Attn. :".$target_data['dealer_payableto']),0,'L',0,1,10,$this->GetY());
                    $this->Ln(3);
                }
            }
            $target_height = $this->GetY();
            $this->SetFont('helvetica','B',8);
            
            $this->MultiCell(60,3,strtoupper("Date :".date('d-M-Y')),0,'L',0,1,140,$tmy + 1);
            $this->MultiCell(60,3,strtoupper($this->soa_no),0,'L',0,1,140,$this->GetY());
            $this->MultiCell(60,3,strtoupper($this->cprofile_name),0,'L',0,1,140,$this->GetY());
            $this->MultiCell(60,3,strtoupper("GST Reg No.".$this->cprofile_reg_no),0,'L',0,1,140,$this->GetY());
            $this->SetFont('helvetica','',8);
            if($target_height > $this->GetY()){
                $this->SetY($target_height);
            }
            $this->Ln(5);
            
        }
    
        return $this->GetY();
    
    }
}