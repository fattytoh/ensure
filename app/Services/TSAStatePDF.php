<?php

namespace App\Services;

use setasign\Fpdi\Fpdi;

class TSAStatePDF extends BasePDF {
    // Page header
    function Header() {
        $this->getLogoUrl();
        $logo_height = 0;
        $show_title = "";
        $this->SetY(5);
        $this->SetFont('helvetica','B',14);
        $this->Cell(100,10, "TSA REFERRAL FEES REPORT",0,0,'L');
        $this->Ln(10);
        $this->SetFont('helvetica','',8);
        $this->Cell(100,3, "Period : ".strtoupper(date('d-M-Y',strtotime($this->period_from)))." - ".strtoupper(date('d-M-Y',strtotime($this->period_to))) ,0,0,'L');
        $this->Ln(3);
        if($this->show_os == "Y"){
            $show_title = "(Show Outstanding Only)";
        }
        $this->Cell(100,3, "Filter By : ".$this->insurance_type_mapping[$this->filter_by]." ".$show_title ,0,0,'L');
        $title_height = $this->GetY();
        if(file_exists($this->cprofile_logo_url)){
            $this->SetY(2);
            $iniy = 6;
            $new_height = 0;
            $new_width = 20;
            list($img_width, $img_height)  = getimagesize($this->cprofile_logo_url);
            $width_scalling_per = (($new_width / $img_width) * 100);
            $new_height = ($img_height/100) * $width_scalling_per;
            $this->Image($this->cprofile_logo_url, 260, $iniy, $new_width, $new_height, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $logo_height = $this->GetY() + $new_height;
        }
        if($logo_height > $title_height){
            $this->line(10,$logo_height + 5,290,$logo_height + 5);
            $this->header_title_height = $logo_height+10 ;
        }
        else{
            $this->line(10,$title_height + 5,290,$title_height + 5);
            $this->header_title_height = $title_height+10 ;
        }
        $this->SetMargins(15,  $this->header_title_height, 15);
    }

    public function renderPDF(){
        // $this->AliasNbPages();
        $this->AddPage("L");
        $this->SetAutoPageBreak(true, 15);
        $this->SetY($this->header_title_height);
        
        $land_height = 170;
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        $maxlength = 260;
        $ini_height = 3;
        
        $this->SetFont($this->font_name,'BU',5);
        if(in_array($this->filter_by,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "No" => array('width'=>8),
                "DN No" => array('width'=>20, 'param'=>'order_no'),
                "Rec. Date" =>array('width'=>20, 'param'=>'order_date'),
                "Veh No" => array('width'=>17, 'param'=>'order_vehicles_no'),
                "Insured Name" => array('width'=>35, 'param'=>'partner_name'),
                "Policy no" => array('width'=>23, 'param'=>'order_policyno'),
                "Gross Premium" => array('width'=>20, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Pmm Receivable" => array('width'=>20, 'param'=>'order_receivable_premiumreceivable', 'number' => true, 'position'=>'R'),
                "Rec Amount" => array('width'=>20, 'param'=>'paid_total', 'number' => true, 'position'=>'R'),
                "Discount" => array('width'=>20, 'param'=>'order_direct_discount_amt', 'number' => true, 'position'=>'R'),
                "TSA %" => array('width'=>20, 'param'=>'order_receivable_dealercommpercent', 'number' => true, 'position'=>'R'),
                "TSA refer fee" => array('width'=>20, 'param'=>'order_receivable_dealercomm', 'number' => true, 'position'=>'R'),
                "GST refer fee" => array('width'=>20, 'param'=>'order_receivable_dealergstamount', 'number' => true, 'position'=>'R'),
                "Net refer fee" => array('width'=>20, 'param'=>'total_tsa_comm', 'number' => true, 'position'=>'R'),
            );
        }
        elseif(in_array($this->filter_by,array("GMM"))){
            $header = array(
                "No" => array('width'=>8),
                "DN No" => array('width'=>20, 'param'=>'order_no'),
                "Rec. Date" =>array('width'=>20, 'param'=>'order_date'),
                "Insured Name" => array('width'=>35, 'param'=>'partner_name'),
                "Maid Name" => array('width'=>20, 'param'=>'order_maid_name'),
                "Policy no" => array('width'=>17, 'param'=>'order_policyno'),
                "Gross Premium" => array('width'=>20, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Pmm Receivable" => array('width'=>20, 'param'=>'order_receivable_premiumreceivable', 'number' => true, 'position'=>'R'),
                "Rec Amount" => array('width'=>20, 'param'=>'paid_total', 'number' => true, 'position'=>'R'),
                "Discount" => array('width'=>20, 'param'=>'order_direct_discount_amt', 'number' => true, 'position'=>'R'),
                "TSA %" => array('width'=>20, 'param'=>'order_receivable_dealercommpercent', 'number' => true, 'position'=>'R'),
                "TSA refer fee" => array('width'=>20, 'param'=>'order_receivable_dealercomm', 'number' => true, 'position'=>'R'),
                "GST refer fee" => array('width'=>20, 'param'=>'order_receivable_dealergstamount', 'number' => true, 'position'=>'R'),
                "Net refer fee" => array('width'=>20, 'param'=>'total_tsa_comm', 'number' => true, 'position'=>'R'),
            );
        }
        else{
            $header = array(
                "No" => array('width'=>8),
                "DN No" => array('width'=>20, 'param'=>'order_no'),
                "Rec. Date" =>array('width'=>20, 'param'=>'order_date'),
                "Coverage" =>array('width'=>20, 'param'=>'order_coverage'),
                "Policy no" => array('width'=>20, 'param'=>'order_policyno'),
                "Insured Name" => array('width'=>35, 'param'=>'partner_name'),
                "Gross Premium" => array('width'=>20, 'param'=>'order_gross_premium', 'number' => true, 'position'=>'R'),
                "Pmm Receivable" => array('width'=>20, 'param'=>'order_receivable_premiumreceivable', 'number' => true, 'position'=>'R'),
                "Rec Amount" => array('width'=>20, 'param'=>'paid_total', 'number' => true, 'position'=>'R'),
                "Discount" => array('width'=>20, 'param'=>'order_direct_discount_amt', 'number' => true, 'position'=>'R'),
                "TSA %" => array('width'=>20, 'param'=>'order_receivable_dealercommpercent', 'number' => true, 'position'=>'R'),
                "TSA refer fee" => array('width'=>20, 'param'=>'order_receivable_dealercomm', 'number' => true, 'position'=>'R'),
                "GST refer fee" => array('width'=>20, 'param'=>'order_receivable_dealergstamount', 'number' => true, 'position'=>'R'),
                "Net refer fee" => array('width'=>20, 'param'=>'total_tsa_comm', 'number' => true, 'position'=>'R'),
            );
        }

        $this->tsa_grand_total = array(
            'order_gross_premium' => 0,
            'order_receivable_premiumreceivable' => 0,
            'paid_total' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'total_tsa_comm' => 0,
        );
        
        if($this->hide_comn == 'Y'){
            unset($header["TSA %"],$header["TSA refer fee"],$header["GST refer fee"]);
            foreach($header as $ky => $li){
                if(!isset($li['number'])){
                    $header[$ky]['width'] = $li['width'] + 3;
                }
            }
        }
        
        
        if($this->dealer_id > 0){
            $dealer_data = $this->getDealerInfoByID($this->dealer_id);
            $this->SetFont('','B',9);
            $this->MultiCell(65,0,strtoupper($dealer_data['dealer_name']),0,'L');
            $this->Ln(2);
            $this->SetFont('','',9);
            if($dealer_data['dealer_blk_no'] || $dealer_data['dealer_street']){
                $this->MultiCell(65,0,strtoupper($dealer_data['dealer_blk_no']." ".$dealer_data['dealer_street']),0,'L');
            }
            if($dealer_data['dealer_unit_no']){
                $this->MultiCell(65,0,strtoupper($dealer_data['dealer_unit_no']),0,'L');
            }
            if($dealer_data['dealer_postalcode']){
                $this->MultiCell(65,0,strtoupper("SINGAPORE ".$dealer_data['dealer_postalcode']),0,'L');
            }
            $this->Ln(3);
        }
        
        foreach($header as $ky => $li){
            $ini_height = $this->checkMulticellHeight($ky, $li['width'], $ini_height);
        }
        $this->Ln(5);        
        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        $tmx = 10;
        $this->SetFont('','B',6);
        $tmy = $this->generateColumnHeader($tmy, 10, $header);
        $this->SetFont($this->font_name,'',6);

        foreach($this->data_listing as $li_ky => $li_data){
            if($this->dealer_id > 0){
                $dealer_data = $this->getDealerInfoByID($li_ky);
            }else{
                $dealer_data = $this->getDealerInfoByID($li_data[0]['order_dealer']);
                if($dealer_data == ""){
                    continue;
                }
            }

            $this->TSABaseReport($li_data,$dealer_data, $header);
        }
    
        
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $tmy = $this->checkYaxisoverflow($this->GetY() + 5, 170);
        $this->Line(10, $tmy, 290, $tmy);
        $tmy = $this->checkYaxisoverflow($tmy + 3, 170);
        $this->MultiCell(25,3,"GRAND TOTAL :",0,'L',0,1,10,$tmy);
        $ini_height = 5;
        $tmx = 10;
        foreach($header as $heli){
            if(isset($heli['param']) && array_key_exists($heli['param'] , $this->tsa_grand_total)){
                if($this->tsa_grand_total[$heli['param']] != 0){
                    if($this->tsa_grand_total[$heli['param']] >=0){
                         $this->MultiCell($heli['width'] + $number_addtional,$ini_height,"$".$this->num_format($this->tsa_grand_total[$heli['param']]),0,'R',0,1,$tmx,$tmy);
                    }
                    else{
                        $this->MultiCell($heli['width'] + $number_addtional,$ini_height,"($".$this->num_format(abs($this->tsa_grand_total[$heli['param']])).")",0,'R',0,1,$tmx,$tmy);
                    }
                }
            }
            $tmx += $heli['width'];
        }
    }

    public function TSABaseReport($info, $dealer, $header){
        $land_height = 170;
        $list_border_style = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0,'color' => array(220, 220, 200)));
        $list_react_color_style= array(220, 220, 200);
        
        $sub_total = array(
            'order_gross_premium' => 0,
            'order_receivable_premiumreceivable' => 0,
            'paid_total' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'total_tsa_comm' => 0,
        );
       
        $this->SetFont($this->font_name,'',7);        
        $this->SetFont($this->font_name,'BUI',7);
        $this->MultiCell(270,5,"TSA :".$dealer['dealer_name'],0,'L',0,1,15,$this->GetY()+3);
        $this->SetFont($this->font_name,'',6);
        
        foreach($info as $in_ky => $in_li){
            $ini_height =3;
            $tmx = 10;            
            foreach($header as $ky => $li){
                $ini_height = $this->checkMulticellHeight($in_li[$li['param']], $li['width'], $ini_height);
                if(isset($li['param']) && array_key_exists($li['param'],$sub_total)){
                    if($li['param'] == 'order_receivable_premiumreceivable'){
                        if($this->show_os){
                            $sub_total[$li['param']] += $in_li['order_outstanding'];
                            $this->tsa_grand_total[$li['param']] += $in_li['order_outstanding'];
                        }else{
                            $sub_total[$li['param']] += $in_li[$li['param']];
                            $this->tsa_grand_total[$li['param']] += $in_li[$li['param']];
                        }
                    }else{
                        $sub_total[$li['param']] += $in_li[$li['param']];
                        $this->tsa_grand_total[$li['param']] += $in_li[$li['param']];
                    }
                }
            }
            
            $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
            if($this->isAddPage){
                $this->SetFont($this->font_name,'BU',6);
                $tmy = $this->generateColumnHeader($tmy,$tmx, $header);
                $this->SetFont($this->font_name,'',6);
            }
            if(($in_ky + 1) % 2 == 0){
                $this->Rect($tmx, $tmy, 280, $ini_height,'DF',$list_border_style,$list_react_color_style);
            }
            
            foreach($header as $ky => $li){
                if(isset($li['param'])){
                    if(isset($li['number'])){
                        if($in_li[$li['param']] != 0){
                            if(in_array($li['param'], array('order_receivable_dealercommpercent'))){
                                $this->MultiCell($li['width'],$ini_height,$this->num_format($in_li[$li['param']]),0,'R',0,1,$tmx,$tmy);
                            }
                            else{
                                if($this->show_os){
                                    if($li['param'] == 'order_receivable_premiumreceivable'){
                                        $this->MultiCell($li['width'],$ini_height,"$".$this->num_format($in_li['order_outstanding']),0,'R',0,1,$tmx,$tmy);
                                    }else{
                                        $this->MultiCell($li['width'],$ini_height,"$".$this->num_format($in_li[$li['param']]),0,'R',0,1,$tmx,$tmy);
                                    }
                                }else{
                                    $this->MultiCell($li['width'],$ini_height,"$".$this->num_format($in_li[$li['param']]),0,'R',0,1,$tmx,$tmy);
                                }
                            }
                        }
                    }
                    else{
                        
                        $this->MultiCell($li['width'],$ini_height,$in_li[$li['param']],0,'L',0,1,$tmx,$tmy);
                    }
                    
                }
                else{
                    $this->MultiCell($li['width'],$ini_height,$in_ky + 1,0,'L',0,1,$tmx,$tmy);
                }
                $tmx += $li['width'];
            }
        }
        $this->Ln(3);
        $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',6);
            $tmy = $this->generateColumnHeader($tmy,10, $header);
            $this->SetFont($this->font_name,'',6);
        }
        $this->Line(10, $tmy, 290, $tmy);
        $this->Ln(3);
        $tmy = $this->checkYaxisoverflow($this->GetY(), $land_height);
        if($this->isAddPage){
            $this->SetFont($this->font_name,'BU',6);
            $tmy = $this->generateColumnHeader($tmy,10, $header);
            $this->SetFont($this->font_name,'',6);
        }
        $this->MultiCell(25,$ini_height,"SUB TOTAL :",0,'L',0,1,10,$tmy);
        $ini_height = 3;
        
        foreach($header as $ky => $li){
            if(isset($li['param']) && isset($sub_total[$li['param']])){
                $ini_height = $this->checkMulticellHeight($sub_total[$li['param']], $li['width'], $ini_height);
            }
        }
        $tmx = 10;
        foreach($header as $ky => $li){
            if(isset($li['param']) &&  array_key_exists($li['param'],$sub_total)){
                if($sub_total[$li['param']] != 0){
                    if($sub_total[$li['param']] > 0){
                        $this->MultiCell($li['width'],$ini_height,"$".$this->num_format($sub_total[$li['param']]),0,'R',0,1,$tmx,$tmy);
                    }
                    else{
                        $this->MultiCell($li['width'],$ini_height,"$".$this->num_format(abs($sub_total[$li['param']])),0,'R',0,1,$tmx,$tmy);
                    }
                }
            }
            $tmx += $li['width'];
        }
    }

    // Page footer
    function Footer() {
        $this->getFooterCompInfo();
        $this->SetY(-15);
        // times italic 8
        $this->SetFont('helvetica','',8);
        // Page number
        if($this->w > 220){
            $length = 280;
        }
        else{
            $length = 190;
        }
        
        $this->setY(-10);
        $this->SetFont('helvetica', 'BI', 8);
        $this->Cell($length,0,$this->comp_name,0,0,'R');
        $this->Ln(4);
        $this->SetFont('helvetica', '', 6);
        $this->Cell($length,0,$this->footer_str,0,0,'R');
    }
}