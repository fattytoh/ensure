<?php

namespace App\Providers;

use App\Http\Controllers\UserGroupPrivilegeController;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //    
        Blade::if('PrivilegeCheck', function (string $value) {
            return UserGroupPrivilegeController::checkPrivilegeWithAuth($value);
        });
        Blade::if('PrivilegeGrpCheck', function (array $value) {
            return UserGroupPrivilegeController::checkPrivilegeGroup($value);
        });
    }
}
