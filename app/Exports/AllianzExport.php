<?php 
namespace App\Exports;

use App\Models\Country;
use App\Models\HiresComp;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AllianzExport implements FromCollection, WithHeadings
{
    use Exportable;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {   
        $count = 1;
        $output = [];
        foreach($this->data as $li){
            $output[] = [
                $count,
                $li['order_date']?date('Y-m-d', strtotime($li['order_date'])):'-',
                '',
                '',
                $li['order_policyno']?:'-',
                $li['dealer_name']?:'-',
                $li['order_tsa_email']?:'-',
                $li['order_promo_code']?:'-',
                $li['order_period_from']?:'-',
                $li['order_period_to']?:'-',
                $li['order_cus_lname']?:'-',
                $li['order_cus_fname']?:'-',
                $li['order_cus_nirc']?:'-',
                $li['order_cus_dob']?date('Y-m-d', strtotime($li['order_cus_dob'])):'-',
                $li['order_cus_gender']?:'-',
                $li['order_cus_unitno']?:'-',
                $li['order_cus_street_name']?:'-',
                $li['order_cus_build_name']?:'-',
                $li['order_cus_postal_code']?:'-',
                $li['order_cus_contact']?:'-',
                $li['order_cus_email']?:'-',
                $li['order_cus_claim']?:'-',
                $li['order_cus_claim_amount']?:'0.00',
                $li['order_vehicles_reg_year']?:'-',
                $li['order_vehicles_pro_year']?:'-',
                $li['order_vehicles_no']?:'-',
                $li['vehicles_name']?:'-',
                $li['order_vehicles_model']?:'-',
                $li['order_vehicles_capacity']?:'-',
                $li['order_vehicles_engine']?:'-',
                $li['order_vehicles_chasis']?:'-',
                $this->HiresComp($li['order_financecode']),
                $li['order_ncd_entitlement']?:'-',
                $li['order_coverage']?$this->getCovertype($li['order_coverage']):'-',
                '',
                $li['order_premium_amt']?:'0',
                $li['order_accident_amt']?:'0',
                $li['order_ncd_dis_amt']?:'0',
                $li['order_in_exp_amt']?:'0',
                $li['order_addtional_amt']?:'0',
                $li['order_subtotal_amount']?:'0',
                $li['order_gst_amount']?:'0',
                $li['order_grosspremium_amount']?:'0',
                $li['driver_lname_1']?:'-',
                $li['driver_fname_1']?:'-',
                $li['driver_nirc_1']?:'-',
                $li['driver_gender_1']?:'-',
                $li['driver_dob_1']?:'-',
                $li['driver_exp_1']?:'0',
                ($li['driver_lname_2'] && $li['driver_nirc_2'])?$li['driver_lname_2']:'-',
                ($li['driver_fname_2'] && $li['driver_nirc_2'])?$li['driver_fname_2']:'-',
                $li['driver_nirc_2']?:'-',
                ($li['driver_gender_2'] && $li['driver_nirc_2'])?$li['driver_gender_2']:'-',
                ($li['driver_dob_2'] && $li['driver_nirc_2'])?$li['driver_dob_2']:'-',
                ($li['driver_exp_2'] && $li['driver_nirc_2'])?$li['driver_exp_2']:'0',
                ($li['driver_lname_3'] && $li['driver_nirc_3'])?$li['driver_lname_3']:'-',
                ($li['driver_fname_3'] && $li['driver_nirc_3'])?$li['driver_fname_3']:'-',
                $li['driver_nirc_3']?:'-',
                ($li['driver_gender_3'] && $li['driver_nirc_3'])?$li['driver_gender_3']:'-',
                ($li['driver_dob_3'] && $li['driver_nirc_3'])?$li['driver_dob_3']:'-',
                ($li['driver_exp_3'] && $li['driver_nirc_3'])?$li['driver_exp_3']:'0',
                ($li['driver_lname_4'] && $li['driver_nirc_4'])?$li['driver_lname_4']:'-',
                ($li['driver_fname_4'] && $li['driver_nirc_4'])?$li['driver_fname_4']:'-',
                $li['driver_nirc_4']?:'-',
                ($li['driver_gender_4'] && $li['driver_nirc_4'])?$li['driver_gender_4']:'-',
                ($li['driver_dob_4'] && $li['driver_nirc_4'])?$li['driver_dob_4']:'-',
                ($li['driver_exp_4'] && $li['driver_nirc_4'])?$li['driver_exp_4']:'0',
                $li['order_excess_amount']?:'0.00',
                '',
                ($li['order_delivery_amt']> 0)?'Yes':'No',
            ];
            $count ++;
        }
        
        return collect($output);
    }

    public function headings(): array
    {
        return [
            'No',
            'Issued Date',
            'Agent Code',
            'Webuser ID',
            'Cover Note Number',
            'TSA name',
            'TSA email',
            'Promo Code',
            'Policy Start Date',
            'Policy End Date',
            'Policyholder Last Name',
            'Policyholder First Name',
            'Policyholder IC',
            'Policyholder DOB',
            'Policyholder Gender',
            'Policyholder Unit No',
            'Policyholder Building Name',
            'Policyholder Street Name',
            'Policyholder Postal Code',
            'Policyholder Mobile',
            'Policyholder Email',
            'Claim In Last Three Years',
            'Total Amount Of Claims',
            'Year of Registration',
            'Year of Manufacture',
            'Vehicle No',
            'Vehicle Make',
            'Vehicle Model',
            'Engine Capacity',
            'Engine No',
            'Chassis No',
            'Hire Purchase',
            'NCD',
            'Coverage',
            'Workshop',
            'Basic Premium',
            'Claim loading Charge',
            'NCD Discount',
            'In Experience Charge',
            'Additional Driver Charge',
            'Total Premium Before GST',
            'GST on Total Premium',
            'Total Premium After GST',
            'Main Driver Last Name',
            'Main Driver First Name',
            'Main Driver IC',
            'Main Driver Gender',
            'Main Driver DOB',
            'Main Driver Driving Experience',
            'Named Driver 1 Last Name',
            'Named Driver 1 First Name',
            'Named Driver 1 IC',
            'Named Driver 1 Gender',
            'Named Driver 1 DOB',
            'Named Driver 1 Driving Experience',
            'Named Driver 2 Last Name',
            'Named Driver 2 First Name',
            'Named Driver 2 IC',
            'Named Driver 2 Gender',
            'Named Driver 2 DOB',
            'Named Driver 2 Driving Experience',
            'Named Driver 3 Last Name',
            'Named Driver 3 First Name',
            'Named Driver 3 IC',
            'Named Driver 3 Gender',
            'Named Driver 3 DOB',
            'Named Driver 3 Driving Experience',
            'Own Damage Excess',
            'Third Parties Liability Excess',
            'Commercial Use',

        ];
    }

    public function getCountryName($code){
        $country = Country::where('country_id', $code)->first();
        if($country){
            return $country->country_code;
        }
        else{
            return '-';
        }
    }

    public function HiresComp($code){
        $hirescomp = HiresComp::where('id', $code)->first();
        if($hirescomp){
            return $hirescomp->comp_name;
        }
        else{
            return '-';
        }
    }

    public function getNRICtype($key){
        $nirc_type = [
            'B' => 'NRIC (Singapore PR)',
            'E' => 'FIN (Employment Pass)',
            'N' => 'NRIC (Singaporean)',
            'W' => 'FIN (Work Permit / S Pass)',
        ];
        if(isset($nirc_type[$key]) && $nirc_type[$key]){
            return $nirc_type[$key];
        }
        else{
            return '-';
        }
    }

    public function getRelationtype($key){
        $relation_type = [
            'I' => 'SELF',
            'F' => 'FRIEND',
            'R' => 'RELATIVE',
            'O' => 'OTHERS',
            'L' => 'SIBLINGS',
            'P' => 'PARENT',
            'S' => 'SPOUSE',
            'C' => 'CHILD',
        ];
        if(isset($relation_type[$key]) && $relation_type[$key]){
            return $relation_type[$key];
        }
        else{
            return '-';
        }
    }

    public function getCovertype($key){
        $cover_type = [
            'COMP' => 'Comprehensive',
            'TPFT' => 'Third Party, Fire & Theft',
            'TPO' => 'Third Party',
        ];
        if(isset($cover_type[$key]) && $cover_type[$key]){
            return $cover_type[$key];
        }
        else{
            return '-';
        }
    }
}