<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    //
    public function Login(Request $request) {
   
        $rules = [
            'email'    => 'required',
            'password'    => 'required',
        ];
        $msg = [
            'email.required' => "Email is required",
            'password.required' => "Password is required",
        ];
        $request->validate($rules, $msg);

        $loginusr = User::where('email', $request->input('email'))
        ->where('status','1')
        ->first();
        if($loginusr){
            if($loginusr->cprofile == env("APP_ID") || $loginusr->cprofile == 1){
                if(Hash::check($request->input('password'), $loginusr->password) ){
                    Auth::login($loginusr);
                    return redirect()->intended('');
                }
                else{
                    Auth::logout();
                    return redirect()->back()->withErrors(['message' => 'Invalid login credentials.']);
                }
            }
            else{
                Auth::logout();
                return redirect()->back()->withErrors(['message' => 'Invalid login credentials.']);
            }
        }
        else {
            Auth::logout();
            return redirect()->back()->withErrors(['message' => 'Invalid login credentials.']);
        }
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('login');
    }

    public function switchSite(){
        if(Auth::user()->cprofile == 1){
            $token = [
                'user_id' => Auth::user()->id,
                'time' => strtotime(Carbon::now()),
            ];

            $token = json_encode($token);
            $enc_token = Crypt::encryptString($token);

            $link = env('SWITCH_URL').'/'.'switchLogin/'.$enc_token;

            return redirect()->away($link);
            
        }
        else{
            return redirect()->route('dashboard');
        }
    }

    public function switchLogin(Request $request){
        if($request->route('token')){
            $dec_token = Crypt::decryptString($request->route('token'));
            $token = json_decode($dec_token, true);
            if(isset($token['user_id'])){
                $left_time = strtotime(Carbon::now()) - $token['time'];
                if($left_time > 600){
                    Auth::logout();
                    return redirect()->route('login');
                }
                else{
                    if(Auth::user()){
                        if(Auth::user()->id != $token['user_id']){
                            Auth::logout();
                            Auth::loginUsingId($token['user_id']);
                        }
                        return redirect()->route('dashboard');
                    }
                    else{
                        Auth::loginUsingId($token['user_id']);
                        return redirect()->route('dashboard');
                    }
                }
            }
            else{
                Auth::logout();
                return redirect()->route('login');
            }
        }
        else{
            Auth::logout();
            return redirect()->route('login');
        }
        
    }
}
