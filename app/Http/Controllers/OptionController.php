<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Option;
use Illuminate\Support\Facades\Auth;

class OptionController extends Controller
{
    //
    public $motor_gst_key = "MI_GST";
    public $maid_gst_key = "GM_GST";
    public $general_gst_key = "GI_GST";

    public function index(){
        $data = [
            'motor_gst' => $this->getMotorGST(),
            'maid_gst' => $this->getMaidGST(),
            'general_gst' => $this->getGeneralGST(),
        ];
        return view('option.Index', $data);
    }

    public function update(Request $request){
        if($request->input('motor_gst')){
            Option::updateOrCreate(
                ['option_key' => $this->motor_gst_key, 'option_cprofile' => env('APP_ID')],
                ['option_value' => $request->input('motor_gst'), 'updateBy'=> Auth::user()->id, 'insertBy' => Auth::user()->id]
            );
        }

        if($request->input('maid_gst')){
            Option::updateOrCreate(
                ['option_key' => $this->maid_gst_key, 'option_cprofile' => env('APP_ID')],
                ['option_value' => $request->input('maid_gst'), 'updateBy'=> Auth::user()->id, 'insertBy' => Auth::user()->id]
            );
        }

        if($request->input('general_gst')){
            Option::updateOrCreate(
                ['option_key' => $this->general_gst_key, 'option_cprofile' => env('APP_ID')],
                ['option_value' => $request->input('general_gst'), 'updateBy'=> Auth::user()->id, 'insertBy' => Auth::user()->id]
            );
        }

        return redirect()->back()->with('sweet_success_msg','Update Success');
    }

    public function getMotorGST(){
        $gst = Option::where("option_cprofile", env('APP_ID'))
        ->where("option_key", $this->motor_gst_key)
        ->first();

        return isset($gst->option_value)?$gst->option_value:0;
    }

    public function getMaidGST(){
        $gst = Option::where("option_cprofile", env('APP_ID'))
        ->where("option_key", $this->maid_gst_key)
        ->first();

        return isset($gst->option_value)?$gst->option_value:0;
    }

    public function getGeneralGST(){
        $gst = Option::where("option_cprofile", env('APP_ID'))
        ->where("option_key", $this->general_gst_key)
        ->first();

        return isset($gst->option_value)?$gst->option_value:0;
    }
}
