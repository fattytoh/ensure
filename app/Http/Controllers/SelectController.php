<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TSA;
use App\Models\Vehicles;
use App\Models\HiresComp;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class SelectController extends Controller
{
    //
    public function getTSA(Request $request){
        $search = $request->input('term');
        $output = [];
        $sql_data = TSA::select("dealer_code", "dealer_name", "dealer_id")
        ->where(
            function ($query) use ($search) {
                $query->where('dealer_code', 'LIKE', "%".$search."%")
                      ->orWhere('dealer_name', 'LIKE', "%".$search."%");
            }
        )
        ->where('dealer_status', 1)
        ->take(10)->get()->toArray();
        foreach($sql_data as $sli){
            $output[] = [
                'id' => $sli['dealer_id'],
                'text' => $sli['dealer_code']." - ".$sli['dealer_name'],
            ];
        }

        return response()->json([
            'results' => $output
        ], 200); 
    }

    public function getVehicles(Request $request){
        $search = $request->input('term');
        $output = [];
        $sql_data = Vehicles::select("id", "name")
        ->where('name', 'LIKE', "%".$search."%")
        ->take(10)->get()->toArray();
        foreach($sql_data as $sli){
            $output[] = [
                'id' => $sli['id'],
                'text' => $sli['name'],
            ];
        }

        return response()->json([
            'results' => $output
        ], 200); 
    }

    public function getHiresComp(Request $request){
        $search = $request->input('term');
        $output = [];
        $sql_data = HiresComp::select("id", "comp_name")
        ->where('comp_name', 'LIKE', "%".$search."%")
        ->where('status', '1')
        ->take(10)->get()->toArray();
        foreach($sql_data as $sli){
            $output[] = [
                'id' => $sli['id'],
                'text' => $sli['comp_name'],
            ];
        }

        return response()->json([
            'results' => $output
        ], 200); 
    }

    public function getCustomer(Request $request){
        $search = $request->input('term');
        $output = [];
        $sql_data = Customer::select("partner_nirc", "partner_name", "partner_id")
        ->where(
            function ($query) use ($search) {
                $query->where('partner_nirc', 'LIKE', "%".$search."%")
                      ->orWhere('partner_name', 'LIKE', "%".$search."%");
            }
        )
        ->where('partner_status', 1)
        ->take(10)->get()->toArray();
        foreach($sql_data as $sli){
            $output[] = [
                'id' => $sli['partner_id'],
                'text' => $sli['partner_name']." - ".substr_replace($sli['partner_nirc'],'XXXX', 1,-4),
            ];
        }

        return response()->json([
            'results' => $output
        ], 200); 
    }

    public function getCustomerDetails(Request $request){
        $search = $request->input('partner_id');
        $sql_data = Customer::select("partner_nirc", "partner_name")
        ->where('partner_id', $search)->first();
        if($sql_data){
            return response()->json([
                'results' => [
                    'name' => $sql_data->partner_name,
                    'nric' => substr_replace($sql_data->partner_nirc,'XXXX', 1,-4),
                ]
            ], 200); 
        }
        else{
            return response()->json([
                'results' => []
            ], 200); 
        }
        
    }

    public function getTSAOption($id){
        $html = '';
        $sql_data = TSA::select("dealer_code", "dealer_name", "dealer_id")
        ->where('dealer_id', $id)
        ->where('dealer_status', 1)
        ->first();

        if($sql_data){
            $html .= '<option value="'.$id.'" SELECTED >'.$sql_data->dealer_code." - ".$sql_data->dealer_name.'</option>';
        }

        return $html;
    }

    public function getCustomerOption($id){
        $html = '';
        $sql_data = Customer::select("partner_nirc", "partner_name", "partner_id")
        ->where('partner_id', $id)
        ->where('partner_status', 1)
        ->first();

        if($sql_data){
            $html .= '<option value="'.$id.'" SELECTED >'.$sql_data->partner_name." - ".substr_replace($sql_data->partner_nirc,'XXXX', 1,-4).'</option>';
        }

        return $html;
    }
}
