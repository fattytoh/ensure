<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\General;
use App\Models\Worker;
use App\Models\Order;
use App\Models\Country;
use App\Models\Customer;
use App\Models\TSA;
use App\Models\User;
use App\Models\Insuranceco;
use App\Models\InsurancePlan;
use App\Models\Refno;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GeneralInsuranceController extends Controller
{
    //
    public $prefix = "GI";

    public function getListing(Request $request){
        return view('general.List');
    }

    public function getListData(Request $request){
         // Total records
        $order_fields = ['','order_no','order_policyno','partner_name','order_date', ''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $count_query = Order::select('count(*) as allcount')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_prefix_type',$this->prefix)
        ->where('order.order_status', "!=","0");

        $main_query = Order::select('order.*', "customer.partner_name", "insurance_general.order_policyno")
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_prefix_type',$this->prefix)
        ->where('order.order_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('order.order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('insurance_general.order_policyno', 'like', '%' . $searchValue . '%')
                ->orWhere('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('order.order_date', 'like', '%' . $searchValue . '%');
        });

        if($request->input('order_policyno')){
            $main_query->Where('insurance_general.order_policyno', 'like', '%' . $request->input('order_policyno') . '%');
            $count_query->Where('insurance_general.order_policyno', 'like', '%' . $request->input('order_policyno'). '%');
        }

        if($request->input('order_period_from')){
            $main_query->Where('order.order_date', '>=', $request->input('order_period_from'));
            $count_query->Where('order.order_date', '>=', $request->input('order_period_from'));
        }

        if($request->input('order_period_to')){
            $main_query->Where('order.order_date', '<=', $request->input('order_period_to'));
            $count_query->Where('order.order_date', '<=', $request->input('order_period_to'));
        }

        if($request->input('order_cvnote')){
            $main_query->Where('insurance_general.order_cvnote', 'like', '%' . $request->input('order_cvnote') . '%');
            $count_query->Where('insurance_general.order_cvnote', 'like', '%' . $request->input('order_cvnote'). '%');
        }

        if($request->input('order_no')){
            $main_query->Where('order.order_no', 'like', '%' . $request->input('order_no') . '%');
            $count_query->Where('order.order_no', 'like', '%' . $request->input('order_no'). '%');
        }

        if($request->input('dealer_name')){
            $main_query->Where('tsa.dealer_name', 'like', '%' . $request->input('dealer_name') . '%');
            $count_query->Where('tsa.dealer_name', 'like', '%' . $request->input('dealer_name'). '%');
        }
        
        if($request->input('partner_nirc')){
            $main_query->Where('customer.partner_nirc', 'like', '%' . $request->input('partner_nirc') . '%');
            $count_query->Where('customer.partner_nirc', 'like', '%' . $request->input('partner_nirc'). '%');
        }

        if($request->input('partner_name')){
            $main_query->Where('customer.partner_name', 'like', '%' . $request->input('partner_name') . '%');
            $count_query->Where('customer.partner_name', 'like', '%' . $request->input('partner_name'). '%');
        }

        if($request->input('insertDateTime')){
            $main_query->Where('order.created_at', $request->input('partner_name'));
            $count_query->Where('order.created_at', $request->input('partner_name'));
        }

        $totalRecords = $count_query->count();
        $totalRecordswithFilter = $count_query
        ->where(function ($query) use ($searchValue) {
            $query->where('order.order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('insurance_general.order_policyno', 'like', '%' . $searchValue . '%')
                ->orWhere('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('order.order_date', 'like', '%' . $searchValue . '%');
        })
        ->count();

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }

        $listing_data = $main_query
        ->skip($start)
        ->take($rowperpage)
        ->get()->toArray();

        $out_data = [];

        foreach($listing_data as $ky => $li){
            $action = '';

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("GeneralInfo")){
                $action .= '<a href="'.route('GeneralInfo', ['id' => $li['order_id'] ]).'" class="btn btn-warning">Edit</a>';
            }

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("GeneralDelete")){
                $action .= '<a href="'.route('GeneralDelete', ['id' => $li['order_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['order_no'].'">Delete</a>';
            }
            
            $out_data[] = [
                $ky + 1,
                $li['order_no']?:'-',
                $li['order_policyno'],
                $li['partner_name']?:'-',
                $li['order_date']?:'-',
                $action
            ];
        }
       
        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200);
    }

    public function GeneralInfo(Request $request){
        $opt = new OptionController();
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();

        $insuranceplan_list = InsurancePlan::select("insuranceclass_id", "insuranceclass_code")->where("insuranceclass_status", 1)->where("insuranceclass_cprofile", env('APP_ID'))->get()->toArray();

        if($request->route('id')){
            $info_data = $this->getGeneralInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                return view('general.Info', $info_data);
            }
            else{
                return redirect()->route('GeneralList');
            }
        }
        else{
            $data = [
                'country_list'=> $country_list,
                'gov_gst' =>  $opt->getGeneralGST(),
                'insuranceco_list'=> $insuranceco_list,
                'insuranceplan_list'=> $insuranceplan_list,
                'min_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(70))),
                'max_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(18))),
                'min_year' => date('Y', strtotime(Carbon::now()->subYears(30))),
            ];
            return view('general.Add', $data);
        }
    }

    public function getGeneralInfo($order_id){
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();

        $insuranceplan_list = InsurancePlan::select("insuranceclass_id", "insuranceclass_code")->where("insuranceclass_status", 1)->where("insuranceclass_cprofile", env('APP_ID'))->get()->toArray();

        $data = Order::select('order.*', 'insurance_general.*' )
        ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
        ->where('order.order_id', $order_id)
        ->where('order.order_status', "!=","0")
        ->where('order.order_prefix_type',$this->prefix)
        ->first();

        if(isset($data->order_id)){
            $exist_dealer = TSA::find($data->order_dealer);
            $exist_customer = Customer::find($data->order_customer);
            $add_name = User::find($data->insertBy);
            $up_name = User::find($data->updateBy);
            $wrk_list = $this->getWorkerList($data->order_id);
            $data->update_name =  $up_name->getName()?:'Webmaster';
            $data->insert_name =  $add_name->getName()?:'Webmaster';
            $data->dealer_id = $data->order_dealer;
            $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
            $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
            $data->customer_name = isset($exist_customer->partner_name)?$exist_customer->partner_name:'';
            $data->customer_ic = isset($exist_customer->partner_nirc)?substr_replace($exist_customer->partner_nirc,'XXXX', 1,-4):'';
            $data->financecode_name = isset($exist_hires->id)?$exist_hires->comp_name:'';
            $data->min_age = date('Y-m-d', strtotime(Carbon::now()->subYears(70)));
            $data->max_age = date('Y-m-d', strtotime(Carbon::now()->subYears(18)));
            $data->min_year = date('Y', strtotime(Carbon::now()->subYears(30)));
            $data->insuranceplan_list = $insuranceplan_list;
            $data->country_list = $country_list;
            $data->insuranceco_list = $insuranceco_list;
            $data->worker_list = $wrk_list;
            $file_attach = new FilesController();
            $data->file_list = $file_attach->getFilesList('ens_order',$data->order_id);
            $receipt = new ReceiptController();
            $data->receipt_list = $receipt->getOrderReceipt($data->order_id);
            $pay = new PaymentController();
            $data->payment_list = $pay->getOrderPayment($data->order_id);
            $data->record_list = $this->getRelatedRecord($data->order_id);
            $data->total_worker = count($wrk_list)?:0;
                        
            return $data;
        }

    }

    public function GeneralRenewal(Request $request){
        if($request->route('id')){
            $info_data = $this->getGeneralInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                $info_data->order_no = "-- System Generate --";
                $info_data->order_ref = 0;
                $info_data->order_renewal_ref = $info_data->order_id;
                $info_data->order_premium_amt = 0;
                $info_data->order_addtional_amt = 0;
                $info_data->order_subtotal_amount = 0;
                $info_data->order_direct_discount_amt = 0;
                $info_data->order_direct_discount_percentage = 0;
                $info_data->order_bank_interest = 0;
                $info_data->order_grosspremium_amount = 0;
                $info_data->order_receivable_dealercomm = 0;
                $info_data->order_receivable_dealergstamount = 0;
                $info_data->order_receivable_gstamount = 0;
                $info_data->order_receivable_premiumreceivable = 0;
                $info_data->order_payable_ourcomm = 0;
                $info_data->order_payable_nettcomm = 0;
                $info_data->order_payable_gst = 0;
                $info_data->order_payable_premium = 0;
                $info_data->order_date = date('Y-m-d');
                $info_data->order_period_from = date('Y-m-d');
                $info_data->order_period_to = date('Y-m-d', strtotime(date('Y-m-d'). " +1 year"));
                $info_data->order_period_to = date('Y-m-d', strtotime($info_data->order_period_to. " -1 day"));

                return view('general.Renewal', $info_data);
            }
            else{
                return redirect()->route('GeneralList');
            }
        }
        else{
            return redirect()->route('GeneralList');
        }
    }

    public function GeneralEndorsement(Request $request){
        if($request->route('id')){
            $info_data = $this->getGeneralInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                $info_data->order_no = "-- System Generate --";
                $info_data->order_ref = $info_data->order_id;
                $info_data->order_renewal_ref = 0;
                $info_data->order_premium_amt = 0;
                $info_data->order_addtional_amt = 0;
                $info_data->order_subtotal_amount = 0;
                $info_data->order_direct_discount_amt = 0;
                $info_data->order_direct_discount_percentage = 0;
                $info_data->order_bank_interest = 0;
                $info_data->order_grosspremium_amount = 0;
                $info_data->order_receivable_dealercomm = 0;
                $info_data->order_receivable_dealergstamount = 0;
                $info_data->order_receivable_gstamount = 0;
                $info_data->order_receivable_premiumreceivable = 0;
                $info_data->order_payable_ourcomm = 0;
                $info_data->order_payable_nettcomm = 0;
                $info_data->order_payable_gst = 0;
                $info_data->order_payable_premium = 0;
                $info_data->order_date = date('Y-m-d');
                $info_data->order_period_from = date('Y-m-d');
                $info_data->order_period_to = date('Y-m-d', strtotime(date('Y-m-d'). " +1 months"));
                $info_data->order_period_to = date('Y-m-d', strtotime($info_data->order_period_to. " -1 day"));

                return view('general.Endorsement', $info_data);
            }
            else{
                return redirect()->route('GeneralList');
            }
        }
        else{
            return redirect()->route('GeneralList');
        }
    }

    public function create(Request $request){
        $rules = [
            'order_record_billto' => 'required',
            'order_period_from' => 'required|date|after_or_equal:today',
            'order_period_to' => 'required|date|after:order_period_from',
            'order_date' => 'required|date',
            'order_insurco' => 'required',
            'order_coverage' => 'required',
            'order_premium_amt' => 'required|gt:0',
        ];

        $attr = [
            'order_record_billto' => 'Bill To',
            'order_period_from' => 'Coverage Period From',
            'order_period_to' => 'Coverage Period To',
            'order_date' => 'Issue Date',
            'order_insurco' => 'Insurance Company',
            'order_coverage' => 'Coverage Package',
        ];

        $request->validate($rules, [], $attr);

        $errors_input = [];

        $files_allow = ['jpg','bmp','png','pdf'];

        if ($request->hasFile('order_files_attach')) {
            foreach($request->file('order_files_attach') as $key => $fli){
                if($fli->getSize() > 5242880 ){
                    $errors_input['order_files_attach'] = 'Attachment Cannot over 5MB';
                }
                elseif(!in_array($fli->extension(),$files_allow)){
                    $errors_input['order_files_attach'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                }
            }
        }

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $order_ctrl = new OrderController();
        $new_order_id = $order_ctrl->create($request, $this->prefix);

        if($new_order_id){
            $new_general = new General();
            $new_general->order_record_billto = $request->input('order_record_billto');
            $new_general->order_policyno = $request->input('order_policyno');
            $new_general->order_coverage = $request->input('order_coverage');
            $new_general->order_period_from = $request->input('order_period_from');
            $new_general->order_period_to = $request->input('order_period_to');
            $new_general->order_cancel_date = $request->input('order_cancel_date');
            $new_general->order_ins_doc_no = $request->input('order_ins_doc_no');
            $new_general->order_description = $request->input('order_description');
            $new_general->order_notes_remark = $request->input('order_notes_remark');
            $new_general->order_workername_display = $request->input('order_workername_display');
            $new_general->order_workpermit_display = $request->input('order_workpermit_display');
            $new_general->order_workerdob_display = $request->input('order_workerdob_display');
            $new_general->order_workeroccup_display = $request->input('order_workeroccup_display');
            $new_general->order_workerwages_display = $request->input('order_workerwages_display');
            $new_general->order_workerstatus_display = $request->input('order_workerstatus_display');
            $new_general->order_workereffdate_display = $request->input('order_workereffdate_display');
            $new_general->order_id = $new_order_id;
            $new_general->insertBy = Auth::user()->id;
            $new_general->updateBy = Auth::user()->id;
            $new_general->save();

            if($request->input('worker_list')){
                foreach($request->input('worker_list') as $list_data){
                    $this->insertWorker($list_data, $new_order_id);
                }
            }

            if($request->hasFile('order_files_attach')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_order',$new_order_id,'uploads/general','order_files_attach');
            }
        }
        else{
            return redirect()->route('GeneralList')->with('error_msg', 'System Error, Please Contact Admin');
        }

        return redirect()->route('GeneralList')->with('success_msg', 'Add Case Succesfully');
    }

    public function update(Request $request){
        $exist_order = Order::where('order_id', $request->input('order_id'))
            ->where('order_status', "!=","0")
            ->first();
        $exist_general = General::where('order_id', $request->input('order_id'))
            ->first();

        if(!$exist_order || !$exist_general){
            return redirect()->route('MaidList')->with('error_msg', 'Invalid Case');
        }
        else{
            $rules = [
                'order_record_billto' => 'required',
                'order_period_from' => 'required|date|after_or_equal:today',
                'order_period_to' => 'required|date|after:order_period_from',
                'order_date' => 'required|date',
                'order_insurco' => 'required',
                'order_coverage' => 'required',
                'order_premium_amt' => 'required|gt:0',
            ];

            $attr = [
                'order_record_billto' => 'Bill To',
                'order_period_from' => 'Coverage Period From',
                'order_period_to' => 'Coverage Period To',
                'order_date' => 'Issue Date',
                'order_insurco' => 'Insurance Company',
                'order_coverage' => 'Coverage Package',
            ];

            $request->validate($rules, [], $attr);

            $errors_input = [];

            $files_allow = ['jpg','bmp','png','pdf'];

            if ($request->hasFile('order_files_attach')) {
                foreach($request->file('order_files_attach') as $key => $fli){
                    if($fli->getSize() > 5242880 ){
                        $errors_input['order_files_attach'] = 'Attachment Cannot over 5MB';
                    }
                    elseif(!in_array($fli->extension(),$files_allow)){
                        $errors_input['order_files_attach'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                    }
                }
            }

            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }
            
            $order_ctrl = new OrderController();
            $order_ctrl->update($request);

            $exist_general->order_record_billto = $request->input('order_record_billto');
            $exist_general->order_policyno = $request->input('order_policyno');
            $exist_general->order_coverage = $request->input('order_coverage');
            $exist_general->order_period_from = $request->input('order_period_from');
            $exist_general->order_period_to = $request->input('order_period_to');
            $exist_general->order_cancel_date = $request->input('order_cancel_date');
            $exist_general->order_ins_doc_no = $request->input('order_ins_doc_no');
            $exist_general->order_description = $request->input('order_description');
            $exist_general->order_notes_remark = $request->input('order_notes_remark');
            $exist_general->order_workername_display = $request->input('order_workername_display');
            $exist_general->order_workpermit_display = $request->input('order_workpermit_display');
            $exist_general->order_workerdob_display = $request->input('order_workerdob_display');
            $exist_general->order_workeroccup_display = $request->input('order_workeroccup_display');
            $exist_general->order_workerwages_display = $request->input('order_workerwages_display');
            $exist_general->order_workerstatus_display = $request->input('order_workerstatus_display');
            $exist_general->order_workereffdate_display = $request->input('order_workereffdate_display');
            $exist_general->updateBy = Auth::user()->id;
            $exist_general->save();

            if($request->input('worker_list')){
                Worker::where('order_id', $exist_order->order_id)->update(['status' => 0]);
                foreach($request->input('worker_list') as $list_data){
                    $this->updateWorker($list_data, $exist_order->order_id);
                }
            }

            if($request->hasFile('order_files_attach')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_order',$exist_order->order_id,'uploads/general','order_files_attach');
            }
            
            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $exist_order = Order::find($request->route('id'));
        if($exist_order){
            $exist_order->order_status = "0";
            $exist_order->save();
        }
    
        return redirect()->route('GeneralList')->with('success_msg', 'Delete Case Succesfully');
    }

    public function getWorkerList($order_id){

        $data = Worker::where("order_id", $order_id)->where('status', 1)->get()->toArray();

        return $data;
    }

    public function insertWorker($input_data, $order_id){
        $new_worker = new Worker();
        $new_worker->worker_display = isset($input_data['worker_display'])?$input_data['worker_display']:null;
        $new_worker->worker_name = isset($input_data['worker_name'])?$input_data['worker_name']:null;
        $new_worker->worker_permit = isset($input_data['worker_permit'])?$input_data['worker_permit']:null;
        $new_worker->worker_dob = isset($input_data['worker_dob'])?$input_data['worker_dob']:null;
        $new_worker->worker_occupation = isset($input_data['worker_occupation'])?$input_data['worker_occupation']:null;
        $new_worker->worker_wages = isset($input_data['worker_wages'])?$input_data['worker_wages']:null;
        $new_worker->worker_status = isset($input_data['worker_status'])?$input_data['worker_status']:null;
        $new_worker->worker_effdate = isset($input_data['worker_effdate'])?$input_data['worker_effdate']:null;
        $new_worker->status = 1;
        $new_worker->order_id = $order_id;
        $new_worker->insertBy = Auth::user()->id;
        $new_worker->updateBy = Auth::user()->id;
        $new_worker->save();
    }

    public function updateWorker($input_data, $order_id){
        $exist_worker = Worker::find($input_data['worker_id']);
        if($exist_worker){
            $exist_worker->worker_display = isset($input_data['worker_display'])?$input_data['worker_display']:null;
            $exist_worker->worker_name = isset($input_data['worker_name'])?$input_data['worker_name']:null;
            $exist_worker->worker_permit = isset($input_data['worker_permit'])?$input_data['worker_permit']:null;
            $exist_worker->worker_dob = isset($input_data['worker_dob'])?$input_data['worker_dob']:null;
            $exist_worker->worker_occupation = isset($input_data['worker_occupation'])?$input_data['worker_occupation']:null;
            $exist_worker->worker_wages = isset($input_data['worker_wages'])?$input_data['worker_wages']:null;
            $exist_worker->worker_status = isset($input_data['worker_status'])?$input_data['worker_status']:null;
            $exist_worker->worker_effdate = isset($input_data['worker_effdate'])?$input_data['worker_effdate']:null;
            $exist_worker->status = 1;
            $exist_worker->updateBy = Auth::user()->id;
            $exist_worker->save();
        }
        else{
            $this->insertWorker($input_data, $order_id);
        }
    }

    public function getRelatedRecord($order_id){
        $output = [];
        $current_order = Order::select("order_ref", "order_renewal_ref")
        ->where('order_status', 1)
        ->where('order_id', $order_id)
        ->first();

        $main_order = 0;

        if($current_order->order_ref > 0){
            $main_order = $current_order->order_ref;
        }
        elseif($current_order->order_renewal_ref > 0){
            $main_order = $current_order->order_renewal_ref;
        }
        
        $main_query = Order::select('order.*', "customer.partner_name", "insurance_general.order_coverage", "insurance_general.order_period_from", "insurance_general.order_period_to", "insurance_general.order_policyno")
        ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->where('order.order_status', 1)
        ->where(function ($query) use ($order_id, $main_order) {
            $query->where('order.order_ref',  $order_id )
                ->orWhere('order.order_renewal_ref', $order_id)
                ->orWhere('order.order_id', $main_order);
        })->get()->toArray();

        foreach($main_query as $mli){
            $order_no = '';

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("GeneralInfo")){
                $order_no = '<a href="'.route('GeneralInfo', ['id' => $mli['order_id'] ]).'" target="_blank">'.$mli['order_no'].'</a>';
            }
            else{
                $order_no = $mli['order_no'];
            }

            $plan = InsurancePlan::select("insuranceclass_code")->where("insuranceclass_id", $mli['order_coverage'])->first();
            $output[] = [
                'order_id' => $mli['order_id'],
                'order_no' => $order_no,
                'order_doc_type' => $mli['order_doc_type'],
                'order_grosspremium_amount' => $mli['order_grosspremium_amount'],
                'order_coverage' => $plan->insuranceclass_code?:"-",
                'partner_name' => $mli['partner_name'],
                'order_period_from' => $mli['order_period_from'],
                'order_period_to' => $mli['order_period_to'],
                'order_policyno' => $mli['order_policyno'],
            ];
        }

        return $output;
    }
}