<?php

namespace App\Http\Controllers;

use App\Models\GroupPrivilege;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserGroupController extends Controller
{
    //
    public function getListing(){
        $listing_data = Group::select('*')
        ->where('group_status', "!=","0")
        ->where('group_cprofile', env('APP_ID'))
        ->get()->toArray();

        $data = [
            'list_data' => $listing_data,
        ];
        return view('group.list', $data);
    }

    public function getListData(Request $request){

    }

    public function GroupInfo(Request $request){
        $new_pri = new UserGroupPrivilegeController();
        $privilege_list =  $new_pri->getPrivilegeList();
        if($request->route('id')){
            $routelist = [];
            $grp_per = GroupPrivilege::select('route_name')
            ->where('group_id', $request->route('id'))
            ->where('status', 'ACTIVE')
            ->get()->toArray();
            if($grp_per){
                $routelist = array_column($grp_per, 'route_name');
            }
            $data = Group::find($request->route('id'));
            $data->privilege_list = $privilege_list;
            $data->routelist = $routelist;
            return view('group.info', $data);
        }
        else{
            $data = [
                'privilege_list' =>  $privilege_list,
            ];
            return view('group.add', $data);
        }
    }

    public function create(Request $request){
        $rules = [
            'group_code'  => 'required|unique:group,group_code',
        ];

        $attr = [
            'group_code' => 'Group Code',
        ];

        $request->validate($rules, [], $attr);

        $new_usrgrp = new Group();
        $new_usrgrp->group_code = $request->input('group_code');
        $new_usrgrp->group_type = $request->input('group_type');
        $new_usrgrp->group_desc = $request->input('group_desc');
        $new_usrgrp->group_cprofile = env('APP_ID');
        $new_usrgrp->group_status = 1;
        $new_usrgrp->insertBy = Auth::user()->id;
        $new_usrgrp->updateBy = Auth::user()->id;
        $new_usrgrp->save();
        $pri_list = $request->input('privilege_box');
        
        foreach($pri_list as $li){
            GroupPrivilege::updateOrCreate(
                [
                    'group_id' => $new_usrgrp->group_id,
                    'route_name' => $li,
                ],
                [
                    'status' => 'ACTIVE',
                    'insertBy' => Auth::user()->id,
                    'updateBy' => Auth::user()->id,
                ]
            );
        }

        return redirect()->route('UserGroupList')->with('success_msg', 'Add Group Succesfully');
    }

    public function update(Request $request){
        $exist_grp = Group::find($request->input('group_id'));
        if(!$exist_grp || $exist_grp->group_status == '0'){
            return redirect()->route('UserGroupList')->with('error_msg', 'Invalid Group');
        }
        else{
            $rules = [
                'group_code'  => 'required|unique:group,group_code,'.$request->input('group_id'),
            ];
    
            $attr = [
                'group_code' => 'Group Code',
                'group_id' => 'Group',
            ];
    
            $request->validate($rules, [], $attr);
    
            $errors_input = [];
            
            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }
    
            DB::table("ens_group_privilege")
            ->where('group_id' , $request->input('group_id'))
            ->update(['status' => 'INACTIVE']);
    
            $pri_list = $request->input('privilege_box');
            
            foreach($pri_list as $li){
                GroupPrivilege::updateOrCreate(
                    [
                        'group_id' => $request->input('group_id'),
                        'route_name' => $li,
                    ],
                    [
                        'status' => 'ACTIVE',
                        'updateBy' => Auth::user()->id,
                    ]
                );
            }

            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $exist_grp = Group::find($request->input('group_id'));
        if($exist_grp){
            $exist_grp->group_status = 0;
            $exist_grp->updateBy = Auth::user()->id;
            $exist_grp->save();
            return redirect()->route('UserGroupList')->with('success_msg', 'Delete Group Succesfully');
        }
        else{
            return redirect()->route('UserGroupList')->with('error_msg', 'Invalid Group');
        }
    }
}
