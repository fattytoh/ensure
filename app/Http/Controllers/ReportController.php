<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Motor;
use App\Models\Maid;
use App\Models\General;
use App\Models\Order;
use App\Models\Country;
use App\Models\Customer;
use App\Models\TSA;
use App\Models\User;
use App\Models\Insuranceco;
use App\Models\HiresComp;
use App\Models\Refno;
use PDF;

class ReportController extends Controller
{
    //
    public function tsastatementForm(){
        return view('report.tsaform');
    }

    public function outstandingForm(){
        return view('report.outstanding');
    }

    public function accstatementForm(){
        return view('report.accstatementForm');
    }

    public function premiumstateForm(){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = [
            'insuranceco_list'=> $insuranceco_list,
        ];
        return view('report.premiumstateForm', $data);
    }
       
    public function debitnotesForm(){
        return view('report.debitnotesForm');
    }

    public function creditnotesForm(){
        return view('report.creditnotesForm');
    }

    public function dailytransForm(){
        return view('report.dailytransForm');
    }

    public function cussalesForm(){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = [
            'insuranceco_list'=> $insuranceco_list,
        ];
        return view('report.cussalesForm', $data);
    }

    public function tsasalesForm(){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = [
            'insuranceco_list'=> $insuranceco_list,
        ];
        return view('report.tsasalesForm', $data);
    }

    public function compsalesForm(){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = [
            'insuranceco_list'=> $insuranceco_list,
        ];
        return view('report.compsalesForm', $data);
    }
}
