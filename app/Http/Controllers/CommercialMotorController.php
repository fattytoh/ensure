<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Motor;
use App\Models\Order;
use App\Models\Country;
use App\Models\Customer;
use App\Models\TSA;
use App\Models\User;
use App\Models\Insuranceco;
use App\Models\HiresComp;
use App\Models\Refno;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommercialMotorController extends Controller
{
     //
    public $prefix = "CMI";
    public $order_prefix = "MT";

    public function getListing(Request $request){
        return view('motor.cmi.List');
    }

    public function getListData(Request $request){
        // Total records
        $order_fields = ['','order_no','order_vehicles_no','partner_name','order_date', ''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $count_query = Order::select('count(*) as allcount')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_prefix_type',$this->order_prefix)
        ->where('order.order_status', "!=","0");

        $main_query = Order::select('order.*', "customer.partner_name", "insurance_motor.order_vehicles_no")
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_prefix_type',$this->order_prefix)
        ->where('order.order_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('order.order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('insurance_motor.order_vehicles_no', 'like', '%' . $searchValue . '%')
                ->orWhere('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('order.order_date', 'like', '%' . $searchValue . '%');
        });

        if($request->input('order_vehicles_no')){
            $main_query->Where('insurance_motor.order_vehicles_no', 'like', '%' . $request->input('order_vehicles_no') . '%');
            $count_query->Where('insurance_motor.order_vehicles_no', 'like', '%' . $request->input('order_vehicles_no') . '%');
        }

        if($request->input('order_policyno')){
            $main_query->Where('insurance_motor.order_policyno', 'like', '%' . $request->input('order_policyno') . '%');
            $count_query->Where('insurance_motor.order_policyno', 'like', '%' . $request->input('order_policyno'). '%');
        }

        if($request->input('order_period_from')){
            $main_query->Where('order.order_date', '>=', $request->input('order_period_from'));
            $count_query->Where('order.order_date', '>=', $request->input('order_period_from'));
        }

        if($request->input('order_period_to')){
            $main_query->Where('order.order_date', '<=', $request->input('order_period_to'));
            $count_query->Where('order.order_date', '<=', $request->input('order_period_to'));
        }

        if($request->input('order_cvnote')){
            $main_query->Where('insurance_motor.order_cvnote', 'like', '%' . $request->input('order_cvnote') . '%');
            $count_query->Where('insurance_motor.order_cvnote', 'like', '%' . $request->input('order_cvnote'). '%');
        }

        if($request->input('dealer_name')){
            $main_query->Where('tsa.dealer_name', 'like', '%' . $request->input('dealer_name') . '%');
            $count_query->Where('tsa.dealer_name', 'like', '%' . $request->input('dealer_name'). '%');
        }

        if($request->input('order_cancel_date')){
            $main_query->Where('insurance_motor.order_cancel_date', $request->input('order_cancel_date'));
            $count_query->Where('insurance_motor.order_cancel_date', $request->input('order_cancel_date'));
        }

        if($request->input('order_type')){
            $main_query->Where('order.order_type', 'like', '%' . $request->input('order_type') . '%');
            $count_query->Where('order.order_type', 'like', '%' . $request->input('order_type'). '%');
        }

        if($request->input('partner_nirc')){
            $main_query->Where('customer.partner_nirc', 'like', '%' . $request->input('partner_nirc') . '%');
            $count_query->Where('customer.partner_nirc', 'like', '%' . $request->input('partner_nirc'). '%');
        }

        if($request->input('partner_name')){
            $main_query->Where('customer.partner_name', 'like', '%' . $request->input('partner_name') . '%');
            $count_query->Where('customer.partner_name', 'like', '%' . $request->input('partner_name'). '%');
        }

        $totalRecords = $count_query->count();
        $totalRecordswithFilter = $count_query
        ->where(function ($query) use ($searchValue) {
            $query->where('order.order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('insurance_motor.order_vehicles_no', 'like', '%' . $searchValue . '%')
                ->orWhere('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('order.order_date', 'like', '%' . $searchValue . '%');
        })
        ->count();

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }

        $listing_data = $main_query
        ->skip($start)
        ->take($rowperpage)
        ->get()->toArray();

        $out_data = [];

        foreach($listing_data as $ky => $li){
        $action = '';
        if(UserGroupPrivilegeController::checkPrivilegeWithAuth("CommercialMotorInfo")){
            $action .= '<a href="'.route('CommercialMotorInfo', ['id' => $li['order_id'] ]).'" class="btn btn-warning">Edit</a>';
        }

        if(UserGroupPrivilegeController::checkPrivilegeWithAuth("CommercialMotorDelete")){
            $action .= '<a href="'.route('CommercialMotorDelete', ['id' => $li['order_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['order_no'].'">Delete</a>';
        }
            
            $out_data[] = [
                $ky + 1,
                $li['order_no']?:'-',
                $li['order_vehicles_no'],
                $li['partner_name']?:'-',
                $li['order_date']?:'-',
                $action
            ];
        }
    
        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200);
    }

    public function MotorInfo(Request $request){
        $opt = new OptionController();
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        if($request->route('id')){
            $info_data = $this->getMotorInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                return view('motor.cmi.Info', $info_data);
            }
            else{
                return redirect()->route('CommercialMotorList');
            }
        }
        else{
            $data = [
                'country_list'=> $country_list,
                'gov_gst' =>  $opt->getMotorGST(),
                'insuranceco_list'=> $insuranceco_list,
                'min_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(70))),
                'max_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(18))),
                'min_year' => date('Y', strtotime(Carbon::now()->subYears(30))),
            ];
            return view('motor.cmi.Add', $data);
        }
    }

    public function MotorRenewal(Request $request){
        if($request->route('id')){
            $info_data = $this->getMotorInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                $info_data->order_no = "-- System Generate --";
                $info_data->order_ref = 0;
                $info_data->order_renewal_ref = $info_data->order_id;
                $info_data->order_premium_amt = 0;
                $info_data->order_commercial_use_amt = 0;
                $info_data->order_accident_amt = 0;
                $info_data->order_in_exp_amt = 0;
                $info_data->order_vehicle_load_amt = 0;
                $info_data->order_ncd_dis_amt = 0;
                $info_data->order_ofd_dis_amt = 0;
                $info_data->order_3rd_rider_amt = 0;
                $info_data->order_4th_rider_amt = 0;
                $info_data->order_addtional_amt = 0;
                $info_data->order_subtotal_amount = 0;
                $info_data->order_direct_discount_amt = 0;
                $info_data->order_bank_interest = 0;
                $info_data->order_grosspremium_amount = 0;
                $info_data->order_extra_name = "";
                $info_data->order_extra_amount = 0;
                $info_data->order_receivable_dealercomm = 0;
                $info_data->order_receivable_dealergstamount = 0;
                $info_data->order_receivable_gstamount = 0;
                $info_data->order_receivable_premiumreceivable = 0;
                $info_data->order_payable_ourcomm = 0;
                $info_data->order_payable_nettcomm = 0;
                $info_data->order_payable_gst = 0;
                $info_data->order_payable_premium = 0;
                $info_data->order_proformsendout_date = "";
                $info_data->order_proformsendout_by = "";
                $info_data->order_policysendout_date = "";
                $info_data->order_policysendout_by = "";
                $info_data->order_dncheckedsendout_date = "";
                $info_data->order_dncheckedsendout_by = "";
                $info_data->order_type = "Renewal";
                $info_data->order_doc_type = "DN";
                $info_data->order_date = date('Y-m-d');
                $info_data->order_period_from = date('Y-m-d');
                $info_data->order_period_to = date('Y-m-d', strtotime(date('Y-m-d'). " +1 year"));
                $info_data->order_period_to = date('Y-m-d', strtotime($info_data->order_period_to. " -1 day"));
                
                return view('motor.cmi.Renewal', $info_data);
            }
            else{
                return redirect()->route('CommercialMotorList');
            }
        }
        else{
            return redirect()->route('CommercialMotorList');
        }
    }

    public function MotorEndorsement(Request $request){
        if($request->route('id')){
            $info_data = $this->getMotorInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                $info_data->order_no = "-- System Generate --";
                $info_data->order_ref = $info_data->order_id;
                $info_data->order_renewal_ref = 0;
                $info_data->order_premium_amt = 0;
                $info_data->order_commercial_use_amt = 0;
                $info_data->order_accident_amt = 0;
                $info_data->order_in_exp_amt = 0;
                $info_data->order_vehicle_load_amt = 0;
                $info_data->order_ncd_dis_amt = 0;
                $info_data->order_ofd_dis_amt = 0;
                $info_data->order_3rd_rider_amt = 0;
                $info_data->order_4th_rider_amt = 0;
                $info_data->order_addtional_amt = 0;
                $info_data->order_subtotal_amount = 0;
                $info_data->order_direct_discount_amt = 0;
                $info_data->order_bank_interest = 0;
                $info_data->order_grosspremium_amount = 0;
                $info_data->order_extra_name = "";
                $info_data->order_extra_amount = 0;
                $info_data->order_receivable_dealercomm = 0;
                $info_data->order_receivable_dealergstamount = 0;
                $info_data->order_receivable_gstamount = 0;
                $info_data->order_receivable_premiumreceivable = 0;
                $info_data->order_payable_ourcomm = 0;
                $info_data->order_payable_nettcomm = 0;
                $info_data->order_payable_gst = 0;
                $info_data->order_payable_premium = 0;
                $info_data->order_proformsendout_date = "";
                $info_data->order_proformsendout_by = "";
                $info_data->order_policysendout_date = "";
                $info_data->order_policysendout_by = "";
                $info_data->order_dncheckedsendout_date = "";
                $info_data->order_dncheckedsendout_by = "";
                $info_data->order_date = date('Y-m-d');
                $info_data->order_period_from = date('Y-m-d');
                $info_data->order_period_to = date('Y-m-d', strtotime(date('Y-m-d'). " +1 year"));
                $info_data->order_period_to = date('Y-m-d', strtotime($info_data->order_period_to. " -1 day"));

                return view('motor.cmi.Endorsement', $info_data);
            }
            else{
                return redirect()->route('CommercialMotorList');
            }
        }
        else{
            return redirect()->route('CommercialMotorList');
        }
    }
 
    public function getMotorInfo($order_id){
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = Order::select('order.*', 'insurance_motor.*' )
        ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id')
        ->where('order.order_id', $order_id)
        ->where('order.order_status', "!=","0")
        ->where('order.order_prefix_type',$this->order_prefix)
        ->first();
        if(isset($data->order_id)){
            $exist_dealer = TSA::find($data->order_dealer);
            $exist_customer = Customer::find($data->order_customer);
            $exist_hires = HiresComp::find($data->order_financecode);
            $add_name = User::find($data->insertBy);
            $up_name = User::find($data->updateBy);
            $data->update_name =  $up_name->getName()?:'Webmaster';
            $data->insert_name =  $add_name->getName()?:'Webmaster';
            $data->dealer_id = $data->order_dealer;
            $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
            $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
            $data->customer_name = isset($exist_customer->partner_name)?$exist_customer->partner_name:'';
            $data->customer_ic = isset($exist_customer->partner_nirc)?substr_replace($exist_customer->partner_nirc,'XXXX', 1,-4):'';
            $data->financecode_name = isset($exist_hires->id)?$exist_hires->comp_name:'';
            $data->min_age = date('Y-m-d', strtotime(Carbon::now()->subYears(70)));
            $data->max_age = date('Y-m-d', strtotime(Carbon::now()->subYears(18)));
            $data->min_year = date('Y', strtotime(Carbon::now()->subYears(30)));
            $data->country_list = $country_list;
            $data->insuranceco_list = $insuranceco_list;
            $file_attach = new FilesController();
            $data->file_list = $file_attach->getFilesList('ens_order',$data->order_id);
            $receipt = new ReceiptController();
            $data->receipt_list = $receipt->getOrderReceipt($data->order_id);
            $pay = new PaymentController();
            $data->payment_list = $pay->getOrderPayment($data->order_id);
            $data->record_list = $this->getRelatedRecord($data->order_id);
            $data->order_financecode_name = HiresComp::find($data->order_financecode);
            
            return $data;
        }
    }
 
    public function create(Request $request){
        $rules = [
            'order_record_billto' => 'required',
            'order_type' => 'required',
            'order_period_from' => 'required|date|after_or_equal:today',
            'order_period_to' => 'required|date|after:order_period_from',
            'order_date' => 'required|date',
            'order_insurco' => 'required',
            'order_coverage' => 'required',
            'order_customer' => 'required',
            'order_dealer' => 'required',
            // 'order_proformsendout_date' => 'required',
            // 'order_policysendout_date' => 'required',
            // 'order_dncheckedsendout_date' => 'required',
            'order_vehicles_no' => 'required',
            'order_premium_amt' => 'required|gt:0',
            // 'order_driver1name' => 'required',
            // 'order_driver1pass_ic' => 'required',
            // 'order_driver1dob' => 'required|date',
        ];
 
        $attr = [
            'order_record_billto' => 'Bill To',
            'order_type' => 'Type',
            // 'order_proformsendout_date' => 'Proposal Form Send Out',
            // 'order_policysendout_date' => 'Policy / CI Send Out',
            // 'order_dncheckedsendout_date' => 'DN Checked',
            'order_insuranceType' => 'Insurance Type',
            'order_period_from' => 'Coverage Period From',
            'order_period_to' => 'Coverage Period To',
            'order_date' => 'Issue Date',
            'order_insurco' => 'Insurance Company',
            'order_coverage' => 'Coverage Package',
            'order_customer' => 'Customer',
            'order_dealer' => 'TSA',
            'order_vehicles_no' => 'Vehicles No',
            'order_premium_amt' => 'Basic Premium',
            'order_driver1name' => 'Driver 1 Name',
            'order_driver1occupation' => 'Driver 1 Occupation',
            'order_driver1pass_date' => 'Driver 1 Driving Pass Dt',
            'order_driver1relationship' => 'Driver 1 Relationship',
            'order_driver1pass_ic' => 'Driver 1 IC',
            'order_driver1dob' => 'Driver 1 DOB',
            'order_driver1_remark' => 'Driver 1 remark',
        ];
 
        $request->validate($rules, [], $attr);
 
        $errors_input = [];

        $files_allow = ['jpg','bmp','png','pdf'];

        if ($request->hasFile('order_files_attach')) {
            foreach($request->file('order_files_attach') as $key => $fli){
                if($fli->getSize() > 5242880 ){
                    $errors_input['order_files_attach'] = 'Attachment Cannot over 5MB';
                }
                elseif(!in_array($fli->extension(),$files_allow)){
                    $errors_input['order_files_attach'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                }
            }
        }
 
        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $order_financecode = $request->input('order_financecode');
        if( $request->input('order_financecode') == '115'){
            $order_financecode = $this->getFinanceCodeID($request->input('order_other_financecode'));
        }
         
        $order_ctrl = new OrderController();
        $new_order_id = $order_ctrl->create($request, $this->order_prefix);
        
        if($new_order_id){
            $new_motor = new Motor();
            $new_motor->order_record_billto = $request->input('order_record_billto');
            // $new_motor->order_insuranceType = $request->input('order_insuranceType');
            $new_motor->order_period_from = $request->input('order_period_from');
            $new_motor->order_period_to = $request->input('order_period_to');
            $new_motor->order_cancel_date = $request->input('order_cancel_date');
            $new_motor->order_coverage = $request->input('order_coverage');
            $new_motor->order_vehicles_no = $request->input('order_vehicles_no');
            $new_motor->order_cvnote = $request->input('order_cvnote');
            $new_motor->order_policyno = $request->input('order_policyno');
            $new_motor->order_financecode = $order_financecode;
            $new_motor->order_vehicles_brand = $request->input('order_vehicles_brand');
            $new_motor->order_vehicles_model = $request->input('order_vehicles_model');
            $new_motor->order_vehicles_engine = $request->input('order_vehicles_engine');
            $new_motor->order_vehicles_body_type = $request->input('order_vehicles_body_type');
            $new_motor->order_vehicles_chasis = $request->input('order_vehicles_chasis');
            $new_motor->order_vehicles_origdt = $request->input('order_vehicles_origdt');
            $new_motor->order_vehicles_capacity = $request->input('order_vehicles_capacity');
            $new_motor->order_vehicles_seatno = $this->cleanNumber($request->input('order_vehicles_seatno'));
            $new_motor->order_nameddriver_amount = $this->cleanNumber($request->input('order_nameddriver_amount'));
            $new_motor->order_nameddriver = $request->input('order_nameddriver');
            $new_motor->order_tpft_amount = $this->cleanNumber($request->input('order_tpft_amount'));
            $new_motor->order_tpft_driver = $request->input('order_tpft_driver');
            $new_motor->order_out_amount = $this->cleanNumber($request->input('order_out_amount'));
            $new_motor->order_out_name = $request->input('order_out_name');
            $new_motor->order_extrabenefit = $request->input('order_extrabenefit');
            $new_motor->order_proformsendout_date = $request->input('order_proformsendout_date');
            $new_motor->order_proformsendout_by = $request->input('order_proformsendout_by');
            $new_motor->order_policysendout_date = $request->input('order_policysendout_date');
            $new_motor->order_policysendout_by = $request->input('order_policysendout_by');
            $new_motor->order_dncheckedsendout_date = $request->input('order_dncheckedsendout_date');
            $new_motor->order_dncheckedsendout_by = $request->input('order_dncheckedsendout_by');
            $new_motor->order_ncd_previousinsurer = $request->input('order_ncd_previousinsurer');
            $new_motor->order_ncd_entitlement = $request->input('order_ncd_entitlement');
            $new_motor->order_ncd_cancellationdate = $request->input('order_ncd_cancellationdate');
            $new_motor->order_ncd_vehiclenumber = $request->input('order_ncd_vehiclenumber');
            $new_motor->order_ncd_remark = $request->input('order_ncd_remark');
            $new_motor->order_notes_remark = $request->input('order_notes_remark');
            $new_motor->order_driver1name = $request->input('order_driver1name');
            $new_motor->order_driver1occupation = $request->input('order_driver1occupation');
            $new_motor->order_driver1pass_date = $request->input('order_driver1pass_date');
            $new_motor->order_driver1pass_ic = $request->input('order_driver1pass_ic');
            $new_motor->order_driver1relationship = $request->input('order_driver1relationship');
            $new_motor->order_driver1dob = $request->input('order_driver1dob');
            $new_motor->order_driver1_remark = $request->input('order_driver1_remark');
            $new_motor->order_driver2name = $request->input('order_driver2name');
            $new_motor->order_driver2occupation = $request->input('order_driver2occupation');
            $new_motor->order_driver2pass_date = $request->input('order_driver2pass_date');
            $new_motor->order_driver2pass_ic = $request->input('order_driver2pass_ic');
            $new_motor->order_driver2relationship = $request->input('order_driver2relationship');
            $new_motor->order_driver2dob = $request->input('order_driver2dob');
            $new_motor->order_driver2_remark = $request->input('order_driver2_remark');
            $new_motor->order_driver3name = $request->input('order_driver3name');
            $new_motor->order_driver3occupation = $request->input('order_driver3occupation');
            $new_motor->order_driver3pass_date = $request->input('order_driver3pass_date');
            $new_motor->order_driver3pass_ic = $request->input('order_driver3pass_ic');
            $new_motor->order_driver3relationship = $request->input('order_driver3relationship');
            $new_motor->order_driver3dob = $request->input('order_driver3dob');
            $new_motor->order_driver3_remark = $request->input('order_driver3_remark');
            $new_motor->order_driver4name = $request->input('order_driver4name');
            $new_motor->order_driver4occupation = $request->input('order_driver4occupation');
            $new_motor->order_driver4pass_date = $request->input('order_driver4pass_date');
            $new_motor->order_driver4pass_ic = $request->input('order_driver4pass_ic');
            $new_motor->order_driver4relationship = $request->input('order_driver4relationship');
            $new_motor->order_driver4dob = $request->input('order_driver4dob');
            $new_motor->order_driver4_remark = $request->input('order_driver4_remark');
            $new_motor->order_id = $new_order_id;
            $new_motor->insertBy = Auth::user()->id;
            $new_motor->updateBy = Auth::user()->id;
            $new_motor->save();

            if($request->hasFile('order_files_attach')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_order',$new_order_id,'uploads/motor','order_files_attach');
            }
        }
        else{
            return redirect()->route('CommercialMotorList')->with('error_msg', 'System Error, Please Contact Admin');
        }
 
        return redirect()->route('CommercialMotorList')->with('success_msg', 'Add Case Succesfully');
    }
 
    public function update(Request $request){
        $exist_order = Order::where('order_id', $request->input('order_id'))
            ->where('order_status', "!=","0")
            ->where('order_prefix_type',$this->prefix)
            ->first();
        $exist_motor = Motor::where('order_id', $request->input('order_id'))
            ->first();

        if(!$exist_order || !$exist_motor){
            return redirect()->route('CommercialMotorList')->with('error_msg', 'Invalid Case');
        }
        else{
            $rules = [
                'order_record_billto' => 'required',
                'order_type' => 'required',
                'order_period_from' => 'required|date|after_or_equal:today',
                'order_period_to' => 'required|date|after:order_period_from',
                'order_date' => 'required|date',
                'order_insurco' => 'required',
                'order_coverage' => 'required',
                'order_customer' => 'required',
                'order_dealer' => 'required',
                'order_vehicles_no' => 'required',
                // 'order_proformsendout_date' => 'required',
                // 'order_policysendout_date' => 'required',
                // 'order_dncheckedsendout_date' => 'required',
                'order_premium_amt' => 'required|gt:0',
                // 'order_driver1name' => 'required',
                // 'order_driver1pass_ic' => 'required',
                // 'order_driver1dob' => 'required|date',
            ];
    
            $attr = [
                'order_record_billto' => 'Bill To',
                'order_type' => 'Type',
                'order_insuranceType' => 'Insurance Type',
                'order_period_from' => 'Coverage Period From',
                'order_period_to' => 'Coverage Period To',
                // 'order_proformsendout_date' => 'Proposal Form Send Out',
                // 'order_policysendout_date' => 'Policy / CI Send Out',
                // 'order_dncheckedsendout_date' => 'DN Checked',
                'order_date' => 'Issue Date',
                'order_insurco' => 'Insurance Company',
                'order_coverage' => 'Coverage Package',
                'order_customer' => 'Customer',
                'order_dealer' => 'TSA',
                'order_vehicles_no' => 'Vehicles No',
                'order_premium_amt' => 'Basic Premium',
                'order_driver1name' => 'Driver 1 Name',
                'order_driver1occupation' => 'Driver 1 Occupation',
                'order_driver1pass_date' => 'Driver 1 Driving Pass Dt',
                'order_driver1relationship' => 'Driver 1 Relationship',
                'order_driver1pass_ic' => 'Driver 1 IC',
                'order_driver1dob' => 'Driver 1 DOB',
                'order_driver1_remark' => 'Driver 1 remark',
            ];
    
            $request->validate($rules, [], $attr);
    
            $errors_input = [];

            $files_allow = ['jpg','bmp','png','pdf'];

            if ($request->hasFile('order_files_attach')) {
                foreach($request->file('order_files_attach') as $key => $fli){
                    if($fli->getSize() > 5242880 ){
                        $errors_input['order_files_attach'] = 'Attachment Cannot over 5MB';
                    }
                    elseif(!in_array($fli->extension(),$files_allow)){
                        $errors_input['order_files_attach'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                    }
                }
            }
    
            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }

            $order_financecode = $request->input('order_financecode');
            if( $request->input('order_financecode') == '115'){
                $order_financecode = $this->getFinanceCodeID($request->input('order_other_financecode'));
            }
            
            $order_ctrl = new OrderController();
            $order_ctrl->update($request);
    
            $exist_motor->order_record_billto = $request->input('order_record_billto');
            // $exist_motor->order_insuranceType = $request->input('order_insuranceType');
            $exist_motor->order_period_from = $request->input('order_period_from');
            $exist_motor->order_period_to = $request->input('order_period_to');
            $exist_motor->order_cancel_date = $request->input('order_cancel_date');
            $exist_motor->order_coverage = $request->input('order_coverage');
            $exist_motor->order_vehicles_no = $request->input('order_vehicles_no');
            $exist_motor->order_cvnote = $request->input('order_cvnote');
            $exist_motor->order_policyno = $request->input('order_policyno');
            $exist_motor->order_financecode = $order_financecode;
            $exist_motor->order_vehicles_brand = $request->input('order_vehicles_brand');
            $exist_motor->order_vehicles_model = $request->input('order_vehicles_model');
            $exist_motor->order_vehicles_engine = $request->input('order_vehicles_engine');
            $exist_motor->order_vehicles_body_type = $request->input('order_vehicles_body_type');
            $exist_motor->order_vehicles_chasis = $request->input('order_vehicles_chasis');
            $exist_motor->order_vehicles_origdt = $request->input('order_vehicles_origdt');
            $exist_motor->order_vehicles_capacity = $request->input('order_vehicles_capacity');
            $exist_motor->order_vehicles_seatno = $this->cleanNumber($request->input('order_vehicles_seatno'));
            $exist_motor->order_nameddriver_amount = $this->cleanNumber($request->input('order_nameddriver_amount'));
            $exist_motor->order_nameddriver = $request->input('order_nameddriver');
            $exist_motor->order_tpft_amount = $this->cleanNumber($request->input('order_tpft_amount'));
            $exist_motor->order_tpft_driver = $request->input('order_tpft_driver');
            $exist_motor->order_out_amount = $this->cleanNumber($request->input('order_out_amount'));
            $exist_motor->order_out_name = $request->input('order_out_name');
            $exist_motor->order_extrabenefit = $request->input('order_extrabenefit');
            $exist_motor->order_proformsendout_date = $request->input('order_proformsendout_date');
            $exist_motor->order_proformsendout_by = $request->input('order_proformsendout_by');
            $exist_motor->order_policysendout_date = $request->input('order_policysendout_date');
            $exist_motor->order_policysendout_by = $request->input('order_policysendout_by');
            $exist_motor->order_dncheckedsendout_date = $request->input('order_dncheckedsendout_date');
            $exist_motor->order_dncheckedsendout_by = $request->input('order_dncheckedsendout_by');
            $exist_motor->order_ncd_previousinsurer = $request->input('order_ncd_previousinsurer');
            $exist_motor->order_ncd_entitlement = $request->input('order_ncd_entitlement');
            $exist_motor->order_ncd_cancellationdate = $request->input('order_ncd_cancellationdate');
            $exist_motor->order_ncd_vehiclenumber = $request->input('order_ncd_vehiclenumber');
            $exist_motor->order_ncd_remark = $request->input('order_ncd_remark');
            $exist_motor->order_notes_remark = $request->input('order_notes_remark');
            $exist_motor->order_driver1name = $request->input('order_driver1name');
            $exist_motor->order_driver1occupation = $request->input('order_driver1occupation');
            $exist_motor->order_driver1pass_date = $request->input('order_driver1pass_date');
            $exist_motor->order_driver1pass_ic = $request->input('order_driver1pass_ic');
            $exist_motor->order_driver1relationship = $request->input('order_driver1relationship');
            $exist_motor->order_driver1dob = $request->input('order_driver1dob');
            $exist_motor->order_driver1_remark = $request->input('order_driver1_remark');
            $exist_motor->order_driver2name = $request->input('order_driver2name');
            $exist_motor->order_driver2occupation = $request->input('order_driver2occupation');
            $exist_motor->order_driver2pass_date = $request->input('order_driver2pass_date');
            $exist_motor->order_driver2pass_ic = $request->input('order_driver2pass_ic');
            $exist_motor->order_driver2relationship = $request->input('order_driver2relationship');
            $exist_motor->order_driver2dob = $request->input('order_driver2dob');
            $exist_motor->order_driver2_remark = $request->input('order_driver2_remark');
            $exist_motor->order_driver3name = $request->input('order_driver3name');
            $exist_motor->order_driver3occupation = $request->input('order_driver3occupation');
            $exist_motor->order_driver3pass_date = $request->input('order_driver3pass_date');
            $exist_motor->order_driver3pass_ic = $request->input('order_driver3pass_ic');
            $exist_motor->order_driver3relationship = $request->input('order_driver3relationship');
            $exist_motor->order_driver3dob = $request->input('order_driver3dob');
            $exist_motor->order_driver3_remark = $request->input('order_driver3_remark');
            $exist_motor->order_driver4name = $request->input('order_driver4name');
            $exist_motor->order_driver4occupation = $request->input('order_driver4occupation');
            $exist_motor->order_driver4pass_date = $request->input('order_driver4pass_date');
            $exist_motor->order_driver4pass_ic = $request->input('order_driver4pass_ic');
            $exist_motor->order_driver4relationship = $request->input('order_driver4relationship');
            $exist_motor->order_driver4dob = $request->input('order_driver4dob');
            $exist_motor->order_driver4_remark = $request->input('order_driver4_remark');
            $exist_motor->updateBy = Auth::user()->id;
            $exist_motor->save();

            if($request->hasFile('order_files_attach')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_order',$request->input('order_id'),'uploads/motor','order_files_attach');
            }
            
            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }
 
    public function delete(Request $request){
        $exist_order = Order::find($request->route('id'));
        if($exist_order){
            $exist_order->order_status = "0";
            $exist_order->save();
        }
    
        return redirect()->route('CommercialMotorList')->with('success_msg', 'Delete Case Succesfully');
    }
 
    public function getFinanceCodeID($comp_name){
        $exist_hire = HiresComp::where('comp_name', strtoupper($comp_name))->first();
        if(!$exist_hire){
            $new_hire = new HiresComp();
            $new_hire->comp_name = strtoupper($comp_name);
            $new_hire->insertBy = Auth::user()->id;
            $new_hire->updateBy = Auth::user()->id;
            $new_hire->save();
            return $new_hire->id;
        }
        else{
            return $exist_hire->id;
        }
    }

    public function HiresComp($code){
        $hirescomp = HiresComp::where('id', $code)->first();
        if($hirescomp){
            return $hirescomp->comp_name;
        }
        else{
            return '-';
        }
    }

    public function cleanNumber($input){
        $input = preg_replace('/[^\d.]/', '', $input);
        return $input?:0;
    }

    public function getRelatedRecord($order_id){
        $output = [];
        $current_order = Order::select("order_ref", "order_renewal_ref")
        ->where('order_status', 1)
        ->where('order_id', $order_id)
        ->first();

        $main_order = 0;

        if($current_order->order_ref > 0){
            $main_order = $current_order->order_ref;
        }
        elseif($current_order->order_renewal_ref > 0){
            $main_order = $current_order->order_renewal_ref;
        }

        $main_query = Order::select('order.*', "customer.partner_name", "insurance_motor.order_coverage", "insurance_motor.order_period_from", "insurance_motor.order_period_to", "insurance_motor.order_policyno")
        ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->where('order.order_status', 1)
        ->where(function ($query) use ($order_id, $main_order) {
            $query->where('order.order_ref',  $order_id )
                ->orWhere('order.order_renewal_ref', $order_id)
                ->orWhere('order.order_id', $main_order);
        })
        ->get()->toArray();

        foreach($main_query as $mli){
            $insert_name = User::find($mli['insertBy']);

            $order_no = '';

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("CommercialMotorInfo")){
                $order_no = '<a href="'.route('CommercialMotorInfo', ['id' => $mli['order_id'] ]).'" target="_blank">'.$mli['order_no'].'</a>';
            }
            else{
                $order_no = $mli['order_no'];
            }

            $output[] = [
                'order_id' => $mli['order_id'],
                'order_no' => $mli['order_no'],
                'order_doc_type' => $mli['order_doc_type'],
                'order_grosspremium_amount' => $mli['order_grosspremium_amount'],
                'partner_name' => $mli['partner_name'],
                'order_period_from' => $mli['order_period_from'],
                'order_period_to' => $mli['order_period_to'],
                'insertBy' => $insert_name->getName(),
            ];
        }

        return $output;
    }
}
