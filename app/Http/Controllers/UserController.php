<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Empl;
use App\Models\TSA;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    //
    public function profile(){
        $data = [];
        if(Auth::user()->ref_code == 'EMPL'){
            $usr = Empl::find(Auth::user()->ref)->first();
            $data = [
                'name' => $usr->empl_name,
            ];
        }
        else{
            $usr = TSA::find(Auth::user()->ref)->first();
            $data = [
                'name' => $usr->dealer_name,
            ];
        }
        return view('profile', $data);
    }

    public function updateProfile(Request $request){
        $rules = [
            'password'    => 'nullable|min:5',
            'cfm_password'    => 'same:password',
        ];

        $msg = [
        ];

        $request->validate($rules, $msg);
        $errors_input = [];

        if($request->input('password')){
            if(!Hash::check($request->input('old_password'), Auth::user()->password) ){
                $errors_input['old_password'] = "Invalid Old Password";
            }
        }

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $exist_admin = User::find(Auth::user()->id);
        if($request->input('password')){
            $exist_admin->password = Hash::make($request->input('password'));
        }
        $exist_admin->save();

        return redirect()->back()->withInput()->with('success_msg', 'Upadate Profile Succesfully');
    }
}
