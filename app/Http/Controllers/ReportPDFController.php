<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Motor;
use App\Models\Maid;
use App\Models\General;
use App\Models\Order;
use App\Models\Country;
use App\Models\Payment;
use App\Models\PaymentDetail;
use App\Models\Receipt;
use App\Models\ReceiptDetail;
use App\Models\Refno;
use App\Models\Customer;
use App\Models\TSA;
use App\Models\User;
use App\Models\Insuranceco;
use App\Models\InsurancePlan;
use App\Models\HiresComp;
use App\Services\TSAStatePDF;
use App\Services\OutstandingPDF;
use App\Services\DebitNotePDF;
use App\Services\CreditNotePDF;
use App\Services\PremiumStatementPDF;
use App\Services\SalesReportPDF;
use App\Services\TsaSalesReportPDF;
use App\Services\DailyTransPDF;
use App\Services\CusSalesReportPDF;
use App\Services\AccountStatementPDF;
use Illuminate\Support\Facades\DB;

class ReportPDFController extends Controller
{
    //
    public $insurance_type_mapping = [
        "MI" => "MotorCycle Insurance",
        "PMI" => "Private Motor Insurance",
        "CMI" => "Commercial Motor Insurance",
        "GM" => "Domestic Maid Insurance",
        "GI" => "General Insurance",
    ];

    public $maid_coverage_mapping = [
        'Exclusive Plan' => 'E',
        'Deluxe Plan' => 'D',
        'Classic Plan' => 'C',
        '300MDC' => 'O3',
        '500MDC' => 'O5',
        '2K' => 'PHB2',
        '7K' => 'PHB7',
    ];

    public function tsastatementreport(Request $request){
        $listing_data = [];
        $dealer_list = [];
        
        $sub_sql = "SELECT SUM(rc.recpline_offset_amount) as total FROM ens_receipt_detail rc INNER JOIN ens_receipt r ON r.receipt_id = rc.recpline_receipt_id WHERE rc.recpline_order_id = ens_order.order_id AND r.receipt_status = '1' ";

        $sub_payment_sql = "SELECT SUM(rc.paymentline_offset_amount) as total FROM ens_payment_detail rc INNER JOIN ens_payment r ON r.payment_id = rc.paymentline_payment_id WHERE rc.paymentline_order_id = ens_order.order_id AND r.payment_status = '1' AND r.payment_to = 'TSA' ";
        
        if($request->input('order_dealer') > 0){
            $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'),  DB::raw('COALESCE(('.$sub_payment_sql.'),0) as payment_total'), 'tsa.dealer_name', 'customer.partner_name')
                ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
                ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
                ->where('order.order_dealer', $request->input('order_dealer'))
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

            if($request->input('insurance_type') == 'PCMI'){
                $order_query->where(function($query)
                {
                    $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                    ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                });
            }
            elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
            }
    
            if($request->input('period_from')){
                $order_query->where('order.order_date', ">=", $request->input('period_from'));
            }
    
            if($request->input('period_to')){
                $order_query->where('order.order_date', "<=", $request->input('period_to'));
            }

            $order_data = $order_query->get()->toArray();

            foreach($order_data as $row){
                if($request->input('show_outstanding') == 'Y'){
                    $leftover = $row['order_receivable_premiumreceivable'] - $row['paid_total'] + $row['payment_total'] ;
                    if($leftover == 0){
                        continue;
                    }
                }

                if(in_array($row['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                    $sub_data = Motor::select('order_policyno', 'order_vehicles_no')
                    ->where('order_id', $row['order_id'])
                    ->first()->toArray();
                    $row['order_policyno'] = $sub_data['order_policyno'];
                    $row['order_vehicles_no'] = $sub_data['order_vehicles_no'];
                    $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'];
                }
                else if($row['order_prefix_type'] == 'GMM'){
                    $sub_data = Maid::select('order_policyno', 'order_maid_name') 
                    ->where('order_id', $row['order_id'])
                    ->first()->toArray();
                    $row['order_policyno'] = $sub_data['order_policyno'];
                    $row['order_maid_name'] = $sub_data['order_maid_name'];
                    $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'] + $row['order_cover_plan_no_gst'];
                }
                else{
                    $sub_data = General::select('order_policyno', 'order_coverage')
                    ->where('order_id', $dli['order_id'])
                    ->first()->toArray();
                    $row['order_policyno'] = $sub_data['order_policyno'];
                    $plan = InsurancePlan::find($gen_info['order_coverage']);
                    $row['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                    $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'];
                }

                $row['total_tsa_comm'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];

                $listing_data['0'][] = $row;
            }
        }
        else{
            $dealer_list = TSA::select('dealer_id')
            ->join('order', 'order.order_dealer', '=', 'tsa.dealer_id')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', '1')
            ->get()->toArray();

            foreach($dealer_list as $dky => $dli){
                $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'),  DB::raw('COALESCE(('.$sub_payment_sql.'),0) as payment_total'), 'tsa.dealer_name', 'customer.partner_name')
                ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
                ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_dealer', $dli['dealer_id'])
                ->where('order.order_status', '1');

                if($request->input('insurance_type') == 'PCMI'){
                    $order_query->where(function($query)
                    {
                        $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                        ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                    });
                }
                elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                    $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
                }
        
                if($request->input('period_from')){
                    $order_query->where('order.order_date', ">=", $request->input('period_from'));
                }
        
                if($request->input('period_to')){
                    $order_query->where('order.order_date', "<=", $request->input('period_to'));
                }

                $order_data = $order_query->get()->toArray();

                foreach($order_data as $row){
                    if($request->input('show_outstanding') == 'Y'){
                        $leftover = $row['order_receivable_premiumreceivable'] - $row['paid_total'] + $row['payment_total'] ;
                        if($leftover == 0){
                            continue;
                        }
                    }

                    if(in_array($row['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                        $sub_data = Motor::select('order_policyno', 'order_vehicles_no')
                        ->where('order_id', $row['order_id'])
                        ->first()->toArray();
                        $row['order_policyno'] = $sub_data['order_policyno'];
                        $row['order_vehicles_no'] = $sub_data['order_vehicles_no'];
                        $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'];
                    }
                    else if($row['order_prefix_type'] == 'GMM'){
                        $sub_data = Maid::select('order_policyno', 'order_maid_name') 
                        ->where('order_id', $row['order_id'])
                        ->first()->toArray();
                        $row['order_policyno'] = $sub_data['order_policyno'];
                        $row['order_maid_name'] = $sub_data['order_maid_name'];
                        $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'] + $row['order_cover_plan_no_gst'];
                    }
                    else{
                        $sub_data = General::select('order_policyno', 'order_coverage')
                        ->where('order_id', $dli['order_id'])
                        ->first()->toArray();
                        $row['order_policyno'] = $sub_data['order_policyno'];
                        $plan = InsurancePlan::find($gen_info['order_coverage']);
                        $row['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                        $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'];
                    }

                    $row['total_tsa_comm'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];

                    $listing_data[$dky][] = $row;
                }

            }
        }
        
        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type');
            $excel_ctr->show_os = $request->input('show_outstanding');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->hide_commission = $request->input('hide_commission');
            $excel_ctr->data_listing = array_values($listing_data);
            $excel_ctr->TSAStateExcel();
        }
        else{
            $filename = 'TSA_Report-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new TSAStatePDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->filter_by = $request->input('insurance_type');
            $pdf->show_os = $request->input('show_outstanding');
            $pdf->hide_comn = $request->input('hide_commission');
            $pdf->dealer_id = $request->input('order_dealer');
            $pdf->data_listing = array_values($listing_data);
            $pdf->SetTitle("TSA Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function outstandingreport(Request $request){       
        $listing_data = [];
        $user_list = [];

        $sub_sql = "SELECT SUM(rc.recpline_offset_amount) as total FROM ens_receipt_detail rc INNER JOIN ens_receipt r ON r.receipt_id = rc.recpline_receipt_id WHERE rc.recpline_order_id = ens_order.order_id AND r.receipt_status = '1' ";

        $sub_payment_sql = "SELECT SUM(rc.paymentline_offset_amount) as total FROM ens_payment_detail rc INNER JOIN ens_payment r ON r.payment_id = rc.paymentline_payment_id WHERE rc.paymentline_order_id = ens_order.order_id AND r.payment_status = '1' AND r.payment_to = 'TSA'";

        if($request->input('group_by')){
            $user_list = [];
            if($request->input('group_by')== 'dealer'){
                $user_list = TSA::select('dealer_id as usr_id')
                ->join('order', 'order.order_dealer', '=', 'tsa.dealer_id')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1')
                ->get()->toArray();
            }
            elseif($request->input('group_by') == 'customer'){
                $user_list = Customer::select('partner_id as usr_id')
                ->join('order', 'order.order_customer', '=', 'customer.partner_id')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1')
                ->get()->toArray();
            }
            
            foreach($user_list as $usr_li){
                $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'), DB::raw('COALESCE(('.$sub_payment_sql.'),0) as payment_total'),  'tsa.dealer_name', 'customer.partner_name')
                ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
                ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');
    
                if($request->input('insurance_type') == 'PCMI'){
                    $order_query->where(function($query)
                    {
                        $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                        ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                    });
                }
                elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                    $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
                }
        
                if($request->input('group_by')== 'dealer'){
                    $order_query->where('order.order_dealer', $usr_li['usr_id']);
                }
                else{
                    $order_query->where('order.order_customer', $usr_li['usr_id']);
                }
        
                if($request->input('period_from')){
                    $order_query->where('order.order_date', ">=", $request->input('period_from'));
                }
        
                if($request->input('period_to')){
                    $order_query->where('order.order_date', "<=", $request->input('period_to'));
                }
    
                $order_data = $order_query->get()->toArray();
    
                foreach($order_data as $row){
                    
                    if(in_array($row['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                        $sub_data = Motor::select('order_policyno', 'order_vehicles_no')
                        ->where('order_id', $row['order_id'])
                        ->first()->toArray();
                        $row['order_policyno'] = $sub_data['order_policyno'];
                        $row['order_vehicles_no'] = $sub_data['order_vehicles_no'];
                    }
                    else if($row['order_prefix_type'] == 'GMM'){
                        $sub_data = Maid::select('order_policyno', 'order_maid_name') 
                        ->where('order_id', $row['order_id'])
                        ->first()->toArray();
                        $row['order_policyno'] = $sub_data['order_policyno'];
                        $row['order_coverage'] = $sub_data['order_coverage'];
                    }
                    else{
                        $sub_data = General::select('order_policyno', 'order_coverage')
                        ->where('order_id', $dli['order_id'])
                        ->first()->toArray();
                        $row['order_policyno'] = $sub_data['order_policyno'];
                        $plan = InsurancePlan::find($sub_data['order_coverage']);
                        $row['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                    }
    
                    if($row['order_type']){
                        $row['order_display_type'] = strtoupper(substr($row['order_type'], 0, 1));
                    }
                    else{
                        $row['order_display_type'] = strtoupper(substr($row['order_endorsement_type'], 0, 1));
                    }
    
                    $date = Carbon::parse($row['order_date']);
                    $now = Carbon::now();
                    $days = $date->diffInDays($now);
                    $row['order_outstanding_day'] = $days;
                    $row['order_outstanding'] = $row['order_receivable_premiumreceivable'] - $row['paid_total'];
                    $listing_data[$usr_li['usr_id']][] = $row;
                }
            }
        }
        else{
            $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'), DB::raw('COALESCE(('.$sub_payment_sql.'),0) as payment_total'),  'tsa.dealer_name', 'customer.partner_name')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', '1');

            if($request->input('insurance_type') == 'PCMI'){
                $order_query->where(function($query)
                {
                    $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                    ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                });
            }
            elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
            }
    
            if($request->input('order_dealer')){
                $order_query->where('order.order_dealer', $request->input('order_dealer'));
            }
    
            if($request->input('order_customer')){
                $order_query->where('order.order_customer', $request->input('order_customer'));
            }
    
            if($request->input('period_from')){
                $order_query->where('order.order_date', ">=", $request->input('period_from'));
            }
    
            if($request->input('period_to')){
                $order_query->where('order.order_date', "<=", $request->input('period_to'));
            }

            $order_data = $order_query->get()->toArray();

            foreach($order_data as $row){
                
                if(in_array($row['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                    $sub_data = Motor::select('order_policyno', 'order_vehicles_no')
                    ->where('order_id', $row['order_id'])
                    ->first()->toArray();
                    $row['order_policyno'] = $sub_data['order_policyno'];
                    $row['order_vehicles_no'] = $sub_data['order_vehicles_no'];
                }
                else if($row['order_prefix_type'] == 'GMM'){
                    $sub_data = Maid::select('order_policyno', 'order_maid_name') 
                    ->where('order_id', $row['order_id'])
                    ->first()->toArray();
                    $row['order_policyno'] = $sub_data['order_policyno'];
                    $row['order_coverage'] = $sub_data['order_coverage'];
                }
                else{
                    $sub_data = General::select('order_policyno', 'order_coverage')
                    ->where('order_id', $dli['order_id'])
                    ->first()->toArray();
                    $row['order_policyno'] = $sub_data['order_policyno'];
                    $plan = InsurancePlan::find($gen_info['order_coverage']);
                    $row['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                }

                if($row['order_type']){
                    $row['order_display_type'] = strtoupper(substr($row['order_type'], 0, 1));
                }
                else{
                    $row['order_display_type'] = strtoupper(substr($row['order_endorsement_type'], 0, 1));
                }

                $date = Carbon::parse($row['order_date']);
                $now = Carbon::now();
                $days = $date->diffInDays($now);
                $row['order_outstanding_day'] = $days;
                $row['order_outstanding'] = $row['order_receivable_premiumreceivable'] - $row['paid_total'];
                $listing_data[] = $row;
            }

        }
        
        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type');
            $excel_ctr->show_os = $request->input('show_outstanding');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->order_customer = $request->input('order_customer');
            $excel_ctr->order_insurco = $request->input('order_insurco');
            $excel_ctr->report_group = $request->input('group_by');
            $excel_ctr->data_listing = $listing_data;
            $excel_ctr->OutStandingExcel();
        }
        else{
            $filename = 'Outstanding_Report-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new OutstandingPDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->filter_by = $request->input('insurance_type');
            $pdf->dealer_id = $request->input('order_dealer');
            $pdf->cus_id = $request->input('order_customer');
            $pdf->group_by = $request->input('group_by');
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("Outstanding Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }
   
    public function debitnotereport(Request $request){  

        if($request->input('group_by')){
            $user_list = [];
            if($request->input('group_by')== 'dealer'){
                $user_list = TSA::select('dealer_id as usr_id')
                ->join('order', 'order.order_dealer', '=', 'tsa.dealer_id')
                ->where('order.order_doc_type', 'DN')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if($request->input('order_dealer')){
                    $user_list->where('order.order_dealer', $request->input('order_dealer'));
                }
    
                $user_list->get()->toArray();
            }
            elseif($request->input('group_by') == 'customer'){
                $user_list = Customer::select('partner_id as usr_id')
                ->join('order', 'order.order_customer', '=', 'customer.partner_id')
                ->where('order.order_doc_type', 'DN')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if($request->input('order_customer')){
                    $user_list->where('order.order_customer', $request->input('order_customer'));
                }

                $user_list->get()->toArray();
            }
            
            foreach($user_list as $usr_li){
                $order_query = Order::select('order.*', 'customer.partner_name')
                ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
                ->where('order.order_doc_type', 'DN')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if(in_array($request->input('insurance_type'),array("MI","PMI","CMI","PCMI"))){
                    $order_query->addSelect("insurance_motor.order_vehicles_no", "insurance_motor.order_cvnote", "insurance_motor.order_policyno")
                    ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id');
                }

                if($request->input('insurance_type') == 'PCMI'){
                    $order_query->where(function($query)
                    {
                        $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                        ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                    });
                }
                elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                    $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
                }

                if($request->input('group_by')== 'dealer'){
                    $order_query->where('order.order_dealer', $usr_li['usr_id']);

                    if($request->input('order_customer')){
                        $user_list->where('order.order_customer', $request->input('order_customer'));
                    }
                }
                elseif($request->input('group_by')== 'customer'){
                    $order_query->where('order.order_customer',  $usr_li['usr_id']);

                    if($request->input('order_dealer')){
                        $user_list->where('order.order_dealer', $request->input('order_dealer'));
                    }
                }

                if($request->input('period_from')){
                    $order_query->where('order.order_date', ">=", $request->input('period_from'));
                }

                if($request->input('period_to')){
                    $order_query->where('order.order_date', "<=", $request->input('period_to'));
                }

                $order_data = $order_query->get()->toArray();

                $listing_data = [];

                foreach($order_data as $dky => $dli){
                    if(in_array($dli['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                        $sub_data = Motor::select('order_policyno')
                        ->where('order_id', $dli['order_id'])
                        ->first();
                    }
                    else if($dli['order_prefix_type'] == 'GMM'){
                        $sub_data = Maid::select('order_policyno') 
                        ->where('order_id', $dli['order_id'])
                        ->first();
                    }
                    else{
                        $sub_data = General::select('order_policyno')
                        ->where('order_id', $dli['order_id'])
                        ->first();

                    }

                    $dli['order_policyno'] = $sub_data->order_policyno; 
                    $listing_data[] = $dli;
                }

            }
        }
        else{
            $order_query = Order::select('order.*', 'customer.partner_name')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->where('order.order_doc_type', 'DN')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', '1');

            if(in_array($request->input('insurance_type'),array("MI","PMI","CMI","PCMI"))){
                $order_query->addSelect("insurance_motor.order_vehicles_no", "insurance_motor.order_cvnote", "insurance_motor.order_policyno")
                ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id');
            }

            if($request->input('insurance_type') == 'PCMI'){
                $order_query->where(function($query)
                {
                    $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                    ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                });
            }
            elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
            }

            if($request->input('order_dealer')){
                $order_query->where('order.order_dealer', $request->input('order_dealer'));
            }

            if($request->input('order_customer')){
                $order_query->where('order.order_customer', $request->input('order_customer'));
            }

            if($request->input('period_from')){
                $order_query->where('order.order_date', ">=", $request->input('period_from'));
            }

            if($request->input('period_to')){
                $order_query->where('order.order_date', "<=", $request->input('period_to'));
            }

            $order_data = $order_query->get()->toArray();

            $listing_data = [];

            foreach($order_data as $dky => $dli){
                if(in_array($dli['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                    $sub_data = Motor::select('order_policyno')
                    ->where('order_id', $dli['order_id'])
                    ->first();
                }
                else if($dli['order_prefix_type'] == 'GMM'){
                    $sub_data = Maid::select('order_policyno') 
                    ->where('order_id', $dli['order_id'])
                    ->first();
                }
                else{
                    $sub_data = General::select('order_policyno')
                    ->where('order_id', $dli['order_id'])
                    ->first();

                }

                $dli['order_policyno'] = $sub_data->order_policyno; 
                $listing_data[] = $dli;
            }
        }

        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type');
            $excel_ctr->show_os = $request->input('show_outstanding');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->order_customer = $request->input('order_customer');
            $excel_ctr->hide_commission = $request->input('hide_commission');
            $excel_ctr->data_listing = $listing_data;
            $excel_ctr->DebitNoteExcel();
        }
        else{
            $filename = 'Debit_note-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new DebitNotePDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->filter_by = $request->input('insurance_type');
            $pdf->dealer_id = $request->input('order_dealer');
            $pdf->cus_id = $request->input('order_customer');
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("Debit Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function CreditNotereport(Request $request){  
        if($request->input('group_by')){
            $user_list = [];
            if($request->input('group_by')== 'dealer'){
                $user_list = TSA::select('dealer_id as usr_id')
                ->join('order', 'order.order_dealer', '=', 'tsa.dealer_id')
                ->where('order.order_doc_type', 'CN')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if($request->input('order_dealer')){
                    $user_list->where('order.order_dealer', $request->input('order_dealer'));
                }
    
                $user_list->get()->toArray();
            }
            elseif($request->input('group_by') == 'customer'){
                $user_list = Customer::select('partner_id as usr_id')
                ->join('order', 'order.order_customer', '=', 'customer.partner_id')
                ->where('order.order_doc_type', 'CN')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if($request->input('order_customer')){
                    $user_list->where('order.order_customer', $request->input('order_customer'));
                }

                $user_list->get()->toArray();
            }
            
            foreach($user_list as $usr_li){
                $order_query = Order::select('order.*', 'customer.partner_name')
                ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
                ->where('order.order_doc_type', 'CN')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if(in_array($request->input('insurance_type'),array("MI","PMI","CMI","PCMI"))){
                    $order_query->addSelect("insurance_motor.order_vehicles_no", "insurance_motor.order_cvnote", "insurance_motor.order_policyno")
                    ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id');
                }

                if($request->input('insurance_type') == 'PCMI'){
                    $order_query->where(function($query)
                    {
                        $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                        ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                    });
                }
                elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                    $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
                }

                if($request->input('group_by')== 'dealer'){
                    $order_query->where('order.order_dealer', $usr_li['usr_id']);

                    if($request->input('order_customer')){
                        $user_list->where('order.order_customer', $request->input('order_customer'));
                    }
                }
                elseif($request->input('group_by')== 'customer'){
                    $order_query->where('order.order_customer',  $usr_li['usr_id']);

                    if($request->input('order_dealer')){
                        $user_list->where('order.order_dealer', $request->input('order_dealer'));
                    }
                }

                if($request->input('period_from')){
                    $order_query->where('order.order_date', ">=", $request->input('period_from'));
                }

                if($request->input('period_to')){
                    $order_query->where('order.order_date', "<=", $request->input('period_to'));
                }

                $order_data = $order_query->get()->toArray();

                $listing_data = [];

                foreach($order_data as $dky => $dli){
                    if(in_array($dli['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                        $sub_data = Motor::select('order_policyno')
                        ->where('order_id', $dli['order_id'])
                        ->first();
                    }
                    else if($dli['order_prefix_type'] == 'GMM'){
                        $sub_data = Maid::select('order_policyno') 
                        ->where('order_id', $dli['order_id'])
                        ->first();
                    }
                    else{
                        $sub_data = General::select('order_policyno')
                        ->where('order_id', $dli['order_id'])
                        ->first();

                    }

                    $dli['order_policyno'] = $sub_data->order_policyno; 
                    $listing_data[] = $dli;
                }
            }
        }
        else{
            $order_query = Order::select('order.*', 'customer.partner_name')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->where('order.order_doc_type', 'CN')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', '1');

            if(in_array($request->input('insurance_type'),array("MI","PMI","CMI","PCMI"))){
                $order_query->addSelect("insurance_motor.order_vehicles_no", "insurance_motor.order_cvnote", "insurance_motor.order_policyno")
                ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id');
            }

            if($request->input('insurance_type') == 'PCMI'){
                $order_query->where(function($query)
                {
                    $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                    ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                });
            }
            elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
            }

            if($request->input('order_dealer')){
                $order_query->where('order.order_dealer', $request->input('order_dealer'));
            }

            if($request->input('order_customer')){
                $order_query->where('order.order_customer', $request->input('order_customer'));
            }

            if($request->input('period_from')){
                $order_query->where('order.order_date', ">=", $request->input('period_from'));
            }

            if($request->input('period_to')){
                $order_query->where('order.order_date', "<=", $request->input('period_to'));
            }

            $order_data = $order_query->get()->toArray();

            $listing_data = [];

            foreach($order_data as $dky => $dli){
                if(in_array($dli['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                    $sub_data = Motor::select('order_policyno')
                    ->where('order_id', $dli['order_id'])
                    ->first();
                }
                else if($dli['order_prefix_type'] == 'GMM'){
                    $sub_data = Maid::select('order_policyno') 
                    ->where('order_id', $dli['order_id'])
                    ->first();
                }
                else{
                    $sub_data = General::select('order_policyno')
                    ->where('order_id', $dli['order_id'])
                    ->first();

                }

                $dli['order_policyno'] = $sub_data->order_policyno; 
                $listing_data[] = $dli;
                
            }
        }

        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type');
            $excel_ctr->show_os = $request->input('show_outstanding');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->order_customer = $request->input('order_customer');
            $excel_ctr->hide_commission = $request->input('hide_commission');
            $excel_ctr->data_listing = $listing_data;
            $excel_ctr->CreditNoteExcel();
        }
        else{
            $filename = 'Credit_note-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new CreditNotePDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->filter_by = $request->input('insurance_type');
            $pdf->dealer_id = $request->input('order_dealer');
            $pdf->cus_id = $request->input('order_customer');
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("Credit Note Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function PremiumStatementReport(Request $request){
        $sub_sql = "SELECT SUM(rc.recpline_offset_amount) as total FROM ens_receipt_detail rc INNER JOIN ens_receipt r ON r.receipt_id = rc.recpline_receipt_id WHERE rc.recpline_order_id = ens_order.order_id AND r.receipt_status = '1' ";

        if($request->input('group_by')){
            $user_list = [];
            if($request->input('group_by')== 'dealer'){
                $user_list = TSA::select('dealer_id as usr_id')
                ->join('order', 'order.order_dealer', '=', 'tsa.dealer_id')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if($request->input('order_dealer')){
                    $user_list->where('order.order_dealer', $request->input('order_dealer'));
                }
    
                $user_list->get()->toArray();
            }
            elseif($request->input('group_by') == 'customer'){
                $user_list = Customer::select('partner_id as usr_id')
                ->join('order', 'order.order_customer', '=', 'customer.partner_id')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if($request->input('order_customer')){
                    $user_list->where('order.order_customer', $request->input('order_customer'));
                }

                $user_list->get()->toArray();
            }
            elseif($request->input('group_by') == 'insuranceco'){
                $user_list = Insuranceco::select('partner_id as usr_id')
                ->join('order', 'order.order_insurco', '=', 'insurance_comp.insuranceco_id')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');

                if($request->input('order_insurco')){
                    $user_list->where('order.order_insurco', $request->input('order_insurco'));
                }

                $user_list->get()->toArray();
            }

            foreach($user_list as $usr_li){
                $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'),  'tsa.dealer_name', 'customer.partner_name')
                ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
                ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
                ->where('order.order_cprofile', env('APP_ID'))
                ->where('order.order_status', '1');
    
                if($request->input('insurance_type') == 'PCMI'){
                    $order_query->where(function($query)
                    {
                        $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                        ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                    });
                }
                elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                    $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
                }
        
                if($request->input('group_by')== 'dealer'){
                    $order_query->where('order.order_dealer', $usr_li['usr_id']);
                }
                elseif($request->input('group_by')== 'customer'){
                    $order_query->where('order.order_customer', $usr_li['usr_id']);
                }
                elseif($request->input('group_by')== 'insuranceco'){
                    $order_query->where('order.order_insurco', $usr_li['usr_id']);
                }
        
                if($request->input('period_from')){
                    $order_query->where('order.order_date', ">=", $request->input('period_from'));
                }
        
                if($request->input('period_to')){
                    $order_query->where('order.order_date', "<=", $request->input('period_to'));
                }

                $order_data = $order_query->get()->toArray();

                $listing_data = [];

                foreach($order_data as $dky => $dli){
                    if(in_array($dli['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                        $sub_data = Motor::select('order_vehicles_no')
                        ->where('order_id', $dli['order_id'])
                        ->first();
                        $dli['order_vehicles_no'] = $sub_data->order_vehicles_no;
                    }
                    else if($dli['order_prefix_type'] == 'GMM'){
                        $sub_data = Maid::select('order_coverage') 
                        ->where('order_id', $dli['order_id'])
                        ->first();
                        $dli['order_coverage'] = $sub_data->order_coverage;
                    }
                    else{
                        $sub_data = General::select('order_coverage')
                        ->where('order_id', $dli['order_id'])
                        ->first();
                        $plan = InsurancePlan::find($sub_data->order_coverage);
                        $dli['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';

                    }
                    $listing_data[$usr_li][] = $dli;
                }
            }

        }
        else{
            $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'),  'tsa.dealer_name', 'customer.partner_name')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', '1');

            if($request->input('insurance_type') == 'PCMI'){
                $order_query->where(function($query)
                {
                    $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                    ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
                });
            }
            elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
                $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
            }

            if($request->input('order_customer')){
                $order_query->where('order.order_customer', $request->input('order_customer'));
            }

            if($request->input('order_insurco')){
                $order_query->where('order.order_insurco', $request->input('order_insurco'));
            }

            if($request->input('order_dealer')){
                $order_query->where('order.order_dealer', $request->input('order_dealer'));
            }
    
            if($request->input('period_from')){
                $order_query->where('order.order_date', ">=", $request->input('period_from'));
            }
    
            if($request->input('period_to')){
                $order_query->where('order.order_date', "<=", $request->input('period_to'));
            }

            $order_data = $order_query->get()->toArray();

            $listing_data = [];

            foreach($order_data as $dky => $dli){
                if(in_array($dli['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                    $sub_data = Motor::select('order_vehicles_no', 'order_policyno')
                    ->where('order_id', $dli['order_id'])
                    ->first();
                    $dli['order_vehicles_no'] = $sub_data->order_vehicles_no;
                    $dli['order_policyno'] = $sub_data->order_policyno;
                }
                else if($dli['order_prefix_type'] == 'GMM'){
                    $sub_data = Maid::select('order_coverage', 'order_policyno') 
                    ->where('order_id', $dli['order_id'])
                    ->first();
                    $dli['order_coverage'] = $sub_data->order_coverage;
                    $dli['order_policyno'] = $sub_data->order_policyno;
                }
                else{
                    $sub_data = General::select('order_coverage', 'order_policyno')
                    ->where('order_id', $dli['order_id'])
                    ->first();
                    $plan = InsurancePlan::find($sub_data->order_coverage);
                    $dli['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                    $dli['order_policyno'] = $sub_data->order_policyno;
                }
                $listing_data[] = $dli;
            }
        }

        $filename = 'Premium_statement-'.strtoupper(date('d-M-Y')).".pdf";

        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type');
            $excel_ctr->dealer_id = $request->input('order_dealer');
            $excel_ctr->cus_id = $request->input('order_customer');
            $excel_ctr->insurer_id = $request->input('order_insurco');
            $excel_ctr->group_by = $request->input('group_by');
            $excel_ctr->data_listing = $listing_data;
            $excel_ctr->PremiumStateExcel();
        }
        else{
            // Create instance of PDF class
            $pdf = new PremiumStatementPDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->filter_by = $request->input('insurance_type');
            $pdf->dealer_id = $request->input('order_dealer');
            $pdf->cus_id = $request->input('order_customer');
            $pdf->insurer_id = $request->input('order_insurco');
            $pdf->group_by = $request->input('group_by');
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("Premium Statement Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }

    }

    public function DailyTransReport(Request $request){
        if($request->input('insurance_type') == 'receipt'){
            $order_query = ReceiptDetail::select('receipt_detail.*', 'order.order_no', 'receipt.receipt_remarks', 'tsa.dealer_name', 'customer.partner_name')
            ->join('receipt', 'receipt.receipt_id', '=', 'receipt_detail.recpline_receipt_id')
            ->leftJoin('order', 'receipt_detail.recpline_order_id', '=', 'order.order_id')
            ->leftJoin('customer', 'customer.partner_id', '=', 'receipt.receipt_customer')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'receipt.receipt_dealer')
            ->where('receipt.receipt_cprofile', env('APP_ID'))
            ->where('receipt.receipt_status', 1)
            ->where('receipt_detail.recpline_status', 1);

            if($request->input('period_from')){
                $order_query->where('receipt.receipt_date', ">=", $request->input('period_from'));
            }
    
            if($request->input('period_to')){
                $order_query->where('receipt.receipt_date', "<=", $request->input('period_to'));
            }
        }
        else{
            $order_query = PaymentDetail::select('payment_detail.*', 'order.order_no', 'payment.payment_remarks', 'payment.payment_paid','payment.payment_date', 'tsa.dealer_name', 'customer.partner_name', 'insurance_comp.insuranceco_name')
            ->join('payment', 'payment.payment_id', '=', 'payment_detail.paymentline_payment_id')
            ->leftJoin('order', 'payment_detail.paymentline_order_id', '=', 'order.order_id')
            ->leftJoin('customer', 'customer.partner_id', '=', 'payment.payment_customer')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'payment.payment_dealer')
            ->leftJoin('insurance_comp', 'insurance_comp.insuranceco_id', '=', 'payment.payment_insuranco')
            ->where('payment.payment_cprofile', env('APP_ID'))
            ->where('payment.payment_status', 1)
            ->where('payment_detail.paymentline_status', 1);

            if($request->input('period_from')){
                $order_query->where('payment.payment_date', ">=", $request->input('period_from'));
            }
    
            if($request->input('period_to')){
                $order_query->where('payment.payment_date', "<=", $request->input('period_to'));
            }
        }

        $order_data = $order_query->get()->toArray();

        $listing_data = [];

        foreach($order_data as $dky => $dli){
            $listing_data[$dky] = $dli;

            if($request->input('insurance_type') == 'receipt'){
                if($dli['receipt_customer'] > 0){
                    $listing_data[$dky]['receive_from'] = $dli['partner_name'];
                }
                else{
                    $listing_data[$dky]['receive_from'] = $dli['dealer_name'];
                }
                
            }
            else{
                if($dli['payment_customer'] > 0){
                    $listing_data[$dky]['payment_to'] = $dli['partner_name'];
                }
                elseif($dli['payment_dealer'] > 0){
                    $listing_data[$dky]['payment_to'] = $dli['dealer_name'];
                }
                else{
                    $listing_data[$dky]['payment_to'] = $dli['insuranceco_name'];
                }
                if($dli['paymentline_order_id'] > 0){
                    $listing_data[$dky]['paid_amt'] = $dli['paymentline_offset_amount'];
                }
                else{
                    $listing_data[$dky]['paid_amt'] = $dli['payment_paid'];
                }
            }
        
        }

        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type');
            $excel_ctr->data_listing = array_values($listing_data);
            $excel_ctr->PremiumStateExcel();
        }
        else{
            $filename = 'Daily_transaction-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new DailyTransPDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->filter_by = $request->input('insurance_type');
            $pdf->data_listing = array_values($listing_data);
            $pdf->SetTitle("Daily Transaction Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function AccountStatementReport(Request $request){
        $order_query = Order::select("order.*", 'tsa.dealer_name', 'customer.partner_name')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_status', '1');

        if($request->input('insurance_type') == 'PCMI'){
            $order_query->where(function($query)
            {
                $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
            });
        }
        elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
            $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
        }

        if($request->input('order_customer')){
            $order_query->where('order.order_customer', $request->input('order_customer'));
        }

        if($request->input('order_dealer')){
            $order_query->where('order.order_dealer', $request->input('order_dealer'));
        }

        if($request->input('period_from')){
            $order_query->where('order.order_date', ">=", $request->input('period_from'));
        }

        if($request->input('period_to')){
            $order_query->where('order.order_date', "<=", $request->input('period_to'));
        }

        $order_data = $order_query->get()->toArray();

        $listing_data = [];

        if($request->input('insurance_type') == 'GMM'){
            $listing_data = $this->maidstateData($order_data);
        }
        else{
            $listing_data = $this->stateData($order_data);
        }

        if($request->input('soa_no')){
            $soa = $request->input('soa_no');
        }
        else{
            $soa = $this->generateCode();
        }
        
        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->order_customer = $request->input('order_customer');
            $excel_ctr->soa_no = $soa;
            $excel_ctr->data_listing = $listing_data;
            if($request->input('insurance_type') == 'GMM'){
                $excel_ctr->MaidaccstateExcel();
            }   
            else{
                $excel_ctr->accstateExcel();
            }
        }
        else{
            $filename = 'Account_statement_report-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            if($request->input('insurance_type') == 'GMM'){
                $pdf = new MaidAccountStatement();
            }   
            else{
                $pdf = new AccountStatementPDF();
            }
            
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->filter_by = $request->input('insurance_type')?:"ALL";
            $pdf->cus_id = $request->input('order_customer');
            $pdf->dealer_id = $request->input('order_dealer');
            $pdf->soa_no = $soa;
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("Account Statement Report", true);
            $pdf->renderPDF();
            // Output the PDF
            
            // $pdf->Output(public_path('genpdf/'.$filename), 'F');
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function maidstateData($order_data){
        $listing_data = [];
        foreach($order_data as $dky => $dli){
            $next_line = false;
            $sub_data = Maid::select('order_coverage', 'order_period', 'order_waiver_indemnity', 'order_optional_coverage_plan', 'order_optional_coverage_plan1','order_policyno', 'order_discharge_date', 'order_record_billto') 
            ->where('order_id', $dli['order_id'])
            ->first();
            $coverage = '';

            if($sub_data->order_coverage){
                $coverage .= "(".$this->maid_coverage_mapping[$sub_data->order_coverage].$sub_data->order_period.")";
            }

            if(isset($this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan]) && $this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan]){
                $coverage .= "(".$this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan].")";
            }

            if(isset($this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan1]) && $this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan1]){
                if($coverage){
                    $coverage .= "\n(".$this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan1].")";
                    $next_line = true;
                }
                else{
                    $coverage .= "(".$this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan1].")";
                }
            }

            if($sub_data->order_waiver_indemnity == 'Yes'){
                if($coverage){
                    if(!isset($this->maid_coverage_mapping[$sub_data->order_optional_coverage_plan1])){
                        $coverage .= "\n(W)";
                        $next_line = true;
                    }
                    else{
                        $coverage .= "(W)";
                    }
                }
                else{
                    $coverage .= "(W)";
                }
            }

            $dli['order_coverage'] = $coverage;
            $dli['next_line'] = $next_line;
            $dli['order_policyno'] = $sub_data->order_policyno;
            $dli['order_discharge_date'] = $sub_data->order_discharge_date;
            $dli['order_period'] = $sub_data->order_discharge_date;
            $dli['order_waiver_indemnity'] = $sub_data->order_waiver_indemnity;
            $dli['order_record_billto'] = $sub_data->order_record_billto;
            $dli['cus_refund'] = $sub_data->order_grosspremium_amount;
            $dli['total_refund'] = $sub_data->order_grosspremium_amount;
            
            // Debit note part
            $dli['order_tsa_pay'] = 0;
            $dli['order_cus_pay'] = 0;
            $dli['order_display_type'] = '';
            $dli['order_gross_premium'] = $dli['order_subtotal_amount'] + $dli['order_gst_amount'] + $dli['order_cover_plan_no_gst'];
            $dli['order_tsa_pay'] = $dli['order_gross_premium'] - $dli['order_receivable_dealercomm'] - $dli['order_receivable_dealergstamount']; 
            $dli['order_cus_pay'] = $dli['order_gross_premium'] - $dli['order_direct_discount_amt'];
            $dli['total_refund'] = $dli['order_gross_premium']; 
            $dli['tsa_refund'] = $dli['order_gross_premium'] - $dli['order_receivable_dealercomm'] - $dli['order_receivable_dealergstamount'];
            $dli['comm_cawl_back'] = $dli['order_receivable_dealercomm'] + $dli['order_receivable_dealergstamount'];  

            if($dli['order_type']){
                $dli['order_display_type'] = strtoupper(substr($dli['order_type'], 0, 1));
            }
            else{
                $dli['order_display_type'] = strtoupper(substr($dli['order_endorsement_type'], 0, 1));
            }
            if($dli['order_doc_type'] == 'DN'){
                $listing_data['DN'][] = $dli;
            }
            else{
                if($dli['order_record_billto'] == 'To TSA'){
                    $listing_data['CN']['TSA'][] = $dli;
                }
                else{
                    $listing_data['CN']['CUS'][] = $dli;
                }
            }
            
        }

        return $listing_data;
    }

    public function stateData($order_data){
        $listing_data = [];
        foreach($order_data as $dky => $dli){
            if(in_array($dli['order_prefix_type'],array("MI","PMI","CMI","PCMI"))){
                $sub_data = Motor::select('order_vehicles_no', 'order_policyno', 'order_record_billto')
                ->where('order_id', $dli['order_id'])
                ->first();
                $dli['order_record_billto'] = $sub_data->order_record_billto;
                $dli['order_vehicles_no'] = $sub_data->order_vehicles_no;
                $dli['order_policyno'] = $sub_data->order_policyno;
            }
            else{
                $sub_data = General::select('order_coverage', 'order_policyno', 'order_record_billto')
                ->where('order_id', $dli['order_id'])
                ->first();
                $plan = InsurancePlan::find($sub_data->order_coverage);
                $dli['order_record_billto'] = $sub_data->order_record_billto;
                $dli['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                $dli['order_policyno'] = $sub_data->order_policyno;
            }
            // Debit note part
            $dli['order_tsa_pay'] = 0;
            $dli['order_cus_pay'] = 0;
            $dli['order_display_type'] = '';
            $dli['order_gross_premium'] = $dli['order_subtotal_amount'] + $dli['order_gst_amount'];
            $dli['comm_cawl_back'] = $dli['order_receivable_dealercomm'] + $dli['order_receivable_dealergstamount']; 
            $dli['order_tsa_pay'] = $dli['order_gross_premium'] - $dli['order_receivable_dealercomm'] - $dli['order_receivable_dealergstamount']; 
            $dli['order_cus_pay'] = $dli['order_gross_premium'] - $dli['order_direct_discount_amt'];
            $dli['total_refund'] = $dli['order_gross_premium']; 
            $dli['tsa_refund'] = $dli['order_gross_premium'] - $dli['order_receivable_dealercomm'] - $dli['order_receivable_dealergstamount'];

            if($dli['order_type']){
                $dli['order_display_type'] = strtoupper(substr($dli['order_type'], 0, 1));
            }
            else{
                $dli['order_display_type'] = strtoupper(substr($dli['order_endorsement_type'], 0, 1));
            }
            if($dli['order_doc_type'] == 'DN'){
                $listing_data['DN'][] = $dli;
            }
            else{
                $listing_data['CN'][] = $dli;
            }
        }

        return $listing_data;
    }

    public function SalesReport(Request $request){
        $sub_sql = "SELECT SUM(rc.recpline_offset_amount) as total FROM ens_receipt_detail rc INNER JOIN ens_receipt r ON r.receipt_id = rc.recpline_receipt_id WHERE rc.recpline_order_id = ens_order.order_id AND r.receipt_status = '1' ";

        $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'), 'tsa.dealer_name', 'customer.partner_name', 'insurance_comp.insuranceco_name')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->leftJoin('insurance_comp', 'insurance_comp.insuranceco_id', '=', 'order.order_insurco')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_status', '1');

        if($request->input('insurance_type') == 'PCMI'){
            $order_query->where(function($query)
            {
                $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
            });
        }
        elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
            $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
        }

        if($request->input('insurance_company')){
            $order_query->where('order.order_insurco', $request->input('insurance_company'));
        }

        if($request->input('order_dealer')){
            $order_query->where('order.order_dealer', $request->input('order_dealer'));
        }

        if($request->input('order_customer')){
            $order_query->where('order.order_customer', $request->input('order_customer'));
        }

        if($request->input('period_from')){
            $order_query->where('order.order_date', ">=", $request->input('period_from'));
        }

        if($request->input('period_to')){
            $order_query->where('order.order_date', "<=", $request->input('period_to'));
        }

        $order_data = $order_query->get()->toArray();

        $listing_data = [];

        foreach($order_data as $dky => $row){
            $coverage = "";
            $bill_to = "";
            if($row['order_prefix_type'] == "GI"){
                $gen_info = General::where('order_id', $row['order_id'])->first()->toArray();
                $plan = InsurancePlan::find($gen_info['order_coverage']);
                $row['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                $bill_to = $gen_info['order_record_billto'];
                $row['related_info'] = "";
                $row['order_policyno'] = $gen_info['order_policyno'];
            }
            else if($row['order_prefix_type'] == "GMM"){
                $maid_info = Maid::where('order_id', $row['order_id'])->first()->toArray();
                $bill_to = $maid_info['order_record_billto'];
                if(isset($this->maid_coverage_mapping[$maid_info['order_coverage']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_coverage']].$maid_info['order_period'].")";
                }
                if(isset($this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan']].$maid_info['order_period'].")";
                }
                if(isset($this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan1']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan1']].")";
                }

                $row['order_coverage'] = $coverage;
                $row['related_info'] = $maid_info['order_maid_name'];
                $row['order_policyno'] = $maid_info['order_policyno'];
            }
            else{
                $motor_info = Motor::where('order_id', $row['order_id'])->first()->toArray();
                $bill_to = $motor_info['order_record_billto'];
                $row['order_coverage'] = $motor_info['order_coverage'];
                $row['related_info'] = $motor_info['order_vehicles_no'];
                $row['order_policyno'] = $motor_info['order_policyno'];
            }
            
            if($row['order_type']){
                $row['order_display_type'] = strtoupper(substr($row['order_type'], 0, 1));
            }
            else{
                $row['order_display_type'] = strtoupper(substr($row['order_endorsement_type'], 0, 1));
            }
            
            $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'] + $row['order_cover_plan_no_gst'];
            
            if($row['order_doc_type']=="CN"){
                if($bill_to =="To Customer"){
                    $row['order_tsa_pay'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];
                }
                else{
                    $row['order_tsa_pay'] = $row['order_gross_premium'] - $row['order_receivable_dealercomm'] - $row['order_receivable_dealergstamount'];
                }
            }
            else{
                $row['order_tsa_pay'] = $row['order_gross_premium'] - $row['order_receivable_dealercomm'] - $row['order_receivable_dealergstamount'];  
            }

            $row['total_tsa_comm'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];
            $row['to_ins_cheque'] = $this->getPaymentcheque($row['order_id'], 'insurance');
            $row['tsa_comm_cheque'] = $this->getPaymentcheque($row['order_id'], 'dealer', $row['total_tsa_comm']);
            $listing_data[$row['order_prefix_type']][] = $row;
        }

        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type')?:"ALL";
            $excel_ctr->order_insurco = $request->input('order_insurco');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->order_customer = $request->input('order_customer');
            $excel_ctr->data_listing = $listing_data;
            $excel_ctr->SalesReportExcel();
        }
        else{
            $filename = 'Sales_report-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new SalesReportPDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->insurance_type = $request->input('insurance_type')?:"ALL";
            $pdf->order_insurco = $request->input('order_insurco');
            $pdf->order_dealer = $request->input('order_dealer');
            $pdf->order_customer = $request->input('order_customer');
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("Sales Report", true);
            $pdf->renderPDF();
            // Output the PDF
            
            // $pdf->Output(public_path('genpdf/'.$filename), 'F');
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function TSASalesReport(Request $request){
        $sub_sql = "SELECT SUM(rc.recpline_offset_amount) as total FROM ens_receipt_detail rc INNER JOIN ens_receipt r ON r.receipt_id = rc.recpline_receipt_id WHERE rc.recpline_order_id = ens_order.order_id AND r.receipt_status = '1' ";

        $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'), 'tsa.dealer_name', 'customer.partner_name')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_status', '1');

        if($request->input('insurance_type') == 'PCMI'){
            $order_query->where(function($query)
            {
                $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
            });
        }
        elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
            $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
        }

        if($request->input('insurance_company')){
            $order_query->where('order.order_insurco', $request->input('insurance_company'));
        }

        if($request->input('order_dealer')){
            $order_query->where('order.order_dealer', $request->input('order_dealer'));
        }

        if($request->input('order_customer')){
            $order_query->where('order.order_customer', $request->input('order_customer'));
        }

        if($request->input('period_from')){
            $order_query->where('order.order_date', ">=", $request->input('period_from'));
        }

        if($request->input('period_to')){
            $order_query->where('order.order_date', "<=", $request->input('period_to'));
        }

        $order_data = $order_query->get()->toArray();

        $listing_data = [];

        foreach($order_data as $dky => $row){
            $coverage = "";
            $bill_to = "";
            if($row['order_prefix_type'] == "GI"){
                $gen_info = General::where('order_id', $row['order_id'])->first()->toArray();
                $plan = InsurancePlan::find($gen_info['order_coverage']);
                $row['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                $bill_to = $gen_info['order_record_billto'];
                $row['related_info'] = "";
                $row['order_policyno'] = $gen_info['order_policyno'];
            }
            else if($row['order_prefix_type'] == "GMM"){
                $maid_info = Maid::where('order_id', $row['order_id'])->first()->toArray();
                $bill_to = $maid_info['order_record_billto'];
                if(isset($this->maid_coverage_mapping[$maid_info['order_coverage']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_coverage']].$maid_info['order_period'].")";
                }
                if(isset($this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan']].$maid_info['order_period'].")";
                }
                if(isset($this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan1']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan1']].")";
                }

                $row['order_coverage'] = $coverage;
                $row['related_info'] = $maid_info['order_maid_name'];
                $row['order_policyno'] = $maid_info['order_policyno'];
            }
            else{
                $motor_info = Motor::where('order_id', $row['order_id'])->first()->toArray();
                $bill_to = $motor_info['order_record_billto'];
                $row['related_info'] = $motor_info['order_vehicles_no'];
                $row['order_policyno'] = $motor_info['order_policyno'];
            }
            
            if($row['order_type']){
                $row['order_display_type'] = strtoupper(substr($row['order_type'], 0, 1));
            }
            else{
                $row['order_display_type'] = strtoupper(substr($row['order_endorsement_type'], 0, 1));
            }
            
            $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'] + $row['order_cover_plan_no_gst'];
            
            if($row['order_doc_type']=="CN"){
                if($bill_to =="To Customer"){
                    $row['order_tsa_pay'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];
                }
                else{
                    $row['order_tsa_pay'] = $row['order_gross_premium'] - $row['order_receivable_dealercomm'] - $row['order_receivable_dealergstamount'];
                }
            }
            else{
                $row['order_tsa_pay'] = $row['order_gross_premium'] - $row['order_receivable_dealercomm'] - $row['order_receivable_dealergstamount'];  
            }

            $row['total_tsa_comm'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];
            $row['to_ins_cheque'] = $this->getPaymentcheque($row['order_id'], 'insurance');
            $row['tsa_comm_cheque'] = $this->getPaymentcheque($row['order_id'], 'dealer', $row['total_tsa_comm']);
            $listing_data[$row['order_prefix_type']][] = $row;
        }

        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type')?:"ALL";
            $excel_ctr->order_insurco = $request->input('order_insurco');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->order_customer = $request->input('order_customer');
            $excel_ctr->data_listing = $listing_data;
            $excel_ctr->TSASalesReportExcel();
        }
        else{
            $filename = 'TSA_Sales_report-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new TsaSalesReportPDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->insurance_type = $request->input('insurance_type')?:"ALL";
            $pdf->order_insurco = $request->input('order_insurco');
            $pdf->order_dealer = $request->input('order_dealer');
            $pdf->order_customer = $request->input('order_customer');
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("TSA Sales Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function CusSalesReport(Request $request){
        $sub_sql = "SELECT SUM(rc.recpline_offset_amount) as total FROM ens_receipt_detail rc INNER JOIN ens_receipt r ON r.receipt_id = rc.recpline_receipt_id WHERE rc.recpline_order_id = ens_order.order_id AND r.receipt_status = '1' ";

        $order_query = Order::select("order.*", DB::raw('COALESCE(('.$sub_sql.'),0) as paid_total'), 'tsa.dealer_name', 'customer.partner_name')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_status', '1');

        if($request->input('insurance_type') == 'PCMI'){
            $order_query->where(function($query)
            {
                $query->where('order.order_prefix_type', 'LIKE', 'CMI')
                ->orWhere('order.order_prefix_type', 'LIKE', 'PMI');
            });
        }
        elseif($request->input('insurance_type') && $request->input('insurance_type') != 'ALL'){
            $order_query->where('order.order_prefix_type', $request->input('insurance_type'));
        }

        if($request->input('insurance_company')){
            $order_query->where('order.order_insurco', $request->input('insurance_company'));
        }

        if($request->input('order_dealer')){
            $order_query->where('order.order_dealer', $request->input('order_dealer'));
        }

        if($request->input('order_customer')){
            $order_query->where('order.order_customer', $request->input('order_customer'));
        }

        if($request->input('period_from')){
            $order_query->where('order.order_date', ">=", $request->input('period_from'));
        }

        if($request->input('period_to')){
            $order_query->where('order.order_date', "<=", $request->input('period_to'));
        }

        $order_data = $order_query->get()->toArray();

        $listing_data = [];

        foreach($order_data as $dky => $row){
            $coverage = "";
            $bill_to = "";
            if($row['order_prefix_type'] == "GI"){
                $gen_info = General::where('order_id', $row['order_id'])->first()->toArray();
                $plan = InsurancePlan::find($gen_info['order_coverage']);
                $row['order_coverage'] = isset($plan->insuranceclass_code)?$plan->insuranceclass_code:'';
                $bill_to = $gen_info['order_record_billto'];
                $row['related_info'] = "";
                $row['order_policyno'] = $gen_info['order_policyno'];
            }
            else if($row['order_prefix_type'] == "GMM"){
                $maid_info = Maid::where('order_id', $row['order_id'])->first()->toArray();
                $bill_to = $maid_info['order_record_billto'];
                if(isset($this->maid_coverage_mapping[$maid_info['order_coverage']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_coverage']].$maid_info['order_period'].")";
                }
                if(isset($this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan']].$maid_info['order_period'].")";
                }
                if(isset($this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan1']])){
                    $coverage .= "(".$this->maid_coverage_mapping[$maid_info['order_optional_coverage_plan1']].")";
                }

                $row['order_coverage'] = $coverage;
                $row['related_info'] = $maid_info['order_maid_name'];
                $row['order_policyno'] = $maid_info['order_policyno'];
            }
            else{
                $motor_info = Motor::where('order_id', $row['order_id'])->first()->toArray();
                $bill_to = $motor_info['order_record_billto'];
                $row['related_info'] = $motor_info['order_vehicles_no'];
                $row['order_policyno'] = $motor_info['order_policyno'];
            }
            
            if($row['order_type']){
                $row['order_display_type'] = strtoupper(substr($row['order_type'], 0, 1));
            }
            else{
                $row['order_display_type'] = strtoupper(substr($row['order_endorsement_type'], 0, 1));
            }
            
            $row['order_gross_premium'] = $row['order_subtotal_amount'] + $row['order_gst_amount'] + $row['order_cover_plan_no_gst'];
            
            if($row['order_doc_type']=="CN"){
                if($bill_to =="To Customer"){
                    $row['order_tsa_pay'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];
                }
                else{
                    $row['order_tsa_pay'] = $row['order_gross_premium'] - $row['order_receivable_dealercomm'] - $row['order_receivable_dealergstamount'];
                }
            }
            else{
                $row['order_tsa_pay'] = $row['order_gross_premium'] - $row['order_receivable_dealercomm'] - $row['order_receivable_dealergstamount'];  
            }

            $row['total_tsa_comm'] = $row['order_receivable_dealercomm'] + $row['order_receivable_dealergstamount'];
            $row['to_ins_cheque'] = $this->getPaymentcheque($row['order_id'], 'insurance');
            $row['tsa_comm_cheque'] = $this->getPaymentcheque($row['order_id'], 'dealer', $row['total_tsa_comm']);
            $listing_data[$row['order_prefix_type']][] = $row;
        }

        if($request->input('excel_export') == 1){
            $excel_ctr = new ExcelController();
            $excel_ctr->period_from = $request->input('period_from');
            $excel_ctr->period_to = $request->input('period_to');
            $excel_ctr->insurance_type = $request->input('insurance_type')?:"ALL";
            $excel_ctr->order_insurco = $request->input('order_insurco');
            $excel_ctr->order_dealer = $request->input('order_dealer');
            $excel_ctr->order_customer = $request->input('order_customer');
            $excel_ctr->data_listing = $listing_data;
            $excel_ctr->CusSalesReportExcel();
        }
        else{
            $filename = 'Customer_Sales_report-'.strtoupper(date('d-M-Y')).".pdf";
            // Create instance of PDF class
            $pdf = new CusSalesReportPDF();
            $pdf->period_from = $request->input('period_from');
            $pdf->period_to = $request->input('period_to');
            $pdf->insurance_type = $request->input('insurance_type')?:"ALL";
            $pdf->order_insurco = $request->input('order_insurco');
            $pdf->order_dealer = $request->input('order_dealer');
            $pdf->order_customer = $request->input('order_customer');
            $pdf->data_listing = $listing_data;
            $pdf->SetTitle("Customer Sales Report", true);
            $pdf->renderPDF();
            // Output the PDF
            return response($pdf->Output('S'))
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        }
    }

    public function getPaymentcheque($order_id, $type="", $comm = "" ){
        $payment_query = Payment::select('payment.payment_cheque', 'payment_detail.paymentline_order_amount')
        ->join('payment_detail', 'payment_detail.paymentline_payment_id', '=', 'payment.payment_id')
        ->where('payment.payment_status', 1)
        ->where('payment_detail.paymentline_order_id ', $order_id);

        if($type){
            $payment_query->where('payment.payment_type', $type);
        }

        $payment_query->first();

        if($comm){
            if($comm == $payment_query->paymentline_order_amount){
                return $payment_query->payment_cheque;
            }
            else{
                return '';
            }
        }

        return $payment_query->payment_cheque;
    }

    public function generateCode($prefix ="SOA"){
        $ref = Refno::where('prefix_code', $prefix)
        ->where('cprofile', env("APP_CODE"))
        ->first();
        if($ref){
            $last_id = $ref->last_no;
            $last_id ++;
        }
        else{
            $new_ref = new Refno();
            $new_ref->prefix_code = $prefix;
            $new_ref->cprofile = env("APP_CODE");
            $new_ref->last_no = 0;
            $new_ref->save();
            $last_id = 1;
        }

        $code =  env("APP_CODE").$prefix .'-'. sprintf("%06d", $last_id);

        return $code; 
    }

    public function updateRefCode($order_ref, $prefix){
        if($order_ref == 0){
            $ref = Refno::where('prefix_code', $prefix)
            ->where('cprofile', env("APP_CODE"))
            ->first();
            $ref->last_no =  $ref->last_no + 1;
            $ref->save();
        }
    }
}
