<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\User;
use App\Models\Empl;
use App\Models\Files;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    //

    public function getListing(Request $request){
        return view('customer.List');
    }

    public function getListData(Request $request){
         // Total records
        $order_fields = ['','partner_name','partner_nirc','partner_tel','partner_house_no',''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $totalRecords = Customer::select('count(*) as allcount')->where('partner_status', "!=","0")->count();
        $totalRecordswithFilter = Customer::select('count(*) as allcount')
        ->where(function ($query) use ($searchValue) {
            $query->where('partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('partner_tel', 'like', '%' . $searchValue . '%')
                ->orWhere('partner_house_no', 'like', '%' . $searchValue . '%')
                ->orWhere('partner_nirc', 'like', '%' . $searchValue . '%');
        })
        ->where('partner_status', "!=","0")->count();
        $listing_data = [];

        $main_query = Customer::where('partner_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('partner_tel', 'like', '%' . $searchValue . '%')
                ->orWhere('partner_house_no', 'like', '%' . $searchValue . '%')
                ->orWhere('partner_nirc', 'like', '%' . $searchValue . '%');
        })
        ->where('partner_status', "!=","0");

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }
       
        $listing_data = $main_query->skip($start)
        ->take($rowperpage)
        ->get()->toArray();
        $out_data = [];

        foreach($listing_data as $ky => $li){
            $action = '';
            $action .= '<a href="'.route('CustomerInfo', ['id' => $li['partner_id'] ]).'" class="btn btn-warning">Edit</a>';
            $action .= '<a href="'.route('CustomerDelete', ['id' => $li['partner_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['partner_name'].'">Delete</a>';
            
            $out_data[] = [
                $ky + 1,
                $li['partner_name']?:'-',
                substr_replace($li['partner_nirc'],'XXXX', 1,-4),
                $li['partner_tel']?:'-',
                $li['partner_house_no']?:'-',
                $action
            ];
        }
 
         $output = [
             "draw" => intval($draw),
             "iTotalRecords" => $totalRecords,
             "iTotalDisplayRecords" => $totalRecordswithFilter,
             "aaData" => $out_data,
         ];
 
         return response()->json($output, 200); 
    }

    public function CustomerInfo(Request $request){
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        if($request->route('id')){
            $data = Customer::where('partner_id',$request->route('id'))->where('partner_status', '1')->first();
            if(isset($data->partner_id)){
                $data->attachment = $this->getAttachment($request->route('id'));
                $data->country_list = $country_list;
                return view('customer.Info', $data);
            }
            else{
                return redirect()->route('CustomerList');
            }
        }
        else{
            $data = [
                'country_list' => $country_list,
            ];
            return view('customer.Add', $data);
        }
    }

    public function create(Request $request){
        $rules = [
            'partner_name'  => 'required|min:3',
            'partner_nirc'  => 'required|min:3|unique:customer,partner_nirc',
        ];

        $attr = [
            'partner_name' => 'Customer Name',
            'partner_nirc' => 'Customer NIRC',
        ];

        $request->validate($rules, [], $attr);

        $errors_input = [];

        if ($request->hasFile('partner_file')) {
            if($request->file('partner_file')->getSize() > 5242880 ){
                $errors_input['partner_file'] = 'Attachment Cannot over 5MB';
            }
            else{
                $filename = $this->rename_filename($request->file('partner_file')->getClientOriginalName());
            }
        }

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $cus = new Customer();
        $cus->partner_name = $request->input('partner_name');
        $cus->partner_nirc = $request->input('partner_nirc');
        $cus->partner_type = $request->input('partner_type');
        $cus->partner_blk_no = $request->input('partner_blk_no');
        $cus->partner_street = $request->input('partner_street');
        $cus->partner_unit_no = $request->input('partner_unit_no');
        $cus->partner_outlet = $request->input('partner_outlet');
        $cus->partner_postal_code = $request->input('partner_postal_code');
        $cus->partner_bil_blk_no = $request->input('partner_bil_blk_no');
        $cus->partner_bil_street = $request->input('partner_bil_street');
        $cus->partner_bil_unit_no = $request->input('partner_bil_unit_no');
        $cus->partner_bil_country = $request->input('partner_bil_country');
        $cus->partner_bil_postalcode = $request->input('partner_bil_postalcode');
        $cus->partner_co = $request->input('partner_co');
        $cus->partner_status = $request->input('partner_status');
        $cus->partner_tel = $request->input('partner_tel');
        $cus->partner_tel2 = $request->input('partner_tel2');
        $cus->partner_house_no = $request->input('partner_house_no');
        $cus->partner_fax = $request->input('partner_fax');
        $cus->partner_dob = $request->input('partner_dob');
        $cus->partner_email = $request->input('partner_email');
        $cus->partner_marital = $request->input('partner_marital');
        $cus->partner_income = $request->input('partner_income');
        $cus->partner_drivingpass = $request->input('partner_drivingpass');
        $cus->partner_drivingpassexpiry = $request->input('partner_drivingpassexpiry');
        $cus->partner_gender = $request->input('partner_gender');
        $cus->partner_occupation = $request->input('partner_occupation');
        $cus->partner_country = $request->input('partner_country');
        $cus->partner_employername = $request->input('partner_employername');
        $cus->partner_employeraddress = $request->input('partner_employeraddress');
        $cus->partner_remark = $request->input('partner_remark');
        $cus->partner_recommended = $request->input('partner_recommended');
        $cus->partner_primarybusinesstype = $request->input('partner_primarybusinesstype');
        $cus->partner_secondbusinesstype = $request->input('partner_secondbusinesstype');
        $cus->partner_note = $request->input('partner_note');
        $cus->insertBy = Auth::user()->id;
        $cus->updateBy = Auth::user()->id;
        $cus->save();

        if($filename != ''){
            $new_file = new Files();
            $new_file->files_ref_table = 'ens_customer';
            $new_file->files_ref_id = $cus->partner_id;
            $new_file->files_name = $cus->partner_id."_".$filename;
            $new_file->files_ori_name = $request->file('partner_file')->getClientOriginalName();
            $new_file->file_type = $request->file('partner_file')->extension();
            $new_file->upload_field = 0;
            $new_file->seq_no = 0;
            $new_file->status = 1;
            $new_file->insertBy = Auth::user()->id;
            $new_file->updateBy = Auth::user()->id;
            $new_file->save();
            $request->file('partner_file')->move('uploads/customer', $cus->partner_id."_".$filename);
        }

        return redirect()->route('CustomerList')->with('success_msg', 'Add Customer Succesfully');
    }

    public function update(Request $request){
        $exist_cus = Customer::find($request->input('partner_id'));
        if(!$exist_cus){
            return redirect()->route('CustomerList')->with('error_msg', 'Invalid Customer');
        }
        else{
            $rules = [
                'partner_name'  => 'required|min:3',
                'partner_nirc'  => 'required|min:3|unique:customer,partner_nirc,'.$request->input('partner_id').',partner_id',
            ];

            $attr = [
                'partner_name' => 'Customer Name',
                'partner_nirc' => 'Customer NIRC',
            ];

            $request->validate($rules, [], $attr);

            $errors_input = [];

            if ($request->hasFile('partner_file')) {
                if($request->file('partner_file')->getSize() > 5242880 ){
                    $errors_input['partner_file'] = 'Attachment Cannot over 5MB';
                }
                else{
                    $filename = $this->rename_filename($request->file('partner_file')->getClientOriginalName());
                }
            }

            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }

            $exist_cus->partner_name = $request->input('partner_name');
            $exist_cus->partner_nirc = $request->input('partner_nirc');
            $exist_cus->partner_type = $request->input('partner_type');
            $exist_cus->partner_blk_no = $request->input('partner_blk_no');
            $exist_cus->partner_street = $request->input('partner_street');
            $exist_cus->partner_unit_no = $request->input('partner_unit_no');
            $exist_cus->partner_outlet = $request->input('partner_outlet');
            $exist_cus->partner_postal_code = $request->input('partner_postal_code');
            $exist_cus->partner_bil_blk_no = $request->input('partner_bil_blk_no');
            $exist_cus->partner_bil_street = $request->input('partner_bil_street');
            $exist_cus->partner_bil_unit_no = $request->input('partner_bil_unit_no');
            $exist_cus->partner_bil_country = $request->input('partner_bil_country');
            $exist_cus->partner_bil_postalcode = $request->input('partner_bil_postalcode');
            $exist_cus->partner_co = $request->input('partner_co');
            $exist_cus->partner_status = $request->input('partner_status');
            $exist_cus->partner_tel = $request->input('partner_tel');
            $exist_cus->partner_tel2 = $request->input('partner_tel2');
            $exist_cus->partner_house_no = $request->input('partner_house_no');
            $exist_cus->partner_fax = $request->input('partner_fax');
            $exist_cus->partner_dob = $request->input('partner_dob');
            $exist_cus->partner_email = $request->input('partner_email');
            $exist_cus->partner_marital = $request->input('partner_marital');
            $exist_cus->partner_income = $request->input('partner_income');
            $exist_cus->partner_drivingpass = $request->input('partner_drivingpass');
            $exist_cus->partner_drivingpassexpiry = $request->input('partner_drivingpassexpiry');
            $exist_cus->partner_gender = $request->input('partner_gender');
            $exist_cus->partner_occupation = $request->input('partner_occupation');
            $exist_cus->partner_country = $request->input('partner_country');
            $exist_cus->partner_employername = $request->input('partner_employername');
            $exist_cus->partner_employeraddress = $request->input('partner_employeraddress');
            $exist_cus->partner_remark = $request->input('partner_remark');
            $exist_cus->partner_recommended = $request->input('partner_recommended');
            $exist_cus->partner_primarybusinesstype = $request->input('partner_primarybusinesstype');
            $exist_cus->partner_secondbusinesstype = $request->input('partner_secondbusinesstype');
            $exist_cus->partner_note = $request->input('partner_note');
            $exist_cus->updateBy = Auth::user()->id;
            $exist_cus->save();

            if($filename != ''){
                $exist_file = Files::where('files_ref_table','ens_customer')->where('files_ref_id', $exist_cus->partner_id)->first();
                if(!$exist_file){
                    $new_file = new Files();
                    $new_file->files_ref_table = 'ens_customer';
                    $new_file->files_ref_id = $exist_cus->partner_id;
                    $new_file->files_name = $exist_cus->partner_id."_".$filename;
                    $new_file->files_ori_name = $request->file('partner_file')->getClientOriginalName();
                    $new_file->file_type = $request->file('partner_file')->extension();
                    $new_file->upload_field = 0;
                    $new_file->seq_no = 0;
                    $new_file->status = 1;
                    $new_file->insertBy = Auth::user()->id;
                    $new_file->updateBy = Auth::user()->id;
                    $new_file->save();
                    $request->file('partner_file')->move('uploads/customer', $exist_cus->partner_id."_".$filename);
                }
                else{
                    $exist_file->files_name = $exist_cus->partner_id."_".$filename;
                    $exist_file->files_ori_name = $request->file('partner_file')->getClientOriginalName();
                    $exist_file->file_type = $request->file('partner_file')->extension();
                    $exist_file->save();
                    $request->file('partner_file')->move('uploads/customer', $exist_cus->partner_id."_".$filename);
                }
            }
            return redirect()->back()
            ->with('sweet_success_msg','Update Success');
        }
    }
    
    public function delete(Request $request){
        $exist_cus = Customer::find($request->route('id'));
        if($exist_cus){
            $exist_cus->partner_status = "0";
            $exist_cus->save();
        }
       
        return redirect()->route('CustomerList')->with('success_msg', 'Delete Customer Succesfully');
    }

    public function rename_filename($string){
        $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
        
        return preg_replace('/[^A-Za-z0-9\-._]/', '_', $string); // Removes special chars.
    }

    public function getAttachment($cus_id){
        $file = Files::where('files_ref_id', $cus_id)->where('files_ref_table', 'ens_customer')->where('status', 1)->first();

        return isset($file->files_name)?$file->files_name:'';
    }
}
