<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Sompo;
use App\Models\SompoData;
use App\Models\SompoDriver;
use App\Models\FinanceClass;
use App\Models\Order;
use App\Models\Country;
use App\Models\Refno;
use App\Models\TSA;
use App\Models\Vehicles;
use App\Models\HiresComp;
use App\Models\Empl;
use App\Models\User;
use App\Models\Customer;
use App\Models\Motor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SompoController extends Controller
{
    //
    public $motor_prefix = "MI";
    public $motor_order_prefix = "CY";

    public function getListing(Request $request){
        return view('sompo.List');
    }

    public function getListData(Request $request){
        $order_fields = ['','order_vehicles_no','order_no','order_policyno','order_cus_name','order_cus_nirc','order_period_from','order_period_to',''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $count_query = Sompo::select('count(*) as allcount')
        ->leftJoin('order', 'order.order_id', '=', 'insurance_sompo.order_id')
        ->where('order_sompo_status', "!=","0");

        $main_query = Sompo::select('insurance_sompo.*', 'order.order_no')
        ->leftJoin('order', 'order.order_id', '=', 'insurance_sompo.order_id')
        ->where('insurance_sompo.order_sompo_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('insurance_sompo.order_vehicles_no', 'like', '%' . $searchValue . '%');
        });

        if(Auth::user()->ref_code == 'TSA'){
            $count_query->where('insurance_sompo.insertBy', Auth::user()->id);
            $main_query->where('insurance_sompo.insertBy', Auth::user()->id);
        }

        $totalRecords = $count_query->count();
        $totalRecordswithFilter = $count_query
        ->where(function ($query) use ($searchValue) {
            $query->where('order_vehicles_no', 'like', '%' . $searchValue . '%');
        })
        ->count();

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }

        $listing_data = $main_query
        ->skip($start)
        ->take($rowperpage)
        ->get()->toArray();

        $out_data = [];
        
        foreach($listing_data as $ky => $li){
            $action = "";
            if($li['order_sompo_status'] == 1){
                if(UserGroupPrivilegeController::checkPrivilegeWithAuth("SompoView")){
                    $action .= '<a href="'.route('SompoView', ['id' => $li['order_sompo_id'] ]).'" class="btn btn-warning">Edit</a>';
                }

                if(UserGroupPrivilegeController::checkPrivilegeWithAuth("SompoDelete")){
                    $action .= '<a href="'.route('SompoDelete', ['id' => $li['order_sompo_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['order_policyno'].'">Delete</a>';
                }
            }
            else{
                if(UserGroupPrivilegeController::checkPrivilegeWithAuth("SompoView")){
                    $action .= '<a href="'.route('SompoView', ['id' => $li['order_sompo_id'] ]).'" class="btn btn-secondary">View</a>';
                }
                if($li['order_ref'] == 0 && UserGroupPrivilegeController::checkPrivilegeWithAuth("SompoCopy")){
                    $action .= '<a href="'.route('SompoCopy', ['id' => $li['order_sompo_id'] ]).'" class="btn btn-success">Copy to Local</a>';
                }
            }
            
            $out_data[] = [
                $ky + 1,
                $li['order_vehicles_no']?:'-',
                $li['order_no']?:'-',
                $li['order_policyno']?:'-',
                $li['order_cus_name'],
                substr_replace($li['order_cus_nirc'],'XXXX', 1,-4),
                $li['order_period_from']?:'-',
                $li['order_period_to']?:'-',
                $action
            ];
        }
       
        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200); 
    }

    public function getSompoInfo(Request $request){
        $nation = $this->getSompoData("NATIONALITY");
        $occupation = $this->getSompoData("OCCUPATION_LIST", true);
        $vehicles = $this->getSompoData("PQ_VEHICLE_MATRIX", true);
        $ncd = $this->getSompoData("MT_NCD");
        $financecode = $this->getSompoData("HP_NAME", true);
        $pre_financecode = $this->getSompoData("PREVIOUS_INS_COMP", true);
        $coverage_plan = $this->getSompoData("POLICY_TYPE", true);
        $relationship = $this->getSompoData("RELATIONSHIP", true);
        $identity = $this->getSompoData("IDENTITY_TYPE", true);
        $maritial = $this->getSompoData("MARITIAL_STATUS");
        
        if($request->route('id')){
            $info_data = $this->getSompoDetails($request->route('id'));
            
            if($info_data){
                $info_data->disabled = "";
                $info_data->country_list = $nation;
                $info_data->occupation_list = $occupation;
                $info_data->vehicles_list = $vehicles;
                $info_data->ncd_list = $ncd;
                $info_data->coverage_list = $coverage_plan;
                $info_data->maritial_list = $maritial;
                $info_data->relationship_list = $relationship;
                $info_data->identity_list = $identity;
                $info_data->financecode_list = $financecode;
                $info_data->pre_financecode_list = $pre_financecode;
                $info_data->min_age = date('Y-m-d', strtotime(Carbon::now()->subYears(70)));
                $info_data->max_age = date('Y-m-d', strtotime(Carbon::now()->subYears(18)));
                return view('sompo.Info', $info_data);
            }
            else{
                return redirect()->route('SompoList');
            }
        }
        else{
            $output = [
                'country_list' => $nation,
                'occupation_list' => $occupation,
                'vehicles_list' => $vehicles,
                'ncd_list' => $ncd,
                'coverage_list' => $coverage_plan,
                'maritial_list' => $maritial,
                'relationship_list' => $relationship,
                'identity_list' => $identity,
                'financecode_list' => $financecode,
                'pre_financecode_list' => $pre_financecode,
                'min_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(70))),
                'max_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(18))),
            ];
            return view('sompo.Add', $output);
        }
    }

    public function getSompoViewInfo(Request $request){
        $nation = $this->getSompoData("NATIONALITY");
        $occupation = $this->getSompoData("OCCUPATION_LIST", true);
        $vehicles = $this->getSompoData("PQ_VEHICLE_MATRIX", true);
        $ncd = $this->getSompoData("MT_NCD");
        $financecode = $this->getSompoData("HP_NAME", true);
        $pre_financecode = $this->getSompoData("PREVIOUS_INS_COMP", true);
        $coverage_plan = $this->getSompoData("POLICY_TYPE", true);
        $relationship = $this->getSompoData("RELATIONSHIP", true);
        $identity = $this->getSompoData("IDENTITY_TYPE", true);
        $maritial = $this->getSompoData("MARITIAL_STATUS");
        
        if($request->route('id')){
            $info_data = $this->getSompoDetails($request->route('id'));
            if($info_data){
                $info_data->disabled = "DISABLED";
                $info_data->country_list = $nation;
                $info_data->occupation_list = $occupation;
                $info_data->vehicles_list = $vehicles;
                $info_data->ncd_list = $ncd;
                $info_data->coverage_list = $coverage_plan;
                $info_data->maritial_list = $maritial;
                $info_data->relationship_list = $relationship;
                $info_data->identity_list = $identity;
                $info_data->financecode_list = $financecode;
                $info_data->pre_financecode_list = $pre_financecode;
                $info_data->min_age = date('Y-m-d', strtotime(Carbon::now()->subYears(70)));
                $info_data->max_age = date('Y-m-d', strtotime(Carbon::now()->subYears(18)));
                return view('sompo.Info', $info_data);
            }
            else{
                return redirect()->route('SompoList');
            }
        }
    }

    public function getSompoDetails($sompo_id){
        $data = Sompo::find($sompo_id);
        $data->driver_list = $this->getSompoDriverList($sompo_id);
        return $data;
    }

    public function getSompoDriverList($sompo_id){
        $driver = SompoDriver::where('sompo_driver_order_id', $sompo_id)
        ->where('sompo_driver_status', 1)
        ->orderBy('sompo_driver_seqno', 'ASC')
        ->get()->toArray();
        return $driver;
    }

    public function create(Request $request){
        $rules = [
            'order_record_billto' => 'required',
            'order_period_from' => 'required|date|after_or_equal:today',
            'order_period_to' => 'required|date|after:order_period_from',
            'order_cus_name' => 'required',
            'order_cus_nirc' => 'required',
            'order_cus_occ_code' => 'required',
            'order_cus_nirc_type' => 'required',
            'order_cus_unitno' => 'required',
            'order_cus_email' => 'required|email',
            'order_tsa_email' => 'required|email',
            'order_cus_national' => 'required',
            'order_cus_contact' => 'required|numeric',
            'order_cus_gender' => 'required',
            'order_cus_dob' => 'required',
            'order_cus_marital' => 'required',
            'order_cus_postal_code' => 'required|digits:6',
            'order_cus_address' => 'required',
            'order_coverage' => 'required',
            'order_vehicle_type' => 'required',
            'order_vehicles_no' => 'required',
            'order_vehicles_reg_year' => 'required',
            'order_vehicles_pro_year' => 'required|numeric',
            'order_vehicles_engine' => 'required',
            'order_vehicles_chasis' => 'required',
            'order_vehicles_capacity' => 'required|numeric',
            'order_sompo_payment_method' => 'required',
            'order_cus_claim_amount' => 'nullable|numeric',
            'order_cus_claim' => 'nullable|numeric',
            'order_ncd_vehiclenumber' => 'required',
        ];

        $attr = [
            'order_record_billto' => 'Bill To',
            'order_period_from' => 'Coverage Period From',
            'order_period_to' => 'Coverage Period To',
            'order_cus_name' => 'Customer Name',
            'order_cus_nirc' => 'Customer NRIC',
            'order_cus_occ_code' => 'Customer Occupcation',
            'order_cus_nirc_type' => 'Customer NRIC Type',
            'order_cus_unitno' => 'Unit No',
            'order_cus_email' => 'Customer Email',
            'order_tsa_email' => 'TSA Email',
            'order_cus_national' => 'Nationality',
            'order_cus_contact' => 'Mobile Number',
            'order_cus_gender' => 'Gender',
            'order_cus_dob' => 'Date Of Birth',
            'order_cus_marital' => 'Marital Status',
            'order_cus_postal_code' => 'Postal Code',
            'order_cus_address' => 'Address',
            'order_coverage' => 'Cover Type',
            'order_vehicle_type' => 'Vehicles Make',
            'order_vehicles_no' => 'Vehicle Registration No',
            'order_vehicles_reg_year' => 'Year of Registration',
            'order_vehicles_pro_year' => 'Year of Registration',
            'order_vehicles_engine' => 'Engine No',
            'order_vehicles_chasis' => 'Chassis No',
            'order_vehicles_capacity' => 'Engine Capacity',
            'order_sompo_payment_method' => 'Payment Method',
            'order_cus_claim_amount' => 'Total Amount Of Claims',
            'order_cus_claim' => 'Claim In Last Three Years',
            'order_ncd_vehiclenumber' => 'NCD Vehicle Number',
        ];

        $request->validate($rules, [], $attr);

        $errors_input = [];

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $new_sompo = new Sompo();
        $new_sompo->order_record_billto = $request->input('order_record_billto');
        $new_sompo->order_dealer = $request->input('order_dealer');
        $new_sompo->order_cus_driving = $request->input('order_cus_driving');
        $new_sompo->order_cus_enc_name = $request->input('order_cus_enc_name');
        $new_sompo->order_cus_name = $request->input('order_cus_name');
        $new_sompo->order_cus_national = $request->input('order_cus_national');
        $new_sompo->order_cus_enc_nirc = $request->input('order_cus_enc_nirc');
        $new_sompo->order_cus_nirc = $request->input('order_cus_nirc');
        $new_sompo->order_cus_nirc_type = $request->input('order_cus_nirc_type');
        $new_sompo->order_cus_dob = $request->input('order_cus_dob');
        $new_sompo->order_cus_gender = $request->input('order_cus_gender');
        $new_sompo->order_cus_exp = $request->input('order_cus_exp');
        $new_sompo->order_cus_marital = $request->input('order_cus_marital');
        $new_sompo->order_cus_occ_code = $request->input('order_cus_occ_code');
        $new_sompo->order_cus_occ = $request->input('order_cus_occ');
        $new_sompo->order_cus_blkno = $request->input('order_cus_blkno');
        $new_sompo->order_cus_street = $request->input('order_cus_street');
        $new_sompo->order_cus_building = $request->input('order_cus_building');
        $new_sompo->order_cus_unitno = $request->input('order_cus_unitno');
        $new_sompo->order_cus_postal_code = $request->input('order_cus_postal_code');
        $new_sompo->order_cus_address = $request->input('order_cus_address');
        $new_sompo->order_cus_contact = $request->input('order_cus_contact');
        $new_sompo->order_cus_enc_contact = $request->input('order_cus_enc_contact');
        $new_sompo->order_cus_enc_email = $request->input('order_cus_enc_email');
        $new_sompo->order_cus_email = $request->input('order_cus_email');
        $new_sompo->order_tsa_enc_email = $request->input('order_tsa_enc_email');
        $new_sompo->order_tsa_email = $request->input('order_tsa_email');
        $new_sompo->order_cus_claim = $request->input('order_cus_claim');
        $new_sompo->order_cus_claim_amount = $request->input('order_cus_claim_amount');
        $new_sompo->order_vehicles_reg_year = $request->input('order_vehicles_reg_year');
        $new_sompo->order_vehicles_pro_year = $request->input('order_vehicles_pro_year');
        $new_sompo->order_vehicle_type = $request->input('order_vehicle_type');
        $new_sompo->order_vehicles_model = $request->input('order_vehicles_model');
        $new_sompo->order_vehicles_capacity = $request->input('order_vehicles_capacity');
        $new_sompo->order_new_reg = $request->input('order_new_reg');
        $new_sompo->order_vehicles_no = $request->input('order_vehicles_no');
        $new_sompo->order_vehicles_engine = $request->input('order_vehicles_engine');
        $new_sompo->order_vehicles_chasis = $request->input('order_vehicles_chasis');
        $new_sompo->order_sompo_offroad = $request->input('order_sompo_offroad');
        $new_sompo->order_ofd_dis_per = $request->input('order_ofd_dis_per');
        $new_sompo->order_sompo_reward = $request->input('order_sompo_reward');
        $new_sompo->order_ncd_entitlement = $request->input('order_ncd_entitlement');
        $new_sompo->order_financecode = $request->input('order_financecode');
        $new_sompo->order_financecode_type_input = $request->input('order_financecode_type_input');
        $new_sompo->order_ncd_previousinsurer = $request->input('order_ncd_previousinsurer');
        $new_sompo->order_pre_policyno = $request->input('order_pre_policyno');
        $new_sompo->order_ncd_vehiclenumber = $request->input('order_ncd_vehiclenumber');
        $new_sompo->order_coverage = $request->input('order_coverage');
        $new_sompo->order_period_from = $request->input('order_period_from');
        $new_sompo->order_period_to = $request->input('order_period_to');
        $new_sompo->order_sompo_discount = $request->input('order_sompo_discount');
        $new_sompo->order_sompo_terms = $request->input('order_sompo_terms');
        $new_sompo->order_sompo_payment_method = $request->input('order_sompo_payment_method');
        $new_sompo->order_sompo_status = $request->input('order_sompo_status');
        $new_sompo->insertBy = Auth::user()->id;
        $new_sompo->updateBy = Auth::user()->id;
        $new_sompo->save();

        if($request->input('sompo_driver')){
            foreach($request->input('sompo_driver') as $dli){
                $this->AddDriver($dli, $new_sompo->order_sompo_id);
            }
        }

        if($request->input('api_sent') == 1){
            $this->callSompoApi($request, $new_sompo->order_sompo_id);
        }

        return redirect()->route('SompoInfo',['id' => $new_sompo->order_sompo_id])->with('sweet_success_msg','Add Success');
    }

    public function update(Request $request){
        $exist_sompo = Sompo::where('order_sompo_id', $request->input('order_sompo_id'))->Where('order_sompo_status', '!=', '0')->first();

        if(!$exist_sompo){
            return redirect()->route('SompoList')->with('error_msg', 'Invalid Case');
        }
        else{
            $rules = [
                'order_record_billto' => 'required',
                'order_period_from' => 'required|date|after_or_equal:today',
                'order_period_to' => 'required|date|after:order_period_from',
                'order_cus_name' => 'required',
                'order_cus_nirc' => 'required',
                'order_cus_occ_code' => 'required',
                'order_cus_nirc_type' => 'required',
                'order_cus_unitno' => 'required',
                'order_cus_email' => 'required|email',
                'order_tsa_email' => 'required|email',
                'order_cus_national' => 'required',
                'order_cus_contact' => 'required|numeric',
                'order_cus_gender' => 'required',
                'order_cus_dob' => 'required',
                'order_cus_marital' => 'required',
                'order_cus_postal_code' => 'required|digits:6',
                'order_cus_address' => 'required',
                'order_coverage' => 'required',
                'order_vehicle_type' => 'required',
                'order_vehicles_no' => 'required',
                'order_vehicles_reg_year' => 'required',
                'order_vehicles_pro_year' => 'required|numeric',
                'order_vehicles_engine' => 'required',
                'order_vehicles_chasis' => 'required',
                'order_vehicles_capacity' => 'required|numeric',
                'order_sompo_payment_method' => 'required',
                'order_cus_claim_amount' => 'nullable|numeric',
                'order_cus_claim' => 'nullable|numeric',
                'order_ncd_vehiclenumber' => 'required',
            ];

            $attr = [
                'order_record_billto' => 'Bill To',
                'order_period_from' => 'Coverage Period From',
                'order_period_to' => 'Coverage Period To',
                'order_cus_name' => 'Customer Name',
                'order_cus_nirc' => 'Customer NRIC',
                'order_cus_occ_code' => 'Customer Occupcation',
                'order_cus_nirc_type' => 'Customer NRIC Type',
                'order_cus_unitno' => 'Unit No',
                'order_cus_email' => 'Customer Email',
                'order_tsa_email' => 'TSA Email',
                'order_cus_national' => 'Nationality',
                'order_cus_contact' => 'Mobile Number',
                'order_cus_gender' => 'Gender',
                'order_cus_dob' => 'Date Of Birth',
                'order_cus_marital' => 'Marital Status',
                'order_cus_postal_code' => 'Postal Code',
                'order_cus_address' => 'Address',
                'order_coverage' => 'Cover Type',
                'order_vehicle_type' => 'Vehicles Make',
                'order_vehicles_no' => 'Vehicle Registration No',
                'order_vehicles_reg_year' => 'Year of Registration',
                'order_vehicles_pro_year' => 'Year of Registration',
                'order_vehicles_engine' => 'Engine No',
                'order_vehicles_chasis' => 'Chassis No',
                'order_vehicles_capacity' => 'Engine Capacity',
                'order_sompo_payment_method' => 'Payment Method',
                'order_cus_claim_amount' => 'Total Amount Of Claims',
                'order_cus_claim' => 'Claim In Last Three Years',
                'order_ncd_vehiclenumber' => 'NCD Vehicle Number',
            ];

            $request->validate($rules, [], $attr);

            $errors_input = [];

            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }

            $exist_sompo->order_record_billto = $request->input('order_record_billto');
            $exist_sompo->order_dealer = $request->input('order_dealer');
            $exist_sompo->order_cus_driving = $request->input('order_cus_driving');
            $exist_sompo->order_cus_enc_name = $request->input('order_cus_enc_name');
            $exist_sompo->order_cus_name = $request->input('order_cus_name');
            $exist_sompo->order_cus_national = $request->input('order_cus_national');
            $exist_sompo->order_cus_enc_nirc = $request->input('order_cus_enc_nirc');
            $exist_sompo->order_cus_nirc = $request->input('order_cus_nirc');
            $exist_sompo->order_cus_nirc_type = $request->input('order_cus_nirc_type');
            $exist_sompo->order_cus_dob = $request->input('order_cus_dob');
            $exist_sompo->order_cus_gender = $request->input('order_cus_gender');
            $exist_sompo->order_cus_exp = $request->input('order_cus_exp');
            $exist_sompo->order_cus_marital = $request->input('order_cus_marital');
            $exist_sompo->order_cus_occ_code = $request->input('order_cus_occ_code');
            $exist_sompo->order_cus_occ = $request->input('order_cus_occ');
            $exist_sompo->order_cus_blkno = $request->input('order_cus_blkno');
            $exist_sompo->order_cus_street = $request->input('order_cus_street');
            $exist_sompo->order_cus_building = $request->input('order_cus_building');
            $exist_sompo->order_cus_unitno = $request->input('order_cus_unitno');
            $exist_sompo->order_cus_postal_code = $request->input('order_cus_postal_code');
            $exist_sompo->order_cus_address = $request->input('order_cus_address');
            $exist_sompo->order_cus_contact = $request->input('order_cus_contact');
            $exist_sompo->order_cus_enc_contact = $request->input('order_cus_enc_contact');
            $exist_sompo->order_cus_enc_email = $request->input('order_cus_enc_email');
            $exist_sompo->order_cus_email = $request->input('order_cus_email');
            $exist_sompo->order_tsa_enc_email = $request->input('order_tsa_enc_email');
            $exist_sompo->order_tsa_email = $request->input('order_tsa_email');
            $exist_sompo->order_cus_claim = $request->input('order_cus_claim');
            $exist_sompo->order_cus_claim_amount = $request->input('order_cus_claim_amount');
            $exist_sompo->order_vehicles_reg_year = $request->input('order_vehicles_reg_year');
            $exist_sompo->order_vehicles_pro_year = $request->input('order_vehicles_pro_year');
            $exist_sompo->order_vehicle_type = $request->input('order_vehicle_type');
            $exist_sompo->order_vehicles_model = $request->input('order_vehicles_model');
            $exist_sompo->order_vehicles_capacity = $request->input('order_vehicles_capacity');
            $exist_sompo->order_new_reg = $request->input('order_new_reg');
            $exist_sompo->order_vehicles_no = $request->input('order_vehicles_no');
            $exist_sompo->order_vehicles_engine = $request->input('order_vehicles_engine');
            $exist_sompo->order_vehicles_chasis = $request->input('order_vehicles_chasis');
            $exist_sompo->order_sompo_offroad = $request->input('order_sompo_offroad');
            $exist_sompo->order_ofd_dis_per = $request->input('order_ofd_dis_per');
            $exist_sompo->order_sompo_reward = $request->input('order_sompo_reward');
            $exist_sompo->order_ncd_entitlement = $request->input('order_ncd_entitlement');
            $exist_sompo->order_financecode = $request->input('order_financecode');
            $exist_sompo->order_financecode_type_input = $request->input('order_financecode_type_input');
            $exist_sompo->order_ncd_previousinsurer = $request->input('order_ncd_previousinsurer');
            $exist_sompo->order_pre_policyno = $request->input('order_pre_policyno');
            $exist_sompo->order_ncd_vehiclenumber = $request->input('order_ncd_vehiclenumber');
            $exist_sompo->order_coverage = $request->input('order_coverage');
            $exist_sompo->order_period_from = $request->input('order_period_from');
            $exist_sompo->order_period_to = $request->input('order_period_to');
            $exist_sompo->order_sompo_discount = $request->input('order_sompo_discount');
            $exist_sompo->order_sompo_terms = $request->input('order_sompo_terms');
            $exist_sompo->order_sompo_payment_method = $request->input('order_sompo_payment_method');
            $exist_sompo->updateBy = Auth::user()->id;
            $exist_sompo->save();

            if($request->input('sompo_driver')){
                SompoDriver::where('sompo_driver_order_id', $exist_sompo->order_sompo_id)->update(['sompo_driver_status' => 0]);
                foreach($request->input('sompo_driver') as $dli){
                    $dli['sompo_driver_claim'] = ($dli['seqno'] == 1)? $request->input('order_cus_claim'):'0';
                    $dli['sompo_driver_claim_amount'] = ($dli['seqno'] == 1)? $request->input('order_cus_claim_amount'):'0';
                    $this->AddDriver($dli, $exist_sompo->order_sompo_id);
                }
            }

            if($request->input('api_sent') == 1){
                $this->callSompoApi($request, $new_sompo->order_sompo_id);
            }

            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $exist_order = Sompo::find($request->route('id'));
        if($exist_order){
            $exist_order->order_sompo_status = "0";
            $exist_order->save();
        }
    
        return redirect()->route('SompoList')->with('success_msg', 'Delete Case Succesfully');
    }

    public function AddDriver($input_data, $sompo_id){
        $driver = SompoDriver::where('sompo_driver_order_id', $sompo_id)
        ->where('sompo_driver_seqno', $input_data['seqno'])->first();
        if($driver){
            $driver->sompo_driver_order_id = $sompo_id;
            $driver->sompo_driver_name = $input_data['name'];
            $driver->sompo_driver_enc_name = $input_data['enc_name'];
            $driver->sompo_driver_nirc = $input_data['nirc'];
            $driver->sompo_driver_enc_nirc = $input_data['enc_nirc'];
            $driver->sompo_driver_nirc_type = $input_data['nirc_type'];
            $driver->sompo_driver_national = $input_data['national'];
            $driver->sompo_driver_exp = $input_data['exp'];
            $driver->sompo_driver_gender = $input_data['gender'];
            $driver->sompo_driver_occupation_code = $input_data['occupation_code'];
            $driver->sompo_driver_other_occupation = $input_data['other_occupation'];
            $driver->sompo_driver_dob = $input_data['dob'];
            $driver->sompo_driver_marital_status = $input_data['marital_status'];
            $driver->sompo_driver_relation = $input_data['relation'];
            $driver->sompo_driver_seqno = $input_data['seqno'];
            $driver->sompo_driver_is_app = ($input_data['seqno'] == 1)?'Y':'N';
            $driver->sompo_driver_claim = isset($input_data['sompo_driver_claim'])?$input_data['sompo_driver_claim']:null;
            $driver->sompo_driver_claim_amount = isset($input_data['sompo_driver_claim_amount'])?$input_data['sompo_driver_claim_amount']:null;
            $driver->sompo_driver_status = 1;
            $driver->updateBy = Auth::user()->id;
            $driver->save();
        }
        else{
            $new_driver = new SompoDriver();
            $new_driver->sompo_driver_order_id = $sompo_id;
            $new_driver->sompo_driver_name = $input_data['name'];
            $new_driver->sompo_driver_enc_name = $input_data['enc_name'];
            $new_driver->sompo_driver_nirc = $input_data['nirc'];
            $new_driver->sompo_driver_enc_nirc = $input_data['enc_nirc'];
            $new_driver->sompo_driver_nirc_type = $input_data['nirc_type'];
            $new_driver->sompo_driver_national = $input_data['national'];
            $new_driver->sompo_driver_exp = $input_data['exp'];
            $new_driver->sompo_driver_gender = $input_data['gender'];
            $new_driver->sompo_driver_occupation_code = $input_data['occupation_code'];
            $new_driver->sompo_driver_other_occupation = $input_data['other_occupation'];
            $new_driver->sompo_driver_dob = $input_data['dob'];
            $new_driver->sompo_driver_marital_status = $input_data['marital_status'];
            $new_driver->sompo_driver_relation = $input_data['relation'];
            $new_driver->sompo_driver_seqno = $input_data['seqno'];
            $new_driver->sompo_driver_is_app = ($input_data['seqno'] == 1)?'Y':'N';
            $new_driver->sompo_driver_claim = isset($input_data['sompo_driver_claim'])?$input_data['sompo_driver_claim']:null;
            $new_driver->sompo_driver_claim_amount = isset($input_data['sompo_driver_claim_amount'])?$input_data['sompo_driver_claim_amount']:null;
            $new_driver->sompo_driver_status = 1;
            $new_driver->insertBy = Auth::user()->id;
            $new_driver->updateBy = Auth::user()->id;
            $new_driver->save();
        }
    }

    public function getSompoData($enc_type , $select_one = false){
        $main_query = SompoData::select('sompo_ref_code as filter', 'sompo_data_value as value', 'sompo_display_name as display_name')
            ->where('sompo_ec_type', $enc_type)
            ->where('sompo_data_status', 1);

        if($enc_type == 'PQ_VEHICLE_MATRIX'){
            $main_query->where('sompo_ref_code', 'MOTORCYCLE');
        }
        else if($enc_type == 'MT_NCD'){
            $main_query->where('sompo_ref_code', 'MTMC01')
            ->orderBy('sompo_display_name', 'ASC');
        }
        else if($enc_type == 'POLICY_TYPE'){
            $main_query->where('sompo_ref_code', 'MTMC01');
        }
        else if($enc_type == 'RELATIONSHIP'){
            $main_query->where('sompo_ref_code', 'MTMC01');
        }
        else if($enc_type == 'IDENTITY_TYPE'){
            $main_query->where('sompo_ref_code', 'MTMC01');
        }

        $listing_data = $main_query->get()->toArray();

        if(!$listing_data){
            $listing_data = [];
        }

        if($select_one){
            $listing_data = array_merge([['filter' => '','value' => '','display_name' => 'Select One']], $listing_data);
        }

        if($enc_type == 'PREVIOUS_INS_COMP'){
            $listing_data = array_merge($listing_data, [['filter' => '','value' => 'other','display_name' => 'OTHER']]);
        }
        if($enc_type == 'HP_NAME'){
            $financecode_list = FinanceClass::select('financeclass_id as value', 'financeclass_code as display_name')
            ->where('financeclass_status', 1)->get()->toArray();
            $listing_data = array_merge($listing_data, $financecode_list);
        }

        return $listing_data;
    }

    public static function validIdentification($number)
    {

        if (strlen($number) !== 9) {
            return false;
        }
        $newNumber = str_split(strtoupper($number));
        $icArray = [];
        for ($i = 0; $i < 9; $i++) {
            $icArray[$i] = $newNumber[$i];
        }
        $icArray[1] = intval($icArray[1], 10) * 2;
        $icArray[2] = intval($icArray[2], 10) * 7;
        $icArray[3] = intval($icArray[3], 10) * 6;
        $icArray[4] = intval($icArray[4], 10) * 5;
        $icArray[5] = intval($icArray[5], 10) * 4;
        $icArray[6] = intval($icArray[6], 10) * 3;
        $icArray[7] = intval($icArray[7], 10) * 2;

        $weight = 0;
        for ($i = 1; $i < 8; $i++) {
            $weight += $icArray[$i];
        }
        $offset = ($icArray[0] === "T" || $icArray[0] == "G") ? 4 : 0;
        $temp = ($offset + $weight) % 11;

        $st = ["J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A"];
        $fg = ["X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K"];

        $theAlpha = "";
        if ($icArray[0] == "S" || $icArray[0] == "T") {
            $theAlpha = $st[$temp];
        } else if ($icArray[0] == "F" || $icArray[0] == "G") {
            $theAlpha = $fg[$temp];
        }
        return ($icArray[8] === $theAlpha);
    }


    public function validate_plan_period(Request $request ){
        $msg = '';
        foreach($request->input('dri_dob') as $li){
            if(strtotime($li)){
                $d1 = Carbon::createFromDate($li);
                $d2 =  Carbon::now();
                $diff = $d1->diffInYears($d2);
                if($request->input('order_coverage') == "MTMC01CP" && $diff > 65){
                    $msg = "Applicant age should not exceed 65 years, Please contact Ensure.";
                    break;
                }
                elseif($diff > 70){
                    $msg = "Applicant age should not exceed 70 years, Please contact Ensure.";
                    break;
                }
            }
        }

        if($msg){
            $output = array('status'=>0,'msg'=>$msg);
            return response()->json($output, 200); 
        }
        else{
            $output = array('status'=>1,'msg'=>$msg);
            return response()->json($output, 200); 
        }
    }

    public function getCusInfo(Request $request){
        $customer = Customer::find($request->input('order_customer'));
        return response()->json($customer, 200); 
    }

    public function getCustomerId($sompo_id){
        $exist_sompo = Sompo::find($sompo_id);
        $exist_cus = Customer::where("partner_nirc", $exist_sompo->order_cus_nirc)->first();
        if($exist_cus){
            return $exist_cus->partner_id;
        }
        else{
            $new_cus = new Customer();
            $new_cus->partner_name = $exist_sompo->order_cus_name;
            $new_cus->partner_nirc = $exist_sompo->order_cus_nirc;
            $new_cus->partner_dob = $exist_sompo->order_cus_dob;
            $new_cus->partner_unit_no = $exist_sompo->order_cus_unitno;
            $new_cus->partner_tel = $exist_sompo->order_cus_contact;
            $new_cus->partner_email = $exist_sompo->order_cus_email;
            $new_cus->partner_country = $exist_sompo->order_cus_national;
            $new_cus->partner_outlet = $exist_sompo->order_cus_national;
            $new_cus->partner_postal_code = $exist_sompo->order_cus_postal_code;
            $new_cus->partner_address = $exist_sompo->order_cus_address;
            $new_cus->partner_status = 1;
            $new_cus->insertBy = Auth::user()->id;
            $new_cus->updateBy = Auth::user()->id;
            $new_cus->save();

            return $new_cus->partner_id;
        }
    }

    public function getPartnerInfoByNRIC(Request $request){
        $customer = Customer::select('partner_id')
        ->where('partner_nirc', $request->input('order_cus_nirc'))
        ->first();
        if($customer){
            return response()->json(['status'=> 1, 'partner_id' => $customer->partner_id], 200); 
        }
        else{
            return response()->json(['status'=> 0], 200); 
        }
    }

    public function callSompoApi(Request $request, $sompo_id){
        $exist_sompo = Sompo::find($sompo_id);
        $api = new SompoApiController();
        $gender_convert = array(
            "male" => 'M',
            "female" => 'F',
        );
        
        $marital_convert = array(
            "single" => 'S',
            "married" => 'M',
            "divorced" => 'S',
            "Other" => 'S',
        );

        $veh_data = SompoData::select('sompo_original_data')
            ->where('sompo_ec_type', 'PQ_VEHICLE_MATRIX')
            ->where('sompo_data_value', $request->input('order_vehicle_type'))
            ->where('sompo_data_status', 1)->first();
        $veh_input_data = json_decode($veh_data->sompo_original_data, true);
        
        if($request->input('order_ncd_entitlement')){
            $clm_data = SompoData::select('sompo_original_data')
            ->where('sompo_ec_type', 'MT_NCD')
            ->where('sompo_data_value', $request->input('order_ncd_entitlement'))
            ->where('sompo_data_status', 1)->first();
        }
        else{
            $clm_data = SompoData::select('sompo_original_data')
            ->where('sompo_ec_type', 'MT_NCD')
            ->where('sompo_id', 711)
            ->where('sompo_data_status', 1)->first();
        }
        
        $clm_data = json_decode($clm_data->sompo_original_data,true);

        $finan_data = FinanceClass::select('financeclass_code')
        ->where('financeclass_id', $request->input('order_financecode'))->first();
        $sompo_finance = false;
        $sompo_finance_code = '';
         
        if(!$finan_data){
            $sompo_finance = true;
            $sompo_finance_code = $request->input('order_financecode');
        }
        else{
            $sompo_finance_code = $finan_data->financeclass_code;
        }

        $inexp_drv = 0;
        $driver_listing = [];
        foreach($request->input('sompo_driver') as $dli){
            if($dli['exp'] == 0){
                $inexp_drv++;
            }

            $clam = 0;
            $clam_amt = 0;

            if($dli['seqno'] == '1'){
                $clam = $request->input('order_cus_claim');
                $clam_amt = $request->input('order_cus_claim_amount');
            }

            $driver_listing[] = [
                "gender"=>$dli['gender'],
                "exp"=>$dli['exp'],
                "occupation_code"=>$dli['occupation_code'],
                "is_app"=>($dli['seqno'] == '1')?'Y':'N', 
                "dob"=>$dli['exp'],
                "claim"=>$clam,
                "claim_amount"=>$clam_amt,
                "point"=>0,
                "marital_status"=>$dli['marital_status'],
                "nirc_type"=>$dli['nirc_type'],
                "enc_name"=>$dli['enc_name'],
                "relation"=>$dli['relation'],
                "other_occupation"=>$dli['other_occupation'],
            ];
        }
        
        $api->VEH_NO_INEXPER_DRIVER = strtoupper($inexp_drv);
        $api->driver_list = $driver_listing;
        $api->requestUniqueId = strtoupper("ENSSOMPO"."-".sprintf("%06d", $sompo_id));
        $api->POL_GENDER = $gender_convert[$request->input('order_cus_gender')]?:null;
        $api->POL_FM_DT = $request->input('order_period_from')?:null;
        $api->POL_TO_DT = $request->input('order_period_to')?:null;
        $api->MOTOROFD = round($request->input('order_ofd_dis_per'))?:0;
        $api->POL_MAR_STATUS = $marital_convert[$request->input('order_cus_marital')]?:null;
        $api->VEH_INSURED_DRIVING_YN = "Y";
        $api->DRV_VALID_LIC_YN = "Y";
        $api->POL_RATING_EFF_DT = date('Y-m-d H:i:s');
        $api->VEH_COE_FLG = "N";
        $api->NCD_DISC_CODE = $request->input('order_ncd_entitlement')?:null;
        $api->VEH_CC = $request->input('order_vehicles_capacity');
        $api->VEH_NO_PASS = $veh_input_data['VM_NO_PASS'];
        $api->VEH_GRADE = $veh_input_data['VM_GRADE'];
        $api->VEH_MODEL_CODE = $veh_input_data['VM_MODEL_CODE'];
        $api->VM_BODY_DESC = strtoupper($request->input('order_vehicles_model'));
        $api->IS_NAMEDRIVERS_TERMS_AGREED = $request->input('order_sompo_terms')?:null;
        $api->VEH_BODY_TYPE = $veh_input_data['VM_BODY_TYPE'];
        $api->VEH_REGN_YEAR = $request->input('order_vehicles_reg_year')?:null;
        if($request->input('order_coverage') == 'MTMC01CP' && in_array($request->input('order_ncd_entitlement') , array('MTNCD020'))){
            $api->VEH_NCD_PROT_ELIG = "Y";
        }
        else{
            $api->VEH_NCD_PROT_ELIG = "N";
        }
        $api->VEH_SPOUSE_DRIVING_YN = "N";
        $api->VEH_YEAR = $request->input('order_vehicles_pro_year')?:null;
        $api->VEH_ONT_PARKING = "N";
        $api->VEH_REGN_NO = $request->input('order_vehicles_no')?:null;
        $api->REASON_FOR_NCD = null;
        $api->VEH_MAKE_CODE = $veh_input_data['VM_MAKE_CODE']?:null;
        $api->VEH_PRV_CLM_FREE_YR = $clm_data['NCD_Grade']?:null;
        $api->NO_OF_PERSONS = 0;
        $api->TRIP_TYPE = $request->input('order_coverage')?:null;
        $api->POL_OCC_CODE = strtoupper($request->input('order_cus_occ_code'));
        $api->POL_OCC_DESC = strtoupper($request->input('order_cus_occ'))?:null;
        $api->POL_DOB = $request->input('order_cus_dob')?:null;
        $api->PROMO_CODE = null;
        $api->NOOFCLAIMS = intval($request->input('order_cus_claim'))?:null;
        $api->TOTALCLAIMAMT = ($request->input('order_cus_claim_amount')>0)?intval($request->input('order_cus_claim_amount')):null;
        $api->POL_ASSURED_NAME = $request->input('order_cus_enc_name')?:null;
        $api->POL_INSURED_ID = $request->input('order_cus_enc_nirc')?:null;
        $api->POL_IDENTITY_TYPE = $request->input('order_cus_nirc_type')?:null;
        $api->POL_NATIONALITY = $request->input('order_cus_national')?:null;
        $api->POL_PHONE = $request->input('order_cus_enc_contact')?:null;
        $api->POL_PREMIUMBEFORETAX = str_replace(",", "",$request->input('order_premium_amt'))?:null;
        $api->POL_PLANTAXAMOUNT = str_replace(",", "",$request->input('order_gst_amount'))?:null;
        $api->POL_PLANTOTALPREMIUM = str_replace(",", "",$request->input('order_grosspremium_amount'))?:null;
        $api->POL_PYMT_MODE = $request->input('order_sompo_payment_method')?:null;
        $return_plan = json_decode($exist_sompo->order_sompo_return_plan, true);
        
        if(!empty($return_plan)){
            $api->SEL_PLANLIST = $return_plan;
        }
        else{
            if($exist_sompo && is_array($exist_sompo->order_sompo_plan_list)){
                foreach($exist_sompo->order_sompo_plan_list as $list_item){
                    foreach($list_item['PLANXRISKLIST'] as $plancode){
                        if($plancode['RISK_CLASS'] == $request->input('order_coverage')){
                            $api->SEL_PLANLIST = array($list_item);
                            break;
                        }
                    }
                }
            }
        }
        
        if($request->input('order_financecode')){
            if($sompo_finance){
                $api->VEH_HP_DTLS = strtoupper($this->getSompoDisplayName($sompo_finance_code, 'HP_NAME'));
                $api->VEH_HP_CODE = $sompo_finance_code;
            }
            else{
                $api->VEH_HP_DTLS = strtoupper( $sompo_finance_code);
                $api->VEH_HP_CODE = 'HPN999';
            }
        }
        else{
            $api->VEH_HP_DTLS = null;
            $api->VEH_HP_CODE = null;
        }
        
        $api->VEH_CHASSIS_NO = strtoupper($request->input('order_vehicles_chasis'))?:null;
        $api->VEH_ENGINE_NO = strtoupper($request->input('order_vehicles_engine'))?:null;
        $api->APPQUOTEPOLICYREFID = $exist_sompo->order_sompo_qoute_id?:null;
        $api->POL_ADDR1 = strtoupper($request->input('order_cus_blkno')." ".$request->input('order_cus_street'));
        $api->POL_ADDR2 = strtoupper($request->input('order_cus_unitno'))?:null;
        $api->POL_POSTAL_CODE = $request->input('order_cus_postal_code')?:null;
        $api->POL_AGENT_REF_NO = $exist_sompo->order_sompo_agent_id?:null;
        $api->XCOUNTRY_BIKE = $request->input('order_sompo_offroad');
        $api->POL_EMAIL = $request->input('order_tsa_enc_email');
        $api->HIRE_REWARD = $request->input('order_sompo_reward');
        $api->PREVIOUSREGISTRATIONNO = $request->input('order_ncd_vehiclenumber');
        $api->FEMALE_DISC = "N";
        
        if( $request->input('order_ofd_dis_per') > 0){
            $api->OFD_YN = "Y";
        }
        else{
            $api->OFD_YN = "N";
        }       
        $return_response = true;
        $resend = false;

        if(in_array($exist_sompo->order_sompo_status, [11,21,31]) ||  $request->input('resend') == 1){
            $resend = true;
        }

        if(!$resend){
            if($exist_sompo->order_sompo_status == 1){
                $response = $api->SompoGetQoutation();
                if(isset($response['serviceReplyObject']['CURRENTPOLICY']['APPQUOTEPOLICYREFID']) && !isset($response['serviceReplyObject']['BUSINESSVALIDATIONMESSAGE'])){
                    $exist_sompo->order_sompo_status = 10;
                    $exist_sompo->order_sompo_qoute_id = $response['serviceReplyObject']['CURRENTPOLICY']['APPQUOTEPOLICYREFID'];
                    $exist_sompo->order_sompo_plan_list = json_encode($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST']);
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                }
                else{
                    $exist_sompo->order_sompo_status = 11;
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                    $return_response = false;
                }

                if(isset($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST'])){
                    $this->getSompoPrice($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST'], $request->input('order_coverage'), $sompo_id);
                }
                     
            }
            elseif($exist_sompo->order_sompo_status=='10'){
                $response = $api->SompoGetProposal();
                if(isset($response['serviceReplyObject']['CURRENTPOLICY']['APPQUOTEPOLICYREFID']) && !isset($response['serviceReplyObject']['BUSINESSVALIDATIONMESSAGE'])){
                    $exist_sompo->order_sompo_status = 20;
                    $exist_sompo->order_sompo_agent_id = $response['serviceReplyObject']['CURRENTPOLICY']['POL_AGENT_REF_NO'];
                    $exist_sompo->order_sompo_return_plan = json_encode($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST']);
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                }
                else{
                    $exist_sompo->order_sompo_status = 21;
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                    $return_response = false;
                }
                
                $this->getSompoPrice($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST'], $request->input('order_coverage'), $sompo_id);
            }
            elseif($exist_sompo->order_sompo_status=='20'){
                $response = $api->SompoCreatePolicy();
                
                if(isset($response['serviceReplyObject']['ACKNOWLEGMENT_ID']) && !isset($response['serviceReplyObject']['BUSINESSVALIDATIONMESSAGE'])){
                    $exist_sompo->order_sompo_status = 30;
                    $exist_sompo->order_sompo_policy_id = $response['serviceReplyObject']['ACKNOWLEGMENT_ID'];
                    $exist_sompo->order_sompo_agent_id = $response['serviceReplyObject']['PROPOSAL_NO'];
                    $exist_sompo->order_sompo_issue_date = date('Y-m-d H:i:s');
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                }
                else{
                    $exist_sompo->order_sompo_status = 31;
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                    $return_response = false;
                }
                
            }
        }
        else{
            if(in_array($exist_sompo->order_sompo_status, array(10,11,20,21,31))){
                $response = $api->SompoGetQoutation();
                if(isset($response['serviceReplyObject']['CURRENTPOLICY']['APPQUOTEPOLICYREFID']) && !isset($response['serviceReplyObject']['BUSINESSVALIDATIONMESSAGE'])){
                    $exist_sompo->order_sompo_status = 10;
                    $exist_sompo->order_sompo_qoute_id = $response['serviceReplyObject']['CURRENTPOLICY']['APPQUOTEPOLICYREFID'];
                    $exist_sompo->order_sompo_plan_list = json_encode($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST']);
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                }
                else{
                    $exist_sompo->order_sompo_status = 11;
                    $exist_sompo->updateBy = Auth::user()->id;
                    $exist_sompo->save();
                    $return_response = false;
                }

                $this->getSompoPrice($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST'], $request->input('order_coverage'), $sompo_id);

                if(!in_array($exist_sompo->order_sompo_status, array(10,11)) && $return_response){
                    $api->APPQUOTEPOLICYREFID = $response['serviceReplyObject']['CURRENTPOLICY']['APPQUOTEPOLICYREFID'];
                    $response = $api->SompoGetProposal();
                    if(isset($response['serviceReplyObject']['CURRENTPOLICY']['APPQUOTEPOLICYREFID']) && !isset($response['serviceReplyObject']['BUSINESSVALIDATIONMESSAGE'])){
                        
                        $exist_sompo->order_sompo_status = 20;
                        $exist_sompo->order_sompo_agent_id = $response['serviceReplyObject']['CURRENTPOLICY']['POL_AGENT_REF_NO'];
                        $exist_sompo->order_sompo_return_plan = json_encode($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST']);
                        $exist_sompo->updateBy = Auth::user()->id;
                        $exist_sompo->save();
                    }
                    else{
                        $exist_sompo->order_sompo_status = 21;
                        $exist_sompo->updateBy = Auth::user()->id;
                        $exist_sompo->save();
                        $return_response = false;
                    }

                    $this->getSompoPrice($response['serviceReplyObject']['CURRENTPOLICY']['PLAN_LIST'], $request->input('order_coverage'), $sompo_id);
                }
                elseif($exist_sompo->order_sompo_status==31 && $return_response){
                    $api->POL_AGENT_REF_NO = $response['serviceReplyObject']['CURRENTPOLICY']['POL_AGENT_REF_NO'];
                    $response = $api->SompoCreatePolicy();
                    if($response['serviceReplyObject']['ACKNOWLEGMENT_ID'] && !$response['serviceReplyObject']['BUSINESSVALIDATIONMESSAGE']){
                        
                        $exist_sompo->order_sompo_status = 30;
                        $exist_sompo->order_sompo_policy_id = $response['serviceReplyObject']['ACKNOWLEGMENT_ID'];
                        $exist_sompo->order_sompo_agent_id = $response['serviceReplyObject']['PROPOSAL_NO'];
                        $exist_sompo->order_sompo_issue_date = date('Y-m-d H:i:s');
                        $exist_sompo->updateBy = Auth::user()->id;
                        $exist_sompo->save();
                    }
                    else{
                        $exist_sompo->order_sompo_status = 31;
                        $exist_sompo->updateBy = Auth::user()->id;
                        $exist_sompo->save();
                        $return_response = false;
                    }
                    
                }
            }
        }
        
        return $return_response;
    }

    public function getSompoPrice($plan, $order_coverage, $sompo_id){
        $exist_sompo = Sompo::find($sompo_id);
        foreach($plan as $plan_li){
            foreach($plan_li['PLANXRISKLIST'] as $risk_list){
                if(strtoupper($risk_list['RISK_CLASS']) == strtoupper($order_coverage)){
                    $exist_sompo->order_premium_amt = $plan_li['PREMIUMBEFORETAX'];
                    $exist_sompo->order_gst_amount = $plan_li['PLANTAXAMOUNT'];
                    $exist_sompo->order_grosspremium_amount = $plan_li['PLANTOTALPREMIUM'];
                    
                    if(is_array($risk_list['RiskPlanXExcessList'])){
                        foreach($risk_list['RiskPlanXExcessList'] as $exc_list){
                            $exist_sompo->order_policy_excess = $exc_list['Excess_LC_VAL'];
                        }
                    }
                }
            }
        }
        $exist_sompo->save();
    }

    public function getSompoDisplayName ($value, $type){
        switch($type){
            case "MT_NCD":
            case "IDENTITY_TYPE":
            case "POLICY_TYPE":
            case "RELATIONSHIP":
            case "ofd_discount":
                $extra_where = "MTMC01";
                break;
            case "PQ_VEHICLE_MATRIX":
                $extra_where = "MOTORCYCLE";
                break;
            default:
                $extra_where = "";
                break;
        }
        $main_query = SompoData::select('sompo_display_name')
        ->where('sompo_data_value', $value)
        ->where('sompo_ec_type',$type);

        if($extra_where){
            $main_query->where('sompo_ref_code',$extra_where);
        }

        $data = $main_query->first();

        return $data->sompo_display_name;
    }

    public function GetAddressByPostalCode($code){
        $api = new SompoApiController();
        $response = $api->SompoLookUp("POSTAL_CODE_ADDRESS",true,$code);
        if(count($response['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList']) > 0){
            foreach($response['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'] as $list_item){
                $result = array(
                    "status" => 1,
                    "block_number" => $list_item['Address_Building_No'],
                    "postal_code" => $list_item['Address_Postal_Code'],
                    "street_name" => $list_item['Address_Street_Name'],
                    "building_name" => $list_item['Address_Building_Name'],
                );
            }
        }
        else{
            $result["status"] = 0;
        }

        return response()->json($result, 200); 
    }

    public function validateAge(Request $request){
        if($request->input('sompo_driver')){
            foreach($request->input('sompo_driver') as $li){
                $dob = $li['dob'];
            }
        }
        else{
            $dob = $request->input('order_cus_dob');
        }

        if(strtotime($dob)){
            if(Carbon::parse($dob)->age < 18){
                return response()->json(['status'=>1,'msg'=>"Application Must Over 18 years Old"], 200); 
            }
            else{
                return response()->json(['status'=>1,'msg'=>""], 200); 
            }
        }
        else{
            return response()->json(['status'=>1,'msg'=>"Invalid Date Format"], 200); 
        }
        
    }

    public function validatePeriodFrom(Request $request){
        if(!strtotime($request->input('order_period_from'))){
            return response()->json(['status'=>1,'msg'=>$request->input('order_period_from')], 200); 
        }
        else{
            if(strtotime($request->input('order_period_from')) < strtotime(date('Y-m-d'))){
                return response()->json(['status'=>1,'msg'=>"Date must today onward"], 200); 
            }
            else{
                return response()->json(['status'=>1,'msg'=>""], 200); 
            }
        }
    }

    public function validatePeriodTo(Request $request){
        if(!strtotime($request->input('order_period_to'))){
            $result = array('status'=>1,'msg'=>"Invalid Date Format");
        }
        else{
            if(strtotime($o->order_period_from)){
                $effectiveDate1 = strtotime("+12 months", strtotime($request->input('order_period_from'))) - 86400;
                $effectiveDate2 = strtotime("+18 months", strtotime($request->input('order_period_from'))) - 86400;
                if(strtotime($request->input('order_period_to')) >= $effectiveDate1 && strtotime($request->input('order_period_to')) <= $effectiveDate2 ){
                    $result = array('status'=>1,'msg'=>"");
                }
                else{
                    if(strtotime($request->input('order_period_to')) < $effectiveDate1){
                        $result = array('status'=>1,'msg'=>"Period cannot less than one year");
                    }
                    else{
                        $result = array('status'=>1,'msg'=>"Period cannot more than 18 months");
                    }
                }
            }
            else{
                if(strtotime($request->input('order_period_to')) < strtotime(date('Y-m-d'))){
                    $result = array('status'=>1,'msg'=>"Date must today onward");
                }
                else{
                    $result = array('status'=>1,'msg'=>$o->order_period_from);
                }
            }
        }

        return response()->json($result , 200); 
    }

    public function validateExp(Request $request){
        $error = false;
        $err_msg = array();

        if($request->input('sompo_driver')){
            foreach($request->input('sompo_driver') as $li){
                if(Carbon::parse($li['dob'])->age < $li['exp']){
                    $error = true;
                    $err_msg[] = "Rider ".$li['seqno']." Had Invalid Driver Experience.";
                }
            }
        }

        if($request->input('order_cus_dob')){
            if(Carbon::parse($request->input('order_cus_dob'))->age < $request->input('order_cus_exp')){
                $error = true;
                $err_msg[] = "Applier Had Invalid Driver Experience.";
            }
        }

        if($error){
            $msg = implode("\n", $err_msg);
            $result = array('status'=>0,'msg'=>$msg);
        }
        else{
            $result = array('status'=>1,'msg'=>"");
        }

        return response()->json($result , 200); 
    }

    public function copyToLocal(Request $request){
        $exist_sompo = Sompo::find($request->route('id'));
        if($exist_sompo && $exist_sompo->order_id == 0){
            $plan_mapping = array(
                'MTMC01CP' => 'C - Comprehensive',
                'MTMC01TF' => 'F - Third Party Fire and Theft',
                'MTMC01TP' => 'T - Third Party',
            );

            $new_order = new Order();
            $new_order->order_prefix_type = $this->motor_prefix;
            $new_order->order_cprofile = env('APP_ID');
            $new_order->order_no = $this->generateMotorCode();
            $this->updateMotorRefCode();
            $new_order->order_doc_type = "DN";
            $new_order->order_ref = 0;
            $new_order->order_renewal_ref = 0;
            $new_order->order_ins_doc_no = '';
            $new_order->order_type = "New Case";
            $new_order->order_customer = $this->getCustomerId($request->route('id'));
            $new_order->order_dealer = $exist_sompo->order_dealer?:0;
            $new_order->order_premium_amt = $this->cleanNumber($exist_sompo->order_premium_amt?:0);
            $new_order->order_commercial_use_per = $this->cleanNumber($exist_sompo->order_commercial_use_per?:0);
            $new_order->order_accident_per = $this->cleanNumber($exist_sompo->order_accident_per?:0);
            $new_order->order_accident_amt = $this->cleanNumber($exist_sompo->order_accident_amt?:0);
            $new_order->order_in_exp_amt = $this->cleanNumber($exist_sompo->order_in_exp_amt?:0);
            $new_order->order_vehicle_load_per = $this->cleanNumber($exist_sompo->order_vehicle_load_per?:0);
            $new_order->order_vehicle_load_amt = $this->cleanNumber($exist_sompo->order_vehicle_load_amt?:0);
            $new_order->order_ncd_dis_per = $this->cleanNumber($exist_sompo->order_ncd_dis_per?:0);
            $new_order->order_ncd_dis_amt = $this->cleanNumber($exist_sompo->order_ncd_dis_amt?:0);
            $new_order->order_ofd_dis_per = $this->cleanNumber($exist_sompo->order_ofd_dis_per?:0);
            $new_order->order_ofd_dis_amt = $this->cleanNumber($exist_sompo->order_ofd_dis_amt?:0);
            $new_order->order_3rd_rider_amt = $this->cleanNumber($exist_sompo->order_3rd_rider_amt?:0);
            $new_order->order_4th_rider_amt = $this->cleanNumber($exist_sompo->order_4th_rider_amt?:0);
            $new_order->order_addtional_amt = $this->cleanNumber($exist_sompo->order_addtional_amt?:0);
            $new_order->order_subtotal_amount = $this->cleanNumber($exist_sompo->order_subtotal_amount?:0);
            $new_order->order_gst_percent = $this->cleanNumber($exist_sompo->order_gst_percent?:0);
            $new_order->order_gst_amount = $this->cleanNumber($exist_sompo->order_gst_amount?:0);
            $new_order->order_direct_discount_amt = $this->cleanNumber($exist_sompo->order_sompo_discount?:0);
            $new_order->order_bank_interest = $this->cleanNumber($exist_sompo->order_bank_interest?:0);
            $new_order->order_grosspremium_amount = $this->cleanNumber($exist_sompo->order_grosspremium_amount?:0);
            $new_order->order_extra_name = $exist_sompo->order_extra_name?:"";
            $new_order->order_extra_amount = $this->cleanNumber($exist_sompo->order_extra_amount?:0);
            $new_order->order_receivable_dealercommpercent = $this->cleanNumber($exist_sompo->order_receivable_dealercommpercent?:0);
            $new_order->order_receivable_dealercomm = $this->cleanNumber($exist_sompo->order_receivable_dealercomm?:0);
            $new_order->order_receivable_dealergst = $this->cleanNumber($exist_sompo->order_receivable_dealergst?:0);
            $new_order->order_receivable_dealergstamount = $this->cleanNumber($exist_sompo->order_receivable_dealergstamount?:0);
            $new_order->order_receivable_gstpercent = $this->cleanNumber($exist_sompo->order_receivable_gstpercent?:0);
            $new_order->order_receivable_gstamount = $this->cleanNumber($exist_sompo->order_receivable_gstamount?:0);
            $new_order->order_receivable_premiumreceivable = $this->cleanNumber($exist_sompo->order_receivable_premiumreceivable?:0);
            $new_order->order_payable_ourcommpercent = $this->cleanNumber($exist_sompo->order_payable_ourcommpercent?:0);
            $new_order->order_payable_ourcomm = $this->cleanNumber($exist_sompo->order_payable_ourcomm?:0);
            $new_order->order_payable_nettcommpercent = $this->cleanNumber($exist_sompo->order_payable_nettcommpercent?:0);
            $new_order->order_payable_nettcomm = $this->cleanNumber($exist_sompo->order_payable_nettcomm?:0);
            $new_order->order_payable_gstpercent = $this->cleanNumber($exist_sompo->order_payable_gstpercent?:0);
            $new_order->order_payable_gst = $this->cleanNumber($exist_sompo->order_payable_gst?:0);
            $new_order->order_payable_premium = $this->cleanNumber($exist_sompo->order_payable_premium?:0);
            $new_order->order_status = 1;
            $new_order->insertBy = Auth::user()->id;
            $new_order->updateBy = Auth::user()->id;
            $new_order->save();

            $new_motor = new Motor();
            $new_motor->order_record_billto = $exist_sompo->order_record_billto?:"";
            $new_motor->order_period_from = $exist_sompo->order_period_from?:"";
            $new_motor->order_period_to = $exist_sompo->order_period_to?:"";
            $new_motor->order_cancel_date = $exist_sompo->order_cancel_date?:NULL;
            $new_motor->order_date = $exist_sompo->order_sompo_issue_date?:"";
            $new_motor->order_insurco = $exist_sompo->order_insurco?:"";
            $new_motor->order_coverage = $plan_mapping[$exist_sompo->order_coverage]?:"";
            $new_motor->order_vehicles_no = $exist_sompo->order_vehicles_no?:"";
            $new_motor->order_cvnote = $exist_sompo->order_cvnote?:"";
            $new_motor->order_policyno = $exist_sompo->order_policyno?:"";
            $new_motor->order_financecode = $exist_sompo->order_financecode?:"";
            $new_motor->order_vehicles_brand = $exist_sompo->order_vehicles_brand?:"";
            $new_motor->order_vehicles_model = $exist_sompo->order_vehicles_model?:"";
            $new_motor->order_vehicles_engine = $exist_sompo->order_vehicles_engine?:"";
            $new_motor->order_vehicles_body_type = $exist_sompo->order_vehicles_body_type?:"";
            $new_motor->order_vehicles_chasis = $exist_sompo->order_vehicles_chasis?:"";
            $new_motor->order_vehicles_origdt = $exist_sompo->order_vehicles_origdt?:NULL;
            $new_motor->order_vehicles_capacity = $exist_sompo->order_vehicles_capacity?:"";
            $new_motor->order_vehicles_seatno = $this->cleanNumber($exist_sompo->order_vehicles_seatno?:0);
            $new_motor->order_nameddriver_amount = $this->cleanNumber($exist_sompo->order_nameddriver_amount?:0);
            $new_motor->order_nameddriver = $exist_sompo->order_nameddriver?:"";
            $new_motor->order_tpft_amount = $this->cleanNumber($exist_sompo->order_tpft_amount?:0);
            $new_motor->order_tpft_driver = $exist_sompo->order_tpft_driver?:"";
            $new_motor->order_out_amount = $this->cleanNumber($exist_sompo->order_out_amount?:0);
            $new_motor->order_out_name = $exist_sompo->order_out_name?:"";
            $new_motor->order_extrabenefit = $exist_sompo->order_extrabenefit?:"";
            $new_motor->order_ncd_previousinsurer = $exist_sompo->order_ncd_previousinsurer?:"";
            $new_motor->order_ncd_entitlement = $exist_sompo->order_ncd_entitlement?:"";
            $new_motor->order_ncd_cancellationdate = $exist_sompo->order_ncd_cancellationdate?:NULL;
            $new_motor->order_ncd_vehiclenumber = $exist_sompo->order_ncd_vehiclenumber?:"";
            $new_motor->order_ncd_remark = $exist_sompo->order_ncd_remark?:"";
            $new_motor->order_notes_remark = $exist_sompo->order_notes_remark?:"";
            $driver_list = $this->getSompoDriverList($exist_sompo->order_sompo_id);
            foreach($driver_list as $dli){
                $new_motor->{'order_driver'.$dli['sompo_driver_seqno'].'name'} = $exist_sompo->sompo_driver_name;
                $new_motor->{'order_driver'.$dli['sompo_driver_seqno'].'occupation'} = $exist_sompo->sompo_driver_other_occupation?:$this->getSompoDisplayName($exist_sompo->sompo_driver_occupation_code, 'OCCUPATION_LIST');
                $new_motor->{'order_driver'.$dli['sompo_driver_seqno'].'pass_ic'} = $exist_sompo->sompo_driver_nirc;
                $new_motor->{'order_driver'.$dli['sompo_driver_seqno'].'relationship'} = $this->getSompoDisplayName($exist_sompo->sompo_driver_relation, 'RELATIONSHIP');
                $new_motor->{'order_driver'.$dli['sompo_driver_seqno'].'dob'} = $exist_sompo->sompo_driver_dob?:null;
                $new_motor->order_driver1dob = $exist_sompo->driver_dob_1?:NULL;
            }
            $new_motor->order_id = $new_order->order_id;
            $new_motor->insertBy = Auth::user()->id;
            $new_motor->updateBy = Auth::user()->id;
            $new_motor->save();

            $exist_sompo->order_id = $new_order->order_id;
            $exist_sompo->save();

            return redirect()->route('SompoList')->with('success_msg', 'Copy Case Succesfully');
        }
        else{
            return redirect()->back()->with('error_msg','Invalid case');
        }
    }

    public function generateMotorCode(){
        $ref = Refno::where('prefix_code', $this->motor_order_prefix)->first();
        if($ref){
            $last_id = $ref->last_no;
            $last_id ++;
        }
        else{
            $new_ref = new Refno();
            $new_ref->prefix_code = $this->motor_order_prefix;
            $new_ref->last_no = 0;
            $new_ref->save();
            $last_id = 1;
        }

        $code =  env("APP_CODE").$this->motor_order_prefix .'-'. sprintf("%06d", $last_id);

        $motor_code = Order::where('order_no',$code)->first();
        if($motor_code){
            $this->updateMotorRefCode();
            return $this->generateMotorCode();
        }
        else{
            return $code; 
        }
        
    }

    public function updateMotorRefCode(){
        $ref = Refno::where('prefix_code', $this->motor_order_prefix)->first();
        $ref->last_no =  $ref->last_no + 1;
        $ref->save();
    }

    public function cleanNumber($input){
        $input = preg_replace('/[^\d.]/', '', $input);
        return $input?:0;
    }

}
