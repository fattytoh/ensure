<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\ReceiptDetail;
use App\Models\Receipt;
use App\Models\Order;
use App\Models\Motor;
use App\Models\Maid;
use App\Models\User;
use App\Models\General;
use App\Models\Customer;
use App\Models\Insuranceco;
use App\Models\InsurancePlan;
use App\Models\Refno;
use App\Models\TSA;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReceiptController extends Controller
{
    //

    public function getListing(Request $request){
        return view('receipt.List');
    }

    public function getListData(Request $request){
        // Total records
        $order_fields = ['','receipt_no','receipt_date','partner_name','receipt_bank','receipt_paid', '', ''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $count_query = Receipt::select('count(*) as allcount')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'receipt.receipt_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'receipt.receipt_customer')
        ->where('receipt.receipt_cprofile', env('APP_ID'))
        ->where('receipt.receipt_status', "!=","0");

        $main_query = Receipt::select('receipt.*', "customer.partner_name", "tsa.dealer_name")
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'receipt.receipt_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'receipt.receipt_customer')
        ->where('receipt.receipt_cprofile', env('APP_ID'))
        ->where('receipt.receipt_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('tsa.dealer_name', 'like', '%' . $searchValue . '%')
                ->orWhere('receipt.receipt_bank', 'like', '%' . $searchValue . '%')
                ->orWhere('receipt.receipt_date', 'like', '%' . $searchValue . '%')
                ->orWhere('receipt.receipt_no', 'like', '%' . $searchValue . '%');
        });

        $totalRecords = $count_query->count();
        $totalRecordswithFilter = $count_query
        ->where(function ($query) use ($searchValue) {
            $query->where('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('tsa.dealer_name', 'like', '%' . $searchValue . '%')
                ->orWhere('receipt.receipt_bank', 'like', '%' . $searchValue . '%')
                ->orWhere('receipt.receipt_date', 'like', '%' . $searchValue . '%')
                ->orWhere('receipt.receipt_no', 'like', '%' . $searchValue . '%');
        })
        ->count();

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }

        $listing_data = $main_query
        ->skip($start)
        ->take($rowperpage)
        ->get()->toArray();

        $out_data = [];

        foreach($listing_data as $ky => $li){
            $action = '';

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("ReceiptView")){
                $action .= '<a href="'.route('ReceiptView', ['id' => $li['receipt_id'] ]).'" class="btn btn-success">View</a>';
            }

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("ReceiptInfo")){
                $action .= '<a href="'.route('ReceiptInfo', ['id' => $li['receipt_id'] ]).'" class="btn btn-warning">Edit</a>';
            }

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("ReceiptDelete")){
                $action .= '<a href="'.route('ReceiptDelete', ['id' => $li['receipt_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['receipt_no'].'">Delete</a>';
            }
            $empl = User::find($li['insertBy']);
            $out_data[] = [
                $ky + 1,
                $li['receipt_no']?:'-',
                $li['receipt_date'],
                $li['partner_name']?:($li['dealer_name']?:'-'),
                $li['receipt_bank']?:'-',
                $li['receipt_paid']?:'-',
                $empl->getName(),
                $action
            ];
        }
    
        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200);
    }

    public function getOrderList(Request $request){
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        if(!$request->input("receipt_dealer") && !$request->input("receipt_customer")){
            $output = [
                "draw" => intval($draw),
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => [],
            ];

            return response()->json($output, 200);
        }
        else{
            $count_query = Order::select('count(*) as allcount')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', "!=","0");

            $main_query = Order::select('order.*', "customer.partner_name", "insurance_general.order_policyno AS gi_policyno", "insurance_motor.order_policyno AS mi_policyno", "insurance_maid.order_policyno AS gm_policyno", 'insurance_maid.order_maid_name', "insurance_motor.order_vehicles_no", "insurance_general.order_ins_doc_no", "insurance_maid.order_coverage AS gm_coverage",  "insurance_motor.order_coverage AS mi_coverage",  "insurance_general.order_coverage AS gi_coverage" )
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
            ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id')
            ->leftJoin('insurance_maid', 'insurance_maid.order_id', '=', 'order.order_id')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', "!=","0");

            if($request->input('receipt_type') == 'TSA'){
                $main_query->where('order.order_dealer', $request->input("receipt_dealer"));
                $count_query->where('order.order_dealer', $request->input("receipt_dealer"));
            }
            else{
                $main_query->where('order.order_customer', $request->input("receipt_customer"));
                $count_query->where('order.order_customer', $request->input("receipt_customer"));
            }
            
            if($request->input("fil_start_date")){
                $main_query->where('order.order_date', '>=', $request->input("fil_start_date"));
                $count_query->where('order.order_date', '>=', $request->input("fil_start_date"));
            }

            if($request->input("fil_end_date")){
                $main_query->where('order.order_date', '<=', $request->input("fil_end_date"));
                $count_query->where('order.order_date', '<=', $request->input("fil_end_date"));
            }

            if($request->input("fil_order_type") && $request->input("fil_order_type") != "ALL"){
                $main_query->where('order.order_prefix_type', $request->input("fil_order_type"));
                $count_query->where('order.order_prefix_type', $request->input("fil_order_type"));
            }

            if($request->input("fil_policyno")){
                $policy_no = $request->input("fil_policyno");
                $main_query->where(function ($query) use ($policy_no) {
                    $query->where('insurance_general.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_motor.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_maid.order_policyno', 'like', '%' . $policy_no . '%');
                });
                $count_query->where(function ($query) use ($policy_no) {
                    $query->where('insurance_general.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_motor.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_maid.order_policyno', 'like', '%' . $policy_no . '%');
                });
            }

            $totalRecords = $count_query->count();
            $totalRecordswithFilter = $count_query->count();

            $listing_data = $main_query
            ->skip($start)
            ->take($rowperpage)
            ->get()->toArray();

            $out_data = [];
            foreach($listing_data as $ky => $li){
                $no = '<div class="form-check"> <input type="checkbox" class="form-check-input" name="order_case[]" value="'.$li['order_id'].'"> <label class="form-check-label" for="order_case"> '.($ky + 1).' </label> </div>';

                if($li['order_prefix_type'] == 'GI'){
                    $cover_plan = InsurancePlan::find($li['gi_coverage']);
                    $coverage = "-";
                    if($cover_plan){
                        $coverage = $cover_plan->insuranceclass_code;
                    }
                    $related = $li['order_ins_doc_no'];
                    $order_num = '<a href="'.route('GeneralInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    $policy = $li['gi_policyno'];
                }
                elseif($li['order_prefix_type'] == 'GM'){
                    $coverage = $li['gm_coverage'];
                    $related = $li['order_maid_name'];
                    $order_num = '<a href="'.route('MaidInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    $policy = $li['gm_policyno'];
                }
                else{
                    $coverage = $li['mi_coverage'];
                    $related = $li['order_vehicles_no'];
                    $policy = $li['mi_policyno'];
                    if($li['order_prefix_type'] == 'CY'){
                        $order_num = '<a href="'.route('MotorInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    }
                    elseif($li['order_prefix_type'] == 'MT'){
                        $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    }
                    else{
                        $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    }
                }
                $amount_list = '';
                if($li['order_doc_type'] == 'DN'){
                    if($request->input('receipt_type') == 'TSA'){
                        $total = $li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'];
                        $amount_list .= 'Premium Amount :'.$li['order_grosspremium_amount']."<br>";
                        $amount_list .= 'TSA refer Fee % :'.$li['order_receivable_dealercommpercent']."%<br>";
                        $amount_list .= 'TSA refer Fee Amount :'.$li['order_receivable_dealercomm']."%<br>";
                        $amount_list .= 'Amount :'.($li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'])."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($total, $li['order_id']);
                    }
                    else{
                        $amount_list .= 'Premium Amount :'.$li['order_grosspremium_amount'] ."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($li['order_grosspremium_amount'], $li['order_id']);
                    }
                  
                }
                else{
                    if($request->input('receipt_type') == 'TSA'){
                        $total = $li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'];
                        $amount_list .= 'Premium Amount :-'.$li['order_grosspremium_amount']."<br>";
                        $amount_list .= 'TSA refer Fee % :'.$li['order_receivable_dealercommpercent']."%<br>";
                        $amount_list .= 'TSA refer Fee Amount :'.$li['order_receivable_dealercomm']."%<br>";
                        $amount_list .= 'Amount :'.($li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'])."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance( $total, $li['order_id']);
                    }
                    else{
                        $amount_list .= 'Premium Amount :-'.$li['order_grosspremium_amount'] ."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($li['order_grosspremium_amount'], $li['order_id']);
                    }
                }

                $out_data[] = [
                    $no,
                    $coverage,
                    $related,
                    $li['partner_name']?:'-',
                    $order_num,
                    $policy,
                    $amount_list
                ];
            }
        
            $output = [
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordswithFilter,
                "aaData" => $out_data,
            ];

            return response()->json($output, 200);
        }
    } 

    public function getBalance($amount, $order_id){
        $balance = 0;
        $total = ReceiptDetail::leftJoin('receipt', 'receipt.receipt_id', '=', 'receipt_detail.recpline_receipt_id')
        ->where('receipt.receipt_status', '1')
        ->where('receipt_detail.recpline_status', '1')
        ->where('receipt_detail.recpline_order_id', $order_id)
        ->sum("receipt_detail.recpline_offset_amount");

        $balance = $amount - $total;
        return $balance;
    }

    public function getPaidTotal($order_id){
        $balance = 0;
        $total = ReceiptDetail::leftJoin('receipt', 'receipt.receipt_id', '=', 'receipt_detail.recpline_receipt_id')
        ->where('receipt.receipt_status', '1')
        ->where('receipt_detail.recpline_status', '1')
        ->where('receipt_detail.recpline_order_id', $order_id)
        ->sum("receipt_detail.recpline_offset_amount");

        return $total;
    }

    public function receiptInfo(Request $request){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        if($request->route('id')){
            $data = Receipt::where('receipt_id',$request->route('id'))
            ->where('receipt_cprofile', env("APP_ID"))
            ->where('receipt_status', 1)
            ->first();
            if($data){
                $exist_dealer = TSA::find($data->receipt_dealer);
                $exist_customer = Customer::find($data->receipt_customer);
                $add_name = User::find($data->insertBy);
                $up_name = User::find($data->updateBy);
                $data->update_name =  $up_name->getName()?:'Webmaster';
                $data->insert_name =  $add_name->getName()?:'Webmaster';
                $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
                $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
                $data->customer_name = isset($exist_customer->partner_name)?$exist_customer->partner_name:'';
                $data->customer_ic = isset($exist_customer->partner_nirc)?substr_replace($exist_customer->partner_nirc,'XXXX', 1,-4):'';
                $data->receipt_list = $this->getReceiptDetailList($request->route('id'), $data->receipt_type);
                $file_attach = new FilesController();
                $data->file_list = $file_attach->getFilesList('ens_receipt',$data->receipt_id);
                $data->disabled = "";
                $data->insuranceco_list = $insuranceco_list;
                return view('receipt.Info', $data);
            }
            else{
                return redirect()->route('ReceiptList');
            }
        }
        else{
            $data = [
                'insuranceco_list'=> $insuranceco_list,
            ];
            return view('receipt.Add', $data);
        }
    }

    public function receiptViewInfo(Request $request){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = Receipt::where('receipt_id',$request->route('id'))
            ->where('receipt_cprofile', env("APP_ID"))
            ->where('receipt_status', 1)
            ->first();
        if($data){
            $exist_dealer = TSA::find($data->receipt_dealer);
            $exist_customer = Customer::find($data->receipt_customer);
            $add_name = User::find($data->insertBy);
            $up_name = User::find($data->updateBy);
            $data->update_name =  $up_name->getName()?:'Webmaster';
            $data->insert_name =  $add_name->getName()?:'Webmaster';
            $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
            $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
            $data->customer_name = isset($exist_customer->partner_name)?$exist_customer->partner_name:'';
            $data->customer_ic = isset($exist_customer->partner_nirc)?substr_replace($exist_customer->partner_nirc,'XXXX', 1,-4):'';
            $data->receipt_list = $this->getReceiptDetailList($request->route('id'), $data->receipt_type, true);
            $file_attach = new FilesController();
            $data->file_list = $file_attach->getFilesList('ens_receipt',$data->receipt_id);
            $data->disabled = "disabled";
            $data->insuranceco_list = $insuranceco_list;
            return view('receipt.View', $data);
        }
        else{
            return redirect()->route('ReceiptList');
        }
    }

    public function AddOrderToList(Request $request){
        $html = "";
        foreach($request->input('order_case') as $ord_li){
            $old_post = $request->input('old_post')?:[];
            if(!in_array($ord_li, $old_post)){
                $order_data = Order::where('order_id',$ord_li)->where('order_status', 1)->first();
                if($order_data){
                    $customer_name = Customer::where('partner_id', $order_data->order_customer)->first();
                    if($order_data->order_prefix_type == 'GI'){
                        $gi_order = General::where('order_id',$ord_li)->first();
                        $cover_plan = InsurancePlan::find($gi_order->order_coverage);
                        $coverage = "-";
                        if($cover_plan){
                            $coverage = $cover_plan->insuranceclass_code;
                        }
                        $related = $gi_order->order_ins_doc_no;
                        $order_num = '<a href="'.route('GeneralInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gi_order->order_policyno;
                    }
                    elseif($order_data->order_prefix_type == 'GM'){
                        $gm_order = Maid::where('order_id',$ord_li)->first();
                        $coverage = $gm_order->order_coverage;
                        $related = $gm_order->order_maid_name;
                        $order_num = '<a href="'.route('MaidInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gm_order->order_policyno;
                    }
                    else{
                        $mi_order = Motor::where('order_id',$ord_li)->first();
                        $coverage = $mi_order->order_coverage;
                        $related = $mi_order->order_vehicles_no;
                        $policy = $mi_order->order_policyno;
                        if($li['order_prefix_type'] == 'CY'){
                            $order_num = '<a href="'.route('MotorInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        elseif($li['order_prefix_type'] == 'MT'){
                            $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        else{
                            $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                    }

                    $amount_list = '';
                    if($order_data->order_doc_type == 'DN'){
                        if($request->input('receipt_type') == 'TSA'){
                            $total = $order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $order_data->order_grosspremium_amount;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        
                    }
                    else{
                        if($request->input('receipt_type') == 'TSA'){
                            $total = $order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."%<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $order_data->order_grosspremium_amount;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                    }
                    
                    $html .= '<div id="order_box_'.$ord_li.'" class="row order_wrapper info_order_wrapper">';
                    $html .= '<div class="col-md-3">';
                    $html .= 'Coverage :'.$coverage.'<br>';
                    $html .= 'Related Info :'.$related.'<br>';
                    $html .= 'Issue Name :'.$customer_name->partner_name;
                    $html .= '</div>';
                    $html .= '<div class="col-md-3">';
                    $html .= 'Order No :'.$order_num.'<br>';
                    $html .= 'Policy Number :'.$policy;
                    $html .= '</div>';
                    $html .= '<div class="col-md-3">';
                    $html .=  $amount_list;
                    $html .= '<div class="mt-3">';
                    $html .= '<button type="button" class="del_ord_btn btn btn-danger" rel="'.$ord_li.'">Delete</button>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="col-md-3">';
                    $html .= '<input type="hidden" name="receipt_list['.$ord_li.'][recpline_id]" value="0">';
                    $html .= '<input type="hidden" name="receipt_list['.$ord_li.'][order_id]" class="order_id_input" value="'.$ord_li.'">';
                    $html .= '<input type="hidden" name="receipt_list['.$ord_li.'][balance]" value="'.$balance.'">';
                    $html .= '<input type="hidden" name="receipt_list['.$ord_li.'][order_amount]" value="'.$total.'">';
                    $html .= '<input type="number" step="0.01" class="form-control offset_amt text-right" name="receipt_list['.$ord_li.'][offset_amount]" value="'.$balance.'" >';
                    $html .= '</div>';
                    $html .= '</div>';
                }
            }
        }
        $output = [
            'html' => $html
        ];
        return response()->json($output, 200);
    }

    public function create(Request $request){
        $rules = [
            'receipt_dealer' => 'required_if:receipt_type,==,TSA',
            'receipt_insuranco' => 'required_if:receipt_type,==,InsuranceComp',
            'receipt_customer' => 'required_if:receipt_type,==,Customer',
            'receipt_date' => 'required|date',
            'receipt_paid' => 'required',
            'receipt_received' => 'required',
        ];

        $attr = [
            'receipt_dealer' => 'TSA',
            'receipt_insuranco' => 'Insurance Company',
            'receipt_customer' => 'Customer',
            'receipt_date' => 'Receipt Date',
            'receipt_paid' => 'Receipt Amount',
            'receipt_received' => 'Received Amount',
        ];

        $request->validate($rules, [], $attr);

        $errors_input = [];

        $files_allow = ['jpg','bmp','png','pdf'];

        if ($request->hasFile('receipt_files')) {
            foreach($request->file('receipt_files') as $key => $fli){
                if($fli('receipt_files')->getSize() > 5242880 ){
                    $errors_input['receipt_files'] = 'Attachment Cannot over 5MB';
                }
                elseif(!in_array($fli->extension(),$files_allow)){
                    $errors_input['receipt_files'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                }
            }
        }     

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $new_receipt = new Receipt();
        $new_receipt->receipt_no = $this->generateCode();
        $this->updateRefCode();
        $new_receipt->receipt_type = $request->input('receipt_type');
        $new_receipt->receipt_dealer = $request->input('receipt_dealer')?:0;
        $new_receipt->receipt_customer = $request->input('receipt_customer')?:0;
        $new_receipt->receipt_cprofile = env("APP_ID");
        $new_receipt->receipt_date = $request->input('receipt_date');
        $new_receipt->receipt_method = $request->input('receipt_method');
        $new_receipt->receipt_bank = $request->input('receipt_bank');
        $new_receipt->receipt_cheque = $request->input('receipt_cheque');
        $new_receipt->receipt_received = $request->input('receipt_received');
        $new_receipt->receipt_paid = $request->input('receipt_paid');
        $new_receipt->receipt_remarks = $request->input('receipt_remarks');
        $new_receipt->receipt_status = 1;
        $new_receipt->insertBy = Auth::user()->id;
        $new_receipt->updateBy = Auth::user()->id;
        $new_receipt->save();

        if($request->input('receipt_list')){
            foreach($request->input('receipt_list') as $pli){
                $new_recpline = new ReceiptDetail();
                $new_recpline->recpline_receipt_id  = $new_receipt->receipt_id;
                $new_recpline->recpline_order_id  = $pli['order_id'];
                $new_recpline->recpline_order_amount  = $pli['order_amount'];
                $new_recpline->recpline_offset_amount  = $pli['offset_amount']?:0;
                $new_recpline->recpline_status = 1;
                $new_recpline->insertBy = Auth::user()->id;
                $new_recpline->updateBy = Auth::user()->id;
                $new_recpline->save();
            }
        }

        if($request->hasFile('receipt_files')){
            $file_ctrl = new FilesController();
            $file_ctrl->uploadMultipleFile($request,'ens_receipt',$new_receipt->receipt_id,'uploads/receipt','receipt_files');
        }

        return redirect()->route('ReceiptList')->with('success_msg', 'Add Case Succesfully');
    }

    public function update(Request $request){
        $exist_receipt = Receipt::where('receipt_id',$request->input('receipt_id'))
        ->where('receipt_cprofile', env("APP_ID"))
        ->where('receipt_status', 1)
        ->first();
        if(!$exist_receipt){
            return redirect()->route('ReceiptList')->with('error_msg', 'Invalid Case');
        }
        else{
            $rules = [
                'receipt_dealer' => 'required_if:receipt_type,==,TSA',
                'receipt_insuranco' => 'required_if:receipt_type,==,InsuranceComp',
                'receipt_customer' => 'required_if:receipt_type,==,Customer',
                'receipt_date' => 'required|date',
                'receipt_paid' => 'required',
            ];

            $attr = [
                'receipt_dealer' => 'TSA',
                'receipt_insuranco' => 'Insurance Company',
                'receipt_customer' => 'Customer',
                'receipt_date' => 'receipt Date',
                'receipt_paid' => 'receipt Amount',
            ];

            $request->validate($rules, [], $attr);

            $errors_input = [];

            $files_allow = ['jpg','bmp','png','pdf'];

            if ($request->hasFile('receipt_files')) {
                foreach($request->file('receipt_files') as $key => $fli){
                    if($fli->getSize() > 5242880 ){
                        $errors_input['receipt_files'] = 'Attachment Cannot over 5MB';
                    }
                    elseif(!in_array($fli->extension(),$files_allow)){
                        $errors_input['receipt_files'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                    }
                }
            }     

            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }

            $exist_receipt->receipt_type = $request->input('receipt_type');
            $exist_receipt->receipt_dealer = $request->input('receipt_dealer')?:0;
            $exist_receipt->receipt_insuranco = $request->input('receipt_insuranco')?:0;
            $exist_receipt->receipt_customer = $request->input('receipt_customer')?:0;
            $exist_receipt->receipt_date = $request->input('receipt_date');
            $exist_receipt->receipt_method = $request->input('receipt_method');
            $exist_receipt->receipt_bank = $request->input('receipt_bank');
            $exist_receipt->receipt_cheque = $request->input('receipt_cheque');
            $exist_receipt->receipt_paid = $request->input('receipt_paid');
            $exist_receipt->receipt_remarks = $request->input('receipt_remarks');
            $exist_receipt->updateBy = Auth::user()->id;
            $exist_receipt->save();

            if($request->input('receipt_list')){
                ReceiptDetail::where('recpline_receipt_id', $exist_receipt->receipt_id)->update(['recpline_status' => 0]);
                foreach($request->input('receipt_list') as $pli){
                    if($pli['recpline_id'] == 0){
                        $new_recpline = new ReceiptDetail();
                        $new_recpline->recpline_receipt_id  = $new_receipt->receipt_id;
                        $new_recpline->recpline_order_id  = $pli['order_id'];
                        $new_recpline->recpline_order_amount  = $pli['order_amount'];
                        $new_recpline->recpline_offset_amount  = $pli['offset_amount']?:0;
                        $new_recpline->recpline_status = 1;
                        $new_recpline->insertBy = Auth::user()->id;
                        $new_recpline->updateBy = Auth::user()->id;
                        $new_recpline->save();
                    }
                    else{
                        $exist_recpline = ReceiptDetail::find($pli['recpline_id']);
                        $exist_recpline->recpline_offset_amount  = $pli['offset_amount']?:0;
                        $exist_recpline->recpline_status = 1;
                        $exist_recpline->updateBy = Auth::user()->id;
                        $exist_recpline->save();
                    }
                }
            }

            if($request->hasFile('receipt_files')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_receipt',$exist_receipt->receipt_id,'uploads/receipt','receipt_files');
            }

            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $exist_receipt = Receipt::find($request->route('id'));
        if($exist_receipt){
            $exist_receipt->order_status = "0";
            $exist_receipt->save();
        }
    
        return redirect()->route('ReceiptList')->with('success_msg', 'Delete Case Succesfully');
    }

    public function generateCode(){
        if(env("APP_ID") == 2){
            $prefix = "RE";
        }
        else{
            $prefix = "RP";
        }
        $ref = Refno::where('prefix_code', $prefix)->first();
        if($ref){
            $last_id = $ref->last_no;
            $last_id ++;
        }
        else{
            $new_ref = new Refno();
            $new_ref->prefix_code = $prefix;
            $new_ref->last_no = 0;
            $new_ref->save();
            $last_id = 1;
        }

        $code =  $prefix .'-'. sprintf("%06d", $last_id);

        $motor_code = Receipt::where('receipt_no',$code)->first();
        if($motor_code){
            $this->updateRefCode();
            return $this->generateCode();
        }
        else{
            return $code; 
        }
        
    }

    public function updateRefCode(){
        if(env("APP_ID") == 2){
            $prefix = "RE";
        }
        else{
            $prefix = "RP";
        }

        $ref = Refno::where('prefix_code', $prefix)->first();
        $ref->last_no =  $ref->last_no + 1;
        $ref->save();
    }

    public function getReceiptDetailList($receipt_id, $pay_to, $is_view = false){
        $py_dt = ReceiptDetail::where('recpline_receipt_id', $receipt_id)
                ->where('recpline_status', 1)
                ->get()->toArray();
        $output_data = [];
        if($py_dt){
            foreach($py_dt as $py_ky => $py_li){
                $order_data = Order::where('order_id',$py_li['recpline_order_id'])->where('order_status', 1)->first();
                if($order_data){
                    $customer_name = Customer::where('partner_id', $order_data->order_customer)->first();
                    if($order_data->order_prefix_type == 'GI'){
                        $gi_order = General::where('order_id',$py_li['recpline_order_id'])->first();
                        $cover_plan = InsurancePlan::find($gi_order->order_coverage);
                        $coverage = "-";
                        if($cover_plan){
                            $coverage = $cover_plan->insuranceclass_code;
                        }
                        $related = $gi_order->order_ins_doc_no;
                        $order_num = '<a href="'.route('GeneralInfo', ['id' => $py_li['recpline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gi_order->order_policyno;
                    }
                    elseif($order_data->order_prefix_type == 'GM'){
                        $gm_order = Maid::where('order_id',$py_li['recpline_order_id'])->first();
                        $coverage = $gm_order->order_coverage;
                        $related = $gm_order->order_maid_name;
                        $order_num = '<a href="'.route('MaidInfo', ['id' => $py_li['recpline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gm_order->order_policyno;
                    }
                    else{
                        $mi_order = Motor::where('order_id',$py_li['recpline_order_id'])->first();
                        $coverage = $mi_order->order_coverage;
                        $related = $mi_order->order_vehicles_no;
                        $policy = $mi_order->order_policyno;
                        if($li['order_prefix_type'] == 'CY'){
                            $order_num = '<a href="'.route('MotorInfo', ['id' => $py_li['recpline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        elseif($li['order_prefix_type'] == 'MT'){
                            $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $py_li['recpline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        else{
                            $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $py_li['recpline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                    }

                    $amount_list = '';
                    if($order_data->order_doc_type == 'DN'){
                        if($pay_to == 'TSA'){
                            $total = $py_li['recpline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $py_li['recpline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                    }
                    else{
                        if($pay_to == 'TSA'){
                            $total = $py_li['recpline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."%<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $py_li['recpline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                    }
                    
                    $output_data[] = [
                        'recpline_id' => $py_li['recpline_id'],
                        'order_id' => $py_li['recpline_order_id'],
                        'coverage' => $coverage,
                        'related' => $related,
                        'customer_name' => $customer_name->partner_name,
                        'order_no' => $order_num,
                        'policy' => $policy,
                        'amount_list' => $amount_list,
                        'balance' => $balance,
                        'total' => $total,
                        'offset_amt' => $py_li['recpline_offset_amount'],
                        'is_view' => $is_view,
                    ];
                }
            }
        }

        return $output_data;
    }

    public function getOldCaseList($order_id, $balance, $offset, $order_amt, $recpline_id, $receipt_type){
        $html = "";
        $order_data = Order::where('order_id',$order_id)->where('order_status', 1)->first();
        if($order_data){
            $customer_name = Customer::where('partner_id', $order_data->order_customer)->first();
            if($order_data->order_prefix_type == 'GI'){
                $gi_order = General::where('order_id',$order_id)->first();
                $cover_plan = InsurancePlan::find($gi_order->order_coverage);
                $coverage = "-";
                if($cover_plan){
                    $coverage = $cover_plan->insuranceclass_code;
                }
                $related = $gi_order->order_ins_doc_no;
                $order_num = '<a href="'.route('GeneralInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                $policy = $gi_order->order_policyno;
            }
            elseif($order_data->order_prefix_type == 'GM'){
                $gm_order = Maid::where('order_id',$order_id)->first();
                $coverage = $gm_order->order_coverage;
                $related = $gm_order->order_maid_name;
                $order_num = '<a href="'.route('MaidInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                $policy = $gm_order->order_policyno;
            }
            else{
                $mi_order = Motor::where('order_id',$order_id)->first();
                $coverage = $mi_order->order_coverage;
                $related = $mi_order->order_vehicles_no;
                $policy = $mi_order->order_policyno;
                if($li['order_prefix_type'] == 'CY'){
                    $order_num = '<a href="'.route('MotorInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                }
                elseif($li['order_prefix_type'] == 'MT'){
                    $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                }
                else{
                    $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                }
            }

            $amount_list = '';
            if($order_data->order_doc_type == 'DN'){
                if($receipt_type == 'TSA'){
                    $total = $order_amt;
                    $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                    $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                    $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."<br>";
                    $amount_list .= 'Amount :'.$total."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
                else{
                    $total = $order_amt;
                    $amount_list .= 'Premium Amount :'.$total ."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
                
            }
            else{
                if($request->input('receipt_type') == 'TSA'){
                    $total = $order_amt;
                    $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                    $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                    $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."%<br>";
                    $amount_list .= 'Amount :'.$order_amt."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
                else{
                    $total = $order_amt;
                    $amount_list .= 'Premium Amount :-'.$order_amt ."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
            }
            
            $html .= '<div id="order_box_'.$order_id.'" class="row order_wrapper info_order_wrapper">';
            $html .= '<div class="col-md-3">';
            $html .= 'Coverage :'.$coverage.'<br>';
            $html .= 'Related Info :'.$related.'<br>';
            $html .= 'Issue Name :'.$customer_name->partner_name;
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .= 'Order No :'.$order_num.'<br>';
            $html .= 'Policy Number :'.$policy;
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .=  $amount_list;
            $html .= '<div class="mt-3">';
            $html .= '<button type="button" class="del_ord_btn btn btn-danger" rel="'.$order_id.'">Delete</button>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .= '<input type="hidden" name="receipt_list['.$order_id.'][recpline_id]" value="'.$recpline_id.'">';
            $html .= '<input type="hidden" name="receipt_list['.$order_id.'][order_id]" class="order_id_input" value="'.$order_id.'">';
            $html .= '<input type="hidden" name="receipt_list['.$order_id.'][balance]" value="'.$balance.'">';
            $html .= '<input type="hidden" name="receipt_list['.$order_id.'][order_amount]" value="'.$total.'">';
            $html .= '<input type="number" step="0.01" class="form-control offset_amt text-right" name="receipt_list['.$order_id.'][offset_amount]" value="'.$offset.'" >';
            $html .= '</div>';
            $html .= '</div>';
        }

        return $html;
    }

    public function getOrderReceipt($order_id){
        $output = [];
        $main_query = ReceiptDetail::select('receipt_detail.*', 'receipt.receipt_no', 'receipt.receipt_date', 'receipt.receipt_bank', 'receipt.receipt_cheque' )
        ->leftJoin('receipt', 'receipt.receipt_id', '=', 'receipt_detail.recpline_receipt_id')
        ->where('receipt.receipt_status', 1)
        ->where('receipt_detail.recpline_order_id', $order_id)
        ->where('receipt_detail.recpline_status', 1)
        ->get()->toArray();
        $is_view = false;
        $is_edit = false;
        
        if(UserGroupPrivilegeController::checkPrivilegeWithAuth("ReceiptInfo")){
            $is_edit = true;
        }
        
        if(UserGroupPrivilegeController::checkPrivilegeWithAuth("ReceiptView")){
            $is_view = true;
        }

        foreach($main_query as $mli){
            $insert_name = User::find($mli['insertBy']);
            $file_attach = new FilesController();
            $file_list = $file_attach->getFilesList('ens_receipt',$mli['recpline_receipt_id']);
            $output[] = [
                'receipt_id' => $mli['recpline_receipt_id'],
                'receipt_no' => $mli['receipt_no'],
                'receipt_date' => $mli['receipt_date'],
                'receipt_bank' => $mli['receipt_bank'],
                'receipt_cheque' => $mli['receipt_cheque'],
                'recpline_offset_amount' => $mli['recpline_offset_amount'],
                'insertBy' => $insert_name->getName(),
                'file_list' =>  $file_list ,
                'is_view' =>  $is_view ,
                'is_edit' =>  $is_edit ,
            ];
        }

        return $output;
    }
}
