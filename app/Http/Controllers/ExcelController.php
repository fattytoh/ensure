<?php

namespace App\Http\Controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\TSA;
use App\Models\Insuranceco;

class ExcelController extends Controller
{
    public $insurance_type_mapping = [
        "MI" => "MotorCycle Insurance",
        "PMI" => "Private Motor Insurance",
        "CMI" => "Commercial Motor Insurance",
        "GMM" => "Domestic Maid Insurance",
        "GI" => "General Insurance",
        "ALL" => "ALL Insurance",
    ];

    public $maid_coverage_mapping = [
        'Exclusive Plan' => 'E',
        'Deluxe Plan' => 'D',
        'Classic Plan' => 'C',
        '300MDC' => 'O3',
        '500MDC' => 'O5',
        '2K' => 'PHB2',
        '7K' => 'PHB7',
    ];
    //
    public function TSAStateExcel()
    {
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'TSA Referral Fees Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );

        if(in_array($this->insurance_type,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "No" => array('row'=>"A"),
                "DN No" => array('row'=>"B", 'param'=>'order_no'),
                "Rec. Date" =>array('row'=>"C", 'param'=>'order_date'),
                "Veh No" => array('row'=>"D", 'param'=>'order_vehicles_no'),
                "Insured Name" => array('row'=>"E", 'param'=>'partner_name'),
                "IC No" => array('row'=>"F", 'param'=>'partner_nirc'),
                "Gross Premium" => array('row'=>"G", 'param'=>'order_gross_premium', 'number' => true),
                "Pmm Receivable" => array('row'=>"H", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Rec Amount" => array('row'=>"I", 'param'=>'paid_total', 'number' => true),
                "Discount" => array('row'=>"J", 'param'=>'order_direct_discount_amt', 'number' => true),
                "TSA %" => array('row'=>"K", 'param'=>'order_receivable_dealercommpercent', 'number' => true),
                "TSA Refer fee" => array('row'=>"L", 'param'=>'order_receivable_dealercomm', 'number' => true),
                "GST Refer fee" => array('row'=>"M", 'param'=>'order_receivable_dealergstamount', 'number' => true),
                "Net Refer fee" => array('row'=>"N", 'param'=>'total_tsa_comm', 'number' => true),
            );
        }
        else{
            $header = array(
                "No" => array('row'=>"A"),
                "DN No" => array('row'=>"B", 'param'=>'order_no'),
                "Rec. Date" =>array('row'=>"C", 'param'=>'order_date'),
                "Coverage" =>array('row'=>"D", 'param'=>'order_coverage'),
                "Insured Name" => array('row'=>"E", 'param'=>'partner_name'),
                "IC No" => array('row'=>"F", 'param'=>'partner_nirc'),
                "Gross Premium" => array('row'=>"G", 'param'=>'order_gross_premium', 'number' => true),
                "Pmm Receivable" => array('row'=>"H", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Rec Amount" => array('row'=>"I", 'param'=>'paid_total', 'number' => true),
                "Discount" => array('row'=>"J", 'param'=>'order_direct_discount_amt', 'number' => true),
                "TSA %" => array('row'=>"K", 'param'=>'order_receivable_dealercommpercent', 'number' => true),
                "TSA Refer fee" => array('row'=>"L", 'param'=>'order_receivable_dealercomm', 'number' => true),
                "GST Refer fee" => array('row'=>"M", 'param'=>'order_receivable_dealergstamount', 'number' => true),
                "Net Refer fee" => array('row'=>"N", 'param'=>'total_tsa_comm', 'number' => true),
            );
        }
        
        if($this->hide_commission == 'Y'){
            unset($header["TSA %"],$header["TSA Refer fee"],$header["GST Refer fee"]);   
        }

        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }
        
        $grand_total = array(
            'order_gross_premium' => 0,
            'order_receivable_premiumreceivable' => 0,
            'paid_total' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'total_tsa_comm' => 0,
        );
        $count_no = 0;
        $row_count = 3;
        if($this->order_dealer > 0){
            $dealer_data = $this->getDealerInfoByID($this->order_dealer);
            $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
            $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
            $row_count++;
            $sub_total = array(
                'order_gross_premium' => 0,
                'order_receivable_premiumreceivable' => 0,
                'paid_total' => 0,
                'order_direct_discount_amt' => 0,
                'order_receivable_dealercomm' => 0,
                'order_receivable_dealergstamount' => 0,
                'total_tsa_comm' => 0,
            );
            foreach($this->data_listing as $key => $list_item){
                foreach($sub_total as $sub_key => $sub_li){
                    $sub_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                    $grand_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                }

                foreach($header as $key => $li){
                    if(isset($li['param'])){
                        $sheet->setCellValue($li['row'].$row_count, $list_item[$li['param']]);
                    }else{
                        $sheet->setCellValue($li['row'].$row_count, $dis + 1);
                    }
                    if(isset($li['number'])){
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                }
                
                $row_count++;
            
            }

            $sheet->setCellValue("A".$row_count, "Sub Total");
            foreach($header as $key => $li){
                if(isset($li['param']) && isset($sub_total[$li['param']])){
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
            $row_count++;
            $row_count++;
        }
        else{
            foreach($this->data_listing as $key => $list_item){
                $sub_total = array(
                    'order_gross_premium' => 0,
                    'order_receivable_premiumreceivable' => 0,
                    'paid_total' => 0,
                    'order_direct_discount_amt' => 0,
                    'order_receivable_dealercomm' => 0,
                    'order_receivable_dealergstamount' => 0,
                    'total_tsa_comm' => 0,
                );

                if($key > 0){
                    $dealer_data = $this->getDealerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }

                foreach($list_item as $dis => $list_data){
                    foreach($sub_total as $sub_key => $sub_li){
                        $sub_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                        $grand_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                    }

                    foreach($header as $key => $li){
                        if(isset($li['param'])){
                            $sheet->setCellValue($li['row'].$row_count, $list_data[$li['param']]);
                        }else{
                            $sheet->setCellValue($li['row'].$row_count, $dis + 1);
                        }
                        if(isset($li['number'])){
                            $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                        }
                    }
                    $row_count++;
                }
                $row_count++;
                $sheet->setCellValue("A".$row_count, "Sub Total");
                foreach($header as $key => $li){
                    if(isset($li['param']) && isset($sub_total[$li['param']])){
                        $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                }
                $row_count++;
                $row_count++;
            }
        }

        $sheet->setCellValue("A".$row_count, "Grand Total");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        foreach($header as $key => $li){
            if(isset($li['param']) && isset($grand_total[$li['param']])){
                $sheet->setCellValue($li['row'].$row_count, $grand_total[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
            }
        }

        // Create a writer and write to a memory stream
        $writer = new Xlsx($spreadsheet);
        $filename = 'TSA_Report-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function OutStandingExcel()
    {
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Outstanding Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );
        if(in_array($this->insurance_type,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "No" => array('row'=>"A"),
                "Entry Date" =>array('row'=>"A", 'param'=>'order_date'),
                "DN No" => array('row'=>"B", 'param'=>'order_no'),
                "Policy Number" => array('row'=>"C", 'param'=>'order_policyno'),
                "Vehicle Number" => array('row'=>"D", 'param'=>'order_vehicles_no'),
                "Hirer Name" => array('row'=>"E", 'param'=>'partner_name'),
                "TSA Name" => array('row'=>"F", 'param'=>'dealer_name'),
                "Premium" => array('row'=>"G", 'param'=>'order_gross_premium', 'number' => true),
                "Receivable Premium" => array('row'=>"H", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Receivable Payment" => array('row'=>"I", 'param'=>'paid_total', 'number' => true),
                "Payment O/S" => array('row'=>"J", 'param'=>'order_outstanding', 'number' => true),
                "O/S day" => array('row'=>"K", 'param'=>'order_outstanding_day'),
                "Sales Type" => array('row'=>"L", 'param'=>'order_display_type'),
            );
        }
        else{
            $header = array(
                "No" => array('row'=>"A"),
                "Entry Date" =>array('row'=>"B", 'param'=>'order_date'),
                "DN No" => array('row'=>"C", 'param'=>'order_no'),
                "Policy Number" => array('row'=>"D", 'param'=>'order_policyno'),
                "Coverage Plan" => array('row'=>"E", 'param'=>'order_coverage'),
                "Hirer Name" => array('row'=>"F", 'param'=>'partner_name'),
                "TSA Name" => array('row'=>"G", 'param'=>'dealer_name'),
                "Premium" => array('row'=>"H", 'param'=>'order_gross_premium', 'number' => true),
                "Receivable Premium" => array('row'=>"I", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Receivable Payment" => array('row'=>"J", 'param'=>'paid_total', 'number' => true),
                "Payment O/S" => array('row'=>"K", 'param'=>'order_outstanding', 'number' => true),
                "O/S day" => array('row'=>"L", 'param'=>'order_outstanding_day'),
                "Sales Type" => array('row'=>"M", 'param'=>'order_display_type'),
            );
        }
        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }
        $grand_total = array(
            'order_gross_premium' => 0,
            'order_receivable_premiumreceivable' => 0,
            'paid_total' => 0,
            'order_outstanding' => 0,
        );
        $count_no = 0;
        $row_count = 3;
        if($this->report_group){
            foreach($this->data_listing as $key => $list_item){
                $sub_total = array(
                    'order_gross_premium' => 0,
                    'order_receivable_premiumreceivable' => 0,
                    'paid_total' => 0,
                    'order_outstanding' => 0,
                );
                if($this->report_group == 'dealer'){
                    $dealer_data = $this->getDealerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }
                elseif($this->report_group == 'customer'){
                    $partner_data = $this->getPartnerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "Customer :".$partner_data['partner_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }
                elseif($this->report_group == 'insuranceco'){
                    $ins_data = $this->getInsuranceComapnyInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "Insurance Comapny :".$ins_data['insuranceco_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }
                
                foreach($list_item as $dis => $list_data){
                    foreach($sub_total as $sub_key => $sub_li){
                        $sub_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                        $grand_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                    }
                    
                    foreach($header as $key => $li){
                        if($li['param']){
                            $sheet->setCellValue($li['row'].$row_count, $list_data[$li['param']]);
                        }else{
                            $sheet->setCellValue($li['row'].$row_count, $dis + 1);
                        }
                        if($li['number']){
                            $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                        }
                    }
                    $row_count++;
                }
                $row_count++;
                $sheet->setCellValue("A".$row_count, "Sub Total");
                foreach($header as $key => $li){
                    if($sub_total[$li['param']]){
                        $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                }
                $row_count++;
                $row_count++;
            }
        }
        else{
            if($this->order_dealer > 0){
                $dealer_data = $this->getDealerInfoByID($this->order_dealer);
                $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
                $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                $row_count++;
            }
            elseif($this->order_customer > 0){
                $partner_data = $this->getPartnerInfoByID($key);
                $sheet->setCellValue('A'.$row_count, "Customer :".$partner_data['partner_name']);
                $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                $row_count++;
            }
            elseif($this->order_insurco > 0){
                $ins_data = $this->getInsuranceComapnyInfoByID($key);
                $sheet->setCellValue('A'.$row_count, "Insurance Comapny :".$ins_data['insuranceco_name']);
                $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                $row_count++;
            }
            foreach($this->data_listing as $key => $list_item){
                $sub_total = array(
                    'order_gross_premium' => 0,
                    'order_receivable_premiumreceivable' => 0,
                    'paid_total' => 0,
                    'order_outstanding' => 0,
                );
            }
            foreach($this->data_listing as $dis => $list_data){
                foreach($sub_total as $sub_key => $sub_li){
                    $sub_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                    $grand_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                }

                foreach($header as $key => $li){
                    if(isset($li['param'])){
                        $sheet->setCellValue($li['row'].$row_count, $list_data[$li['param']]);
                    }else{
                        $sheet->setCellValue($li['row'].$row_count, $dis + 1);
                    }
                    if(isset($li['number'])){
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                }
                $row_count++;
            }
            $row_count++;
            $sheet->setCellValue("A".$row_count, "Sub Total");
            $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
            foreach($header as $key => $li){
                if(isset($li['param']) && isset($sub_total[$li['param']])){
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
                }
            }
            $row_count++;
            $row_count++;
        }
        $sheet->setCellValue("A".$row_count, "Grand Total");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        foreach($header as $key => $li){
            if(isset($li['param']) && isset($grand_total[$li['param']])){
                $sheet->setCellValue($li['row'].$row_count, $grand_total[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
            }
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'Outstanding_Report-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function DebitNoteExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        if(in_array($this->insurance_type,array("MI","PMI","CMI", "PCMI"))){
            $header = array(
                "No" => array('row'=>"A"),
                "Issued Date" =>array('row'=>"B", 'param'=>'order_date'),
                "Debit Number" => array('row'=>"C", 'param'=>'order_no'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Vehicles Number" => array('row'=>"E", 'param'=>'order_vehicles_no'),
                "Policy Number" => array('row'=>"F", 'param'=>'order_policyno'),
                "Premium Charges" => array('row'=>"G", 'param'=>'order_grosspremium_amount', 'number' => true),
                "Receipt Number" => array('row'=>"H", 'param'=>'receipt_data'),
                "Name Of TSA" => array('row'=>"I", 'param'=>'dealer_name'),
                "TSA Comm" => array('row'=>"J", 'param'=>'total_tsa_comm'),
                "Comm Cheque" => array('row'=>"K", 'param'=>'tsa_comm_cheque'),
                "Insurance company" => array('row'=>"L", 'param'=>'insuranceco_name'),
                "Cheque paid to Insurance company" => array('row'=>"M", 'param'=>'to_ins_cheque'),
                "Premium charge to TSA" => array('row'=>"N", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Nett premium" => array('row'=>"O", 'param'=>'order_subtotal_amount', 'number' => true),
                "Premium GST" => array('row'=>"P", 'param'=>'order_gst_amount', 'number' => true),
                "Commission to Ensure/ Peoples" => array('row'=>"Q", 'param'=>'order_payable_ourcomm', 'number' => true),
                "GST on commission" => array('row'=>"R", 'param'=>'order_payable_gstcomm', 'number' => true),
                "Profit/ Loss" => array('row'=>"S", 'param'=>'order_payable_nettcomm', 'number' => true),
                "Invoice number to insurance company" => array('row'=>"T", 'param'=>'ins_inv'),
                "Ins document number" => array('row'=>"U", 'param'=>'order_ins_doc_no'),
                "Direct Discount" => array('row'=>"V", 'param'=>'order_direct_discount_amt'),
            );
        }
        else{
            $header = array(
                "No" => array('row'=>"A"),
                "Issued Date" =>array('row'=>"B", 'param'=>'order_date'),
                "Debit Number" => array('row'=>"C", 'param'=>'order_no'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Policy Number" => array('row'=>"E", 'param'=>'order_policyno'),
                "Premium Charges" => array('row'=>"F", 'param'=>'order_grosspremium_amount', 'number' => true),
                "Receipt Number" => array('row'=>"G", 'param'=>'receipt_data'),
                "Name Of TSA" => array('row'=>"H", 'param'=>'dealer_name'),
                "TSA Comm" => array('row'=>"I", 'param'=>'total_tsa_comm'),
                "Comm Cheque" => array('row'=>"J", 'param'=>'tsa_comm_cheque'),
                "Insurance company" => array('row'=>"K", 'param'=>'insuranceco_name'),
                "Cheque paid to Insurance company" => array('row'=>"L", 'param'=>'to_ins_cheque'),
                "Premium charge to TSA" => array('row'=>"M", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Nett premium" => array('row'=>"N", 'param'=>'order_subtotal_amount', 'number' => true),
                "Premium GST" => array('row'=>"O", 'param'=>'order_gst_amount', 'number' => true),
                "Commission to Ensure/ Peoples" => array('row'=>"P", 'param'=>'order_payable_ourcomm', 'number' => true),
                "GST on commission" => array('row'=>"Q", 'param'=>'order_payable_gstcomm', 'number' => true),
                "Profit/ Loss" => array('row'=>"R", 'param'=>'order_payable_nettcomm', 'number' => true),
                "Invoice number to insurance company" => array('row'=>"S", 'param'=>'ins_inv'),
                "Ins document number" => array('row'=>"T", 'param'=>'order_ins_doc_no'),
                "Direct Discount" => array('row'=>"U", 'param'=>'order_direct_discount_amt'),
            );
        }

        $sheet->setCellValue('A1', 'Debit Note Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );

        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }
        
        $grand_total = array(
            'order_grosspremium_amount' => 0,
            'total_tsa_comm' => 0,
            'order_receivable_premiumreceivable' => 0,
            'order_subtotal_amount' => 0,
            'order_gst_amount' => 0,
            'order_payable_ourcomm' => 0,
            'order_payable_nettcomm' => 0,
            'order_direct_discount_amt' => 0,
        );
        $count_no = 0;
        $row_count = 3;
        if($this->report_group){
            foreach($this->data_listing as $key => $list_item){
                $sub_total = array(
                    'order_grosspremium_amount' => 0,
                    'total_tsa_comm' => 0,
                    'order_receivable_premiumreceivable' => 0,
                    'order_subtotal_amount' => 0,
                    'order_gst_amount' => 0,
                    'order_payable_ourcomm' => 0,
                    'order_payable_nettcomm' => 0,
                    'order_direct_discount_amt' => 0,
                );

                if($this->report_group == 'dealer'){
                    $dealer_data = $this->getDealerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }
                elseif($this->report_group == 'customer'){
                    $partner_data = $this->getPartnerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "Customer :".$partner_data['partner_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }

                foreach($list_item as $row){
                    foreach($sub_total as $sub_ky => $sub_li){
                        $sub_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;;
                        $grand_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;;
                    }
    
                    foreach($header as $ky => $li){
                        if(isset($li['param'])){
                            if($li['param'] == 'receipt_data'){
                                $rec_input = array();
                                foreach($row['receipt_data'] as $rec_ky => $rec_li){
                                    $rec_input[] = $rec_li['receipt_no'];
                                }
                                $recstr = implode(',',$rec_input);
                                $sheet->setCellValue($li['row'].$row_count, $recstr);
                            }
                            elseif($li['param'] == 'order_payable_nettcomm'){
                                $sheet->setCellValue($li['row'].$row_count, $row[$li['param']]);
                                if($row[$li['param']] >=0){
                                    $this->setFontColor($objPHPExcel,$li['row'].$row_count,'056608');
                                }
                                else{
                                    $this->setFontColor($objPHPExcel,$li['row'].$row_count,'ff0000');
                                }
                            }
                            else{
                                $sheet->setCellValue($li['row'].$row_count, html_entity_decode($row[$li['param']]));
                            }
                        }else{
                            $sheet->setCellValue($li['row'].$row_count, $count_no);
                        }
                        if(isset($li['number'])){
                            $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                        }
    
                    }
                    $count_no++;
                    $row_count ++;
                }
    
                $row_count ++;
                $sheet->setCellValue('A'.$row_count, 'Sub Total');
                foreach($header as $ky => $li){
                    if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                        $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                }
                
            }
        }
        else{
            $sub_total = array(
                'order_grosspremium_amount' => 0,
                'total_tsa_comm' => 0,
                'order_receivable_premiumreceivable' => 0,
                'order_subtotal_amount' => 0,
                'order_gst_amount' => 0,
                'order_payable_ourcomm' => 0,
                'order_payable_nettcomm' => 0,
                'order_direct_discount_amt' => 0,
            );

            foreach($this->data_listing as $key => $row){
                foreach($sub_total as $sub_ky => $sub_li){
                    $sub_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;;
                    $grand_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;;
                }

                foreach($header as $ky => $li){
                    if(isset($li['param'])){
                        if($li['param'] == 'receipt_data'){
                            $rec_input = array();
                            foreach($row['receipt_data'] as $rec_ky => $rec_li){
                                $rec_input[] = $rec_li['receipt_no'];
                            }
                            $recstr = implode(',',$rec_input);
                            $sheet->setCellValue($li['row'].$row_count, $recstr);
                        }
                        elseif($li['param'] == 'order_payable_nettcomm'){
                            $sheet->setCellValue($li['row'].$row_count, $row[$li['param']]);
                            if($row[$li['param']] >=0){
                                $this->setFontColor($objPHPExcel,$li['row'].$row_count,'056608');
                            }
                            else{
                                $this->setFontColor($objPHPExcel,$li['row'].$row_count,'ff0000');
                            }
                        }
                        else{
                            $sheet->setCellValue($li['row'].$row_count, html_entity_decode($row[$li['param']]));
                        }
                    }else{
                        $sheet->setCellValue($li['row'].$row_count, $count_no);
                    }
                    if(isset($li['number'])){
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }

                }
                $count_no++;
                $row_count ++;
            }

            $row_count ++;
            $sheet->setCellValue('A'.$row_count, 'Sub Total');
            foreach($header as $ky => $li){
                if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
        }
        

        $row_count ++;
        $sheet->setCellValue('A'.$row_count, 'Total');
        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $grand_total)){
                $sheet->setCellValue($li['row'].$row_count, $grand_total[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'DebitNotes_Report-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function CreditNoteExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        if(in_array($this->insurance_type,array("MI","PMI","CMI", "PCMI"))){
            $header = array(
                "No" => array('row'=>"A"),
                "Issued Date" =>array('row'=>"B", 'param'=>'order_date'),
                "Debit Number" => array('row'=>"C", 'param'=>'order_no'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Vehicles Number" => array('row'=>"E", 'param'=>'order_vehicles_no'),
                "Policy Number" => array('row'=>"F", 'param'=>'order_policyno'),
                "Premium Charges" => array('row'=>"G", 'param'=>'order_grosspremium_amount', 'number' => true),
                "Receipt Number" => array('row'=>"H", 'param'=>'receipt_data'),
                "Name Of TSA" => array('row'=>"I", 'param'=>'dealer_name'),
                "TSA Comm" => array('row'=>"J", 'param'=>'total_tsa_comm'),
                "Comm Cheque" => array('row'=>"K", 'param'=>'tsa_comm_cheque'),
                "Insurance company" => array('row'=>"L", 'param'=>'insuranceco_name'),
                "Cheque paid to Insurance company" => array('row'=>"M", 'param'=>'to_ins_cheque'),
                "Premium charge to TSA" => array('row'=>"N", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Nett premium" => array('row'=>"O", 'param'=>'order_subtotal_amount', 'number' => true),
                "Premium GST" => array('row'=>"P", 'param'=>'order_gst_amount', 'number' => true),
                "Commission to Ensure/ Peoples" => array('row'=>"Q", 'param'=>'order_payable_ourcomm', 'number' => true),
                "GST on commission" => array('row'=>"R", 'param'=>'order_payable_gstcomm', 'number' => true),
                "Profit/ Loss" => array('row'=>"S", 'param'=>'order_payable_nettcomm', 'number' => true),
                "Invoice number to insurance company" => array('row'=>"T", 'param'=>'ins_inv'),
                "Ins document number" => array('row'=>"U", 'param'=>'order_ins_doc_no'),
                "Direct Discount" => array('row'=>"V", 'param'=>'order_direct_discount_amt'),
            );
        }
        else{
            $header = array(
                "No" => array('row'=>"A"),
                "Issued Date" =>array('row'=>"B", 'param'=>'order_date'),
                "Debit Number" => array('row'=>"C", 'param'=>'order_no'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Policy Number" => array('row'=>"E", 'param'=>'order_policyno'),
                "Premium Charges" => array('row'=>"F", 'param'=>'order_grosspremium_amount', 'number' => true),
                "Receipt Number" => array('row'=>"G", 'param'=>'receipt_data'),
                "Name Of TSA" => array('row'=>"H", 'param'=>'dealer_name'),
                "TSA Comm" => array('row'=>"I", 'param'=>'total_tsa_comm'),
                "Comm Cheque" => array('row'=>"J", 'param'=>'tsa_comm_cheque'),
                "Insurance company" => array('row'=>"K", 'param'=>'insuranceco_name'),
                "Cheque paid to Insurance company" => array('row'=>"L", 'param'=>'to_ins_cheque'),
                "Premium charge to TSA" => array('row'=>"M", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Nett premium" => array('row'=>"N", 'param'=>'order_subtotal_amount', 'number' => true),
                "Premium GST" => array('row'=>"O", 'param'=>'order_gst_amount', 'number' => true),
                "Commission to Ensure/ Peoples" => array('row'=>"P", 'param'=>'order_payable_ourcomm', 'number' => true),
                "GST on commission" => array('row'=>"Q", 'param'=>'order_payable_gstcomm', 'number' => true),
                "Profit/ Loss" => array('row'=>"R", 'param'=>'order_payable_nettcomm', 'number' => true),
                "Invoice number to insurance company" => array('row'=>"S", 'param'=>'ins_inv'),
                "Ins document number" => array('row'=>"T", 'param'=>'order_ins_doc_no'),
                "Direct Discount" => array('row'=>"U", 'param'=>'order_direct_discount_amt'),
            );
        }

        $sheet->setCellValue('A1', 'Credit Note Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );

        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }
        
        $grand_total = array(
            'order_grosspremium_amount' => 0,
            'total_tsa_comm' => 0,
            'order_receivable_premiumreceivable' => 0,
            'order_subtotal_amount' => 0,
            'order_gst_amount' => 0,
            'order_payable_ourcomm' => 0,
            'order_payable_nettcomm' => 0,
            'order_direct_discount_amt' => 0,
        );
        $count_no = 0;
        $row_count = 3;
        if($this->report_group){
            foreach($this->data_listing as $key => $list_item){
                $sub_total = array(
                    'order_grosspremium_amount' => 0,
                    'total_tsa_comm' => 0,
                    'order_receivable_premiumreceivable' => 0,
                    'order_subtotal_amount' => 0,
                    'order_gst_amount' => 0,
                    'order_payable_ourcomm' => 0,
                    'order_payable_nettcomm' => 0,
                    'order_direct_discount_amt' => 0,
                );

                if($this->report_group == 'dealer'){
                    $dealer_data = $this->getDealerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }
                elseif($this->report_group == 'customer'){
                    $partner_data = $this->getPartnerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "Customer :".$partner_data['partner_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }

                foreach($list_item as $row){
                    foreach($sub_total as $sub_ky => $sub_li){
                        $sub_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                        $grand_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                    }
    
                    foreach($header as $ky => $li){
                        if(isset($li['param'])){
                            if($li['param'] == 'receipt_data'){
                                $rec_input = array();
                                foreach($row['receipt_data'] as $rec_ky => $rec_li){
                                    $rec_input[] = $rec_li['receipt_no'];
                                }
                                $recstr = implode(',',$rec_input);
                                $sheet->setCellValue($li['row'].$row_count, $recstr);
                            }
                            elseif($li['param'] == 'order_payable_nettcomm'){
                                $sheet->setCellValue($li['row'].$row_count, $row[$li['param']]);
                                if($row[$li['param']] >=0){
                                    $this->setFontColor($objPHPExcel,$li['row'].$row_count,'056608');
                                }
                                else{
                                    $this->setFontColor($objPHPExcel,$li['row'].$row_count,'ff0000');
                                }
                            }
                            else{
                                $sheet->setCellValue($li['row'].$row_count, html_entity_decode($row[$li['param']]));
                            }
                        }else{
                            $sheet->setCellValue($li['row'].$row_count, $count_no);
                        }
                        if(isset($li['number'])){
                            $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                        }
    
                    }
                    $count_no++;
                    $row_count ++;
                }
    
                $row_count ++;
                $sheet->setCellValue('A'.$row_count, 'Sub Total');
                foreach($header as $ky => $li){
                    if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                        $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                }
                
            }
        }
        else{
            $sub_total = array(
                'order_grosspremium_amount' => 0,
                'total_tsa_comm' => 0,
                'order_receivable_premiumreceivable' => 0,
                'order_subtotal_amount' => 0,
                'order_gst_amount' => 0,
                'order_payable_ourcomm' => 0,
                'order_payable_nettcomm' => 0,
                'order_direct_discount_amt' => 0,
            );

            foreach($this->data_listing as $key => $row){
                foreach($sub_total as $sub_ky => $sub_li){
                    $sub_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                    $grand_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                }

                foreach($header as $ky => $li){
                    if(isset($li['param'])){
                        if($li['param'] == 'receipt_data'){
                            $rec_input = array();
                            foreach($row['receipt_data'] as $rec_ky => $rec_li){
                                $rec_input[] = $rec_li['receipt_no'];
                            }
                            $recstr = implode(',',$rec_input);
                            $sheet->setCellValue($li['row'].$row_count, $recstr);
                        }
                        elseif($li['param'] == 'order_payable_nettcomm'){
                            $sheet->setCellValue($li['row'].$row_count, $row[$li['param']]);
                            if($row[$li['param']] >=0){
                                $this->setFontColor($objPHPExcel,$li['row'].$row_count,'056608');
                            }
                            else{
                                $this->setFontColor($objPHPExcel,$li['row'].$row_count,'ff0000');
                            }
                        }
                        else{
                            $sheet->setCellValue($li['row'].$row_count, html_entity_decode($row[$li['param']]));
                        }
                    }else{
                        $sheet->setCellValue($li['row'].$row_count, $count_no);
                    }
                    if(isset($li['number'])){
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }

                }
                $count_no++;
                $row_count ++;
            }

            $row_count ++;
            $sheet->setCellValue('A'.$row_count, 'Sub Total');
            foreach($header as $ky => $li){
                if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
        }
        

        $row_count ++;
        $sheet->setCellValue('A'.$row_count, 'Total');
        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $grand_total)){
                $sheet->setCellValue($li['row'].$row_count, $grand_total[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'CreditNotes_Report-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function PremiumStateExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        if(in_array($this->insurance_type,array("MI","PMI","CMI","PCMI"))){
            $header = array(
                "Date" => array('row'=>"A", 'param'=>'order_date'),
                "Ins Type" => array('row'=>"B", 'param'=>'order_prefix_type'),
                "Policy Number" => array('row'=>"C", 'param'=>'order_policyno'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Veh. No CoverType" => array('row'=>"E", 'param'=>'order_vehicles_no'),
                "Sum Insured" =>array('row'=>"F", 'param'=>'order_suminsured_amt'),
                "Gr. Premium" => array('row'=>"G", 'param'=>'order_gross_premium', 'number' => true),
                "NCD %" => array('row'=> "H", 'param'=>'order_ncd_dis_per', 'number' => true),
                "Our Comm %" => array('row'=>"I", 'param'=>'order_payable_ourcommpercent', 'number' => true),
                "GST" => array('row'=>"J", 'param'=>'order_receivable_gstamount', 'number' => true),
                "Direct Disc" => array('row'=>"K", 'param'=>'order_direct_discount_amt', 'number' => true),
                "Premium Receivable" => array('row'=>"L", 'param'=>'order_receivable_premiumreceivable','number' => true),
                "Document No." => array('row'=>"M", 'param'=>'order_no'),
                "TSA" => array('row'=>"N", 'param'=>'dealer_name'),
                "Customer" => array('row'=>"O", 'param'=>'partner_name'),
            );
        }
        else{
            $header = array(
                "Date" => array('row'=>"A", 'param'=>'order_date'),
                "Ins Type" => array('row'=>"B", 'param'=>'order_prefix_type'),
                "Policy Number" => array('row'=>"C", 'param'=>'order_policyno'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Veh. No CoverType" => array('row'=>"E", 'param'=>'order_coverage'),
                "Gr. Premium" => array('row'=>"F", 'param'=>'order_gross_premium', 'number' => true),
                "Our Comm %" => array('row'=>"G", 'param'=>'order_payable_ourcommpercent', 'number' => true),
                "GST" => array('row'=>"H", 'param'=>'order_receivable_gstamount', 'number' => true),
                "Direct Disc" => array('row'=>"I", 'param'=>'order_direct_discount_amt','number' => true),
                "Premium Receivable" => array('row'=>"J", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
                "Document No." => array('row'=>"M", 'param'=>'order_no'),
                "TSA" => array('row'=>"N", 'param'=>'dealer_name'),
                "Customer" => array('row'=>"O", 'param'=>'partner_name'),
            );
        }

        $sheet->setCellValue('A1', 'Premiun Statement Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );

        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }
        
        $grand_total = array(
            'order_gross_premium' => 0,
            'order_receivable_gstamount' =>0,
            'order_direct_discount_amt' =>0,
            'order_receivable_premiumreceivable' =>0,
        );
        $count_no = 0;
        $row_count = 3;
        if($this->report_group){
            foreach($this->data_listing as $key => $list_item){
                $sub_total = array(
                    'order_gross_premium' => 0,
                    'order_receivable_gstamount' =>0,
                    'order_direct_discount_amt' =>0,
                    'order_receivable_premiumreceivable' =>0,
                );

                if($this->report_group == 'dealer'){
                    $dealer_data = $this->getDealerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }
                elseif($this->report_group == 'customer'){
                    $partner_data = $this->getPartnerInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "Customer :".$partner_data['partner_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }
                elseif($this->report_group == 'insuranceco'){
                    $ins_data = $this->getInsuranceComapnyInfoByID($key);
                    $sheet->setCellValue('A'.$row_count, "Insurance Comapny :".$ins_data['insuranceco_name']);
                    $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
                    $row_count++;
                }

                foreach($list_item as $row){
                    foreach($sub_total as $sub_ky => $sub_li){
                        $sub_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                        $grand_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                    }
    
                    foreach($header as $ky => $li){
                        if(isset($li['param'])){
                            $sheet->setCellValue($li['row'].$row_count, html_entity_decode($row[$li['param']]));
                        }else{
                            $sheet->setCellValue($li['row'].$row_count, $count_no);
                        }

                        if(isset($li['number'])){
                            $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                        }
    
                    }
                    $count_no++;
                    $row_count ++;
                }
    
                $row_count ++;
                $sheet->setCellValue('A'.$row_count, 'Sub Total');
                foreach($header as $ky => $li){
                    if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                        $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                }
                
            }
        }
        else{
            $sub_total = array(
                'order_gross_premium' => 0,
                'order_receivable_gstamount' =>0,
                'order_direct_discount_amt' =>0,
                'order_receivable_premiumreceivable' =>0,
            );

            foreach($this->data_listing as $key => $row){
                foreach($sub_total as $sub_ky => $sub_li){
                    $sub_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                    $grand_total[$sub_ky] += isset($row[$sub_ky])?$row[$sub_ky]:0;
                }

                foreach($header as $ky => $li){
                    if(isset($li['param'])){
                        $sheet->setCellValue($li['row'].$row_count, html_entity_decode($row[$li['param']]));
                    }else{
                        $sheet->setCellValue($li['row'].$row_count, $count_no);
                    }

                    if(isset($li['number'])){
                        $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }

                }
                $count_no++;
                $row_count ++;
            }

            $row_count ++;
            $sheet->setCellValue('A'.$row_count, 'Sub Total');
            foreach($header as $ky => $li){
                if(isset($li['param']) && array_key_exists($li['param'], $sub_total)){
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
        }
        

        $row_count ++;
        $sheet->setCellValue('A'.$row_count, 'Total');
        foreach($header as $ky => $li){
            if(isset($li['param']) && array_key_exists($li['param'], $grand_total)){
                $sheet->setCellValue($li['row'].$row_count, $grand_total[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'Premium_statement-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function DailyTransExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        if($this->insurance_type == 'receipt'){
            $header = array(
                "No" => array('row'=>"A"),
                "Document No" => array('row'=>"B", 'param'=>'order_no'),
                "Receipt Date" => array('row'=>"C", 'param'=>'receipt_date'),
                "Receipt No" => array('row'=>"D", 'param'=>'receipt_no'),
                "Received Method" => array('row'=>"E", 'param'=>'receipt_method'),
                "Receive From" => array('row'=>"F", 'param'=>'receive_from'),
                "Receive Amount" => array('row'=>"G", 'param'=>'recpline_offset_amount', 'number' => true),
                "Receive Remark" => array('row'=>"H", 'param'=>'recpline_offset_amount'),
            );
        }
        else{
            $header = array(
                "No" => array('row'=>"A"),
                "Document No" => array('row'=>"B", 'param'=>'order_no'),
                "Payment Date" => array('row'=>"C", 'param'=>'payment_date'),
                "Payment No" => array('row'=>"D", 'param'=>'payment_no'),
                "Payment Purpose" => array('row'=>"E", 'param'=>'paymentline_purpose_id'),
                "Payment Method" => array('row'=>"F", 'param'=>'payment_method'),
                "Payment To" => array('row'=>"G", 'param'=>'payment_to'),
                "Payment Amount" => array('row'=>"H", 'param'=>'paid_amt', 'number' => true),
                "Payment Remark" => array('row'=>"I", 'param'=>'pay_remarks'),
            );
        }

        $sheet->setCellValue('A1', 'Daily Transaction Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );

        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }

        $grand_total = array(
            'paid_amt' => 0,
            'recpline_offset_amount' => 0,
        );
        $row_count = 3;

        foreach($this->data_listing as $dis => $list_data){
            $sub_total = array(
                'paid_amt' => 0,
                'recpline_offset_amount' => 0,
            );
            foreach($sub_total as $sub_key => $sub_li){
                $sub_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
                $grand_total[$sub_key] += isset($list_data[$sub_key])?$list_data[$sub_key]:0;
            }

            foreach($header as $key => $li){
                if(isset($li['param'])){
                    $sheet->setCellValue($li['row'].$row_count, $list_data[$li['param']]);
                }
                else{
                    $sheet->setCellValue($li['row'].$row_count, $dis + 1);
                }

                if(isset($li['number'])){
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
            $row_count++;
        }
        $row_count++;
        $sheet->setCellValue("A".$row_count, "Sub Total");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        foreach($header as $key => $li){
            if(isset($sub_total[$li['param']])){
                $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
            }
        }
        $row_count++;
        $row_count++;
        $sheet->setCellValue("A".$row_count, "Total");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        foreach($header as $key => $li){
            if(isset($grand_total[$li['param']])){
                $sheet->setCellValue($li['row'].$row_count, $grand_total[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
            }
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'Daily_Transaction-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function accstateExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        if($this->insurance_type == 'GI'){
            $header = array(
                "No" => array('row'=>"A"),
                "Date" => array('row'=>"B", 'param'=>'order_date'),
                "DN No" => array('row'=>"C", 'param'=>'order_no'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Coverage Plan" =>array('row'=>"E", 'param'=>'order_coverage'),
                "Premium" => array('row'=>"F", 'param'=>'order_gross_premium', 'number' => true),
                "Direct Disc" => array('row'=> "G", 'param'=>'order_direct_discount_amt', 'number' => true),
                "TSA Ref Fee" => array('row'=>"H", 'param'=>'order_receivable_dealercomm', 'number' => true),
                "Ref Fee GST" => array('row'=>"I", 'param'=>'order_receivable_dealergstamount', 'number' => true),
                "Due Pay" => array('row'=>"J", 'param'=>'order_tsa_pay', 'number' => true),
                "Type" => array('row'=>"K", 'param'=>'order_display_type'),
            );
        }
        else{
            $header = array(
                "No" => array('row'=>"A"),
                "Date" => array('row'=>"B", 'param'=>'order_date'),
                "DN No" => array('row'=>"C", 'param'=>'order_no'),
                "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
                "Vehicle No" =>array('row'=>"E", 'param'=>'order_vehicles_no'),
                "Premium" => array('row'=>"F", 'param'=>'order_gross_premium', 'number' => true),
                "Direct Disc" => array('row'=> "G", 'param'=>'order_direct_discount_amt', 'number' => true),
                "TSA Ref Fee" => array('row'=>"H", 'param'=>'order_receivable_dealercomm', 'number' => true),
                "Ref Fee GST" => array('row'=>"I", 'param'=>'order_receivable_dealergstamount', 'number' => true),
                "Due Pay" => array('row'=>"J", 'param'=>'order_tsa_pay', 'number' => true),
                "Type" => array('row'=>"K", 'param'=>'order_display_type'),
            );
        }

        if($this->order_customer > 0){
            unset($header['TSA Ref Fee'], $header['Ref Fee GST']);
            $header['Due Pay']['param'] = "order_cus_pay"; 
            $header['Due Pay']['row'] = "H"; 
            $header['Type']['row'] = "I"; 
        }
        else{
            unset($header['Direct Disc']);
            $header['TSA Ref Fee']['row'] = "G"; 
            $header['Ref Fee GST']['row'] = "H"; 
            $header['Due Pay']['row'] = "I"; 
            $header['Type']['row'] = "J"; 
        }

        //debit note listing
        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }

        $grand_total = array(
            'order_gross_premium' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'order_cus_pay' => 0,
            'order_tsa_pay' => 0,
        );
    
        $sheet->setCellValue('A1', 'Statement Account Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );
    
        $row_count = 3;
        
        if($this->order_dealer > 0){
            $dealer_data = $this->getDealerInfoByID($this->order_dealer );
            $sheet->setCellValue('A'.$row_count, "TSA :".$dealer_data['dealer_name']);
            $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
            $row_count++;
        }
        elseif($this->order_customer > 0){
            $partner_data = $this->getPartnerInfoByID($this->order_customer);
            $sheet->setCellValue('A'.$row_count, "Customer :".$partner_data['partner_name']);
            $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
            $row_count++;
        }

        $sub_total = array(
            'order_gross_premium' => 0,
            'order_direct_discount_amt' => 0,
            'order_receivable_dealercomm' => 0,
            'order_receivable_dealergstamount' => 0,
            'order_cus_pay' => 0,
            'order_tsa_pay' => 0,
        );
    
        $count_no = 1;
        $receipt_total  = 0;

        foreach($this->data_listing['DN'] as $li_ky => $dn_row){
            foreach($sub_total as $sub_key => $sub_li){
                $sub_total[$sub_key] = $sub_li + $dn_row[$sub_key];
                $grand_total[$sub_key] = $grand_total[$sub_key] + $dn_row[$sub_key];   
            }

            foreach($header as $key => $li){
                if(isset($li['param'])){
                    $sheet->setCellValue($li['row'].$row_count, removeAmpcharacter($dn_row[$li['param']]));
                }
                else{
                    $sheet->setCellValue($li['row'].$row_count, $count_no);
                }
    
                if(isset($li['number'])){
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);   
                }
            }

            $count_no++;
            $row_count++;
            $receipt_sub_total =0;
            $receipt_data = $this->getReceiptData($dn_row['order_id']);

            foreach($receipt_data as $rec_ky => $rec_li){
                $rec_string = "Receipt:".date("d-m-Y",strtotime($rec_li['receipt_date'])). " ". $rec_li['receipt_cheque'];
                $sheet->setCellValue("B".$row_count, $rec_string);
                $receipt_sub_total += $rec_li['paid_total'];
                $receipt_total += $rec_li['paid_total'];
                foreach($header as $key => $li){
                    if(isset($li['param']) && ($li['param'] == 'order_gross_premium' || $li['param'] == 'order_tsa_pay' || $li['param'] == 'order_cus_pay')){
                        $sheet->setCellValue($li['row'].$row_count, $rec_li['paid_total']);
                    }
                }
                $row_count++;
            }

            
        }

        $sub_total["order_gross_premium"] -= $receipt_total; 
        $sub_total["order_cus_pay"] -= $receipt_total; 
        $sub_total["order_tsa_pay"] -= $receipt_total; 

        $row_count++;
        $sheet->setCellValue("A".$row_count, "Sub Total");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        
        foreach($header as $key => $li){
            if(isset($sub_total[$li['param']]) && $sub_total[$li['param']]){
                if(isset($li['param']) && ($li['param'] == 'order_gross_premium' || $li['param'] == 'order_tsa_pay' || $li['param'] == 'order_cus_pay')){
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                }
                else{
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                }
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
            }
        }

        $row_count++;
        $sheet->setCellValue("A".$row_count, "Craw Back Fees");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        $row_count++;

        //Credit Notes
        foreach($this->data_listing['CN'] as $li_ky => $cn_row){
            foreach($sub_total as $sub_key => $sub_li){
                if($sub_key == 'order_tsa_pay' && $cn_row['order_record_billto'] == "To Customer"){
                    $sub_total[$sub_key] += abs($cn_row[$sub_key]);
                    $grand_total[$sub_key] += abs($cn_row[$sub_key]);
                }else{
                    $sub_total[$sub_key] -= abs($cn_row[$sub_key]);
                    $grand_total[$sub_key] -= abs($cn_row[$sub_key]);       
                }
            }

            foreach($header as $key => $li){
                if(isset($li['param'])){
                     if($li['param'] == 'order_tsa_pay' && $cn_row['order_record_billto'] == "To Customer"){
                         $sheet->setCellValue($li['row'].$row_count,abs($cn_row[$li['param']]));
                     }else{
                         $sheet->setCellValue($li['row'].$row_count,$cn_row[$li['param']]);
                     }
                    
                }else{
                    $sheet->setCellValue($li['row'].$row_count, $count_no);
                }
    
                if(iisset($li['number'])){
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
    
            $row_count++;
            $count_no++;
        }

        $row_count++;
        $sheet->setCellValue("A".$row_count, "Sub Total");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        
        foreach($header as $key => $li){
            if(isset($sub_total[$li['param']])){
                if($li['param'] == 'order_gross_premium' || $li['param'] == 'order_tsa_pay' || $li['param'] == 'order_cus_pay'){
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                }
                else{
                    $sheet->setCellValue($li['row'].$row_count, $sub_total[$li['param']]);
                }
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
            }
        }
        $row_count++;

        $grand_total["order_gross_premium"] = $grand_total["order_gross_premium"] - $receipt_total; 
        $grand_total["order_cus_pay"] = $grand_total["order_cus_pay"] - $receipt_total; 
        $grand_total["order_tsa_pay"] = $grand_total["order_tsa_pay"] - $receipt_total;  

        $sheet->setCellValue("A".$row_count, "Grand Total");
        $sheet->getStyle('A'.$row_count)->getFont()->setBold( true );
        
        foreach($header as $key => $li){
            if($grand_total[$li['param']]){
            if($li['param'] == 'order_gross_premium' || $li['param'] == 'order_tsa_pay' || $li['param'] == 'order_cus_pay'){
                    $sheet->setCellValue($li['row'].$row_count, ($grand_total[$li['param']]));
                }
                else{
                    $sheet->setCellValue($li['row'].$row_count, $grand_total[$li['param']]);
                }
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold( true );
            }
        }

        // final output
        $writer = new Xlsx($spreadsheet);
        $filename = 'Account_statement-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function MaidaccstateExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dn_header = array(
            'No' => array('row' => "A"),
            'Acceptance Date' => array('row' => "B", "param" => 'order_date'),
            'Commence Date' => array('row' => "C" , "param" => 'order_period_from'),
            'Policy No' => array('row' => "D" , "param" => 'order_policyno'),
            'Name of applicant / Insured Name' => array('row' => "E" , "param" => 'partner_name'),
            'Maid Name / Insured Person Name' => array('row' => "F" , "param" => 'order_maid_name'),
            'Plan' => array('row' => "G" , "param" => 'order_coverage'),
            'Premium After GST' => array('row' => "H" , "param" => 'order_gross_premium', "number" => true),
            'TSA Referral Fee' => array('row' => "I" , "param" => 'order_receivable_dealercomm', "number" => true),
            'Due Payment' => array('row' => "J", "param" => 'order_tsa_pay', "number" => true),
        );
    
        $grand_total = array(
            'order_gross_premium' => 0,
            'paid_total' => 0,
            'order_receivable_dealercomm' => 0,
            'total_refund' => 0,
            'comm_cawl_back' => 0,
            'order_tsa_pay' => 0,
            'order_cus_pay' => 0,
            'tsa_refund' => 0,
            'cus_refund' => 0,
        );
    
        $dn_subtotal = array(
            'order_gross_premium' => 0,
            'order_receivable_dealercomm' => 0,
            'order_tsa_pay' => 0,
            'order_cus_pay' => 0,
        );
    
        $addtional = 0;
        if($this->order_customer > 0){
            $dn_header['Due Payment']['param'] = 'order_cus_pay';
            $dn_header['Due Payment']['row'] = 'I';
            unset($dn_header['TSA Referral Fee']);
            unset($dn_subtotal['order_tsa_pay']);
            unset($dn_subtotal['order_receivable_dealercomm']);
        }

        $sheet->setCellValue('A1', 'Maid Statement Account Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        $sheet->getStyle('A1')->getFont()->setBold( true );
        
        if($this->order_dealer > 0){
            $dealer_data = $this->getDealerInfoByID($this->order_dealer );
            $sheet->setCellValue('A2', "TSA :".$dealer_data['dealer_name']);
            $sheet->getStyle('A2')->getFont()->setBold( true );
            $row_count++;
        }
        elseif($this->order_customer > 0){
            $partner_data = $this->getPartnerInfoByID($this->order_customer);
            $sheet->setCellValue('A2', "Customer :".$partner_data['partner_name']);
            $sheet->getStyle('A2')->getFont()->setBold( true );
            $row_count++;
        }

        //debit note listing
        foreach($dn_header as $key => $li){
            $sheet->setCellValue($li['row'].'3', $key);
            $sheet->getStyle($li['row'].'3')->getFont()->setBold(true);
        }
        $row_count = 5;
        $count_no = 1;

        foreach($this->data_listing['DN'] as $li_ky => $dn_row){
            foreach($dn_subtotal as $sub_key  => $sub_item){
                $dn_subtotal[$sub_key] = $dn_subtotal[$sub_key] + $dn_row[$sub_key];
                $grand_total[$sub_key] = $grand_total[$sub_key] + $dn_row[$sub_key];
            }
            
    
            foreach($dn_header as $hky => $hli){
                if(isset($hli['number'])){
                    $sheet->setCellValue($hli['row'].$row_count,$dn_row[$hli['param']]);
                    $sheet->getStyle($hli['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
                elseif(isset($hli['param'])){
                    $sheet->setCellValue($hli['row'].$row_count,$dn_row[$hli['param']]);
                }
                else{
                    $sheet->setCellValue($hli['row'].$row_count, $count_no);
                }
            }
            $row_count ++;
            $receipt_data = $this->getReceiptData($dn_row['order_id']);
    
            foreach($receipt_data as $rec_ky => $rec_li){
                $rec_string = "Receipt:".date("d-m-Y",strtotime($rec_li['receipt_date'])). " ". $rec_li['receipt_cheque'];
                $sheet->setCellValue("B".$row_count, $rec_string);
                $grand_total['paid_total'] += $rec_li['paid_total'];
                foreach($dn_header as $key => $li){
                    if(isset($hli['param']) && ($li['param'] == 'order_gross_premium' || $li['param'] == 'order_tsa_pay' || $li['param'] == 'order_cus_pay')){
                        $sheet->setCellValue($li['row'].$row_count, $rec_li['paid_total']);
                    }
                }
                $row_count++;
            }
            
            $count_no ++;
        }

        $row_count ++;
        $sheet->setCellValue("A".$row_count, 'Sub Total');
        $sheet->getStyle("A".$row_count)->getFont()->setBold(true);
        foreach($dn_header as $key => $li){
            if(isset($li['param']) && array_key_exists($li['param'],$dn_subtotal)){
                $sheet->setCellValue($li['row'].$row_count, $dn_subtotal[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold(true);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }

        // credit note listing
        if($this->order_dealer > 0){
            $row_count++;
            $sheet->setCellValue("A".$row_count, 'CANCELLATION : Refund to TSA');
            $sheet->getStyle("A".$row_count)->getFont()->setBold(true);

            $cn_subtotal = array(
                'total_refund' => 0,
                'comm_cawl_back' => 0,
                'tsa_refund' => 0,
            );
            $row_count++;
            foreach($cn_header as $hky => $hli){
               $sheet->setCellValue($hli['row'].$row_count, $hky);
               $sheet->getStyle($hli['row'].$row_count)->getFont()->setBold(true);
            }
            $count_no = 1;
            $row_count++;

            foreach($this->data_listing['CN']['TSA'] as $li_ky => $cn_row){
                foreach($cn_subtotal as $sub_key  => $sub_item){
                    $cn_subtotal[$sub_key] = $cn_subtotal[$sub_key] + $cn_row[$sub_key];
                    $grand_total[$sub_key] = $grand_total[$sub_key] + $cn_row[$sub_key];
                }
                foreach($cn_header as $hky => $hli){
                    if(isset($hli['number'])){
                       $sheet->setCellValue($hli['row'].$row_count,$cn_row[$hli['param']]);
                       $sheet->getStyle($hli['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    }
                    elseif(isset($hli['param'])){
                       $sheet->setCellValue($hli['row'].$row_count,$cn_row[$hli['param']]);
                    }
                    else{
                       $sheet->setCellValue($hli['row'].$row_count, $count_no);
                    }
                }
                $row_count++;
                $count_no++;
            }

            $row_count ++;
            $sheet->setCellValue("A".$row_count, 'Sub Total');
            $sheet->getStyle("A".$row_count)->getFont()->setBold(true);
            foreach($cn_header as $key => $li){
                if(isset($li['param']) && array_key_exists($li['param'],$cn_subtotal)){
                   $sheet->setCellValue($li['row'].$row_count, $cn_subtotal[$li['param']]);
                   $sheet->getStyle($li['row'].$row_count)->getFont()->setBold(true);
                   $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }

            // crawl back commission
            $th_header = array(
                'No' => array('row' => 'A'),
                'Discharge Date' => array('row' => 'B', "param" => 'order_discharge_date'),
                'Commence Date' => array('row' => 'C' , "param" => 'order_cancel_date'),
                'Policy No' => array('row' => 'D' , "param" => 'order_policyno'),
                'Name of applicant / Insured Name' => array('row' => 'E' , "param" => 'partner_name'),
                'Maid Name / Insured Person Name' => array('row' => 'F' , "param" => 'order_maid_name'),
                'Plan' => array('row' => 'G' , "param" => 'order_coverage'),
                'Referral Fee crawl back' => array('row' => 'H' , "param" => 'comm_cawl_back', "number" => true),
            );
    
            $row_count++;
            $sheet->setCellValue("A".$row_count, 'CANCELLATION : REFERRAL FEE CRAWL BACK FROM TSA LISTING');
            $sheet->getStyle("A".$row_count)->getFont()->setBold(true);
    
            $th_subtotal = array(
                'comm_cawl_back' => 0,
            );
    
            $row_count++;
            $count_no = 1;

            foreach($this->data_listing['CN']['CUS'] as $li_ky => $th_row){
                if(isset($th_row['order_cancel_rate']) && $th_row['order_cancel_rate'] == 100){
                    foreach($th_subtotal as $sub_key  => $sub_item){
                        $th_subtotal[$sub_key] = $th_subtotal[$sub_key] + $th_row[$sub_key];
                        $grand_total[$sub_key] = $grand_total[$sub_key] + $th_row[$sub_key];
                    }
                    foreach($th_subtotal as $hky => $hli){
                        if(isset($hli['number'])){
                            $sheet->setCellValue($hli['row'].$row_count,$th_row[$hli['param']]);
                            $sheet->getStyle($hli['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                        }
                        elseif(isset($hli['param'])){
                            $sheet->setCellValue($hli['row'].$row_count,$th_row[$hli['param']]);
                        }
                        else{
                            $sheet->setCellValue($hli['row'].$row_count, $count_no);
                        }
                    }
                    $row_count++;
                    $count_no++;
                }
            }

            $row_count ++;
            $sheet->setCellValue("A".$row_count, 'Sub Total');
            $sheet->getStyle("A".$row_count)->getFont()->setBold(true);
            foreach($cn_header as $key => $li){
                if(isset($li['param']) && array_key_exists($li['param'],$cn_subtotal)){
                    $sheet->setCellValue($li['row'].$row_count, $cn_subtotal[$li['param']]);
                    $sheet->getStyle($li['row'].$row_count)->getFont()->setBold(true);
                    $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
        }
        $row_count++;
        //Customer Refund
        $fr_header = array(
            'No' => array('row' => 'A'),
            'Discharge Date' => array('row' => 'B', "param" => 'order_discharge_date'),
            'Commence Date' => array('row' => 'C' , "param" => 'order_cancel_date'),
            'Policy No' => array('row' => 'D' , "param" => 'order_policyno'),
            'Name of applicant / Insured Name' => array('row' => 'E' , "param" => 'partner_name'),
            'Maid Name / Insured Person Name' => array('row' => 'F' , "param" => 'order_maid_name'),
            'Cancelation Rate' => array('row' => 'G' , "param" => 'order_cancel_rate'),
            'Refund' => array('row' => 'H' , "param" => 'total_refund', "number" => true),
        );
        
        $fr_subtotal = array(
            'total_refund' => 0,
        );
        $count_no= 1;

        $sheet->setCellValue("A".$row_count, 'CANCELLATION : Refundable To Employer Directly Listing');
        $sheet->getStyle("A".$row_count)->getFont()->setBold(true);
        $row_count++;
        foreach($fr_header as $hky => $hli){
            $sheet->setCellValue($hli['row'].$row_count, $hky);
            $sheet->getStyle($hli['row'].$row_count)->getFont()->setBold(true);
        }
        $row_count++;

        foreach($this->data_listing['CN']['CUS'] as $fr_row){
            foreach($fr_subtotal as $sub_key  => $sub_item){
                $fr_subtotal[$sub_key] = $fr_subtotal[$sub_key] + abs($fr_row[$sub_key]) * -1;
                $grand_total[$sub_key] = $grand_total[$sub_key] + abs($fr_row[$sub_key]) * -1;
            }
    
            foreach($fr_header as $hky => $hli){
                if(isset($hli['number'])){
                    $sheet->setCellValue($hli['row'].$row_count,$fr_row[$hli['param']]);
                    $sheet->getStyle($hli['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
                elseif(isset($hli['param'])){
                    $sheet->setCellValue($hli['row'].$row_count,$fr_row[$hli['param']]);
                }
                else{
                    $sheet->setCellValue($hli['row'].$row_count, $count_no);
                }
            }
            $row_count++;
            $count_no++;
        }

        $row_count ++;
        $sheet->setCellValue("A".$row_count, 'Sub Total');
        $sheet->getStyle("A".$row_count)->getFont()->setBold(true);
        foreach($fr_header as $key => $li){
            if(isset($hli['param']) && array_key_exists($li['param'],$fr_subtotal)){
                $sheet->setCellValue($li['row'].$row_count, $fr_subtotal[$li['param']]);
                $sheet->getStyle($li['row'].$row_count)->getFont()->setBold(true);
                $sheet->getStyle($li['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }

        // final output
        $writer = new Xlsx($spreadsheet);
        $filename = 'Account_statement-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;
    }

    public function SalesReportExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $header = array(
            "No" => array('row'=>"A"),
            "Issued Date" =>array('row'=>"B", 'param'=>'order_date'),
            "Document Number" => array('row'=>"C", 'param'=>'order_no'),
            "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
            "Related Info" => array('row'=>"E", 'param'=>'related_info'),
            "Policy Number" => array('row'=>"F", 'param'=>'order_policyno'),
            "Premium Charges" => array('row'=>"G", 'param'=>'order_grosspremium_amount', 'number' => true),
            "Receipt Number" => array('row'=>"H", 'param'=>'receipt_data'),
            "Name Of TSA" => array('row'=>"I", 'param'=>'dealer_name'),
            "TSA Comm" => array('row'=>"J", 'param'=>'total_tsa_comm'),
            "Comm Cheque" => array('row'=>"K", 'param'=>'tsa_comm_cheque'),
            "Insurance company" => array('row'=>"L", 'param'=>'insurance_company'),
            "Cheque paid to Insurance company" => array('row'=>"M", 'param'=>'to_ins_cheque'),
            "Premium charge to TSA" => array('row'=>"N", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
            "Nett premium" => array('row'=>"O", 'param'=>'order_subtotal_amount', 'number' => true),
            "Premium GST" => array('row'=>"P", 'param'=>'order_gst_amount', 'number' => true),
            "Commission to Ensure/ Peoples" => array('row'=>"Q", 'param'=>'order_payable_ourcomm', 'number' => true),
            "GST on commission" => array('row'=>"R", 'param'=>'order_payable_gstcomm', 'number' => true),
            "Profit/ Loss" => array('row'=>"S", 'param'=>'order_payable_nettcomm', 'number' => true),
            "Invoice number to insurance company" => array('row'=>"T", 'param'=>'ins_inv'),
            "Ins document number" => array('row'=>"U", 'param'=>'order_ins_doc_no'),
            "Direct Discount" => array('row'=>"V", 'param'=>'order_direct_discount_amt'),
        );

        $sheet->setCellValue('A1', 'Company Sales Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }

        $grand_total = array(
            'order_grosspremium_amount' => 0,
            'order_receivable_premiumreceivable' => 0,
            'order_subtotal_amount' => 0,
            'order_gst_amount' => 0,
            'order_payable_ourcomm' => 0,
            'order_payable_gstcomm' => 0,
            'order_payable_nettcomm' => 0,
            'order_direct_discount_amt' => 0,
        );

        $row_count = 3;
        foreach($this->data_listing as $key => $list_data){
            $sheet->setCellValue("A".$row_count, isset($this->insurance_type_mapping[$key])?$this->insurance_type_mapping[$key]:'-');
            $sheet->getStyle("A".$row_count)->getFont()->setBold( true );
            $row_count ++;
            $count_no = 1;
            $sub_total = array(
                'order_grosspremium_amount' => 0,
                'order_receivable_premiumreceivable' => 0,
                'order_subtotal_amount' => 0,
                'order_gst_amount' => 0,
                'order_payable_ourcomm' => 0,
                'order_payable_gstcomm' => 0,
                'order_payable_nettcomm' => 0,
                'order_direct_discount_amt' => 0,
            );
            foreach($list_data as $list_item){
                foreach($header as $ky => $hli){
                    if($ky == 'No'){
                        $sheet->setCellValue($hli['row'].$row_count, $count_no);
                    }
                    elseif(isset($hli['number'])){
                        $sheet->setCellValue($hli['row'].$row_count, $list_item[$hli['param']]);
                        $sheet->getStyle($hli['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    
                    }
                    else{
                        $sheet->setCellValue($hli['row'].$row_count, $list_item[$hli['param']]);
                    }
    
                    if(isset($hli['param']) && array_key_exists($hli['param'], $sub_total)){
                        $sub_total[$hli['param']] += $list_item[$hli['param']];
                        $grand_total[$hli['param']] += $list_item[$hli['param']];
                    }
                }
                $row_count++;
                $count_no ++;
            }

            $row_count++;
            $sheet->setCellValue("A".$row_count, "Sub Total :");
            foreach($header as $val){
                if(isset($val['param']) && array_key_exists($val['param'], $sub_total)){
                    $sheet->setCellValue($val['row'].$row_count, $sub_total[$val['param']]);
                    $sheet->getStyle($val['row'].$row_count)->getFont()->setBold( true );
                    $sheet->getStyle($val['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
            $row_count ++;
        }

        $row_count++;
        $sheet->setCellValue("A".$row_count, "Total :");
        foreach($header as $val){
            if(isset($val['param']) && array_key_exists($val['param'], $grand_total)){
                $sheet->setCellValue($val['row'].$row_count, $grand_total[$val['param']]);
                $sheet->getStyle($val['row'].$row_count)->getFont()->setBold( true );
                $sheet->getStyle($val['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }
        
        $writer = new Xlsx($spreadsheet);
        $filename = 'Sales_Report-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;

    }

    public function TSASalesReportExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $header = array(
            "No" => array('row'=>"A"),
            "Issued Date" =>array('row'=>"B", 'param'=>'order_date'),
            "Document Number" => array('row'=>"C", 'param'=>'order_no'),
            "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
            "Related Info" => array('row'=>"E", 'param'=>'related_info'),
            "Policy Number" => array('row'=>"F", 'param'=>'order_policyno'),
            "Premium Charges" => array('row'=>"G", 'param'=>'order_grosspremium_amount', 'number' => true),
            "Receipt Number" => array('row'=>"H", 'param'=>'receipt_data'),
            "Name Of TSA" => array('row'=>"I", 'param'=>'dealer_name'),
            "TSA Comm" => array('row'=>"J", 'param'=>'total_tsa_comm'),
            "Comm Cheque" => array('row'=>"K", 'param'=>'tsa_comm_cheque'),
            "Insurance company" => array('row'=>"L", 'param'=>'insurance_company'),
            "Cheque paid to Insurance company" => array('row'=>"M", 'param'=>'to_ins_cheque'),
            "Premium charge to TSA" => array('row'=>"N", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
            "Nett premium" => array('row'=>"O", 'param'=>'order_subtotal_amount', 'number' => true),
            "Premium GST" => array('row'=>"P", 'param'=>'order_gst_amount', 'number' => true),
            "Commission to Ensure/ Peoples" => array('row'=>"Q", 'param'=>'order_payable_ourcomm', 'number' => true),
            "GST on commission" => array('row'=>"R", 'param'=>'order_payable_gstcomm', 'number' => true),
            "Profit/ Loss" => array('row'=>"S", 'param'=>'order_payable_nettcomm', 'number' => true),
            "Invoice number to insurance company" => array('row'=>"T", 'param'=>'ins_inv'),
            "Ins document number" => array('row'=>"U", 'param'=>'order_ins_doc_no'),
            "Direct Discount" => array('row'=>"V", 'param'=>'order_direct_discount_amt'),
        );

        $sheet->setCellValue('A1', 'TSA Sales Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }

        $grand_total = array(
            'order_grosspremium_amount' => 0,
            'order_receivable_premiumreceivable' => 0,
            'order_subtotal_amount' => 0,
            'order_gst_amount' => 0,
            'order_payable_ourcomm' => 0,
            'order_payable_gstcomm' => 0,
            'order_payable_nettcomm' => 0,
            'order_direct_discount_amt' => 0,
        );

        $row_count = 3;
        foreach($this->data_listing as $key => $list_data){
            $sheet->setCellValue("A".$row_count, isset($this->insurance_type_mapping[$key])?$this->insurance_type_mapping[$key]:'-');
            $sheet->getStyle("A".$row_count)->getFont()->setBold( true );
            $row_count ++;
            $count_no = 1;
            $sub_total = array(
                'order_grosspremium_amount' => 0,
                'order_receivable_premiumreceivable' => 0,
                'order_subtotal_amount' => 0,
                'order_gst_amount' => 0,
                'order_payable_ourcomm' => 0,
                'order_payable_gstcomm' => 0,
                'order_payable_nettcomm' => 0,
                'order_direct_discount_amt' => 0,
            );
            foreach($list_data as $list_item){
                foreach($header as $ky => $hli){
                    if($ky == 'No'){
                        $sheet->setCellValue($hli['row'].$row_count, $count_no);
                    }
                    elseif(isset($hli['number'])){
                        $sheet->setCellValue($hli['row'].$row_count, $list_item[$hli['param']]);
                        $sheet->getStyle($hli['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    
                    }
                    else{
                        $sheet->setCellValue($hli['row'].$row_count, $list_item[$hli['param']]);
                    }
    
                    if(isset($hli['param']) && array_key_exists($hli['param'], $sub_total)){
                        $sub_total[$hli['param']] += $list_item[$hli['param']];
                        $grand_total[$hli['param']] += $list_item[$hli['param']];
                    }
                }
                $row_count++;
                $count_no ++;
            }

            $row_count++;
            $sheet->setCellValue("A".$row_count, "Sub Total :");
            foreach($header as $val){
                if(isset($val['param']) && array_key_exists($val['param'], $sub_total)){
                    $sheet->setCellValue($val['row'].$row_count, $sub_total[$val['param']]);
                    $sheet->getStyle($val['row'].$row_count)->getFont()->setBold( true );
                    $sheet->getStyle($val['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
            $row_count ++;
        }

        $row_count++;
        $sheet->setCellValue("A".$row_count, "Total :");
        foreach($header as $val){
            if(isset($val['param']) && array_key_exists($val['param'], $grand_total)){
                $sheet->setCellValue($val['row'].$row_count, $grand_total[$val['param']]);
                $sheet->getStyle($val['row'].$row_count)->getFont()->setBold( true );
                $sheet->getStyle($val['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }
        
        $writer = new Xlsx($spreadsheet);
        $filename = 'TSA_Sales_Report-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;

    }

    public function CusSalesReportExcel(){
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $header = array(
            "No" => array('row'=>"A"),
            "Issued Date" =>array('row'=>"B", 'param'=>'order_date'),
            "Document Number" => array('row'=>"C", 'param'=>'order_no'),
            "Insured Name" => array('row'=>"D", 'param'=>'partner_name'),
            "Related Info" => array('row'=>"E", 'param'=>'related_info'),
            "Policy Number" => array('row'=>"F", 'param'=>'order_policyno'),
            "Premium Charges" => array('row'=>"G", 'param'=>'order_grosspremium_amount', 'number' => true),
            "Receipt Number" => array('row'=>"H", 'param'=>'receipt_data'),
            "Name Of TSA" => array('row'=>"I", 'param'=>'dealer_name'),
            "TSA Comm" => array('row'=>"J", 'param'=>'total_tsa_comm'),
            "Comm Cheque" => array('row'=>"K", 'param'=>'tsa_comm_cheque'),
            "Insurance company" => array('row'=>"L", 'param'=>'insurance_company'),
            "Cheque paid to Insurance company" => array('row'=>"M", 'param'=>'to_ins_cheque'),
            "Premium charge to TSA" => array('row'=>"N", 'param'=>'order_receivable_premiumreceivable', 'number' => true),
            "Nett premium" => array('row'=>"O", 'param'=>'order_subtotal_amount', 'number' => true),
            "Premium GST" => array('row'=>"P", 'param'=>'order_gst_amount', 'number' => true),
            "Commission to Ensure/ Peoples" => array('row'=>"Q", 'param'=>'order_payable_ourcomm', 'number' => true),
            "GST on commission" => array('row'=>"R", 'param'=>'order_payable_gstcomm', 'number' => true),
            "Profit/ Loss" => array('row'=>"S", 'param'=>'order_payable_nettcomm', 'number' => true),
            "Invoice number to insurance company" => array('row'=>"T", 'param'=>'ins_inv'),
            "Ins document number" => array('row'=>"U", 'param'=>'order_ins_doc_no'),
            "Direct Discount" => array('row'=>"V", 'param'=>'order_direct_discount_amt'),
        );

        $sheet->setCellValue('A1', 'Customer Sales Report - '.date('d-M-Y',strtotime($this->period_from))." To ".date('d-M-Y',strtotime($this->period_to)));
        foreach($header as $key => $li){
            $sheet->setCellValue($li['row'].'2', $key);
            $sheet->getStyle($li['row'].'2')->getFont()->setBold( true );
        }

        $grand_total = array(
            'order_grosspremium_amount' => 0,
            'order_receivable_premiumreceivable' => 0,
            'order_subtotal_amount' => 0,
            'order_gst_amount' => 0,
            'order_payable_ourcomm' => 0,
            'order_payable_gstcomm' => 0,
            'order_payable_nettcomm' => 0,
            'order_direct_discount_amt' => 0,
        );

        $row_count = 3;
        foreach($this->data_listing as $key => $list_data){
            $sheet->setCellValue("A".$row_count, isset($this->insurance_type_mapping[$key])?$this->insurance_type_mapping[$key]:'-');
            $sheet->getStyle("A".$row_count)->getFont()->setBold( true );
            $row_count ++;
            $count_no = 1;
            $sub_total = array(
                'order_grosspremium_amount' => 0,
                'order_receivable_premiumreceivable' => 0,
                'order_subtotal_amount' => 0,
                'order_gst_amount' => 0,
                'order_payable_ourcomm' => 0,
                'order_payable_gstcomm' => 0,
                'order_payable_nettcomm' => 0,
                'order_direct_discount_amt' => 0,
            );
            foreach($list_data as $list_item){
                foreach($header as $ky => $hli){
                    if($ky == 'No'){
                        $sheet->setCellValue($hli['row'].$row_count, $count_no);
                    }
                    elseif(isset($hli['number'])){
                        $sheet->setCellValue($hli['row'].$row_count, $list_item[$hli['param']]);
                        $sheet->getStyle($hli['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                    
                    }
                    else{
                        $sheet->setCellValue($hli['row'].$row_count, $list_item[$hli['param']]);
                    }
    
                    if(isset($hli['param']) && array_key_exists($hli['param'], $sub_total)){
                        $sub_total[$hli['param']] += $list_item[$hli['param']];
                        $grand_total[$hli['param']] += $list_item[$hli['param']];
                    }
                }
                $row_count++;
                $count_no ++;
            }

            $row_count++;
            $sheet->setCellValue("A".$row_count, "Sub Total :");
            foreach($header as $val){
                if(isset($val['param']) && array_key_exists($val['param'], $sub_total)){
                    $sheet->setCellValue($val['row'].$row_count, $sub_total[$val['param']]);
                    $sheet->getStyle($val['row'].$row_count)->getFont()->setBold( true );
                    $sheet->getStyle($val['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }
            $row_count ++;
        }

        $row_count++;
        $sheet->setCellValue("A".$row_count, "Total :");
        foreach($header as $val){
            if(isset($val['param']) && array_key_exists($val['param'], $grand_total)){
                $sheet->setCellValue($val['row'].$row_count, $grand_total[$val['param']]);
                $sheet->getStyle($val['row'].$row_count)->getFont()->setBold( true );
                $sheet->getStyle($val['row'].$row_count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
        }
        
        $writer = new Xlsx($spreadsheet);
        $filename = 'Customer_Sales_Report-'.strtoupper(date('d-M-Y')).".xlsx";
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        $writer->save('php://output');
        exit;

    }

    public function getDealerInfoByID($tsa_id){
        $dealer_info = TSA::select('dealer_name', 'dealer_blk_no', 'dealer_street', 'dealer_unit_no', 'dealer_postalcode')
        ->where('dealer_id', $tsa_id)
        ->first()->toArray();
        return $dealer_info;
    }
    
    public function getPartnerInfoByID($cus_id){
        $cus_info = Customer::select('partner_name', 'partner_blk_no', 'partner_street', 'partner_unit_no', 'partner_postalcode')
        ->where('partner_id', $cus_id)
        ->first()->toArray();
        return $cus_info;
    }

    public function getInsuranceComapnyInfoByID($ins_id){
        $ins_info = Insuranceco::select('insuranceco_name', 'insuranceco_address')
        ->where('insuranceco_id', $ins_id)
        ->first()->toArray();
        return $ins_info;
    }

    public function getReceiptData($order_id){
        $receipt_query = ReceiptDetail::select('receipt_detail.recpline_order_amount', 'receipt_detail.recpline_offset_amount AS paid_total', 'receipt.receipt_no', 'receipt.receipt_date', 'receipt.receipt_cheque')
        ->join('receipt', 'receipt.receipt_id', '=', 'receipt_detail.recpline_receipt_id')
        ->where('receipt_detail.recpline_order_id', $order_id)
        ->where('receipt.receipt_status', 1)
        ->where('receipt_detail.recpline_status', 1)
        ->get()->toArray();

        $output = [];

        foreach($receipt_query as $row){
            $output[] = $row;
        }

        return $output;
    }

    public function getPaymentData($order_id, $bill_to = ''){
        $payment_query = Payment::select('payment.payment_cheque', 'payment.payment_method')
        ->join('payment', 'payment.payment_id', '=', 'payment_detail.paymentline_payment_id')
        ->where('payment_detail.paymentline_order_id', $order_id)
        ->where('payment.payment_status', 1)
        ->where('payment_detail.paymentline_status', 1);

        if($this->dealer_id > 0 && $bill_to == 'To Customer'){
            $payment_query->where('payment.payment_type', '!=', 'customer');
        }

        $pay_data = $payment_query->first()->toArray();
        return $pay_data;
    }
}
