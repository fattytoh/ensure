<?php

namespace App\Http\Controllers;

use App\Models\GroupPrivilege;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserGroupPrivilegeController extends Controller
{
    protected $excluded_routename = [
        'SwitchSite',
        'dashboard'
    ];
    //
    public function getPrivilegeList(){
        return [
            'empl' => [
                'name' => 'Employee Control',
                'list' => [
                    'EmplList' => 'View Employee List',
                    'EmplView' => 'View Employee Profile',
                    'EmplAddNew' => 'Add Employee Form',
                    'EmplCreate' => 'Create Employee',
                    'EmplUpdate' => 'Update Employee',
                    'EmplDelete' => 'Delete Employee',
                ]
            ],
            'tsa' => [
                'name' => 'TSA Control',
                'list' => [
                    'TSAList' => 'View TSA List',
                    'TSAView' => 'View TSA Profile',
                    'TSAAddNew' => 'Add TSA Form',
                    'TSACreate' => 'Create TSA',
                    'TSAUpdate' => 'Update TSA',
                    'TSADelete' => 'Delete TSA',
                ]
            ],
            'usergroup' => [
                'name' => 'User Group Control',
                'list' => [
                    'UserGroupList' => 'User Group List',
                    'UserGroupInfo' => 'View User Group',
                    'UserGroupAddnew' => 'Add User Group Form',
                    'UserGroupCreate' => 'Add User Group',
                    'UserGroupUpdate' => 'Update User Group',
                    'UserGroupDel' => 'Delete User Group',
                ]
            ],
            'allianz' => [
                'name' => 'Allianz Control',
                'list' => [
                    'AllianzExportData' => 'Export Allianz Report',
                    'AllianzList' => 'Allianz List',
                    'AllianzAddNew' => 'Add Allianz Form',
                    'AllianzCreate' => 'Add Allianz',
                    'AllianzUpdate' => 'Update Allianz',
                    'AllianzDelete' => 'Delete Allianz',
                    'AllianzView' => 'View / Edit Allianz',
                    'AllianzPrint' => 'Print Allianz',
                ]
            ],
        ];
    }

    public static function checkPrivilegeWithAuth($routename){
        if(Auth::user()->id != '1'){
            if(!in_array($routename, $this->excluded_routename)){
                if(env('APP_ID') == 2){
                    $pri = GroupPrivilege::where("group_id", Auth::user()->ens_group_id )
                    ->where('route_name', $routename)
                    ->where('status', 'ACTIVE')
                    ->first();
                }
                else{
                    $pri = GroupPrivilege::where("group_id", Auth::user()->peo_group_id )
                    ->where('route_name', $routename)
                    ->where('status', 'ACTIVE')
                    ->first();
                }

                if($pri){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return true;
            }
        }
        else{
            return true;
        }
    }

    public static function checkPrivilegeGroup($route_list){
        foreach($route_list as $li){
            if(self::checkPrivilegeWithAuth($li)){
                return true;
            }
        }

        return false;
    }
}
