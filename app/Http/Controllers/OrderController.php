<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Refno;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    //
    public function create(Request $request, $prefix, $order_prefix = ''){
        if($order_prefix == ''){
            $order_prefix = $prefix;
        }
        $create_order_ref = $request->input('order_ref')?:0;
        $new_order = new Order();
        $new_order->order_prefix_type = $prefix;
        $new_order->order_cprofile = env('APP_ID');
        $new_order->order_no = $this->generateCode($create_order_ref, $request->input('order_doc_type'), $order_prefix);
        $this->updateRefCode($create_order_ref, $order_prefix);
        $new_order->order_doc_type = $request->input('order_doc_type');
        $new_order->order_ref = $create_order_ref; 
        $new_order->order_renewal_ref = $request->input('order_renewal_ref')?:0;
        $new_order->order_ins_doc_no = $request->input('order_ins_doc_no');
        $new_order->order_type = $request->input('order_type');
        $new_order->order_insurco = $request->input('order_insurco');
        $new_order->order_date = $request->input('order_date');
        $new_order->order_endorsement_type = $request->input('order_endorsement_type');
        $new_order->order_endorsement_type_input = $request->input('order_endorsement_type_input');
        $new_order->order_customer = $request->input('order_customer')?:0;
        $new_order->order_dealer = $request->input('order_dealer')?:0;
        $new_order->order_premium_amt = $this->cleanNumber($request->input('order_premium_amt'));
        $new_order->order_commercial_use_per = $this->cleanNumber($request->input('order_commercial_use_per'));
        $new_order->order_accident_per = $this->cleanNumber($request->input('order_accident_per'));
        $new_order->order_accident_amt = $this->cleanNumber($request->input('order_accident_amt'));
        $new_order->order_in_exp_amt = $this->cleanNumber($request->input('order_in_exp_amt'));
        $new_order->order_vehicle_load_per = $this->cleanNumber($request->input('order_vehicle_load_per'));
        $new_order->order_vehicle_load_amt = $this->cleanNumber($request->input('order_vehicle_load_amt'));
        $new_order->order_ncd_dis_per = $this->cleanNumber($request->input('order_ncd_dis_per'));
        $new_order->order_ncd_dis_amt = $this->cleanNumber($request->input('order_ncd_dis_amt'));
        $new_order->order_ofd_dis_per = $this->cleanNumber($request->input('order_ofd_dis_per'));
        $new_order->order_ofd_dis_amt = $this->cleanNumber($request->input('order_ofd_dis_amt'));
        $new_order->order_3rd_rider_amt = $this->cleanNumber($request->input('order_3rd_rider_amt'));
        $new_order->order_4th_rider_amt = $this->cleanNumber($request->input('order_4th_rider_amt'));
        $new_order->order_addtional_amt = $this->cleanNumber($request->input('order_addtional_amt'));
        $new_order->order_subtotal_amount = $this->cleanNumber($request->input('order_subtotal_amount'));
        $new_order->order_gst_percent = $this->cleanNumber($request->input('order_gst_percent'));
        $new_order->order_gst_amount = $this->cleanNumber($request->input('order_gst_amount'));
        $new_order->order_direct_discount_amt = $this->cleanNumber($request->input('order_direct_discount_amt'));
        $new_order->order_bank_interest = $this->cleanNumber($request->input('order_bank_interest'));
        $new_order->order_grosspremium_amount = $this->cleanNumber($request->input('order_grosspremium_amount'));
        $new_order->order_extra_name = $request->input('order_extra_name');
        $new_order->order_extra_amount = $this->cleanNumber($request->input('order_extra_amount'));
        $new_order->order_receivable_dealercommpercent = $this->cleanNumber($request->input('order_receivable_dealercommpercent'));
        $new_order->order_receivable_dealercomm = $this->cleanNumber($request->input('order_receivable_dealercomm'));
        $new_order->order_receivable_dealergst = $this->cleanNumber($request->input('order_receivable_dealergst'));
        $new_order->order_receivable_dealergstamount = $this->cleanNumber($request->input('order_receivable_dealergstamount'));
        $new_order->order_receivable_gstpercent = $this->cleanNumber($request->input('order_receivable_gstpercent'));
        $new_order->order_receivable_gstamount = $this->cleanNumber($request->input('order_receivable_gstamount'));
        $new_order->order_receivable_premiumreceivable = $this->cleanNumber($request->input('order_receivable_premiumreceivable'));
        $new_order->order_payable_ourcommpercent = $this->cleanNumber($request->input('order_payable_ourcommpercent'));
        $new_order->order_payable_ourcomm = $this->cleanNumber($request->input('order_payable_ourcomm'));
        $new_order->order_payable_nettcommpercent = $this->cleanNumber($request->input('order_payable_nettcommpercent'));
        $new_order->order_payable_nettcomm = $this->cleanNumber($request->input('order_payable_nettcomm'));
        $new_order->order_payable_premiumpayable = $request->input('order_payable_premiumpayable');
        $new_order->order_payable_gstcomm = $this->cleanNumber($request->input('order_payable_gstcomm'));
        $new_order->order_payable_gstpercent = $this->cleanNumber($request->input('order_payable_gstpercent'));
        $new_order->order_payable_gst = $this->cleanNumber($request->input('order_payable_gst'));
        $new_order->order_payable_premium = $this->cleanNumber($request->input('order_payable_premium'));
        //maid cal
        $new_order->order_direct_discount_percentage = $this->cleanNumber($request->input('order_direct_discount_percentage'));
        $new_order->order_cover_plan_no_gst = $this->cleanNumber($request->input('order_cover_plan_no_gst'));
        $new_order->order_status = 1;
        $new_order->insertBy = Auth::user()->id;
        $new_order->updateBy = Auth::user()->id;
        $new_order->save();

        return $new_order->order_id;
    }

    public function update(Request $request){
        $exist_order = Order::find( $request->input('order_id'));
        
        $exist_order->order_doc_type = $request->input('order_doc_type');
        $exist_order->order_ref = $request->input('order_ref')?:0;
        $exist_order->order_renewal_ref = $request->input('order_renewal_ref')?:0;
        $exist_order->order_ins_doc_no = $request->input('order_ins_doc_no');
        $exist_order->order_type = $request->input('order_type');
        $exist_order->order_insurco = $request->input('order_insurco');
        $exist_order->order_date = $request->input('order_date');
        $exist_order->order_endorsement_type = $request->input('order_endorsement_type');
        $exist_order->order_endorsement_type_input = $request->input('order_endorsement_type_input');
        $exist_order->order_customer = $request->input('order_customer');
        $exist_order->order_dealer = $request->input('order_dealer');
        $exist_order->order_premium_amt = $this->cleanNumber($request->input('order_premium_amt'));
        $exist_order->order_commercial_use_per = $this->cleanNumber($request->input('order_commercial_use_per'));
        $exist_order->order_accident_per = $this->cleanNumber($request->input('order_accident_per'));
        $exist_order->order_accident_amt = $this->cleanNumber($request->input('order_accident_amt'));
        $exist_order->order_in_exp_amt = $this->cleanNumber($request->input('order_in_exp_amt'));
        $exist_order->order_vehicle_load_per = $this->cleanNumber($request->input('order_vehicle_load_per'));
        $exist_order->order_vehicle_load_amt = $this->cleanNumber($request->input('order_vehicle_load_amt'));
        $exist_order->order_ncd_dis_per = $this->cleanNumber($request->input('order_ncd_dis_per'));
        $exist_order->order_ncd_dis_amt = $this->cleanNumber($request->input('order_ncd_dis_amt'));
        $exist_order->order_ofd_dis_per = $this->cleanNumber($request->input('order_ofd_dis_per'));
        $exist_order->order_ofd_dis_amt = $this->cleanNumber($request->input('order_ofd_dis_amt'));
        $exist_order->order_3rd_rider_amt = $this->cleanNumber($request->input('order_3rd_rider_amt'));
        $exist_order->order_4th_rider_amt = $this->cleanNumber($request->input('order_4th_rider_amt'));
        $exist_order->order_addtional_amt = $this->cleanNumber($request->input('order_addtional_amt'));
        $exist_order->order_subtotal_amount = $this->cleanNumber($request->input('order_subtotal_amount'));
        $exist_order->order_gst_percent = $this->cleanNumber($request->input('order_gst_percent'));
        $exist_order->order_gst_amount = $this->cleanNumber($request->input('order_gst_amount'));
        $exist_order->order_direct_discount_amt = $this->cleanNumber($request->input('order_direct_discount_amt'));
        $exist_order->order_bank_interest = $this->cleanNumber($request->input('order_bank_interest'));
        $exist_order->order_grosspremium_amount = $this->cleanNumber($request->input('order_grosspremium_amount'));
        $exist_order->order_extra_name = $request->input('order_extra_name');
        $exist_order->order_extra_amount = $this->cleanNumber($request->input('order_extra_amount'));
        $exist_order->order_receivable_dealercommpercent = $this->cleanNumber($request->input('order_receivable_dealercommpercent'));
        $exist_order->order_receivable_dealercomm = $this->cleanNumber($request->input('order_receivable_dealercomm'));
        $exist_order->order_receivable_dealergst = $this->cleanNumber($request->input('order_receivable_dealergst'));
        $exist_order->order_receivable_dealergstamount = $this->cleanNumber($request->input('order_receivable_dealergstamount'));
        $exist_order->order_receivable_gstpercent = $this->cleanNumber($request->input('order_receivable_gstpercent'));
        $exist_order->order_receivable_gstamount = $this->cleanNumber($request->input('order_receivable_gstamount'));
        $exist_order->order_receivable_premiumreceivable = $this->cleanNumber($request->input('order_receivable_premiumreceivable'));
        $exist_order->order_payable_ourcommpercent = $this->cleanNumber($request->input('order_payable_ourcommpercent'));
        $exist_order->order_payable_ourcomm = $this->cleanNumber($request->input('order_payable_ourcomm'));
        $exist_order->order_payable_nettcommpercent = $this->cleanNumber($request->input('order_payable_nettcommpercent'));
        $exist_order->order_payable_nettcomm = $this->cleanNumber($request->input('order_payable_nettcomm'));
        $exist_order->order_payable_premiumpayable = $request->input('order_payable_premiumpayable');
        $exist_order->order_payable_gstcomm = $this->cleanNumber($request->input('order_payable_gstcomm'));
        $exist_order->order_payable_gstpercent = $this->cleanNumber($request->input('order_payable_gstpercent'));
        $exist_order->order_payable_gst = $this->cleanNumber($request->input('order_payable_gst'));
        $exist_order->order_payable_premium = $this->cleanNumber($request->input('order_payable_premium'));
        //maid cal
        $exist_order->order_direct_discount_percentage = $this->cleanNumber($request->input('order_direct_discount_percentage'));
        $exist_order->order_cover_plan_no_gst = $this->cleanNumber($request->input('order_cover_plan_no_gst'));
        $exist_order->updateBy = Auth::user()->id;
        $exist_order->save();
    }

    public function cleanNumber($input){
        $input = preg_replace('/[^\d.]/', '', $input);
        return $input?:0;
    }

    public function generateCode($order_ref, $type = "DN", $prefix){
        if($order_ref > 0){
            $order_ref_no = Order::find($order_ref);
            $last_count = Order::where("order_ref", $order_ref)->where("order_doc_type", $type)->count();
            $last_count ++;
            if($type == 'DN'){
                $code =  $order_ref_no->order_no.'-'.env("APP_CODE")."D". sprintf("%02d", $last_count);
            }
            else{
                $code =  $order_ref_no->order_no.'-'.env("APP_CODE")."C". sprintf("%02d", $last_count);
            }

            return $code; 
        }
        else{
            $ref = Refno::where('prefix_code', $prefix)
            ->where('cprofile', env("APP_CODE"))
            ->first();
            if($ref){
                $last_id = $ref->last_no;
                $last_id ++;
            }
            else{
                $new_ref = new Refno();
                $new_ref->prefix_code = $prefix;
                $new_ref->cprofile = env("APP_CODE");
                $new_ref->last_no = 0;
                $new_ref->save();
                $last_id = 1;
            }

            $code =  env("APP_CODE").$prefix .'-'. sprintf("%06d", $last_id);

            $motor_code = Order::where('order_no',$code)->first();
            if($motor_code){
                $this->updateRefCode($order_ref, $prefix);
                return $this->generateCode();
            }
            else{
                return $code; 
            }
        }
    }

    public function updateRefCode($order_ref, $prefix){
        if($order_ref == 0){
            $ref = Refno::where('prefix_code', $prefix)
            ->where('cprofile', env("APP_CODE"))
            ->first();
            $ref->last_no =  $ref->last_no + 1;
            $ref->save();
        }
    }

}
