<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\PaymentDetail;
use App\Models\Payment;
use App\Models\Order;
use App\Models\Motor;
use App\Models\Maid;
use App\Models\User;
use App\Models\General;
use App\Models\Customer;
use App\Models\Insuranceco;
use App\Models\InsurancePlan;
use App\Models\Refno;
use App\Models\TSA;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    //

    public function getListing(Request $request){
        return view('payment.List');
    }

    public function getListData(Request $request){
         // Total records
        $order_fields = ['','payment_no','payment_date','partner_name','payment_bank','payment_paid', '', ''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $count_query = Payment::select('count(*) as allcount')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'payment.payment_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'payment.payment_customer')
        ->leftJoin('insurance_comp', 'insurance_comp.insuranceco_id', '=', 'payment.payment_insuranco')
        ->where('payment.payment_cprofile', env('APP_ID'))
        ->where('payment.payment_to', 'Customer')
        ->where('payment.payment_status', "!=","0");

        $main_query = Payment::select('payment.*', "customer.partner_name", "tsa.dealer_name", "insurance_comp.insuranceco_name")
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'payment.payment_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'payment.payment_customer')
        ->leftJoin('insurance_comp', 'insurance_comp.insuranceco_id', '=', 'payment.payment_insuranco')
        ->where('payment.payment_cprofile', env('APP_ID'))
        ->where('payment.payment_to', 'Customer')
        ->where('payment.payment_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('tsa.dealer_name', 'like', '%' . $searchValue . '%')
                ->orWhere('payment.payment_bank', 'like', '%' . $searchValue . '%')
                ->orWhere('payment.payment_date', 'like', '%' . $searchValue . '%')
                ->orWhere('payment.payment_no', 'like', '%' . $searchValue . '%');
        });

        $totalRecords = $count_query->count();
        $totalRecordswithFilter = $count_query
        ->where(function ($query) use ($searchValue) {
            $query->where('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('tsa.dealer_name', 'like', '%' . $searchValue . '%')
                ->orWhere('payment.payment_bank', 'like', '%' . $searchValue . '%')
                ->orWhere('payment.payment_date', 'like', '%' . $searchValue . '%')
                ->orWhere('payment.payment_no', 'like', '%' . $searchValue . '%');
        })
        ->count();

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }

        $listing_data = $main_query
        ->skip($start)
        ->take($rowperpage)
        ->get()->toArray();

        $out_data = [];

        foreach($listing_data as $ky => $li){
            $action = '';

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("PaymentView")){
                $action .= '<a href="'.route('PaymentView', ['id' => $li['payment_id'] ]).'" class="btn btn-success">View</a>';
            }

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("PaymentInfo")){
                $action .= '<a href="'.route('PaymentInfo', ['id' => $li['payment_id'] ]).'" class="btn btn-warning">Edit</a>';
            }

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("PaymentDelete")){
                $action .= '<a href="'.route('PaymentDelete', ['id' => $li['payment_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['payment_no'].'">Delete</a>';
            }
            $empl = User::find($li['insertBy']);
            $out_data[] = [
                $ky + 1,
                $li['payment_no']?:'-',
                $li['payment_date'],
                $li['partner_name']?:($li['dealer_name']?:($li['insuranceco_name']?:'-')),
                $li['payment_bank']?:'-',
                $li['payment_paid']?:'-',
                $empl->getName(),
                $action
            ];
        }
       
        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200);
    }

    public function getOrderList(Request $request){
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        if(!$request->input("payment_dealer") && !$request->input("payment_insuranco") && !$request->input("payment_customer")){
            $output = [
                "draw" => intval($draw),
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => [],
            ];

            return response()->json($output, 200);
        }
        else{
            $count_query = Order::select('count(*) as allcount')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', "!=","0");

            $main_query = Order::select('order.*', "customer.partner_name", "insurance_general.order_policyno AS gi_policyno", "insurance_motor.order_policyno AS mi_policyno", "insurance_maid.order_policyno AS gm_policyno", 'insurance_maid.order_maid_name', "insurance_motor.order_vehicles_no", "insurance_general.order_ins_doc_no", "insurance_maid.order_coverage AS gm_coverage",  "insurance_motor.order_coverage AS mi_coverage",  "insurance_general.order_coverage AS gi_coverage" )
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
            ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
            ->leftJoin('insurance_general', 'insurance_general.order_id', '=', 'order.order_id')
            ->leftJoin('insurance_motor', 'insurance_motor.order_id', '=', 'order.order_id')
            ->leftJoin('insurance_maid', 'insurance_maid.order_id', '=', 'order.order_id')
            ->where('order.order_cprofile', env('APP_ID'))
            ->where('order.order_status', "!=","0");

            if($request->input('payment_to') == 'TSA'){
                $main_query->where('order.order_dealer', $request->input("payment_dealer"));
                $count_query->where('order.order_dealer', $request->input("payment_dealer"));
            }
            elseif($request->input('payment_to') == 'Customer'){
                $main_query->where('order.order_customer', $request->input("payment_customer"));
                $count_query->where('order.order_customer', $request->input("payment_customer"));
            }
            else{
                $main_query->where('order.order_insurco', $request->input("payment_insuranco"));
                $count_query->where('order.order_insurco', $request->input("payment_insuranco"));
            }
            
            if($request->input("fil_start_date")){
                $main_query->where('order.order_date', '>=', $request->input("fil_start_date"));
                $count_query->where('order.order_date', '>=', $request->input("fil_start_date"));
            }

            if($request->input("fil_end_date")){
                $main_query->where('order.order_date', '<=', $request->input("fil_end_date"));
                $count_query->where('order.order_date', '<=', $request->input("fil_end_date"));
            }

            if($request->input("fil_order_type") && $request->input("fil_order_type") != "ALL"){
                $main_query->where('order.order_prefix_type', $request->input("fil_order_type"));
                $count_query->where('order.order_prefix_type', $request->input("fil_order_type"));
            }

            if($request->input("fil_policyno")){
                $policy_no = $request->input("fil_policyno");
                $main_query->where(function ($query) use ($policy_no) {
                    $query->where('insurance_general.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_motor.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_maid.order_policyno', 'like', '%' . $policy_no . '%');
                });
                $count_query->where(function ($query) use ($policy_no) {
                    $query->where('insurance_general.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_motor.order_policyno', 'like', '%' . $policy_no . '%')
                        ->orWhere('insurance_maid.order_policyno', 'like', '%' . $policy_no . '%');
                });
            }

            $totalRecords = $count_query->count();
            $totalRecordswithFilter = $count_query->count();

            $listing_data = $main_query
            ->skip($start)
            ->take($rowperpage)
            ->get()->toArray();

            $out_data = [];
            foreach($listing_data as $ky => $li){
                $no = '<div class="form-check"> <input type="checkbox" class="form-check-input" name="order_case[]" value="'.$li['order_id'].'"> <label class="form-check-label" for="order_case"> '.($ky + 1).' </label> </div>';

                if($li['order_prefix_type'] == 'GI'){
                    $cover_plan = InsurancePlan::find($li['gi_coverage']);
                    $coverage = "-";
                    if($cover_plan){
                        $coverage = $cover_plan->insuranceclass_code;
                    }
                    $related = $li['order_ins_doc_no'];
                    $order_num = '<a href="'.route('GeneralInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    $policy = $li['gi_policyno'];
                }
                elseif($li['order_prefix_type'] == 'GM'){
                    $coverage = $li['gm_coverage'];
                    $related = $li['order_maid_name'];
                    $order_num = '<a href="'.route('MaidInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    $policy = $li['gm_policyno'];
                }
                else{
                    $coverage = $li['mi_coverage'];
                    $related = $li['order_vehicles_no'];
                    $policy = $li['mi_policyno'];
                    if($li['order_prefix_type'] == 'CY'){
                        $order_num = '<a href="'.route('MotorInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    }
                    elseif($li['order_prefix_type'] == 'MT'){
                        $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    }
                    else{
                        $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $li['order_id'] ]).'" target="_blank" >'.$li['order_no'].'</a>';
                    }
                }
                $amount_list = '';
                if($li['order_doc_type'] == 'DN'){
                    if($request->input('payment_to') == 'TSA'){
                        $total = $li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'];
                        $amount_list .= 'Premium Amount :'.$li['order_grosspremium_amount']."<br>";
                        $amount_list .= 'TSA refer Fee % :'.$li['order_receivable_dealercommpercent']."%<br>";
                        $amount_list .= 'TSA refer Fee Amount :'.$li['order_receivable_dealercomm']."%<br>";
                        $amount_list .= 'Amount :'.($li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'])."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($total, $li['order_id']);
                    }
                    elseif($request->input('payment_to') == 'Customer'){
                        $amount_list .= 'Premium Amount :'.$li['order_grosspremium_amount'] ."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($li['order_grosspremium_amount'], $li['order_id']);
                    }
                    else{
                        $amount_list .= 'Premium Amount :'.$li['order_grosspremium_amount']."<br>";
                        $amount_list .= 'Our commission % :'.$li['order_payable_ourcommpercent']."%<br>";
                        $amount_list .= 'Comm of GST :'.($li['order_payable_ourcomm'] + $li['order_payable_gstcomm'])."<br>";
                        $amount_list .= 'Premium payable :'.$li['order_payable_premium'] ."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($li['order_payable_premium'], $li['order_id']);
                    }
                }
                else{
                    if($request->input('payment_to') == 'TSA'){
                        $total = $li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'];
                        $amount_list .= 'Premium Amount :-'.$li['order_grosspremium_amount']."<br>";
                        $amount_list .= 'TSA refer Fee % :'.$li['order_receivable_dealercommpercent']."%<br>";
                        $amount_list .= 'TSA refer Fee Amount :'.$li['order_receivable_dealercomm']."%<br>";
                        $amount_list .= 'Amount :'.($li['order_receivable_dealercomm'] + $li['order_receivable_dealergstamount'])."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance( $total, $li['order_id']);
                    }
                    elseif($request->input('payment_to') == 'Customer'){
                        $amount_list .= 'Premium Amount :-'.$li['order_grosspremium_amount'] ."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($li['order_grosspremium_amount'], $li['order_id']);
                    }
                    else{
                        $amount_list .= 'Premium Amount :-'.$li['order_grosspremium_amount']."<br>";
                        $amount_list .= 'Our commission % :'.$li['order_payable_ourcommpercent']."%<br>";
                        $amount_list .= 'Comm of GST :'.($li['order_payable_ourcomm'] + $li['order_payable_gstcomm'])."<br>";
                        $amount_list .= 'Premium payable :'.$li['order_payable_premium'] ."<br>";
                        $amount_list .= 'Balance :'.$this->getBalance($li['order_payable_premium'], $li['order_id']);
                    }
                }

                $out_data[] = [
                    $no,
                    $coverage,
                    $related,
                    $li['partner_name']?:'-',
                    $order_num,
                    $policy,
                    $amount_list
                ];
            }
        
            $output = [
                "draw" => intval($draw),
                "iTotalRecords" => $totalRecords,
                "iTotalDisplayRecords" => $totalRecordswithFilter,
                "aaData" => $out_data,
            ];

            return response()->json($output, 200);
        }
    } 

    public function getBalance($amount, $order_id){
        $balance = 0;
        $total = PaymentDetail::leftJoin('payment', 'payment.payment_id', '=', 'payment_detail.paymentline_payment_id')
        ->where('payment.payment_status', '1')
        ->where('payment_detail.paymentline_status', '1')
        ->where('payment_detail.paymentline_order_id', $order_id)
        ->sum("payment_detail.paymentline_offset_amount");

        $balance = $amount - $total;
        return $balance;
    }

    public function PaymentInfo(Request $request){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        if($request->route('id')){
            $data = Payment::where('payment_id',$request->route('id'))
            ->where('payment_cprofile', env("APP_ID"))
            ->where('payment_to', 'Customer')
            ->where('payment_status', 1)
            ->first();
            if($data){
                $exist_dealer = TSA::find($data->payment_dealer);
                $exist_customer = Customer::find($data->payment_customer);
                $add_name = User::find($data->insertBy);
                $up_name = User::find($data->updateBy);
                $data->update_name =  $up_name->getName()?:'Webmaster';
                $data->insert_name =  $add_name->getName()?:'Webmaster';
                $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
                $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
                $data->customer_name = isset($exist_customer->partner_name)?$exist_customer->partner_name:'';
                $data->customer_ic = isset($exist_customer->partner_nirc)?substr_replace($exist_customer->partner_nirc,'XXXX', 1,-4):'';
                $data->payment_list = $this->getPaymentDetailList($request->route('id'), $data->payment_to);
                $file_attach = new FilesController();
                $data->file_list = $file_attach->getFilesList('ens_payment',$data->payment_id);
                $data->disabled = "";
                $data->insuranceco_list = $insuranceco_list;
                return view('payment.Info', $data);
            }
            else{
                return redirect()->route('PaymentList');
            }
        }
        else{
            $data = [
                'insuranceco_list'=> $insuranceco_list,
            ];
            return view('payment.Add', $data);
        }
    }

    public function PaymentViewInfo(Request $request){
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = Payment::where('payment_id',$request->route('id'))
            ->where('payment_cprofile', env("APP_ID"))
            ->where('payment_to', 'Customer')
            ->where('payment_status', 1)
            ->first();
        if($data){
            $exist_dealer = TSA::find($data->payment_dealer);
            $exist_customer = Customer::find($data->payment_customer);
            $add_name = User::find($data->insertBy);
            $up_name = User::find($data->updateBy);
            $data->update_name =  $up_name->getName()?:'Webmaster';
            $data->insert_name =  $add_name->getName()?:'Webmaster';
            $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
            $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
            $data->customer_name = isset($exist_customer->partner_name)?$exist_customer->partner_name:'';
            $data->customer_ic = isset($exist_customer->partner_nirc)?substr_replace($exist_customer->partner_nirc,'XXXX', 1,-4):'';
            $data->payment_list = $this->getPaymentDetailList($request->route('id'), $data->payment_to);
            $file_attach = new FilesController();
            $data->file_list = $file_attach->getFilesList('ens_payment',$data->payment_id);
            $data->disabled = "disabled";
            $data->insuranceco_list = $insuranceco_list;
            return view('payment.Info', $data);
        }
        else{
            return redirect()->route('PaymentList');
        }
    }

    public function AddOrderToList(Request $request){
        $html = "";
        foreach($request->input('order_case') as $ord_li){
            $old_post = $request->input('old_post')?:[];
            if(!in_array($ord_li, $old_post)){
                $order_data = Order::where('order_id',$ord_li)->where('order_status', 1)->first();
                if($order_data){
                    $customer_name = Customer::where('partner_id', $order_data->order_customer)->first();
                    if($order_data->order_prefix_type == 'GI'){
                        $gi_order = General::where('order_id',$ord_li)->first();
                        $cover_plan = InsurancePlan::find($gi_order->order_coverage);
                        $coverage = "-";
                        if($cover_plan){
                            $coverage = $cover_plan->insuranceclass_code;
                        }
                        $related = $gi_order->order_ins_doc_no;
                        $order_num = '<a href="'.route('GeneralInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gi_order->order_policyno;
                    }
                    elseif($order_data->order_prefix_type == 'GM'){
                        $gm_order = Maid::where('order_id',$ord_li)->first();
                        $coverage = $gm_order->order_coverage;
                        $related = $gm_order->order_maid_name;
                        $order_num = '<a href="'.route('MaidInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gm_order->order_policyno;
                    }
                    else{
                        $mi_order = Motor::where('order_id',$ord_li)->first();
                        $coverage = $mi_order->order_coverage;
                        $related = $mi_order->order_vehicles_no;
                        $policy = $mi_order->order_policyno;
                        if($li['order_prefix_type'] == 'CY'){
                            $order_num = '<a href="'.route('MotorInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        elseif($li['order_prefix_type'] == 'MT'){
                            $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        else{
                            $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $ord_li ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                    }

                    $amount_list = '';
                    if($order_data->order_doc_type == 'DN'){
                        if($request->input('payment_to') == 'TSA'){
                            $total = $order_data->order_payable_premium;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        elseif($request->input('payment_to') == 'Customer'){
                            $total = $order_data->order_payable_premium;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $order_data->order_payable_premium;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'Our commission % :'.$order_data->order_payable_ourcommpercent."%<br>";
                            $amount_list .= 'Comm of GST :'.($order_data->order_payable_ourcomm + $order_data->order_payable_gstcomm)."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                    }
                    else{
                        if($request->input('payment_to') == 'TSA'){
                            $total = $order_data->order_payable_premium;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."%<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        elseif($request->input('payment_to') == 'Customer'){
                            $total = $order_data->order_payable_premium;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $order_data->order_payable_premium;
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'Our commission % :'.$order_data->order_payable_ourcommpercent."%<br>";
                            $amount_list .= 'Comm of GST :'.($order_data->order_payable_ourcomm + $order_data->order_payable_gstcomm)."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                    }
                    
                    $html .= '<div id="order_box_'.$ord_li.'" class="row order_wrapper info_order_wrapper">';
                    $html .= '<div class="col-md-3">';
                    $html .= 'Coverage :'.$coverage.'<br>';
                    $html .= 'Related Info :'.$related.'<br>';
                    $html .= 'Issue Name :'.$customer_name->partner_name;
                    $html .= '</div>';
                    $html .= '<div class="col-md-3">';
                    $html .= 'Order No :'.$order_num.'<br>';
                    $html .= 'Policy Number :'.$policy;
                    $html .= '</div>';
                    $html .= '<div class="col-md-3">';
                    $html .=  $amount_list;
                    $html .= '<div class="mt-3">';
                    $html .= '<button type="button" class="del_ord_btn btn btn-danger" rel="'.$ord_li.'">Delete</button>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="col-md-3">';
                    $html .= '<input type="hidden" name="payment_list['.$ord_li.'][paymentline_id]" value="0">';
                    $html .= '<input type="hidden" name="payment_list['.$ord_li.'][order_id]" class="order_id_input" value="'.$ord_li.'">';
                    $html .= '<input type="hidden" name="payment_list['.$ord_li.'][balance]" value="'.$balance.'">';
                    $html .= '<input type="hidden" name="payment_list['.$ord_li.'][order_amount]" value="'.$total.'">';
                    $html .= '<input type="number" step="0.01" class="form-control offset_amt text-right" name="payment_list['.$ord_li.'][offset_amount]" value="'.$balance.'" >';
                    $html .= '</div>';
                    $html .= '</div>';
                }
            }
        }
        $output = [
            'html' => $html
        ];
        return response()->json($output, 200);
    }

    public function create(Request $request){
        $rules = [
            'payment_dealer' => 'required_if:payment_to,==,TSA',
            'payment_insuranco' => 'required_if:payment_to,==,InsuranceComp',
            'payment_customer' => 'required_if:payment_to,==,Customer',
            'payment_date' => 'required|date',
            'payment_paid' => 'required',
            'payment_files'    => 'nullable|mimes:jpg,bmp,png,pdf',
        ];

        $attr = [
            'payment_dealer' => 'TSA',
            'payment_insuranco' => 'Insurance Company',
            'payment_customer' => 'Customer',
            'payment_date' => 'Payment Date',
            'payment_paid' => 'Payment Amount',
            'payment_files' => 'Files upload',
        ];

        $request->validate($rules, [], $attr);

        $errors_input = [];

        $files_allow = ['jpg','bmp','png','pdf'];

        if ($request->hasFile('payment_files')) {
            foreach($request->file('payment_files') as $key => $fli){
                if($fli->getSize() > 5242880 ){
                    $errors_input['payment_files'] = 'Attachment Cannot over 5MB';
                }
                elseif(!in_array($fli->extension(),$files_allow)){
                    $errors_input['payment_files'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                }
            }
        }       

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $new_payment = new Payment();
        $new_payment->payment_no = $this->generateCode();
        $this->updateRefCode();
        $new_payment->payment_to = $request->input('payment_to');
        $new_payment->payment_type = 'customer';
        $new_payment->payment_dealer = $request->input('payment_dealer')?:0;
        $new_payment->payment_insuranco = $request->input('payment_insuranco')?:0;
        $new_payment->payment_customer = $request->input('payment_customer')?:0;
        $new_payment->payment_cprofile = env("APP_ID");
        $new_payment->payment_date = $request->input('payment_date');
        $new_payment->payment_bank = $request->input('payment_bank');
        $new_payment->payment_cheque = $request->input('payment_cheque');
        $new_payment->payment_method = $request->input('payment_method');
        $new_payment->payment_paid = $request->input('payment_paid');
        $new_payment->payment_remarks = $request->input('payment_remarks');
        $new_payment->payment_status = 1;
        $new_payment->payment_receipt_ref = 0;
        $new_payment->insertBy = Auth::user()->id;
        $new_payment->updateBy = Auth::user()->id;
        $new_payment->save();

        if($request->input('payment_list')){
            foreach($request->input('payment_list') as $pli){
                $new_paymentline = new PaymentDetail();
                $new_paymentline->paymentline_payment_id  = $new_payment->payment_id;
                $new_paymentline->paymentline_order_id  = $pli['order_id'];
                $new_paymentline->paymentline_order_amount  = $pli['order_amount'];
                $new_paymentline->paymentline_offset_amount  = $pli['offset_amount']?:0;
                $new_paymentline->paymentline_status = 1;
                $new_paymentline->insertBy = Auth::user()->id;
                $new_paymentline->updateBy = Auth::user()->id;
                $new_paymentline->save();
            }
        }

        if($request->hasFile('payment_files')){
            $file_ctrl = new FilesController();
            $file_ctrl->uploadMultipleFile($request,'ens_payment',$new_payment->payment_id,'uploads/payment','payment_files');
        }

        return redirect()->route('PaymentList')->with('success_msg', 'Add Case Succesfully');
    }

    public function update(Request $request){
        $exist_payment = Payment::where('payment_id',$request->input('payment_id'))
        ->where('payment_cprofile', env("APP_ID"))
        ->where('payment_status', 1)
        ->first();
        if(!$exist_payment){
            return redirect()->route('PaymentList')->with('error_msg', 'Invalid Case');
        }
        else{
            $rules = [
                'payment_dealer' => 'required_if:payment_to,==,TSA',
                'payment_insuranco' => 'required_if:payment_to,==,InsuranceComp',
                'payment_customer' => 'required_if:payment_to,==,Customer',
                'payment_date' => 'required|date',
                'payment_paid' => 'required',
            ];

            $attr = [
                'payment_dealer' => 'TSA',
                'payment_insuranco' => 'Insurance Company',
                'payment_customer' => 'Customer',
                'payment_date' => 'Payment Date',
                'payment_paid' => 'Payment Amount',
            ];

            $request->validate($rules, [], $attr);

            $errors_input = [];

            $files_allow = ['jpg','bmp','png','pdf'];

            if ($request->hasFile('payment_files')) {
                foreach($request->file('payment_files') as $key => $fli){
                    if($fli->getSize() > 5242880 ){
                        $errors_input['payment_files'] = 'Attachment Cannot over 5MB';
                    }
                    elseif(!in_array($fli->extension(),$files_allow)){
                        $errors_input['payment_files'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                    }
                }
            }     

            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }

            $exist_payment->payment_dealer = $request->input('payment_dealer')?:0;
            $exist_payment->payment_insuranco = $request->input('payment_insuranco')?:0;
            $exist_payment->payment_customer = $request->input('payment_customer')?:0;
            $exist_payment->payment_date = $request->input('payment_date');
            $exist_payment->payment_method = $request->input('payment_method');
            $exist_payment->payment_bank = $request->input('payment_bank');
            $exist_payment->payment_cheque = $request->input('payment_cheque');
            $exist_payment->payment_paid = $request->input('payment_paid');
            $exist_payment->payment_remarks = $request->input('payment_remarks');
            $exist_payment->updateBy = Auth::user()->id;
            $exist_payment->save();

            if($request->input('payment_list')){
                PaymentDetail::where('paymentline_payment_id', $exist_payment->payment_id)->update(['paymentline_status' => 0]);
                foreach($request->input('payment_list') as $pli){
                    if($pli['paymentline_id'] == 0){
                        $new_paymentline = new PaymentDetail();
                        $new_paymentline->paymentline_payment_id  = $new_payment->payment_id;
                        $new_paymentline->paymentline_order_id  = $pli['order_id'];
                        $new_paymentline->paymentline_order_amount  = $pli['order_amount'];
                        $new_paymentline->paymentline_offset_amount  = $pli['offset_amount']?:0;
                        $new_paymentline->paymentline_status = 1;
                        $new_paymentline->insertBy = Auth::user()->id;
                        $new_paymentline->updateBy = Auth::user()->id;
                        $new_paymentline->save();
                    }
                    else{
                        $exist_paymentline = PaymentDetail::find($pli['paymentline_id']);
                        $exist_paymentline->paymentline_offset_amount  = $pli['offset_amount']?:0;
                        $exist_paymentline->paymentline_status = 1;
                        $exist_paymentline->updateBy = Auth::user()->id;
                        $exist_paymentline->save();
                    }
                }
            }

            if($request->hasFile('payment_files')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_payment',$exist_payment->payment_id,'uploads/payment','payment_files');
            }

            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $exist_payment = Payment::find($request->route('id'));
        if($exist_payment){
            $exist_payment->order_status = "0";
            $exist_payment->save();
        }
       
        return redirect()->route('PaymentList')->with('success_msg', 'Delete Case Succesfully');
    }

    public function generateCode(){
        if(env("APP_ID") == 2){
            $prefix = "PE";
        }
        else{
            $prefix = "PV";
        }
        $ref = Refno::where('prefix_code', $prefix)->first();
        if($ref){
            $last_id = $ref->last_no;
            $last_id ++;
        }
        else{
            $new_ref = new Refno();
            $new_ref->prefix_code = $prefix;
            $new_ref->last_no = 0;
            $new_ref->save();
            $last_id = 1;
        }

        $code =  $prefix .'-'. sprintf("%06d", $last_id);

        $motor_code = Payment::where('payment_no',$code)->first();
        if($motor_code){
            $this->updateRefCode();
            return $this->generateCode();
        }
        else{
            return $code; 
        }
        
    }

    public function updateRefCode(){
        if(env("APP_ID") == 2){
            $prefix = "PE";
        }
        else{
            $prefix = "PV";
        }

        $ref = Refno::where('prefix_code', $prefix)->first();
        $ref->last_no =  $ref->last_no + 1;
        $ref->save();
    }

    public function getPaymentDetailList($payment_id, $pay_to){
        $py_dt = PaymentDetail::where('paymentline_payment_id', $payment_id)
                ->where('paymentline_status', 1)
                ->get()->toArray();
        $output_data = [];
        if($py_dt){
            foreach($py_dt as $py_ky => $py_li){
                $order_data = Order::where('order_id',$py_li['paymentline_order_id'])->where('order_status', 1)->first();
                if($order_data){
                    $customer_name = Customer::where('partner_id', $order_data->order_customer)->first();
                    if($order_data->order_prefix_type == 'GI'){
                        $gi_order = General::where('order_id',$py_li['paymentline_order_id'])->first();
                        $cover_plan = InsurancePlan::find($gi_order->order_coverage);
                        $coverage = "-";
                        if($cover_plan){
                            $coverage = $cover_plan->insuranceclass_code;
                        }
                        $related = $gi_order->order_ins_doc_no;
                        $order_num = '<a href="'.route('GeneralInfo', ['id' => $py_li['paymentline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gi_order->order_policyno;
                    }
                    elseif($order_data->order_prefix_type == 'GM'){
                        $gm_order = Maid::where('order_id',$py_li['paymentline_order_id'])->first();
                        $coverage = $gm_order->order_coverage;
                        $related = $gm_order->order_maid_name;
                        $order_num = '<a href="'.route('MaidInfo', ['id' => $py_li['paymentline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        $policy = $gm_order->order_policyno;
                    }
                    else{
                        $mi_order = Motor::where('order_id',$py_li['paymentline_order_id'])->first();
                        $coverage = $mi_order->order_coverage;
                        $related = $mi_order->order_vehicles_no;
                        $policy = $mi_order->order_policyno;
                        if($li['order_prefix_type'] == 'CY'){
                            $order_num = '<a href="'.route('MotorInfo', ['id' => $py_li['paymentline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        elseif($li['order_prefix_type'] == 'MT'){
                            $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $py_li['paymentline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                        else{
                            $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $py_li['paymentline_order_id'] ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                        }
                    }

                    $amount_list = '';
                    if($order_data->order_doc_type == 'DN'){
                        if($pay_to == 'TSA'){
                            $total = $py_li['paymentline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        elseif($pay_to == 'Customer'){
                            $total = $py_li['paymentline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $py_li['paymentline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'Our commission % :'.$order_data->order_payable_ourcommpercent."%<br>";
                            $amount_list .= 'Comm of GST :'.($order_data->order_payable_ourcomm + $order_data->order_payable_gstcomm)."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                    }
                    else{
                        if($pay_to == 'TSA'){
                            $total = $py_li['paymentline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                            $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."%<br>";
                            $amount_list .= 'Amount :'.($order_data->order_receivable_dealercomm + $order_data->order_receivable_dealergstamount)."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        elseif($pay_to == 'Customer'){
                            $total = $py_li['paymentline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                        else{
                            $total = $py_li['paymentline_order_amount'];
                            $balance = $this->getBalance($total, $order_data->order_id);
                            $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                            $amount_list .= 'Our commission % :'.$order_data->order_payable_ourcommpercent."%<br>";
                            $amount_list .= 'Comm of GST :'.($order_data->order_payable_ourcomm + $order_data->order_payable_gstcomm)."<br>";
                            $amount_list .= 'Premium payable :'.$order_data->order_payable_premium ."<br>";
                            $amount_list .= 'Balance :'.$balance;
                        }
                    }
                    
                    $output_data[] = [
                        'paymentline_id' => $py_li['paymentline_id'],
                        'order_id' => $py_li['paymentline_order_id'],
                        'coverage' => $coverage,
                        'related' => $related,
                        'customer_name' => $customer_name->partner_name,
                        'order_no' => $order_num,
                        'policy' => $policy,
                        'amount_list' => $amount_list,
                        'balance' => $balance,
                        'total' => $total,
                        'offset_amt' => $py_li['paymentline_offset_amount']
                    ];
                }
            }
        }

        return $output_data;
    }

    public function getOldCaseList($order_id, $balance, $offset, $order_amt, $paymentline_id, $payment_to){
        $html = "";
        $order_data = Order::where('order_id',$order_id)->where('order_status', 1)->first();

        if($order_data){
            $customer_name = Customer::where('partner_id', $order_data->order_customer)->first();
            if($order_data->order_prefix_type == 'GI'){
                $gi_order = General::where('order_id',$order_id)->first();
                $cover_plan = InsurancePlan::find($gi_order->order_coverage);
                $coverage = "-";
                if($cover_plan){
                    $coverage = $cover_plan->insuranceclass_code;
                }
                $related = $gi_order->order_ins_doc_no;
                $order_num = '<a href="'.route('GeneralInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                $policy = $gi_order->order_policyno;
            }
            elseif($order_data->order_prefix_type == 'GM'){
                $gm_order = Maid::where('order_id',$order_id)->first();
                $coverage = $gm_order->order_coverage;
                $related = $gm_order->order_maid_name;
                $order_num = '<a href="'.route('MaidInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                $policy = $gm_order->order_policyno;
            }
            else{
                $mi_order = Motor::where('order_id',$order_id)->first();
                $coverage = $mi_order->order_coverage;
                $related = $mi_order->order_vehicles_no;
                $policy = $mi_order->order_policyno;
                if($li['order_prefix_type'] == 'CY'){
                    $order_num = '<a href="'.route('MotorInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                }
                elseif($li['order_prefix_type'] == 'MT'){
                    $order_num = '<a href="'.route('CommercialMotorInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                }
                else{
                    $order_num = '<a href="'.route('PrivateMotorInfo', ['id' => $order_id ]).'" target="_blank" >'.$order_data->order_no.'</a>';
                }
            }

            $amount_list = '';
            if($order_data->order_doc_type == 'DN'){
                if($payment_to == 'TSA'){
                    $total = $order_amt;
                    $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                    $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                    $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."<br>";
                    $amount_list .= 'Amount :'. $total ."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
                elseif($payment_to == 'Customer'){
                    $total =  $order_amt;
                    $amount_list .= 'Premium Amount :'. $total ."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
                else{
                    $total =  $order_amt;
                    $amount_list .= 'Premium Amount :'.$order_data->order_grosspremium_amount."<br>";
                    $amount_list .= 'Our commission % :'.$order_data->order_payable_ourcommpercent."%<br>";
                    $amount_list .= 'Comm of GST :'.($order_data->order_payable_ourcomm + $order_data->order_payable_gstcomm)."<br>";
                    $amount_list .= 'Premium payable :'. $total ."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
            }
            else{
                if($request->input('payment_to') == 'TSA'){
                    $total =  $order_amt;
                    $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                    $amount_list .= 'TSA refer Fee % :'.$order_data->order_receivable_dealercommpercent."%<br>";
                    $amount_list .= 'TSA refer Fee Amount :'.$order_data->order_receivable_dealercomm."%<br>";
                    $amount_list .= 'Amount :'.$total."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
                elseif($request->input('payment_to') == 'Customer'){
                    $total =  $order_amt;
                    $amount_list .= 'Premium Amount :-'.$total ."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
                else{
                    $total =  $order_amt;
                    $amount_list .= 'Premium Amount :-'.$order_data->order_grosspremium_amount."<br>";
                    $amount_list .= 'Our commission % :'.$order_data->order_payable_ourcommpercent."%<br>";
                    $amount_list .= 'Comm of GST :'.($order_data->order_payable_ourcomm + $order_data->order_payable_gstcomm)."<br>";
                    $amount_list .= 'Premium payable :'.$total ."<br>";
                    $amount_list .= 'Balance :'.$balance;
                }
            }
            
            $html .= '<div id="order_box_'.$order_id.'" class="row order_wrapper info_order_wrapper">';
            $html .= '<div class="col-md-3">';
            $html .= 'Coverage :'.$coverage.'<br>';
            $html .= 'Related Info :'.$related.'<br>';
            $html .= 'Issue Name :'.$customer_name->partner_name;
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .= 'Order No :'.$order_num.'<br>';
            $html .= 'Policy Number :'.$policy;
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .=  $amount_list;
            $html .= '<div class="mt-3">';
            $html .= '<button type="button" class="del_ord_btn btn btn-danger" rel="'.$order_id.'">Delete</button>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .= '<input type="hidden" name="payment_list['.$order_id.'][paymentline_id]" value="'.$paymentline_id.'">';
            $html .= '<input type="hidden" name="payment_list['.$order_id.'][order_id]" class="order_id_input" value="'.$order_id.'">';
            $html .= '<input type="hidden" name="payment_list['.$order_id.'][balance]" value="'.$balance.'">';
            $html .= '<input type="hidden" name="payment_list['.$order_id.'][order_amount]" value="'.$total.'">';
            $html .= '<input type="number" step="0.01" class="form-control offset_amt text-right" name="payment_list['.$order_id.'][offset_amount]" value="'.$offset.'" >';
            $html .= '</div>';
            $html .= '</div>';
        }

        return $html;
    }

    public function getOrderPayment($order_id){
        $output = [];
        $main_query = PaymentDetail::select('payment_detail.*', 'payment.payment_no', 'payment.payment_date','payment.payment_paid' ,'payment.payment_bank', 'payment.payment_cheque' )
        ->leftJoin('payment', 'payment.payment_id', '=', 'payment_detail.paymentline_payment_id')
        ->where('payment.payment_status', 1)
        ->where('payment_detail.paymentline_status', 1)
        ->where('payment_detail.paymentline_order_id', $order_id)
        ->get()->toArray();
        $is_view = false;
        $is_edit = false;
        
        if(UserGroupPrivilegeController::checkPrivilegeWithAuth("PaymentInfo")){
            $is_edit = true;
        }
        
        if(UserGroupPrivilegeController::checkPrivilegeWithAuth("PaymentView")){
            $is_view = true;
        }

        foreach($main_query as $mli){
            $insert_name = User::find($mli['insertBy']);
            $file_attach = new FilesController();
            $file_list = $file_attach->getFilesList('ens_payment',$mli['paymentline_payment_id']);
            $output[] = [
                'payment_id' => $mli['paymentline_payment_id'],
                'payment_no' => $mli['payment_no'],
                'payment_date' => $mli['payment_date'],
                'payment_bank' => $mli['payment_bank'],
                'payment_cheque' => $mli['payment_cheque'],
                'payment_paid' => $mli['payment_paid'],
                'order_amount' => $mli['order_amount'],
                'insertBy' => $insert_name->getName(),
                'file_list' =>  $file_list ,
                'is_view' =>  $is_view ,
                'is_edit' =>  $is_edit ,
            ];
        }

        return $output;
    }
}
