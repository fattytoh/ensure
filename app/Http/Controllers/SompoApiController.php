<?php

namespace App\Http\Controllers;

use App\Models\Sompo;
use App\Models\SompoApi;
use App\Models\SompoData;
use App\Models\SompoDriver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SompoApiController extends Controller
{
    //
    public static $demo_api_link = array(
        'sompo_token_url' => 'http://uat.sompo.com.sg:2082/ssotoken',
        'sompo_qoutation_url' => 'http://uat.sompo.com.sg:2052/getQuote',
        'sompo_proposal_url' => 'http://uat.sompo.com.sg:2052/getProposal',
        'sompo_prolicy_create_url' => 'http://uat.sompo.com.sg:2052/createPolicy',
        'sompo_prolicy_lookup_url' => 'http://uat.sompo.com.sg:2052/getLookupValue',
    );
    
    public static $live_api_link = array(
        'sompo_token_url' => 'https://api.sompo.com.sg:2096/ssotoken',
        'sompo_qoutation_url' => 'https://api.sompo.com.sg:2087/getQuote',
        'sompo_proposal_url' => 'https://api.sompo.com.sg:2087/getProposal',
        'sompo_prolicy_create_url' => 'https://api.sompo.com.sg:2087/createPolicy',
        'sompo_prolicy_lookup_url' => 'https://api.sompo.com.sg:2087/getLookupValue',
    );
    
    protected  static $live_account_pass = array(
        'ecics'=>'DSF!@sf123',
        'sompo' => 'Ensure!ltd01@PRD',
    );
    
    protected  static $demo_account_pass = array(
        'ecics'=>'DSF!@sf123',
        'sompo' => 'Densurettl01',
    );
    
    protected  static $live_account = array(
        'ecics'=>'ecics@gmail.com',
        'sompo' => 'ENSURE',
    );
    
    protected  static $demo_account = array(
        'ecics'=>'ecics@gmail.com',
        'sompo' => 'ENSURE',
    );

    private $application_id = '';
    private $ssl_verify = '';
    private $user_id = '';
    private $policy_code = '';
    private $enviroment = 'LIVE';

    public function __construct()
    {
        if($this->enviroment  == 'LIVE'){
            $this->api_link = static::$live_api_link;
            $this->api_account = static::$live_account;
            $this->api_password = static::$live_account_pass;
            $this->ssl_verify = false;
            $this->application_id = "ENSR";
            $this->user_id = "B2B-013";
            $this->policy_code = "ENS07913";
        }
        else{
            $this->api_link = static::$demo_api_link;
            $this->api_account = static::$demo_account;
            $this->api_password = static::$demo_account_pass;
            $this->ssl_verify = false;
            $this->application_id = "ENSR";
            $this->user_id = "B2B-013";
            $this->policy_code = "ENS07913";
        }
    }

    public function recordApiActivity($input_data){
        $api_act = new SompoApi();
        $api_act->api_type = $input_data['type'];
        $api_act->api_ref = $input_data['ref'];
        $api_act->api_header_data = $input_data['header'];
        $api_act->api_link = $input_data['link'];
        $api_act->api_send_method = $input_data['method'];
        $api_act->api_send_method = $input_data['data'];
        $api_act->api_receive_response_code = $input_data['http_code'];
        $api_act->api_receive_header_data = $input_data['http_header'];
        $api_act->api_receive_data = $input_data['http_response'];
        $api_act->api_status = $input_data['status'];
        $api_act->insertBy = Auth::user()->id;
        $api_act->updateBy = Auth::user()->id;
        $api_act->save();
    }

    public function getSompoApiToken(){
        $post_link = $this->api_link['sompo_token_url'];
        $header = array(
            'Content-Type:application/x-www-form-urlencoded',
            'Accept:application/json',
        );
        $post_data = array(
            'grant_type=password',
            'client_id=fuse-safe-endpoint',
            'username='.$this->api_account['sompo'],
            'password='.$this->api_password['sompo'],
        );
        
        $curl = curl_init($post_link);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, implode("&",$post_data));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->ssl_verify); 
        $curl_response = curl_exec($curl);
        $header_data = curl_getinfo($curl);
        $response = json_decode($curl_response, true);
        curl_close($curl);
        
        if($header_data['http_code'] != 200){
            $api_status = 'Fail';
        }
        else{
            if($response['access_token']){
                $api_status = 'Success';
            }
            else{
                $api_status = 'Fail';
            }
        }
        
        $record_data = array(
            'type' => 'out',
            'ref' => '',
            'header' => json_encode($header),
            'link' => $post_link,
            'method' => 'POST',
            'data' => json_encode($post_data),
            'http_code' => $header_data['http_code'],
            'http_header' => json_encode($header_data),
            'http_response' => $curl_response,
            'status' => $api_status,
        );
        $this->recordApiActivity($record_data);
        return isset($response['access_token'])?$response['access_token']:"";
    }

    public function SompoLookUp($type,$single = false, $single_data=""){
        $response = '';
        $token = $this->getSompoApiToken();
        $post_link = $this->api_link['sompo_prolicy_lookup_url'];
        if($token){
            
            if(!$single){
                $flag = "EC_TYPE";
            }
            else{
                $flag = "SINGLE_LOOKUP";
            }
            
            $header = array(
                'Content-Type:application/json',
                'Authorization: Bearer '.$token,
            );
            
            $post_data = array(
                'applicationId' => $this->application_id,
                'userId' => $this->user_id,
                'serviceId' => "getLov",
                'requestUniqueId' => "ENSRLOKUP-".strtotime(system_datetime),
                'serviceRequestObject' => array(
                    'LOVRQMESSAGE' => array(
                        'ec_flag' => $flag,
                        'ec_type' => $type,
                        'ec_code' => $single_data,
                    )
                )
            );
            
            $curl = curl_init($post_link);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->ssl_verify); 
            $curl_response = curl_exec($curl);
            $header_data = curl_getinfo($curl);
            $response = json_decode($curl_response, true);
            curl_close($curl);
            
            if($header_data['http_code'] != 200){
                $api_status = 'Fail';
            }
            else{
                if($response['statusCd'] == '01'){
                    $api_status = 'Success';
                }
                else{
                    $api_status = 'Fail';
                }
            }
            
            $record_data = array(
                'type' => 'out',
                'ref' => $post_data['requestUniqueId'],
                'header' => json_encode($header),
                'link' => $post_link,
                'method' => 'POST',
                'data' => json_encode($post_data),
                'http_code' => $header_data['http_code'],
                'http_header' => json_encode($header_data),
                'http_response' => $curl_response,
                'status' => $api_status,
            );
            
            $this->recordApiActivity($record_data);
            
        }
        
        return $response;
    }

    public function SompoGetQoutation(){
        $token = $this->getSompoApiToken();
        $post_link = $this->api_link['sompo_qoutation_url'];
        $response = '';
        
        if($token){
            $header = array(
                'Content-Type:application/json',
                'Authorization: Bearer '.$token,
            );
            
            foreach($this->driver_list as $dv_li){
                $driver_list[] = array(
                    "DRV_SEX"=>$dv_li['gender'],
                    "DRV_EXP"=>$dv_li['exp'],
                    "DRV_OCC_CODE"=>$dv_li['occupation_code'],
                    "DRV_RATING_APPL_YN"=>$dv_li['is_app'], 
                    "DRV_DOB"=>$this->SompoDateFormat($dv_li['dob']),
                    "NOOFCLAIMS"=>intval($dv_li['claim']),
                    "TOTALCLAIMAMT"=>intval($dv_li['claim_amount']),
                    "DRV_TRAF_POINTS"=>$dv_li['point'],
                    "DRV_MAR_STATUS"=>$dv_li['marital_status'],
                );
            }
           
            $post_data = array(
                "applicationId"=>$this->application_id,
                "userId"=>$this->user_id,
                "serviceId"=>"getQuote",
                "requestUniqueId"=>$this->requestUniqueId,
                "serviceRequestObject"=>array(
                    "CURRENTPOLICY"=>array(
                        "POL_GENDER"=>$this->POL_GENDER,
                        "VOLUNTARYEXCESSLIST"=>array(),
                        "POL_FM_DT"=>$this->SompoDateFormat($this->POL_FM_DT),
                        "POL_SUB_PRD_CODE"=>$this->policy_code,
                        "POL_MAR_STATUS"=>$this->POL_MAR_STATUS,
                        "DRV_VALID_LIC_YN"=>$this->DRV_VALID_LIC_YN,
                        "MOTOROFD"=>$this->MOTOROFD,
                        "OPTIONALCOVERAGELIST"=>array(),
                        "POL_RATING_EFF_DT"=>$this->SompoDateFormat($this->POL_RATING_EFF_DT,true),
                        "POL_SRC_BUS_CODE"=>"MTMC01",
                        "XCOUNTRY_BIKE" => $this->XCOUNTRY_BIKE,
                        "HIRE_REWARD" => $this->HIRE_REWARD,
                        "FEMALE_DISC" => $this->FEMALE_DISC,
                        "OFD_YN" => $this->OFD_YN,
                        "VEHICLEINFO"=>array(
                            "VEH_INSURED_DRIVING_YN"=>$this->VEH_INSURED_DRIVING_YN,
                            "VEH_COE_FLG"=>$this->VEH_COE_FLG,
                            "VEH_CERT_EXP_DT"=>null,
                            "NCD_DISC_CODE"=>$this->NCD_DISC_CODE,
                            "VEH_CC"=>$this->VEH_CC, 
                            "VEH_NO_PASS"=>$this->VEH_NO_PASS,  
                            "VEH_GRADE"=>$this->VEH_GRADE,  
                            "VEH_MODEL_CODE"=>$this->VEH_MODEL_CODE,  
                            "IS_NAMEDRIVERS_TERMS_AGREED"=>$this->IS_NAMEDRIVERS_TERMS_AGREED,
                            "VEH_BODY_TYPE"=>$this->VEH_BODY_TYPE, 
                            "VEH_NO_DRV"=>count($this->driver_list),
                            "VEH_REGN_YEAR"=>$this->VEH_REGN_YEAR,
                            "VEH_NCD_PROT_ELIG"=>$this->VEH_NCD_PROT_ELIG,
                            "VEH_SPOUSE_DRIVING_YN"=>$this->VEH_SPOUSE_DRIVING_YN,
                            "VEH_YEAR"=>$this->VEH_YEAR,
                            "VEH_ONT_PARKING"=>$this->VEH_ONT_PARKING,
                            "VEH_REGN_NO"=>$this->VEH_REGN_NO,
                            "REASON_FOR_NCD"=>$this->REASON_FOR_NCD,
                            "VEH_MAKE_CODE"=>$this->VEH_MAKE_CODE,
                            "VEH_PRV_CLM_FREE_YR"=>$this->VEH_PRV_CLM_FREE_YR,
                            "VEH_HP_CODE" => $this->VEH_HP_CODE,
                            "VEH_HP_DTLS" => $this->VEH_HP_DTLS,
                            "VEH_NO_INEXPER_DRIVER" => $this->VEH_NO_INEXPER_DRIVER,
                        ),
                        "NO_OF_PERSONS"=>$this->NO_OF_PERSONS,
                        "TRIP_TYPE"=>$this->TRIP_TYPE,
                        "DRIVERSLIST"=>$driver_list,
                        "POL_TO_DT"=>$this->SompoDateFormat($this->POL_TO_DT),
                        "POL_OCC_CODE"=>$this->POL_OCC_CODE,
                        "POL_OCC_DESC"=>$this->POL_OCC_DESC,
                        "POL_DOB"=>$this->SompoDateFormat($this->POL_DOB),
                        "PROMO_CODE"=>$this->PROMO_CODE,
                        "POL_SC_CODE"=>"MTMC01",
                        "NOOFCLAIMS"=>$this->NOOFCLAIMS,
                        "TOTALCLAIMAMT"=>$this->TOTALCLAIMAMT,
                        "MOTORISEXCESSEXISTS"=>"",
                    )
                )
            );
            
            $curl = curl_init($post_link);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->ssl_verify); 
            $curl_response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $allinfo = curl_getinfo($curl);
            $response = json_decode($curl_response, true);
            curl_close($curl);
            
            if($httpcode != 200){
                $api_status = 'Fail';
            }
            else{
                if($response['statusCd'] == '01'){
                    $api_status = 'Success';
                }
                else{
                    $api_status = 'Fail';
                }
            }
            
            $record_data = array(
                'type' => 'out',
                'ref' => $post_data['requestUniqueId'],
                'header' => json_encode($header),
                'link' => $post_link,
                'method' => 'POST',
                'data' => json_encode($post_data),
                'http_code' => $httpcode,
                'api_receive_header_data' => $allinfo,
                'http_response' => $curl_response,
                'status' => $api_status,
            );
            $this->recordApiActivity($record_data);
        }
        
        return $response;
    }
    
    public function SompoGetProposal(){
        $token = $this->getSompoApiToken();
        $post_link = $this->api_link['sompo_proposal_url'];
        $response = '';

        if($token){
            $header = array(
                'Content-Type:application/json',
                'Authorization: Bearer '.$token,
            );
            foreach($this->driver_list as $dv_li){
                $driver_list[] = array(
                    "DRV_CITIZENSHIP" => $dv_li['nirc_type'],
                    "DRV_RELATION" => $dv_li['relation'],
                    "DRV_ID" => $dv_li['enc_nirc'],
                    "DRV_NAME" =>$dv_li['enc_name'],
                    "DRV_SEX"=>$dv_li['gender'],
                    "DRV_EXP"=>$dv_li['exp'],
                    "DRV_OCC_CODE"=>strtoupper($dv_li['occupation_code']),
                    "DRV_OCC_DESC"=>strtoupper($dv_li['other_occupation']),
                    "DRV_RATING_APPL_YN"=>$dv_li['is_app'],
                    "DRV_DOB"=>$this->SompoDateFormat($dv_li['dob']),
                    "NOOFCLAIMS"=>intval($dv_li['claim']),
                    "TOTALCLAIMAMT"=>intval($dv_li['claim_amount']),
                    "DRV_TRAF_POINTS"=>$dv_li['point'],
                    "DRV_MAR_STATUS"=>$dv_li['marital_status'],
                );
            }
            $post_data = array(
                "applicationId"=>$this->application_id,
                "serviceId"=>"getProposal",
                "userId"=>$this->user_id,
                "requestUniqueId"=>$this->requestUniqueId,
                "serviceRequestObject"=>array(
                    "CURRENTPOLICY"=>array(
                        "APPQUOTEPOLICYREFID" => $this->APPQUOTEPOLICYREFID,
                        "POL_SC_CODE" => 'MTMC01',
                        "POL_FM_DT" => $this->SompoDateFormat($this->POL_FM_DT),
                        "POL_TO_DT" => $this->SompoDateFormat($this->POL_TO_DT),
                        "POL_SUB_PRD_CODE" => $this->policy_code,
                        "POL_ASSURED_NAME" => $this->POL_ASSURED_NAME,
                        "POL_INSURED_ID" => $this->POL_INSURED_ID,
                        "POL_IDENTITY_TYPE" => $this->POL_IDENTITY_TYPE,
                        "POL_NATIONALITY" => $this->POL_NATIONALITY,
                        "POL_DOB" => $this->SompoDateFormat($this->POL_DOB),
                        "POL_GENDER" => $this->POL_GENDER,
                        "POL_EMAIL" => $this->POL_EMAIL,
                        "POL_ADDR1" => $this->POL_ADDR1,
                        "POL_ADDR2" => $this->POL_ADDR2,
                        "POL_ADDR3" => null,
                        "POL_POSTAL_CODE" => $this->POL_POSTAL_CODE,
                        "POL_OCC_CODE" => $this->POL_OCC_CODE,
                        "POL_OCC_DESC" => $this->POL_OCC_DESC,
                        "POL_PHONE" => $this->POL_PHONE,
                        "POL_RATING_EFF_DT" => $this->SompoDateFormat($this->POL_RATING_EFF_DT,true),
                        "POL_WP_TXN_ID" => null,
                        "NO_OF_PERSONS" => count($this->driver_list),
                        "TRIP_TYPE" => $this->TRIP_TYPE,
                        "PROMO_CODE" => $this->PROMO_CODE,
                        "POL_MAR_STATUS" => $this->POL_MAR_STATUS,
                        "DRV_VALID_LIC_YN" => $this->DRV_VALID_LIC_YN,
                        "NOOFCLAIMS" => $this->NOOFCLAIMS,
                        "TOTALCLAIMAMT" => $this->TOTALCLAIMAMT,
                        "MOTOROFD" => $this->MOTOROFD,
                        "POL_PREMIUMBEFORETAX" => $this->POL_PREMIUMBEFORETAX,
                        "POL_PLANTAXAMOUNT" => $this->POL_PLANTAXAMOUNT,
                        "POL_PLANTOTALPREMIUM" => $this->POL_PLANTOTALPREMIUM,
                        "XCOUNTRY_BIKE" => $this->XCOUNTRY_BIKE,
                        "HIRE_REWARD" => $this->HIRE_REWARD,
                        "PREVIOUSREGISTRATIONNO" => $this->PREVIOUSREGISTRATIONNO,
                        "FEMALE_DISC" => $this->FEMALE_DISC,
                        "OFD_YN" => $this->OFD_YN,
                        "CUSTOM_OBJECT" => array(
                            "POL_PYMT_MODE" => $this->POL_PYMT_MODE,
                            "POL_NO_OF_INST" => $this->POL_NO_OF_INST,
                        ),
                        "PLANLIST" => $this->SEL_PLANLIST,
                        "OPTIONALCOVERAGELIST"=>array(),
                        "VEHICLEINFO" => array(
                            "VEH_INSURED_DRIVING_YN"=>$this->VEH_INSURED_DRIVING_YN,
                            "VM_BODY_DESC" => $this->VM_BODY_DESC,
                            "VEH_COE_FLG"=>$this->VEH_COE_FLG,
                            "NCD_DISC_CODE"=>$this->NCD_DISC_CODE,
                            "VEH_CC"=>$this->VEH_CC,
                            "VEH_NO_PASS"=>$this->VEH_NO_PASS,
                            "VEH_GRADE"=>$this->VEH_GRADE,
                            "VEH_MODEL_CODE"=>$this->VEH_MODEL_CODE,
                            "IS_NAMEDRIVERS_TERMS_AGREED"=>$this->IS_NAMEDRIVERS_TERMS_AGREED,
                            "VEH_BODY_TYPE"=>$this->VEH_BODY_TYPE,
                            "VEH_NO_DRV"=>count($this->driver_list),
                            "VEH_REGN_YEAR"=>$this->VEH_REGN_YEAR,
                            "VEH_NCD_PROT_ELIG"=>$this->VEH_NCD_PROT_ELIG,
                            "VEH_SPOUSE_DRIVING_YN"=>$this->VEH_SPOUSE_DRIVING_YN,
                            "VEH_YEAR"=>$this->VEH_YEAR,
                            "VEH_ONT_PARKING"=>$this->VEH_ONT_PARKING,
                            "VEH_REGN_NO"=>$this->VEH_REGN_NO,
                            "REASON_FOR_NCD"=>$this->REASON_FOR_NCD,
                            "VEH_MAKE_CODE"=>$this->VEH_MAKE_CODE,
                            "VEH_ENGINE_NO"=>$this->VEH_ENGINE_NO,
                            "VEH_PRV_CLM_FREE_YR"=>$this->VEH_PRV_CLM_FREE_YR,
                            "VEH_CHASSIS_NO" => $this->VEH_CHASSIS_NO,
                            "VEH_HP_CODE" => $this->VEH_HP_CODE,
                            "VEH_HP_DTLS" => $this->VEH_HP_DTLS,
                            "NCD_DISC_CODE" => $this->NCD_DISC_CODE,
                            "VEH_NO_INEXPER_DRIVER" => $this->VEH_NO_INEXPER_DRIVER,
                        ),
                        "DRIVERSLIST" =>$driver_list
                    )
                )
            );
            
            $curl = curl_init($post_link);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
            $curl_response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $allinfo = curl_getinfo($curl);
            $response = json_decode($curl_response, true);
            curl_close($curl);
            if($httpcode != 200){
                $api_status = 'Fail';
            }
            else{
                if($response['statusCd'] == '01'){
                    $api_status = 'Success';
                }
                else{
                    $api_status = 'Fail';
                }
            }
            
            $record_data = array(
                'type' => 'out',
                'ref' => $post_data['requestUniqueId'],
                'header' => json_encode($header),
                'link' => $post_link,
                'method' => 'POST',
                'data' => json_encode($post_data),
                'http_code' => $httpcode,
                'api_receive_header_data' => $allinfo,
                'http_response' => $curl_response,
                'status' => $api_status,
            );
            $this->recordApiActivity($record_data);
        }
        
        return $response;
    }
    
    public function SompoCreatePolicy(){
        $token = $this->getSompoApiToken();
        $post_link = $this->api_link['sompo_prolicy_create_url'];
        $response = '';
        
        if($token){
            $header = array(
                'Content-Type:application/json',
                'Authorization: Bearer '.$token,
            );
            foreach($this->driver_list as $dv_li){
                $driver_list[] = array(
                    "DRV_CITIZENSHIP" => $dv_li['nirc_type'],
                    "DRV_RELATION" => $dv_li['relation'],
                    "DRV_ID" => $dv_li['enc_nirc'],
                    "DRV_NAME" =>$dv_li['enc_name'],
                    "DRV_SEX"=>$dv_li['gender'],
                    "DRV_EXP"=>$dv_li['exp'],
                    "DRV_OCC_CODE"=>strtoupper($dv_li['occupation_code']),
                    "DRV_OCC_DESC"=>strtoupper($dv_li['other_occupation']),
                    "DRV_RATING_APPL_YN"=>$dv_li['is_app'],
                    "DRV_DOB"=>$this->SompoDateFormat($dv_li['dob']),
                    "NOOFCLAIMS"=>intval($dv_li['claim']),
                    "TOTALCLAIMAMT"=>intval($dv_li['claim_amount']),
                    "DRV_TRAF_POINTS"=>$dv_li['point'],
                    "DRV_MAR_STATUS"=>$dv_li['marital_status'],
                );
            }
            $post_data = array(
                "applicationId"=>$this->application_id,
                "serviceId"=>"createPolicy",
                "userId"=>$this->user_id,
                "requestUniqueId"=>$this->requestUniqueId,
                "serviceRequestObject"=>array(
                    "CURRENTPOLICY"=>array(
                        "APPQUOTEPOLICYREFID" => $this->APPQUOTEPOLICYREFID,
                        "POL_AGENT_REF_NO" => $this->POL_AGENT_REF_NO,
                        "POL_SC_CODE" => 'MTMC01',
                        "POL_FM_DT" => $this->SompoDateFormat($this->POL_FM_DT),
                        "POL_TO_DT" => $this->SompoDateFormat($this->POL_TO_DT),
                        "POL_SUB_PRD_CODE" => $this->policy_code,
                        "POL_ASSURED_NAME" => $this->POL_ASSURED_NAME,
                        "POL_INSURED_ID" => $this->POL_INSURED_ID,
                        "POL_IDENTITY_TYPE" => $this->POL_IDENTITY_TYPE,
                        "POL_NATIONALITY" => $this->POL_NATIONALITY,
                        "POL_DOB" => $this->SompoDateFormat($this->POL_DOB),
                        "POL_GENDER" => $this->POL_GENDER,
                        "POL_EMAIL" => $this->POL_EMAIL,
                        "POL_ADDR1" => $this->POL_ADDR1,
                        "POL_ADDR2" => $this->POL_ADDR2,
                        "POL_ADDR3" => null,
                        "POL_POSTAL_CODE" => $this->POL_POSTAL_CODE,
                        "POL_OCC_CODE" => $this->POL_OCC_CODE,
                        "POL_OCC_DESC" => $this->POL_OCC_DESC,
                        "POL_PHONE" => $this->POL_PHONE,
                        "POL_RATING_EFF_DT" => $this->SompoDateFormat($this->POL_RATING_EFF_DT,true),
                        "POL_WP_TXN_ID" => null,
                        "NO_OF_PERSONS" => count($this->driver_list),
                        "TRIP_TYPE" => $this->TRIP_TYPE,
                        "PROMO_CODE" => $this->PROMO_CODE,
                        "POL_MAR_STATUS" => $this->POL_MAR_STATUS,
                        "DRV_VALID_LIC_YN" => $this->DRV_VALID_LIC_YN,
                        "NOOFCLAIMS" => $this->NOOFCLAIMS,
                        "TOTALCLAIMAMT" => $this->TOTALCLAIMAMT,
                        "MOTOROFD" => $this->MOTOROFD,
                        "XCOUNTRY_BIKE" => $this->XCOUNTRY_BIKE,
                        "HIRE_REWARD" => $this->HIRE_REWARD,
                        "FEMALE_DISC" => $this->FEMALE_DISC,
                        "OFD_YN" => $this->OFD_YN,
                        "PREVIOUSREGISTRATIONNO" => $this->PREVIOUSREGISTRATIONNO,
                        "POL_PREMIUMBEFORETAX" => $this->POL_PREMIUMBEFORETAX,
                        "POL_PLANTAXAMOUNT" => $this->POL_PLANTAXAMOUNT,
                        "POL_PLANTOTALPREMIUM" => $this->POL_PLANTOTALPREMIUM,
                        "CUSTOM_OBJECT" => array(
                            "POL_PYMT_MODE" => $this->POL_PYMT_MODE,
                            "POL_NO_OF_INST" => $this->POL_NO_OF_INST,
                        ),
                        "PLANLIST" => $this->SEL_PLANLIST,
                        "OPTIONALCOVERAGELIST"=>array(),
                        "VEHICLEINFO" => array(
                            "VEH_INSURED_DRIVING_YN"=>$this->VEH_INSURED_DRIVING_YN,
                            "VM_BODY_DESC" => $this->VM_BODY_DESC,
                            "VEH_COE_FLG"=>$this->VEH_COE_FLG,
                            "NCD_DISC_CODE"=>$this->NCD_DISC_CODE,
                            "VEH_CC"=>$this->VEH_CC,
                            "VEH_NO_PASS"=>$this->VEH_NO_PASS,
                            "VEH_GRADE"=>$this->VEH_GRADE,
                            "VEH_MODEL_CODE"=>$this->VEH_MODEL_CODE,
                            "IS_NAMEDRIVERS_TERMS_AGREED"=>$this->IS_NAMEDRIVERS_TERMS_AGREED,
                            "VEH_BODY_TYPE"=>$this->VEH_BODY_TYPE,
                            "VEH_NO_DRV"=>count($this->driver_list),
                            "VEH_REGN_YEAR"=>$this->VEH_REGN_YEAR,
                            "VEH_NCD_PROT_ELIG"=>$this->VEH_NCD_PROT_ELIG,
                            "VEH_SPOUSE_DRIVING_YN"=>$this->VEH_SPOUSE_DRIVING_YN,
                            "VEH_YEAR"=>$this->VEH_YEAR,
                            "VEH_ONT_PARKING"=>$this->VEH_ONT_PARKING,
                            "VEH_REGN_NO"=>$this->VEH_REGN_NO,
                            "REASON_FOR_NCD"=>$this->REASON_FOR_NCD,
                            "VEH_MAKE_CODE"=>$this->VEH_MAKE_CODE,
                            "VEH_ENGINE_NO"=>$this->VEH_ENGINE_NO,
                            "VEH_PRV_CLM_FREE_YR"=>$this->VEH_PRV_CLM_FREE_YR,
                            "VEH_CHASSIS_NO" => $this->VEH_CHASSIS_NO,
                            "VEH_HP_CODE" => $this->VEH_HP_CODE,
                            "VEH_HP_DTLS" => $this->VEH_HP_DTLS,
                            "NCD_DISC_CODE" => $this->NCD_DISC_CODE,
                            "VEH_NO_INEXPER_DRIVER" => $this->VEH_NO_INEXPER_DRIVER,
                        ),
                        "DRIVERSLIST" =>$driver_list
                    )
                )
            );
            
            $curl = curl_init($post_link);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->ssl_verify); 
            $curl_response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $allinfo = curl_getinfo($curl);
            $response = json_decode($curl_response, true);
            curl_close($curl);
            
            if($httpcode != 200){
                $api_status = 'Fail';
            }
            else{
                if($response['statusCd'] == '01'){
                    $api_status = 'Success';
                }
                else{
                    $api_status = 'Fail';
                }
            }
            
            $record_data = array(
                'type' => 'out',
                'ref' => $post_data['requestUniqueId'],
                'header' => json_encode($header),
                'link' => $post_link,
                'method' => 'POST',
                'data' => json_encode($post_data),
                'http_code' => $httpcode,
                'api_receive_header_data' => $allinfo,
                'http_response' => $curl_response,
                'status' => $api_status,
            );
            $this->recordApiActivity($record_data);
        }
        return $response;
    }

    public function checkSompoDataExist($type, $pro_code ,$code){
        $exist_data = SompoData::select('sompo_id')
        ->where("sompo_ec_type", $type)
        ->where("sompo_ref_code", $code)
        ->where("sompo_ec_type_code", $pro_code)
        ->first();
        
        if($exist_data->sompo_id){
            return $exist_data->sompo_id;
        }

        return 0;
    }

    public function SompoDateFormat($orignal_date, $timezone = false){
        $strdatetime = strtotime($orignal_date);
        if($timezone){
            $final_date = date("Y-m-d\TH:i:s.000+08:00",$strdatetime);
        }
        else{
            $final_date = date("Y-m-d\TH:i:s",$strdatetime);
        }
        return $final_date;
    }
    
    public function SompoRmoveSyntax($data_array){
        foreach($data_array as $ky => $li){
            if(is_array($li)){
                $data_array[$ky] = $this->SompoRmoveSyntax($li);
            }
            else{
                $data_array[$ky] = trim(preg_replace('/\s\s+/', ' ', $li));
            }
        }
        return $data_array;
    }
    
    public function getSompoPlanClassList(){
        $sompo_ec_type = "PLAN_CLASS";
        $order_field = "order_coverage";
        $sompoo_field = "PLANCODE";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['Plan_Class_Code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Plan_Class_Code'],
                        'sompo_display_name' => $opli['Plan_Class_Name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoRelationshipList(){
        $sompo_ec_type = "RELATIONSHIP";
        $order_field = "driver_relationship";
        $sompoo_field = "DRV_RELATION";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['Product_Code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Relationship_Code'],
                        'sompo_display_name' => $opli['Relationship_Name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoVehicleList(){
        $sompo_ec_type = "PQ_VEHICLE_MATRIX";
        $order_field = "vehicle";
        $sompoo_field = "VEHICLEINFO";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['VM_BODY_DESC'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['VM_MODEL_CODE'],
                        'sompo_display_name' => $opli['VM_MODEL_DESC'],
                        'sompo_original_data' => escape(json_encode($opli)),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoOccupationList(){
        $sompo_ec_type = "OCCUPATION_LIST";
        $order_field = "driver_occupation";
        $sompoo_field = "DRV_OCC_CODE";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['ec_type_code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Occupation_code'],
                        'sompo_display_name' => $opli['Occupation_name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoPolicyList(){
        $sompo_ec_type = "POLICY_TYPE";
        $order_field = "order_policy_type";
        $sompoo_field = "TRIP_TYPE";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['Product_Code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Policy_Type_Code'],
                        'sompo_display_name' => $opli['Policy_Type_Name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoNationList(){
        $sompo_ec_type = "NATIONALITY";
        $order_field = "order_country";
        $sompoo_field = "POL_NATIONALITY";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['ec_type_code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Nationality_Code'],
                        'sompo_display_name' => $opli['Nationality_Name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoIdentityType(){
        $sompo_ec_type = "IDENTITY_TYPE";
        $order_field = "partner_type";
        $sompoo_field = "POL_IDENTITY_TYPE";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['Product_Code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Identity_Type_Code'],
                        'sompo_display_name' => $opli['Identity_Type_Name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoNCDReasonList(){
        $sompo_ec_type = "NCD_REASON_LIST";
        $order_field = "order_ncd_remark";
        $sompoo_field = "REASON_FOR_NCD";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['ec_type_code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['NCD_code'],
                        'sompo_display_name' => $opli['NCD_name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
               
            }
        }
    }
    
    public function getSompoOFDList(){
        $sompo_ec_type = "OFD_LIST";
        $order_field = "order_ofd_dis_per";
        $sompoo_field = "MOTOROFD";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['Product_Code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['OFD_Percentage'],
                        'sompo_display_name' => $opli['NCD_Percentage'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoOptionCoverList(){
        $sompo_ec_type = "OPTIONAL_COVERAGE_LIST";
        $order_field = "order_option_cover";
        $sompoo_field = "OPTIONALCOVERAGELIST";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['Product_Code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['OPT_Coverage_code'],
                        'sompo_display_name' => $opli['OPT_Coverage_name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoPreInsList(){
        $sompo_ec_type = "PREVIOUS_INS_COMP";
        $order_field = "order_ncd_previousinsurer";
        $sompoo_field = "PREVIOUSINSURER";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['ec_type_code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Insurer_code'],
                        'sompo_display_name' => $opli['Insurer_name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoMaritialStatus(){
        $sompo_ec_type = "MARITIAL_STATUS";
        $order_field = "marital_status";
        $sompoo_field = "DRV_MAR_STATUS";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['ec_type_code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['Maritial_code'],
                        'sompo_display_name' => $opli['Maritial_name'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
                
            }
        }
    }
    
    public function getSompoNCDList(){
        $sompo_ec_type = "MT_NCD";
        $order_field = "order_ncd_dis_per";
        $sompoo_field = "NCD_DISC_CODE";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['Product_Code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['NCD_DISC_CODE'],
                        'sompo_display_name' => $opli['NCD_PERCENTAGE'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
    
    public function getSompoHirePurcahseList(){
        $sompo_ec_type = "HP_NAME";
        $order_field = "order_financecode";
        $sompoo_field = "VEH_HP_CODE";
        
        $output = $this->SompoLookUp($sompo_ec_type);
        $output = $output['serviceReplyObject']['LOVRPMESSAGE']['ec_single_resultList'];
        
        if($output){
            foreach($output as $opky => $opli){
                $opli = $this->SompoRmoveSyntax($opli);
                SompoData::updateOrCreate(
                    [
                        'sompo_ec_type' => $sompo_ec_type, 
                        'sompo_ref_code' => $opli['ec_type_code'], 
                        'sompo_ec_type_code' => $opli['ec_type_code']
                    ],
                    [
                        'sompo_field_name' => $sompoo_field,
                        'sompo_order_field_name' => $order_field,
                        'sompo_data_value' => $opli['VEH_HP_CODE'],
                        'sompo_display_name' => $opli['VEH_HP_DTLS'],
                        'sompo_original_data' => json_encode($opli),
                        'sompo_data_status' => 1,
                        'updateBy'=> Auth::user()->id, 
                        'insertBy' => Auth::user()->id
                    ]
                );
            }
        }
    }
}
