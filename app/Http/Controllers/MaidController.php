<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Maid;
use App\Models\Order;
use App\Models\Country;
use App\Models\Customer;
use App\Models\TSA;
use App\Models\User;
use App\Models\Insuranceco;
use App\Models\Refno;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MaidController extends Controller
{
    //
    public $prefix = "GM";
    public $order_prefix = "GMM";

    public function getListing(Request $request){
        return view('maid.List');
    }

    public function getListData(Request $request){
         // Total records
        $order_fields = ['','order_no','order_maid_name','partner_name','order_date', ''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $count_query = Order::select('count(*) as allcount')
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('insurance_maid', 'insurance_maid.order_id', '=', 'order.order_id')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_prefix_type',$this->order_prefix)
        ->where('order.order_status', "!=","0");

        $main_query = Order::select('order.*', "customer.partner_name", "insurance_maid.order_maid_name")
        ->leftJoin('tsa', 'tsa.dealer_id', '=', 'order.order_dealer')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->leftJoin('insurance_maid', 'insurance_maid.order_id', '=', 'order.order_id')
        ->where('order.order_cprofile', env('APP_ID'))
        ->where('order.order_prefix_type',$this->order_prefix)
        ->where('order.order_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('order.order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('insurance_maid.order_maid_name', 'like', '%' . $searchValue . '%')
                ->orWhere('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('order.order_date', 'like', '%' . $searchValue . '%');
        });

        if($request->input('order_policyno')){
            $main_query->Where('insurance_maid.order_policyno', 'like', '%' . $request->input('order_policyno') . '%');
            $count_query->Where('insurance_maid.order_policyno', 'like', '%' . $request->input('order_policyno'). '%');
        }

        if($request->input('order_period_from')){
            $main_query->Where('order.order_date', '>=', $request->input('order_period_from'));
            $count_query->Where('order.order_date', '>=', $request->input('order_period_from'));
        }

        if($request->input('order_period_to')){
            $main_query->Where('order.order_date', '<=', $request->input('order_period_to'));
            $count_query->Where('order.order_date', '<=', $request->input('order_period_to'));
        }

        if($request->input('order_maid_name')){
            $main_query->Where('insurance_maid.order_maid_name', 'like', '%' . $request->input('order_maid_name') . '%');
            $count_query->Where('insurance_maid.order_maid_name', 'like', '%' . $request->input('order_maid_name'). '%');
        }

        if($request->input('dealer_name')){
            $main_query->Where('tsa.dealer_name', 'like', '%' . $request->input('dealer_name') . '%');
            $count_query->Where('tsa.dealer_name', 'like', '%' . $request->input('dealer_name'). '%');
        }
        
        if($request->input('partner_nirc')){
            $main_query->Where('customer.partner_nirc', 'like', '%' . $request->input('partner_nirc') . '%');
            $count_query->Where('customer.partner_nirc', 'like', '%' . $request->input('partner_nirc'). '%');
        }

        if($request->input('partner_name')){
            $main_query->Where('customer.partner_name', 'like', '%' . $request->input('partner_name') . '%');
            $count_query->Where('customer.partner_name', 'like', '%' . $request->input('partner_name'). '%');
        }

        $totalRecords = $count_query->count();
        $totalRecordswithFilter = $count_query
        ->where(function ($query) use ($searchValue) {
            $query->where('order.order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('insurance_maid.order_maid_name', 'like', '%' . $searchValue . '%')
                ->orWhere('customer.partner_name', 'like', '%' . $searchValue . '%')
                ->orWhere('order.order_date', 'like', '%' . $searchValue . '%');
        })
        ->count();

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }

        $listing_data = $main_query
        ->skip($start)
        ->take($rowperpage)
        ->get()->toArray();

        $out_data = [];

        foreach($listing_data as $ky => $li){
            $action = '';

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("MaidInfo")){
                $action .= '<a href="'.route('MaidInfo', ['id' => $li['order_id'] ]).'" class="btn btn-warning">Edit</a>';
            }

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("MaidDelete")){
                $action .= '<a href="'.route('MaidDelete', ['id' => $li['order_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['order_no'].'">Delete</a>';
            }
            
            $out_data[] = [
                $ky + 1,
                $li['order_no']?:'-',
                $li['order_maid_name'],
                $li['partner_name']?:'-',
                $li['order_date']?:'-',
                $action
            ];
        }
       
        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200);
    }

    public function MaidInfo(Request $request){
        $opt = new OptionController();
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        if($request->route('id')){
            $info_data = $this->getMaidInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                return view('maid.Info', $info_data);
            }
            else{
                return redirect()->route('MaidList');
            }
        }
        else{
            $data = [
                'country_list'=> $country_list,
                'gov_gst' =>  $opt->getMaidGST(),
                'insuranceco_list'=> $insuranceco_list,
                'min_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(70))),
                'max_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(18))),
                'min_year' => date('Y', strtotime(Carbon::now()->subYears(30))),
            ];
            return view('maid.Add', $data);
        }
    }

    public function getMaidInfo($order_id){
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        $insuranceco_list = Insuranceco::select("insuranceco_id", "insuranceco_code", "insuranceco_name")->where("insuranceco_status", 1)->where("insuranceco_cprofile", env('APP_ID'))->orderBy('insuranceco_seq', 'ASC')->get()->toArray();
        $data = Order::select('order.*', 'insurance_maid.*' )
        ->leftJoin('insurance_maid', 'insurance_maid.order_id', '=', 'order.order_id')
        ->where('order.order_id', $order_id)
        ->where('order.order_status', "!=","0")
        ->where('order.order_prefix_type',$this->order_prefix)
        ->first();
        if(isset($data->order_id)){
            $exist_dealer = TSA::find($data->order_dealer);
            $exist_customer = Customer::find($data->order_customer);
            $add_name = User::find($data->insertBy);
            $up_name = User::find($data->updateBy);
            $data->update_name =  $up_name->getName()?:'Webmaster';
            $data->insert_name =  $add_name->getName()?:'Webmaster';
            $data->dealer_id = $data->order_dealer;
            $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
            $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
            $data->customer_name = isset($exist_customer->partner_name)?$exist_customer->partner_name:'';
            $data->customer_ic = isset($exist_customer->partner_nirc)?substr_replace($exist_customer->partner_nirc,'XXXX', 1,-4):'';
            $data->financecode_name = isset($exist_hires->id)?$exist_hires->comp_name:'';
            $data->min_age = date('Y-m-d', strtotime(Carbon::now()->subYears(70)));
            $data->max_age = date('Y-m-d', strtotime(Carbon::now()->subYears(18)));
            $data->min_year = date('Y', strtotime(Carbon::now()->subYears(30)));
            $file_attach = new FilesController();
            $data->file_list = $file_attach->getFilesList('ens_order',$data->order_id);
            $receipt = new ReceiptController();
            $data->receipt_list = $receipt->getOrderReceipt($data->order_id);
            $pay = new PaymentController();
            $data->payment_list = $pay->getOrderPayment($data->order_id);
            $data->country_list = $country_list;
            $data->record_list = $this->getRelatedRecord($data->order_id);
            $data->insuranceco_list = $insuranceco_list;
            
            return $data;
        }
    }

    public function MaidRenewal(Request $request){
        if($request->route('id')){
            $info_data = $this->getMaidInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                $info_data->order_no = "-- System Generate --";
                $info_data->order_ref = 0;
                $info_data->order_renewal_ref = $info_data->order_id;
                $info_data->order_premium_amt = 0;
                $info_data->order_addtional_amt = 0;
                $info_data->order_subtotal_amount = 0;
                $info_data->order_direct_discount_amt = 0;
                $info_data->order_direct_discount_percentage = 0;
                $info_data->order_cover_plan_no_gst = 0;
                $info_data->order_bank_interest = 0;
                $info_data->order_grosspremium_amount = 0;
                $info_data->order_receivable_dealercomm = 0;
                $info_data->order_receivable_dealergstamount = 0;
                $info_data->order_receivable_gstamount = 0;
                $info_data->order_receivable_premiumreceivable = 0;
                $info_data->order_payable_ourcomm = 0;
                $info_data->order_payable_nettcomm = 0;
                $info_data->order_payable_gst = 0;
                $info_data->order_payable_premium = 0;
                $info_data->order_date = date('Y-m-d');
                $info_data->order_period_from = date('Y-m-d');
                $info_data->order_period_to = date('Y-m-d', strtotime(date('Y-m-d'). " +14 months"));
                $info_data->order_period_to = date('Y-m-d', strtotime($info_data->order_period_to. " -1 day"));

                return view('maid.Renewal', $info_data);
            }
            else{
                return redirect()->route('MaidList');
            }
        }
        else{
            return redirect()->route('MaidList');
        }
    }

    public function MaidEndorsement(Request $request){
        if($request->route('id')){
            $info_data = $this->getMaidInfo($request->route('id'));
            if($info_data){
                $info_data->disabled = "";
                $info_data->order_no = "-- System Generate --";
                $info_data->order_ref = $info_data->order_id;
                $info_data->order_renewal_ref = 0;
                $info_data->order_premium_amt = 0;
                $info_data->order_addtional_amt = 0;
                $info_data->order_subtotal_amount = 0;
                $info_data->order_direct_discount_amt = 0;
                $info_data->order_direct_discount_percentage = 0;
                $info_data->order_cover_plan_no_gst = 0;
                $info_data->order_bank_interest = 0;
                $info_data->order_grosspremium_amount = 0;
                $info_data->order_receivable_dealercomm = 0;
                $info_data->order_receivable_dealergstamount = 0;
                $info_data->order_receivable_gstamount = 0;
                $info_data->order_receivable_premiumreceivable = 0;
                $info_data->order_payable_ourcomm = 0;
                $info_data->order_payable_nettcomm = 0;
                $info_data->order_payable_gst = 0;
                $info_data->order_payable_premium = 0;
                $info_data->order_date = date('Y-m-d');
                $info_data->order_period_from = date('Y-m-d');
                $info_data->order_period_to = date('Y-m-d', strtotime(date('Y-m-d'). " +14 months"));
                $info_data->order_period_to = date('Y-m-d', strtotime($info_data->order_period_to. " -1 day"));

                return view('maid.Endorsement', $info_data);
            }
            else{
                return redirect()->route('MaidList');
            }
        }
        else{
            return redirect()->route('MaidList');
        }
    }

    public function create(Request $request){
        $rules = [
            'order_record_billto' => 'required',
            'order_period_from' => 'required|date|after_or_equal:today',
            'order_period_to' => 'required|date|after:order_period_from',
            'order_date' => 'required|date',
            'order_insurco' => 'required',
            'order_coverage' => 'required',
            'order_premium_amt' => 'required|gt:0',
        ];

        $attr = [
            'order_record_billto' => 'Bill To',
            'order_period_from' => 'Coverage Period From',
            'order_period_to' => 'Coverage Period To',
            'order_date' => 'Issue Date',
            'order_insurco' => 'Insurance Company',
            'order_coverage' => 'Coverage Package',
        ];

        $request->validate($rules, [], $attr);

        $errors_input = [];

        $files_allow = ['jpg','bmp','png','pdf'];

        if ($request->hasFile('order_files_attach')) {
            foreach($request->file('order_files_attach') as $key => $fli){
                if($fli->getSize() > 5242880 ){
                    $errors_input['order_files_attach'] = 'Attachment Cannot over 5MB';
                }
                elseif(!in_array($fli->extension(),$files_allow)){
                    $errors_input['order_files_attach'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                }
            }
        }

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $order_ctrl = new OrderController();
        $new_order_id = $order_ctrl->create($request, $this->order_prefix, $this->prefix);

        if($new_order_id){
            $new_maid = new Maid();
            $new_maid->order_record_billto = $request->input('order_record_billto');
            $new_maid->order_method = $request->input('order_method');
            $new_maid->order_empl_sbtran_ref = $request->input('order_empl_sbtran_ref');
            $new_maid->order_policyno = $request->input('order_policyno');
            $new_maid->order_main_proposal = $request->input('order_main_proposal');
            $new_maid->order_period_from = $request->input('order_period_from');
            $new_maid->order_period_to = $request->input('order_period_to');
            $new_maid->order_period = $request->input('order_period');
            $new_maid->order_discharge_date = $request->input('order_discharge_date');
            $new_maid->order_coverage = $request->input('order_coverage');
            $new_maid->order_waiver_indemnity = $request->input('order_waiver_indemnity');
            $new_maid->order_optional_coverage_plan = $request->input('order_optional_coverage_plan');
            $new_maid->order_optional_coverage_plan1 = $request->input('order_optional_coverage_plan1');
            $new_maid->order_cancel_date = $request->input('order_cancel_date');
            $new_maid->order_cancel_reason = $request->input('order_cancel_reason');
            $new_maid->order_ins_doc_no = $request->input('order_ins_doc_no');
            $new_maid->order_notes_remark = $request->input('order_notes_remark');
            $new_maid->order_exclude_option = $request->input('order_exclude_option');
            $new_maid->order_maid_passno = $request->input('order_maid_passno');
            $new_maid->order_maid_country = $request->input('order_maid_country');
            $new_maid->order_maid_name = $request->input('order_maid_name');
            $new_maid->order_maid_permit = $request->input('order_maid_permit');
            $new_maid->order_maid_dob = $request->input('order_maid_dob');
            $new_maid->order_cli_postcode = $request->input('order_cli_postcode');
            $new_maid->order_cli_blkno = $request->input('order_cli_blkno');
            $new_maid->order_cli_street = $request->input('order_cli_street');
            $new_maid->order_cli_country = $request->input('order_cli_country');
            $new_maid->order_cli_address = $request->input('order_cli_address');
            $new_maid->order_cli_telresidence = $request->input('order_cli_telresidence');
            $new_maid->order_cli_teloffice = $request->input('order_cli_teloffice');
            $new_maid->order_cli_telmobile = $request->input('order_cli_telmobile');
            $new_maid->order_cli_email = $request->input('order_cli_email');
            $new_maid->order_id = $new_order_id;
            $new_maid->insertBy = Auth::user()->id;
            $new_maid->updateBy = Auth::user()->id;
            $new_maid->save();

            if($request->hasFile('order_files_attach')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_order',$new_order_id,'uploads/maid','order_files_attach');
            }
        }
        else{
            return redirect()->route('MaidList')->with('error_msg', 'System Error, Please Contact Admin');
        }

        return redirect()->route('MaidList')->with('success_msg', 'Add Case Succesfully');
    }

    public function update(Request $request){
        $exist_order = Order::where('order_id', $request->input('order_id'))
            ->where('order_status', "!=","0")
            ->first();
        $exist_maid = Maid::where('order_id', $request->input('order_id'))
            ->first();

        if(!$exist_order || !$exist_maid){
            return redirect()->route('MaidList')->with('error_msg', 'Invalid Case');
        }
        else{
            $rules = [
                'order_record_billto' => 'required',
                'order_period_from' => 'required|date|after_or_equal:today',
                'order_period_to' => 'required|date|after:order_period_from',
                'order_date' => 'required|date',
                'order_insurco' => 'required',
                'order_coverage' => 'required',
                'order_premium_amt' => 'required|gt:0',
            ];
    
            $attr = [
                'order_record_billto' => 'Bill To',
                'order_period_from' => 'Coverage Period From',
                'order_period_to' => 'Coverage Period To',
                'order_date' => 'Issue Date',
                'order_insurco' => 'Insurance Company',
                'order_coverage' => 'Coverage Package',
            ];
    
            $request->validate($rules, [], $attr);
    
            $errors_input = [];

            $files_allow = ['jpg','bmp','png','pdf'];

            if ($request->hasFile('order_files_attach')) {
                foreach($request->file('order_files_attach') as $key => $fli){
                    if($fli->getSize() > 5242880 ){
                        $errors_input['order_files_attach'] = 'Attachment Cannot over 5MB';
                    }
                    elseif(!in_array($fli->extension(),$files_allow)){
                        $errors_input['order_files_attach'] = 'The Files upload field must be a file of type: jpg, bmp, png, pdf.';
                    }
                }
            }
    
            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }
            
            $order_ctrl = new OrderController();
            $order_ctrl->update($request);
    
            $exist_maid->order_record_billto = $request->input('order_record_billto');
            $exist_maid->order_method = $request->input('order_method');
            $exist_maid->order_empl_sbtran_ref = $request->input('order_empl_sbtran_ref');
            $exist_maid->order_policyno = $request->input('order_policyno');
            $exist_maid->order_main_proposal = $request->input('order_main_proposal');
            $exist_maid->order_period_from = $request->input('order_period_from');
            $exist_maid->order_period_to = $request->input('order_period_to');
            $exist_maid->order_period = $request->input('order_period');
            $exist_maid->order_discharge_date = $request->input('order_discharge_date');
            $exist_maid->order_coverage = $request->input('order_coverage');
            $exist_maid->order_waiver_indemnity = $request->input('order_waiver_indemnity');
            $exist_maid->order_optional_coverage_plan = $request->input('order_optional_coverage_plan');
            $exist_maid->order_optional_coverage_plan1 = $request->input('order_optional_coverage_plan1');
            $exist_maid->order_cancel_date = $request->input('order_cancel_date');
            $exist_maid->order_cancel_reason = $request->input('order_cancel_reason');
            $exist_maid->order_ins_doc_no = $request->input('order_ins_doc_no');
            $exist_maid->order_notes_remark = $request->input('order_notes_remark');
            $exist_maid->order_exclude_option = $request->input('order_exclude_option');
            $exist_maid->order_maid_passno = $request->input('order_maid_passno');
            $exist_maid->order_maid_country = $request->input('order_maid_country');
            $exist_maid->order_maid_name = $request->input('order_maid_name');
            $exist_maid->order_maid_permit = $request->input('order_maid_permit');
            $exist_maid->order_maid_dob = $request->input('order_maid_dob');
            $exist_maid->order_cli_postcode = $request->input('order_cli_postcode');
            $exist_maid->order_cli_blkno = $request->input('order_cli_blkno');
            $exist_maid->order_cli_street = $request->input('order_cli_street');
            $exist_maid->order_cli_country = $request->input('order_cli_country');
            $exist_maid->order_cli_address = $request->input('order_cli_address');
            $exist_maid->order_cli_telresidence = $request->input('order_cli_telresidence');
            $exist_maid->order_cli_teloffice = $request->input('order_cli_teloffice');
            $exist_maid->order_cli_telmobile = $request->input('order_cli_telmobile');
            $exist_maid->order_cli_email = $request->input('order_cli_email');
            $exist_maid->updateBy = Auth::user()->id;
            $exist_maid->save();

            if($request->hasFile('order_files_attach')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_order',$request->input('order_id'),'uploads/maid','order_files_attach');
            }
            
            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $exist_order = Order::find($request->route('id'));
        if($exist_order){
            $exist_order->order_status = "0";
            $exist_order->save();
        }
       
        return redirect()->route('MaidList')->with('success_msg', 'Delete Case Succesfully');
    }

    public function getRelatedRecord($order_id){
        $output = [];
        $current_order = Order::select("order_ref", "order_renewal_ref")
        ->where('order_status', 1)
        ->where('order_id', $order_id)
        ->first();

        $main_order = 0;

        if($current_order->order_ref > 0){
            $main_order = $current_order->order_ref;
        }
        elseif($current_order->order_renewal_ref > 0){
            $main_order = $current_order->order_renewal_ref;
        }
        
        $main_query = Order::select('order.*', "customer.partner_name", "insurance_maid.order_maid_name", "insurance_maid.order_coverage", "insurance_maid.order_maid_name","insurance_maid.order_period_from","insurance_maid.order_period_to", "insurance_maid.order_policyno")
        ->leftJoin('insurance_maid', 'insurance_maid.order_id', '=', 'order.order_id')
        ->leftJoin('customer', 'customer.partner_id', '=', 'order.order_customer')
        ->where('order.order_status', 1)
        ->where(function ($query) use ($order_id, $main_order) {
            $query->where('order.order_ref',  $order_id )
                ->orWhere('order.order_renewal_ref', $order_id)
                ->orWhere('order.order_id', $main_order);
        })->get()->toArray();

        foreach($main_query as $mli){
            $order_no = '';

            if(UserGroupPrivilegeController::checkPrivilegeWithAuth("MaidInfo")){
                $order_no = '<a href="'.route('MaidInfo', ['id' => $mli['order_id'] ]).'" target="_blank">'.$mli['order_no'].'</a>';
            }
            else{
                $order_no = $mli['order_no'];
            }

            $output[] = [
                'order_id' => $mli['order_id'],
                'order_no' => $order_no,
                'order_doc_type' => $mli['order_doc_type'],
                'order_grosspremium_amount' => $mli['order_grosspremium_amount'],
                'order_maid_name' => $mli['order_maid_name'],
                'order_coverage' => $mli['order_coverage'],
                'partner_name' => $mli['partner_name'],
                'order_period_from' => $mli['order_period_from'],
                'order_period_to' => $mli['order_period_to'],
                'order_policyno' => $mli['order_policyno'],
            ];
        }

        return $output;
    }

}
