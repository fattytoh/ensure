<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TSA;
use App\Models\User;
use App\Models\Empl;
use App\Models\Files;
use App\Models\Group;
use App\Models\CProfile;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class TsaController extends Controller
{
    //
    public $user_code = "TSA";

    public function getListing(Request $request){

        $listing_data = TSA::select('tsa.*', 'empl.empl_name')->leftJoin('empl', 'empl.empl_id', '=', 'tsa.insertBy')->get()->toArray();
        
        $data = [
            'list_data' => $listing_data,
        ];
        return view('tsa.List', $data);
    }

    public function getListData(Request $request){
        // Total records
       $order_fields = ['','partner_name','partner_nirc','partner_tel','partner_house_no',''];
       $draw = $request->input('draw');
       $start = $request->input("start");
       $rowperpage = $request->input("length")?:10;
       $searchValue = $request->input("search")['value'];

       if($rowperpage > 100){
           $rowperpage = 100;
       }

       $totalRecords = Customer::select('count(*) as allcount')->where('partner_status', "!=","0")->count();
       $totalRecordswithFilter = Customer::select('count(*) as allcount')
       ->where(function ($query) use ($searchValue) {
           $query->where('partner_name', 'like', '%' . $searchValue . '%')
               ->orWhere('partner_tel', 'like', '%' . $searchValue . '%')
               ->orWhere('partner_house_no', 'like', '%' . $searchValue . '%')
               ->orWhere('partner_nirc', 'like', '%' . $searchValue . '%');
       })
       ->where('partner_status', "!=","0")->count();
       $listing_data = [];

       $main_query = Customer::where('partner_status', "!=","0")
       ->where(function ($query) use ($searchValue) {
           $query->where('partner_name', 'like', '%' . $searchValue . '%')
               ->orWhere('partner_tel', 'like', '%' . $searchValue . '%')
               ->orWhere('partner_house_no', 'like', '%' . $searchValue . '%')
               ->orWhere('partner_nirc', 'like', '%' . $searchValue . '%');
       })
       ->where('partner_status', "!=","0");

       foreach($request->input("order") as $or_li){
           if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
               $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
           }
       }
      
       $listing_data = $main_query->skip($start)
       ->take($rowperpage)
       ->get()->toArray();
       $out_data = [];

       foreach($listing_data as $ky => $li){
           $action = '';
           $action .= '<a href="'.route('CustomerInfo', ['id' => $li['partner_id'] ]).'" class="btn btn-warning">Edit</a>';
           $action .= '<a href="'.route('CustomerDelete', ['id' => $li['partner_id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['partner_name'].'">Delete</a>';
           
           $out_data[] = [
               $ky + 1,
               $li['partner_name']?:'-',
               substr_replace($li['partner_nirc'],'XXXX', 1,-4),
               $li['partner_tel']?:'-',
               $li['partner_house_no']?:'-',
               $action
           ];
       }

        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200); 
   }

    public function TSAInfo(Request $request){
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        $group_list = Group::select("group_id", "group_code")->where("group_status", 1)->where("group_type", "TSA")->get()->toArray();
        $cprofile_list = CProfile::select("cprofile_name", "cprofile_id")->get()->toArray();
        if($request->route('id')){
            $data = TSA::where('dealer_id',$request->route('id'))->where('dealer_status', '1')->first();
            if(isset($data->dealer_id)){
                $add_name = Empl::find($data->insertBy);
                $up_name = Empl::find($data->updateBy);
                $data->update_name =  isset($up_name->empl_name)?$up_name->empl_name:'Webmaster';
                $data->insert_name =  isset($add_name->empl_name)?$add_name->empl_name:'Webmaster';
                $data->attachment =  $this->getAttachment($request->route('id'));
                $data->country_list =  $country_list;
                $data->group_list =  $group_list;
                $data->cprofile_list =  $cprofile_list;
                $dusr = User::where('ref', $data->dealer_id)->where('ref_code', $this->user_code)->first();
                if($dusr){
                    $data->login_email =  $dusr->email;
                    $data->login_status =  $dusr->status;
                    if(env("APP_ID") == '2'){
                        $data->group_id = $dusr->ens_group_id;
                        $data->cprofile = $dusr->cprofile;
                    }
                    else{
                        $data->group_id = $dusr->peo_group_id;
                        $data->cprofile = $dusr->cprofile;
                    }
                }
                else{
                    $data->login_email = "";
                    $data->login_status = 0;
                    $data->group_id = 0;
                    $data->cprofile = 0;
                }
                return view('tsa.Info', $data);
            }
            else{
                return redirect()->route('TSAList');
            }
        }
        else{
            
            $data = [
                'country_list' => $country_list,
                'cprofile_list' => $cprofile_list,
                'group_list' => $group_list,
            ];
            return view('tsa.Add', $data);
        }
    }

    public function create(Request $request){
        $rules = [
            'dealer_code'    => 'required|min:3|unique:tsa,dealer_code',
            'dealer_name'    => 'required|min:3',
            'login_email'    => 'nullable|unique:users,tsa,email',
            'password'    => 'required_with:login_email|min:5',
            'cfm_password'    => 'same:password',
            'dealer_email'    => 'required|email|unique:tsa,dealer_email',
            'dealer_file'    => 'nullable|mimes:jpg,bmp,png,pdf',
        ];

        $attr = [ 
            'dealer_code'    => 'Dealer Account Code',
            'dealer_name'    => 'Dealer Name',
            'login_email'    => 'Login Email',
            'password'    => 'Login Password',
            'cfm_password'    => 'Confirm Login Password',
            'dealer_email'    => 'Dealer Email',
            'dealer_file'    => 'File Attachment',
        ];

        $request->validate($rules, [], $attr);
        $errors_input = [];

        if ($request->hasFile('dealer_file')) {
            if($request->file('dealer_file')->getSize() > 5242880 ){
                $errors_input['dealer_file'] = 'Attachment Cannot over 5MB';
            }
        }

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }
               
        $new_tsa = new TSA();
        $new_tsa->dealer_code = $request->input('dealer_code')?:'';
        $new_tsa->dealer_name = $request->input('dealer_name')?:'';
        $new_tsa->dealer_category = $request->input('dealer_category')?:'';
        $new_tsa->dealer_gst_reg = $request->input('dealer_gst_reg')?:'0';
        $new_tsa->dealer_blk_no = $request->input('dealer_blk_no')?:'';
        $new_tsa->dealer_street = $request->input('dealer_street')?:'';
        $new_tsa->dealer_unit_no = $request->input('dealer_unit_no')?:'';
        $new_tsa->dealer_country = $request->input('dealer_country')?:'0';
        $new_tsa->dealer_postalcode = $request->input('dealer_postalcode')?:'';
        $new_tsa->dealer_contact_name = $request->input('dealer_contact_name')?:'';
        $new_tsa->dealer_payableto = $request->input('dealer_payableto')?:'';
        $new_tsa->dealer_status = $request->input('dealer_status')?:'0';
        $new_tsa->dealer_telo = $request->input('dealer_telo')?:'';
        $new_tsa->dealer_telm = $request->input('dealer_telm')?:'';
        $new_tsa->dealer_email = $request->input('dealer_email')?:'';
        $new_tsa->dealer_remark = $request->input('dealer_remark')?:'';
        if($request->input('password') != ''){
            $new_tsa->dealer_cprofile = '0';
        }
        else{
            $new_tsa->dealer_cprofile = $request->input('cprofile');
        }
        $new_tsa->insertBy = Auth::user()->id;
        $new_tsa->updateBy = Auth::user()->id;
        $new_tsa->save();

        if($request->input('login_email') != ''){
            $new_userbase = new User();
            $new_userbase->ref = $new_tsa->dealer_id;
            $new_userbase->email = $request->input('login_email');
            if(env("APP_ID") == '2'){
                $new_userbase->ens_group_id = $request->input('group_id');
            }
            else{
                $new_userbase->peo_group_id = $request->input('group_id');
            }
            
            $new_userbase->ref_code = $this->user_code;
            $new_userbase->cprofile = $request->input('cprofile');
            $new_userbase->password = Hash::make($request->input('password'));
            $new_userbase->status = $request->input('login_status');
            $new_userbase->save();
        }

        if($request->hasFile('dealer_file')){
            $file_ctrl = new FilesController();
            $file_ctrl->uploadMultipleFile($request,'ens_tsa',$new_tsa->dealer_id,'uploads/tsa','dealer_file');
        }
        return redirect()->route('TSAList')->with('success_msg', 'Add TSA Succesfully');
    }

    public function update(Request $request){
        $exist_tsa = TSA::find($request->input('dealer_id'));
        $exist_usr = User::where('ref',$request->input('dealer_id'))->where('ref_code', $this->user_code)->first();

        if(!$exist_tsa){
            return redirect()->route('TSAList')->with('error_msg', 'Invalid TSA');
        }
        else{
            $rules = [
                'dealer_code'    => 'required|min:3|unique:tsa,dealer_code,'.$request->input('dealer_id').',dealer_id',
                'dealer_name'    => 'required|min:3',
                'password'    => 'nullable|min:5',
                'cfm_password'    => 'same:password',
                'dealer_email'    => 'required|email|unique:tsa,dealer_email,'.$request->input('dealer_id').',dealer_id',
                'dealer_file'    => 'nullable|mimes:jpg,bmp,png,pdf',
            ];

            $attr = [ 
                'dealer_code'    => 'Dealer Account Code',
                'dealer_name'    => 'Dealer Name',
                'login_email'    => 'Login Email',
                'password'    => 'Login Password',
                'cfm_password'    => 'Confirm Login Password',
                'dealer_email'    => 'Dealer Email',
                'dealer_file'    => 'File Attachment',
            ];
    
            $request->validate($rules, [], $attr);
            $errors_input = [];
            $filename = "";
            if ($request->hasFile('dealer_file')) {
                if($request->file('dealer_file')->getSize() > 5242880 ){
                    $errors_input['dealer_file'] = 'Attachment Cannot over 5MB';
                }
            }

            if($exist_usr && $request->input('login_email')){
                $uniqe_email = User::where('email',$request->input('login_email'))->where('id', '!=', $exist_usr->id)->first();
                if($uniqe_email){
                    $errors_input['login_email'] = 'Login Email Exist';
                }
            }
            elseif($request->input('login_email')){
                $uniqe_email = User::where('email',$request->input('login_email'))->first();
                if($uniqe_email){
                    $errors_input['login_email'] = 'Login Email Exist';
                }
                elseif(!$request->input('password')){
                    $errors_input['login_email'] = 'Login password is requeired';
                }
            }

            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }
                
            $exist_tsa->dealer_code = $request->input('dealer_code')?:'';
            $exist_tsa->dealer_name = $request->input('dealer_name')?:'';
            $exist_tsa->dealer_category = $request->input('dealer_category')?:'';
            $exist_tsa->dealer_gst_reg = $request->input('dealer_gst_reg')?:'0';
            $exist_tsa->dealer_blk_no = $request->input('dealer_blk_no')?:'';
            $exist_tsa->dealer_street = $request->input('dealer_street')?:'';
            $exist_tsa->dealer_unit_no = $request->input('dealer_unit_no')?:'';
            $exist_tsa->dealer_country = $request->input('dealer_country')?:'0';
            $exist_tsa->dealer_postalcode = $request->input('dealer_postalcode')?:'';
            $exist_tsa->dealer_contact_name = $request->input('dealer_contact_name')?:'';
            $exist_tsa->dealer_payableto = $request->input('dealer_payableto')?:'';
            $exist_tsa->dealer_status = $request->input('dealer_status')?:'0';
            $exist_tsa->dealer_telo = $request->input('dealer_telo')?:'';
            $exist_tsa->dealer_telm = $request->input('dealer_telm')?:'';
            $exist_tsa->dealer_email = $request->input('dealer_email')?:'';
            $exist_tsa->dealer_remark = $request->input('dealer_remark')?:'';
            $exist_tsa->updateBy = Auth::user()->id;
            $exist_tsa->save();

            if($request->input('login_email') != ''){
                if(!$exist_usr){
                    $new_userbase = new User();
                    $new_userbase->ref = $exist_tsa->dealer_id;
                    $new_userbase->email = $request->input('login_email');
                    $new_userbase->ref_code =  $this->user_code;
                    $new_userbase->cprofile = $request->input('cprofile');
                    $new_userbase->password = Hash::make($request->input('password'));
                    if(env("APP_ID") == '2'){
                        $new_userbase->ens_group_id = $request->input('group_id');
                    }
                    else{
                        $new_userbase->peo_group_id = $request->input('group_id');
                    }
                    $new_userbase->status = 1;
                    $new_userbase->save();
                }
                else{
                    if(env("APP_ID") == '2'){
                        $exist_usr->ens_group_id = $request->input('group_id');
                    }
                    else{
                        $exist_usr->peo_group_id = $request->input('group_id');
                    }
                    $exist_usr->cprofile = $request->input('cprofile');
                    $exist_usr->email = $request->input('login_email');
                    if($request->input('password') != ''){
                        $exist_usr->password = Hash::make($request->input('password'));
                    }
                    $exist_usr->save();
                }
            }

            if($request->hasFile('dealer_file')){
                $file_ctrl = new FilesController();
                $file_ctrl->uploadMultipleFile($request,'ens_tsa',$new_tsa->dealer_id,'uploads/tsa','dealer_file');
            }

            return redirect()->back()
            ->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $tsa = TSA::find($request->route('id'));
        if($tsa){
            $tsa->dealer_status = "0";
            $tsa->save();
        }
        $usr = User::where('ref',$request->route('id'))->where('ref_code', $this->user_code)->first();
        if($usr){
            $usr->status = "0";
            $usr->save();
        }
        return redirect()->route('TSAList')->with('success_msg', 'Delete TSA Succesfully');
    }

    public function getAttachment($tsa_id){
        $file = Files::where('files_ref_id', $tsa_id)->where('files_ref_table', 'ens_tsa')->where('status', 1)->first();

        return isset($file->files_name)?$file->files_name:'';
    }
}
