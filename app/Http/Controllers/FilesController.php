<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Files;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FilesController extends Controller
{
    //
    public function uploadFile(Request $request, $db_name, $ref_id, $path="uploads", $field_name = "attachment"){
        $exist_file = Files::where('files_ref_table',$db_name)->where('files_ref_id',  $ref_id)->first();
        if(!$exist_file){
            $new_file = new Files();
            $new_file->files_ref_table = $db_name;
            $new_file->files_ref_id =  $ref_id;
            $new_file->files_name = $ref_id."_".$this->rename_filename($request->file($field_name)->getClientOriginalName());
            $new_file->files_ori_name = $request->file($field_name)->getClientOriginalName();
            $new_file->file_type = $request->file($field_name)->extension();
            $new_file->upload_path = $path;
            $new_file->seq_no = 0;
            $new_file->status = 1;
            $new_file->insertBy = Auth::user()->id;
            $new_file->updateBy = Auth::user()->id;
            $new_file->save();
            $request->file($field_name)->move($path, $exist_tsa->dealer_id."_".$filename);
        }
        else{
            $exist_file->files_name = $ref_id."_".$this->rename_filename($request->file($field_name)->getClientOriginalName());
            $exist_file->files_ori_name = $request->file($field_name)->getClientOriginalName();
            $exist_file->file_type = $request->file($field_name)->extension();
            $exist_file->status = 1;
            $exist_file->upload_path = $path;
            $exist_file->save();
            $request->file($field_name)->move($path, $exist_tsa->dealer_id."_".$filename);
        }
    }

    public function uploadMultipleFile(Request $request, $db_name, $ref_id, $path="uploads" ,$field_name = "attachment"){
        $total_files = Files::where('files_ref_table',$db_name)->where('files_ref_id',  $ref_id)->count();
        foreach($request->file($field_name) as $key => $fli){
            $total_files++;
            $new_file = new Files();
            $new_file->files_ref_table = $db_name;
            $new_file->files_ref_id = $ref_id;
            $new_file->files_name = $ref_id."_".$this->rename_filename($fli->getClientOriginalName());
            $new_file->files_ori_name = $fli->getClientOriginalName();
            $new_file->file_type = $fli->extension();
            $new_file->upload_path = $path;
            $new_file->seq_no = $total_files;
            $new_file->status = 1;
            $new_file->insertBy = Auth::user()->id;
            $new_file->updateBy = Auth::user()->id;
            $new_file->save();
            $fli->move($path,  $new_file->files_name);
        }
    }

    public function rename_filename($string){
        $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
        
        return preg_replace('/[^A-Za-z0-9\-._]/', '_', $string); // Removes special chars.
    }

    public function getFilesList($db_name, $ref_id){
        $exist_file = Files::select('files_id', 'files_name', 'files_ori_name', 'upload_path', 'created_at', 'insertBy')
        ->where('files_ref_table',$db_name)
        ->where('files_ref_id',  $ref_id)
        ->where('status',  '1')
        ->get()->toArray();
        foreach($exist_file as $exist_ky => $exist_li){
            $insert_name = User::find($exist_li['insertBy']);
            $exist_file[$exist_ky]['insertBy'] = $insert_name->getName();
        }
        return $exist_file;
    }

    public function removeFile(Request $request){
        $exist_file = Files::find($request->route('id'));
        if($exist_file){
            $exist_file->status = 0;
            $exist_file->save();
        }
        return redirect()->back()->with('sweet_success_msg','Delete File Success');
    }

    public function removeAllFile(Request $request){
        if($request->route()->getName() == 'AllReceiptFileDelete'){
            Files::where('files_ref_id', $request->route('id'))
            ->where('files_ref_table', 'ens_receipt')
            ->update(['status' => 0, 'updateBy' => Auth::user()->id]);
        }

        return redirect()->back()->with('sweet_success_msg','Delete File Success');
    }
}
