<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Allianz;
use App\Models\Order;
use App\Models\Country;
use App\Models\Refno;
use App\Models\TSA;
use App\Models\Vehicles;
use App\Models\HiresComp;
use App\Models\Empl;
use App\Models\User;
use App\Models\Customer;
use App\Models\Motor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use App\Exports\AllianzExport;
use Maatwebsite\Excel\Facades\Excel;

class AllianzController extends Controller
{
    //
    public $prefix = "AMI";
    public $motor_prefix = "MI";
    public $motor_order_prefix = "CY";

    public $comp_base_premium = [
        '20' => [
            '200' =>[   
                '0' => '1350',
                '1' => '1300',
                '2' => '1250',
                '5' => '1200',
                '15' => '1250',
            ],
            '400' =>[   
                '0' => '1250',
                '1' => '1300',
                '2' => '1250',
                '5' => '1200',
                '15' => '1250',
            ],
            '750' =>[   
                '0' => '1550',
                '1' => '1530',
                '2' => '1510',
                '5' => '1500',
                '15' => '1550',
            ]
        ],
        '25' => [
            '200' =>[   
                '0' => '750',
                '1' => '650',
                '2' => '640',
                '5' => '640',
                '15' => '690',
            ],
            '400' =>[   
                '0' => '900',
                '1' => '780',
                '2' => '760',
                '5' => '750',
                '15' => '800',
            ],
            '750' =>[   
                '0' => '900',
                '1' => '780',
                '2' => '760',
                '5' => '760',
                '15' => '810',
            ],
            '1200' =>[   
                '0' => '1350',
                '1' => '1300',
                '2' => '1250',
                '5' => '1200',
                '15' => '1250',
            ],
            '1500' =>[   
                '0' => '2000',
                '1' => '1950',
                '2' => '1900',
                '5' => '1850',
                '15' => '1900',
            ],
        ],
        '30' => [
            '200' =>[   
                '0' => '520',
                '1' => '440',
                '2' => '430',
                '5' => '430',
                '15' => '480',
            ],
            '400' =>[   
                '0' => '620',
                '1' => '520',
                '2' => '510',
                '5' => '510',
                '15' => '560',
            ],
            '750' =>[   
                '0' => '620',
                '1' => '520',
                '2' => '500',
                '5' => '500',
                '15' => '550',
            ],
            '1200' =>[   
                '0' => '820',
                '1' => '750',
                '2' => '720',
                '5' => '710',
                '15' => '760',
            ],
            '1500' =>[   
                '0' => '1850',
                '1' => '1800',
                '2' => '1750',
                '5' => '1700',
                '15' => '1750',
            ],
        ],
        '50' => [
            '200' =>[   
                '0' => '310',
                '1' => '310',
                '2' => '300',
                '5' => '300',
                '15' => '350',
            ],
            '400' =>[   
                '0' => '370',
                '1' => '360',
                '2' => '340',
                '5' => '340',
                '15' => '390',
            ],
            '750' =>[   
                '0' => '450',
                '1' => '400',
                '2' => '390',
                '5' => '390',
                '15' => '440',
            ],
            '1200' =>[   
                '0' => '540',
                '1' => '500',
                '2' => '500',
                '5' => '490',
                '15' => '540',
            ],
            '1500' =>[   
                '0' => '810',
                '1' => '730',
                '2' => '720',
                '5' => '710',
                '15' => '760',
            ],
        ],
        '70' => [
            '200' =>[   
                '0' => '320',
                '1' => '310',
                '2' => '300',
                '5' => '300',
                '15' => '350',
            ],
            '400' =>[   
                '0' => '410',
                '1' => '390',
                '2' => '380',
                '5' => '380',
                '15' => '430',
            ],
            '750' =>[   
                '0' => '460',
                '1' => '410',
                '2' => '400',
                '5' => '400',
                '15' => '450',
            ],
            '1200' =>[   
                '0' => '550',
                '1' => '510',
                '2' => '510',
                '5' => '500',
                '15' => '550',
            ],
            '1500' =>[   
                '0' => '820',
                '1' => '740',
                '2' => '730',
                '5' => '720',
                '15' => '770',
            ],
        ],
    ];

    public $tpft_base_premium = [
        '20' => [
            '200' =>[   
                '0' => '950',
                '1' => '820',
                '2' => '790',
                '5' => '750',
                '15' => '800',
            ],
            '400' =>[   
                '0' => '980',
                '1' => '950',
                '2' => '930',
                '5' => '910',
                '15' => '960',
            ],
            '750' =>[   
                '0' => '1200',
                '1' => '1180',
                '2' => '1160',
                '5' => '1140',
                '15' => '1190',
            ]
        ],
        '25' => [
            '200' =>[   
                '0' => '535',
                '1' => '520',
                '2' => '460',
                '5' => '450',
                '15' => '500',
            ],
            '400' =>[   
                '0' => '650',
                '1' => '620',
                '2' => '610',
                '5' => '600',
                '15' => '650',
            ],
            '750' =>[   
                '0' => '720',
                '1' => '620',
                '2' => '610',
                '5' => '605',
                '15' => '655',
            ],
            '1200' =>[   
                '0' => '920',
                '1' => '800',
                '2' => '780',
                '5' => '760',
                '15' => '810',
            ],
            '1500' =>[   
                '0' => '1350',
                '1' => '1200',
                '2' => '1100',
                '5' => '1000',
                '15' => '1050',
            ],
        ],
        '30' => [
            '200' =>[   
                '0' => '350',
                '1' => '340',
                '2' => '320',
                '5' => '310',
                '15' => '360',
            ],
            '400' =>[   
                '0' => '420',
                '1' => '410',
                '2' => '400',
                '5' => '350',
                '15' => '400',
            ],
            '750' =>[   
                '0' => '530',
                '1' => '460',
                '2' => '450',
                '5' => '440',
                '15' => '490',
            ],
            '1200' =>[   
                '0' => '710',
                '1' => '640',
                '2' => '600',
                '5' => '600',
                '15' => '650',
            ],
            '1500' =>[   
                '0' => '760',
                '1' => '750',
                '2' => '720',
                '5' => '680',
                '15' => '730',
            ],
        ],
        '50' => [
            '200' =>[   
                '0' => '220',
                '1' => '200',
                '2' => '190',
                '5' => '190',
                '15' => '240',
            ],
            '400' =>[   
                '0' => '280',
                '1' => '260',
                '2' => '250',
                '5' => '240',
                '15' => '290',
            ],
            '750' =>[   
                '0' => '370',
                '1' => '340',
                '2' => '320',
                '5' => '310',
                '15' => '360',
            ],
            '1200' =>[   
                '0' => '450',
                '1' => '420',
                '2' => '410',
                '5' => '400',
                '15' => '450',
            ],
            '1500' =>[   
                '0' => '460',
                '1' => '460',
                '2' => '450',
                '5' => '430',
                '15' => '480',
            ],
        ],
        '70' => [
            '200' =>[   
                '0' => '230',
                '1' => '210',
                '2' => '200',
                '5' => '200',
                '15' => '250',
            ],
            '400' =>[   
                '0' => '290',
                '1' => '270',
                '2' => '250',
                '5' => '250',
                '15' => '300',
            ],
            '750' =>[   
                '0' => '380',
                '1' => '350',
                '2' => '330',
                '5' => '320',
                '15' => '370',
            ],
            '1200' =>[   
                '0' => '460',
                '1' => '430',
                '2' => '420',
                '5' => '410',
                '15' => '460',
            ],
            '1500' =>[   
                '0' => '520',
                '1' => '520',
                '2' => '510',
                '5' => '480',
                '15' => '530',
            ],
        ],
    ];

    public $tpo_base_premium = [
        '20' => [
            '200' => '600',
            '400' => '650',
            '750' => '800',
        ],
        '25' => [
            '200' => '310',
            '400' => '400',
            '750' => '610',
            '1200' => '615',
            '1500' => '930',
        ],
        '30' => [
            '200' => '220',
            '400' => '270',
            '750' => '310',
            '1200' => '420',
            '1500' => '650',
        ],
        '50' => [
            '200' => '130',
            '400' => '170',
            '750' => '245',
            '1200' => '330',
            '1500' => '350',
        ],
        '70' => [
            '200' => '150',
            '400' => '180',
            '750' => '250',
            '1200' => '340',
            '1500' => '360',
        ],
    ];

    public $comp_excess = [
        '20' => [
            '200' => '1000',
            '400' => '1000',
            '750' => '1250',
        ],
        '25' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
        '30' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
        '50' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
        '70' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
    ];

    public $tpft_excess = [
        '20' => [
            '200' => '1000',
            '400' => '1000',
            '750' => '1250',
        ],
        '25' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
        '30' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
        '50' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
        '70' => [
            '200' => '300',
            '400' => '500',
            '750' => '750',
            '1200' => '1000',
            '1500' => '1500',
        ],
    ];

    public $delivery_fees = 50;

    public function getListing(Request $request){
        return view('allianz.List');
    }

    public function getListData(Request $request){
        $order_fields = ['','order_vehicles_no','order_no','order_policyno','order_cus_lname','order_cus_nirc','order_period_from','order_period_to',''];
        $draw = $request->input('draw');
        $start = $request->input("start");
        $rowperpage = $request->input("length")?:10;
        $searchValue = $request->input("search")['value'];

        if($rowperpage > 100){
            $rowperpage = 100;
        }

        $count_query = Allianz::select('count(*) as allcount')
        ->where('order_status', "!=","0");

        $main_query = Allianz::where('order_status', "!=","0")
        ->where(function ($query) use ($searchValue) {
            $query->where('order_vehicles_no', 'like', '%' . $searchValue . '%')
                ->orWhere('order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('order_policyno', 'like', '%' . $searchValue . '%')
                ->orWhere('order_cus_lname', 'like', '%' . $searchValue . '%')
                ->orWhere('order_cus_fname', 'like', '%' . $searchValue . '%')
                ->orWhere('order_cus_nirc', 'like', '%' . $searchValue . '%');
        });

        if(Auth::user()->ref_code == 'TSA'){
            $count_query->where('insertBy', Auth::user()->id);
            $main_query->where('insertBy', Auth::user()->id);
        }

        $totalRecords = $count_query->count();
        $totalRecordswithFilter = $count_query
        ->where(function ($query) use ($searchValue) {
            $query->where('order_vehicles_no', 'like', '%' . $searchValue . '%')
                ->orWhere('order_no', 'like', '%' . $searchValue . '%')
                ->orWhere('order_policyno', 'like', '%' . $searchValue . '%')
                ->orWhere('order_cus_lname', 'like', '%' . $searchValue . '%')
                ->orWhere('order_cus_fname', 'like', '%' . $searchValue . '%')
                ->orWhere('order_cus_nirc', 'like', '%' . $searchValue . '%');
        })
        ->count();

        foreach($request->input("order") as $or_li){
            if(isset($order_fields[$or_li['column']]) && $order_fields[$or_li['column']] ){
                $main_query->orderBy($order_fields[$or_li['column']], $or_li['dir']);
            }
        }

        $listing_data = $main_query
        ->skip($start)
        ->take($rowperpage)
        ->get()->toArray();

        $out_data = [];
        
        foreach($listing_data as $ky => $li){
            $action = '';
            if($li['order_status'] == 1){
                if(UserGroupPrivilegeController::checkPrivilegeWithAuth("AllianzView")){
                    $action .= '<a href="'.route('AllianzView', ['id' => $li['id'] ]).'" class="btn btn-warning">Edit</a>';
                }

                if(UserGroupPrivilegeController::checkPrivilegeWithAuth("AllianzDelete")){
                    $action .= '<a href="'.route('AllianzDelete', ['id' => $li['id'] ]).'" class="btn btn-danger del_btn" rel="'.$li['order_no'].'">Delete</a>';
                }
            }
            else{
                if(UserGroupPrivilegeController::checkPrivilegeWithAuth("AllianzView")){
                    $action .= '<a href="'.route('AllianzView', ['id' => $li['id'] ]).'" class="btn btn-secondary">View</a>';
                }
                if($li['order_ref'] == 0 && UserGroupPrivilegeController::checkPrivilegeWithAuth("AllianzCopy")){
                    $action .= '<a href="'.route('AllianzCopy', ['id' => $li['id'] ]).'" class="btn btn-success">Copy to Local</a>';
                }
            }
            
            $out_data[] = [
                $ky + 1,
                $li['order_vehicles_no']?:'-',
                $li['order_no']?:'-',
                $li['order_policyno']?:'-',
                $li['order_cus_lname']." ".$li['order_cus_fname'],
                substr_replace($li['order_cus_nirc'],'XXXX', 1,-4),
                $li['order_period_from']?:'-',
                $li['order_period_to']?:'-',
                $action
            ];
        }
       
        $output = [
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $out_data,
        ];

        return response()->json($output, 200); 
    }

    public function InsuranceInfo(Request $request){
        $country_list = Country::select("country_id", "country_code")->where("country_status", 1)->orderBy('country_seqno', 'ASC')->get()->toArray();
        if($request->route('id')){
            if(Auth::user()->ref_code == 'TSA'){
                $data= Allianz::where("id", $request->route('id'))->where('insertBy', Auth::user()->id);
            }
            else{
                $data = Allianz::find($request->route('id'));
            }
            if(isset($data->id)){
                $exist_dealer = TSA::find($data->order_dealer);
                $exist_vehicle = Vehicles::find($data->order_vehicles_type);
                $exist_hires = HiresComp::find($data->order_financecode);
                $add_name = User::find($data->insertBy);
                $up_name = User::find($data->updateBy);
                $data->update_name =  $up_name->getName()?:'Webmaster';
                $data->insert_name =  $add_name->getName()?:'Webmaster';
                $data->country_list = $country_list;
                $data->dealer_name = isset($exist_dealer->dealer_name)?$exist_dealer->dealer_name:'';
                $data->dealer_code = isset($exist_dealer->dealer_code)?$exist_dealer->dealer_code:'';
                $data->vehicles_name = isset($exist_vehicle->id)?$exist_vehicle->name:'';
                $data->financecode_name = isset($exist_hires->id)?$exist_hires->comp_name:'';
                $data->order_no = isset($data->order_no)?$data->order_no:'';
                $data->dealer_id = $data->order_dealer;
                $data->min_age = date('Y-m-d', strtotime(Carbon::now()->subYears(70)));
                $data->max_age = date('Y-m-d', strtotime(Carbon::now()->subYears(18)));
                $data->min_year = date('Y', strtotime(Carbon::now()->subYears(30)));
                if($data->order_status == 2){
                    $data->disabled = "disabled";
                }
                else{
                    $data->disabled = "";
                }
                return view('allianz.Info', $data);
            }
            else{
                return redirect()->route('AllianzList');
            }
        }
        else{
            $data = [
                'country_list'=> $country_list,
                'min_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(70))),
                'max_age' => date('Y-m-d', strtotime(Carbon::now()->subYears(18))),
                'min_year' => date('Y', strtotime(Carbon::now()->subYears(30))),
            ];
            return view('allianz.Add', $data);
        }
    }

    public function create(Request $request){
        $rules = [
            'order_date'  => 'required',
            'order_cus_lname' => 'required|min:3',
            'order_cus_fname' => 'required|min:3',
            'order_dealer' => 'nullable|exists:tsa,dealer_id',
            'order_cus_national' => 'required|exists:country,country_id',
            'order_cus_nirc'  => 'required|min:5',
            'order_cus_dob' => 'required|date',
            'order_cus_gender'  => 'required',
            'order_cus_exp'  => 'required',
            'order_cus_unitno'  => 'required',
            'order_cus_postal_code'  => 'required|min:5',
            'order_cus_contact' => 'required',
            'order_cus_email' => 'required|email',
            'order_tsa_email' => 'required|email',
            'order_vehicles_type' => 'required',
            'order_vehicles_no' => 'required_if:order_new_reg,==,N',
            'order_coverage' => 'required',
            'order_vehicles_capacity' => 'required',
            'order_period_from' => 'required|date|after_or_equal:today',
            'order_period_to' => 'required|date|after:order_period_from',
            'order_premium_amt' => 'required_if:order_status,==,2',
            'order_other_financecode' => 'required_if:order_financecode,==,115',
            "driver_lname_1" => "required",
            "driver_fname_1" => "required",
            "driver_nirc_1" => "required",
            "driver_national_1" => "required|exists:country,country_id",
            "driver_gender_1" => "required",
            "driver_dob_1" => "required|date",
            "driver_exp_1" => "required",
            "driver_exp_2" => "required_with:driver_nirc_2",
            "driver_exp_3" => "required_with:driver_nirc_3",
            "driver_exp_4" => "required_with:driver_nirc_4",
            "driver_dob_2" => "required_with:driver_nirc_2",
            "driver_dob_3" => "required_with:driver_nirc_3",
            "driver_dob_4" => "required_with:driver_nirc_4",
        ];

        $attr = [
            'order_date' => 'Issue Date',
            'order_cus_lname' => 'Customer Last Name',
            'order_cus_fname' => 'Customer First Name',
            'order_cus_national' => 'Nationality',
            'order_cus_nirc'  => 'Identity Number',
            'order_cus_nirc_type'  => 'Identity Type',
            'order_cus_dob' => 'Date Of Birth',
            'order_cus_gender'  => 'Gender',
            'order_cus_exp'  => 'Driving Exprience In singapore',
            'order_cus_unitno'  => 'Unit Number',
            'order_cus_postal_code'  => 'Post Code',
            'order_cus_contact' => 'Mobile',
            'order_cus_email' => 'Customer Email',
            'order_tsa_email' => 'TSA Email',
            'order_vehicles_type' => 'Vehicle Make',
            'order_vehicles_no' => 'Vehicle Plate',
            'order_coverage' => 'Cover Type',
            'order_vehicles_capacity' => 'Vehicle Capacity',
            'order_period_from' => 'Coverage Period From',
            'order_period_to' => 'Coverage Period To',
            'order_premium_amt' => 'Basic Premium',
            'order_other_financecode' => 'Other Hires Company Name',
            "driver_lname_1" => "Last Name",
            "driver_fname_1" => "First Name",
            "driver_nirc_1" => "NRIC",
            "driver_national_1" => "Nationality",
            "driver_gender_1" => "Gender",
            "driver_dob_1" => "Rider DOB",
            "driver_dob_2" => "Rider DOB",
            "driver_dob_3" => "Rider DOB",
            "driver_dob_4" => "Rider DOB",
            "driver_exp_1" => "Rider Experience",
            "driver_exp_2" => "Rider Experience",
            "driver_exp_3" => "Rider Experience",
            "driver_exp_4" => "Rider Experience",
            "driver_nirc_2" => "Rider NRIC",
            "driver_nirc_3" => "Rider NRIC",
            "driver_nirc_4" => "Rider NRIC",
        ];

        $request->validate($rules, [], $attr);

        $errors_input = [];

        $errors_input = $this->validationDetails($request);

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $order_financecode = $request->input('order_financecode');
        if( $request->input('order_financecode') == '115'){
            $order_financecode = $this->getFinanceCodeID($request->input('order_other_financecode'));
        }
        
        $new_allianz = new Allianz();
        $new_allianz->order_policyno = $request->input('order_policyno');
        $new_allianz->order_dealer = $request->input('order_dealer');
        $new_allianz->order_date = $request->input('order_date');
        $new_allianz->order_promo_code = $request->input('order_promo_code');
        $new_allianz->order_no = $this->generateCode();
        $this->updateRefCode();
        $new_allianz->order_record_billto = $request->input('order_record_billto');
        $new_allianz->order_cus_driving = $request->input('order_cus_driving');
        $new_allianz->order_cus_lname = $request->input('order_cus_lname');
        $new_allianz->order_cus_fname = $request->input('order_cus_fname');
        $new_allianz->order_cus_national = $request->input('order_cus_national');
        $new_allianz->order_cus_nirc = $request->input('order_cus_nirc');
        $new_allianz->order_cus_dob = $request->input('order_cus_dob');
        $new_allianz->order_cus_gender = $request->input('order_cus_gender');
        $new_allianz->order_cus_exp = $request->input('order_cus_exp');
        $new_allianz->order_cus_unitno = $request->input('order_cus_unitno');
        $new_allianz->order_cus_street_name = $request->input('order_cus_street_name');
        $new_allianz->order_cus_build_name = $request->input('order_cus_build_name');
        $new_allianz->order_cus_postal_code = $request->input('order_cus_postal_code');
        $new_allianz->order_cus_address = $request->input('order_cus_address');
        $new_allianz->order_cus_contact = $request->input('order_cus_contact');
        $new_allianz->order_cus_email = $request->input('order_cus_email');
        $new_allianz->order_tsa_email = $request->input('order_tsa_email');
        $new_allianz->order_cus_claim = $request->input('order_cus_claim');
        $new_allianz->order_cus_claim_amount = $request->input('order_cus_claim_amount');
        $new_allianz->order_vehicles_reg_year = $request->input('order_vehicles_reg_year');
        $new_allianz->order_vehicles_pro_year = $request->input('order_vehicles_pro_year');
        $new_allianz->order_new_reg = $request->input('order_new_reg');
        $new_allianz->order_vehicles_no = $request->input('order_vehicles_no');
        $new_allianz->order_vehicles_type = $request->input('order_vehicles_type');
        $new_allianz->order_vehicles_model = $request->input('order_vehicles_model');
        $new_allianz->order_vehicles_capacity = $request->input('order_vehicles_capacity');
        $new_allianz->order_vehicles_engine = $request->input('order_vehicles_engine');
        $new_allianz->order_vehicles_chasis = $request->input('order_vehicles_chasis');
        $new_allianz->order_financecode = $order_financecode;
        $new_allianz->order_ncd_entitlement = $request->input('order_ncd_entitlement');
        $new_allianz->order_coverage = $request->input('order_coverage');
        $new_allianz->order_period = $request->input('order_period');
        $new_allianz->order_period_from = $request->input('order_period_from');
        $new_allianz->order_period_to = $request->input('order_period_to');
        $new_allianz->order_premium_amt = $request->input('order_premium_amt');
        $new_allianz->order_accident_amt = $request->input('order_accident_amt');
        $new_allianz->order_ncd_dis_amt = $request->input('order_ncd_dis_amt');
        $new_allianz->order_in_exp_amt = $request->input('order_in_exp_amt');
        $new_allianz->order_delivery_amt = $request->input('order_delivery_amt');
        $new_allianz->order_addtional_amt = $request->input('order_addtional_amt');
        $new_allianz->order_subtotal_amount = $request->input('order_subtotal_amount');
        $new_allianz->order_gst_percent = $request->input('order_gst_percent');
        $new_allianz->order_gst_amount = $request->input('order_gst_amount');
        $new_allianz->order_grosspremium_amount = $request->input('order_grosspremium_amount');
        $new_allianz->order_excess_amount = $request->input('order_excess_amount');
        $new_allianz->driver_lname_1 = $request->input('driver_lname_1');
        $new_allianz->driver_fname_1 = $request->input('driver_fname_1');
        $new_allianz->driver_nirc_1 = $request->input('driver_nirc_1');
        $new_allianz->driver_national_1 = $request->input('driver_national_1');
        $new_allianz->driver_gender_1 = $request->input('driver_gender_1');
        $new_allianz->driver_dob_1 = $request->input('driver_dob_1');
        $new_allianz->driver_exp_1 = $request->input('driver_exp_1');
        $new_allianz->driver_lname_2 = $request->input('driver_lname_2');
        $new_allianz->driver_fname_2 = $request->input('driver_fname_2');
        $new_allianz->driver_nirc_2 = $request->input('driver_nirc_2');
        $new_allianz->driver_national_2 = $request->input('driver_national_2');
        $new_allianz->driver_gender_2 = $request->input('driver_gender_2');
        $new_allianz->driver_dob_2 = $request->input('driver_dob_2');
        $new_allianz->driver_exp_2 = $request->input('driver_exp_2');
        $new_allianz->driver_lname_3 = $request->input('driver_lname_3');
        $new_allianz->driver_fname_3 = $request->input('driver_fname_3');
        $new_allianz->driver_nirc_3 = $request->input('driver_nirc_3');
        $new_allianz->driver_national_3 = $request->input('driver_national_3');
        $new_allianz->driver_gender_3 = $request->input('driver_gender_3');
        $new_allianz->driver_dob_3 = $request->input('driver_dob_3');
        $new_allianz->driver_exp_3 = $request->input('driver_exp_3');
        $new_allianz->driver_lname_4 = $request->input('driver_lname_4');
        $new_allianz->driver_fname_4 = $request->input('driver_fname_4');
        $new_allianz->driver_nirc_4 = $request->input('driver_nirc_4');
        $new_allianz->driver_national_4 = $request->input('driver_national_4');
        $new_allianz->driver_gender_4 = $request->input('driver_gender_4');
        $new_allianz->driver_dob_4 = $request->input('driver_dob_4');
        $new_allianz->driver_exp_4 = $request->input('driver_exp_4');
        $new_allianz->order_status = $request->input('order_status');
        $new_allianz->insertBy = Auth::user()->id;
        $new_allianz->updateBy = Auth::user()->id;
        $new_allianz->save();

        return redirect()->route('AllianzList')->with('success_msg', 'Add Case Succesfully');
    }

    public function update(Request $request){
        $exist_ins = Allianz::where('id', $request->input('allianz_id'))->Where('order_status','1')->first();

        if(!$exist_ins){
            return redirect()->route('AllianzList')->with('error_msg', 'Invalid Case');
        }
        else{
            $rules = [
                'order_date'  => 'required',
                'order_cus_lname' => 'required|min:3',
                'order_cus_fname' => 'required|min:3',
                'order_dealer' => 'nullable|exists:tsa,dealer_id',
                'order_cus_national' => 'required|exists:country,country_id',
                'order_cus_nirc'  => 'required|min:5',
                'order_cus_dob' => 'required|date',
                'order_cus_gender'  => 'required',
                'order_cus_exp'  => 'required',
                'order_cus_unitno'  => 'required',
                'order_cus_postal_code'  => 'required|min:5',
                'order_cus_contact' => 'required',
                'order_cus_email' => 'required|email',
                'order_tsa_email' => 'required|email',
                'order_vehicles_type' => 'required',
                'order_vehicles_no' => 'required_if:order_new_reg,==,N',
                'order_coverage' => 'required',
                'order_vehicles_capacity' => 'required',
                'order_period_from' => 'required|date|after_or_equal:today',
                'order_period_to' => 'required|date|after:order_period_from',
                'order_premium_amt' => 'required_if:order_status,==,2|gt:0',
                'order_other_financecode' => 'required_if:order_financecode,==,115',
                "driver_lname_1" => "required",
                "driver_fname_1" => "required",
                "driver_nirc_1" => "required",
                "driver_national_1" => "required|exists:country,country_id",
                "driver_gender_1" => "required",
                "driver_dob_1" => "required|date",
                "driver_exp_1" => "required",
                "driver_exp_2" => "required_with:driver_nirc_2",
                "driver_exp_3" => "required_with:driver_nirc_3",
                "driver_exp_4" => "required_with:driver_nirc_4",
                "driver_dob_2" => "required_with:driver_nirc_2",
                "driver_dob_3" => "required_with:driver_nirc_3",
                "driver_dob_4" => "required_with:driver_nirc_4",
            ];
    
            $attr = [
                'order_date' => 'Issue Date',
                'order_cus_lname' => 'Customer Last Name',
                'order_cus_fname' => 'Customer First Name',
                'order_cus_national' => 'Nationality',
                'order_cus_nirc'  => 'Identity Number',
                'order_cus_nirc_type'  => 'Identity Type',
                'order_cus_dob' => 'Date Of Birth',
                'order_cus_gender'  => 'Gender',
                'order_cus_exp'  => 'Driving Exprience In singapore',
                'order_cus_unitno'  => 'Unit Number',
                'order_cus_postal_code'  => 'Post Code',
                'order_cus_contact' => 'Mobile',
                'order_cus_email' => 'Customer Email',
                'order_tsa_email' => 'TSA Email',
                'order_vehicles_type' => 'Vehicle Make',
                'order_vehicles_no' => 'Vehicle Plate',
                'order_coverage' => 'Cover Type',
                'order_vehicles_capacity' => 'Vehicle Capacity',
                'order_period_from' => 'Coverage Period From',
                'order_period_to' => 'Coverage Period To',
                'order_premium_amt' => 'Basic Premium',
                'order_other_financecode' => 'Other Hires Company Name',
                "driver_lname_1" => "Last Name",
                "driver_fname_1" => "First Name",
                "driver_nirc_1" => "NRIC",
                "driver_national_1" => "Nationality",
                "driver_gender_1" => "Gender",
                "driver_dob_1" => "Rider DOB",
                "driver_dob_2" => "Rider DOB",
                "driver_dob_3" => "Rider DOB",
                "driver_dob_4" => "Rider DOB",
                "driver_exp_1" => "Rider Experience",
                "driver_exp_2" => "Rider Experience",
                "driver_exp_3" => "Rider Experience",
                "driver_exp_4" => "Rider Experience",
                "driver_nirc_2" => "Rider NRIC",
                "driver_nirc_3" => "Rider NRIC",
                "driver_nirc_4" => "Rider NRIC",
            ];
    
    
            $request->validate($rules, [], $attr);
    
            $errors_input = [];
            $errors_input = $this->validationDetails($request);
    
            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }

            $order_financecode = $request->input('order_financecode');
            if( $request->input('order_financecode') == '115'){
                $order_financecode = $this->getFinanceCodeID($request->input('order_other_financecode'));
            }

            $exist_ins->order_policyno = $request->input('order_policyno');
            $exist_ins->order_dealer = $request->input('order_dealer');
            $exist_ins->order_promo_code = $request->input('order_promo_code');
            $exist_ins->order_date = $request->input('order_date');
            $exist_ins->order_record_billto = $request->input('order_record_billto');
            $exist_ins->order_cus_driving = $request->input('order_cus_driving');
            $exist_ins->order_cus_lname = $request->input('order_cus_lname');
            $exist_ins->order_cus_fname = $request->input('order_cus_fname');
            $exist_ins->order_cus_national = $request->input('order_cus_national');
            $exist_ins->order_cus_nirc = $request->input('order_cus_nirc');
            $exist_ins->order_cus_dob = $request->input('order_cus_dob');
            $exist_ins->order_cus_gender = $request->input('order_cus_gender');
            $exist_ins->order_cus_exp = $request->input('order_cus_exp');
            $exist_ins->order_cus_unitno = $request->input('order_cus_unitno');
            $exist_ins->order_cus_street_name = $request->input('order_cus_street_name');
            $exist_ins->order_cus_build_name = $request->input('order_cus_build_name');
            $exist_ins->order_cus_postal_code = $request->input('order_cus_postal_code');
            $exist_ins->order_cus_address = $request->input('order_cus_address');
            $exist_ins->order_cus_contact = $request->input('order_cus_contact');
            $exist_ins->order_cus_email = $request->input('order_cus_email');
            $exist_ins->order_tsa_email = $request->input('order_tsa_email');
            $exist_ins->order_cus_claim = $request->input('order_cus_claim');
            $exist_ins->order_cus_claim_amount = $request->input('order_cus_claim_amount');
            $exist_ins->order_vehicles_reg_year = $request->input('order_vehicles_reg_year');
            $exist_ins->order_vehicles_pro_year = $request->input('order_vehicles_pro_year');
            $exist_ins->order_new_reg = $request->input('order_new_reg');
            $exist_ins->order_vehicles_no = $request->input('order_vehicles_no');
            $exist_ins->order_vehicles_type = $request->input('order_vehicles_type');
            $exist_ins->order_vehicles_model = $request->input('order_vehicles_model');
            $exist_ins->order_vehicles_capacity = $request->input('order_vehicles_capacity');
            $exist_ins->order_vehicles_engine = $request->input('order_vehicles_engine');
            $exist_ins->order_vehicles_chasis = $request->input('order_vehicles_chasis');
            $exist_ins->order_financecode = $order_financecode;
            $exist_ins->order_ncd_entitlement = $request->input('order_ncd_entitlement');
            $exist_ins->order_coverage = $request->input('order_coverage');
            $exist_ins->order_period = $request->input('order_period');
            $exist_ins->order_period_from = $request->input('order_period_from');
            $exist_ins->order_period_to = $request->input('order_period_to');
            $exist_ins->order_premium_amt = $request->input('order_premium_amt');
            $exist_ins->order_accident_amt = $request->input('order_accident_amt');
            $exist_ins->order_ncd_dis_amt = $request->input('order_ncd_dis_amt');
            $exist_ins->order_in_exp_amt = $request->input('order_in_exp_amt');
            $exist_ins->order_delivery_amt = $request->input('order_delivery_amt');
            $exist_ins->order_addtional_amt = $request->input('order_addtional_amt');
            $exist_ins->order_subtotal_amount = $request->input('order_subtotal_amount');
            $exist_ins->order_gst_percent = $request->input('order_gst_percent');
            $exist_ins->order_gst_amount = $request->input('order_gst_amount');
            $exist_ins->order_grosspremium_amount = $request->input('order_grosspremium_amount');
            $exist_ins->order_excess_amount = $request->input('order_excess_amount');
            $exist_ins->driver_lname_1 = $request->input('driver_lname_1');
            $exist_ins->driver_fname_1 = $request->input('driver_fname_1');
            $exist_ins->driver_nirc_1 = $request->input('driver_nirc_1');
            $exist_ins->driver_national_1 = $request->input('driver_national_1');
            $exist_ins->driver_gender_1 = $request->input('driver_gender_1');
            $exist_ins->driver_dob_1 = $request->input('driver_dob_1');
            $exist_ins->driver_exp_1 = $request->input('driver_exp_1');
            $exist_ins->driver_lname_2 = $request->input('driver_lname_2');
            $exist_ins->driver_fname_2 = $request->input('driver_fname_2');
            $exist_ins->driver_nirc_2 = $request->input('driver_nirc_2');
            $exist_ins->driver_national_2 = $request->input('driver_national_2');
            $exist_ins->driver_gender_2 = $request->input('driver_gender_2');
            $exist_ins->driver_dob_2 = $request->input('driver_dob_2');
            $exist_ins->driver_exp_2 = $request->input('driver_exp_2');
            $exist_ins->driver_lname_3 = $request->input('driver_lname_3');
            $exist_ins->driver_fname_3 = $request->input('driver_fname_3');
            $exist_ins->driver_nirc_3 = $request->input('driver_nirc_3');
            $exist_ins->driver_national_3 = $request->input('driver_national_3');
            $exist_ins->driver_gender_3 = $request->input('driver_gender_3');
            $exist_ins->driver_dob_3 = $request->input('driver_dob_3');
            $exist_ins->driver_exp_3 = $request->input('driver_exp_3');
            $exist_ins->driver_lname_4 = $request->input('driver_lname_4');
            $exist_ins->driver_fname_4 = $request->input('driver_fname_4');
            $exist_ins->driver_nirc_4 = $request->input('driver_nirc_4');
            $exist_ins->driver_national_4 = $request->input('driver_national_4');
            $exist_ins->driver_gender_4 = $request->input('driver_gender_4');
            $exist_ins->driver_dob_4 = $request->input('driver_dob_4');
            $exist_ins->driver_exp_4 = $request->input('driver_exp_4');
            $exist_ins->order_status = $request->input('order_status');
            $exist_ins->updateBy = Auth::user()->id;
            $exist_ins->save();

            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        $exist_ins = Allianz::where('id', $request->route('id'))->Where('order_status','1')->first();
        if($exist_ins){
            $exist_ins->order_status = 0;
            $exist_ins->save();
            return redirect()->route('AllianzList')->with('success_msg', 'Delete Case Succesfully');
        }
        else{
            return redirect()->route('AllianzList')->with('error_msg', 'Invalid Case');
        }
        
    }

    public function copyToLocal(Request $request){
        $exist_alianz = Allianz::find($request->route('id'));
        if($exist_alianz && $exist_alianz->order_ref == 0){
            $new_order = new Order();
            $new_order->order_prefix_type = $this->motor_prefix;
            $new_order->order_cprofile = env('APP_ID');
            $new_order->order_no = $this->generateMotorCode();
            $this->updateMotorRefCode();
            $new_order->order_doc_type = "DN";
            $new_order->order_ref = 0;
            $new_order->order_renewal_ref = 0;
            $new_order->order_ins_doc_no = '';
            $new_order->order_type = "New Case";
            $new_order->order_customer = $this->getCustomerId($request->route('id'));
            $new_order->order_dealer = $exist_alianz->order_dealer?:0;
            $new_order->order_premium_amt = $this->cleanNumber($exist_alianz->order_premium_amt?:0);
            $new_order->order_commercial_use_per = $this->cleanNumber($exist_alianz->order_commercial_use_per?:0);
            $new_order->order_accident_per = $this->cleanNumber($exist_alianz->order_accident_per?:0);
            $new_order->order_accident_amt = $this->cleanNumber($exist_alianz->order_accident_amt?:0);
            $new_order->order_in_exp_amt = $this->cleanNumber($exist_alianz->order_in_exp_amt?:0);
            $new_order->order_vehicle_load_per = $this->cleanNumber($exist_alianz->order_vehicle_load_per?:0);
            $new_order->order_vehicle_load_amt = $this->cleanNumber($exist_alianz->order_vehicle_load_amt?:0);
            $new_order->order_ncd_dis_per = $this->cleanNumber($exist_alianz->order_ncd_dis_per?:0);
            $new_order->order_ncd_dis_amt = $this->cleanNumber($exist_alianz->order_ncd_dis_amt?:0);
            $new_order->order_ofd_dis_per = $this->cleanNumber($exist_alianz->order_ofd_dis_per?:0);
            $new_order->order_ofd_dis_amt = $this->cleanNumber($exist_alianz->order_ofd_dis_amt?:0);
            $new_order->order_3rd_rider_amt = $this->cleanNumber($exist_alianz->order_3rd_rider_amt?:0);
            $new_order->order_4th_rider_amt = $this->cleanNumber($exist_alianz->order_4th_rider_amt?:0);
            $new_order->order_addtional_amt = $this->cleanNumber($exist_alianz->order_addtional_amt?:0);
            $new_order->order_subtotal_amount = $this->cleanNumber($exist_alianz->order_subtotal_amount?:0);
            $new_order->order_gst_percent = $this->cleanNumber($exist_alianz->order_gst_percent?:0);
            $new_order->order_gst_amount = $this->cleanNumber($exist_alianz->order_gst_amount?:0);
            $new_order->order_direct_discount_amt = $this->cleanNumber($exist_alianz->order_direct_discount_amt?:0);
            $new_order->order_bank_interest = $this->cleanNumber($exist_alianz->order_bank_interest?:0);
            $new_order->order_grosspremium_amount = $this->cleanNumber($exist_alianz->order_grosspremium_amount?:0);
            $new_order->order_extra_name = $exist_alianz->order_extra_name?:"";
            $new_order->order_extra_amount = $this->cleanNumber($exist_alianz->order_extra_amount?:0);
            $new_order->order_receivable_dealercommpercent = $this->cleanNumber($exist_alianz->order_receivable_dealercommpercent?:0);
            $new_order->order_receivable_dealercomm = $this->cleanNumber($exist_alianz->order_receivable_dealercomm?:0);
            $new_order->order_receivable_dealergst = $this->cleanNumber($exist_alianz->order_receivable_dealergst?:0);
            $new_order->order_receivable_dealergstamount = $this->cleanNumber($exist_alianz->order_receivable_dealergstamount?:0);
            $new_order->order_receivable_gstpercent = $this->cleanNumber($exist_alianz->order_receivable_gstpercent?:0);
            $new_order->order_receivable_gstamount = $this->cleanNumber($exist_alianz->order_receivable_gstamount?:0);
            $new_order->order_receivable_premiumreceivable = $this->cleanNumber($exist_alianz->order_receivable_premiumreceivable?:0);
            $new_order->order_payable_ourcommpercent = $this->cleanNumber($exist_alianz->order_payable_ourcommpercent?:0);
            $new_order->order_payable_ourcomm = $this->cleanNumber($exist_alianz->order_payable_ourcomm?:0);
            $new_order->order_payable_nettcommpercent = $this->cleanNumber($exist_alianz->order_payable_nettcommpercent?:0);
            $new_order->order_payable_nettcomm = $this->cleanNumber($exist_alianz->order_payable_nettcomm?:0);
            $new_order->order_payable_gstpercent = $this->cleanNumber($exist_alianz->order_payable_gstpercent?:0);
            $new_order->order_payable_gst = $this->cleanNumber($exist_alianz->order_payable_gst?:0);
            $new_order->order_payable_premium = $this->cleanNumber($exist_alianz->order_payable_premium?:0);
            $new_order->order_status = 1;
            $new_order->insertBy = Auth::user()->id;
            $new_order->updateBy = Auth::user()->id;
            $new_order->save();

            $new_motor = new Motor();
            $new_motor->order_record_billto = $exist_alianz->order_record_billto?:"";
            $new_motor->order_period_from = $exist_alianz->order_period_from?:"";
            $new_motor->order_period_to = $exist_alianz->order_period_to?:"";
            $new_motor->order_cancel_date = $exist_alianz->order_cancel_date?:NULL;
            $new_motor->order_date = $exist_alianz->order_date?:"";
            $new_motor->order_insurco = $exist_alianz->order_insurco?:"";
            $new_motor->order_coverage = $exist_alianz->order_coverage?:"";
            $new_motor->order_vehicles_no = $exist_alianz->order_vehicles_no?:"";
            $new_motor->order_cvnote = $exist_alianz->order_cvnote?:"";
            $new_motor->order_policyno = $exist_alianz->order_policyno?:"";
            $new_motor->order_financecode = $exist_alianz->order_financecode?:"";
            $new_motor->order_vehicles_brand = $exist_alianz->order_vehicles_brand?:"";
            $new_motor->order_vehicles_model = $exist_alianz->order_vehicles_model?:"";
            $new_motor->order_vehicles_engine = $exist_alianz->order_vehicles_engine?:"";
            $new_motor->order_vehicles_body_type = $exist_alianz->order_vehicles_body_type?:"";
            $new_motor->order_vehicles_chasis = $exist_alianz->order_vehicles_chasis?:"";
            $new_motor->order_vehicles_origdt = $exist_alianz->order_vehicles_origdt?:NULL;
            $new_motor->order_vehicles_capacity = $exist_alianz->order_vehicles_capacity?:"";
            $new_motor->order_vehicles_seatno = $this->cleanNumber($exist_alianz->order_vehicles_seatno?:0);
            $new_motor->order_nameddriver_amount = $this->cleanNumber($exist_alianz->order_nameddriver_amount?:0);
            $new_motor->order_nameddriver = $exist_alianz->order_nameddriver?:"";
            $new_motor->order_tpft_amount = $this->cleanNumber($exist_alianz->order_tpft_amount?:0);
            $new_motor->order_tpft_driver = $exist_alianz->order_tpft_driver?:"";
            $new_motor->order_out_amount = $this->cleanNumber($exist_alianz->order_out_amount?:0);
            $new_motor->order_out_name = $exist_alianz->order_out_name?:"";
            $new_motor->order_extrabenefit = $exist_alianz->order_extrabenefit?:"";
            $new_motor->order_proformsendout_date = date('Y-m-d');
            $new_motor->order_proformsendout_by = Auth::user()->getName();
            $new_motor->order_policysendout_date = date('Y-m-d');
            $new_motor->order_policysendout_by = Auth::user()->getName();
            $new_motor->order_dncheckedsendout_date = date('Y-m-d');
            $new_motor->order_dncheckedsendout_by = Auth::user()->getName();
            $new_motor->order_ncd_previousinsurer = $exist_alianz->order_ncd_previousinsurer?:"";
            $new_motor->order_ncd_entitlement = $exist_alianz->order_ncd_entitlement?:"";
            $new_motor->order_ncd_cancellationdate = $exist_alianz->order_ncd_cancellationdate?:NULL;
            $new_motor->order_ncd_vehiclenumber = $exist_alianz->order_ncd_vehiclenumber?:"";
            $new_motor->order_ncd_remark = $exist_alianz->order_ncd_remark?:"";
            $new_motor->order_notes_remark = $exist_alianz->order_notes_remark?:"";
            $new_motor->order_driver1name = $exist_alianz->driver_lname_1." ".$exist_alianz->driver_fname_1;
            $new_motor->order_driver1occupation = $exist_alianz->order_driver1occupation?:"";
            $new_motor->order_driver1pass_date = $exist_alianz->order_driver1pass_date?:NULL;
            $new_motor->order_driver1pass_ic = $exist_alianz->driver_nirc_1?:"";
            $new_motor->order_driver1relationship = $exist_alianz->order_driver1relationship?:"";
            $new_motor->order_driver1dob = $exist_alianz->driver_dob_1?:NULL;
            $new_motor->order_driver1_remark = $exist_alianz->order_driver1_remark?:"";
            $new_motor->order_driver2name = $exist_alianz->driver_lname_2." ".$exist_alianz->driver_fname_2;
            $new_motor->order_driver2occupation = $exist_alianz->order_driver2occupation?:"";
            $new_motor->order_driver2pass_date = $exist_alianz->order_driver2pass_date?:NULL;
            $new_motor->order_driver2pass_ic = $exist_alianz->driver_nirc_2?:"";
            $new_motor->order_driver2relationship = $exist_alianz->order_driver2relationship?:"";
            $new_motor->order_driver2dob = $exist_alianz->driver_dob_2?:NULL;
            $new_motor->order_driver2_remark = $exist_alianz->order_driver2_remark?:"";
            $new_motor->order_driver3name = $exist_alianz->driver_lname_3." ".$exist_alianz->driver_fname_3;
            $new_motor->order_driver3occupation = $exist_alianz->order_driver3occupation?:"";
            $new_motor->order_driver3pass_date = $exist_alianz->order_driver3pass_date?:NULL;
            $new_motor->order_driver3pass_ic = $exist_alianz->driver_nirc_3?:"";
            $new_motor->order_driver3relationship = $exist_alianz->order_driver3relationship?:"";
            $new_motor->order_driver3dob = $exist_alianz->driver_dob_3?:NULL;
            $new_motor->order_driver3_remark = $exist_alianz->order_driver3_remark?:"";
            $new_motor->order_driver4name = $exist_alianz->driver_lname_4." ".$exist_alianz->driver_fname_4;
            $new_motor->order_driver4occupation = $exist_alianz->order_driver4occupation?:"";
            $new_motor->order_driver4pass_date = $exist_alianz->order_driver4pass_date?:NULL;
            $new_motor->order_driver4pass_ic = $exist_alianz->driver_nirc_4?:"";
            $new_motor->order_driver4relationship = $exist_alianz->order_driver4relationship?:"";
            $new_motor->order_driver4dob = $exist_alianz->driver_dob_4?:NULL;
            $new_motor->order_driver4_remark = $exist_alianz->order_driver4_remark?:"";
            $new_motor->order_id = $new_order->order_id;
            $new_motor->insertBy = Auth::user()->id;
            $new_motor->updateBy = Auth::user()->id;
            $new_motor->save();

            $exist_alianz->order_ref = $new_order->order_id;
            $exist_alianz->save();

            return redirect()->route('AllianzList')->with('success_msg', 'Copy Case Succesfully');
        }
        else{
            return redirect()->back()->with('error_msg','Invalid case');
        }
    }

    public function get_comp_base_premium($age, $capacity, $vehicles_age){
        return $this->comp_base_premium[$age][$capacity][$vehicles_age]?:0;
    }

    public function get_tpft_base_premium($age, $capacity, $vehicles_age){
        return $this->tpft_base_premium[$age][$capacity][$vehicles_age]?:0;
    }

    public function get_tpo_base_premium($age, $capacity){
        return $this->tpo_base_premium[$age][$capacity]?:0;
    }

    public function calculatetotal(Request $request){
        $order_excess_amount = 0;
        $cover_period = $this->periodCal($request->input('order_period_from'), $request->input('order_period_to'));
        if($request->input('driver_dob_1') && $request->input('driver_nirc_1') && $request->input('order_vehicles_reg_year') && $request->input('order_vehicles_capacity') && $request->input('order_period_from') && $request->input('order_period_to') && $request->input('order_coverage')){
            $order_excess_amount = 0;
            $order_premium_amt = 0;
            $vage = $this->VehiclesAge($request->input('order_vehicles_reg_year'));
            $vcap = $this->VehiclesCap($request->input('order_vehicles_capacity'));

            for($i=1;$i<5;$i++){
                if($request->input('driver_dob_'.$i) && $request->input('driver_nirc_'.$i)){
                    $driver_age = $this->AgeCal($request->input('driver_dob_'.$i));
                    $age = $this->AgeLVl($driver_age);
                    if($request->input('order_coverage') == 'COMP'){
                        $cur_order_premium_amt = isset($this->comp_base_premium[$age][$vcap][$vage])?$this->comp_base_premium[$age][$vcap][$vage]:0;
                        $cur_order_excess_amount = isset($this->comp_excess[$age][$vcap])?$this->comp_excess[$age][$vcap]:0;
                    }
                    elseif($request->input('order_coverage') == 'TPFT'){
                        $cur_order_premium_amt = isset($this->tpft_base_premium[$age][$vcap][$vage])?$this->tpft_base_premium[$age][$vcap][$vage]:0;
                        $cur_order_excess_amount = isset($this->tpft_excess[$age][$vcap])?$this->tpft_excess[$age][$vcap]:0;
                    }
                    elseif($request->input('order_coverage') == 'TPO'){
                        $cur_order_premium_amt = isset($this->tpo_base_premium[$age][$vcap])?$this->tpo_base_premium[$age][$vcap]:0;
                        $cur_order_excess_amount = 0;
                    }

                    if($cur_order_premium_amt > $order_premium_amt){
                        $order_premium_amt = $cur_order_premium_amt;
                        $order_excess_amount = $cur_order_excess_amount;
                    }
                }
            }
            
            $first_premium_amt = $order_premium_amt;
            $next_premium_amt = 0;
            if($cover_period == 2){
                $next_premium_amt = $order_premium_amt;
                $order_premium_amt = $order_premium_amt + $next_premium_amt;
            }
        }   
        else{
            $output2 = [
                'order_premium_amt' => 0,
                'order_accident_amt' => 0,
                'order_ncd_dis_amt' => 0,
                'order_in_exp_amt' => 0,
                'order_addtional_amt' => 0,
                'order_delivery_amt' => 0,
                'order_subtotal_amount' => 0,
                'order_excess_amount' => 0,
                'order_discount' => 0,
            ];
    
            return response()->json($output2, 200); 
        }

        $first_accident_amt = 0;
        $next_accident_amt = 0;
        $order_accident_amt = 0;
        if($request->input('order_delivery_opt') == 'y'){
            $order_delivery_fees = $this->delivery_fees;
        }
        else{
            $order_delivery_fees = 0;
        }
       

        if($request->input('order_cus_claim_amount') > 5000 && $request->input('order_cus_claim_amount') < 10000 ){
            $first_accident_amt = $first_premium_amt * 0.5;
            $next_accident_amt = $next_premium_amt * 0.5;
            $order_accident_amt = $order_premium_amt * 0.5;
        }
        elseif($request->input('order_cus_claim_amount') > 10000 && $request->input('order_cus_claim_amount') <= 15000){
            $first_accident_amt = $first_premium_amt;
            $next_accident_amt = $next_premium_amt;
            $order_accident_amt = $order_premium_amt;
        }
        
        $order_ncd_dis_amt = ($first_premium_amt + $first_accident_amt) * $request->input('order_ncd_entitlement') / 100;

        if($request->input('order_ncd_entitlement') == 20){
            $first_extra_5 = ($first_premium_amt + $first_accident_amt - $order_ncd_dis_amt) * 5 /100;
            $order_ncd_dis_amt += $first_extra_5;
        }

        if($cover_period == 2){
            $current_ncd = $request->input('order_ncd_entitlement')?:0;
            $next_ncd = $this->NextNCD($current_ncd);
            $next_ncd_amt = ($next_premium_amt + $next_accident_amt) * $next_ncd / 100;
            if($next_ncd == 20){
                $extra_5 = ($next_premium_amt + $next_accident_amt - $next_ncd_amt) * 5 /100;
                $next_ncd_amt += $extra_5;
            }
            $order_ncd_dis_amt += $next_ncd_amt;
            $order_delivery_fees +=  $order_delivery_fees;
        }

        $order_in_exp_amt = 0;

        for($i=1;$i<5;$i++){
            if($request->input('driver_exp_'.$i) == 0 && $request->input('driver_nirc_'.$i)){
                $order_in_exp_amt = 200;
                break;
            }
        }

        if($order_in_exp_amt == 2){
            $order_in_exp_amt += $order_in_exp_amt;
        }

        $order_addtional_amt = 0;

        if($request->input('driver_nirc_3')){
            $order_addtional_amt += 100;
        }

        if($request->input('driver_nirc_4')){
            $order_addtional_amt += 100;
        }
        
        if($cover_period == 2){
            $order_addtional_amt += $order_addtional_amt;
        }

        $order_subtotal_amount = $order_premium_amt + $order_accident_amt - $order_ncd_dis_amt + $order_in_exp_amt + $order_addtional_amt +  $order_delivery_fees;

        $order_discount = 0;

        if($cover_period == 2){
            $order_discount = $order_subtotal_amount * 0.1;
        }

        $output = [
            'order_premium_amt' => $order_premium_amt,
            'order_accident_amt' => $order_accident_amt,
            'order_ncd_dis_amt' => $order_ncd_dis_amt,
            'order_in_exp_amt' => $order_in_exp_amt,
            'order_addtional_amt' => $order_addtional_amt,
            'order_delivery_amt' => $order_delivery_fees,
            'order_subtotal_amount' => $order_subtotal_amount,
            'order_excess_amount' => $order_excess_amount,
            'order_discount' => $order_discount,
        ];

        return response()->json($output, 200); 
    }

    public function AgeCal($dob){
        $years = Carbon::parse($dob)->age;
        return $years;
    }

    public function AgeLVl($age){
        switch (true) {
            case $age <= 20:
                $age = 20;
                break;
            case $age <= 25:
                $age = 25;
                break;
            case $age <= 30:
                $age = 30;
                break;
            case $age <= 50:
                $age = 50;
                break;
            case $age <= 70:
                $age = 70;
                break;
        }
        return $age;
    }

    public function VehiclesAge($year){
        $diff = date('Y') - $year;
        switch (true) {
            case $diff <= 0:
                $diff = 0;
                break;
            case $diff <= 1:
                $diff = 1;
                break;
            case $diff <= 2:
                $diff = 2;
                break;
            case $diff <= 5:
                $diff = 5;
                break;
            case $diff <= 15:
                $diff = 15;
                break;
        }
        return $diff;
    }   

    public function VehiclesCap($cap){
        switch (true) {
            case $cap <= 200:
                $cap = 200;
                break;
            case $cap <= 400:
                $cap = 400;
                break;
            case $cap <= 750:
                $cap = 750;
                break;
            case $cap <= 1200:
                $cap = 1200;
                break;
            case $cap <= 1500:
                $cap = 1500;
                break;
        }
        return $cap;
    }

    public function NextNCD($ncd){
        switch ($ncd) {
            case 0:
                $ncd = 10;
                break;
            case 10:
                $ncd = 15;
                break;
            case 15:
                $ncd = 20;
                break;
        }
        return $ncd;
    }

    public function periodCal($from, $to){
        $to = Carbon::parse($to);
        $from = Carbon::parse($from);
        $days = $to->diffInDays($from);
        if($days > 365){
            return 2;
        }
        else{
            return 1;
        }
    }

    public static function validIdentification($number)
    {

        if (strlen($number) !== 9) {
            return false;
        }
        $newNumber = str_split(strtoupper($number));
        $icArray = [];
        for ($i = 0; $i < 9; $i++) {
            $icArray[$i] = $newNumber[$i];
        }
        $icArray[1] = intval($icArray[1], 10) * 2;
        $icArray[2] = intval($icArray[2], 10) * 7;
        $icArray[3] = intval($icArray[3], 10) * 6;
        $icArray[4] = intval($icArray[4], 10) * 5;
        $icArray[5] = intval($icArray[5], 10) * 4;
        $icArray[6] = intval($icArray[6], 10) * 3;
        $icArray[7] = intval($icArray[7], 10) * 2;

        $weight = 0;
        for ($i = 1; $i < 8; $i++) {
            $weight += $icArray[$i];
        }
        $offset = ($icArray[0] === "T" || $icArray[0] == "G") ? 4 : 0;
        $temp = ($offset + $weight) % 11;

        $st = ["J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A"];
        $fg = ["X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K"];

        $theAlpha = "";
        if ($icArray[0] == "S" || $icArray[0] == "T") {
            $theAlpha = $st[$temp];
        } else if ($icArray[0] == "F" || $icArray[0] == "G") {
            $theAlpha = $fg[$temp];
        }
        return ($icArray[8] === $theAlpha);
    }

    public function generateCode(){
        $ref = Refno::where('prefix_code', $this->prefix)->first();
        if($ref){
            $last_id = $ref->last_no;
            $last_id ++;
            $code =  "P".$this->prefix .'-'. sprintf("%06d", $last_id);
            $alliz_code = Order::where('order_no',$code)->first();
            if($alliz_code){
                $this->updateRefCode();
                return $this->generateCode();
            }
            else{
                return $code; 
            }
        }
        else{
            $new_ref = new Refno();
            $new_ref->prefix_code = $this->prefix;
            $new_ref->last_no = 0;
            $new_ref->save();
            $last_id = 1;
            $code =  "P".$this->prefix .'-'. sprintf("%06d", $last_id);
            $alliz_code = Order::where('order_no',$code)->first();
            if($alliz_code){
                $this->updateRefCode();
                return $this->generateCode();
            }
            else{
                return $code; 
            }
        }
    }

    public function updateRefCode(){
        $ref = Refno::where('prefix_code', $this->prefix)->first();
        $ref->last_no =  $ref->last_no + 1;
        $ref->save();
    }

    public static function hideNRICNumber($nric){
        $fnric = substr_replace($nric,'XXXX', 1,-4);
        return $fnric;
    }

    public function print(Request $request){
       
        $print_invoice_view = "printInvoice";

        $data = Allianz::where('order_status', "2")
        ->where('id', $request->route('id'))
        ->first()->toArray();
        
        if($data){
            $data['trans'] = [
                'COMP' => 'Comprehensive',
                'TPFT' => 'Third Party, Fire & Theft',
                'TPO' => 'Third Party',
            ];

            $data['order_financecode'] = $this->HiresComp($data['order_financecode']);
            $pdf = PDF::loadView($print_invoice_view, $data);
            $filename = $data['order_policyno']?:'NA'.'-AllianzCOI';
            return $pdf->stream($filename.'.pdf');
        }
        else{
            return redirect()->route('AllianzList')->with('error_msg', 'Invalid CI');
        }
    }

    public function ExportData(Request $request){

        if($request->input('start_date') && $request->input('end_date')){
            $excel_data = Allianz::select('insurance_allianz.*',  'tsa.dealer_name', 'vehicles_brand.name as vehicles_name')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'insurance_allianz.order_dealer')
            ->leftJoin('vehicles_brand', 'vehicles_brand.id', '=', 'insurance_allianz.order_vehicles_type')
            ->where('insurance_allianz.order_status',"2")
            ->where('insurance_allianz.order_date',">=", $request->input('start_date'))
            ->where('insurance_allianz.order_date',"<=", $request->input('end_date'))
            ->get()->toArray();
        }
        elseif($request->input('start_date') && !$request->input('end_date')){
            $excel_data = Allianz::select('insurance_allianz.*',  'tsa.dealer_name', 'vehicles_brand.name as vehicles_name')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'insurance_allianz.order_dealer')
            ->leftJoin('vehicles_brand', 'vehicles_brand.id', '=', 'insurance_allianz.order_vehicles_type')
            ->where('insurance_allianz.order_status',"2")
            ->where('insurance_allianz.order_date',">=", $request->input('start_date'))
            ->get()->toArray();
        }
        elseif(!$request->input('start_date') && $request->input('end_date')){
            $excel_data = Allianz::select('insurance_allianz.*',  'tsa.dealer_name', 'vehicles_brand.name as vehicles_name')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'insurance_allianz.order_dealer')
            ->leftJoin('vehicles_brand', 'vehicles_brand.id', '=', 'insurance_allianz.order_vehicles_type')
            ->where('insurance_allianz.order_status',"2")
            ->where('insurance_allianz.order_date',"<=", $request->input('end_date'))
            ->get()->toArray();
        }
        else{
            $excel_data = Allianz::select('insurance_allianz.*',  'tsa.dealer_name', 'vehicles_brand.name as vehicles_name')
            ->leftJoin('tsa', 'tsa.dealer_id', '=', 'insurance_allianz.order_dealer')
            ->leftJoin('vehicles_brand', 'vehicles_brand.id', '=', 'insurance_allianz.order_vehicles_type')
            ->where('insurance_allianz.order_status',"2")
            ->get()->toArray();
        }
        
        //$result = Excel::store(new AllianzExport($excel_data), 'daily_allianz_'.date('Y-m-d') . ".xlsx",'daily_allianz');
        $result = Excel::download(new AllianzExport($excel_data), 'daily_allianz_'.date('Y-m-d') . ".xlsx");

        return $result;
    }

    public function validationDetails(Request $request){
        $errors_input = [];

        if(!self::validIdentification($request->input('order_cus_nirc'))){
            $errors_input['order_cus_nirc'] = 'Customer Identity Number is invalid';
        }

        if($request->input('order_new_reg') == 'N' && !PlateCheckerController::check($request->input('order_vehicles_no'))){
            $errors_input['order_vehicles_no'] = 'Invalid Vehicle Plate';
        }

        if($request->input('order_status') == '2' && $request->input('order_premium_amt') <= 0){
            $errors_input['order_vehicles_no'] = 'Basic Premium must greater than 0';
        }

        if($request->input('order_cus_claim_amount') > 15000){
            $errors_input['order_cus_claim_amount'] = 'Claim Amount cannot Over 15000';
        }
        
        if($request->input('order_vehicles_capacity') > 1500){
            $errors_input['order_vehicles_capacity'] = 'Max Capacity is 1500';
        }
        elseif($request->input('order_vehicles_capacity') < 124){
            
            $errors_input['order_vehicles_capacity'] = 'Capacity lower than 124CC';
        }

        if($request->input('order_cus_claim') > 1){
            $errors_input['order_cus_claim'] = 'Over Claim, please contact with your supervisor';
        }

        if(strlen($request->input('order_cus_contact')) != 8){
            $errors_input['order_cus_contact'] = 'Mobile number only allow 8 digit';
        }

        for($i=1;$i<5;$i++){
            if($request->input('driver_nirc_'.$i) && $request->input('driver_dob_'.$i)){
                $age = $this->AgeLVl($request->input('driver_dob_'.$i));
                if($age > 70){
                    $errors_input['driver_dob_'.$i] = 'Rider '.$i.' age Cannot Over 70';
                }
                elseif($age < 18){
                    $errors_input['driver_dob_'.$i] = 'Rider '.$i.' age Cannot Lower than 18';
                }
            }

            if($request->input('driver_nirc_'.$i) && !self::validIdentification($request->input('driver_nirc_'.$i))){
                $errors_input['driver_nirc_'.$i] = 'Rider '.$i.' NRIC is invalid';
            }
            
        }

        if($request->input('order_vehicles_reg_year') && $request->input('order_coverage')){
            $year_diff = date('Y') - $request->input('order_vehicles_reg_year');
            if($request->input('order_coverage') == 'COMP' && $year_diff > 10){
                $errors_input['order_vehicles_reg_year'] = 'Registration Year Cannot Over 10 Year in this package';
            }
            elseif($request->input('order_coverage') == 'TPFT' && $year_diff > 15){
                $errors_input['order_vehicles_reg_year'] = 'Registration Year Cannot Over 15 Year in this package';
            }
            elseif($request->input('order_coverage') == 'TPO' && $year_diff > 30){
                $errors_input['order_vehicles_reg_year'] = 'Registration Year Cannot Over 30 Year in this package';
            }
        }

        return $errors_input;
    }

    public function AjaxvalidationDetails($field= '', $value='', Request $request){
        $errors_input = [];

        if($field == 'order_cus_nirc' || $request->input('order_cus_nirc') ){
            $check_val = $request->input('order_cus_nirc')?:$value;
            if(!self::validIdentification($check_val)){
                $errors_input['order_cus_nirc'] = 'Customer Identity Number is invalid';
            }
        }

        if($field == 'order_vehicles_no' || $request->input('order_vehicles_no') ){
            $check_val = $request->input('order_vehicles_no')?:$value;
            if(!PlateCheckerController::check($check_val)){
                $errors_input['order_vehicles_no'] = 'Invalid Vehicle Plate';
            }
        }

        if($field == 'order_cus_claim_amount' || $request->input('order_cus_claim_amount') ){
            $check_val = $request->input('order_cus_claim_amount')?:$value;
            if($check_val > 15000){
                $errors_input['order_cus_claim_amount'] = 'Claim Amount cannot Over 15000';
            }
        }

        if($field == 'order_vehicles_capacity' || $request->input('order_vehicles_capacity') ){
            $check_val = $request->input('order_vehicles_capacity')?:$value;
            if($check_val > 1500){
                $errors_input['order_vehicles_capacity'] = 'Max Capacity is 1500';
            }

            if($check_val < 124){
                $errors_input['order_vehicles_capacity'] = 'Capacity lower than 124CC';
            }
        }

        if($field == 'order_cus_claim' || $request->input('order_cus_claim') ){
            $check_val = $request->input('order_cus_claim')?:$value;
            if($check_val > 1){
                $errors_input['order_cus_claim'] = 'Over Claim, please contact with your supervisor';
            }
        }

        if($field == 'order_cus_contact' || $request->input('order_cus_contact') ){
            $check_val = $request->input('order_cus_contact')?:$value;
            if(strlen($check_val) != 8){
                $errors_input['order_cus_contact'] = 'Mobile number only allow 8 digit';
            }
        }

        for($i=1;$i<5;$i++){
            if($field == 'driver_dob_'.$i || $request->input('driver_dob_'.$i)){
                $check_val = $request->input('driver_dob_'.$i)?:$value;
                $age = $this->AgeLVl($check_val);
                if($age > 70){
                    $errors_input['driver_dob_'.$i] = 'Rider '.$i.' age Cannot Over 70';
                }
                elseif($age < 18){
                    $errors_input['driver_dob_'.$i] = 'Rider '.$i.' age Cannot Lower than 18';
                }
            }

            if($field == 'driver_nirc_'.$i || $request->input('driver_nirc_'.$i)){
                $check_val = $request->input('driver_nirc_'.$i)?:$value;
                if(!self::validIdentification($check_val)){
                    $errors_input['driver_nirc_'.$i] = 'Rider '.$i.' NRIC is invalid';
                }
            }
        }

        if($request->input('order_vehicles_reg_year') && $request->input('order_coverage')){
            $year_diff = date('Y') - $request->input('order_vehicles_reg_year');
            if($request->input('order_coverage') == 'COMP' && $year_diff > 10){
                $errors_input['order_vehicles_reg_year'] = 'Registration Year Cannot Over 10 Year in this package';
            }
            elseif($request->input('order_coverage') == 'TPFT' && $year_diff > 15){
                $errors_input['order_vehicles_reg_year'] = 'Registration Year Cannot Over 15 Year in this package';
            }
            elseif($request->input('order_coverage') == 'TPO' && $year_diff > 30){
                $errors_input['order_vehicles_reg_year'] = 'Registration Year Cannot Over 30 Year in this package';
            }
        }

        return $errors_input;
    }

    public function ajaxValidate(Request $request){
        $errors_input = [];
        $errors_input = $this->AjaxvalidationDetails($request->input('field'), $request->input('value'), $request);
        if(count( $errors_input) > 0){
            $output = [
                'status' => 'failed',
                'msg' => $errors_input[$request->input('field')],
            ];
            return response()->json($output, 200); 
        }
        else{
            $output = [
                'status' => 'success',
                'msg' => '',
            ];
            return response()->json($output, 200); 
        }
    }

    public function getFinanceCodeID($comp_name){
        $exist_hire = HiresComp::where('comp_name', strtoupper($comp_name))->first();
        if(!$exist_hire){
            $new_hire = new HiresComp();
            $new_hire->comp_name = strtoupper($comp_name);
            $new_hire->insertBy = Auth::user()->id;
            $new_hire->updateBy = Auth::user()->id;
            $new_hire->save();
            return $new_hire->id;
        }
        else{
            return $exist_hire->id;
        }
    }

    public function HiresComp($code){
        $hirescomp = HiresComp::where('id', $code)->first();
        if($hirescomp){
            return $hirescomp->comp_name;
        }
        else{
            return '-';
        }
    }

    public function getCustomerId($alianz_id){
        $exist_alianz = Allianz::find($alianz_id);
        $exist_cus = Customer::where("partner_nirc", $exist_alianz->order_cus_nirc)->first();
        if($exist_cus){
            return $exist_cus->partner_id;
        }
        else{
            $new_cus = new Customer();
            $new_cus->partner_name = $exist_alianz->order_cus_lname." ".$exist_alianz->order_cus_fname;
            $new_cus->partner_nirc = $exist_alianz->order_cus_nirc;
            $new_cus->partner_dob = $exist_alianz->order_cus_dob;
            $new_cus->partner_blk_no = $exist_alianz->order_cus_unitno;
            $new_cus->partner_street = $exist_alianz->order_cus_street_name;
            $new_cus->partner_unit_no = $exist_alianz->order_cus_build_name;
            $new_cus->partner_tel = $exist_alianz->order_cus_contact;
            $new_cus->partner_email = $exist_alianz->order_cus_email;
            $new_cus->partner_country = $exist_alianz->order_cus_national;
            $new_cus->partner_outlet = $exist_alianz->order_cus_national;
            $new_cus->partner_postal_code = $exist_alianz->order_cus_postal_code;
            $new_cus->partner_address = $exist_alianz->order_cus_address;
            $new_cus->partner_status = 1;
            $new_cus->insertBy = Auth::user()->id;
            $new_cus->updateBy = Auth::user()->id;
            $new_cus->save();

            return $new_cus->partner_id;
        }
    }

    public function generateMotorCode(){
        $ref = Refno::where('prefix_code', $this->motor_order_prefix)->first();
        if($ref){
            $last_id = $ref->last_no;
            $last_id ++;
        }
        else{
            $new_ref = new Refno();
            $new_ref->prefix_code = $this->motor_order_prefix;
            $new_ref->last_no = 0;
            $new_ref->save();
            $last_id = 1;
        }

        $code =  env("APP_CODE").$this->motor_order_prefix .'-'. sprintf("%06d", $last_id);

        $motor_code = Order::where('order_no',$code)->first();
        if($motor_code){
            $this->updateMotorRefCode();
            return $this->generateMotorCode();
        }
        else{
            return $code; 
        }
        
    }

    public function updateMotorRefCode(){
        $ref = Refno::where('prefix_code', $this->motor_order_prefix)->first();
        $ref->last_no =  $ref->last_no + 1;
        $ref->save();
    }

    public function cleanNumber($input){
        $input = preg_replace('/[^\d.]/', '', $input);
        return $input?:0;
    }
}
