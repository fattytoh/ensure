<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Empl;
use App\Models\Refno;
use App\Models\Department;
use App\Models\Group;
use App\Models\CProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EmplController extends Controller
{
    public $user_code = "EMPL";
    //
    public function getListing(Request $request){
        if(env('APP_ID') == 2){
            $listing_data = Empl::select('empl.*', 'department.department_code', 'group.group_code')
            ->leftJoin('department', 'department.department_id', '=', 'empl.empl_department')
            ->leftJoin('users', function($join){
                $join->on('empl.empl_id', '=', 'users.ref');
                $join->where('users.ref_code', '=', 'EMPL');
            })
            ->leftJoin('group', 'group.group_id', '=', 'users.ens_group_id')
            ->where('empl.empl_id', "!=",'1')
            ->where('empl.empl_outlet', '1')
            ->orWhere('empl.empl_outlet', env('APP_ID'))
            ->get()->toArray();
        }
        else{
            $listing_data = Empl::select('empl.*', 'department.department_code', 'group.group_code')
            ->leftJoin('department', 'department.department_id', '=', 'empl.empl_department')
            ->leftJoin('users', function($join){
                $join->on('empl.empl_id', '=', 'users.ref');
                $join->where('users.ref_code', '=', 'EMPL');
            })
            ->leftJoin('group', 'group.group_id', '=', 'users.peo_group_id')
            ->where('empl.empl_id', "!=",'1')
            ->where('empl.empl_outlet', '1')
            ->orWhere('empl.empl_outlet', env('APP_ID'))
            ->get()->toArray();
        }
        
        $data = [
            'list_data' => $listing_data,
        ];
        return view('empl.List', $data);
    }

    public function EmplInfo(Request $request){
        $cprofile_list = CProfile::select("cprofile_id", "cprofile_name")->get()->toArray();
        $department_list = Department::select("department_id", "department_code")->where("department_status", 1)->orderBy('department_seqno', 'ASC')->get()->toArray();
        $group_list = Group::select("group_id", "group_code")->where("group_status", 1)->where("group_type", "EMPL")->get()->toArray();
        if($request->route('id')){
            $data = Empl::find($request->route('id'));
            if(isset($data->empl_id)){
                $add_name = Empl::find($data->insertBy);
                $up_name = Empl::find($data->updateBy);
                $login_email = User::where('ref_code', $this->user_code)->where('ref', $request->route('id'))->first();
                $data->update_name =  isset($up_name->empl_name)?$up_name->empl_name:'Webmaster';
                $data->insert_name =  isset($add_name->empl_name)?$add_name->empl_name:'Webmaster';
                $data->empl_login_email =  isset($login_email->email)?$login_email->email:'';
                $data->cprofile_list = $cprofile_list;
                $data->department_list = $department_list;
                $data->group_list = $group_list;
                if(env('APP_ID') == 2){
                    $data->empl_group  = $login_email->ens_group_id;
                }
                else{
                    $data->empl_group  = $login_email->peo_group_id;
                }
               
                return view('empl.Info', $data);
            }
            else{
                return redirect()->route('EmplList');
            }
        }
        else{
            
            $data = [
                'cprofile_list' => $cprofile_list,
                'department_list' => $department_list,
                'group_list' => $group_list,
            ];
            return view('empl.Add', $data);
        }
    }

    public function create(Request $request){
        $rules = [
            'empl_name'    => 'required|min:3',
            'password'    => 'required|min:5',
            'cfm_password'    => 'same:password',
            'empl_login_email'    => 'required|email|unique:users,email',
            'empl_email'    => 'nullable|email',
            'empl_department'    => 'required',
            'empl_group'    => 'required',
            'empl_outlet'    => 'required',
        ];

        $msg = [
            'empl_name.required' => 'Please enter Employee name.',
            'empl_name.min' => 'Minimun Employee name is 3 character',
            'password.min' => 'Minimun Password is 5 character',
            'cfm_password.same' => 'Confirm Password Is not Same',
            'empl_login_email.required' => 'Login Email is require',
            'empl_login_email.email' => 'Invalid Email format',
            'empl_login_email.unique' => 'Login Email duplicate',
            'empl_email.email' => 'Invalid Email format',
            'empl_department.required' => 'Please Select a department',
            'empl_group.required' => 'Please Select a group',
            'empl_outlet.required' => 'Please Select a default Company',
        ];

        $request->validate($rules, $msg);
        $errors_input = [];

        if(count( $errors_input) > 0){
            return redirect()->back()->withInput()->withErrors($errors_input);
        }

        $new_empl = new Empl();
        $new_empl->empl_code = $this->generateCode();
        $this->updateRefCode();
        $new_empl->empl_name = $request->input('empl_name');
        $new_empl->empl_department = $request->input('empl_department');
        $new_empl->empl_outlet = $request->input('empl_outlet');
        $new_empl->empl_status = $request->input('empl_status');
        $new_empl->empl_mobile = $request->input('empl_mobile')?:'';
        $new_empl->empl_email = $request->input('empl_email')?:'';
        $new_empl->empl_tel = $request->input('empl_tel')?:'';
        $new_empl->empl_birthday = $request->input('empl_birthday')?:null;
        $new_empl->empl_joindate = $request->input('empl_joindate')?:null;
        $new_empl->empl_address = $request->input('empl_address')?:'';
        $new_empl->empl_remark = $request->input('empl_remark')?:'';
        $new_empl->insertBy = Auth::user()->id;
        $new_empl->updateBy = Auth::user()->id;
        $new_empl->save();

        $new_usr = new User();
        $new_usr->ref = $new_empl->empl_id;
        $new_usr->email = $request->input('empl_login_email');
        $new_usr->cprofile = $request->input('empl_outlet');
        $new_usr->ref_code = $this->user_code;
        if(env('APP_ID') == 2){
            $new_usr->ens_group_id  = $request->input('empl_group');
            $new_usr->peo_group_id  = 0;
        }
        else{
            $new_usr->peo_group_id  = $request->input('empl_group');
            $new_usr->ens_group_id  = 0;
        }
        $new_usr->password = Hash::make($request->input('password'));
        $new_usr->status = $request->input('empl_status');
        $new_usr->save();

        return redirect()->route('EmplList')->with('success_msg', 'Add Employee Succesfully');
    }

    public function update(Request $request){
        $exist_empl = Empl::find($request->input('empl_id'));
        $exist_usr = User::where('ref',$request->input('empl_id'))->where('ref_code', $this->user_code)->first();

        if(!$exist_empl || !$exist_usr){
            return redirect()->route('EmplList')->with('error_msg', 'Invalid Employee');
        }
        else{
            $rules = [
                'empl_name'    => 'required|min:3',
                'password'    => 'nullable|min:5',
                'cfm_password'    => 'same:password',
                'empl_login_email'    => 'required|email|unique:users,email,'.$request->input('empl_id').',ref',
                'empl_email'    => 'nullable|email',
                'empl_department'    => 'required',
                'empl_group'    => 'required',
                'empl_outlet'    => 'required',
            ];
    
            $msg = [
                'empl_name.required' => 'Please enter Employee name.',
                'empl_name.min' => 'Minimun Employee name is 3 character',
                'password.min' => 'Minimun Password is 5 character',
                'cfm_password.same' => 'Confirm Password Is not Same',
                'empl_login_email.required' => 'Login Email is require',
                'empl_login_email.email' => 'Invalid Email format',
                'empl_login_email.unique' => 'Login Email duplicate',
                'empl_email.email' => 'Invalid Email format',
                'empl_department.required' => 'Please Select a department',
                'empl_group.required' => 'Please Select a group',
                'empl_outlet.required' => 'Please Select a default Company',
            ];

            $request->validate($rules, $msg);
            $errors_input = [];

            if(count( $errors_input) > 0){
                return redirect()->back()->withInput()->withErrors($errors_input);
            }

            

            $exist_empl->empl_name = $request->input('empl_name');
            $exist_empl->empl_department = $request->input('empl_department');
            $exist_empl->empl_outlet = $request->input('empl_outlet');
            $exist_empl->empl_status = $request->input('empl_status');
            $exist_empl->empl_mobile = $request->input('empl_mobile')?:'';
            $exist_empl->empl_email = $request->input('empl_email')?:'';
            $exist_empl->empl_tel = $request->input('empl_tel')?:'';
            $exist_empl->empl_birthday = $request->input('empl_birthday')?:null;
            $exist_empl->empl_joindate = $request->input('empl_joindate')?:null;
            $exist_empl->empl_address = $request->input('empl_address')?:'';
            $exist_empl->empl_remark = $request->input('empl_remark')?:'';
            $exist_empl->updateBy = Auth::user()->id;
            $exist_empl->save();

            $exist_usr->email = $request->input('empl_login_email');
            if($request->input('password')){
                $exist_usr->password = Hash::make($request->input('password'));
            }
            $exist_usr->status = $request->input('empl_status');
            if(env('APP_ID') == 2){
                $exist_usr->ens_group_id  = $request->input('empl_group');
            }
            else{
                $exist_usr->peo_group_id  = $request->input('empl_group');
            }
            $exist_usr->save();
            return redirect()->back()->with('sweet_success_msg','Update Success');
        }
    }

    public function delete(Request $request){
        if($request->route('id') == Auth::user()->ref){
            return redirect()->route('EmplList')->with('error_msg', 'You Cannot Delete Yourself');
        }
        else{
            $empl = Empl::find($request->route('id'));
            if($empl){
                $empl->empl_status = "0";
                $empl->save();
            }
            $usr = User::where('ref',$request->route('id'))->where('ref_code', $this->user_code)->first();
            if($usr){
                $usr->status = "0";
                $usr->save();
            }
            return redirect()->route('EmplList')->with('success_msg', 'Delete Employee Succesfully');
        }
    }

    public function generateCode(){
        $ref = Refno::where('prefix_code', $this->user_code)->first();
        if($ref){
            $last_id = $ref->last_no;
            $last_id ++;
            $code =  $this->user_code .'-'. sprintf("%04d", $last_id);
            $empl_code = Empl::where('empl_code',$code)->first();
            if($empl_code){
                $this->updateRefCode();
                return $this->generateCode();
            }
            else{
                return $code; 
            }
        }
        else{
            $new_ref = new Refno();
            $new_ref->prefix_code = $this->user_code;
            $new_ref->last_no = 0;
            $new_ref->save();
            $last_id = 1;
            $code =  $this->user_code .'-'. sprintf("%04d", $last_id);
            $empl_code = Empl::where('empl_code',$code)->first();
            if($empl_code){
                $this->updateRefCode();
                return $this->generateCode();
            }
            else{
                return $code; 
            }
        }
    }

    public function updateRefCode(){
        $ref = Refno::where('prefix_code', $this->user_code)->first();
        $ref->last_no =  $ref->last_no + 1;
        $ref->save();
    }
}
