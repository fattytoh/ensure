<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\UserGroupPrivilegeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class PrivilegeCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $pri = new UserGroupPrivilegeController();
        if($pri->checkPrivilegeWithAuth(Route::currentRouteName())){
            return $next($request);
        }
        else{
            abort("403");
        }
    }
}
