<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allianz extends Model
{
    use HasFactory;

    protected $table = 'insurance_allianz';
}
