<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'paymentline_id';

    protected $table = 'payment_detail';

    protected $fillable = ['paymentline_status'];

}
