<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SompoData extends Model
{
    use HasFactory;

    protected $primaryKey = 'sompo_id';

    protected $table = 'sompo_data';

    protected $fillable = ['sompo_ec_type', 'sompo_ref_code', 'sompo_ec_type_code', 'sompo_field_name', 'sompo_order_field_name', 'sompo_data_value', 'sompo_display_name', 'sompo_original_data', 'sompo_data_status','insertBy', 'updateBy'];
}
