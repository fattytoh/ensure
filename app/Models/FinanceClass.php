<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinanceClass extends Model
{
    use HasFactory;

    protected $primaryKey = 'financeclass_id';

    protected $table = 'financeclass';
}
