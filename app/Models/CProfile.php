<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CProfile extends Model
{
    use HasFactory;

    protected $primaryKey = 'cprofile_id';

    protected $table = 'cprofile';
}
