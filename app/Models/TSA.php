<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TSA extends Model
{
    use HasFactory;

    protected $primaryKey = 'dealer_id';

    protected $table = 'tsa';
}
