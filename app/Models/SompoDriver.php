<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SompoDriver extends Model
{
    use HasFactory;

    protected $primaryKey = 'sompo_driver_id';

    protected $table = 'sompo_driver_list';

    protected $fillable = ['sompo_driver_status', 'updateBy'];
}
