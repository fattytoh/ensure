<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Insuranceco extends Model
{
    use HasFactory;

    protected $primaryKey = 'insuranceco_id';

    protected $table = 'insurance_comp';

}
