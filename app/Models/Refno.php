<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Refno extends Model
{
    use HasFactory;

    protected $table = 'refno';
}
