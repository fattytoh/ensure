<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupPrivilege extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'group_privilege';

    protected $fillable = ['group_id', 'route_name', 'insertBy', 'updateBy', 'status'];
}
