<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sompo extends Model
{
    use HasFactory;

    protected $primaryKey = 'order_sompo_id';

    protected $table = 'insurance_sompo';
}
