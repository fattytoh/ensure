<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HiresComp extends Model
{
    use HasFactory;

    protected $table = 'hire_comp';
}
