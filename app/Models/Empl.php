<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empl extends Model
{
    use HasFactory;

    protected $primaryKey = 'empl_id';

    protected $table = 'empl';
}
