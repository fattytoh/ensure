<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiptDetail extends Model
{
    use HasFactory;

    protected $primaryKey = 'recpline_id';

    protected $table = 'receipt_detail';

    protected $fillable = ['recpline_status'];
}
