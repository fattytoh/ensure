<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SompoApi extends Model
{
    use HasFactory;

    protected $primaryKey = 'api_id';

    protected $table = 'sompo_api';
}
