<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', function () {
    Auth::logout();
    // dd(Hash::make('testing123'));
    return view('login');
})->name('login');

Route::get('/logout', 'App\Http\Controllers\LoginController@logout')->name('logout');
Route::get('/switchLogin/{token}', 'App\Http\Controllers\LoginController@switchLogin')->name('switchLogin');

Route::post('/userlogin', 'App\Http\Controllers\LoginController@Login')->name('userlogin');

Route::middleware('auth')->group(function () {   
    Route::get('/', function () { return view('dashboard'); })->name('dashboard');
    Route::get('/switch', 'App\Http\Controllers\LoginController@switchSite')->name('SwitchSite');

    // Select management
    Route::controller('App\Http\Controllers\SelectController')->group(function () {
        Route::get('/select/tsa', 'getTSA')->name('TSAselect');
        Route::get('/select/vehicles', 'getVehicles')->name('Vehicleselect');
        Route::get('/select/HiresComp', 'getHiresComp')->name('HiresCompselect');
        Route::get('/select/Customer', 'getCustomer')->name('Cusselect');
    });

    // User Controller
    Route::controller('App\Http\Controllers\UserController')->group(function () {
        Route::get('/profile', 'profile')->name('profile');
        Route::post('/profile/update', 'updateProfile')->name('ProfileUpdate');
    });

    // Sompo insurance
    Route::controller('App\Http\Controllers\SompoController')->group(function () {
        Route::get('/sompo/list', 'getListing')->name('SompoList');
        Route::get('/sompo/addnew', 'getSompoInfo')->name('SompoAddNew');
        Route::get('/sompo/{id}', 'getSompoInfo')->name('SompoInfo');
        Route::post('/sompo/create', 'create')->name('SompoCreate');
        Route::post('/sompo/update', 'update')->name('SompoUpdate');
        Route::get('/sompo/delete/{id}', 'delete')->name('SompoDelete');
        Route::get('/sompo/copy/{id}', 'copyToLocal')->name('SompoCopy');
        Route::get('/sompo/view/{id}', 'getSompoViewInfo')->name('SompoView');
    });

    // Allianz insurance
    Route::controller('App\Http\Controllers\AllianzController')->group(function () {
        Route::get('/allianz/list', 'getListing')->name('AllianzList');
        Route::get('/allianz/addnew', 'InsuranceInfo')->name('AllianzAddNew');
        Route::post('/allianz/create', 'create')->name('AllianzCreate');
        Route::post('/allianz/update', 'update')->name('AllianzUpdate');
        Route::get('/allianz/delete/{id}', 'delete')->name('AllianzDelete');
        Route::get('/allianz/{id}', 'InsuranceInfo')->name('AllianzView');
        Route::get('/allianz/copy/{id}', 'copyToLocal')->name('AllianzCopy');
        Route::get('/allianz/print/{id}', 'print')->name('AllianzPrint');
        Route::post('/allianz/exportdata', 'ExportData')->name('AllianzExportData');
    });
    
    // TSA management
    Route::controller('App\Http\Controllers\TsaController')->group(function () {
        Route::get('/tsa/list', 'getListing')->name('TSAList');
        Route::get('/tsa/addnew', 'TSAInfo')->name('TSAAddNew');
        Route::post('/tsa/create', 'create')->name('TSACreate');
        Route::post('/tsa/update', 'update')->name('TSAUpdate');
        Route::get('/tsa/delete/{id}', 'delete')->name('TSADelete');
        Route::get('/tsa/{id}', 'TSAInfo')->name('TSAView');
    });

    // Customer management
    Route::controller('App\Http\Controllers\CustomerController')->group(function () {
        Route::get('/customer/list', 'getListing')->name('CustomerList');
        Route::get('/customer/info/{id}', 'CustomerInfo')->name('CustomerInfo');
        Route::get('/customer/addNew', 'CustomerInfo')->name('CustomerAddnew');
        Route::post('/customer/create', 'create')->name('CustomerCreate');
        Route::post('/customer/update', 'update')->name('CustomerUpdate');
        Route::get('/customer/delete/{id}', 'delete')->name('CustomerDelete');
    });
    // User Group management
    Route::controller('App\Http\Controllers\UserGroupController')->group(function () {
        Route::get('/usrgrp/list', 'getListing')->name('UserGroupList');
        Route::get('/usrgrp/info/{id}', 'GroupInfo')->name('UserGroupInfo');
        Route::get('/usrgrp/addNew', 'GroupInfo')->name('UserGroupAddnew');
        Route::post('/usrgrp/create', 'create')->name('UserGroupCreate');
        Route::post('/usrgrp/update', 'update')->name('UserGroupUpdate');
        Route::get('/usrgrp/delete/{id}', 'delete')->name('UserGroupDel');
    });

    // Employee management
    Route::controller('App\Http\Controllers\EmplController')->group(function () {
        Route::get('/employee/list', 'getListing')->name('EmplList');
        Route::get('/employee/addnew', 'EmplInfo')->name('EmplAddNew');
        Route::post('/employee/create', 'create')->name('EmplCreate');
        Route::post('/employee/update', 'update')->name('EmplUpdate');
        Route::get('/employee/delete/{id}', 'delete')->name('EmplDelete');
        Route::get('/employee/{id}', 'EmplInfo')->name('EmplView');
    });

    // motor insurance
    Route::controller('App\Http\Controllers\MotorController')->group(function () {
        Route::get('/motor/list', 'getListing')->name('MotorList');
        Route::get('/motor/addnew', 'MotorInfo')->name('MotorAddNew');
        Route::post('/motor/create', 'create')->name('MotorCreate');
        Route::post('/motor/update', 'update')->name('MotorUpdate');
        Route::get('/motor/delete/{id}', 'delete')->name('MotorDelete');
        Route::get('/motor/{id}', 'MotorInfo')->name('MotorInfo');
        Route::get('/motor/endorsement/{id}', 'MotorEndorsement')->name('MotorEndorsement');
        Route::get('/motor/renewal/{id}', 'MotorRenewal')->name('MotorRenewal');
    });

    //Private motor insurance
    Route::controller('App\Http\Controllers\PrivateMotorController')->group(function () {
        Route::get('/pmotor/list', 'getListing')->name('PrivateMotorList');
        Route::get('/pmotor/addnew', 'MotorInfo')->name('PrivateMotorAddNew');
        Route::post('/pmotor/create', 'create')->name('PrivateMotorCreate');
        Route::post('/pmotor/update', 'update')->name('PrivateMotorUpdate');
        Route::get('/pmotor/delete/{id}', 'delete')->name('PrivateMotorDelete');
        Route::get('/pmotor/{id}', 'MotorInfo')->name('PrivateMotorInfo');
        Route::get('/pmotor/endorsement/{id}', 'MotorEndorsement')->name('PrivateMotorEndorsement');
        Route::get('/pmotor/renewal/{id}', 'MotorRenewal')->name('PrivateMotorRenewal');
    });

    //Commercial motor insurance
    Route::controller('App\Http\Controllers\CommercialMotorController')->group(function () {
        Route::get('/cmotor/list', 'getListing')->name('CommercialMotorList');
        Route::get('/cmotor/addnew', 'MotorInfo')->name('CommercialMotorAddNew');
        Route::post('/cmotor/create', 'create')->name('CommercialMotorCreate');
        Route::post('/cmotor/update', 'update')->name('CommercialMotorUpdate');
        Route::get('/cmotor/delete/{id}', 'delete')->name('CommercialMotorDelete');
        Route::get('/cmotor/{id}', 'MotorInfo')->name('CommercialMotorInfo');
        Route::get('/cmotor/endorsement/{id}', 'MotorEndorsement')->name('CommercialMotorEndorsement');
        Route::get('/cmotor/renewal/{id}', 'MotorRenewal')->name('CommercialMotorRenewal');
    });

    // Option Settings
    Route::controller('App\Http\Controllers\OptionController')->group(function () {
        Route::get('/option', 'index')->name('optionIndex');
        Route::post('/option/update', 'update')->name('optionUpdate');
    });

     //Maid insurance
    Route::controller('App\Http\Controllers\MaidController')->group(function () {
        Route::get('/maid/list', 'getListing')->name('MaidList');
        Route::get('/maid/addnew', 'MaidInfo')->name('MaidAddNew');
        Route::post('/maid/create', 'create')->name('MaidCreate');
        Route::post('/maid/update', 'update')->name('MaidUpdate');
        Route::get('/maid/delete/{id}', 'delete')->name('MaidDelete');
        Route::get('/maid/{id}', 'MaidInfo')->name('MaidInfo');
        Route::get('/maid/endorsement/{id}', 'MaidEndorsement')->name('MaidEndorsement');
        Route::get('/maid/renewal/{id}', 'MaidRenewal')->name('MaidRenewal');
    });

     //General insurance
    Route::controller('App\Http\Controllers\GeneralInsuranceController')->group(function () {
        Route::get('/general/list', 'getListing')->name('GeneralList');
        Route::get('/general/addnew', 'GeneralInfo')->name('GeneralAddNew');
        Route::post('/general/create', 'create')->name('GeneralCreate');
        Route::post('/general/update', 'update')->name('GeneralUpdate');
        Route::get('/general/delete/{id}', 'delete')->name('GeneralDelete');
        Route::get('/general/{id}', 'GeneralInfo')->name('GeneralInfo');
        Route::get('/general/endorsement/{id}', 'GeneralEndorsement')->name('GeneralEndorsement');
        Route::get('/general/renewal/{id}', 'GeneralRenewal')->name('GeneralRenewal');
    });

     //Payment
    Route::controller('App\Http\Controllers\PaymentController')->group(function () {
        Route::get('/payment/list', 'getListing')->name('PaymentList');
        Route::get('/payment/addnew', 'PaymentInfo')->name('PaymentAddNew');
        Route::get('/payment/{id}', 'PaymentInfo')->name('PaymentInfo');
        Route::get('/payment/view/{id}', 'PaymentViewInfo')->name('PaymentView');
        Route::post('/payment/create', 'create')->name('PaymentCreate');
        Route::post('/payment/update', 'update')->name('PaymentUpdate');
        Route::get('/payment/delete/{id}', 'delete')->name('PaymentDelete');
    });

    //Paymenttsa
    Route::controller('App\Http\Controllers\PaymentTsaController')->group(function () {
        Route::get('/payment/tsa/list', 'getListing')->name('PaymentTsaList');
        Route::get('/payment/tsa/addnew', 'PaymentInfo')->name('PaymentTsaAddNew');
        Route::get('/payment/tsa/{id}', 'PaymentInfo')->name('PaymentTsaInfo');
        Route::get('/payment/tsa/view/{id}', 'PaymentViewInfo')->name('PaymentTsaView');
        Route::post('/payment/tsa/create', 'create')->name('PaymentTsaCreate');
        Route::post('/payment/tsa/update', 'update')->name('PaymentTsaUpdate');
        Route::get('/payment/tsa/delete/{id}', 'delete')->name('PaymentTsaDelete');
    });

     //Paymentins
     Route::controller('App\Http\Controllers\PaymentInsureController')->group(function () {
        Route::get('/payment/ins/list', 'getListing')->name('PaymentInsList');
        Route::get('/payment/ins/addnew', 'PaymentInfo')->name('PaymentInsAddNew');
        Route::get('/payment/ins/{id}', 'PaymentInfo')->name('PaymentInsInfo');
        Route::get('/payment/ins/view/{id}', 'PaymentViewInfo')->name('PaymentInsView');
        Route::post('/payment/ins/create', 'create')->name('PaymentInsCreate');
        Route::post('/payment/ins/update', 'update')->name('PaymentInsUpdate');
        Route::get('/payment/ins/delete/{id}', 'delete')->name('PaymentInsDelete');
    });

     //Receipt
     Route::controller('App\Http\Controllers\ReceiptController')->group(function () {
        Route::get('/receipt/list', 'getListing')->name('ReceiptList');
        Route::get('/receipt/addnew', 'receiptInfo')->name('ReceiptAddNew');
        Route::get('/receipt/{id}', 'receiptInfo')->name('ReceiptInfo');
        Route::get('/receipt/view/{id}', 'receiptViewInfo')->name('ReceiptView');
        Route::post('/receipt/create', 'create')->name('ReceiptCreate');
        Route::post('/receipt/update', 'update')->name('ReceiptUpdate');
        Route::get('/receipt/delete/{id}', 'delete')->name('ReceiptDelete');
    });

     //Delete Attachment
     Route::controller('App\Http\Controllers\FilesController')->group(function () {
        Route::get('/payment/attach/delete/{id}', 'removeFile')->name('PaymentFileDelete');
        Route::get('/receipt/attach/delete/{id}', 'removeFile')->name('ReceiptFileDelete');
        Route::get('/maid/attach/delete/{id}', 'removeFile')->name('MaidFileDelete');
        Route::get('/receipt/allfiles/delete/{id}', 'removeAllFile')->name('AllReceiptFileDelete');
    });

    Route::controller('App\Http\Controllers\ReportController')->group(function () {
        Route::get('/report/tsa', 'tsastatementForm')->name('TsaFeesForm');
        Route::get('/report/outsatnding', 'outstandingForm')->name('OutStandingForm');
        Route::get('/report/accstate', 'accstatementForm')->name('AccStatementForm');
        Route::get('/report/premiumstate', 'premiumstateForm')->name('PremiumStateForm');
        Route::get('/report/debitnotes', 'debitnotesForm')->name('DebitNotesForm');
        Route::get('/report/creditnotes', 'creditnotesForm')->name('CreditNotesForm');
        Route::get('/report/dailytrans', 'dailytransForm')->name('DailyTransForm');
        Route::get('/report/compsale', 'compsalesForm')->name('CompSalesForm');
        Route::get('/report/cussale', 'cussalesForm')->name('CusSalesForm');
        Route::get('/report/tsasale', 'tsasalesForm')->name('TSASalesForm');
    });

    Route::controller('App\Http\Controllers\ReportPDFController')->group(function () {
        Route::get('/report/pdf/TsaFees', 'tsastatementreport')->name('TsaFeesReport');
        Route::get('/report/pdf/outstanding', 'outstandingreport')->name('OutstandingReport');
        Route::get('/report/pdf/accstate', 'AccountStatementReport')->name('AccountStatementReport');
        Route::get('/report/pdf/compsale', 'SalesReport')->name('CompSalesReport');
        Route::get('/report/pdf/creditNote', 'CreditNotereport')->name('CreditNoteReport');
        Route::get('/report/pdf/cussale', 'CusSalesReport')->name('CusSalesReport');
        Route::get('/report/pdf/dailtrans', 'DailyTransReport')->name('DailyTransReport');
        Route::get('/report/pdf/debinotes', 'debitnotereport')->name('debitnotereport');
        Route::get('/report/pdf/premiumstate', 'PremiumStatementReport')->name('PremiumStatementReport');
        Route::get('/report/pdf/tsasale', 'TSASalesReport')->name('TSASalesReport');
    });
});

Route::middleware('auth')->group(function () { 
    // Listdata
    Route::post('/customer/list/data', 'App\Http\Controllers\CustomerController@getListData')->name('cusListData');
    Route::post('/allianz/list/data', 'App\Http\Controllers\AllianzController@getListData')->name('allianzListData');
    Route::post('/sompo/list/data', 'App\Http\Controllers\SompoController@getListData')->name('sompoListData');
    Route::post('/motor/list/data', 'App\Http\Controllers\MotorController@getListData')->name('motorListData');
    Route::post('/pmotor/list/data', 'App\Http\Controllers\PrivateMotorController@getListData')->name('PrivatemotorListData');
    Route::post('/cmotor/list/data', 'App\Http\Controllers\CommercialMotorController@getListData')->name('CommercialmotorListData');
    Route::post('/maid/list/data', 'App\Http\Controllers\MaidController@getListData')->name('MaidListData');
    Route::post('/general/list/data', 'App\Http\Controllers\GeneralInsuranceController@getListData')->name('GeneralListData');

    Route::post('/payment/list/data', 'App\Http\Controllers\PaymentController@getListData')->name('PaymentListData');
    Route::post('/payment/list/orderdata', 'App\Http\Controllers\PaymentController@getOrderList')->name('PaymentOrderListData');
    Route::post('/payment/add/orderdata', 'App\Http\Controllers\PaymentController@AddOrderToList')->name('PaymentAddOrder');

    Route::post('/payment/tsa/list/data', 'App\Http\Controllers\PaymentTsaController@getListData')->name('PaymentTsaListData');
    Route::post('/payment/tsa/list/orderdata', 'App\Http\Controllers\PaymentTsaController@getOrderList')->name('PaymentOrderTsaListData');
    Route::post('/payment/tsa/add/orderdata', 'App\Http\Controllers\PaymentTsaController@AddOrderToList')->name('PaymentTsaAddOrder');

    Route::post('/payment/ins/list/data', 'App\Http\Controllers\PaymentInsureController@getListData')->name('PaymentInsListData');
    Route::post('/payment/ins/list/orderdata', 'App\Http\Controllers\PaymentInsureController@getOrderList')->name('PaymentOrderInsListData');
    Route::post('/payment/ins/add/orderdata', 'App\Http\Controllers\PaymentInsureController@AddOrderToList')->name('PaymentInsAddOrder');

    Route::post('/receipt/list/data', 'App\Http\Controllers\ReceiptController@getListData')->name('ReceiptListData');
    Route::post('/receipt/list/orderdata', 'App\Http\Controllers\ReceiptController@getOrderList')->name('ReceiptOrderListData');
    Route::post('/receipt/add/orderdata', 'App\Http\Controllers\ReceiptController@AddOrderToList')->name('ReceiptAddOrder');

    // ajax checking
    Route::controller('App\Http\Controllers\AllianzController')->group(function () {
        Route::post('/allianz/cal', 'calculatetotal')->name('AllianzCal');
        Route::post('/allianz/ajaxValidate', 'ajaxValidate')->name('AllianzValidate');
    });

    Route::controller('App\Http\Controllers\SompoController')->group(function () {
        Route::post('/sompo/vplanperiod', 'validate_plan_period')->name('Sompovplanperiod');
        Route::post('/sompo/postalcode', 'GetAddressByPostalCode')->name('SompoGetAddress');
        Route::post('/sompo/cusinfo', 'getCusInfo')->name('SompoGetCusInfo');
        Route::post('/sompo/cusinfonyic', 'getPartnerInfoByNRIC')->name('SompoGetCusInfoByIC');
        Route::post('/sompo/age/validate', 'validateAge')->name('SompoAgeValidate');
        Route::post('/sompo/validate/periodfrom', 'validatePeriodFrom')->name('SompoPeriodFromValidate');
        Route::post('/sompo/validate/periodto', 'validatePeriodTo')->name('SompoPeriodToValidate');
        Route::post('/sompo/validate/exp', 'validateExp')->name('SompoExpValidate');
    });

});

Route::controller('App\Http\Controllers\ExcelController')->group(function () {
    Route::get('/report/test', 'streamDownload')->name('reporttest');
});

 
